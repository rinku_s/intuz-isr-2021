// const withBundleAnalyzer = require("@next/bundle-analyzer")({
//   enabled: process.env.ANALYZE === "true",
// });

// module.exports = withBundleAnalyzer({});

module.exports = {
  images: {
    deviceSizes: [640, 750, 828, 1080, 1200],
  },
};
