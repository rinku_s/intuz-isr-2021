//@ts-nocheck
export const data = [
    {
        'index': 1,
        'id' : 'General',
        'content' : [
            {
                'index': 1,
                'image' : 'cat_general.png',
                'icons' : [
                    {
                        'name' : 'acceleration.png',
                        'title' : 'Acceleration'
                    },
                    {
                        'name' : 'angle.png',
                        'title' : 'Angle'
                    },
                    {
                        'name' : 'Area.png',
                        'title' : 'Area'
                    },
                    {
                        'name' : 'astronomical.png',
                        'title' : 'Astronomical'
                    },{
                        'name' : 'capacitance.png',
                        'title' : 'Capacitance'
                    },{
                        'name' : 'cooking.png',
                        'title' : 'Cooking'
                    },{
                        'name' : 'currency.png',
                        'title' : 'Currency'
                    },{
                        'name' : 'data.png',
                        'title' : 'Data'
                    },{
                        'name' : 'density.png',
                        'title' : 'Density'
                    },{
                        'name' : 'data transfer speed.png',
                        'title' : 'Data Transfer Speed'
                    },{
                        'name' : 'Electric-Current.png',
                        'title' : 'Electric Current'
                    },{
                        'name' : 'energy.png',
                        'title' : 'Energy'
                    },{
                        'name' : 'force.png',
                        'title' : 'Force'
                    },{
                        'name' : 'frequency.png',
                        'title' : 'Frequency'
                    },{
                        'name' : 'fuel.png',
                        'title' : 'Fuel'
                    },{
                        'name' : 'illuminance.png',
                        'title' : 'Illuminance'
                    },{
                        'name' : 'Light.png',
                        'title' : 'Light'
                    },{
                        'name' : 'Length.png',
                        'title' : 'Length'
                    },{
                        'name' : 'luminance.png',
                        'title' : 'Luminance'
                    },{
                        'name' : 'mass.png',
                        'title' : 'Mass'
                    },{
                        'name' : 'metric-prefixes.png',
                        'title' : 'Metric Prefixes'
                    },{
                        'name' : 'power.png',
                        'title' : 'Power'
                    },{
                        'name' : 'pressure.png',
                        'title' : 'Pressure'
                    },{
                        'name' : 'radiocativity.png',
                        'title' : 'Radio Activity'
                    },{
                        'name' : 'sound.png',
                        'title' : 'Sound'
                    },{
                        'name' : 'speed.png',
                        'title' : 'Speed'
                    },{
                        'name' : 'Temperature.png',
                        'title' : 'Temperature'
                    },{
                        'name' : 'typography.png',
                        'title' : 'Typography'
                    },{
                        'name' : 'torque.png',
                        'title' : 'Torque'
                    },{
                        'name' : 'time.png',
                        'title' : 'Time'
                    },{
                        'name' : 'viscosity.png',
                        'title' : 'Viscosity'
                    },{
                        'name' : 'volume.png',
                        'title' : 'Volume'
                    },{
                        'name' : 'volumetric-flow.png',
                        'title' : 'Volumetric Flow'
                    },{
                        'name' : 'weight.png',
                        'title' : 'Weight'
                    },
                ]
            }
        ]
    },
    {
        'id' : 'Clothing',
        'content' : [
            {
                'index': 2,
                'image' : 'cat_clothing.png',
                'icons' : [
                    {
                        'name' : 'children-clothing.png',
                        'title' : 'Children Clothing'
                    },{
                        'name' : 'children-shoes.png',
                        'title' : 'Children Shoes'
                    },{
                        'name' : 'male-shirts.png',
                        'title' : 'Male Shirts'
                    },{
                        'name' : 'male-suits.png',
                        'title' : 'Male Suits'
                    },{
                        'name' : 'male-tshirts.png',
                        'title' : 'Male Shirt'
                    },{
                        'name' : 'male-shoes.png',
                        'title' : 'Male Shoes'
                    },{
                        'name' : 'male-pants.png',
                        'title' : 'Male pants'
                    },{
                        'name' : 'women-shoes.png',
                        'title' : 'Women Shoes'
                    },{
                        'name' : 'women-clothing.png',
                        'title' : 'Women Clothing'
                    },{
                        'name' : 'women-dresses.png',
                        'title' : 'Women Dresses'
                    },{
                        'name' : 'women-ring-size.png',
                        'title' : 'Women Ring Sizes'
                    },
                ]
            }
        ]
    },
    {
        'id' : 'Utilities',
        'content' : [
            {
                'index': 3,
                'image' : 'cat_utilities.png',
                'icons' : [
                    {
                        'name' : 'business-day.png',
                        'title' : 'Business Day'
                    },{
                        'name' : 'calender-days.png',
                        'title' : 'Calendar Day'
                    },{
                        'name' : 'mortgage-payments.png',
                        'title' : 'Mortgage Payments'
                    },{
                        'name' : 'percentage.png',
                        'title' : 'Percentage'
                    },{
                        'name' : 'ROI.png',
                        'title' : 'ROI'
                    },{
                        'name' : 'split-check.png',
                        'title' : 'Split Check'
                    },{
                        'name' : 'tip-calculator.png',
                        'title' : 'Tip Calculator'
                    },
                ]
            }
        ]
    }
]