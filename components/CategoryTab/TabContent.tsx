//@ts-nocheck

import gsap from 'gsap';
import React, { Fragment, useEffect, useState } from "react";
import { cdn } from '../../config/cdn';
import LoadingSpinner from '../LoadingScreen/LoadingSpinner';
import style from './styles.module.scss';

const TabContent = (props) => {
    const [loaded, setLoaded] = useState(false);
    const tl = gsap.timeline({ paused: true });
    useEffect(() => {
        setTimeout(() => {
            setLoaded(true)
        }, 1000)
    }, [])


    return (
        <>
            {!loaded ? <LoadingSpinner /> :
                <div className={`${style.TabContent}`}>
                    {props.tabcontent && props.tabcontent.content.map((tab, index) => {
                        return (
                            <Fragment key={index}>
                                <div className={style.mobileImg}>
                                    <img src={cdn(`${tab.image}?auto=format&fm=png`)} alt={tab.image} />
                                </div>
                                <div className={style.imageGrp}>
                                    {tab.icons.map((icon, index) => {
                                        return (
                                            <div key={index}>

                                                <img src={(`/static/Images/icons/category_icon/${icon.name}`)} />

                                                <span>{icon.title}</span>
                                            </div>
                                        )
                                    })}
                                </div>
                            </Fragment>
                        )
                    })}
                </div>
            }
        </>
    )
}

export default TabContent


