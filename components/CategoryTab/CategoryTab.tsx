//@ts-nocheck

import React, { Component } from 'react';
import { data } from './data';
import DesktopTabView from './DesktopTabView';
import TabContent from './TabContent';

export default class CategoryTab extends Component {
    constructor(props) {
        super(props)  
    }
    state = {
        activeTab: 'General',
        content: null
    }

    setTab = (id) => {
        if(this.state.activeTab !== null && this.state.activeTab !== id){
            this.setState({ activeTab: id })
        } 
    }

    componentDidMount() {
        let content = data.find(e=>e.id=== this.state.activeTab);
        if(content != null) {
           this.setState({
                content: content
           }); 
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if(prevState.activeTab !== this.state.activeTab && this.state.activeTab !==null) {
            let content = data.find(e=>e.id===this.state.activeTab);
            this.setState({
                content: content
            });
        }  
    }

    render() {
        let tabContent = <TabContent tabcontent={this.state.content} />        
        return(
            <>
                <DesktopTabView
                    setTab={this.setTab} 
                    activeTab={this.state.activeTab} />
                    {tabContent}
            </>
        )
    }
}
