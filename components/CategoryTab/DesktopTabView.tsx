//@ts-nocheck


import React from 'react';
import style from './styles.module.scss';


const DesktopTabView = (props) => {
    return(
        <div className={`${style.TabTitle}`}>
            <a onClick={()=>props.setTab("General")} className={`${props.activeTab === "General" ? style.active : ''}`}>General</a>
            <a onClick={()=>props.setTab("Clothing")} className={`${props.activeTab === "Clothing" ? style.active : ''}`}>Clothing</a>
            <a onClick={()=>props.setTab("Utilities")} className={`${props.activeTab === "Utilities" ? style.active : ''}`}>Utilities</a>
        </div>
    )
}
export default DesktopTabView 


