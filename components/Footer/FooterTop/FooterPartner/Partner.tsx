import { myLoader } from 'config/image-loader';
import Image from 'next/image';
import React from 'react';
import classes from './partner.module.module.scss';

const Partner = (props) => {
    return (
        <div className={`col-sm-12 col-md-4 col-lg-2 ${classes.Partner}`} >
            {/*<LazyLoad height={300} offset={300}>
            <ImageLoader src={props.imgSrc} alt={props.alt} className={props.class}/>*/}
            <Image className="opacity-100" loader={myLoader} src={props.imgSrc} layout="responsive" width={300} height={300} />

        </div>
    )
}

export default Partner;