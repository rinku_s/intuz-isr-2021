import React from 'react';
import Partner from './Partner';
import styles from './partner.module.module.scss';

const FooterPartner = (props) => {
    return (
        <div className={`row ${styles.PartnerBlock}`}>
            <Partner 
                imgSrc={"/static/Images/Footer/ic_iso2x.png"}
                alt="ISO Certified Company"
                title="ISO Certified Company"
                
                />
            <Partner 
                imgSrc={"/static/Images/Footer/aws.png"}
                alt="Amazon Web Services"
                title="Amazon Web Services"
                
                />
            <Partner 
                imgSrc={"/static/Images/Footer/ic_microsoft2x.png"}
                alt="Microsoft Silver Certified"
                title="Microsoft Silver Certified"
                
                />
            <Partner 
                imgSrc={"/static/Images/Footer/spriteimg01.png"}
                alt="iOS Developers"
                title="iOS Developers"
                
                />
            <Partner 
                imgSrc={"/static/Images/Footer/spriteimg02.png"}
                alt="Android Developers"
                title="Android Developers"
                />
        </div>
    )
}

export default FooterPartner;
