import Link from 'next/link';
import React from 'react';
const List = (props) => {
    return (
        <>
            {props.list.map((item, i) => (
                <li key={i}>
                    <Link href={item.link} prefetch={false}>
                        <a>{item.title}</a>
                    </Link>

                </li>
            ))}
        </>
    )
}

export default List;