import React, { Component } from 'react';
import Container from '../Container/Container';
import styles from './footer.module.module.scss';
import FooterBottom from './FooterBottom/FooterBottom';
import FooterTop from './FooterTop/FooterTop';

class Footer extends Component {
    render() {
        return (
            <footer className={styles.Footer}>
                 <Container>
                    <FooterTop />
                    <FooterBottom />
                 </Container>
            </footer>
        )
    }
}

export default Footer;