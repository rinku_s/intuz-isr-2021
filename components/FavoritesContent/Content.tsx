//@ts-nocheck


import gsap from 'gsap';
import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';
import windowWidth from '../../hooks/windowWidth';
import style from './styles.module.scss';

const Content = (props) => {
    let windowSize = windowWidth();

    // setOpacity('.favContentLeft, .favContentRight');

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(`.favContentLeft`, 1, { x: -20, opacity: 0, ease: "Power0.ease" }, { x: 0, opacity: 1, ease: "Power0.ease" }, 0.1)
            .fromTo(`.favContentRight`, 1, { x: 20, opacity: 0, ease: "Power0.ease" }, { x: 0, opacity: 1, ease: "Power0.ease" }, 0.1);
    }

    return (
        <div className={style.ContentBlock}>

            <div className='row favContentLeft'>
                <LazyLoad height={300} offset={300} once>
                    <div className={`col-md-6 ${style.contentText}`}>
                        <img src={cdn('favc-categories-icon.png')} alt='favc-categories-icon' />
                        <h4><span>Units</span></h4>
                        <p>Mark your most important units as favorite from conversion screen and use it anytime for faster conversion.</p>
                    </div>
                </LazyLoad>
                <LazyLoad height={300} offset={300} once>
                    <div className={`col-md-6 ${style.contentText}`}>
                        <img src={cdn('favc-units-icon.png')} alt='favc-units-icon' />
                        <h4><span>Categories</span></h4>
                        <p>Tap on category icon to add into favorite list and get fastest access to all yours favorite categories.</p>
                    </div>
                </LazyLoad>
            </div>

            <div className='favContentRight'>
                <LazyLoad height={300} offset={300} once>
                    <img src={cdn(`favorites-utilities-img.png?auto=format&w=${windowSize < 768 ? '767' : '640'}`)} alt='favorites-utilities-img' />
                </LazyLoad>
            </div>
        </div >
    )
}

export default Content