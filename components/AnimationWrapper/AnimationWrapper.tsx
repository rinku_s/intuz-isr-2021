//@ts-nocheck
import React, { useState } from 'react';
import VisibilitySensor from 'react-visibility-sensor';
const VisibilityWrapper = (props) => {
    
    const [visible, setVisible] = useState(false)

    function setVisiblity(visibility){
        if(visibility){
            setVisible(true);
        }
    }
    return (
        <VisibilitySensor partialVisibility offset={{ bottom: 200}} onChange={setVisiblity}>
            {props.children(visible)}
        </VisibilitySensor>
    
    )
}

export default VisibilityWrapper
