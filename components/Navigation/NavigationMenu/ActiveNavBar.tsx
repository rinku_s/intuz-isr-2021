import Container from "components/Container/Container";
import Link from "next/link";
import { useRouter } from "next/router";
import React from "react";
import { Menuitem } from "./NavMenu";
import classes from "./styles.module.scss";
const ActiveNavBar: React.FC<{
  show?: boolean;
  onMouseEnter?: () => void;
  onMouseExit?: () => void;
  subLinks: Menuitem["submenu"];
}> = (props) => {
  const router = useRouter();
  return (
    <div
      className={
        "hidden fixed bg-header bg-opacity-70 w-full md:block py-3 px-8"
      }
      style={{ top: "8.5rem", left: "0", overflow: "hidden" }}
    >
      <Container className="flex items-center justify-center">
        {props.subLinks?.map((c) => (
          <Link href={c.link} prefetch={false}>
            <a
              className={`text-center block ${classes.link} px-20 ${
                router.asPath === c.link ? "text-brand-secondary" : "text-white"
              }`}
              style={{ textDecoration: "none" }}
            >
              <span className="capitalize mt-3 mb-3 text-2xl inline-block">
                {c.label}
              </span>
            </a>
          </Link>
        ))}
      </Container>
    </div>
  );
};

export default ActiveNavBar;
