import Router from 'next/router';
import React from 'react';
import Scrollbars from 'react-custom-scrollbars';
import SocialLink from '../../SocialLinks/SocialLink';
import classes from './styles.module.scss';
const SideBar = (props) => {

    function renderThumb({ style, ...props }) {
        const thumbStyle = {
            backgroundColor: `rgba(0,0,0,0.01)`,
            width: "2px",
            paddingRight: 0
        };
        return (
            <div
                style={{ ...style, ...thumbStyle }}
                {...props} />
        );
    }

    return (
        <div className={`${classes.SideBar} ${props.activeFilter !== null ? classes.active : ''}`}>
            <Scrollbars
                renderThumbHorizontal={renderThumb}
                renderThumbVertical={renderThumb}
            >
                <div className={classes.MenuLogo} onClick={() => Router.push("/")} />
                <ul className={classes.Menu}>
                    <li onClick={() => props.setFilter("mobile")} className={props.activeFilter === "mobile" ? classes.activeTag : ''}>
                        <div dangerouslySetInnerHTML={{ __html: "/static/Images/sidebaricon/ic_mobile.svg?include" }} />
                        <a>APP DESIGN & ENGINNERING</a>
                    </li>
                    <li onClick={() => props.setFilter("cloud")} className={props.activeFilter === "cloud" ? classes.activeTag : ''}>
                        <div dangerouslySetInnerHTML={{ __html: "/static/Images/sidebaricon/dashicons-cloud.svg?include" }} />
                        <a>CLOUD SOLUTIONS</a>
                    </li>
                    <li onClick={() => props.setFilter("web")} className={props.activeFilter === "web" ? classes.activeTag : ''}>
                        <div dangerouslySetInnerHTML={{ __html: "/static/Images/sidebaricon/web.svg?include" }} />
                        <a>WEB APPLICATIONS</a>
                    </li>
                    <li onClick={() => props.setFilter("ai")} className={props.activeFilter === "ai" ? classes.activeTag : ''}>
                        <div dangerouslySetInnerHTML={{ __html: "/static/Images/sidebaricon/ai.svg?include" }} />
                        <a>AI BASED APPLICATIONS</a>
                    </li>
                    <li onClick={() => props.setFilter("blockchain")} className={props.activeFilter === "blockchain" ? classes.activeTag : ''}>
                        <div dangerouslySetInnerHTML={{ __html: "/static/Images/sidebaricon/blockchain.svg?include" }} />
                        <a>BLOCKCHAIN DEVELOPMENT</a>
                    </li>
                </ul>
                <div className={classes.Seperator} />

                <ul className={classes.Links}>
                    <li><a>PARTNER WITH US</a></li>
                    <li><a>OUR ACCREDITATIONS</a></li>
                    <li><a>HIRE DEVELOPERS</a></li>
                    <li><a>PROCESS</a></li>
                    <li><a>LABS</a></li>
                    <li><a>WHY US</a></li>
                    <li><a>ABOUT</a></li>
                    <li><a>CONTACT US</a></li>
                </ul>

                <SocialLink className={classes.socialLinks} />
                <p>© 2019 Intuz. All Rights Reserved.</p>
            </Scrollbars>
        </div>
    )
}

export default SideBar;
