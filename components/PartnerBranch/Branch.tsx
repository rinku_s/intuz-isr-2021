//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import PrimaryHeading from '../Heading/Heading';
import SecondaryHeading from '../Heading/SecondaryHeading';

const Branch = () => {

    // setOpacity(`.FirstHeading, .SecondHeading`);
    function play() {
        const tl = gsap.timeline();
        tl.fromTo('.FirstHeading', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5)
            .fromTo('.SecondHeading', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5);
        tl.play();
    }

    return (
        <div className='text-center'>
            {/*  */}
            <SecondaryHeading className="FirstHeading">Our partnerships with agencies and consultants span across</SecondaryHeading>
            {/*  */}
            <PrimaryHeading className="SecondHeading">USA, UK, Australia, India, Singapore, and more.</PrimaryHeading>
        </div>
    )
}

export default Branch