//@ts-nocheck
import React from 'react'
import { cdn } from '../../config/cdn'
import classes from './styles.module.scss'
import HtmlParser from 'html-react-parser'

const data = [
    {
        icon: cdn('iot_process1_694da6af93.png'),
        description: 'Evaluate technical capabilities, industrial credibility and development process',
        heading:"Identifying the right <strong>IoT Development</strong> Partner"
    },
    {
        icon: cdn('iot_process2_741a44f7f7.png'),
        description: 'Ideate, design and plan IoT solution architecture ',
        heading:"User Centric Design <strong>(PoC)</strong>"
    },
    {
        icon: cdn('iot_process3_d2a55a6bd7.png'),
        description: 'IoT gateway and Cloud, sensor network, Hardware – infrastructure backbone exploration ',
        heading:"Identifying Hardware and <strong>Technology Stack</strong>"
    },
    {
        icon: cdn('iot_process4_2b9fd3db30.png'),
        description: 'Design and develop mobile app, web interface, and firmware',
        heading:"Application <strong>Software & Firmware</strong> Development"
    },
    {
        icon: cdn('iot_process5_860e3e7817.png'),
        description: 'Build modules, drafts, testing it and deployment',
        heading:"Prototype Testing and <strong>Deployment</strong>"
    },
    {
        icon: cdn('iot_process6_94e092d790.png'),
        description: 'Final Development, Launch and post support to clients',
        heading:"Manufacturing and <strong>Support</strong>"
    },
]



const IotRoadMapComponent = () => {
    return (
        <ul className={`${classes.IotRoadMapComponent}`}>
            {data.map((d,i)=>(
                <li key={i}>
                    <div>
                    <img src={d.icon} alt={d.heading}/>
                    <h5>{HtmlParser(`${d.heading}`)}</h5>
                    <p>{d.description}</p>
                    </div>
                </li>
            ))}
        </ul>
    )
}

export default IotRoadMapComponent
