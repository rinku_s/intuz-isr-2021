//@ts-nocheck

import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';
import style from './styles.module.scss';

const Content = (props) => {
    return (
        <div className={`col-md-6 ${style.imgBlock}`}>
            <LazyLoad height={300} offset={300} once>
                <img src={cdn(props.content.icon.name)} alt={props.content.icon.name} />
            </LazyLoad>
            <div className={style.text}>
                {props.content.title ? <h4>{props.content.title}</h4> : ''}
                {props.content.description ? <p>{props.content.description}</p> : ''}
                {props.content.link ? <a href={props.content.link}>More</a> : ''}
            </div>
        </div>
    )
}

export default Content