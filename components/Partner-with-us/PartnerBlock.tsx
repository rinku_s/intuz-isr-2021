//@ts-nocheck

import React from 'react';
import Description from './Description';
import Model from './Model';
import style from './styles.module.scss';

const PartnerBlock = () => {
    return (
        <div className={style.PartnerBlock}>
            <Description />
            <Model />
        </div>
    )
}

export default PartnerBlock;