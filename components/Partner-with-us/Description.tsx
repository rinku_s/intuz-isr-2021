//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import style from './styles.module.scss';

const Description = () => {
    // setOpacity(`.Heading, .LeftBlock`);

    function play() {
        const tl = gsap.timeline();
        tl.fromTo('.Heading', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5)
            .fromTo('.LeftBlock', 1, { x: -50, opacity: 0, ease: "power0.ease" }, { x: 0, opacity: 1, ease: "power0.ease" }, 0.5);
        tl.play();
    }

    return (
        <div className={style.DescBlock}>

            <h2 className="Heading">Partner Speak</h2>

            <div className="LeftBlock">
                <img src={"/static/Images/partner/quote_img.png"} alt="quote_img.png" />
                <p>We have found Intuz a great asset for our business. Their high standards and work ethic have made projects a breeze and allowed us to focus on our core business knowing that we have the support we need. They are knowledgeable in the latest digital technologies which gives me confidence that they are the right partner for the long term</p>
                <img src={"/static/Images/partner/user1.png"} alt="user1.png" className={style.LogoImage} />
                <span>Alex B. – Head of Digital</span>
                <span>Leading Digital Agency, London</span>
            </div>
        </div >
    )
}

export default Description;