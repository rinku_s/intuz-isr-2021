//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import style from './styles.module.scss';

const Model = () => {
    // setOpacity(`.ModelHeading, .RightBlock`);
    function play() {
        const tl = gsap.timeline();
        tl.fromTo('.ModelHeading', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5)
            .fromTo('.RightBlock', 1, { x: 50, opacity: 0, ease: "power0.ease" }, { x: 0, opacity: 1, ease: "power0.ease" }, 0.5);
        tl.play();
    }

    return (
        <div className={style.ModelBlock}>

            <h2 className="ModelHeading">Engagement Model</h2>

            <ul className="RightBlock">
                <li>
                    <img src={"/static/Images/partner/circle.png"} alt="circle.png" />
                    Project Outsourcing
                </li>
                <li>
                    <img src={"/static/Images/partner/circle.png"} alt="circle.png" />
                    Dedicated Resources
                </li>
                <li>
                    <img src={"/static/Images/partner/circle.png"} alt="circle.png" />
                    Time & Material
                </li>
            </ul>
        </div >
    )
}

export default Model;