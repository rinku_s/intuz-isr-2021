//@ts-nocheck


import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';

const Layers = (props) => {
    return(
        <ul>
            {
                props.content.map((content) => 
                    <li key={content.id}> 
                        <LazyLoad height={300} offset={300}>
                            <img src={cdn(content.icon.name + '?auto=format')}  alt={content.icon.name} />
                        </LazyLoad>
                        {content.name}
                    </li>
                )
            }
        </ul>
    )
}

export default Layers