//@ts-nocheck

import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';
import Layers from './Layers';
import style from './styles.module.scss';

const CommunicationLayers = (props) => {
    return (
        <div className={`${style.CommunicationLayer}`}>
            {
                props.content && props.content.map((content) => 
                    <div key={content.id}>
                        <LazyLoad height={300} offset={300}>
                            <img src={cdn(`${content.icon.name}?auto=format`)} alt={content.icon.name} />
                        </LazyLoad>
                        <h3>{content.name}</h3>
                        {content.nameInfo && <span>{content.nameInfo}</span> }
                        {content.layers.length > 0 && <Layers content = {content.layers} />}
                    </div>
                )
            }
        </div>
    )
}

export default CommunicationLayers