//@ts-nocheck
//@ts-nocheck
import React, { Component } from "react";
import { getCareersData } from "../../lib/api";
//import Title from './Title';
import style from "./style.module.scss";
export default class Accordian extends Component {
  // constructor(props) {
  //     super(props)
  // }
  state = {
    activeTab: null,
    content: this.props.data,
  };
  componentDidMount = async () => {
    let data = await getCareersData();
    this.setState({
      content: data.careers,
    });
  };
  render() {
    //let content = <AccordianContent content={this.state.content} />
    return (
      <>
        <div className={style.TabHeading}>
          {this.state.content.map(
            (tab, index) =>
              tab.ready && (
                <div className={`${style.Heading}`} key={index}>
                  <h4>{tab.position_name}</h4>
                  <a
                    target="_blank"
                    rel="noreferrer"
                    href={tab.apply_link}
                    className={style.btnApply}
                  >
                    Apply Now
                  </a>
                </div>
              )
          )}
        </div>
        {/* <Title 
                    setTab={this.setTab} 
                    activeTab={this.state.activeTab} 
                    titles = {data}
                >
                    {content}
                </Title> */}
      </>
    );
  }
}
