//@ts-nocheck


export const data = [
    {
        "index": 1,
        "id":"PPC Executive",
        "apply_link":"https://intuz.kekahire.com/JobDetails/2130"
    },
    {
        "index": 2,
        "id":"Android Developer",
        "apply_link":"https://intuz.kekahire.com/JobDetails/2157"
    },
    {
        "index": 3,
        "id":"Marketing Manager",
        "apply_link":"https://intuz.kekahire.com/JobDetails/2319"
    },
    {
        "index": 4,
        "id":"React Native Developer",
        "apply_link":"https://intuz.kekahire.com/jobdetails/2311"
    },
    {
        "index": 5,
        "id":"MERN Stack / Full Stack Developer",
        "apply_link":"https://intuz.kekahire.com/JobDetails/2065"
    },
    {
        "index": 6,
        "id":"Sr. System Administrator",
        "apply_link":"https://intuz.kekahire.com/JobDetails/2075"
    },
    {
        "index": 7,
        "id":"Sr. Python Developer",
        "apply_link":" https://intuz.kekahire.com/JobDetails/2734"
    },
]




// export const data = [
//     {
//         "index": 1,
//         "id":"Sr. QA Engineer",
//         "content":[
//             {
//                 "qualification":"B.E./MCA/Msc.IT(Preferable) or Any other Bachelor degree",
//                 "opening" : 2,
//                 "location": "Ahmedabad",
//                 "salary" : "Best in Industry",
//                 "requirement" : "<li>Should have expertise in Manual Testing</li><li>Must have experience in&nbsp;<strong>Mobile Application Testing&nbsp;</strong>&nbsp;</li><li>Should have experience in App Simulator testing</li><li>Must have expertise in Functional Testing and UI/UX Testing</li><li>Must have experience in testing the Porting of the application on different Mobile Handset &amp; Platforms.</li><li>Should have expertise in Validation and Verification testing.</li><li>Should have a good experience in review of Functional Non-Functional Requirements, Bugs and Improvement.</li><li>Should have experience in Smoke Testing, Black Box Testing, Performance Testing, Security Testing, Regression Testing, Integration Testing &amp; Database Testing</li><li>Knowledge required about Sql database for Database Testing</li><li>Experience in automation tool, various mobile operating systems and Web will be an advantage.</li>",
//                 "responsibility" :  "<li>Need to manage plan for SRS/Implementation and demo phase.</li><li>Need to manage plan for Testing as per Severity and Priority.</li><li>Responsible for Training/Demo to Client.</li><li>Responsible for Defect Tracking and Reporting.</li><li>Responsible for Writing, Updating and Executing Test Cases.</li>",
//             },
//         ]
//     },
//     {
//         "index": 2,
//         "id":"SEO Executive",
//         "content":[
//             {
//                 "requirement" : "<li>1+ Years experience with good understanding of digital marketing and latest SEO concepts/trends i.e. Google updates, technical aspects, on-site, off-site aspects, creative and analytics.</li><li>Good knowledge of SEO Tools such as Google Search Console, Screaming Frog, DeepCrawl, SEMrush, Majestic and Ahrefs.</li><li>High level of competence using Analytics platforms including Google Analytics, Statcounter etc.</li><li>Basic knowledge of HTML, CSS and/or Javascript is a plus.</li><li>Developing end-to-end SEO strategy that have a demonstrable impact on our organic performance.</li><li>Meticulous attention to detail with very strong spelling and grammar skills.</li>"
//             },
//         ]
//     },
//     {
//         "index": 3,
//         "id":"React.JS Developer",
//         "content":[
//             {
//                 "qualification":"B.E./MCA/Msc.IT(Preferable) or Any other Bachelor degree",
//                 "responsibility" :  "<li>Developing new user-facing features using React.js</li><li>Building reusable components and front-end libraries for future use</li><li>Translating designs and wireframes into high quality code</li><li>Optimizing components for maximum performance across a vast array of web-capable devices and browsers</li>",
//                 "description" : "We are looking for a great JavaScript developer who is proficient with React.js. Your primary focus will be on developing user interface components and implementing them following well-known React.js workflows (such as Flux or Redux). You will ensure that these components and the overall application are robust and easy to maintain. You will coordinate with the rest of the team working on different layers of the infrastructure. Therefore, a commitment to collaborative problem solving, sophisticated design, and quality product is important."
//             },
//         ]
//     },
//     {
//         "index": 4,
//         "id":"Mean Stack Developer",
//         "content":[
//             {
//                 "qualification":"B.E./MCA/Msc.IT(Preferable) or Any other Bachelor degree",
//                 "requirement" : "<li>Experience of 1+ years&nbsp;developing using MEAN Stack&nbsp;</li><li>Strong development experience using Node.JS, Express, Angularjs/React.JS&nbsp;and Mongodb</li><li>Experience in UX, CSS, LESS/SASS, Git, Gulp</li><li>Write clean, well-designed code.</li><li>Self-motivated, able to work proficiently both independently and in a team.</li><li>Cloud deployment experience is preferred, working with Google, AWS, Jenkins, Docker</li><li>Amazon Web Services</li><li>Understanding of ES6 and ES7 features</li><li>Unit Testing, E2E, selenium webdriver</li>",
//             },
//         ]
//     },
//     {
//         "index": 5,
//         "id":"Front End Developer",
//         "content":[
//             {
//                 "requirement" : "<li>Strong Javascript development skill with in-depth knowledge of concepts like asynchronous programming, closures, types, and ES6 along with a strong understanding of JQuery, CSS and HTML is a must.</li><li>Having hands-on experience working with any JavaScript based front end development framework such as AngularJS, ReactJS, Vue.js, etc. (Having hands-on experience on ReactJS is a huge plus)</li><li>Strong knowledge of Web Technologies including HTTP protocol, XML, JSON, REST is required.</li><li>Experience with LESS and SASS compilers.</li><li>Good understanding of asynchronous request handling, partial page updates, and AJAX.</li><li>Experience with Git version control.</li><li>Experience with responsive and cross-browser testing techniques.</li><li>Basic understanding of SEO principles, website performance and ensuring that the website will adhere to them.</li><li>A sharp eye for detail with a need to fix misalignments.</li>"
//             },
//         ]
//     },
//     {
//         "index": 6,
//         "id":"Project Manager",
//         "content":[
//             {
//                 "qualification":"MSC-IT or MCA or BE-IT / CE & preferably Certified in PMP or other equivalent certification in Project Management",
//                 "opening" : 1,
//                 "experience" : "5+ Years",
//                 "requirement" : "<li>Should have minimum 5+ yrs. experience in End to End Project Life Cycle =&gt; Analysing the Requirement =&gt; Project Planning =&gt; Execution =&gt; Deployment with Excellent Client Service.</li><li>Responsible for complete Project Life Cycle right from the Receiving &amp; understanding Requirement - Client Servicing - Project Planning &amp; Execution.</li><li>Responsible for Project Deployment with gained Client Satisfaction - Team Development &amp; Motivation.</li><li>To work closely with Top management officials, Pre-sales &amp; Development team to establish needs &amp; specify development of work.</li>",
//                 "responsibility" :  "<li>Need to manage plan for SRS/Implementation and demo phase.</li><li>Need to manage plan for Testing as per Severity and Priority.</li><li>Responsible for Training/Demo to Client.</li><li>Responsible for Defect Tracking and Reporting.</li><li>Responsible for Writing, Updating and Executing Test Cases.</li>",
//             },
//         ]
//     },
//     {
//         "index": 7,
//         "id":"Business Analyst",
//         "content":[
//             {
//                 "qualification":"MSC-IT or MCA or BE-IT / CE & MBA - IT / Marketing",
//                 "opening" : 1,
//                 "location": "Ahmedabad",
//                 "requirement" : "<li>Must have Business Analysis experience in Mobile App &amp;&nbsp;<strong>Web development</strong>.</li><li>Should have expertise in document preparation for Business Requirements, Functional requirement, Diagrams - (Process Flow, Context Diagram, Flow Charts), System Requirements Specification, User Interface Documents &amp; Wireframes, office tools&nbsp; (Word / Excel / Power-point).</li><li>Experience across all phases of SDLC (Requirement Gathering, Design, Coding, Testing, Implementation, Maintenance), especially Implementation</li><li>Technical inclination and knowledge – API integration etc.</li><li>Able to prepare analysis based on gathered requirement.</li><li>Able to prepare various reports i.e. Gap Analysis, Work Flow analysis etc. and based on that able to design development process.</li><li>Should have excellent Communication Skills (Both Written &amp; Verbal).</li><li><u><strong>Hands on prior experience in back end development/Web Development.</strong></u></li>",
//                 "responsibility" :  "<li>A self-starter that has the ability to lead requirements gathering sessions with BD Team.</li><li>Documenting the requirements in structured format.</li><li>Work closely with the Development and Testing Teams on complex system changes.</li><li>Prepare visual diagrams &amp; wire-frames for ease of understanding.</li><li>Analyze complex processes, interfaces and sub-system problems for modification.</li><li>Write business requirements for complex projects, including the definition, scope, objectives, and defining the approach.</li><li>Define and document detailed, functional business requirements, including system scope and objectives for complex projects.</li><li>Liaise between customer and technical team for technical discussions.</li>"
//             },
//         ]
//     },
//     {
//         "index": 8,
//         "id":"Sr. Android Application Developer",
//         "content":[
//             {
//                 "qualification":"MSC-IT or MCA or BE-IT / CE",
//                 "opening" : 2,
//                 "experience" : "4+ Years",
//                 "requirement" : "<li>To develop applications for Android Mobile and Tablet devices.</li><li>Willing to deliver cutting edge design solutions to global clients.</li><li>To use Android SDK to develop, test and deliver the applications.</li><li>To work with project leads &amp; ensure timely milestone deliveries.</li>",
//                 "responsibility" :  "<li>Should have extensive knowledge of Android OS and good understanding of Object Oriented Programming (OOPS) and Core Java.</li><li>Should be well-versed use of Parsing [XML, JSON &amp; REST API], Social media integration, Photo gallery and common features to communicate with hardware of Android device (like Camera, Sound, Audio/Video Player, etc).</li><li>Should be experienced in In-app Billing, GCM (Push Notification), Animation, Google Maps and Database, Fragment, Interface, Implements, Singleton Class, Design Patterns [OOAD], Basic Knowledge of NDK, XMPP, Augmented Reality apps, Gestures.</li><li>Basic knowledge of Open GL is preferable.</li><li>Ability to execute the project independently.</li><li>Experience with adding accessibility to end user applications.</li><li>Sound thinker with the ability to demystify complex technical requirements.</li><li>A self-starter that relies on experience and judgment to plan and accomplish individual and team goals &amp; must be a fast learner.</li>",
//                 "tools": "<li>Java.</li><li>Framework - Android SDK 4.1 and Above (at least up to Android Nought).</li><li>Operating System -&nbsp;Windows 7, Linux.</li><li>Tools -&nbsp;Android Studio.</li><li>Database - SQLite.</li>"
//             },
//         ]
//     },
//     {
//         "index": 9,
//         "id":"Sr. IOS Application Developer",
//         "content":[
//             {
//                 "qualification":"B.E./MCA/Msc.IT(Preferable) or Any other Bachelor degree",
//                 "opening" : 2,
//                 "location": "Ahmedabad",
//                 "salary" : "Best in Industry",
//                 "requirement" : "<li>To develop applications for iOS Mobile devices.</li><li>Willing to deliver cutting edge design solutions to global clients.</li><li>Need to work with project leads & ensure timely milestone deliveries.</li>",
//                 "responsibility" :  "<li>Should have Proven working experience in iPhone application development.</li><li>Experience with development of iPad application will be added advantage.</li><li>Intense knowledge of C/C++, OOPS, Objective-C , REST, XML, JSON, SQLite, Xcode, SWIFT.</li><li>Should have knowledge of Mac OS X Yosemite.</li><li>Must have worked on in-app purchase &amp; Push Notification.</li><li>Good to have experience working with Animation, Gestures &amp; Map SDK.</li><li>Ability to execute the project independently.</li><li>A self-starter that relies on experience and judgment to plan and accomplish individual and team goals &amp; must be a fast learner.</li><li>Should have developed more then 5 plus native apps.</li>",
//                 "tools" : "<li>Objective C</li><li>Tools:&nbsp;Xcode 6.1 and higher, Interface Builder, iPhone SDK 7.0 and higher</li><li>Database: SQL Lite</li>"
//             },
//         ]
//     },
//     {
//         "index": 10,
//         "id":"React Native Developer",
//         "content":[
//             {
//                 "qualification":"MSC-IT or MCA or BE-IT / CE",
//                 "opening" : 3,
//                 "location": "Ahmedabad",
//                 "experience": "2 Years+",
//                 "requirement" : "<li>To develop applications using React Native.</li><li>Willing to deliver cutting edge solutions for global clients.</li><li>Need to work with project leads &amp; ensure timely milestone deliveries.</li>",
//                 "responsibility" :  "<li>Should have proven working experience in React Native development.</li><li>Should have developed 2+ apps independently in React Native.</li><li>Should have strong understanding of Javascript</li><li>Should quickly adapt to emerging technologies.</li><li>Ability to execute the project independently.</li><li>A self-starter that relies on experience and judgment to plan and accomplish individual and team goals &amp; must be a fast learner.</li>",
//             },
//         ]
//     },
// ]