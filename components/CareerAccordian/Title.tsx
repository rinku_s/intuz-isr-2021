//@ts-nocheck


import React, { Fragment } from 'react';
import style from './style.module.scss';

const Title = (props) => {
    return (
        <div className={style.TabHeading}>
            {props.titles.map((tab, i) => {
                return (
                    <Fragment key={i}>
                        <div onClick={() => props.setTab(tab.id)} className={`${style.Heading}`}>
                            <h4>{tab.id}</h4>
                            <span>{props.activeTab == tab.id ? <img src={"/static/Images/icons/minus.svg')"} alt="open_accordian" /> : <img src={require('/static/Images/icons/plus.svg')} alt="close_accordian" />}</span>
                        </div>
                        {props.activeTab == tab.id ? props.children : ''}
                    </Fragment>
                )
            })}
        </div>
    )
}
export default Title


