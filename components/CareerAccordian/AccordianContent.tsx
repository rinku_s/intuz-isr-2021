//@ts-nocheck

import React, { useState, Fragment } from 'react';
import { Modal } from 'react-bootstrap';
import FormFields from '../JobApplyForm/FormFields';
import FormButton from '../UI/FormButton/FormButton';
import style from './style.module.scss';


const AccordianContent = (props) => {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    const position = ['Select Position', 'Android Application Developer', 'Business Analyst', 'Creative Designer - Mobile App', 'Digital Marketing Expert + PPC', 'iPhone (iOS) Application Developer', 'Fresher', 'Project Manager', 'Sr. Web Designer', 'Sr. Business Development Manager', 'Software Engineer - Asp.Net', 'SEO Executive', 'Sr. Developer - PHP', 'Business Development Executive', 'Marketing Manager', 'Management Trainee - Digital Marketing', 'Developer / Jr. Developer (Cloud)', 'Creative Designer', 'Frontend-Javascript Developer', 'Sr. Android Application Developer', 'Jr. System Administrator', 'Content Writer', 'Quality Analyst', 'Web Designer', 'Digital Marketing Executive - Paid Marketing', 'HTML + Creative Designer', 'Javascript/Front End Developer', 'Management Trainee - Sales', 'WordPress Developer', 'Full Stack Developer (Node.JS+React/Angular)', 'Account Executive', 'Accounts & Admin Support Executive', 'Java Developer', 'Sr. Mean Stack Developer', 'UI/UX Designer', 'HR Manager', 'NodeJS Developer', 'Cloud Developer', 'Xamarin/.Net Developer', 'HR & Admin Coordinator', '3D Animator', 'Analyst Programmer', 'Asp.net Developer', 'AWS Developer', 'Blackberry Developer', 'CCE Developer', 'Database Administrator', 'Division Manager', 'Flash Action Script Expert', 'Jr. Marketing Executive', 'Link Building Expert', 'Product Development Executive', 'Programmer', 'Project Coordinator', 'QA Tester', 'Software Trainee', 'Senior Technical Manager', 'Sr. QA Executive - Web and Mobile', 'Sr. Technical Recruiter/ Talent Acquisition Executive', 'Sr. IOS Application Developer', 'React Native Developer', 'Sr .NET Developer', 'Management Trainee - Production', 'Management Trainee - Project Coordinator', 'Business Development Manager', 'Intern - Project Management', 'Sr. QA Engineer', 'Technical Head/CTO', 'Project Coordinatior', 'React.JS Developer', 'Blockchain Developer', 'Mean Stack Developer', 'Front End Developer', 'other']
    
    let positionArray = [];
    position.map((name, index) => {
        positionArray.push({'value' : index, 'displayValue' : name})
    })
    return(
        <>
        <div className={`${style.Content} currentcontent`}>
            {props.content && props.content.content.map((tab, index) => {
                return(
                    <Fragment key={index}>
                        {tab.qualification ? <h3><span>Qualification :</span> {tab.qualification}</h3> : ''}
                        <div className={style.jobdescription} key={index}>
                            {tab.opening ? <p><strong>No. of opening:  </strong>{tab.opening}</p> : ''}
                            {tab.experience ? <p><strong>Experience: </strong>{tab.experience}</p>: ''}
                            {tab.location ? <p><strong>Location: </strong>{tab.location}</p> : ''}
                            {tab.salary ? <p><strong>Salary: </strong>{tab.salary}</p> : ''}
                            {tab.requirement ? 
                                <>
                                    <p><strong>Job Requirement:</strong></p>
                                    <ul dangerouslySetInnerHTML={{__html:tab.requirement}}></ul>
                                </>
                            : '' }
                            {tab.responsibility ?
                                <>
                                    <p><strong>Responsibility</strong></p>
                                    <ul dangerouslySetInnerHTML={{__html:tab.responsibility}}></ul>
                                </>
                            : ''}
                            {tab.tools ?
                                <>
                                    <p><strong>Language & Tools Knowledge: </strong></p>
                                    <ul dangerouslySetInnerHTML={{__html:tab.tools}}></ul>
                                </>
                            : ''
                            }
                             {tab.description ?
                                <>
                                    <p><strong>Description</strong></p>
                                    <p dangerouslySetInnerHTML={{__html:tab.description}}></p>
                                </>
                            : ''}
                            <a href="mailto:careers@intuz.com" className={style.btnApply}>Apply Now</a>
                        </div>
                    </Fragment>
                )
                
            })}
        </div>    
        <Modal show={show} onHide={handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>Job Application</Modal.Title>
            </Modal.Header>
            <Modal.Body>        
                <div>
                    <form className="row">
                        <FormFields selectValues = {positionArray} />  
                        <FormButton 
                            submitBtn='Send'
                            btnText='Clear'
                            variation = 'applicationform'
                        />
                    </form>
                </div>
            </Modal.Body>
        </Modal>
        <style>
            {`
                .close>span {
                    font-size: 35px;
                    height: 40px;
                    font-weight: 300;
                    border-radius: 100%;
                    border: 1px solid #000;
                    width: 40px;
                    text-align: center;
                    display: block;
                }
                .modal-dialog {

                    max-width: 850px !important;
                    @media(max-width:767px){
                        margin: 2rem;
                    }
                    @media(min-width: 576px) {
                        max-width: 550px !important;
                    }
                }
                .modal-header {
                    border-bottom:none;
                    padding: 3rem 3rem 0;
                }
                .modal-title {
                    font-size: 3rem;
                    font-weight: 600;
                    color: #3bebeb;
                }

                .modal-body{
                    padding: 3rem;
                }

                .modal-body .FormButton {
                    text-align: right !important;
                }
            `}
            </style>
         </>
    )
}
export default AccordianContent