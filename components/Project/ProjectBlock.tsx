//@ts-nocheck
//@ts-nocheck
import React from "react";
import { cdn } from '../../config/cdn';
import IconBlock from '../IconBlock/IconBlock';
import ImageBlock from "../ImageBlock/ImageBlock";
import style from './styles.module.scss';

const ProjectBlock = (props) => {

    const variation = props.variation;
    let imageFile = props.backImgUrl.hash + '.jpg?fm=pjpg&auto=format';
    var backImage = { background: `url(${cdn(imageFile)}) center center no-repeat`, }

    return (
        <div className={`col-md-12 col-lg-6 ${style.Content} ${style[variation]}`} style={backImage}>
            <div>
                <ImageBlock src={props.logo} alt={props.log} mwidth={100} dwidth={200} format="png" />
                {/* <img src={props.logo} className={style.Logo} /> */}
                <IconBlock icons={props.IconArray} />
            </div>
            <div className={`${style.TopImg}`}>
                <ImageBlock src={props.topImage} alt={props.topImage} mwidth={200} dwidth={400} format="png" />
            </div>
        </div>
    )
}

export default ProjectBlock;

