//@ts-nocheck

import React from "react";
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';
import ProjectBlock from "./ProjectBlock";
import style from './styles.module.scss';

const Project = (props) => {
   
    if(props.sectionrecentworks.length <= 0) {
        return null;
    } else {
        return(
            <LazyLoad offset={300}>
                <div className={`row ${style.RecentWorkBlock}`}>
                    {props.sectionrecentworks.map((section, index) => {
                        var IconArray = [];
                        if(section.firstIcon) {
                            IconArray.push({
                                src : cdn(section.firstIcon.name),
                                alt : section.firstIcon.name,
                                title : section.firstIcon.name,
                                variation : "circle_img",    
                            })
                        }
                        if(section.secondIcon) {
                            IconArray.push({
                                src : cdn(section.secondIcon.name),
                                alt : section.secondIcon.name,
                                title : section.secondIcon.name,
                                variation : "circle_img",    
                            })
                        }
                        return(
                            <ProjectBlock 
                                backImgUrl = {section.backImage}
                                logo = {cdn(section.topLeftImage.name)}
                                topImage = {cdn(section.topRightImage.name)}
                                IconArray = {IconArray}
                                variation = 'leftBlock'
                                key = {index}
                        />
                        )
                    })}                
                </div>
            </LazyLoad>
        )
    }
}

export default Project;

