//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss';
const CaseStudyFeatureMobile = ({ title, src }) => {
    return (
        <div className={classes.CaseStudyFeatureMobile}>
           <p>{title}</p>
           <img className="lazyload img-fluid" data-src={src} alt={title}/> 
        </div>
    )
}

export default CaseStudyFeatureMobile
