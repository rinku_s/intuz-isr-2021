//@ts-nocheck
import React, { useEffect, useState } from "react";
import style from "./styles.module.scss";
import SVG from "react-inlinesvg";
import { cdn } from "../../config/cdn";
import { Carousel } from "react-bootstrap";
import { MediaContextProvider, Media } from "config/responsiveQuery";
import CaseStudyFeatureMobile from "./CaseStudyFeatureMobile";
const CaseStudyFeature = ({ list }) => {
  const [index, setIndex] = useState(0);

  const [paused, setPaused] = useState(false);

  useEffect(() => {
    let interval = null;
    if (interval == null && window.innerWidth > 990) {
      interval = setInterval(() => {
        if (!paused) {
          if (index == list.length - 1) {
            setIndex(0);
          } else {
            setIndex(index + 1);
          }
        }
      }, 2000);
    }
    return () => {
      clearInterval(interval);
    };
  }, [index, paused]);

  function mouseOver(i) {
    setIndex(i);
    setPaused(true);
  }

  return (
    <MediaContextProvider>
      <Media greaterThanOrEqual="md">
        <div className={`${style.caseStudyFeature} block d-mflex`}>
          <div className={"align-self-stretch"}>
            <ul className={style.list}>
              {list.map((point, i) => (
                <li
                  key={i}
                  className={`inline-flex mb-5 ${style.listli} ${
                    index == i ? style.active : ""
                  }`}
                  onClick={() => setIndex(i)}
                  onMouseOver={() => mouseOver(i)}
                  onMouseOut={() => setPaused(false)}
                  onMouseLeave={() => setPaused(false)}
                >
                  <SVG src={cdn(point.icon.name)} width="30px" />
                  <p className={style.listtitle}>{point.title}</p>
                </li>
              ))}
            </ul>
          </div>
          <div className={`${style.imgBlock}`}>
            {/* <div> */}
            {list.map((l, i) => (
              <img
                key={i}
                data-src={cdn(l.image.name + "?auto=format,compress")}
                alt={l.title}
                className={`lazyload img-fluid ${
                  index == i ? style.imagein : ""
                }`}
              />
            ))}
            {/* </div> */}
          </div>
        </div>
      </Media>
      <Media lessThan="md">
        <Carousel
          fade={true}
          controls={false}
          indicators={false}
          interval={1000}
        >
          {list.map((l, i) => (
            <Carousel.Item key={i} className={`${style.newitemheight}`}>
              <CaseStudyFeatureMobile title={l.title} src={cdn(l.image.name)} />
            </Carousel.Item>
          ))}
        </Carousel>
      </Media>
    </MediaContextProvider>
  );
};

export default CaseStudyFeature;
