//@ts-nocheck


import Link from 'next/link';
import React from 'react';
import LazyLoad from 'react-lazyload';
import Moment from 'react-moment';
import style from './styles.module.scss';

const BlogAuthor = (props) => {
    if (props.link) {
        return (
            <div className={style.authorDetail}>
                { props.image &&
                    <Link as={`/blog/author/${props.link}`} href='/blog/author/[name]' prefetch={false}>
                        <a>
                            <LazyLoad height={300} offset={300}>
                                <img src={props.image} height='40px' width='40px' alt={props.image} />
                            </LazyLoad>
                        </a>
                    </Link>}
                <div>
                    <Link as={`/blog/author/${props.link}`} href='/blog/author/[name]' prefetch={false}>
                        <a>{props.name}</a>
                    </Link>
                    <span className={style.grey}><Moment format="MMM DD,YYYY">{props.date}</Moment></span>
                </div>
            </div>
        )
    } else {
        return (<div></div>);
    }
}

export default BlogAuthor