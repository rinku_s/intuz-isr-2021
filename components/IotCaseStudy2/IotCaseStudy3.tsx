//@ts-nocheck
import React from "react";
import PrimaryHeading from "../Heading/Heading";
import SecondaryHeading from "../Heading/SecondaryHeading";
import Container from "../Container/Container";
import classes from "./styles.module.scss";
import { cdn } from "../../config/cdn";
import VisibilitySensor from "react-visibility-sensor";
import SVG from "react-inlinesvg";
import LazyLoad from "react-lazyload";

const points = [
  "Complex Mobile Commerce App solution to accept card payments",
  "Well integration of credit card hardware swiper for Android and iOS apps",
  "Highly secure, PCI-DSS compliant App",
  "Super-secure payment process ",
];

const IotCaseStudy3 = (props) => {
  return (
    <div
      className={classes.IotCaseStudy2 + " cst"}
      data-scrollcolor={"#196588"}
    >
      <Container>
        <PrimaryHeading style={{ color: "#fff" }}>
          IoT enabled Enterprise Mobile Point-Of-Sale (POS) Solutions for
          Retails
        </PrimaryHeading>
        <SecondaryHeading style={{ color: "#fff" }}>
          Online Payments Made Easy – Enter Amount, Swipe, Confirm and Send
          Receipt!
        </SecondaryHeading>

        <div
          className={`flex flex-col md:flex-row-reverse ${classes.FlexContainer}`}
        >
          <div
            className={`${classes.Image} align-self-center`}
            style={{ flex: 0.8 }}
          >
            <LazyLoad offset={200}>
              <SVG className="img-fluid" src={cdn("iot_cs_2_6a7d9ca62c.svg")} />
            </LazyLoad>
          </div>
          <div className={`${classes.Content} mt-5 mt-md-0`}>
            <p className={classes.smallPara} style={{ color: "#fff" }}>
              Card Payments made easy anywhere, everywhere and at any time!
              Intuz has helped US based financial technology enterprise to
              efficiently accept card payment just in just three clicks by
              serving multi-folded, IoT product development solution i.e. Mobile
              POS. The company involved in research, development and
              implementation of efficient software and technology for North
              American fragmented target vertical markets.
            </p>
            <h4>
              Reliable, Fast and Secure IoT mobile POS to expand your business:{" "}
            </h4>
            <ul>
              {points.map((point, i) => (
                <li key={i}>{point}</li>
              ))}
            </ul>
          </div>
        </div>
      </Container>
    </div>
  );
};

export default IotCaseStudy3;
