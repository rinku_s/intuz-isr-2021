//@ts-nocheck
import React from "react";
import PrimaryHeading from "../Heading/Heading";
import SecondaryHeading from "../Heading/SecondaryHeading";
import Container from "../Container/Container";
import classes from "./styles.module.scss";
import { cdn } from "../../config/cdn";
import VisibilitySensor from "react-visibility-sensor";
import SVG from "react-inlinesvg";
import LazyLoad from "react-lazyload";

const points = [
  "iBeacon (Estimode) integration for Bluetooth connectivity",
  "Digital keys to unlock the room in one click with their smart phones",
  "Automatic check-ins and room allocation to the guests on arrival ",
  "Bluetooth enabled door locking system ",
  "External proprietary Hardware integration with the app for seamless functionality ",
  "Up-sell your hotel services via app ",
];

const IotCaseStudy2 = (props) => {
  return (
    <div
      className={classes.IotCaseStudy2 + " cst"}
      data-scrollcolor={"#B89D80"}
    >
      <Container>
        <PrimaryHeading style={{ color: "#fff" }}>
          Cutting-edge Smart Check-in & Door Unlocking IoT Solution for Hotels
        </PrimaryHeading>
        <SecondaryHeading style={{ color: "#fff" }}>
          Reformulate Guests User Experience with advance IoT Technology
        </SecondaryHeading>
        <p
          className={`${classes.smallPara} hidden d-mblock`}
          style={{ color: "#fff" }}
        >
          Intuz offered best in class IoT App Development Services to the
          hospitality industry. A Taiwan based technology consulting firm
          collaborated with us to develop complex app for its hospitality
          client. The solution completely redefines the user experience of
          check-in into the hotels by replacing traditional RFID door locking
          solution that is quick, easy and affordable.
        </p>
        <div className={`flex flex-col md:flex-row ${classes.FlexContainer}`}>
          <div className={classes.Image}>
            <LazyLoad offset={500}>
              <SVG className="img-fluid" src={cdn("iot_cs_3_293ce9dcd3.svg")} />
            </LazyLoad>
          </div>
          <div className={`${classes.Content} mt-5 mt-md-0`}>
            <p
              className={`${classes.smallPara} md:hidden`}
              style={{ color: "#fff" }}
            >
              Intuz offered best in class IoT App Development Services to the
              hospitality industry. A Taiwan based technology consulting firm
              collaborated with us to develop complex app for its hospitality
              client. The solution completely redefines the user experience of
              check-in into the hotels by replacing traditional RFID door
              locking solution that is quick, easy and affordable.
            </p>
            <h4>
              No more standing in queue at hotel reception with IoT app
              development -
            </h4>
            <ul>
              {points.map((point, i) => (
                <li key={i}>{point}</li>
              ))}
            </ul>
          </div>
        </div>
      </Container>
    </div>
  );
};

export default IotCaseStudy2;
