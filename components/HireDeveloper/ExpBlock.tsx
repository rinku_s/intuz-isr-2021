//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
const ExpBlock = (props) => {
    return (
        <div className={classes.ExpBlock}>
            <h6>{props.value}</h6>
                <p>{props.first}
                <br/>
                <span>{props.second}</span>
            </p>
            <style jsx>
                {`
                    .${classes.ExpBlock} h6{
                        background:${props.background};
                    }
                `}
            </style>
        </div>
        )
}

export default ExpBlock
