//@ts-nocheck
import { myLoader } from 'config/image-loader';
import Image from 'next/image';
import React from 'react';
import { Media, MediaContextProvider } from "../../config/responsiveQuery";
import DeveloperInfo from './DeveloperInfo';
import classes from './styles.module.scss';
const Developer = (props) => {
    let overlay = props.name == props.current;
    return (
        <MediaContextProvider>
            <div className={classes.Developer}>

                <Image src={props.image.name} alt={props.name} loader={myLoader} layout={'fixed'} height={100} width={100} />
                <h6>{props.name}</h6>
                <p>{props.designation}</p>

                <a onClick={(e) => props.onClick(props.name)}>Profile</a>
                <Media lessThan="sm">
                    {props.show && overlay && <DeveloperInfo  {...props} />}
                </Media>
            </div>
        </MediaContextProvider>
    )
}

export default Developer
