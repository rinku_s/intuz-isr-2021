//@ts-nocheck
import React from 'react'
import {cdn} from '../../config/cdn';

export const StackIcon = () => {
    return (
        <div>
            <img src={cdn('hireapp_db_icon.png?auto=format')} alt="Stack bg"/>
            <p><span>+</span>80</p>
            <style jsx>
                {`
                    div{
                        position:relative;

                    }
                    p{
                        position:absolute;
                        font-size:5.7rem;
                        line-height:6.1rem;
                        color:#4a4a4a;
                        font-weight:600;
                        top:-1rem;
                    }

                `}
            </style>
        </div>
    )
}
