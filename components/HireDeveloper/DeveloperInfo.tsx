//@ts-nocheck
import gsap from 'gsap';
import ScrollToPlugin from 'gsap/dist/ScrollToPlugin'
import React, { useEffect, useState, useContext } from 'react';
import { cdn } from '../../config/cdn';
import LinkButton from '../UI/LinkButton/LinkButton';
import Exp from './Exp';
import ExpBlock from './ExpBlock';
import classes from './styles.module.scss';
import DeveloperContext from '../../context/developerContext';
const DeveloperInfo = (props) => {
    gsap.registerPlugin(ScrollToPlugin);

    const { dev, setDev } = useContext(DeveloperContext);

    const [activetech, setActivetech] = useState();

    useEffect(() => {
        gsap.set(`.${classes.DeveloperInfo}`, { transformOrigin: "0 0", opacity: 0 });
        gsap.fromTo(`.${classes.DeveloperInfo}`, 0.6, { scaleY: 0.7, autoAlpha: 0 }, { scaleY: 1, autoAlpha: 1, ease: "Power2.easeIn" })
        let active;
        active = props.technicalexperiences[0];
        setActivetech(active.title);
        return () => {
            gsap.set(`.${classes.DeveloperInfo}`, { transformOrigin: "0 0" });
            setActivetech(null);
        };
    }, [props.technicalexperiences])

    let currenttechinfo = props.technicalexperiences.find(tech => tech.title === activetech);

    function setActiveDev() {
        let dv = { image: props.image, name: props.name, designation: props.designation };
        setDev(dv);
        gsap.to(window, 0.1, { scrollTo: { y: '#form', autoKill: false } })
    }


    return (
        <div className={classes.DeveloperInfo}>
            <div className={classes.experience}>
                <div className={classes.totalexperience}>
                    <ExpBlock value={props.totalExperience} first={"Years"} second={"Experience"} />
                    <ExpBlock background="#31D5D5" value={props.projectDone} first={"Projects"} second={"Done"} />
                </div>
                <div className={classes.technologyexperience}>
                    <ul>
                        {props.technicalexperiences.map(te => (
                            <li key={te.title} onClick={() => setActivetech(te.title)} className={activetech == te.title ? classes.active : ''}>
                                <img src={cdn(`${te.image.name}?auto=format`)} alt={te.title} />
                            </li>
                        ))}
                        {/* <li><img src={"/static/Images/developer-projects/aws.png"} alt="IOS"/></li>
                        <li><img src={"/static/Images/developer-projects/android.png"} alt="IOS"/></li>
                        <li><img src={"/static/Images/developer-projects/java.png"} alt="IOS"/></li> */}
                    </ul>
                    <div className={classes.cls}>
                        {currenttechinfo &&
                            <>
                                <Exp
                                    value={currenttechinfo.experience}
                                    strokecolor="#7ed321"
                                    percentage={currenttechinfo.expPercentage}
                                    caption="Experience"
                                />
                                <Exp
                                    value={currenttechinfo.projects}
                                    strokecolor="#31D5D5"
                                    percentage={currenttechinfo.projectsPercentage}
                                    caption="Projects"
                                />
                            </>
                        }

                    </div>
                </div>
            </div>
            <div className={`${classes.recentproject} mb-0`} >
                <h6>Recent Projects</h6>
                <p className="text-left">Get an insight on the recent high-end technology projects</p>
                <ul>
                    {props.recentprojects.map(rp => (
                        <li key={rp.image.name}><img src={cdn(`${rp.image.name}?auto=format`)} alt={rp.image.name} /></li>
                    ))}
                    {/* <li><img src={"/static/Images/developer-projects/aphos.png"} alt=""/></li>
                    <li><img src={"/static/Images/developer-projects/aphos.png"} alt=""/></li>
                    <li><img src={"/static/Images/developer-projects/aphos.png"} alt=""/></li> */}
                </ul>
                <h6 className="mt-4">Member Journey </h6>
                <p className={`${classes.memberJourney} text-left`}>{props.member_journey}</p>
            </div>
            {!props.disableStackIcon && <LinkButton variation="purpleBtn" onClick={setActiveDev}>Acquire The Talent</LinkButton>}
        </div>
    )
}

export default DeveloperInfo
