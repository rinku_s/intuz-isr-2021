//@ts-nocheck
import { myLoader } from 'config/image-loader';
import gql from 'graphql-tag';
import Image from 'next/image';
import React, { useEffect, useState } from 'react';
import { Media, MediaContextProvider } from "../../config/responsiveQuery";
import { callAPI } from '../../lib/api';
import Developer from './Developer';
import DeveloperInfo from './DeveloperInfo';
import { StackIcon } from './StackIcon';
import classes from './styles.module.scss';


const query = gql`
query{
    developers{
      member_journey
      name
      image{
        name
      }
      designation
      totalExperience
      projectDone
      recentprojects{
        link
        image{
          name
        }
      }
      technicalexperiences{
        title
        image{
          name
        }
        experience
        expPercentage
        projects
        projectsPercentage
        
      }
    }
  }
`;

const HireDeveloper = (props) => {


  const [data, setData] = useState(props.data)
  const [loading, setLoading] = useState(true)
  const [current, setCurrent] = useState()
  const [show, setShow] = useState(false);
  const ref = React.useRef();

  function initTooltip(name) {
    setCurrent(name);
    if (name == current && show) {
      setShow(false);
    } else {
      setShow(true);
    }
  }


  useEffect(() => {
    if (data) {
      setLoading(false)
    } else {
      callAPI(query).then(data => {
        setData(data);
        setLoading(false)
      });
    }
  }, [])
  if (loading) return <div> Loading...</div>
  let currentDev = data && data.developers.find(d => d.name == current);
  return (
    <MediaContextProvider>
      <div className={classes.HireDeveloper} ref={ref}>
        {data && data.developers.map(dev => (
          <Developer disableStackIcon={props.disableStackIcon} current={current} key={dev.name} show={show} onClick={initTooltip} {...dev} />
        ))}
        {!props.disableStackIcon && <>
          <Image src={'/1-backup-s3-intuz-site/hire-developers/sep.jpg'} alt="Seperator" loader={myLoader} layout={'fixed'} height={5} width={20} />
          <StackIcon />
        </>}
      </div>
      <Media greaterThanOrEqual="sm">
        {show ? <DeveloperInfo disableStackIcon={props.disableStackIcon}  {...currentDev} /> : ''}
      </Media>
    </MediaContextProvider>
  )
}

export default HireDeveloper
