//@ts-nocheck
const devs = [
    {
        name: "Myra S.",
        image: "https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/hire-developers/img01.jpg",
        designation: "Sr. Android Developer",
        info: {
            totalExperience: "5+",
            projectDone: "14+",
            recentProjects: [
                {
                    link: "#",
                    image: "/static/Images/developer-projects/aphos.png"
                },
                {
                    link: "#",
                    image: "/static/Images/developer-projects/aphos.png"
                },
                {
                    link: "#",
                    image: "/static/Images/developer-projects/aphos.png"
                },
                {
                    link: "#",
                    image: "/static/Images/developer-projects/aphos.png"
                },
            ],

            technicalExperience: [
                {
                    title: "android",
                    image: "/static/Images/developer-projects/ios.png",
                    experience: "5+",
                    expPercentage: "50",
                    projects: "9",
                    projectsPercentage: "70"
                },
                {
                    title: "android",
                    image: "/static/Images/developer-projects/ios.png",
                    experience: "5+",
                    expPercentage: "50",
                    projects: "9",
                    projectsPercentage: "70"
                },
                {
                    title: "android",
                    image: "/static/Images/developer-projects/ios.png",
                    experience: "5+",
                    expPercentage: "50",
                    projects: "9",
                    projectsPercentage: "70"
                },
                {
                    title: "android",
                    image: "/static/Images/developer-projects/ios.png",
                    experience: "5+",
                    expPercentage: "50",
                    projects: "9",
                    projectsPercentage: "70"
                },
            ]

        }
    }
]


export default devs;