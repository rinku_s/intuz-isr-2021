//@ts-nocheck
import React from 'react'
import CircularProgressBar from '../UI/CircularProgressBar/CircularProgressBar'
import classes from './styles.module.scss'
const Exp = (props) => {
    return (
        <div className={classes.Exp}>
            <CircularProgressBar
                value={props.value}
                strokeWidth={5}
                strokecolor={props.strokecolor}
                sqSize={80}
                percentage={props.percentage} />
            <p>{props.caption}</p>
        </div>
    )
}

export default Exp
