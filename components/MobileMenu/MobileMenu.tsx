//@ts-nocheck
import React from 'react';
import SocialLink from '../SocialLinks/SocialLink';
import Button from '../UI/Button/Button';
import classes from './styles.module.scss';
const MobileMenu = (props) => {
    return (
        <div className={`${classes.MobileMenu} ${props.className}`}>
            <div className='text-center'>
                <Button variation="purple">Get Quote</Button>
            </div>
            <div className={classes.contact}>
                <a href="tel:+16504511499">+1 650-451-1499</a>
                <a href="mailto:info@intuz.com">info@intuz.com</a>
            </div>
            <ul className={classes.Links}>
                <li><a href="">PARTNER WITH US</a></li>
                <li><a href="">OUR ACCREDITAIONS</a></li>
                <li><a href="">HIRE DEVELOPERS</a></li>
                <li><a href="">PROCESS</a></li>
                <li><a href="">WORK</a></li>
                <li><a href="">LABS</a></li>
                <li><a href="">BLOCKCHAIN DEVELOPMENT</a></li>
                <li><a href="">WEB APPLICATIONS</a></li>
                <li><a href="">AI BASED APPLICATIONS</a></li>
                <li><a href="">CONTACT US</a></li>
            </ul>
            <SocialLink className={classes.SocialLinks} />
        </div>
    )
}

export default MobileMenu
