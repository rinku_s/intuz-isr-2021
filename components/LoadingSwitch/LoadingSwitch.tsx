//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import { SwitchTransition, Transition } from 'react-transition-group';
import loaded from '../../hooks/loaded';
import LoadingScreen from '../LoadingScreen/LoadingScreen';


const LoadingSwitch = (props) => {
    let load = loaded();
    return (
        <SwitchTransition mode="out-in" >
            <Transition key={load}
                mountOnEnter={true}
                unmountOnExit={true}
                timeout={1000}
                addEndListener={(node, done) => {
                    if (!load) {
                        gsap.to(node, 0.5, {
                            autoAlpha: 0,
                            ease: "Power0.easeIn",
                            onComplete: done
                        });

                    } else {
                        gsap.from(node, 0.5, {
                            autoAlpha: 0,
                            ease: "Power0.easeIn",
                            onComplete: done
                        });
                    }
                }}
            >
                {!load ? <LoadingScreen /> : props.children}
            </Transition>
        </SwitchTransition>
    )
}

export default LoadingSwitch
