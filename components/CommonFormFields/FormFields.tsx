//@ts-nocheck
//@ts-nocheck
import React from 'react';
import Input from '../UI/Input/Input';
const FormFields = (props) => {
    const colWidth = (props.colWidth) ? props.colWidth : 'col-md-12';
    return (
        <>
            <Input
                invalid={!props.form.fields.f_name.valid}
                value={props.form.fields.f_name.value}
                shouldValidate={props.form.fields.f_name.validation}
                touched={props.form.fields.f_name.touched}
                changed={(event) => props.inputChangedHandler(event, "f_name")}
                inputContainerClass="col-md-6"
                elementConfig={{ id: "f_name", type: "text", placeholder: " " }}
                label={"First Name"}
            />
            <Input
                invalid={!props.form.fields.l_name.valid}
                value={props.form.fields.l_name.value}
                shouldValidate={props.form.fields.l_name.validation}
                touched={props.form.fields.l_name.touched}
                changed={(event) => props.inputChangedHandler(event, "l_name")}
                inputContainerClass="col-md-6"
                elementConfig={{ id: "l_name", type: "text", placeholder: " " }}
                label={"Last Name"}
            />
            <Input
                invalid={!props.form.fields.email.valid}
                value={props.form.fields.email.value}
                shouldValidate={props.form.fields.email.validation}
                touched={props.form.fields.email.touched}
                changed={(event) => props.inputChangedHandler(event, "email")}
                inputContainerClass={colWidth}
                elementConfig={{ id: "email", type: "email", placeholder: " " }}
                label={"Email"}
            />
            <Input
                invalid={!props.form.fields.phone.valid}
                value={props.form.fields.phone.value}
                shouldValidate={props.form.fields.phone.validation}
                touched={props.form.fields.phone.touched}
                changed={(event) => props.inputChangedHandler(event, "phone")}
                inputContainerClass={colWidth}
                elementConfig={{ id: "phone", type: "tel", placeholder: " " }}
                label={"Phone Number"}
            />
            <Input
                style={{ resize: "none" }}
                invalid={!props.form.fields.project_brief.valid}
                value={props.form.fields.project_brief.value}
                shouldValidate={props.form.fields.project_brief.validation}
                touched={props.form.fields.project_brief.touched}
                changed={(event) => props.inputChangedHandler(event, "project_brief")}
                elementType="textarea"
                inputContainerClass="col-md-12"
                elementConfig={{ id: "description", placeholder: " " }}
                label={"Project Brief"}
                toolIcon={"/static/Images/icons/info_icon.png"}
                toolTip="Provide a brief description of the project here. Create a separate, more detailed document about your project and upload it under the files section. It will allow you to be creative and save lot of your time in regards to future questions asked. If you need some guidance, you will also receive a detailed project questionnaire that will ask specific questions about your project."
            />
        </>
    )
}

export default FormFields;