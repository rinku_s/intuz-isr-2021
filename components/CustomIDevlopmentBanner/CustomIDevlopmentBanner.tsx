import React, { Component } from "react";
import LinkButton from "../UI/LinkButton/LinkButton";
import style from "./styles.module.scss";


interface CustomIDevlopmentBannerProps {
  variation?: string;
  content?: {
    title?: string;
    subTitle?: string;
    description?: string;
    buttonLink?: string;
    buttonLabel?: string;
  }

}

export default class CustomIDevlopmentBanner extends Component<CustomIDevlopmentBannerProps, {}> {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <div
        className={`${style.Content} ${this.props.variation && style[this.props.variation]
          }`}
      >
        {this.props.content.title ? <h1 className='z-10'>{this.props.content.title}</h1> : ""}
        {this.props.content.subTitle ? (
          <p
            className="mb-1"
            dangerouslySetInnerHTML={{ __html: this.props.content.subTitle }}
          ></p>
        ) : (
          ""
        )}
        {this.props.content.description ? (
          <p
            dangerouslySetInnerHTML={{ __html: this.props.content.description }}
          ></p>
        ) : (
          ""
        )}
        {this.props.content.buttonLabel ? (
          <LinkButton className='z-10'
            href={this.props.content.buttonLink}
            variation="customAppMobile"
          >
            {this.props.content.buttonLabel}
          </LinkButton>
        ) : (
          ""
        )}
      </div>
    );
  }
}
