import { myLoader } from "config/image-loader";
import Image from "next/image";
import React from "react";
import LinkButton from "../UI/LinkButton/LinkButton";
import style from "./styles.module.scss";

const Content = (props) => {
  if (props.children.length <= 0) {
    return null;
  } else {
    return (
      <div className={style.Content}>
        {props.children.map((data, index) => {
          return (
            <div className={style.ContentBlock} key={index}>
              <Image
                className="pr-20"
                layout={"intrinsic"}
                width={102}
                height={88}
                loader={myLoader}
                src={data.icon.name}
              />
              <div className={`${style.text} ml-8 mr-8`}>
                {data.title ? <h3>{data.title}</h3> : ""}
                {data.description ? <p>{data.description}</p> : ""}
                {data.link ? <a href={data.link}>More</a> : ""}
              </div>
            </div>
          );
        })}
        {props.buttonLabel ? (
          <div className="text-center">
            <LinkButton href="/adaptive-growth" variation="blue">
              Learn more
            </LinkButton>
          </div>
        ) : (
          ""
        )}
      </div>
    );
  }
};

export default Content;
