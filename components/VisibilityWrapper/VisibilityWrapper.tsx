//@ts-nocheck
import React from 'react';
import VisibilitySensor from 'react-visibility-sensor';
const VisibilityWrapper = (props) => {
    return (
        <VisibilitySensor partialVisibility offset={{ bottom: props.bottom ? props.bottom : 200 }} onChange={(visible)=>visible? props.tween.play() : ""}>
            {props.children}
        </VisibilitySensor>
    
    )
}

export default VisibilityWrapper
