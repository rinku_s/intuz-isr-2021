//@ts-nocheck
//@ts-nocheck
import React from 'react';
import Feature from './Feature';
import classes from './styles.module.scss';

const Features = (props) => {


    return (

        <ul className={`row ${classes.Featurelist}`} style={{ marginLeft: "1rem" }}>
            {props.features.map(feature => (
                <Feature key={feature.id}>{feature.feature}</Feature>
            ))}
        </ul>
    )
}

export default Features
