//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
const Feature = (props) => {
    return (
        <li className={`col-lg-6 ${classes.Feature}`}>
            {props.children}
        </li>
    )
}

export default Feature
