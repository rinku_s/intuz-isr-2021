//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
const AppleReview = (props) => {
    return (
        <div className={classes.AppleReview}>
            <div className={classes.icon} dangerouslySetInnerHTML={{__html: "/static/Images/icons/ios-ic-gray.svg?include"}}></div>
            <h4>{props.caption}</h4>
            <p>{props.description}<span>by {props.author}</span></p>
        </div>
    )
}

export default AppleReview
