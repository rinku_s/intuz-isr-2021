//@ts-nocheck
import React from 'react';
import classes from './styles.module.scss';

const TwitterReview = (props) => {
    return (
        <div className={classes.TwitterReview}>
            <div className={classes.icon} dangerouslySetInnerHTML={{__html: "/static/Images/addressicons/twitter.svg?include"}}></div>
            <h4>{props.caption}</h4>
            <p>{props.description}</p>
        </div>
    )
}

export default TwitterReview
