//@ts-nocheck
import React from 'react'
import LazyLoad from 'react-lazyload'
const ImageContainer = (props) => {
    return (
        <div>
            <LazyLoad offset={300} height={50}>
            <img src={props.img} alt={props.title}/>
            </LazyLoad>
            <style jsx>
               {`
                    div{
                        display:flex;
                        align-items:center;
                        justify-content:center;
                        background-color:${props.backcolor};    
                        margin-bottom:1.5rem;
                        padding:2rem 1.5rem;
                    }
                    img{
                            max-width:100%;
                    }
               `}
            </style>
        </div>
    )
}

export default ImageContainer
