//@ts-nocheck
import React from 'react'
import TwitterReview from './TwitterReview';
import AppleReview from './AppleReview';
import ImageContainer from './ImageContainer';
import { cdn } from '../../../config/cdn';

const Reviews = (props) => {
    return (
        <div className="row mt-mx">
            <div className="col-sm-4">
                <TwitterReview 
                    caption="Great app, easy and fun to use"
                    description="The perfect balance of simplicity, user friendliness (fun, even!), and all the information I need. Nicely done!"
                />
                <AppleReview
                    caption="Its fine"
                    description="App them is very impressive.I like that and to spend time with this cool app.I would say all can have this app u won't regret"
                    author="Freddo.Enriquee"
                />
            </div>
            <div className="col-sm-4">
                <AppleReview
                    caption="Great app, easy and fun to use"
                    description="The perfect balance of simplicity, user friendliness (fun, even!), and all the information I need. Nicely done!"
                    author="tcmoran"
                />
                <TwitterReview 
                    caption="@AppPicker"
                    description="#freeapp is gr8 4 anyone wanting an attractive #weather tool w beautiful & clean user interface. Personalize ur exp."
                />
               <ImageContainer img={cdn("phonegap-banner.png?auto=format")} title="Phone Gap" backcolor="#337ab7" />
            </div>
            <div className="col-sm-4">
                <ImageContainer img={cdn("feedmyapp-banner.png?auto=format")} title="feedmyapp" backcolor="#000" />
                <AppleReview
                    caption="Quality App!"
                    description="Its very innovative and first time I've seen such an intuitive weather app which display so much important information in a professional design. I like this update as weather is more beautiful with small animations. Its not just conventional Weather but a perfect dynamic weather information tool with accurate data."
                    author="IR07"
                />
            </div>
            <style jsx>
                {`
                    .col-sm-4{
                        display:flex;
                        flex-direction:column;
                    }
                    
                    .col-sm-4{
                        display:flex;
                        flex-direction:column;
                    }
                    .col-sm-4 div:last-child{
                            flex:1;
                    }
                `}
            </style>
        </div>
    )
}

export default Reviews
