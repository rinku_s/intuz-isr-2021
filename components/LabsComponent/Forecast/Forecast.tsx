//@ts-nocheck
import gsap from 'gsap';
import React, { useEffect } from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../../config/cdn';

const Forecast = () => {


    useEffect(() => {
        gsap.set('.img-3', { autoAlpha: 0, rotation: -47, x: -120, y: -40, opacity: 1 });
    }, [])

    function play() {
        let tl = gsap.timeline({ paused: true });
        tl.from(".img-2", 2, { rotation: -22, x: -90, ease: "power0.easeIn" })
            .to(".img-3", 2, { rotation: 0, x: 0, y: 0, autoAlpha: 1, ease: "power0.easeIn" }, "-=0.2");
        tl.play();

    }
    return (

        <div>
            <LazyLoad offset={300} height={500}>

                <img className="img-1" src={cdn("whtr_Interactive_img1.png?auto=format")} alt="Wther Interactive Image 1" />
                <img className="img-2" src={cdn("whtr_Interactive_img2.png?auto=forma")} alt="Wther Interactive Image 2" />
                <img className="img-3" src={cdn("whtr_Interactive_img3.png?auto=format")} alt="Wther Interactive Image 3" />
            </LazyLoad>

            <style jsx>
                {`
                    div{
                        position:relative;
                        text-align:center;
                        margin-top:5rem;
                    }

                    img{
                        width:auto;
                        max-width:100%;
                        vertical-align: bottom;
                    }
                    .img-1{
                        position: absolute;
                        z-index: 2;
                        left: 20%;
                        width:33%;
                        top:10%;
                        height:auto;
                        
                    }
                    .img-2{
                        height:auto;
                        width:43%;

                        position: relative;
                        z-index: 1;
                    }
                    .img-3{
                        position: absolute;
                        right: 14%;
                        bottom: 0;
                        z-index: 0;
                        width:47%;

                        height:auto;
                    }

                    
                `}
            </style>
        </div>
        // </WaypointOnce >
    )
}

export default Forecast
