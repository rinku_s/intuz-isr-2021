//@ts-nocheck
import React from 'react';
import Container from '../../Container/Container';
import classes from './styles.module.scss';
const GetNow = (props) => {
    return (
        <section className={`${classes.GetNow} ${classes[props.variation]}`}>
            <Container>
                <h2>Get Now</h2>
                {props.subtitle ? <p>{props.subtitle}</p> : ''}
                {props.children}
            </Container>
        </section>
    )
}

export default GetNow
