//@ts-nocheck
//@ts-nocheck
import React from 'react';
import Container from '../../Container/Container';
import classes from './styles.module.scss';
const SectionFlex = (props) => {
    let d1 = React.useRef();
    let d2 = React.useRef();



    return (
        <section className={`${classes.ElegentSection} ${classes[props.variation]}`}>
            <Container>
                <div className={classes.flexContainer}>

                    <div className={`${classes.Content}`} ref={d1}>
                        <h2>
                            {props.heading}
                        </h2>
                        <p>{props.description}</p>
                    </div>

                    <div className={`${classes.image}`} ref={d2}>
                        <img className="max-w-full h-auto" src={props.image} mwidth={326} dwidth={526} alt={props.alt} />
                    </div>
                </div>
            </Container>
            <style jsx>
                {`
                    .${classes.ElegentSection}{
                        ${props.backcolor ? 'background-color:' + props.backcolor : ''};
                    }
                    .${classes.flexContainer}{
                        flex-direction: ${props.direction ? props.direction : 'inherit'};
                    }
                    .${classes.Content}{
                        ${props.center ? "align-self:center" : ''}
                    }
                `}
            </style>
        </section>
    )
}

export default SectionFlex
