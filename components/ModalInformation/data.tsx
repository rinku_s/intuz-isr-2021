//@ts-nocheck
export const data = [
        {
            "id":"mobile",
            "title":"App Design & Engineering",
            "content":[
                {
                    "title":"Custom&nbsp;Mobile<br/>Development",
                    "image":"/static/Images/cmd.png",
                    "link":"/applications",
                    "description":"Experience landmark presence in creating trendy user interface, delivering exceptional user experience and presenting visually rich design."
                },
                {
                    "title":"Rapid&nbsp;Application<br/>Development",
                    "image":"/static/Images/cmd.png",
                    "link":"/rapid-application-development",
                    "description":"A software-development approach designed to produce high-quality products quickly using rapid prototyping."
                },
                {
                    "title":"React&nbsp;Native<br/>Development",
                    "image":"/static/Images/cmd.png",
                    "link":"/applications",
                    "description":"Create an app with native mobile app experience having a single code base in substantially less development time with the use of react native."
                }
            ],
    
            "link":"/applications"
    
        },
    
        {
            "id":"web",
            "title":"Enterprise Web Applications",
            "content":[
                {
                    "title":"Business Process Automation",
                    "image":"/static/Images/cmd.png",
                    
                    "description":"Enable desired automation and associate multiple business systems into a seamless environment for reliable cost efficiency."
                },
                {
                    "title":"Enterprise Web Solutions",
                    "image":"/static/Images/cmd.png",
                    
                    "description":"Intuz offers a range of web and mobility solutions for agile enterprises that can resonate the pace technology demand."
                },
                {
                    "title":"Product Engineering",
                    "image":"/static/Images/cmd.png",
                    
                    "description":"Engross in a new-age product life-cycle that enables extensive innovation, intricate architectures, migration and user acceptance."
                }
            ],
            
            "link":"/applications"
    
        },
    
        {
            "id":"blockchain",
            "title":"Blockchain",
            "content":[
                {
                    "title":"dApps <br/>Development",
                    "image":"/static/Images/cmd.png",
                    "description":"Develop highly reliable decentralized applications (dAPPs) for the business that can make the unimaginable possible to improve business operations."
                },
                {
                    "title":"Ethereum Development",
                    "image":"/static/Images/cmd.png",
                    "description":"Develop scalable decentralized business applications and smart contracts on top of robust Ethereum platform to empower your business needs."
                },
                {
                    "title":"Private Blockchain Development",
                    "image":"/static/Images/cmd.png",
                    "description":"Develop private blockchain networks that are essential for enterprises looking for more control over their critical business information."
                }
            ],
            
            "link":"/applications"
        },

        {
            "id":"cloud",
            "title":"Cloud",
            "content":[
                {
                    "title":"Cloud Services",
                    "image":"/static/Images/cmd.png",
                    "description":"Solutions for Cloud Migration, AWS Deployment and Consultation to astute Cost Analysis and DR Plans from certified AWS Consulting Partners.",
                    "link":"/services"
                },
                {
                    "title":"Applications",
                    "image":"/static/Images/cmd.png",
                    "description":"Intuitive server-side applications and AMIs to configure amazing products with ease, exclusively on AWS Marketplace.",
                    "link":"/applications"
                },
                {
                    "title":"Use Cases",
                    "image":"/static/Images/cmd.png",
                    "description":"Explore our work and achieved results through brief case studies of our esteemed clients for cloud applications and solutions.",
                    "link":"/case-studies"
                }
            ],
            
            "link":"https://cloud.intuz.com/"
        },

        {
            "id":"ai",
            "title":"AI Based Software",
            "content":[
                {
                    "title":"Machine Learning",
                    "image":"/static/Images/cmd.png",
                    "description":"We enable our customers with pattern recognition and predictive analytics for better decision making using machine learning capabilities."
                },
                {
                    "title":"Bot Development",
                    "image":"/static/Images/cmd.png",
                    "description":"Intuitive server-side applications and AMIs to configure amazing products with ease, exclusively on AWS Marketplace."
                },
                {
                    "title":"Ops Automation",
                    "image":"/static/Images/cmd.png",
                    "description":"Overcome the challenges on execution of your strategic goals & maximize business ROI by automating complex operations using cognitive technologies."
                }
            ],
            
            "link":"/ai"
        }
    ]