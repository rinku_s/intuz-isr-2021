//@ts-nocheck
//@ts-nocheck
import gsap from "gsap";
import React, { Component } from "react";
import MobileMenu from "../MobileMenu/MobileMenu";
import Input from "../UI/Input/Input";
import { data } from "./data";
import classes from "./styles.module.scss";

export default class ModalInformation extends Component {
  state = {
    data: data,

    currentData: {
      id: "mobile",
      title: "App Design & Engineering",
      content: [
        {
          title: "Custom Mobile Development",
          image: "/static/Images/cmd.png",
          link: "/applications",
          description:
            "Experience landmark presence in creating trendy user interface, delivering exceptional user experience and presenting visually rich design.",
        },
        {
          title: "Rapid Application Development",
          image: "/static/Images/cmd.png",
          link: "/rapid-application-development",
          description:
            "A software-development approach designed to produce high-quality products quickly using rapid prototyping.",
        },
        {
          title: "React Native Development",
          image: "/static/Images/cmd.png",
          link: "/applications",
          description:
            "Create an app with native mobile app experience having a single code base in substantially less development time with the use of react native.",
        },
      ],

      link: "/applications",
    },
    wasMore: false,
  };

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.current !== this.props.current) {
      if (window.innerWidth >= 767) {
        gsap.fromTo(".twen", 0.5, { xPercent: -100 }, { xPercent: 0 });
      } else {
        gsap.fromTo(".twen", 0.5, { yPercent: 100 }, { yPercent: 0 });
      }
    }
  }

  render() {
    if (this.props.activeFilter == "more") {
      return <MobileMenu className="twen" />;
    } else if (this.props.current == undefined) {
      return <MobileMenu className="twen" />;
    } else {
      return (
        <div className={`${classes.ModalInformation} twen`}>
          <button className={classes.Close} onClick={this.props.onClose}>
            <img
              src="https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/2017/close-popup.png"
              alt="Close"
            />
          </button>
          <div className={`${classes.Container}`}>
            {this.props.current.content.map((el) => (
              <div className={classes.Content} key={el.title}>
                {/* <div className={classes.HeadingIcon}> */}
                <img src={el.image} alt={el.title} />
                <h3>
                  <span dangerouslySetInnerHTML={{ __html: el.title }}></span>
                  &nbsp;
                  <br className="block d-sm-none" />
                  {el.link ? <a href={el.link}>Know More</a> : ""}
                </h3>
                {/* </div> */}

                <p>{el.description}</p>
              </div>
            ))}
          </div>
          <p className={classes.CenterText}>
            Learn more about our{" "}
            <a href={this.props.current.link}>{this.props.current.title}</a>{" "}
            services.
          </p>
          <div className={classes.FormContainer}>
            <h3>
              Get
              <br />
              Started
            </h3>
            <form className={classes.Form}>
              <div className="row">
                <Input
                  inputContainerClass="col-4"
                  elementConfig={{
                    id: "f_name",
                    type: "text",
                    placeholder: " ",
                  }}
                  label={"First Name"}
                />
                <Input
                  inputContainerClass="col-4"
                  elementConfig={{
                    id: "l_name",
                    type: "text",
                    placeholder: " ",
                  }}
                  label={"Last Name"}
                />
                <Input
                  inputContainerClass="col-4"
                  elementConfig={{
                    id: "email",
                    type: "text",
                    placeholder: " ",
                  }}
                  label={"Email"}
                />
                <Input
                  inputContainerClass="col-6"
                  elementConfig={{ id: "phone", type: "tel", placeholder: " " }}
                  label={"Phone Number"}
                />
                <Input
                  elementType="textarea"
                  inputContainerClass="col-6"
                  elementConfig={{
                    id: "description",
                    placeholder: " ",
                    rows: "1",
                  }}
                  label={"Project Description?"}
                />
              </div>

              <div className={classes.FormBtn}>
                <button type="submit">Send</button>
                <img
                  src="https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/2017/popup-amazon-logo.png"
                  alt="Amazon Logo"
                />
              </div>
            </form>
          </div>
        </div>
      );
    }
  }
}

{
  /* <div className={classes.Content}>
                        <img src={"/static/Images/cmd.png"} alt="Custom Mobile Development"/>
                        <h3>Custom  Mobile Development <a href="">Know More</a></h3>
                        
                        <p>Experience landmark presence in creating trendy user interface, delivering exceptional user experience and presenting visually rich design.</p>
                    </div>
                    <div className={classes.Content}>
                        <img src={"/static/Images/cmd.png"} alt="Custom Mobile Development"/>
                        <h3>Custom Mobile Development <a href="">Know More</a></h3>
                        
                        <p>Experience landmark presence in creating trendy user interface, delivering exceptional user experience and presenting visually rich design.</p>
                    </div>
                    <div className={classes.Content}>
                        <img src={"/static/Images/cmd.png"} alt="Custom Mobile Development"/>
                        <h3>Custom Mobile Development <a href="">Know More</a></h3>
                        
                        <p>Experience landmark presence in creating trendy user interface, delivering exceptional user experience and presenting visually rich design.</p>
                    </div> */
}
