import Container from "components/Container/Container";
import Link from "next/link";
import { useRouter } from "next/router";
import React, { useEffect, useState } from "react";

interface Category {
  name: string;
  link: string;
}

const CategoriesNav: React.FC<{ categories: Category[] }> = ({
  categories,
}) => {
  let cat = [...categories];
  const [showDropdown, setShowDropdown] = useState(false);
  let cls = showDropdown ? "h-96 py-5" : "h-0 py-0";
  const router = useRouter();

  const [fixed, setFixed] = useState(false);

  let aclass = fixed ? "text-white" : "text-[#6f6f6f]";

  useEffect(() => {
    window.addEventListener("scroll", checkScroll);
    return () => {
      window.removeEventListener("scroll", checkScroll);
    };
  }, []);

  const catref = React.useRef<HTMLDivElement>();

  const checkScroll = () => {
    if (window.innerWidth > 767) {
      let offset = catref.current ? catref.current.offsetTop : "";
      if (offset) {
        if (window.pageYOffset > offset) {
          setFixed(true);
        }
        if (window.pageYOffset < 500) {
          setFixed(false);
        }
      }
    }
  };

  return (
    <section
      className={`w-full z-20 ${fixed ? "fixed bg-header" : ""}`}
      style={{ top: "8rem" }}
      ref={catref}
    >
      <Container>
        <ul className={`hidden md:flex list-none justify-between gap-10`}>
          <li className="py-10">
            <Link href={`/blog`}>
              <a
                className={`${aclass} no-underline text-16 leading-none font-nunitosans whitespace-nowrap ${!router?.query?.name ? "font-bold" : ""
                  }`}
              >
                Latest Article
              </a>
            </Link>
          </li>
          {cat.slice(0, 3).map((c) => (
            <li className="py-10" key={c.name}>
              <Link href={`/blog/category/` + c.link}>
                <a
                  className={`${aclass} text-16 no-underline  leading-none font-nunitosans whitespace-nowrap ${router.query.name === c.link ? "font-bold" : ""
                    }`}
                >
                  {c.name}
                </a>
              </Link>
            </li>
          ))}
          <li className={`py-10 whitespace-nowrap relative`}>
            <button
              onClick={() => setShowDropdown(!showDropdown)}
              className={`${aclass} text-16 leading-none font-nunitosans`}
            >
              More +
            </button>
            <div
              className={
                cls +
                " absolute bg-white overflow-scroll right-0 w-[200px] flex flex-col top-20"
              }
              style={{
                boxShadow: "0 0 30px #ececec",
                transition: "all 0.3s ease-out",
              }}
            >
              {cat.slice(3).map((c) => (
                <Link href={`/blog/category/` + c.link}>
                  <a className="inline-block text-16 leading-none no-underline font-nunitosans py-8 px-3 hover:bg-[#f8f9fa]">
                    {c.name}
                  </a>
                </Link>
              ))}
            </div>
          </li>
        </ul>
      </Container>
    </section>
  );
};

export default CategoriesNav;
