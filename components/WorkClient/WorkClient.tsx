//@ts-nocheck
//@ts-nocheck
import { myLoader } from "config/image-loader";
import gsap from "gsap";
import Image from "next/image";
import Link from "next/link";
import React, { Component } from "react";
import classes from "./styles.module.scss";

const _loaded = {};
class WorkClient extends Component {
  constructor(props) {
    super(props);
  }

  state = {
    loaded: _loaded[this.props.src],
  };

  static defaultProps = {
    className: "",
    loadingClassName: classes.imgloading,
    loadedClassName: classes.imgloaded,
  };

  onLoad = () => {
    _loaded[this.props.src] = true;
    this.setState(() => ({ loaded: true }));
  };

  play = () => {
    gsap.fromTo(
      this.img,
      0.5,
      { scale: 0, autoAlpha: 0 },
      { scale: 1, autoAlpha: 1, ease: "Power0.easeIn" }
    );
  };

  render() {
    let { className, loadedClassName, loadingClassName, ...props } = this.props;
    className = `${className} ${
      this.state.loaded ? loadedClassName : loadingClassName
    }`;

    // let image = (

    // )

    return (
      <div key={this.props.index} className="relative h-[60px]">
        {this.props.link ? (
          <Link as={this.props.link} href={"/case-studies/[id]"}>
            <a>
              {/* <img className="img" ref={c => this.img = c} src={this.props.src} alt={this.props.alt} title={this.props.title} /> */}
              <Image
                sizes="30vw"
                loader={myLoader}
                layout="fill"
                objectFit="contain"
                src={this.props.src}
                alt={this.props.alt}
              />
            </a>
          </Link>
        ) : (
          // <img className="img" ref={c => this.img = c} src={this.props.src} alt={this.props.alt} title={this.props.title} />
          <Image
            sizes="30vw"
            loader={myLoader}
            layout="fill"
            objectFit="contain"
            src={this.props.src}
            alt={this.props.alt}
          />
        )}
      </div>
    );
  }
}
export default WorkClient;
