//@ts-nocheck
import Axios from 'axios';
import React from 'react';
import classes from './styles.module.scss';
const DownloadPDF = (props) => {

    function submit(evt) {
        evt.preventDefault();
        const data={
            link:props.link
        }
        console.log(data);
        
        
        // Axios.post("http://52.88.55.64:5051/download", data).then(c=>{
        //     console.log(c);
        //     var a = document.createElement('a');
        //     window.open(c.data)
        // });
    }

    return (
        <div className={classes.Download}>
            <div className={classes.Image}>
                <img src="https://cdn.cp.adobe.io/content/2/rendition/283104a5-039b-40e3-bdaa-f7b1d28d5cc8/version/0/format/jpg/dimension/width/size/400" alt="Dr"/>
            </div>
            <div className={classes.Content}>
                <h5>Don’t want to miss anything?</h5>
                <p>Download our PDF list of 500 websites that accept and want guests posts!
                </p>
                <form action="http://52.88.55.64:5051/download" method="POST">
                    <input type="hidden" name="link" value={props.link}/>
                    <button type="submit">Download</button>
                </form>
            </div>
        </div>
    )
}

export default DownloadPDF
