//@ts-nocheck
//@ts-nocheck
import { myLoader } from "config/image-loader";
import gsap from "gsap";
import Image from "next/image";
import React, { useState } from "react";
import { Transition } from "react-transition-group";
import { cdn } from "../../../config/cdn";
import ImageBlock from "../../ImageBlock/ImageBlock";
import classes from "./styles.module.scss";

const WhyBlockChain = (props) => {
  const [show, setShow] = useState(false);
  return (
    <div className={classes.WhyBlockChain}>
      <div className={classes.image}>
        <Image
          layout="intrinsic"
          loader={myLoader}
          src="why_blockchain.png"
          width="490"
          height="428"
          alt="why_blockchain"
        />
      </div>
      <div className={classes.content}>
        <p>
          The blockchain technology is continuing to disrupt several industries
          through the variety of use cases. This technology is a tamper-proof
          digital ledger of transactions that can be used to store valuable
          information and can be shared without compromising the security of the
          network. Information stored on a blockchain network is stored in a
          block that is connected to the next block making it a highly-secure
          data chain. Users having a private key can access and share data in
          real-time without having to think about safety.
        </p>
        <p>
          The blockchain is being used by several industries as a self-reliant
          data management framework. Apart from the financial uses such as in
          the case of cryptocurrencies, enterprises are using this to comply
          with data storage and management protocols, understanding customers
          better and transmitting critical information, securely.
        </p>
        <Transition
          in={show}
          timeout={1000}
          mountOnEnter={true}
          unmountOnExit={true}
          addEndListener={(n, done) => {
            gsap.set(n, { transformOrigin: "100% 50%" });
            gsap.set(n, { transformOrigin: "0% 50%" });

            if (show) {
              gsap.fromTo(
                n,
                0.5,
                { width: "0", height: "0", opacity: 0, ease: "power0.easeOut" },
                {
                  width: "100%",
                  height: "135px",
                  opacity: 1,
                  ease: "power0.easeOut",
                }
              );
            } else {
              gsap.fromTo(
                n,
                0.5,
                {
                  width: "100%",
                  height: "135px",
                  opacity: 1,
                  ease: "power0.easeOut",
                },
                { width: "0", height: "0", opacity: 0, ease: "power0.easeOut" }
              );
            }
          }}
        >
          {(state) => (
            <p>
              This technology does not suffer from the problem of single point
              of failure as the network is highly decentralized. No single
              entity can control the network while everyone has the power to
              harness the network. The technology has multiple applications for
              modern businesses. From manufacturing to finance, logistics to
              supply chain management, healthcare to data processing, the
              possibilities are endless here.
            </p>
          )}
        </Transition>
        <a onClick={() => setShow(!show)}>{show ? "Hide" : "Know More"}</a>
      </div>
    </div>
  );
};

export default WhyBlockChain;
