//@ts-nocheck
import React from 'react';
import FormFields from '../../CommonFormFields/FormFields';
import classes from './styles.module.scss';

const Form = () => {
    return (
        <div className={classes.Form}>
            <form action="" className="row">
                <FormFields colWidth="col-md-6" />
                <div className={`col-md-4 ${classes.Buttons}`}>
                    <button type="submit">Send</button>
                    <button type="reset">Clear</button>
                </div>
                <div className={`col-md-8 ${classes.privacytext}`}>
                    <h3>
                        We value your unique ideas and IP
                    </h3>
                    <p>
                        To ensure complete privacy, you can request a non-disclosure agreement.  Just mention it in the message.
                    </p>
                </div>

            </form>
        </div>
    )
}

export default Form
