//@ts-nocheck
import React from "react";
import LazyLoad from "react-lazyload";
import classes from "./styles.module.scss";
import { myLoader } from "config/image-loader";
import Image from "next/image";
const Technology = (props) => {
  console.log(props);
  return (
    <div className={`${props.className} ${classes.Technology}`}>
      <LazyLoad height={300} offset={300} once>
        <Image
          layout="intrinsic"
          loader={myLoader}
          src={props.image}
          alt={props.title}
          width="180"
          height="120"
        />
      </LazyLoad>
      <span>{props.title}</span>
    </div>
  );
};

export default Technology;
