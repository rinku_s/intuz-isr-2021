//@ts-nocheck
import React from "react";
import Technology from "./Technology";

const TechnologyStack = (props) => {
  return (
    <div className={`row justify-center`} style={{ marginTop: "8rem" }}>
      {props.blockchaintechstacks.map((bt) => (
        <Technology
          key={bt.id}
          className="col-4 col-md-2 mb-5"
          image={bt.image.name}
          title={bt.title}
        />
      ))}
    </div>
  );
};

export default TechnologyStack;
