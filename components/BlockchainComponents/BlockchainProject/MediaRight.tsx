//@ts-nocheck
import { myLoader } from "config/image-loader";
import Image from "next/image";
import React from "react";
import { cdn } from "../../../config/cdn";
import classes from "./styles.module.scss";

const MediaRight = () => {
  return (
    <div className={`${classes.MediaRight}`}>
      <Image
        layout="intrinsic"
        loader={myLoader}
        src="blockchain_project.png"
        alt="BlockChain_Project"
        width="650"
        height="460"
      />
    </div>
  );
};

export default MediaRight;
