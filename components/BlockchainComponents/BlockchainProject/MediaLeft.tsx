//@ts-nocheck
import React from 'react'
import Point from './Point';
import classes from './styles.module.scss';
import MediaRight from './MediaRight';
const MediaLeft = (props) => {
    return (
        <div className={`${classes.mediabody}`}>
            <h5>A Cryptocurrency exchange solution based on Blockchain technology</h5>
            <p>Intuz developed a highly secured exchange solution for a US-based client looking for trading Cryptocurrencies like BitCoin and Ethereum with the backbone of Blockchain technology.</p>
            <MediaRight/>
            <h6>Solution Features</h6>
            <ul>
                <Point>Option to trade from USD to BTC/ETH and CAD to BTC/ETH</Point>
                <Point>Real-time exchange rates for Cryptocurrencies</Point>
                <Point>Trading Volume</Point>
                <Point>24 hours High and Low pricing for BTC/ETH</Point>
                <Point>Secure wallet</Point>
                <Point>Strong reporting functionality</Point>
                <Point>The exchange platform is very scalable and highly secured</Point>
                <Point>Implementation of Micro-services architecture</Point>
            </ul>
        </div>
    )
}

export default MediaLeft
