//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
const Point = (props) => {
    return (
        <li className={classes.Points}>
            {props.children}
        </li>
    )
}

export default Point
