//@ts-nocheck
import React from 'react'
import Point from '../Point';
import classes from './styles.module.scss'; 
import Container from '../../../Container/Container';
const Block = (props) => {
    return (
        <div className={classes.Block} style={props.style}>
            <Container>

            <h3>
                {props.title}
            </h3>
            <ul>
                {props.points.map(point=>(
                    <Point key={point}>{point}</Point>
                    ))}
            </ul>
            </Container>
        </div>
    )
}

export default Block
