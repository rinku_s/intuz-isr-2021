//@ts-nocheck
import React from 'react'
import Block from './Block';
import classes from './styles.module.scss';
import Container from '../../../Container/Container';
const features = [
    {
        id: "security",
        title: "Security features include",
        points: [
            "VPN on Mobile Devices",
            "Docker",
            "2 Factor Authentication"
        ]
    },
    {
        id: "technology",
        title: "Technology Stack",
        points: [
            "Node.JS for back-end services",
            "Electron.JS for creating a cross-platform desktop application",
            "MongoDB as a storage engine"
        ]
    },
]

const SecurityandTechPoints = (props) => {
    return (
        <div className={classes.SecurityandTechPoints}>
            <Container>
                {features.map(feature=>(
                    <Block key={feature.id} {...feature}/>
                ))}
            </Container>
        </div>
    )
}

export default SecurityandTechPoints
