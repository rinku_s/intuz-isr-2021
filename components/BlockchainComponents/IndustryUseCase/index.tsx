//@ts-nocheck
import React from 'react';
import { Media, MediaContextProvider } from "../../../config/responsiveQuery";
import ForDesktop from './forDesktop';
import ForMobile from './forMobile';

const IndustryUseCase = (props) => {
    return (
       
       <MediaContextProvider>
        <Media greaterThanOrEqual="sm">
            <ForDesktop/>
        </Media>
        <Media lessThan="sm">
            <ForMobile blockchainusecases={props.blockchainusecases}/>
        </Media>
       </MediaContextProvider>
    )
}

export default IndustryUseCase
