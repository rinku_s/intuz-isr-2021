//@ts-nocheck
import React from "react";
import classes from "./styles.module.scss";
import { myLoader } from "config/image-loader";
import Image from "next/image";
const ImageBlock = (props) => {
  return (
    <figure className={classes.ImageBlock}>
      <Image
        layout="intrinsic"
        loader={myLoader}
        src={props.image}
        alt={props.title}
        width="78"
        height="78"
      />
      {/* <img src={`${props.image}?auto=format`} alt={props.title} /> */}
      <figcaption>{props.title}</figcaption>
    </figure>
  );
};

export default ImageBlock;
