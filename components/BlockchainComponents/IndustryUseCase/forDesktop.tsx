//@ts-nocheck
//@ts-nocheck
import React from "react";
import { cdn } from "../../../config/cdn";
import ImageBlock from "../../ImageBlock/ImageBlock";
import { myLoader } from "config/image-loader";
import Image from "next/image";
const forDesktop = () => {
  return (
    <div style={{ marginTop: "6rem" }}>
      <Image
        layout="intrinsic"
        loader={myLoader}
        src="blockchain-usecase-img.png"
        alt="Block Chain Use Case"
        width="1310"
        height="767"
      />
      {/* <ImageBlock
        src={cdn("blockchain-usecase-img.png")}
        alt="Block Chain Use Case"
        className={`img-fluid`}
        dwidth={1310}
        mwidth={767}
        format={"png"}
      /> */}
    </div>
  );
};

export default forDesktop;
