//@ts-nocheck
import React from "react";
import ImageBlock from "./ImageBlock";
import classes from "./styles.module.scss";

const forMobile = (props) => {
  return (
    <div className={classes.forMobile} style={{ marginTop: "6rem" }}>
      {props.blockchainusecases.map((uc) => (
        <ImageBlock key={uc.id} image={uc.image.name} title={uc.title} />
      ))}
    </div>
  );
};

export default forMobile;
