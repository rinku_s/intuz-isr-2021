//@ts-nocheck
import React from "react";
import { cdn } from "../../../config/cdn";
import BlockchainService from "./BlockchainService";
import classes from "./styles.module.scss";

const BlockchainServices = (props) => {
  return (
    <div className={`row justify-center`} style={{ marginTop: "8rem" }}>
      {props.blockchainservices.map((blockchainservice) => (
        <div
          className={`col-6 col-sm-4 col-md-3 ${classes.ServiceBlock}`}
          key={blockchainservice.id}
        >
          <BlockchainService
            image={blockchainservice.icon.name}
            title={blockchainservice.title}
            subtitle="Development"
          />
        </div>
      ))}
      {/* <div className="col-6 col-sm-4 col-md-3">
                <BlockchainService
                    image={"/static/Images/blockhain-services/dapps.png"}
                    title="dApps"
                    subtitle="Development"
                />
            </div>
            <div className="col-6 col-sm-4 col-md-3">
                <BlockchainService
                    image={"/static/Images/blockhain-services/dapps.png"}
                    title="dApps"
                    subtitle="Development"
                />
            </div>
            <div className="col-6 col-sm-4 col-md-3">
                <BlockchainService
                    image={"/static/Images/blockhain-services/dapps.png"}
                    title="dApps"
                    subtitle="Development"
                />
            </div>
            <div className="col-6 col-sm-4 col-md-3">
                <BlockchainService
                    image={"/static/Images/blockhain-services/dapps.png"}
                    title="dApps"
                    subtitle="Development"
                />
            </div>
            <div className="col-6 col-sm-4 col-md-3">
                <BlockchainService
                    image={"/static/Images/blockhain-services/dapps.png"}
                    title="dApps"
                    subtitle="Development"
                />
            </div>
            <div className="col-6 col-sm-4 col-md-3">
                <BlockchainService
                    image={"/static/Images/blockhain-services/dapps.png"}
                    title="dApps"
                    subtitle="Development"
                />
            </div>
            <div className="col-6 col-sm-4 col-md-3">
                <BlockchainService
                    image={"/static/Images/blockhain-services/dapps.png"}
                    title="dApps"
                    subtitle="Development"
                />
            </div> */}
    </div>
  );
};

export default BlockchainServices;
