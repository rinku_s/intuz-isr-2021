//@ts-nocheck
import Image from "next/image";
import React from "react";
import LazyLoad from "react-lazyload";
import classes from "./styles.module.scss";
import { myLoader } from "config/image-loader";
const BlockchainService = (props) => {
  return (
    <div className={classes.BlockchainService}>
      {/* <LazyLoad height={300} offset={300} once> */}
        <Image
          layout="intrinsic"
          loader={myLoader}
          src={props.image}
          alt={props.title}
          width="180"
          height="180"
        />
      {/* </LazyLoad> */}
      <div>
        <h2 className={classes.boldText}>{props.title}</h2>
        <h2>{props.subtitle}</h2>
      </div>
    </div>
  );
};

export default BlockchainService;
