//@ts-nocheck
//@ts-nocheck
import React, { Component } from "react";
import { cdn } from "../../config/cdn";
import IconBlock from "../IconBlock/IconBlock";
import LinkButton from "../UI/LinkButton/LinkButton";
import style from "./styles.module.scss";

export default class IotCasestudy extends Component {
  constructor(props) {
    super(props);
  }

  // handleElementEnter = (visible) => {
  //     if(visible){
  //         document.getElementById('ftiot').style.backgroundColor = `${this.props.backColor}`
  //         document.getElementById('ftiot').style.transition = 'background 0.5s linear'
  //     }
  // }

  render() {
    // console.log(this.props.backColor);
    return (
      // <VisibilitySensor partialVisibility offset={{ bottom: 200 }} onChange={this.handleElementEnter}>
      <div
        className={`${style.IotCasestudy} ${
          style[this.props.variation]
        } flex flex-col md:flex-row${
          this.props.variation == "sgc" ? "-reverse" : ""
        } items-center justify-center cst`}
        data-scrollcolor={this.props.backColor}
      >
        <div className="mr-3">
          <img
            className="img-fluid"
            src={cdn(`${this.props.image}?auto=format`)}
            alt={this.props.image}
          />
        </div>
        <div className={`${style.desc} text-center my-5 my-md-0`}>
          <img
            src={cdn(`${this.props.logo}?auto=format`)}
            alt={this.props.logo}
            title={this.props.logoTitle}
          />
          <p>{this.props.desc}</p>
          <IconBlock icons={this.props.IconArray} />
          <LinkButton
            as={this.props.href}
            href={"/case-studies/[id]"}
            variation="caseBlockWhiteBtn"
          >
            View Case Study
          </LinkButton>
        </div>
      </div>
      // </VisibilitySensor>
    );
  }
}
