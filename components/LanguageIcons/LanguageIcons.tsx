//@ts-nocheck

import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';
import style from './styles.module.scss';

const LanguageIcons = () => {
    return (
        <div className={style.iconGrp}>
            <LazyLoad height={300} offset={300} once>
                <div>
                    <img src={cdn('flag1.png')} alt='flag1.png' />
                    <img src={cdn('flag2.png')} alt='flag2.png' />
                    <img src={cdn('flag3.png')} alt='flag3.png' />
                    <img src={cdn('flag4.png')} alt='flag4.png' />
                    <img src={cdn('flag5.png')} alt='flag5.png' />
                    <img src={cdn('flag6.png')} alt='flag6.png' />
                </div>
            </LazyLoad>
            <LazyLoad height={300} offset={300} once>
                <div>
                    <img src={cdn('flag7.png')} alt='flag7.png' />
                    <img src={cdn('flag8.png')} alt='flag8.png' />
                    <img src={cdn('flag9.png')} alt='flag9.png' />
                    <img src={cdn('flag10.png')} alt='flag10.png' />
                    <img src={cdn('flag11.png')} alt='flag11.png' />
                </div>
            </LazyLoad>
        </div>
    )
}

export default LanguageIcons