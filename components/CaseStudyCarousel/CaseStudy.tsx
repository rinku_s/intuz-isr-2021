import { myLoader } from "config/image-loader";
import Image from "next/image";
import Link from "next/link";
import React from "react";
import classes from "./styles.module.scss";
const CaseStudy = (props) => {
  return (
    <div className={`relative ${classes.CaseStudy}`}>
      <Image
        loader={myLoader}
        src={props.background}
        layout="fill"
        objectFit="cover"
      />
      <div className="text-center w-9/12 flex-1">
        <Image
          loader={myLoader}
          src={props.logo}
          layout={"responsive"}
          width={700}
          height={600}
        />
      </div>
      <div className={`${classes.content} text-center md:text-left`}>
        <h2 className="relative">{props.title}</h2>
        <p className="relative mx-auto md:mx-px">{props.description}</p>
        <ul className={classes.links}>
          <li className={classes.img}>
            {props.icons.map((icon, i) => {
              return (
                <Image
                  loader={myLoader}
                  src={
                    icon.image.split("https://intuz-site.imgix.net/uploads/")[1]
                  }
                  alt={icon.alt}
                  layout="fixed"
                  width={30}
                  height={30}
                  className="pr-8"
                />
              );
            })}
          </li>
          <li>
            <Link href="case-studies/[id]" as={props.link} prefetch={false}>
              <a className="relative">View Case Study</a>
            </Link>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default CaseStudy;
