import React from "react";
import SwiperCore, { Autoplay, EffectFade } from "swiper";
import "swiper/components/effect-fade/effect-fade.min.css";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.min.css";
import CaseStudy from "./CaseStudy";

const data = [
  {
    background: `case-studies01.jpg`,
    logo: `casestudy_phyzyou.png`,
    title: "Health & Fitness",
    description: "Multi-platform office fitness app for quick on-desk workout",
    link: "/case-studies/phyzyou",
    icons: [
      {
        image:
          "https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/works/ios-ic.svg",
        alt: "Ios Icon",
      },
      {
        image:
          "https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/works/case-studies/and-ic.svg",
        alt: "Android Icon",
      },
      {
        image:
          "https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/works/case-studies/mac.svg",
        alt: "Mac Icon",
      },
      {
        image:
          "https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/works/case-studies/windows.svg",
        alt: "Windows Icon",
      },
    ],
  },
  {
    background: `case-studies02.jpg`,
    logo: `casestudy_foot.png`,
    title: "Sports & Events",
    description:
      "Revolutionary directory app Explore your favourite nearby activities",
    link: "/case-studies/live4it",
    icons: [
      {
        image:
          "https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/works/ios-ic.svg",
        alt: "Ios Icon",
      },
      {
        image:
          "https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/works/case-studies/and-ic.svg",
        alt: "Android Icon",
      },
    ],
  },
  {
    background: `case-studies03.jpg`,
    logo: `biteimg.png`,
    title: "Food On-Demand",
    description:
      "Swipe, Choose, & Bite - Discovering your next meal has never been easier.",
    link: "/case-studies/bite",
    icons: [
      {
        image:
          "https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/works/ios-ic.svg",
        alt: "Ios Icon",
      },
      {
        image:
          "https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/works/case-studies/and-ic.svg",
        alt: "Android Icon",
      },
    ],
  },
];
SwiperCore.use([Autoplay, EffectFade]);
const CaseStudyCarousel = () => {
  return (
    <Swiper
      autoHeight={true}
      fadeEffect={{
        crossFade: true,
      }}
      autoplay={{ delay: 3000, disableOnInteraction: true }}
      effect="fade"
    >
      {data.map((c) => (
        <SwiperSlide key={c.title}>
          <CaseStudy
            background={c.background}
            logo={c.logo}
            title={c.title}
            description={c.description}
            link={c.link}
            icons={c.icons}
          />
        </SwiperSlide>
      ))}
    </Swiper>
  );
};

export default CaseStudyCarousel;
