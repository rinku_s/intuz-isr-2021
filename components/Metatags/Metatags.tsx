//@ts-nocheck
import React, { useEffect, useState } from 'react'
import Head from 'next/head'
const Metatags = (props) => {
    
    const [allowScript, setAllowScript] = useState(false);
    let a;
    useEffect(() => {
        a = setTimeout(() => {
            if(!(window.location.hostname.indexOf('localhost') !== -1 || window.location.hostname.indexOf('relispace') !== -1)) {
                setAllowScript(true);
            }
        }, 1000)
        return () => {
            clearTimeout(a);
        }
    }, [])
    
    let tag = (
        <>
             {props.scriptsrc ? props.scriptsrc.map(sc=>(
                !sc.for_footer ?  <script key={sc.source} async src={sc.source}></script> : ''
               
            )) : ''}
        
            {props.scriptcontent ? props.scriptcontent.map((sc,i)=>(
                !sc.for_footer ? <script key={i} async dangerouslySetInnerHTML={{__html:sc.content}}></script> : ''
            )) : ''}
        </>
    )
    
    return (
        <Head>
           {allowScript && tag}
        </Head>
    )
}

export default Metatags
