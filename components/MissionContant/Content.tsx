//@ts-nocheck
import React from "react";
import style from './styles.module.scss';

const Content = (props) => {
    return(
        <div className={`${style.MissionContent}`}>
                <div>
                    <div className={style.leftTop}>
                        <div className={style.Block}>
                            <img src={"/static/Images/about/map_img.png"} alt='map_img' />
                            <div className={style.Info}>
                                <h3>1500+</h3>
                                <span>Projects Delivered</span>
                            </div>
                            <div className={style.Info}>
                                <h3>40+</h3>
                                <span>Client Countries</span>
                            </div>
                        </div>
                        <div className={style.Block}>
                            <img src={"/static/Images/about/cloud.svg"} alt='cloud' />
                            <div className={style.Info}>
                                <h3>50+</h3>
                                <span>Cloud Applications</span>
                            </div>
                        </div>
                    </div>
                    <div className={style.leftBottom}>
                        <div className={`${style.Block} ${style.vertical}`}>
                            <img src={"/static/Images/about/years.svg"} alt='years' />
                            <h3>9+</h3>
                            <span>Years</span>
                        </div>
                        <div className={`${style.Block} ${style.vertical}`}>
                            <img src={"/static/Images/about/downloads.svg"} alt='download' />
                            <h3>2</h3>
                            <span>Million+</span>
                            <p>App Downloads Achieved for Single Company</p>
                        </div>
                        <div className={`${style.Block} ${style.multiImgBlock}`}>
                            <div>
                                <img src={"/static/Images/about/ios.svg"} alt='ios' style={{maxWidth: '100%', height: '60px'}} />
                                <img src={"/static/Images/about/amazon.png"} alt='amazon' />                                
                            </div>
                            <div>
                                <img src={"/static/Images/about/android.svg"} alt='android' style={{maxWidth: '100%', height: '60px'}} />
                            </div>
                        </div>
                    </div>
                </div>

                <div className={`${style.Block} ${style.rightBlock}`}>
                    <div className={`${style.ImgBlock}`}>
                        <img src={"/static/Images/about/designers.svg"} alt='designers' />
                        <div className={style.Info}>
                            <h3>10+</h3>
                            <span>Creative Designers</span>
                        </div>
                    </div>
                    <div className={`${style.ImgBlock}`}>
                        <img src={"/static/Images/about/developers.svg"} alt='developers' />
                        <div className={style.Info}>
                            <h3>60+</h3>
                            <span>Skilled Developers</span>
                        </div>
                    </div>
                    <div className={`${style.ImgBlock}`}>
                        <img src={"/static/Images/about/sales.svg"} alt='sales & marketing' />
                        <div className={style.Info}>
                            <h3>20+</h3>
                            <span>Sales & Marketing</span>
                        </div>
                    </div>
                </div>
    </div>

    )
}

export default Content