import { myLoader } from "config/image-loader";
import Image from "next/image";
import React from "react";
import classes from "./styles.module.scss";
const AwardBox = (props) => {
  //a
  return (
    <div className={classes.AwardBox}>
      <Image
        loader={myLoader}
        src={props.src}
        alt={props.alt}
        layout={"fixed"}
        width={150}
        height={150}
      />
      <p>{props.desc}</p>
    </div>
  );
};

export default AwardBox;
