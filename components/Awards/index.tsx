import React from "react";
import AwardBox from "./AwardBox";

const awards = [
  {
    src: "/static/Images/awards-associations/clutch.png",
    alt: "Clutch",
    desc: "Rated as a top mobile app development company with 4.6/5 on Clutch",
  },
  {
    src: "/static/Images/awards-associations/appfututua.png",
    alt: "appfututua",
    desc: "Rated as a top app developer with 5/5 rating on AppFutura",
  },
  {
    src: "/static/Images/awards-associations/agencyspotter.png",
    alt: "agen",
    desc: "Featured by AgencySpotter with 5/5 client ratings",
  },
  {
    src: "/static/Images/awards-associations/cvmag.png",
    alt: "cv mag",
    desc: "CV Magazine Award for Best Bespoke Mobile App Development Company in California",
  },
  {
    src: "/static/Images/awards-associations/mpdaily.png",
    alt: "mpdaily",
    desc: "Ranked #4 for Top React Native Developers in 2019 by MobileAppsDaily",
  },
];

const Awards = (props) => {
  return (
    <div className="row justify-center">
      {props.awardandrecognitions.map((aw) => (
        <div className="col-md-4" key={aw.caption}>
          <AwardBox
            src={aw.image.name}
            alt={aw.caption}
            desc={aw.description}
          />
        </div>
      ))}
    </div>
  );
};

export default Awards;
