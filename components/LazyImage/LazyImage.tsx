import React from 'react';
import 'lazysizes';



const LazyImage = ( { src, srcSet, placeholder, placeholderSrcset, alt, className, style } ) => {
    return (
        <img style={style} data-src={src} src={placeholder} alt={alt} className={`lazyload ${className?className:''}`}/>
    )
}

export default LazyImage
