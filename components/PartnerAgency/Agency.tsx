//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap';
import React from "react";
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';
import ImageLoader from '../ImageLoader/ImageLoader';
import style from './styles.module.scss';

const Agency = () => {
    // setOpacity(`.left, .right, .bottom`);

    function play() {
        const tl = gsap.timeline();
        tl.fromTo('.left', 1, { x: -20, opacity: 0, ease: "power0.ease" }, { x: 0, opacity: 1, ease: "power0.ease" }, 0.5)
            .fromTo('.right', 1, { x: 20, opacity: 0, ease: "power0.ease" }, { x: 0, opacity: 1, ease: "power0.ease" }, 0.5)
            .fromTo('.bottom', 1, { y: 20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5);
        tl.play();
    }
    return (
        <>
            <div className={`${style.AgencyBlock}`}>

                <div className={`row ${style.topBlock}`}>
                    <div className={`col-md-6 col-sm-6 ${style.content} left`}>
                        <div>
                            <LazyLoad height={300} offset={300} once>
                                <ImageLoader src={cdn('agency1.png?auto=format')} alt={'Complement_agency'} />
                            </LazyLoad>
                        </div>
                        <p>Complement your in-house team</p>
                    </div>
                    <div className={`col-md-6 col-sm-6 ${style.content} right`}>
                        <div>
                            <LazyLoad height={300} offset={300} once>
                                <ImageLoader src={cdn('agency3.png?auto=format')} alt={'project_agency'} />
                            </LazyLoad>
                        </div>
                        <p>Flexibility to accommodate overload of projects</p>
                    </div>
                </div>


                <div className={`row ${style.bottomBlock}`}>
                    <div className={`col-md-4 col-sm-6 ${style.content} left`}>
                        <div>
                            <LazyLoad height={300} offset={300} once>
                                <ImageLoader src={cdn('agency2.png?auto=format')} alt={'quality_agency'} />
                            </LazyLoad>
                        </div>
                        <p>Quality & reliable teams to work on your project</p>
                    </div>
                    <div className={`col-md-4 col-sm-6 ${style.content} bottom`}>
                        <div>
                            <LazyLoad height={300} offset={300} once>
                                <ImageLoader src={cdn('agency4.png?auto=format')} alt={'completion_agency'} />
                            </LazyLoad>
                        </div>
                        <p>Rock star project support post completion</p>
                    </div>
                    <div className={`col-md-4 col-sm-6 ${style.content} right`}>
                        <div>
                            <LazyLoad height={300} offset={300} once>
                                <ImageLoader src={cdn('agency5.png?auto=format')} alt={'fast_project_agency'} />
                            </LazyLoad>
                        </div>
                        <p>Fast turnaround of projects</p>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Agency;

