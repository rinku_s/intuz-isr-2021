//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
const Secondary = (props) => {
    return (
        <h3 className={classes.Secondary}>
           {props.children} 
        </h3>
    )
}

export default Secondary
