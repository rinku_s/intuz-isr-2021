//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
const Primary = (props) => {
    return (
        <h2 className={classes.Primary}>
           {props.children} 
        </h2>
    )
}

export default Primary
