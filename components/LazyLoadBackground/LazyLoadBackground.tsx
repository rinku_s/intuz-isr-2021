//@ts-nocheck
import React, { useState } from 'react';
import VisibilitySensor from 'react-visibility-sensor';
const LazyLoadBackground = ({ offset, children }) => {
    const [visible, setVisible] = useState(false);
    let ofs = offset ? offset : { top:-10, bottom:-10 }; 
    return (
        <VisibilitySensor onChange={v => v ? setVisible(true) : ''} partialVisibility offset={ofs}>
            {children(visible)}
        </VisibilitySensor>
    )
}

export default LazyLoadBackground
