//@ts-nocheck


import Link from 'next/link';
import React from 'react';
import windowWidth from '../../hooks/windowWidth';
import Container from '../Container/Container';
import Heading from '../Heading/Heading';
import SingleBlogBlock from '../SingleBlogBlock/SingleBlogBlock';
import TopBlogBlock from '../TopBlogBlock/TopBlogBlock';
import style from './styles.module.scss';

const MultiBlogBlock = (props) => {
    if(props.blogs.length <= 0) {
        return null
    } else {
        let topBlockArray = [];
        let windowSize = windowWidth();

        if((props.blogs.length > 2 || props.blogs.length == 1) && windowSize >= 768) {
            topBlockArray[0] = props.blogs[0]
            props.blogs.shift();   
        }
        return (
            <section className={style.MultiBlogBlock}>
                <Container>
                    <Heading style={{'fontFamily' : "'Nunito Sans', sans-serif"}}>{props.title}</Heading>
                    {
                        props.link ? 
                        <Link href="/blog/category/[name]" as={`/blog/category/${props.link}`} prefetch={false}>
                            <a>View More <img src={"/static/Images/right-arrow.png"} alt='right arrow' /></a>
                        </Link> : ''
                    }
                    {   topBlockArray.length > 0 ? <TopBlogBlock {...topBlockArray[0]} /> : '' }
                    {   props.blogs.length > 0 ? <SingleBlogBlock blogs = {props.blogs} length={props.blogs.length} /> : '' }
                </Container>
            </section>
        )
    }
}

export default MultiBlogBlock