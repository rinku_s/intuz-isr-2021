

import React, { useState } from 'react';
import LinkButton from '../../components/UI/LinkButton/LinkButton';
import { cdn } from '../../config/cdn';
import BannerModel from './BannerModel';
import FormPopupButton from './FormPopupButton';

const ServicePageContent = (props) => {

    const [modalShow, setModalShow] = useState(false);
    const [activeLink, setActiveLink] = useState(null);
    const fethYoutube = (link) => {
        setActiveLink(link);
        setModalShow(!modalShow);
    }

    return (
        <>
            {props.content.title ? <h1 className={`text-[5rem] font-semibold text-white leading-normal ${props.headingClass ? props.headingClass : 'md:max-w-[65%]'}`} dangerouslySetInnerHTML={{ __html: props.content.title }}></h1> : ''}
            {props.content.subTitle ? <h3 className="text-5xl font-light leading-normal text-white md:max-w-60">{props.content.subTitle}</h3> : ''}
            {props.content.description ? <p>{props.content.description}</p> : ''}
            <div>
                {props.content.buttonLabel ? props.form ? <FormPopupButton buttonLabel={props.content.buttonLabel} form={props.form} /> : <LinkButton href={props.content.buttonLink} variation={props.content.videolink ? 'blueBannerBtn' : 'bannerBtn'} style={{ marginRight: '2rem' }}>{props.content.buttonLabel}</LinkButton> : ''}
                {props.content.videolink ?
                    <>
                        <LinkButton onClick={() => fethYoutube(props.content.videolink)} variation="bannerBtn">Watch Video<img src={cdn('video_icon_white.png')} alt="Video Icon" width='30' height='30' style={{ marginLeft: '1rem' }} /></LinkButton>
                        <BannerModel link={activeLink} show={modalShow} hide={() => setModalShow(false)} />
                    </> : ''}
            </div>
        </>
    )
}

export default ServicePageContent