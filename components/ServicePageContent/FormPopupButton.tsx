//@ts-nocheck

import React, { useState } from 'react';
import FormPopup from '../GetStartedForm/FormPopup';
import LinkButton from '../UI/LinkButton/LinkButton';

const FormPopupButton = (props) => {
    const [show, setShow] = useState(false);
    const handleClose = () => setShow(false);
    const handleShow = () => setShow(true);
    return (
        <>
            <LinkButton variation="bannerBtn" onClick={handleShow}>{props.buttonLabel}</LinkButton>
            <FormPopup
                show={show}
                onHide={handleClose}
                {...props.form}
            />
        </>
    )
}

export default FormPopupButton