//@ts-nocheck
//@ts-nocheck
import React from 'react';
import Input from '../UI/Input/Input';
const FormFields = (props) => {
    return (
        <>
            <Input
                inputContainerClass="col-md-6"
                elementConfig={{ id: "f_name", type: "text", placeholder: " " }}
                label={"First Name"}
            />
            <Input
                inputContainerClass="col-md-6"
                elementConfig={{ id: "l_name", type: "text", placeholder: " " }}
                label={"Last Name"}
            />
            <Input
                inputContainerClass="col-md-6"
                elementConfig={{ id: "email", type: "email", placeholder: " " }}
                label={"Email address"}
            />
            <Input
                inputContainerClass="col-md-6"
                elementConfig={{ id: "phone", type: "tel", placeholder: " " }}
                label={"Phone Number"}
            />
            <Input
                inputContainerClass="col-md-6"
                elementConfig={{ id: "experiance", type: "text", placeholder: " " }}
                label={"Relevant Experience"}
            />
            <Input
                inputContainerClass="col-md-6"
                elementConfig={{ id: "ctc", type: "text", placeholder: " " }}
                label={"Current CTC"}
            />
            <Input
                elementType="select"
                inputContainerClass="col-md-6"
                elementConfig={{ options: props.selectValues }}
            />

            <Input
                elementType="textarea"
                inputContainerClass="col-md-6"
                elementConfig={{ id: "message", placeholder: " " }}
                label={"Message"}
            />
            <Input
                elementType="file"
                inputContainerClass="col-md-6"
                elementConfig={{ id: "file" }}
                label={"Attach Resume"}
            />

        </>
    )
}

export default FormFields;