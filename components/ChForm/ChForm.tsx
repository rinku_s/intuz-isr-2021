//@ts-nocheck
//@ts-nocheck
import React, { Component } from 'react';
import BlueCheckBox from '../UI/BlueCheckBox/BlueCheckBox';
import FormButton from '../UI/FormButton/FormButton';
import Input from '../UI/Input/Input';
// import FormFields from '../CommonFormFields/FormFields';
import classes from './styles.module.scss';
export default class ChForm extends Component {
    render() {
        return (
            <div className={classes.Form}>
                <form className='row'>
                    <Input
                        inputContainerClass="col-md-6"
                        elementConfig={{ id: "fname", type: "text", placeholder: " " }}
                        label={"First Name"}

                    />
                    <Input
                        inputContainerClass="col-md-6"
                        elementConfig={{ id: "lname", type: "text", placeholder: " " }}
                        label={"Last Name"}
                    />
                    <Input
                        inputContainerClass={"col-md-6"}
                        elementConfig={{ id: "emailid", type: "email", placeholder: " " }}
                        label={"Email"}
                    />
                    <Input
                        inputContainerClass={"col-md-6"}
                        elementConfig={{ id: "phoneno", type: "tel", placeholder: " " }}
                        label={"Phone Number"}
                    />
                    <Input
                        elementType="textarea"
                        inputContainerClass="col-md-12"
                        elementConfig={{ id: "brief", placeholder: " " }}
                        label={"Project Brief"}
                    />
                    <p className="col-12 col-sm-4">Engagement Model</p>
                    <BlueCheckBox id="fcp"
                        containerClass="col-6 col-sm-4"
                        name="so"
                        value="Fixed Cost Project" />
                    <BlueCheckBox id="dt"
                        name="so"
                        containerClass="col-6 col-sm-4"
                        value="Dedicated Team" />
                    <FormButton
                        className="text-center mt-5"
                        btnText="Clear"
                        submitBtn="Send"
                    />
                </form>
            </div>
        )
    }
}
