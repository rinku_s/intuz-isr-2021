//@ts-nocheck




import React from 'react';
import style from './styles.module.scss';

const Content = (props) => {
    return (
        <div className={`row ${style.ChaContent}`}>
           <div className="col-md-6">
                <img src={"/static/Images/case-studies/theaterchat/third-app-icon.png"} />
                <h3>Third Party App Permission</h3>
                <p>After the R&D, we came to know that integration of third party app permission in iPhone was not suggestive as it required changes in inbuilt software configurations of the device.</p>
           </div>
           <div className="col-md-6">
                <img src={"/static/Images/case-studies/theaterchat/third-app-icon.png"} />
                <h3>Brainstorming for App Theme</h3>
                <p>There was a long discussion on various aspects of app’s theme. After the brainstorming, we and client decided to go with a dark color theme with various font sizes and colors.</p>
           </div>
        </div>
    )
}

export default Content