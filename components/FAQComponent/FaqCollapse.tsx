//@ts-nocheck
import gsap from 'gsap';
import HtmlParser from 'html-react-parser';
import React, { useState } from 'react';
import { Transition } from 'react-transition-group';
import { cdn } from '../../config/cdn';
import classes from './styles.module.module.scss';
const FaqCollapse = ({ title, content }) => {
  const [open, setOpen] = useState(false);

  function onEnter(n) {
    gsap.set(n, { transformOrigin: "0 0" })
    gsap.fromTo(n, 0.3, { scaleY: 0.1, autoAlpha: 0 }, { scaleY: 1, autoAlpha: 1 });
  }
  function onExit(n) {
    gsap.set(n, { postion: "absolute" });
    gsap.fromTo(n, 0.3, { scaleY: 1, autoAlpha: 1 }, { scaleY: 0, autoAlpha: 0 });
  }
  function onExited(n) {
  }
  function onEntering(n) {
    gsap.set(n, { display: "relative" });
  }

  return (
    <>
      <div
        className={classes.FaqCollapseBtn}
        onClick={() => setOpen(!open)}
      >
        {title}
        <span style={{ transform: open ? "rotate(0)" : "rotate(180deg)" }}>
          <img src={cdn('larr_37625c3098.png')} alt="Arrow" />
        </span>
      </div>
      <Transition timeout={200} in={open} mountOnEnter={true} unmountOnExit={true} onEnter={onEnter} onEntering={onEntering} onExit={onExit} onExited={onExited} >
        <div className={classes.FaqCollapseContent}>
          {HtmlParser(`${content}`)}
        </div>
      </Transition>
    </>
  );
}

export default FaqCollapse
