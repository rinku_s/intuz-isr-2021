import React from 'react'

const FaqSchema = ({faqs}) => {

    const fs= `
    {
        "@context": "https://schema.org",
        "@type" : "FAQPage",
        "mainEntity": [
        ${faqs.map(f=>(
            `\n{
                "@type": "Question",
                "name": "${f.question}",
                "acceptedAnswer": {
                        "@type": "Answer",
                    "text": "${f.answer}"
                }
              }`
        ))}
        
      ]
    }
    
    `
    return (
        <div>
            <script type="application/ld+json" dangerouslySetInnerHTML={{__html:fs}}></script>
        </div>
    )
}

export default FaqSchema
