import React from 'react'
import classes from './styles.module.module.scss'
import PrimaryHeading from '../Heading/Heading'
import Container from '../Container/Container'
import FaqCollapse from './FaqCollapse'
const FaqComponent = ({faqs}) => {

    
    return (
        <section className={classes.FaqComponent}>
            <Container>
                <PrimaryHeading>FAQs</PrimaryHeading>
                {faqs.map((d,i)=>(
                  <FaqCollapse key={i} title={d.question} content={d.answerHtml} />  
                ))}
            </Container>
        </section>
    )
}

export default FaqComponent
