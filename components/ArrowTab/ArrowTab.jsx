import React, { useEffect, useState } from "react";

const ArrowTab = (props) => {
  const [arr, setArr] = useState([]);

  const [index, setIndex] = useState(0);

  const [hasPrevious, setHasPrevious] = useState(false);

  const [hasNext, setHasNext] = useState(false);

  useEffect(() => {
    if (props.slice >= props.arrayData.length) {
      setHasPrevious(false);
      setHasNext(false);
      setArr(props.arrayData);
      setIndex(0);
    } else {
      let newArray = props.arrayData.slice(index, props.slice);
      setHasNext(true);
      setHasPrevious(false);
      setArr(newArray);
      setIndex(0);
    }
  }, [props.arrayData]);

  const onPrevious = () => {
    if (!hasPrevious) {
      return;
    }
    let newAra = [...props.arrayData];
    let nI = (index - 1) * props.slice;
    let sliceValue = index * props.slice;
    newAra = newAra.slice(nI, sliceValue);
    setIndex(index - 1);
    setArr(newAra);
    setHasNext(true);
    if (nI === 0) {
      setHasPrevious(false);
    }
  };

  const onNext = () => {
    if (!hasNext) {
      return;
    }
    let newAra = [...props.arrayData];
    let nI = (index + 1) * props.slice;
    let sliceValue = (index + 2) * props.slice;
    newAra = newAra.slice(nI, sliceValue);
    setIndex(index + 1);
    setArr(newAra);
    setHasPrevious(true);
    if (sliceValue >= props.arrayData.length) {
      setHasNext(false);
    }
  };

  return (
    <div className={props.className}>
      <div className={props.containerClass}>
        {arr.map((c, i) => (
          <div key={i}>{props.children(c)}</div>
        ))}
        {arr.length < props.slice &&
          [...Array(props.slice - arr.length).keys()].map((c) => (
            <div key={c}></div>
          ))}
      </div>
      <div className={props.buttonContainerClass} style={{ fontSize: "4rem" }}>
        <span
          onClick={onPrevious}
          className="inline-block"
          style={{ cursor: "pointer", marginRight: "1rem" }}
        >
          <img src={"/images/arrow-left-circle.svg"} />
        </span>
        <span
          onClick={onNext}
          className="inline-block"
          style={{ cursor: "pointer" }}
        >
          <img src={"/images/arrow-right-circle.svg"} />
        </span>
      </div>
    </div>
  );
};

export default ArrowTab;
