import { myLoader } from "config/image-loader";
import Image from "next/image";
import React from "react";

interface BackgroundImageProps {
  backImage: string;
  alt: string;
  objectPosition?: string;
  className?: string;
}

const BackgroundImage: React.FC<BackgroundImageProps> = (props) => {
  return (
    <div
      className={`absolute h-full w-full ${props.className}`}
      style={{ zIndex: -1 }}
    >
      <Image
        objectPosition={props.objectPosition}
        className="z-0"
        layout="fill"
        objectFit="cover"
        loader={myLoader}
        src={props.backImage}
        alt={props.alt}
      />
    </div>
  );
};

export default BackgroundImage;
