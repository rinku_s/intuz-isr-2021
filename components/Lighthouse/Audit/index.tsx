//@ts-nocheck
import React, { useState } from "react";
import Container from "../../Container/Container";
import LinkButton from "../../UI/LinkButton/LinkButton";
import Model from "./model";
import classes from "./styles.module.scss";
export default function index() {
  const [show, setShow] = useState(false);
  const [weburl, setweburl] = useState("");
  const handleClose = () => {
    setweburl("");
    setShow(false);
  };
  const handleShow = () => setShow(true);
  const validateurl = () => {
    var pattern = new RegExp(
      /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/
    );
    return pattern.test(weburl);
  };
  const openModel = (e) => {
    e.preventDefault();
    let validate = false;
    if (weburl !== "") {
      validate = validateurl();
    }
    if (validate) {
      handleShow();
      // setweburl("");
    }
  };
  return (
    <div>
      <div className={`${classes.audit}`}>
        <Container>
          <form>
            <div className="row w-100 justify-center m-md-n3 m-0">
              <div className={`col-12 col-md-6 ${classes.colborder}`}>
                <input
                  type="text"
                  placeholder="Enter Website URL"
                  value={weburl}
                  onChange={(e) => {
                    setweburl(e.target.value);
                  }}
                />
                <LinkButton variation={"blueSqured"} onClick={openModel}>
                  Run Audit
                </LinkButton>
              </div>
            </div>
          </form>
          <Model show={show} onHide={handleClose} weburl={weburl} />
        </Container>
      </div>
    </div>
  );
}
