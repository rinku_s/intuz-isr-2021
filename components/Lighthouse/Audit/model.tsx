//@ts-nocheck
import axios from "axios";
import { ProcessingContext } from "context/processingContext";
import { Formik } from "formik";
import Router from "next/router";
import React, { useContext, useState } from "react";
import { Modal } from "react-bootstrap";
import * as Yup from "yup";
import classes from "./styles.module.scss";
// import LinkButton from "../../UI/LinkButton/LinkButton";
const FormPopup = (props) => {
  console.log(props.weburl);
  const { processing, setProcessing } = useContext(ProcessingContext);
  const [form, setFormdata] = useState({
    f_name: "",
    l_name: "",
    email: "",
    phone: "",
    description: "",

    companyname: "",
    leadsource: "lighthouse-get-started",
    url: "",
    subject: "Lighthouse Audit Request",
  });
  const handleClickSubmit = (data) => {
    let result = data.weburl
      .replace(/^(?:https?:\/\/)?(?:www\.)?/i, "")
      .split("/")[0];
    // console.log("dflksajlkasf;jdklsfj", data, result);
    let fData = new FormData();
    fData.append("f_name", data.f_name);
    fData.append("l_name", data.l_name);
    fData.append("email", data.email);
    fData.append("phone", data.phone);
    fData.append("description", data.description);
    fData.append("leadsource", "JamStack Development - Lighthouse Page");
    fData.append("subject", `Lighthouse Audit Request for ${result} - Intuz `);
    fData.append("url", window.location.href);
    fData.append("website", data.weburl);
    fData.append("company_name", data.companyname);
    setProcessing(true);
    StartFormSubmission(fData);
  };

  const StartFormSubmission = async (data) => {
    try {
      console.log(data);
      const result = await axios.post(
        "https://strapimail-prod-aws.intuz.com/mail",
        data,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      console.log(result);
      props.onHide();
      setProcessing(false);
      Router.push(`/success/thankyou-jamstack`);
    } catch (error) {
      throw error;
      setProcessing(false);
    }
  };
  let phoneRegExp = /^(1\s?)?((\([0-9]{3}\))|[0-9]{3})[\s\-]?[\0-9]{3}[\s\-]?[0-9]{4}$/;
  return (
    <>
      <Modal
        show={props.show}
        onHide={props.onHide}
        className={classes.Modal}
        size="xl"
      >
        <Modal.Header closeButton className={classes.ModalHeader}>
          <Modal.Title className={classes.Title}>
            Get Your <span>Free</span> Website <span>Performance Audit</span>
          </Modal.Title>
        </Modal.Header>
        <Modal.Body className={classes.ModalBody}>
          <div>
            {/* <Modal.Title className={classes.Title}>Apply for job</Modal.Title> */}
            <Formik
              initialValues={{ ...form, weburl: props.weburl }}
              onSubmit={handleClickSubmit}
              validationSchema={Yup.object().shape({
                weburl: Yup.string().required("Please Enter your Website Url"),
                f_name: Yup.string().required("Please Enter your first name"),
                l_name: Yup.string().required("Please Enter your last name"),
                email: Yup.string()
                  .email()
                  .required("Please Enter your email address"),
                phone: Yup.string()
                  .required("Please Enter your phone number")
                  .matches(phoneRegExp, "Phone number is not valid"),
                companyname: Yup.string().required(
                  "Please Enter your Company name"
                ),
              })}
            >
              {(props) => {
                const {
                  values,
                  touched,
                  errors,
                  // dirty,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  setFieldValue,
                  // handleReset,
                } = props;

                return (
                  <form noValidate onSubmit={handleSubmit} className="row mt-3">
                    <div
                      className={`col-12 col-md-6 col-sm-12 col-lg-6 col-xs-12 ${classes.InputContainer}`}
                    >
                      {/* <label>First Name</label> */}
                      <input
                        value={values.f_name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="f_name"
                        className={classes.Input}
                        placeholder="First Name"
                        type="text"
                      />
                      {errors.f_name && touched.f_name ? (
                        <div
                          className="errorMsg "
                          style={{
                            fontSize: "12px",
                            textAlign: "left",
                            color: "red",
                          }}
                        >
                          {errors.f_name}
                        </div>
                      ) : null}
                    </div>
                    <div
                      className={`col-12 col-md-6 col-sm-12 col-lg-6 col-xs-12 ${classes.InputContainer}`}
                    >
                      {/* <label>Last Name</label> */}
                      <input
                        value={values.l_name}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="l_name"
                        className={classes.Input}
                        placeholder="Last Name"
                        type="text"
                      />
                      {errors.l_name && touched.l_name ? (
                        <div
                          className="errorMsg "
                          style={{
                            fontSize: "12px",
                            textAlign: "left",
                            color: "red",
                          }}
                        >
                          {errors.l_name}
                        </div>
                      ) : null}
                    </div>
                    <div
                      className={`col-12 col-md-6 col-sm-12 col-lg-6 col-xs-12 ${classes.InputContainer}`}
                    >
                      <input
                        value={values.weburl}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="weburl"
                        className={classes.Input}
                        placeholder="Website"
                        type="text"
                      />
                      {errors.weburl && touched.weburl ? (
                        <div
                          className="errorMsg "
                          style={{
                            fontSize: "12px",
                            textAlign: "left",
                            color: "red",
                          }}
                        >
                          {errors.weburl}
                        </div>
                      ) : null}
                    </div>
                    <div
                      className={`col-12 col-md-6 col-sm-12 col-lg-6 col-xs-12 ${classes.InputContainer}`}
                    >
                      {/* <label>Current/Previous Company</label> */}
                      <input
                        value={values.companyname}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="companyname"
                        className={classes.Input}
                        placeholder="Company"
                        type="text"
                      />
                      {errors.companyname && touched.companyname ? (
                        <div
                          className="errorMsg "
                          style={{
                            fontSize: "12px",
                            textAlign: "left",
                            color: "red",
                          }}
                        >
                          {errors.companyname}
                        </div>
                      ) : null}
                    </div>
                    <div
                      className={`col-12 col-md-6 col-sm-12 col-lg-6 col-xs-12 ${classes.InputContainer}`}
                    >
                      {/* <label>Mobile Number</label> */}
                      <input
                        value={values.phone}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="phone"
                        className={classes.Input}
                        placeholder="Phone No"
                        type="tel"
                      />
                      {errors.phone && touched.phone ? (
                        <div
                          className="errorMsg "
                          style={{
                            fontSize: "12px",
                            textAlign: "left",
                            color: "red",
                          }}
                        >
                          {errors.phone}
                        </div>
                      ) : null}
                    </div>
                    <div
                      className={`col-12 col-md-6 col-sm-12 col-lg-6 col-xs-12 ${classes.InputContainer}`}
                    >
                      {/* <label>Email Address</label> */}
                      <input
                        value={values.email}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="email"
                        className={classes.Input}
                        placeholder="Email Address"
                        type="email"
                      />
                      {errors.email && touched.email ? (
                        <div
                          className="errorMsg "
                          style={{
                            fontSize: "12px",
                            textAlign: "left",
                            color: "red",
                          }}
                        >
                          {errors.email}
                        </div>
                      ) : null}
                    </div>
                    <div
                      className={`col-12 col-md-12 mt-2 ${classes.InputContainer}`}
                    >
                      <textarea
                        value={values.description}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="description"
                        className={classes.Input}
                        placeholder="Message"
                      />
                    </div>
                    <div className={`col-12 text-center mt-4`}>
                      <button
                        type="submit"
                        className={classes.applyButton}
                        disabled={processing}
                      >
                        Run Audit
                      </button>
                    </div>
                  </form>
                );
              }}
            </Formik>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};
export default FormPopup;
