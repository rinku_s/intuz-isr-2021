//@ts-nocheck
import Container from "components/Container/Container";
import React from "react";
import classes from "./styles.module.scss";
import Image from "next/image";
import { myLoader } from "config/image-loader";
const Casestudy = ({
  id,
  image,
  title,
  line,
  isImage,
  img,
  img_mobile,
  iconbox,
}) => {
  let content = "";

  switch (id) {
    case "overview":
      content = (
        <div className={classes.overview}>
          <div className={`text-center mt-5`}>
            <Image
              layout="intrinsic"
              loader={myLoader}
              src={img}
              alt="Poor Web Architecture"
              height="547"
              width="1113"
            />
            {/* <img
              className="img-fluid lazyload inline-block md:hidden"
              data-src={cdn(`${img_mobile}`)}
              alt="Strapi"
            />
             */}
          </div>
          <div className={classes.Content}>
            <h3 className={classes.boldLarge}>{title}</h3>
            {line.map((data, index) => {
              return (
                <p className={classes.smallLight} key={index}>
                  {data}
                </p>
              );
            })}
          </div>
        </div>
      );
      break;
    case "techstack":
      content = (
        <div className={`${classes.techstack} row`}>
          {iconbox.map((icon, i) => (
            <div
              className="col-6 col-md-4 col-lg-3 text-center"
              data-cl="icon"
              key={i}
            >
              <div className={classes.iconImage}>
                <Image
                  layout="intrinsic"
                  loader={myLoader}
                  src={icon.img}
                  alt={icon.title}
                  height="50"
                  width="50"
                />
              </div>
              <h4>{icon.title}</h4>
              <p>{icon.description}</p>
            </div>
          ))}
          <div className={`col-12 ${classes.Content}`}>
            {line.map((l, i) => (
              <p className={classes.boldLarge} key={i}>
                {l}
              </p>
            ))}
          </div>
        </div>
      );
      break;
    case "outcome":
      content = (
        <div className={`${classes.outcome}`}>
          <div>
            <div data-svg>
              <Image
                layout="intrinsic"
                loader={myLoader}
                src={img}
                alt={title}
                height="742"
                width="1280"
              />
              {/* <img
                data-src={cdn(img_mobile)}
                className="d-inline md:hidden lazyload"
              /> */}
            </div>
          </div>
          <div className={`${classes.Content}`}>
            <h3 className={classes.boldLarge}>{title}</h3>
            {line.map((l, i) => (
              <p className={classes.smallLight} key={i}>
                {l}
              </p>
            ))}
          </div>
        </div>
      );
    default:
      break;
  }

  return (
    <div className={classes.Casestudy}>
      <Container>{content}</Container>
    </div>
  );
};

export default Casestudy;
