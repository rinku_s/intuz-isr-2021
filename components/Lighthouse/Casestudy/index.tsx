//@ts-nocheck
//@ts-nocheck
import React, { useState } from "react";
import { Carousel } from "react-bootstrap";
import BottomNavIcon from "./BottomNav";
import NewWhyComponent from "./casestudy";
// import WhyComponentMobile from "./casestudymobile";
// import { cdn } from "config/cdn";
// import VisibilitySensor from "react-visibility-sensor";
import Data from "./data.json";
import classes from "./styles.module.scss";
const CaseStudy = ({ why_component, time }) => {
  const [current, setActiveIndex] = useState(0);
  const handleSelect = (selectedIndex, e) => {
    setActiveIndex(selectedIndex);
  };
  return (
    // <MediaContextProvider>
    <div className={`${classes.Main} feature`}>
      <div className={classes.BottomNav}>
        {Data.navigation.map((w, i) => (
          <BottomNavIcon
            data={w}
            active={current === i}
            key={w}
            setIndex={() => setActiveIndex(i)}
          />
        ))}
      </div>
      {/* <Media greaterThanOrEqual="sm"> */}
      <Carousel
        fade
        controls={false}
        indicators={false}
        activeIndex={current}
        onSelect={handleSelect}
        className={classes.Carousel}
      >
        {Data.slide.map((data, i) => {
          return (
            <Carousel.Item key={i} style={{ display: 'flex', alignItems: 'center' }}>
              <NewWhyComponent {...data} index={i} />
            </Carousel.Item>
          );
        })}
      </Carousel>
      {/* </Media> */}
      {/* <Media lessThan="sm">
          <Carousel
            touch={true}
            fade
            controls={false}
            indicators={false}
            activeIndex={current}
            onSelect={handleSelect}
            className={classes.Carousel}
          >
            {why_component.map((why, i) => (
              <Carousel.Item key={i}>
                <WhyComponentMobile {...why} index={i} />
              </Carousel.Item>
            ))}
          </Carousel>
        </Media> */}
    </div>
    // </MediaContextProvider>
  );
};

export default CaseStudy;
