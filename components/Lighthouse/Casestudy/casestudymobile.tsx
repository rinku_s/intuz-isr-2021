//@ts-nocheck
import { cdn } from "config/cdn";
import React from "react";
import classes from "./styles.module.scss";
const WhyComponentMobile = ({
  image,
  title,
  description,
  active,
  mobile_background,
  background_color,
  index,
}) => {
  console.log(background_color);
  return (
    <div
      className={`${classes.WhyComponentMobile} flex flex-col justify-center text-center items-center py-2`}
      style={{ backgroundColor: `${background_color}` }}
    >
      <div className="col-12 p-0">
        <img
          data-src={cdn(mobile_background?.name + "?auto=format,compress")}
          alt={title}
          className="img-fluid lazyload"
        />
      </div>
      <div className={`${classes.Content} col-12 px-4`}>
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
};

export default WhyComponentMobile;
