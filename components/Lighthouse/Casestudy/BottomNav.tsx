//@ts-nocheck
import React from "react";
import { cdn } from "../../../config/cdn";
import classes from "./styles.module.scss";

const BottomNavIcon = ({ data, icon, active, setIndex }) => {
  return (
    <div
      className={classes.BottomNavIcon + (active ? " " + classes.active : "")}
      onClick={setIndex}
    >
      <p>{data}</p>
    </div>
  );
};

export default BottomNavIcon;
