//@ts-nocheck
import classes from "./styles.module.scss";
import { cdn } from "config/cdn";
import SVG from "react-inlinesvg";
const Jamstack = () => {
  return (
    <div>
      <SVG
        className={`img-fluid`}
        src={cdn("Group_384_d841e7aaa0.svg")}
      />
      {/* <p className="mt-5 firstLabel">Browser</p>
      <div
        className={`${classes.browser} `}
        style={{
          backgroundImage: `url(${cdn(
            "Rectangle_48_7cff160cbf.svg" + "?auto=format,compress"
          )})`,
        }}
      >
        <img
          className="img-fluid lazyload"
          data-src={cdn(`Group_c971a36fdd.png`)}
          alt="Google Chrome"
        />

        <img
          className="img-fluid lazyload"
          data-src={cdn(`logos_safari_706e2d9bc8.png`)}
          alt="Safari"
        />

        <img
          className="img-fluid lazyload"
          data-src={cdn(`Mozilla_Firefox_logo_2013_1_99b7d6a904.png`)}
          alt="Mozila Firefox"
        />
      </div>
      <div className="row">
        <div
          className={`col-6 ${classes.jasmstackleft}`}
          style={{
            backgroundImage: `url(${cdn(
              "Rectangle_49_32fadbbda7.png" + "?auto=format,compress"
            )})`,
          }}
        >
          <div>
            <p>CDN/WAF/File Storage</p>
            <div className="row">
              <div className={`col `}>
                <div>
                  <img
                    className={` img-fluid lazyload`}
                    data-src={cdn(`ion_logo_vercel_46e11b1abf.png`)}
                    alt="Vercel"
                  />
                </div>
                <span>Vercel</span>
              </div>
              <div className={`col`}>
                <div>
                  <img
                    className={` img-fluid lazyload`}
                    data-src={cdn(`logos_aws_cloudfront_43a4b2c99e.png`)}
                    alt="Vercel"
                  />
                </div>
                <span>CloudFront</span>
              </div>
              <div className={`col`}>
                <div>
                  <img
                    className={` img-fluid lazyload`}
                    data-src={cdn(`logos_aws_s3_cd8e2d51e4.png`)}
                    alt="Vercel"
                  />
                </div>
                <span>S3</span>
              </div>
            </div>
          </div>
        </div>
        <div
          className={`col-6 ${classes.jasmstackright}`}
          style={{
            backgroundImage: `url(${cdn(
              "Rectangle_49_1_84c0798437.png" + "?auto=format,compress"
            )})`,
          }}
        >
          <div>
            <p>Micro Services</p>
            <div className="row">
              <div className={`col ${classes.colpad}`}>
                <div>
                  <img
                    className={` img-fluid lazyload`}
                    data-src={cdn(`ic_outline_shopping_cart_9472ca4073.png`)}
                    alt="Vercel"
                  />
                </div>
              </div>
              <div className={`col ${classes.colpad}`}>
                <div>
                  <img
                    className={` img-fluid lazyload`}
                    data-src={cdn(`fa_credit_card_987a3cd1aa.png`)}
                    alt="Vercel"
                  />
                </div>
              </div>
              <div className={`col ${classes.colpad}`}>
                <div>
                  <img
                    className={` img-fluid lazyload`}
                    data-src={cdn(`mdi_api_88d46d3700.png`)}
                    alt="Vercel"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        className={`${classes.traditnalmiddel} `}
        style={{
          backgroundImage: `url(${cdn(
            "Rectangle_47_4814b97fdf.png" + "?auto=format,compress"
          )})`,
        }}
      >
        <div className={`${classes.frontend}`}>
          <p className="">Static Export</p>
          <div className="row">
            <div className="col-3 p-0 icons">
              <div className={` ${classes.traditnolimg}`}>
                <img
                  className={`img-fluid lazyload pt-5`}
                  data-src={cdn(`teenyicons_nextjs_outline_03da2189a4.png`)}
                  alt="Next"
                />
              </div>
              <span>Next</span>
            </div>
            <div className="col-3 p-0 icons">
              <div className={` ${classes.traditnolimg}`}>
                <img
                  className={`img-fluid lazyload pt-5`}
                  data-src={cdn(`logos_gatsby_4a0deb0f5b.png`)}
                  alt="Gatsby"
                />
              </div>
              <span>Gatsby</span>
            </div>
            <div className="col-3 p-0 icons">
              <div className={` ${classes.traditnolimg}`}>
                <img
                  className={`img-fluid lazyload pt-4`}
                  style={{ marginTop: "15px" }}
                  data-src={cdn(`vscode_icons_file_type_nuxt_3f6609af8c.png`)}
                  alt="Nuxt"
                />
              </div>
              <span>Nuxt</span>
            </div>
            <div className="col-3 p-0 icons">
              <div className={` ${classes.traditnolimg}`}>
                <img
                  className={`img-fluid lazyload`}
                  data-src={cdn(`logos_netlify_9baaed3999.png`)}
                  alt="Netlify"
                  style={{ marginTop: "24px" }}
                />
              </div>
              <span>Netlify</span>
            </div>
          </div>
        </div>
        <img
          className={`img-fluid lazyload mt-4`}
          data-src={cdn(`Line_31_618397f533.png`)}
          alt="Javascript"
        />
        <div className={`${classes.backend}`}>
          <p className="">Serverside Render</p>
          <div className="row">
            <div className="col p-0 icons">
              <div className={` ${classes.traditnolimg}`}>
                <img
                  className={`img-fluid lazyload`}
                  // style={{ marginTop: "15px" }}
                  data-src={cdn(`logos_react_2466875fa8.png`)}
                  alt="css"
                />
              </div>
              <span>React</span>
            </div>
            <div className="col p-0 icons">
              <div className={` ${classes.traditnolimg}`}>
                <img
                  className={`img-fluid lazyload`}
                  data-src={cdn(`ri_vuejs_fill_e5b8c85c29.png`)}
                  alt="Javascript"
                  // style={{ marginTop: "24px" }}
                />
              </div>
              <span>VueJs</span>
            </div>
          </div>
        </div>
      </div>
      <div
        className={`${classes.headless} `}
        style={{
          backgroundImage: `url(${cdn(
            "Rectangle_48_e568aa5fe8.png" + "?auto=format,compress"
          )})`,
        }}
      >
        <img
          className={`${classes.stripe} img-fluid lazyload`}
          data-src={cdn(`Group_128_1_277cc9f5f7.png`)}
          alt="Stripe"
        />
        <img
          className={`${classes.pricmic} img-fluid lazyload`}
          data-src={cdn(`logos_prismic_adc6bd0849.png`)}
          alt="Pricmic"
        />
        <img
          className={`${classes.contentful} img-fluid lazyload`}
          data-src={cdn(
            `42300_1590787413_contentful_logo_wordmark_dark_1b5def_1_f306839567.png`
          )}
          alt="contentful"
        />
      </div>
      <p className="finalLabel">Headless CMS &#123;API&#125;</p> */}
    </div>
  );
};
export default Jamstack;
