//@ts-nocheck
import classes from "./styles.module.scss";
import { cdn } from "config/cdn";
import SVG from "react-inlinesvg";
const TraditnalApproch = () => {
  return (
    <div>
        <SVG
        className={`img-fluid`}
        src={cdn("Group_383_1_0cadad38ac.svg")}
      />
      {/* <p className="mt-5 firstLabel">Browser</p>
      <div
        className={`${classes.browser} `}
        style={{
          backgroundImage: `url(${cdn(
            "Rectangle_48_7cff160cbf.svg" + "?auto=format,compress"
          )})`,
        }}
      >
        <img
          className="img-fluid lazyload"
          data-src={cdn(`Group_c971a36fdd.png`)}
          alt="Google Chrome"
        />

        <img
          className="img-fluid lazyload"
          data-src={cdn(`logos_safari_706e2d9bc8.png`)}
          alt="Safari"
        />

        <img
          className="img-fluid lazyload"
          data-src={cdn(`Mozilla_Firefox_logo_2013_1_99b7d6a904.png`)}
          alt="Mozila Firefox"
        />
      </div>
      <div
        className={`${classes.traditonal} `}
        style={{
          backgroundImage: `url(${cdn(
            "Rectangle_47_4814b97fdf.png" + "?auto=format,compress"
          )})`,
        }}
      >
        <div className={`${classes.frontend}`}>
          <p className="">Front End</p>
          <div className="row">
            <div className="col p-0 img-block">
              <div className={` ${classes.traditnolimg}`}>
                <img
                  className={`img-fluid lazyload`}
                  data-src={cdn(`logos_html_5_4283f5c4cb.png`)}
                  alt="Html"
                />
              </div>
              <span>HTML</span>
            </div>
            <div className="col p-0 img-block">
              <div className={` ${classes.traditnolimg}`}>
                <img
                  className={`img-fluid lazyload`}
                  style={{ marginTop: "15px" }}
                  data-src={cdn(`vscode_icons_file_type_css_8786102ada.png`)}
                  alt="css"
                />
              </div>
              <span>CSS</span>
            </div>
            <div className="col p-0 img-block">
              <div className={` ${classes.traditnolimg}`}>
                <img
                  className={`img-fluid lazyload`}
                  data-src={cdn(`logos_javascript_e967933dea.png`)}
                  alt="Javascript"
                  style={{ marginTop: "24px" }}
                />
              </div>
              <span>JavaScript</span>
            </div>
          </div>
        </div>
        <img
          className={`img-fluid mt-4 lazyload`}
          data-src={cdn(`Line_31_618397f533.png`)}
          alt="Javascript"
        />
        <div className={`${classes.backend}`}>
          <p className="">Backend</p>
          <div className="row">
            <div className="col p-0 img-block">
              <div className={` ${classes.traditnolimg}`}>
                <img
                  className={`img-fluid lazyload`}
                  style={{ width: "100px", marginTop: "10px" }}
                  data-src={cdn(`vscode_icons_file_type_php_1e8dca3b55.png`)}
                  alt="Html"
                />
              </div>
              <span>PHP</span>
            </div>
            <div className="col p-0 img-block">
              <div className={` ${classes.traditnolimg}`}>
                <img
                  className={`img-fluid lazyload`}
                  // style={{ marginTop: "15px" }}
                  data-src={cdn(`vscode_icons_file_type_node_2b00a2751a.png`)}
                  alt="css"
                />
              </div>
              <span>Node</span>
            </div>
            <div className="col p-0 img-block">
              <div className={` ${classes.traditnolimg}`}>
                <img
                  className={`img-fluid lazyload`}
                  data-src={cdn(`logos_java_ce4263d6a2.png`)}
                  alt="Javascript"
                  // style={{ marginTop: "24px" }}
                />
              </div>
              <span>Java</span>
            </div>
          </div>
        </div>
      </div>
      <div
        className={`${classes.database} `}
        style={{
          backgroundImage: `url(${cdn(
            "Rectangle_48_e568aa5fe8.png" + "?auto=format,compress"
          )})`,
        }}
      >
        <img
          className={`${classes.mysql} img-fluid lazyload`}
          data-src={cdn(`simple_icons_mysql_c1c93460f3.png`)}
          alt="Mysql"
        />
        <img
          className={`${classes.mongodb} img-fluid lazyload`}
          data-src={cdn(`logos_mongodb_89aa271aec.png`)}
          alt="Mongo Db"
        />
      </div>
      <p className="finalLabel">Database</p> */}
    </div>
  );
};
export default TraditnalApproch;
