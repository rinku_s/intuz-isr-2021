//@ts-nocheck
//@ts-nocheck
import { cdn } from "config/cdn";
import { Media, MediaContextProvider } from "config/responsiveQuery";
import gsap from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import React, { useEffect, useState } from "react";
import Container from "../../Container/Container";
import BottomNavIcon from "../Casestudy/BottomNav";
import Jamstack from "./jamstack";
import classes from "./styles.module.scss";
import TraditnalApproch from "./traditnal";
import Image from "next/image";
import { myLoader } from "config/image-loader";
const Approch = () => {
  const [activeTab, setActiveTab] = useState("traditional");

  useEffect(() => {
    ScrollTrigger.matchMedia({
      "(min-width:768px)": function () {
        const tl = gsap.timeline({
          scrollTrigger: {
            trigger: `.${classes.approch}`,
            start: "top 50%",
            end: "bottom 65%",
            // markers:true,
            once: true,
            // pin:true,
            scrub: 3,
          },
        });

        tl.from(`.${classes.approch} .firstLabel`, {
          y: -10,
          autoAlpha: 0,
          duration: 0.3,
          ease: "power2.out",
        })
          .from(`.${classes.browser}`, {
            y: -10,
            autoAlpha: 0,
            duration: 0.3,
            ease: "power2.out",
          })
          .from(`.${classes.browser} img`, {
            x: 10,
            autoAlpha: 0,
            stagger: 0.14,
            ease: "power2.out",
          })
          .from(`.${classes.traditonal}`, {
            x: -10,
            autoAlpha: 0,
            ease: "power2.out",
          })
          .from(`.${classes.approch} .img-block`, {
            y: -10,
            autoAlpha: 0,
            ease: "power2.out",
          })
          .from(`.${classes.jasmstackleft}`, {
            y: -10,
            autoAlpha: 0,
            ease: "power2.out",
          })
          .from(`.${classes.jasmstackright}`, {
            y: 10,
            autoAlpha: 0,
            ease: "power2.out",
          })
          .from(`.${classes.traditnalmiddel}`, {
            y: 10,
            autoAlpha: 0,
            ease: "power2.out",
          })
          .from(`.${classes.frontend} .icons`, {
            x: 10,
            autoAlpha: 0,
            stagger: 0.15,
            ease: "power2.out",
          })
          .from(`.${classes.backend} .icons`, {
            x: 10,
            autoAlpha: 0,
            stagger: 0.15,
            ease: "power2.out",
          })
          .from(`.${classes.database}`, {
            y: 10,
            autoAlpha: 0,
            ease: "power2.out",
          })
          .from(
            `.${classes.headless}`,
            { x: 10, autoAlpha: 0, stagger: 0.2, ease: "power2.out" },
            "-=0.2"
          )
          .from(`.${classes.database} img`, {
            x: 10,
            autoAlpha: 0,
            stagger: 0.2,
            ease: "power2.out",
          })
          .from(`.${classes.headless} img`, {
            x: 10,
            autoAlpha: 0,
            stagger: 0.2,
            ease: "power2.out",
          })
          .from(`.${classes.approch} .finalLabel`, {
            y: -10,
            autoAlpha: 0,
            ease: "power2.out",
            onComplete: () => {
              if (tl.scrollTrigger) {
                tl.scrollTrigger.disable();
              }
            },
          });
      },
    });

    return () => {};
  }, []);

  return (
    <MediaContextProvider>
      <div className={`${classes.approch}`}>
        <Container>
          <Media greaterThanOrEqual="sm">
            <div className="row">
              <div className="col-6">
                <h4>Traditional Approach</h4>
                <p className="mt-5 firstLabel">Browser</p>
                <div
                  className={`${classes.browser} `}
                  style={{
                    backgroundImage: `url(${cdn(
                      "Rectangle_48_7cff160cbf.svg" + "?auto=format,compress"
                    )})`,
                  }}
                >
                  <img
                    className="img-fluid lazyload"
                    data-src={cdn(`Group_c971a36fdd.png?auto=format,compress`)}
                    alt="Google Chrome"
                  />

                  <img
                    className="img-fluid lazyload"
                    data-src={cdn(
                      `logos_safari_706e2d9bc8.png?auto=format,compress`
                    )}
                    alt="Safari"
                  />

                  <img
                    className="img-fluid lazyload"
                    data-src={cdn(
                      `Mozilla_Firefox_logo_2013_1_99b7d6a904.png?auto=format,compress`
                    )}
                    alt="Mozila Firefox"
                  />
                </div>
                <div
                  className={`${classes.traditonal} `}
                  style={{
                    backgroundImage: `url(${cdn(
                      "Rectangle_47_4814b97fdf.png?auto=format,compress" +
                        "?auto=format,compress"
                    )})`,
                  }}
                >
                  <div className={`${classes.frontend}`}>
                    <p className="">Front End</p>
                    <div className="row">
                      <div className="col p-0 img-block">
                        <div className={` ${classes.traditnolimg}`}>
                          <img
                            className={"img-fluid lazyload"}
                            data-src={cdn(
                              `logos_html_5_4283f5c4cb.png?auto=format,compress`
                            )}
                            alt="Html"
                          />
                        </div>
                        <span>HTML</span>
                      </div>
                      <div className="col p-0 img-block">
                        <div className={` ${classes.traditnolimg}`}>
                          <img
                            className={"img-fluid lazyload"}
                            style={{ marginTop: "15px" }}
                            data-src={cdn(
                              `vscode_icons_file_type_css_8786102ada.png?auto=format,compress`
                            )}
                            alt="css"
                          />
                        </div>
                        <span>CSS</span>
                      </div>
                      <div className="col p-0 img-block">
                        <div className={` ${classes.traditnolimg}`}>
                          <img
                            className={"img-fluid lazyload"}
                            data-src={cdn(
                              `logos_javascript_e967933dea.png?auto=format,compress`
                            )}
                            alt="Javascript"
                            style={{ marginTop: "24px" }}
                          />
                        </div>
                        <span>JavaScript</span>
                      </div>
                    </div>
                  </div>
                  <img
                    className={`img-fluid  lazyload mt-4`}
                    data-src={cdn(
                      `Line_31_618397f533.png?auto=format,compress`
                    )}
                    alt="Javascript"
                  />
                  <div className={`${classes.backend}`}>
                    <p className="">Backend</p>
                    <div className="row">
                      <div className="col p-0 img-block">
                        <div className={` ${classes.traditnolimg}`}>
                          <img
                            className={"img-fluid lazyload"}
                            style={{ width: "100px", marginTop: "10px" }}
                            data-src={cdn(
                              `vscode_icons_file_type_php_1e8dca3b55.png?auto=format,compress`
                            )}
                            alt="Html"
                          />
                        </div>
                        <span>PHP</span>
                      </div>
                      <div className="col p-0 img-block">
                        <div className={` ${classes.traditnolimg}`}>
                          <img
                            className={"img-fluid lazyload"}
                            // style={{ marginTop: "15px" }}
                            data-src={cdn(
                              `vscode_icons_file_type_node_2b00a2751a.png?auto=format,compress`
                            )}
                            alt="css"
                          />
                        </div>
                        <span>Node</span>
                      </div>
                      <div className="col p-0 img-block">
                        <div className={` ${classes.traditnolimg}`}>
                          <img
                            className={"img-fluid lazyload"}
                            data-src={cdn(
                              `logos_java_ce4263d6a2.png?auto=format,compress`
                            )}
                            alt="Javascript"
                            // style={{ marginTop: "24px" }}
                          />
                        </div>
                        <span>Java</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  className={`${classes.database} `}
                  style={{
                    backgroundImage: `url(${cdn(
                      "Rectangle_48_e568aa5fe8.png?auto=format,compress" +
                        "?auto=format,compress"
                    )})`,
                  }}
                >
                  <img
                    className={`${classes.mysql} img-fluid lazyload`}
                    data-src={cdn(
                      `simple_icons_mysql_c1c93460f3.png?auto=format,compress`
                    )}
                    alt="Mysql"
                  />
                  <img
                    className={`${classes.mongodb} img-fluid lazyload`}
                    data-src={cdn(
                      `logos_mongodb_89aa271aec.png?auto=format,compress`
                    )}
                    alt="Mongo Db"
                  />
                </div>
                <p className="finalLabel">Database</p>
              </div>
              <div className="col-6">
                <h4>JAMStack Approach</h4>
                <p className="mt-5 firstLabel">Browser</p>
                <div
                  className={`${classes.browser} `}
                  style={{
                    backgroundImage: `url(${cdn(
                      "Rectangle_48_7cff160cbf.svg" + "?auto=format,compress"
                    )})`,
                  }}
                >
                  <img
                    className="img-fluid lazyload"
                    data-src={cdn(`Group_c971a36fdd.png?auto=format,compress`)}
                    alt="Google Chrome"
                  />

                  <img
                    className="img-fluid lazyload"
                    data-src={cdn(
                      `logos_safari_706e2d9bc8.png?auto=format,compress`
                    )}
                    alt="Safari"
                  />

                  <img
                    className="img-fluid lazyload"
                    data-src={cdn(
                      `Mozilla_Firefox_logo_2013_1_99b7d6a904.png?auto=format,compress`
                    )}
                    alt="Mozila Firefox"
                  />
                </div>
                <div className="row">
                  <div
                    className={`col-6 ${classes.jasmstackleft}`}
                    style={{
                      backgroundImage: `url(${cdn(
                        "Rectangle_49_32fadbbda7.png?auto=format,compress" +
                          "?auto=format,compress"
                      )})`,
                    }}
                  >
                    <div>
                      <p>CDN/WAF/File Storage</p>
                      <div className="row">
                        <div className={`col`}>
                          <div>
                            <img
                              className={` img-fluid lazyload`}
                              data-src={cdn(
                                `ion_logo_vercel_46e11b1abf.png?auto=format,compress`
                              )}
                              alt="Vercel"
                            />
                          </div>
                          <span>Vercel</span>
                        </div>
                        <div className={`col`}>
                          <div>
                            <img
                              className={` img-fluid lazyload`}
                              data-src={cdn(
                                `logos_aws_cloudfront_43a4b2c99e.png?auto=format,compress`
                              )}
                              alt="Vercel"
                            />
                          </div>
                          <span>CloudFront</span>
                        </div>
                        <div className={`col`}>
                          <div>
                            <img
                              className={` img-fluid lazyload`}
                              data-src={cdn(
                                `logos_aws_s3_cd8e2d51e4.png?auto=format,compress`
                              )}
                              alt="Vercel"
                            />
                          </div>
                          <span>S3</span>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div
                    className={`col-6 ${classes.jasmstackright}`}
                    style={{
                      backgroundImage: `url(${cdn(
                        "Rectangle_49_1_84c0798437.png?auto=format,compress" +
                          "?auto=format,compress"
                      )})`,
                    }}
                  >
                    <div>
                      <p>Micro Services</p>
                      <div className="row">
                        <div className={`col`}>
                          <div>
                            <img
                              className={` img-fluid lazyload`}
                              data-src={cdn(
                                `ic_outline_shopping_cart_9472ca4073.png?auto=format,compress`
                              )}
                              alt="Vercel"
                            />
                          </div>
                        </div>
                        <div className={`col`}>
                          <div>
                            <img
                              className={` img-fluid lazyload`}
                              data-src={cdn(
                                `fa_credit_card_987a3cd1aa.png?auto=format,compress`
                              )}
                              alt="Vercel"
                            />
                          </div>
                        </div>
                        <div className={`col`}>
                          <div>
                            <img
                              className={` img-fluid lazyload`}
                              data-src={cdn(
                                `mdi_api_88d46d3700.png?auto=format,compress`
                              )}
                              alt="Vercel"
                            />
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  className={`${classes.traditnalmiddel} `}
                  style={{
                    backgroundImage: `url(${cdn(
                      "Rectangle_47_4814b97fdf.png?auto=format,compress" +
                        "?auto=format,compress"
                    )})`,
                  }}
                >
                  <div className={`${classes.frontend}`}>
                    <p className="">Static Export</p>
                    <div className="row">
                      <div className="col-3 p-0 icons">
                        <div className={` ${classes.traditnolimg}`}>
                          <img
                            className={`img-fluid  lazyload pt-5`}
                            data-src={cdn(
                              `teenyicons_nextjs_outline_03da2189a4.png?auto=format,compress`
                            )}
                            alt="Next"
                          />
                        </div>
                        <span>Next</span>
                      </div>
                      <div className="col-3 p-0 icons">
                        <div className={` ${classes.traditnolimg}`}>
                          <img
                            className={`img-fluid  lazyload pt-5`}
                            data-src={cdn(
                              `logos_gatsby_4a0deb0f5b.png?auto=format,compress`
                            )}
                            alt="Gatsby"
                          />
                        </div>
                        <span>Gatsby</span>
                      </div>
                      <div className="col-3 p-0 icons">
                        <div className={` ${classes.traditnolimg}`}>
                          <img
                            className={`img-fluid  lazyload pt-4`}
                            style={{ marginTop: "15px" }}
                            data-src={cdn(
                              `vscode_icons_file_type_nuxt_3f6609af8c.png?auto=format,compress`
                            )}
                            alt="Nuxt"
                          />
                        </div>
                        <span>Nuxt</span>
                      </div>
                      <div className="col-3 p-0 icons">
                        <div className={` ${classes.traditnolimg}`}>
                          <img
                            className={"img-fluid lazyload"}
                            data-src={cdn(
                              `logos_netlify_9baaed3999.png?auto=format,compress`
                            )}
                            alt="Netlify"
                            style={{ marginTop: "24px" }}
                          />
                        </div>
                        <span>Netlify</span>
                      </div>
                    </div>
                  </div>
                  <img
                    className={`img-fluid mt-4 lazyload`}
                    data-src={cdn(
                      `Line_31_618397f533.png?auto=format,compress`
                    )}
                    alt="Javascript"
                  />
                  <div className={`${classes.backend}`}>
                    <p className="">Serverside Render</p>
                    <div className="row">
                      <div className="col p-0 icons">
                        <div className={` ${classes.traditnolimg}`}>
                          <img
                            className={"img-fluid lazyload"}
                            // style={{ marginTop: "15px" }}
                            data-src={cdn(
                              `logos_react_2466875fa8.png?auto=format,compress`
                            )}
                            alt="css"
                          />
                        </div>
                        <span>React</span>
                      </div>
                      <div className="col p-0 icons">
                        <div className={` ${classes.traditnolimg}`}>
                          <img
                            className={"img-fluid lazyload"}
                            data-src={cdn(
                              `ri_vuejs_fill_e5b8c85c29.png?auto=format,compress`
                            )}
                            alt="Javascript"
                            // style={{ marginTop: "24px" }}
                          />
                        </div>
                        <span>VueJs</span>
                      </div>
                    </div>
                  </div>
                </div>
                <div
                  className={`${classes.headless} `}
                  style={{
                    backgroundImage: `url(${cdn(
                      "Rectangle_48_e568aa5fe8.png?auto=format,compress" +
                        "?auto=format,compress"
                    )})`,
                  }}
                >
                  <img
                    className={`${classes.stripe} img-fluid lazyload`}
                    data-src={cdn(
                      `Group_128_1_277cc9f5f7.png?auto=format,compress`
                    )}
                    alt="Stripe"
                  />
                  <img
                    className={`${classes.pricmic} img-fluid lazyload`}
                    data-src={cdn(
                      `logos_prismic_adc6bd0849.png?auto=format,compress`
                    )}
                    alt="Pricmic"
                  />
                  <img
                    className={`${classes.contentful} img-fluid lazyload`}
                    data-src={cdn(
                      `42300_1590787413_contentful_logo_wordmark_dark_1b5def_1_f306839567.png?auto=format,compress`
                    )}
                    alt="contentful"
                  />
                </div>
                <p className="finalLabel">Headless CMS &#123;API&#125;</p>
              </div>
            </div>
          </Media>
          <Media lessThan="sm">
            <div>
              <div className="flex">
                <BottomNavIcon
                  data={"Traditional Approach"}
                  active={activeTab === "traditional"}
                  setIndex={() => setActiveTab("traditional")}
                />
                <BottomNavIcon
                  data={"JAMStack Approach"}
                  active={activeTab === "jamstack"}
                  setIndex={() => setActiveTab("jamstack")}
                />
              </div>
              {activeTab == "traditional" ? <TraditnalApproch /> : <Jamstack />}
            </div>
          </Media>
        </Container>
      </div>
    </MediaContextProvider>
  );
};
export default Approch;
