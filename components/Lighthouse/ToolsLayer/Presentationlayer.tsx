//@ts-nocheck
import React, { useEffect } from "react";
import classes from "./styles.module.scss";
import { cdn } from "../../../config/cdn";
import Container from "../../Container/Container";
import { scrollAnimation } from "hooks/scrollAnimation";
import gsap from "gsap";
import Image from "next/image";
import { myLoader } from "config/image-loader";
const PresantationLayer = () => {
  useEffect(() => {
    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: "#presentation",
        start: "top center",
        once: true,
      },
    });
    tl.fade("#presentation .first")
      .from(`#presentation .${classes.box}`, {
        y: 10,
        scale: 0.96,
        autoAlpha: 0,
        ease: "power1.out",
      })
      .from("#presentation .icon-block", {
        x: 10,
        scale: 0.96,
        autoAlpha: 0,
        stagger: 0.085,
        ease: "power1.out",
      });
    return () => {};
  }, []);

  return (
    <div className={`${classes.layerstyle}`} id="presentation">
      <Container>
        <h3 className="first">Presentation Layer</h3>
        <div className="row pt-3">
          <div className="col-12 col-md-4 col-sm-12">
            <div className={`${classes.box}`}>
              <div className={`${classes.rectangular} row`}>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_angular_icon_e707806ec5.svg"
                      height="53"
                      width="43"
                      alt="Angular"
                    />
                  </div>
                  <span>Angular</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_react_e6bd1b192d.svg"
                      height="53"
                      width="43"
                      alt="React"
                    />
                  </div>
                  <span>React</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="ri_vuejs_fill_827329a8f4.svg"
                      height="53"
                      width="43"
                      alt="VueJs"
                    />
                  </div>
                  <span>VueJs</span>
                </div>
              </div>
              <p>Javascript Library</p>
            </div>
          </div>
          <div className="col-12 col-md-4 col-sm-12">
            <div className={`${classes.box}`}>
              <div className={`${classes.rectangular} row`}>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_gatsby_a81fa924a1.svg"
                      height="53"
                      width="43"
                      alt="Gatsby"
                    />
                  </div>
                  <span>Gatsby</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="teenyicons_nextjs_outline_44eb50bc7e.svg"
                      height="53"
                      width="43"
                      alt="Next"
                    />
                  </div>
                  <span>Next</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="vscode_icons_file_type_nuxt_77762fead5.svg"
                      height="53"
                      width="43"
                      alt="Nuxt.Js"
                    />
                  </div>
                  <span>Nuxt.Js</span>
                </div>
              </div>
              <p>Static Site Generator</p>
            </div>
          </div>
          <div className="col-12 col-md-4 col-sm-12">
            <div className={`${classes.box}`}>
              <div className={`${classes.rectangular} row`}>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_nodejs_icon_0bc8c9c9d7.svg"
                      height="53"
                      width="43"
                      alt="NodeJs"
                    />
                  </div>
                  <span>NodeJs</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="vscode_icons_file_type_npm_17c64a8ea3.svg"
                      height="53"
                      width="43"
                      alt="NPM"
                    />
                  </div>
                  <span>NPM</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_yarn_95af6784a5.svg"
                      height="53"
                      width="43"
                      alt="YARN"
                    />
                  </div>
                  <span>YARN</span>
                </div>
              </div>
              <p>Build/Installs</p>
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
};
export default PresantationLayer;
