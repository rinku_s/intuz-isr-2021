//@ts-nocheck
import React, { useEffect } from "react";
import classes from "./styles.module.scss";
import { cdn } from "../../../config/cdn";
import Container from "../../Container/Container";
import { scrollAnimation } from "hooks/scrollAnimation";
import gsap from "gsap";
import Image from "next/image";
import { myLoader } from "config/image-loader";
const PresantationLayer = () => {
  useEffect(() => {
    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: "#deploy",
        start: "top center",
        once: true,
      },
    });
    tl.fade("#deploy .first")
      .from(`#deploy .${classes.box}`, {
        y: 10,
        scale: 0.96,
        autoAlpha: 0,
        stagger: 0.085,
        ease: "power1.out",
      })
      .from("#deploy .icon-block", {
        x: 10,
        scale: 0.96,
        autoAlpha: 0,
        stagger: 0.085,
        ease: "power1.out",
      });
    return () => {};
  }, []);

  return (
    <div className={`${classes.layerstyle}`} id="deploy">
      <Container>
        <h3 className="first">Deploy</h3>
        <div className="row md:justify-center pt-3">
          <div className="col-12 col-md-4 col-sm-12">
            <div className={`${classes.box}`}>
              <div className={`${classes.rectangular} row`}>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_git_icon_a10c473682.svg"
                      height="53"
                      width="43"
                      alt="Git"
                    />
                  </div>
                  <span>Git</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_gitlab_34cea8669d.svg"
                      height="53"
                      width="43"
                      alt="Gitlab"
                    />
                  </div>
                  <span>Gitlab</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_github_octocat_c6df110ce4.svg"
                      height="53"
                      width="43"
                      alt="Github"
                    />
                  </div>
                  <span>Github</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_bitbucket_b12c1c027b.svg"
                      height="53"
                      width="43"
                      alt="BitBucket"
                    />
                  </div>
                  <span>BitBucket</span>
                </div>
              </div>
              <p>GIT</p>
            </div>
          </div>
          <div className="col-12 col-md-4 col-sm-12">
            <div className={`${classes.box}`}>
              <div className={`${classes.rectangular} row`}>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="ion_logo_vercel_111d930427.svg"
                      height="53"
                      width="43"
                      alt="Vercel"
                    />
                  </div>
                  <span>Vercel</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_aws_s3_fd80ac06ff.svg"
                      height="53"
                      width="43"
                      alt="AWS S3"
                    />
                  </div>
                  <span>AWS S3</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_aws_cloudfront_b801342073.svg"
                      height="53"
                      width="43"
                      alt="Cloudfront"
                    />
                  </div>
                  <span>Cloudfront</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_cloudflare_d5ca8fdee3.svg"
                      height="53"
                      width="43"
                      alt="Cloudflare"
                    />
                  </div>
                  <span>Cloudflare</span>
                </div>
              </div>
              <p>Hosting/CDN</p>
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
};
export default PresantationLayer;
