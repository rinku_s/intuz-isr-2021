//@ts-nocheck
import React, { useEffect } from "react";
import classes from "./styles.module.scss";
import { cdn } from "../../../config/cdn";
import Container from "../../Container/Container";
import { scrollAnimation } from "hooks/scrollAnimation";
import Image from "next/image";
import { myLoader } from "config/image-loader";
import gsap from "gsap";
const PresantationLayer = () => {
  useEffect(() => {
    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: "#api",
        start: "top center",
        once: true,
      },
    });
    tl.fade("#api .first")
      .from(`#api .${classes.box}`, {
        y: 10,
        scale: 0.96,
        autoAlpha: 0,
        stagger: 0.085,
        ease: "power1.out",
      })
      .from("#api .icon-block", {
        x: 10,
        scale: 0.96,
        autoAlpha: 0,
        stagger: 0.085,
        ease: "power1.out",
      });
    return () => {};
  }, []);

  return (
    <div className={`${classes.layerstyle}`} id="api">
      <Container>
        <h3 className="first">APIs</h3>
        <div className="row md:justify-center pt-3">
          <div className="col-12 col-md-4 col-sm-12">
            <div className={`${classes.box}`}>
              <div className={`${classes.rectangular} row`}>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_strapi_icon_77573d3af3.svg"
                      height="53"
                      width="43"
                      alt="Strapi"
                    />
                  </div>
                  <span>Strapi</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_prismic_icon_946918620e.svg"
                      height="53"
                      width="43"
                      alt="Prismic"
                    />
                  </div>
                  <span>Prismic</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_contentful_ec5f97f85a.svg"
                      height="53"
                      width="43"
                      alt="Contentful"
                    />
                  </div>
                  <span>Contentful</span>
                </div>
              </div>
              <p>Headless CMS</p>
            </div>
          </div>
          <div className="col-12 col-md-5 col-sm-12">
            <div className={`${classes.box}`}>
              <div className={`${classes.rectangular} row`}>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="bx_bxl_stripe_34a4c03ad3.svg"
                      height="53"
                      width="43"
                      alt="Stripe"
                    />
                  </div>
                  <span>Stripe</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_twilio_7836a80861.svg"
                      height="53"
                      width="43"
                      alt="Twilio"
                    />
                  </div>
                  <span>Twilio</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_auth0_1f2b898d92.svg"
                      height="53"
                      width="43"
                      alt="Auth0"
                    />
                  </div>
                  <span>Auth0</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="logos_disqus_655583cb2b.svg"
                      height="53"
                      width="43"
                      alt="DISQUS"
                    />
                  </div>
                  <span>DISQUS</span>
                </div>
                <div className="col icon-block">
                  <div>
                    <Image
                      layout="fixed"
                      loader={myLoader}
                      src="bigcommerce_1_1_ef19d2eee5.svg"
                      height="53"
                      width="43"
                      alt="BigCommerce"
                    />
                  </div>
                  <span>BigCommerce</span>
                </div>
              </div>
              <p>Utility APIs</p>
            </div>
          </div>
        </div>
      </Container>
    </div>
  );
};
export default PresantationLayer;
