//@ts-nocheck
import gsap from "gsap";
import React, { useEffect } from "react";
import classes from "./styles.module.scss";
import Image from "next/image";
import { myLoader } from "config/image-loader";
const WhatMarketingBlock = () => {
  useEffect(() => {
    let tl = gsap.timeline({
      scrollTrigger: {
        trigger: `#what_slow`,
        start: "top 60%",
      },
    });

    tl.from(`.${classes.boxStyleWhymaking}`, {
      y: 30,
      autoAlpha: 0,
      duration: 0.5,
      stagger: 0.3,
    });
  }, []);

  return (
    <>
      <div className={`${classes.boxStyleWhymaking}`}>
        <div className="row m-0">
          <div className="col-2 pr-0">
            <Image
              layout="fixed"
              loader={myLoader}
              src="ph_stack_1_9b4b8ab6b8.png"
              alt="Poor Web Architecture"
              height="55"
              width="55"
            />
          </div>
          <div className="col-10">
            <h4>Poor Web Architecture</h4>
            <p>
              If your web architecture is faulty or full of bugs, your site
              speed will become slow and hamper your user experience.
            </p>
          </div>
        </div>
      </div>
      <div className={`${classes.boxStyleWhymaking} `}>
        <div className="row m-0">
          <div className="col-2 pr-0">
            <Image
              layout="fixed"
              loader={myLoader}
              src="et_browser_1_0a4e28e526.png"
              alt="Old Technology Stack"
              height="55"
              width="55"
            />
          </div>
          <div className="col-10">
            <h4>Old Technology Stack</h4>
            <p>
              Older technologies such as WordPress and regular PHP aren't built
              for scale. If you want to enjoy impressive load capability, you
              need to upgrade to a new technology stack.
            </p>
          </div>
        </div>
      </div>
      <div className={`${classes.boxStyleWhymaking} `}>
        <div className="row m-0">
          <div className="col-2 pr-0">
            <Image
              layout="fixed"
              loader={myLoader}
              src="ion_code_download_outline_1_fa25780e11.png"
              alt="Sloppy & Unoptimized Coding"
              height="55"
              width="55"
            />
          </div>
          <div className="col-10">
            <h4>Sloppy & Unoptimized Coding</h4>
            <p>
              Not following the right coding practices can hamper your website
              speed. Unused fonts and plugins, heavy codes and lengthy scripts
              delay the page load time considerably.
            </p>
          </div>
        </div>
      </div>
      <div className={`${classes.boxStyleWhymaking} `}>
        <div className="row m-0">
          <div className="col-2 pr-0">
            <Image
              layout="fixed"
              loader={myLoader}
              src="mdi_cloud_search_outline_1_3684befae1.png"
              alt="Hosting & Cloud Architecture"
              height="55"
              width="55"
            />
          </div>
          <div className="col-10">
            <h4>Hosting & Cloud Architecture</h4>
            <p>
              Having an unoptimized hosting and cloud solution which is not
              specific to the web application contributes to the slow page load
              speed.
            </p>
          </div>
        </div>
      </div>
      <div className={`${classes.boxStyleWhymaking} `}>
        <div className="row m-0">
          <div className="col-2  pr-0">
            <Image
              layout="fixed"
              loader={myLoader}
              src="ion_speedometer_outline_1_b0b541f247.png"
              alt="Lack of Caching Mechanism"
              height="55"
              width="55"
            />
          </div>
          <div className="col-10">
            <h4>Lack of Caching Mechanism</h4>
            <p>
              Every time a user loads a webpage, the browser downloads a lot of
              data to display that page. Not having a proper cache policy and
              caching mechanism impacts page loading speed significantly.
            </p>
          </div>
        </div>
      </div>
    </>
  );
};
export default WhatMarketingBlock;
