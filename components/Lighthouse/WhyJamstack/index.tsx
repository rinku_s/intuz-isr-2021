//@ts-nocheck
import React, { useEffect } from "react";
import classes from "./styles.module.scss";
import { cdn } from "../../../config/cdn";
import Container from "../../Container/Container";
import gsap from "gsap";
import Image from "next/image";
import { myLoader } from "config/image-loader";
const WhyDoesBlock = () => {
  useEffect(() => {
    const tl = gsap.timeline({
      scrollTrigger: {
        trigger: `#jamstack`,
        start: "top 50%",
      },
    });

    tl.from(`.${classes.boxStyleWhyJamstack}`, {
      y: 30,
      scale: 0.98,
      autoAlpha: 0,
      stagger: 0.2,
    });
  }, []);

  return (
    <Container>
      <div className="row" id="jamstack">
        <div
          className={`${classes.boxStyleWhyJamstack} col-md-6 col-12 col-sm-12`}
        >
          <div className="row m-0">
            <div className="col-2 pr-0">
              <Image
                layout="fixed"
                loader={myLoader}
                src="mdi_rocket_launch_outline_46f32959af.png"
                alt="Unmatched Performance"
                height="43"
                width="43"
              />
            </div>
            <div className="col-10">
              <h4>Unmatched Performance</h4>
              <p>
                JAMstack sites generate web pages during a build. They remove
                the need for developing page views on a server. With all
                pre-built files available on a server, very high performance is
                guaranteed.
              </p>
            </div>
          </div>
        </div>
        <div
          className={`${classes.boxStyleWhyJamstack} col-md-6 col-12 col-sm-12`}
        >
          <div className="row m-0">
            <div className="col-2 pr-0">
              <Image
                layout="fixed"
                loader={myLoader}
                src="flat_ui_search_9e5d9d715e.png"
                alt="SEO Advantage"
                height="43"
                width="43"
              />
            </div>
            <div className="col-10">
              <h4>SEO Advantage</h4>
              <p>
                It's obvious—JAMstack websites are faster, and Google loves fast
                websites. Undeniably, the higher your site ranks, the more
                traffic it is going to get. It's a win-win for you. With
                JAMstack, you'll always have the SEO advantage.
              </p>
            </div>
          </div>
        </div>
        <div
          className={`${classes.boxStyleWhyJamstack} col-md-6 col-12 col-sm-12`}
        >
          <div className="row m-0">
            <div className="col-2 pr-0">
              <Image
                layout="fixed"
                loader={myLoader}
                src="carbon_scale_5d31583bd7.png"
                alt="Limitless Scalability"
                height="43"
                width="43"
              />
            </div>
            <div className="col-10">
              <h4>Limitless Scalability</h4>
              <p>
                Website scalability is a standard feature of JAMstack, which
                means its website caches everything in a CDN—without the hassle
                of convoluted logic and workflows. Enjoy exceptional load
                capacity and simpler deployments.
              </p>
            </div>
          </div>
        </div>
        <div
          className={`${classes.boxStyleWhyJamstack} col-md-6 col-12 col-sm-12`}
        >
          <div className="row m-0">
            <div className="col-2 pr-0">
              <Image
                layout="fixed"
                loader={myLoader}
                src="carbon_security_a18447e0b8.png"
                alt="Maximum Security"
                height="43"
                width="43"
              />
            </div>
            <div className="col-10">
              <h4>Maximum Security</h4>
              <p>
                The JAMstack diffuses all server-side processes into
                microservice APIs or headless CMSes, resulting in fewer servers
                to reduce areas for cyberattacks. Plus, it serves web pages and
                assets as pre-generated files that allow read-only hosting. This
                further reduces the scope of any attack.
              </p>
            </div>
          </div>
        </div>
        <div
          className={`${classes.boxStyleWhyJamstack} col-md-6 col-12 col-sm-12`}
        >
          <div className="row m-0">
            <div className="col-2 pr-0">
              <Image
                layout="fixed"
                loader={myLoader}
                src="carbon_license_maintenance_draft_479b5cb3f5.png"
                alt="Portability & Maintainability"
                height="43"
                width="43"
              />
            </div>
            <div className="col-10">
              <h4>Portability & Maintainability</h4>
              <p>
                Since JAMstack sites are pre-generated, it is possible to host
                them from various hosting services because of the use of
                independent microservices that are flexible to change anytime.
                That makes migrating to a preferred host is also easy.
              </p>
            </div>
          </div>
        </div>
        <div
          className={`${classes.boxStyleWhyJamstack} col-md-6 col-12 col-sm-12`}
        >
          <div className="row m-0">
            <div className="col-2 pr-0">
              <Image
                layout="fixed"
                loader={myLoader}
                src="carbon_scale_5d31583bd7.png"
                alt="Efficient Development"
                height="43"
                width="43"
              />
            </div>
            <div className="col-10">
              <h4>Efficient Development</h4>
              <p>
                Front-end developers do not depend on proprietary technologies
                and little known frameworks, enabling them to build on widely
                available tools. As a result, it's not hard to find developers
                who have the right skill set to make with JAMstack. Efficiency
                prospers!
              </p>
            </div>
          </div>
        </div>
      </div>
    </Container>
  );
};
export default WhyDoesBlock;
