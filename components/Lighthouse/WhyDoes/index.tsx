//@ts-nocheck
import React from "react";
import classes from "./styles.module.scss";
import { cdn } from "../../../config/cdn";
import Container from "../../Container/Container";
import Image from "next/image";
import { myLoader } from "config/image-loader";
const WhyDoesBlock = () => {
  return (
    <Container>
      <div className="row">
        <div
          className={`${classes.boxStyleWhydoes} fourth col-md-6 col-12 col-sm-12`}
        >
          <div className="row m-0">
            <div className="col-2 pr-0">
              <Image
                layout="responsive"
                loader={myLoader}
                src="uil_arrow_growth_f445911363.svg"
                alt="Lose Majority of Traffic"
                height="68"
                width="68"
              />
            </div>
            <div className="col-10">
              <h4>Lose Majority of Traffic</h4>
              <p className="text-left">
                Google says you could lose half of your site visitors if your
                webpages take more than three second to load.
              </p>
            </div>
          </div>
        </div>
        <div
          className={`${classes.boxStyleWhydoes} fourth col-md-6 col-12 col-sm-12`}
        >
          <div className="row m-0">
            <div className="col-2 pr-0">
              <Image
                layout="responsive"
                loader={myLoader}
                src="carbon_arrow_shift_down_6105aeb944.png"
                alt="Higher Advertising Cost"
                height="68"
                width="68"
              />
            </div>
            <div className="col-10">
              <h4>Higher Advertising Cost</h4>
              <p className="text-left">
                Creating a Google Ads strategy? Focus on your site loading speed
                because “High loading time = Bad quality score = High CPC”
              </p>
            </div>
          </div>
        </div>
        <div
          className={`${classes.boxStyleWhydoes} fourth col-md-6 col-12 col-sm-12`}
        >
          <div className="row m-0">
            <div className="col-2 pr-0">
              <Image
                layout="responsive"
                loader={myLoader}
                src="Low_Conversion_Rate_b4a1c2839e.svg"
                alt="Low Conversion Rate"
                height="68"
                width="68"
              />
            </div>
            <div className="col-10">
              <h4>Low Conversion Rate</h4>
              <p className="text-left">
                A 1-second delay in page response can hurt your conversions by
                7%! That means, it affects your bottom line.
              </p>
            </div>
          </div>
        </div>
        <div
          className={`${classes.boxStyleWhydoes} fourth col-md-6 col-12 col-sm-12`}
        >
          <div className="row m-0">
            <div className="col-2 pr-0">
              <Image
                layout="responsive"
                loader={myLoader}
                src="Negative_Impact_on_Search_Engine_Rankings_77dcf1aa3b.svg"
                alt="Negative Impact on Search Engine Rankings"
                height="68"
                width="68"
              />
            </div>
            <div className="col-10">
              <h4>Negative Impact on Search Engine Rankings</h4>
              <p className="text-left">
                As per Google, page speed is considered one of the most
                important ranking factors for mobile SERPs.
              </p>
            </div>
          </div>
        </div>
      </div>
    </Container>
  );
};
export default WhyDoesBlock;
