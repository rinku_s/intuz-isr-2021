//@ts-nocheck
import React from "react";
import classes from "./styles.module.scss";
import Container from "../../Container/Container";
import Accordion from "react-bootstrap/Accordion";
import Card from "react-bootstrap/Card";
import Data from "./data.json";
import Head from "next/head";
const Faq = () => {
  let scr = `
    {
         "@context": "https://schema.org",
         "@type" : "FAQPage",
         "mainEntity": [
            ${Data.faq.map(
              (d) =>
                `\n{
                "@type": "Question",
                "name": "${d.qn}",
                "acceptedAnswer": {
                    "@type": "Answer",
                    "text": "${d.ans}"
                }
              }`
            )}
        ]
    }
`;
  const [activeindex, setactiveindex] = React.useState(null);
  return (
    <div className={`${classes.faqStyle}`}>
      <Head>
        <script
          type="application/ld+json"
          dangerouslySetInnerHTML={{ __html: scr }}
        ></script>
      </Head>
      <Container>
        <Accordion
          activeKey={activeindex}
          // defaultActiveKey={0}
          onSelect={(e) => {
            setactiveindex(e);
          }}
        >
          {Data.faq.map((data, index) => {
            return (
              <Card className={`${classes.cardstyle}`} key={index}>
                <Card.Header className={`${classes.cardheader} `}>
                  <Accordion.Toggle
                    as={Card.Header}
                    className={`${classes.cardheadertwo} ${
                      activeindex == index ? classes.active : ""
                    }`}
                    variant="link"
                    eventKey={`${index}`}
                  >
                    {data.qn}
                  </Accordion.Toggle>
                </Card.Header>
                <Accordion.Collapse
                  eventKey={`${index}`}
                  className={`${classes.accrodianColleps}`}
                >
                  <Card.Body className={`${classes.cardBodyStyle}`}>
                    {data.ans}
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            );
          })}
        </Accordion>
      </Container>
    </div>
  );
};
export default Faq;
