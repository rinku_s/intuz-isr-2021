//@ts-nocheck

import React from 'react';
import { cdn } from '../../config/cdn';
import PrimaryHeading from '../Heading/Heading';
import SecondaryHeading from '../Heading/SecondaryHeading';
import Block from './Block';
import style from './styles.module.scss';

const DevBlock = (props) => {
    return (
        <div className={`${style.ServiceBlock} ${props.className}`}>
            <PrimaryHeading>B2B Digital Agencies and Web development companies outsource application development</PrimaryHeading>
            <SecondaryHeading>to Intuz for fast, reliable and bug-free development</SecondaryHeading>
            
            <div className='row'>
                    {props.content.map((service, index) => {
                        return (
                            <Block 
                                imgSrc={cdn(service.image.name)}
                                alt={service.image.name}
                                title={service.title}
                                subTitle={service.subTitle}
                                key={index}
                            />
                        )
                    })}
            </div>
        </div>
    )
}

export default DevBlock;