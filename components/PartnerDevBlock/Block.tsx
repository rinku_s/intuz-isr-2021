//@ts-nocheck
//@ts-nocheck
import React from 'react';
import LazyLoad from 'react-lazyload';
import ImageLoader from '../ImageLoader/ImageLoader';
import style from './styles.module.scss';

const Block = (props) => {
    return (
        <div className={`col-sm-6 col-md-6 col-lg-3 ${style.col}`}>
            <div className={style.Block}>
                <LazyLoad height={300} offset={800} >
                    <ImageLoader src={`${props.imgSrc}?fm=png&auto=format`} alt={props.alt} />
                </LazyLoad>
                <h2>{props.title}</h2>
                <h4>{props.subTitle}</h4>
            </div>
        </div>
    )
}

export default Block;
