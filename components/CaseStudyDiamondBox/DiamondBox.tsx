//@ts-nocheck
import React from 'react';
import classes from './styles.module.scss';

const DiamondBox = ({ icon, desc, right }) => {
    return (
        <div className={classes.DiamondBox + (right ?  ' ' + classes.right : '' )}>
                <div>
                    <img className="lazyload img-fluid" data-src={icon} alt={desc}/>
                </div>
                <p>{desc}</p>
        </div>
    )
}

export default DiamondBox
