//@ts-nocheck
import React from 'react'
import DiamondBox from './DiamondBox'
import { cdn } from '../../config/cdn';
import classes from './styles.module.scss'
const CaseStudyDiamondBox = ({ projectchallengesiconbox }) => {
    console.log(projectchallengesiconbox);
    return (
        <div className={classes.CaseStudyDiamondBox}>
              {projectchallengesiconbox.map((prc,i)=>(
                   <DiamondBox right={i % 2 == 0} desc={prc.description} icon={cdn(prc.icon.name)} />
              ))}
        </div>
    )
}

export default CaseStudyDiamondBox
