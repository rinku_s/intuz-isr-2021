import gsap from "gsap";
import React, { Component } from "react";
import { cdn } from "../../config/cdn";
import Block from "./ServiceBlock/Block";

interface ServiceProps {
  sectionservices?: ServiceImageProp[];
}

interface ServiceImageProp {
  id: string;
  name: string;
  image: {
    name: string;
  };
  title: string;
  link: string;
}

export default class Service extends Component<ServiceProps, {}> {
  componentDidMount = () => {
    gsap.set(`.serviceblock`, { opacity: 1 });
  };

  render() {
    return (
      <div style={{ marginTop: "6.6rem" }}>
        <div className="row">
          {this.props.sectionservices.map((ss) => (
            <Block
              className="serviceblock"
              key={ss.id}
              imgSrc={cdn(ss.image.name)}
              alt={ss.title}
              title={ss.title}
              link={ss.link}
            />
          ))}
        </div>
      </div>
    );
  }
}
