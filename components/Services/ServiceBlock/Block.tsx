import { myLoader } from "config/image-loader";
import Image from "next/image";
import Link from "next/link";
import React from "react";
import styles from "./serviceblock.module.scss";

const Block = (props) => {
  return (
    <div className={`col-sm-12 col-md-6 col-lg-3 ${props.className}`}>
      <Link href={props.link}>
        <a className={styles.ServicesBlock}>
          <div
            style={{
              height: "100px",
              width: "80px",
              position: "relative",
              margin: "0 auto",
            }}
          >
            {/*<LazyLoad offset={10} height={50} once>
              <img
                src={props.imgSrc + "?auto=format,compress"}
                alt={props.alt}
              />
  </LazyLoad>*/}

            <Image
              layout="fill"
              sizes="20vw"
              loader={myLoader}
              objectFit="contain"
              src={
                props.imgSrc.split("https://intuz-site.imgix.net/uploads/")[1]
              }
            />
          </div>

          <div className="content-center">
            <div
              className={`${styles.ServiceSeparator} flex justify-center `}
            />
            <h3
              className={styles.titleBlock}
              dangerouslySetInnerHTML={{ __html: props.title }}
            ></h3>

            <h4>
              <Link href={props.link}>
                <a title={props.title}>
                  Learn More <span>{">"}</span>
                </a>
              </Link>
            </h4>
          </div>
        </a>
      </Link>
    </div>
  );
};

export default Block;
