//@ts-nocheck
//@ts-nocheck
import { myLoader } from "config/image-loader";
import gsap from "gsap";
import ScrollToPlugin from "gsap/dist/ScrollToPlugin";
import Image from "next/image";
import React, { Component } from "react";
import { Media, MediaContextProvider } from "../../config/responsiveQuery";
import IconBlock from "../IconBlock/IconBlock";
import LinkButton from "../UI/LinkButton/LinkButton";
import styles from "./casefullblock.module.scss";

gsap.registerPlugin(ScrollToPlugin);

class CaseFullBlock extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.blockInfo = props.blockInfo;
  }

  componentDidMount() {
    this.setState({ windowSize: window.innerWidth });
  }

  gotoContent = () => {
    gsap.to(window, 1, { scrollTo: { y: "#content", autoKill: false } });
  };

  render() {
    const logoSrc =
      this.state.windowSize <= 767
        ? this.blockInfo.logoSrcXs
          ? this.blockInfo.logoSrcXs
          : this.blockInfo.logoSrc
        : this.blockInfo.logoSrc;
    const downArrow = `url(../../../static/Images/icons/down-arrow1.png) top center no-repeat`;
    // console.log(this.blockInfo.linkBtnHref);
    let as = this.blockInfo.linkBtnHref;

    return (
      <MediaContextProvider>
        <div
          className={`${styles[this.blockInfo.variation]} ${
            styles.CaseFullBlock
          }`}
        >
          <Media greaterThanOrEqual="sm">
            {/* <img
              data-src={cdn(
                this.blockInfo.backImageSrc + "?auto=format,compress"
              )}
              alt={this.blockInfo.backImageSrc}
              data-class={"backImage"}
              className={`${styles.backImage} lazyload`}
            /> */}
            <div>
              <Image
                src={this.blockInfo.backImageSrc}
                objectFit="cover"
                alt={this.blockInfo.backImageSrc}
                loader={myLoader}
                layout="fill"
              />
            </div>
            {this.blockInfo.variation == "topBlock" ||
            this.blockInfo.variation == "topBlockl" ? (
              <div className={styles.ArrowButton}>
                <span className={styles.explore}>Explore More Work</span>
                <a className={styles.arrow} onClick={this.gotoContent}>
                  &nbsp;
                </a>
              </div>
            ) : (
              ""
            )}
          </Media>
          <Media lessThan="sm">
            {/* <img
              data-src={cdn(
                this.blockInfo.backMobileSrc + "?auto=format,compress"
              )}
              alt={this.blockInfo.backMobileSrc}
              className={`${styles.backImage} lazyload`}
            /> */}
            <Image
              src={this.blockInfo.backMobileSrc}
              alt={this.blockInfo.backMobileSrc}
              loader={myLoader}
              layout="intrinsic"
              height={350}
              width={400}
            />
            <div
              className={styles.MobileArrow}
              style={{ background: downArrow }}
            ></div>
          </Media>
          <div
            className={`${styles.descBlock} ${
              styles[this.blockInfo.variation]
            }`}
          >
            {/* <LazyLoad height={300} offset={300} once>
              <img
                alt={this.blockInfo.logoTitle}
                title={this.blockInfo.logoTitle}
                src={cdn(
                  `${logoSrc}${logoSrc.slice(-1) == "&" ? "" : "?"}auto=format`
                )}
                className={styles.Logo}
              />
            </LazyLoad> */}
            <div>
              <Image
                src={`${logoSrc}${logoSrc.slice(-1) == "&" ? "" : "?"}`}
                alt={this.blockInfo.logoTitle}
                loader={myLoader}
                layout="intrinsic"
                height={114}
                width={444}
              />
            </div>
            <h3>{this.blockInfo.title}</h3>
            {this.blockInfo.isFirstBanner === true ? (
              <h1>{this.blockInfo.desc}</h1>
            ) : (
              <p>{this.blockInfo.desc}</p>
            )}
            <IconBlock icons={this.blockInfo.IconArray} />
            <span className={styles.LinkButton}>
              {this.blockInfo.linkBtnHref ? (
                <LinkButton
                  as={as}
                  href={"/case-studies/[id]"}
                  variation={
                    this.blockInfo.btnVariation
                      ? this.blockInfo.btnVariation
                      : "fullCaseBlockBtn"
                  }
                >
                  View Case Study
                </LinkButton>
              ) : (
                ""
              )}
            </span>
          </div>
        </div>
      </MediaContextProvider>
    );
  }
}

"abc".charAt("abc".length);

export default CaseFullBlock;
