//@ts-nocheck
//@ts-nocheck
import React from 'react';
import { cdn } from '../../../../config/cdn';
import ImageBlock from '../../../ImageBlock/ImageBlock';
import styles from './backimage.module.scss';

const BackImage = (props) => {
    return (
        <ImageBlock
            src={cdn(props.imgSrc)}
            alt={props.imgSrc}
            className={`${styles[props.variation]} ${styles.backImage}`}
            dwidth={1840}
            mwidth={767} />

    )
}
export default BackImage;