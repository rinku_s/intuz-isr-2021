//@ts-nocheck
//@ts-nocheck
import ClipboardJS from 'clipboard';
import 'lazysizes';
import React, { useEffect } from 'react';
import { Snackbar } from '../UI/Snackbar/Snackbar';
import classes from './styles.module.scss';
const Blog = (props) => {

    let r = React.useRef();

    useEffect(() => {
        new ClipboardJS('.cpy', {
            target: function (trigger) {
                //   alert("Copied")
                r.current.openSnackBar('Copied');
                return trigger.nextElementSibling;
            }
        });
        if (document.getElementById('download') != undefined) {
            //   ReactDOM.render(
            //       <DownloadPDF link={props.link}/>,
            //       document.getElementById('download')
            //   )
        }

        !function (window) {
            'use strict';

            // Update this function so it returns the height of your fixed headers
            function fixedHeaderOffset() {
                var width = window.innerWidth;

                if (width < 525) {
                    return 120;
                }
                else if (width < 1024) {
                    return 88;
                }
                else {
                    return 90;
                }
            }

            // Run on first scroll (in case the user loaded a page with a hash in the url)
            window.addEventListener('scroll', onScroll);
            function onScroll() {
                window.removeEventListener('scroll', onScroll);
                scrollUpToCompensateForFixedHeader();
            }

            // Run on hash change (user clicked on anchor link)
            if ('onhashchange' in window) {
                window.addEventListener('hashchange', scrollUpToCompensateForFixedHeader);
            }

            function scrollUpToCompensateForFixedHeader() {
                var hash,
                    target,
                    offset;

                // Get hash, e.g. #mathematics
                hash = window.location.hash;
                if (hash.length < 2) { return; }

                // Get :target, e.g. <h2 id="mathematics">...</h2>
                target = document.getElementById(hash.slice(1));
                if (target === null) { return; }

                // Get distance of :target from top of viewport. If it's near zero, we assume
                // that the user was just scrolled to the :target.
                if (target.getBoundingClientRect().top < 2) {
                    window.scrollBy({
                        top: -fixedHeaderOffset(),
                        behavior: 'smooth'
                    });
                    // window.scrollBy(0, -fixedHeaderOffset());
                }
            }

        }(window);

    }, [])
    return (
        <>
            <Snackbar ref={r} />
            <div className={`${classes.Blog}`}>

                {props.children}
                <style jsx>
                    {`
                pre{
                    background-color: #FBFBFB;
                    height: 24rem;
                    margin: 10rem;
                    border: 1px solid #F3F3F3;
                }
                code{
                    font-size: 14px;
                    font-weight: 600;
                    line-height: 2.3rem;
                    color: #666;
                }
                `}
                </style>
            </div>
        </>

    )
}

export default Blog
