//@ts-nocheck
import gsap from 'gsap';
import React, { useEffect } from 'react';
import BlogBreadcrumb from '../../components/BlogBreadcrumb/BlogBreadcrumb';
import Container from '../Container/Container';
import SecondaryHeading from '../Heading/SecondaryHeading';
import classes from './styles.module.scss';
import HtmlParser from 'html-react-parser';

const ResourcesSectionStyle = (props) => {


    // let position = props.position;
    let h1 = React.useRef()
    useEffect(() => {
        let tl = gsap.timeline({delay:1});
        tl.from(h1.current, 0.6, { y:20, autoAlpha:0, ease:"Power0.easeOut" }, "-=0.2")
            .from(`.${classes.ResourcesSectionStyle} p`, 0.6, { y:30, autoAlpha:0, ease:"Power0.easeOut" },"-=0.3")
            .from(`.${classes.ResourcesSectionStyle} a`, 0.6, { y:10, autoAlpha:0, ease:"Power0.easeOut" },"-=0.3");
    }, []);
    return (
        <section className={`${classes.ResourcesSectionStyle} ${classes[props.variation]}`} style={{ ...props.style}}>
            <Container>
                {props.breadcrumb ? <BlogBreadcrumb {...props.breadcrumb} /> : ''} 
                <h1 ref={h1}>
                    {HtmlParser(`${props.heading}`)}
                </h1>
                <SecondaryHeading font="s" style={{color:'#fff'}}>
                    {props.subtitle}
                </SecondaryHeading>
                {props.children}
            </Container>
            <style jsx>
                {`
                    section{
                        background-image: linear-gradient(rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url(${props.backImage}?auto=format);
                    }

                    @media only screen and (max-width: 767px) { 
                        section{
                            background-image:linear-gradient(rgba(0,0,0,0.5), rgba(0,0,0,0.5)), url(${props.backImage}?auto=format&w=800) ;
                        }
                     };
                `}
            </style>
        </section>
    )
}

export default ResourcesSectionStyle   
