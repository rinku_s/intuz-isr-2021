//@ts-nocheck
import { PageTransition } from 'next-page-transitions'
import React from 'react'

const PGT = (props) => {
  return (
    <>
      <PageTransition timeout={400} classNames="page-transition">
        {props.children}
      </PageTransition>
      <style jsx global>{`
      .page-transition-enter {
        opacity: 0;
      }
      .page-transition-enter-active {
        opacity: 1;

        transition: all 400ms ease-out;
      }
      .page-transition-exit {
        opacity: 1;
      }
      .page-transition-exit-active {
        opacity: 0;
        transition: all 400ms ease-out;
      }
    `}</style>
    </>
  )
}

export default PGT
