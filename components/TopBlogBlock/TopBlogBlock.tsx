//@ts-nocheck


import gsap from 'gsap';
import Link from 'next/link';
import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';
import BlogAuthor from '../BlogAuthor/BlogAuthor';
import Category from '../BlogCategory/Category';
import style from './styles.module.scss';


const TopBlogBlock = (props) => {
    let imgRef = React.useRef();
    let metaRef = React.useRef();
    // setOpacity(`.${style.BlogMeta}, .${style.BlogImage}`)

    function play() {
        const tl = gsap.timeline();
        tl.fromTo(imgRef.current, 0.8, { x: -30, autoAlpha: 0 }, { x: 0, autoAlpha: 1, ease: 'Power2.easeOut' }, 0.1)
            .fromTo(metaRef.current, 0.8, { x: 30, autoAlpha: 0 }, { x: 0, autoAlpha: 1, ease: 'Power2.easeOut' });
    }

    const limit = 100;
    let description = (props.description <= limit ? props.description : props.description.substring(0, limit) + " ...")

    return (

        <div className={`${style.TopBlogBlock} row`} >
            <div className={`${style.BlogImage} col-md-6`} ref={imgRef}>
                <Link as={`/blog/${props.link}`} href='/blog/[name]' prefetch={false}>
                    <a>
                        <LazyLoad height={300} offset={300}>
                            <img src={`${cdn(props.resourceBlockImage.name)}?auto=format&fm=pjpg&w=640&h=450`} alt={props.resourceBlockImage.name} />
                        </LazyLoad>
                    </a>
                </Link>
            </div>
            <div className={`${style.BlogMeta} col-md-6`} ref={metaRef}>
                {props.blog_category ? <Category categories={props.blog_category} /> : ''}
                <h3>
                    <Link as={`/blog/${props.link}`} href='/blog/[name]' prefetch={false}>
                        <a>{props.title}</a>
                    </Link>
                </h3>
                <p>{description}</p>
                <BlogAuthor
                    name={props.blog_author && props.blog_author.name}
                    date={props.updatedAt}
                    image={props.blog_author && cdn(props.blog_author.image.name)}
                    link={props.blog_author && props.blog_author.link}
                />
            </div>
        </div>
        // </WaypointOnce >
    )
}

export default TopBlogBlock;