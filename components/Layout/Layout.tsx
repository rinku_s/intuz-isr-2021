// import gsap from 'gsap';
import Header from "components/Navigation/Header/Header";
import Head from "next/head";
import { useRouter } from "next/router";
import React, { useEffect } from "react";
import { useProcessing } from "../../context/processingContext";
// import { Transition } from 'react-transition-group';
import Footer from "../Footer/Footer";
import ScrollToTopButton from "../ScrollToTopButton/ScrollToTopButton";
import Processing from "../UI/Processing/Processing";

const Layout: React.FC<{
  NoCllectChat?: boolean;
  background?: string;
  maven?: boolean;
  nunito?: boolean;
  whiteBackground?: boolean;
}> = (props) => {
  let location = `https://www.intuz.com${useRouter().asPath}`;

  useEffect(() => {
    if (props.NoCllectChat) {
      if (document.getElementById("chat-bot-launcher-container")) {
        document.getElementById("chat-bot-launcher-container").style.display =
          "none";
      }
    }
  }, []);

  const { processing } = useProcessing();
  return (
    <>
      <Head>
        <link rel="canonical" href={location} />
        <meta name="og:url" content={location} />
      </Head>

      {processing && <Processing />}

      <Header disableTransparency={props.whiteBackground} />
      <main id="main">{props.children}</main>

      <Footer />

      <ScrollToTopButton />
      <style global jsx>
        {`
          body {
            overflow-y: visible;
          }
        `}
      </style>
    </>
  );
};

export default Layout;
