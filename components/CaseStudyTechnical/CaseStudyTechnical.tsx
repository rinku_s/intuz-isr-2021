//@ts-nocheck
import React from 'react';
import LazyLoad from 'react-lazyload';
import CaseStudyParagraph from '../CaseStudyParagraph/CaseStudyParagraph';
import classes from './styles.module.scss';
const CaseStudyTechnical = props => {
    
return (
        <ul className={classes.CaseStudyTechnical}>
           {props.content.map(cst=>(
               <li key={cst.title}>
                   <LazyLoad once height={300} offset={300}>
                        {<img className="lazyload" data-src={cst.icon.url} alt={cst.title} />}
                   </LazyLoad>
                    <CaseStudyParagraph variation="large">{cst.title}</CaseStudyParagraph>
               </li>
           ))} 
        </ul>
    )
}

export default CaseStudyTechnical
