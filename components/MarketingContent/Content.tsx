//@ts-nocheck

import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';
import style from './styles.module.scss';

const Content = (props) => {
    return(
        <div className={`row ${style.MarketingBlock}`}>
            {props.children.map((content, index) => (
                <div className={`col-md-6 ${style.block}`} key={index}>
                    <LazyLoad height={300} offset={300} once>
                        <img src={cdn(`${content.icon.name}`)} />
                    </LazyLoad>
                    <h2>{content.title}</h2>
                    <p>{content.description}</p>
                </div>
            ))}
        </div>

    )
}

export default Content;