//@ts-nocheck
import React from 'react'
import Solution from './Solution';
import { cdn } from '../../../config/cdn';

const ElearningSolutions = (props) => {
    return (
        <div className={`row`} style={{marginTop:"8rem"}}>
            {props.elearningsolutions.map(dd=>(
                <div key={dd.title} className="col-6 col-sm-4 col-lg-3">
                <Solution  
                    image={cdn(dd.image.name)}
                    title={dd.title}
                    subtitle={dd.subtitile}
                />
                </div>
            ))}
        </div>
    )
}

export default ElearningSolutions
