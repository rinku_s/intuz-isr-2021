//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import LazyLoad from 'react-lazyload';
import classes from './styles.module.scss';
const Solution = (props) => {
    let sol = React.useRef();
    // setOpacity(`.${classes.Solution}`);

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(sol.current, 1, { y: 20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' });
    }

    return (


        <div className={classes.Solution} ref={sol}>
            <LazyLoad
                offset={300}
            >
                <img src={props.image} alt={props.title} />
            </LazyLoad>
            <h5>{props.title}</h5>
            <p>{props.subtitle}</p>
        </div>
        // </WaypointOnce >
    )
}

export default Solution
