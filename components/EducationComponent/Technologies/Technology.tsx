//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
const Technology = (props) => {
    return (
        <li className={`${classes.Technology}`}>
            <img src={props.image} alt={props.heading}/>
            <p>{props.heading}</p>
        </li>
    )
}

export default Technology
