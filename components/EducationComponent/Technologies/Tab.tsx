//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
const Tab = (props) => {
    return (
        <li className={`${classes.tab} ${props.active ? classes.active : ''}`} onClick={props.onClick}>
            {props.children}
        </li>
    )
}

export default Tab
