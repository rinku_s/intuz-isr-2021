//@ts-nocheck
export const data = [
        {
             id:"web",
            title: "Web",
            technologies: [
                {
                    techType: "Front-End",
                    technologies:[
                        {
                            image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                            heading:"Angular"
                        },
                        {
                            image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                            heading:"React"
                        },
                        {
                            image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                            heading:"Vue.js"
                        },
                        {
                            image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                            heading:"Backbone.js"
                        },
                        {
                            image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                            heading:"HTML5"
                        },
                        {
                            image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                            heading:"CSS3"
                        },
                    ]
                },
                {
                    techType: "Back-End",
                    technologies:[
                        {
                            image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                            heading:"NOde Js"
                        },
                        {
                            image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                            heading:"React"
                        },
                        {
                            image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                            heading:"Meteor"
                        },
                        {
                            image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                            heading:"PHP"
                        },
                        {
                            image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                            heading:"GO"
                        }
                    ]
                },
            ]
        },


        {
            id:"mobile",
           title: "Mobile",
           technologies: [
               {
                   techType: "Android",
                   technologies:[
                       {
                           image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                           heading:"Java"
                       },
                       {
                           image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                           heading:"Kotlin"
                       }
                   ]
               },
               {
                   techType: "IOS",
                   technologies:[
                       {
                           image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                           heading:"Swift"
                       },
                       {
                           image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                           heading:"Objective-C"
                       }
                   ]
               },

               {
                techType: "Cross Platform",
                technologies:[
                    {
                        image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_react.png",
                        heading:"React Native"
                    }
                    
                ]
            },
           ]
       },
        {
            id:"database",
            title: "Database",
            technologies: [
                {
                    techType: "Database",
                    technologies:[
                        {
                            image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                            heading:"MongoDB"
                        },
                        {
                            image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                            heading:"MySQL"
                        },
                        {
                            image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                            heading:"PostgreSQL"
                        },
                        {
                            image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                            heading:"AuroraDB"
                        },
                        {
                            image:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/solutions/ic_angular.png",
                            heading:"DynamoDB"
                        },
                    ]
                }
            ]
        }

    ]