//@ts-nocheck
import React, { Component } from "react";
import { data } from "./data";
import Tab from "./Tab";
import Content from "./Content";
import classes from "./styles.module.scss";
export default class Technologies extends Component {
  state = {
    activeID: "web",
    technologies: data.find((d) => d.id === "web").technologies,
  };

  setActive = (id) => {
    this.setState({
      activeID: id,
    });
  };

  componentDidMount = () => {
    let tech = data.find((d) => d.id === this.state.activeID).technologies;
    this.setState({
      technologies: tech,
    });
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState.activeID !== this.state.activeID) {
      let tech = data.find((d) => d.id === this.state.activeID).technologies;
      this.setState({
        technologies: tech,
      });
    }
  };

  render() {
    let colwidth;
    if (this.state.technologies.length > 2) {
      colwidth = 4;
    } else if (this.state.technologies.length < 2) {
      colwidth = 12;
    } else {
      colwidth = 5;
    }

    return (
      <div className={classes.Technologies}>
        <ul>
          {data.map((d) => (
            <Tab
              key={d.id}
              onClick={() => this.setActive(d.id)}
              active={this.state.activeID === d.id}
            >
              {d.title}
            </Tab>
          ))}
        </ul>
        <div className="row justify-center">
          {this.state.technologies.map((tech) => (
            <Content
              className={`col-md-${colwidth}`}
              key={tech.techType}
              heading={tech.techType}
              technologies={tech.technologies}
            />
          ))}
        </div>
      </div>
    );
  }
}
