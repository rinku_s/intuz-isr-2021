//@ts-nocheck
import React from 'react';
import classes from './styles.module.scss';
import Technology from './Technology';
const Content = (props) => {
    return (
        <div className={`${props.className} ${classes.Content}`}>
            <h3>{props.heading}</h3>
            <ul className="row">
                {props.technologies.map(technology=>(
                    <Technology
                        key={technology.heading}
                        {...technology}
                    />
                ))}
            </ul>
        </div>
    )
}

export default Content
