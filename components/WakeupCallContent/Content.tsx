//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';
import windowWidth from '../../hooks/windowWidth';
import classes from './styles.module.scss';

const Content = () => {

    let windowSize = windowWidth();

    // setOpacity(`.leftcall, .rightcall`);

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(`.leftcall`, 1, { x: -50, opacity: 0, ease: "power0.ease" }, { x: 0, opacity: 1, ease: "power0.ease" }, 0.5)
            .fromTo(`.rightcall`, 1, { x: 50, opacity: 0, ease: "power0.ease" }, { x: 0, opacity: 1, ease: "power0.ease" }, 0.5);
    }

    return (

        <div className={`${classes.Content} row`}>
            <div className='col-md-3 col-sm-6 leftcall'>
                <LazyLoad height={300} offset={800} >
                    <img src={cdn(`steps.png?auto=format&fm=png&w=${windowSize < 768 ? '170' : '260'}`)} alt='steps' />
                </LazyLoad>
                <span>Wake Up With Steps</span>
            </div>
            <div className='col-md-3 col-sm-6 leftcall'>
                <LazyLoad height={300} offset={300} once>
                    <img src={cdn(`shake_it.png?auto=format&fm=png&w=${windowSize < 768 ? '170' : '260'}`)} alt='shake_it' />
                </LazyLoad>
                <span>Shake It!</span>
            </div>
            <div className='col-md-3 col-sm-6 rightcall'>
                <LazyLoad height={300} offset={300} once>
                    <img src={cdn(`num_scroller.png?auto=format&fm=png&w=${windowSize < 768 ? '170' : '260'}`)} alt='num_scroller' />
                </LazyLoad>
                <span>Number Scroller</span>
            </div>

            <div className='col-md-3 col-sm-6 rightcall'>
                <LazyLoad height={300} offset={300} once>
                    <img src={cdn(`math_puzzle.png?auto=format&fm=png&w=${windowSize < 768 ? '170' : '260'}`)} alt='math_puzzle' />
                </LazyLoad>
                <span>Math Puzzle</span>
            </div>
        </div>
        // </WaypointOnce >
    )
}

export default Content
