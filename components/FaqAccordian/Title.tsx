//@ts-nocheck


import React, { Fragment } from 'react';
import style from './style.module.scss';

const Title = (props) => {
    return(
        <div className={style.TabHeading}>
            {props.titles.map((tab,index) => {
                return(
                    <Fragment key={index}>
                        <div onClick={()=>props.setTab(tab.id)} className={`${style.Heading}`} key={index}>
                            <h4>
                                <span>
                                    <img src={"/static/Images/icons/angle-right.svg"} className={props.activeTab == tab.id ? style.openAcc : ''} alt="ic_accordian" />
                                </span>
                                {tab.id}
                            </h4>
                        </div>        
                        {props.activeTab == tab.id ? props.children : ''}  
                    </Fragment>
                )
            })}
        </div>
    )
}
export default Title


