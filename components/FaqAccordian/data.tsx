//@ts-nocheck
export const data = [
    {
        "id":"How is Rapid Application Development different from Agile?",
        "content": [
            {
                "list" : "<li>The working software or project is delivered quickly (mostly in weeks rather than months) in agile whereas in RAD no specific time-frame is recommended though the emphasis is on speed.</li><li>Simplicity is essential in agile. Whereas, RAD does not emphasize on reducing the work but proclaims in there will be less work in the long run.</li><li>In Agile, there is close cooperation between business and developers. In RAD, there is no equivalent cooperation as in agile though feedback from end users is critical in this process.</li>"
            }
        ]
    },
    {
        "id":"What is RAD model in SDLC?",
        "content":[
            {
                "title" : "The model distributes the phases into a series of short, iterative development cycles.",
                "list" : "<li>Business Modeling: The information flow is for different business channels. A complete analysis is performed to find vital business information on how to obtain, how to process, and what factors drive the successful flow of information.</li><li>Process Modeling: The data objects are then converted to establish the information flow required to achieve the business objectives. Any changes or enhancements to the data object sets is also defined here.</li><li>Application Generation: After the process modeling phase, the system is built and coded using automation tools to convert the process and data models into actual prototypes.</li><li>Testing & Turnover: The prototypes are tested individually during every iteration which drastically reduces the testing time in RAD.</li>"
            }
        ]
    },
    {
        "id":"Does RAD benefit only a startup?",
        "content":[
            {
                "title" : "RAD is helpful for startups as it reduces their development costs significantly. It also reduces the delivery time and thus helps them have the first mover advantage over their competitors. They can get the POC made by Intuz, launch the app in the market and finally develop the full solution later. No matter how big or small an organization is they can always benefit from RAD. It allows users in developing functional interactive prototypes of an application in only a few days or weeks, providing clients and business stakeholders the opportunity to stay involved in the entire development stage."
            },
        ]
    },
    {
        "id":"Is RAD suitable for creating small-scale applications only?",
        "content":[
            {
               "title" : "RAD allows users to create and deploy higher-quality apps significantly faster than the traditional platforms. Since it expedited the development and the delivery process, people believe that the platform is best fit to build and design applications with less code. Hence, can be used to create both small and large-scale applications."
            },
        ]
    },
    {
        "id":"How do RAD based apps perform in real-life situations?",
        "content":[
            {
              "title" : "The performance of RAD based apps would be similar to the web apps, mobile apps built or developed with custom or any other app development approach."
            },
        ]
    },
    {
        "id":"What is the capability of RAD approach for complex apps and architectures?",
        "content":[
            {
                "title" : "There are chances that when you develop an app, you require codes made from scratch. In such a case, we can use RAD approach to develop the application with the available libraries and pre-defined components and do a custom code for the part for which the libraries aren’t available. The extent to which such customization would be required can be evaluated during the first stage of RAD process. Hence, it is equally viable to use RAD for building complex apps and architectures."
            },
        ]
    },
]