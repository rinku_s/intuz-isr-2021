//@ts-nocheck

import React, { Fragment } from 'react';
import style from './style.module.scss';

const AccordianContent = (props) => {
    return(
        <div className={`${style.Content} currentcontent`}>
            {props.content && props.content.content.map((tab, index) => {
                return(
                    <Fragment key={index}>
                        {tab.title ? <p>{tab.title}</p> : '' }
                        {tab.list ? <ul dangerouslySetInnerHTML={{__html:tab.list}}></ul> : ''}
                    </Fragment>
                )
                
            })}
        </div>    
    )
}
export default AccordianContent