//@ts-nocheck

import React, { Component } from "react";
import AccordianContent from './AccordianContent';
import { data } from './data';
import Title from './Title';

export default class Accordian extends Component {
    constructor(props) {
        super(props)  
    }
    state = {
        activeTab: null,
        content: null
    }

    setTab = (id) => {
        if(id == null || id == this.state.activeTab){
            this.setState({ activeTab: null })
        }
        else {
            this.setState({ activeTab: id })
        }
    }

    componentDidUpdate = (prevProps, prevState) => {
        if(prevState.activeTab !== this.state.activeTab && this.state.activeTab !==null) {
            let content = data.find(e=>e.id===this.state.activeTab);
            this.setState({
                content: content
            });
        }
    };
    

    render() {
        let content = <AccordianContent content={this.state.content} />
        return(
            <Title 
                setTab={this.setTab} 
                activeTab={this.state.activeTab} 
                titles = {data}>
                {content}
            </Title>
        )
    }
}



