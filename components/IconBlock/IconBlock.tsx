//@ts-nocheck

//@ts-nocheck
import React, { Component } from 'react';
import AppIcon from '../AppIcon/AppIcon';
import styles from './iconblock.module.scss';

class IconBlock extends Component {
    constructor(props) {
        super(props);
        this.state = {};
    }
    componentDidMount() {
        this.setState({ windowSize: window.innerWidth });
    }
    render() {
        return (
            <div className={styles.IconBlock}>
                {this.props.icons.map((object, index) => {
                    const imgSrc = (this.state.windowSize <= 767) ? ((object.imgSrcXs) ? object.imgSrcXs : object.src) : object.src;
                    const variation = (this.state.windowSize <= 767) ? ((object.variationXs) ? object.variationXs : object.variation) : object.variation;
                    return (
                        <AppIcon
                            link={object.link}
                            imgSrc={imgSrc}
                            alt={object.alt}
                            title={object.title}
                            variation={variation}
                            key={index}
                        />
                    );
                })}
            </div>
        )
    }
}
export default IconBlock;