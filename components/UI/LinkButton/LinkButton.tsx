import Link from "next/link";
import React from "react";
import classes from "./styles.module.scss";

interface LinkButtonProps {
  variation?: string;
  className?: string;
  href?: string;
  onClick?: () => void;
  style?: React.CSSProperties;
  title?: string;
  as?: string;
  target?: string;
  rel?: string;
}

const LinkButton: React.FC<LinkButtonProps> = (props) => {
  // let variation = variation
  let href = props.as ? props.as : props.href;
  let link = (
    <Link href={href} prefetch={false}>
      <a
        target={props.target}
        rel={props.rel}
        className={`${classes.LinkButton} ${classes[props.variation]} ${
          props.className ? props.className : ""
        }`}
        // href={props.href}
        onClick={props.onClick}
        title={props.title}
        style={props.style}
      >
        {props.children}
      </a>
    </Link>
  );
  if (!props.href) {
    link = (
      <a
        target={props.target}
        rel={props.rel}
        className={`${classes.LinkButton} ${classes[props.variation]} ${
          props.className ? props.className : ""
        }`}
        onClick={props.onClick}
        title={props.title}
        style={props.style}
      >
        {props.children}
      </a>
    );
  }

  return <>{link}</>;
};

export default LinkButton;
