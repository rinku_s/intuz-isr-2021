import React from "react";
import { useState } from "react";
import classes from "./styles.module.scss";
interface TextFieldProps {
  type?: "text" | "email" | "password" | "number";
  onChange?: (
    e: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
  onBlur?: (
    e: React.FocusEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => void;
  touched?: boolean;
  error?: string;
  label: string;
  id: string;
  value: string;
  className?: string;
}

const TextField = (props: TextFieldProps) => {
  const [focus, setFocus] = useState(false);
  return (
    <div
      className={`${classes.TextField} ${focus ? classes.focus : ""} ${
        props.error && props.touched ? classes.error : ""
      } ${props.className || ""}`}
    >
      <label
        className={`${props.value.length !== 0 || focus ? classes.up : ""}`}
        htmlFor={props.id}
      >
        {props.label}
      </label>
      <input
        onFocus={() => setFocus(true)}
        name={props.id}
        id={props.id}
        type={props.type || "text"}
        onChange={props.onChange}
        onBlur={(e) => {
          setFocus(false);
          if (props.onBlur) {
            props.onBlur(e);
          }
        }}
        value={props.value}
      />
    </div>
  );
};

export const TextAreaField = (props: TextFieldProps) => {
  const [focus, setFocus] = useState(false);
  return (
    <div
      className={`${classes.TextField} ${focus ? classes.focus : ""} ${
        props.error && props.touched ? classes.error : ""
      } ${props.className || ""}`}
      style={{ height: "80px", paddingTop: "1rem" }}
    >
      <label
        className={`${props.value.length !== 0 || focus ? classes.up : ""}`}
        htmlFor={props.id}
      >
        {props.label}
      </label>
      <textarea
        onFocus={() => setFocus(true)}
        name={props.id}
        id={props.id}
        onChange={props.onChange}
        onBlur={(e) => {
          setFocus(false);
          if (props.onBlur) {
            props.onBlur(e);
          }
        }}
        value={props.value}
      ></textarea>
    </div>
  );
};

export default TextField;
