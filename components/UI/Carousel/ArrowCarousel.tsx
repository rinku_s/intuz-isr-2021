import React from 'react'
import Slider from "react-slick";
// import 'slick-carousel/slick/slick.css';
// import 'slick-carousel/slick/slick-theme.css'
const ArrowCarousel = (props) => {

    const settings = {
        dots: false,
        arrows:true,
        infinite: true,
        speed: 3000,
        slidesToShow: 5,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        ...props.settings
    }

    return (
        <>
        <Slider  {...settings } responsive={props.responsive}>
            {props.children}
        </Slider>
        </>
    )
}

export default ArrowCarousel
