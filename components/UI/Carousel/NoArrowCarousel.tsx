import React, { Component } from 'react';
import Slider from "react-slick";
// import 'slick-carousel/slick/slick.css';
import classes from './styles.module.scss';

export default class NoArrow extends Component {

    constructor(props) {
        super(props);
        this.state = {
            value: 0,

            settings: {
                dots: false,
                arrows: false,
                infinite: true,
                speed: 3500,
                centerMode: true,
                // centerPadding: '0rem',
                slide: "ul",
                className: classes.ara,
                slidesToShow: 2,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 0,
                cssEase: 'linear'
            }

        };
    }

    // we check if we got event from input (and it has target property) or just value from Carousel 
    // onChange = e => this.setState({ value: e.target ? e.target.value : e });
    render() {
        return (
            <Slider >
                {this.props.children}
            </Slider>
        )
    }
}
