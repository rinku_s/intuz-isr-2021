import React from 'react'
import { Spinner } from 'react-bootstrap'
const CustomSpinner = (props) => {
    return (
       <div style={{textAlign:'center', padding:"5rem 0"}}> 
        <Spinner animation="border" variant="info"/>
        <style>
            {`
                .spinner-border{
                    width:5rem;
                    height:5rem;
                }
            `}
        </style>
        </div>
        )
}

export default CustomSpinner
