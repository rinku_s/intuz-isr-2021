import { myLoader } from "config/image-loader";
import Image from "next/image";
import React from "react";
import classes from "./styles.module.scss";
const Nav = (props) => {
  return (
    <ul className={classes.Nav}>
      {props.process.map((dp, i) => {
        return (
          <li
            key={i}
            className={
              props.id ==
                dp.title
                  .replace(/[^a-z0-9\s]/gi, "")
                  .replace(/[ \s]/g, "")
                  .toLowerCase()
                ? `${classes.active} items-center`
                : " items-center"
            }
            onClick={() =>
              props.setCurrent(
                dp.title
                  .replace(/[^a-z0-9\s]/gi, "")
                  .replace(/[ \s]/g, "")
                  .toLowerCase()
              )
            }
          >
            {/*<LazyLoad once height={300} offset={300}>
              {!detEdgeOrIE() ? (
                <SVG src={dp.icon.url} alt={dp.title} />
              ) : (
                <img src={dp.icon.url} alt={dp.title} />
              )}
              </LazyLoad>*/}
            <Image loader={myLoader} layout='fixed' src={dp.icon.url} alt={dp.title} height={100} width={100} />

            <h4>{dp.title}</h4>
          </li>
        );
      })}
    </ul>
  );
};

export default Nav;
