import gsap, { Power1 } from "gsap";
import React, { useEffect, useState } from "react";
import CaseStudyParagraph from "../CaseStudyParagraph/CaseStudyParagraph";
import Nav from "./Nav";

// const data = [
//     {
//         id:1,
//         image:"https://intuzstrapi.s3.amazonaws.com/uploads/case_locations.svg",
//         title:"Analysis",
//         description:"We came up with a lot of value added suggestions that added a lot of business value to the application. It helped the client to visualize the details including the features that will be accessed by the users and how they will look for their preferred sports locations in their locality, similarly for sports facilities owners how they can list out their playground with availability details costs per hour, facilities they will provide, etc."
//     },
//     {
//         id:2,
//         image:"https://intuzstrapi.s3.amazonaws.com/uploads/case_locations.svg",
//         title:"Analysis",
//         description:"We came up with a lot of value added suggestions that added a lot of business value to the application. It helped the client to visualize the details including the features that will be accessed by the users and how they will look for their preferred sports locations in their locality, similarly for sports facilities owners how they can list out their playground with availability details costs per hour, facilities they will provide, etc."
//     },
//     {
//         id:3,
//         image:"https://intuzstrapi.s3.amazonaws.com/uploads/case_locations.svg",
//         title:"Analysis",
//         description:"We came up with a lot of value added suggestions that added a lot of business value to the application. It helped the client to visualize the details including the features that will be accessed by the users and how they will look for their preferred sports locations in their locality, similarly for sports facilities owners how they can list out their playground with availability details costs per hour, facilities they will provide, etc."
//     },
//     {
//         id:4,
//         image:"https://intuzstrapi.s3.amazonaws.com/uploads/case_locations.svg",
//         title:"Analysis",
//         description:"We came up with a lot of value added suggestions that added a lot of business value to the application. It helped the client to visualize the details including the features that will be accessed by the users and how they will look for their preferred sports locations in their locality, similarly for sports facilities owners how they can list out their playground with availability details costs per hour, facilities they will provide, etc."
//     },
//     {
//         id:5,
//         image:"https://intuzstrapi.s3.amazonaws.com/uploads/case_locations.svg",
//         title:"Analysis",
//         description:"We came up with a lot of value added suggestions that added a lot of business value to the application. It helped the client to visualize the details including the features that will be accessed by the users and how they will look for their preferred sports locations in their locality, similarly for sports facilities owners how they can list out their playground with availability details costs per hour, facilities they will provide, etc."
//     }
// ]

export interface CaseStudyDevProcessProps {
  dark: boolean;
  process:
  {
    title: string;
    icon: {
      url: string;
    };
  }[];
  descriptions:
  {
    current: string;
    discovery: string;
    design: string;
    development: string;
    maintenance: string;
  }
  className: string;

}

const CaseStudyDevProcess = (props: CaseStudyDevProcessProps) => {
  const [current, setCurrent] = useState(
    props.process[0]["title"]
      .replace(/[^a-z0-9\s]/gi, "")
      .replace(/[ \s]/g, "")
      .toLowerCase()
  );
  useEffect(() => {
    gsap.fromTo(
      ".desc",
      0.5,
      { autoAlpha: 0 },
      { autoAlpha: 1, ease: Power1.easeIn }
    );
  }, [current]);
  return (
    <>
      <Nav
        dark={props.dark}
        process={props.process}
        setCurrent={(id) => setCurrent(id)}
        id={current}
      />
      <CaseStudyParagraph
        style={{
          color: props.dark ? "#6B6B6B" : "",
          textAlign: props.dark ? "center" : "",
          whiteSpace: "break-spaces",
        }}
        className="desc"
        variation="large"
      >
        {props.descriptions[current]}
        {/* {ReactHtmlParser(`${props.descriptions[current]}`)} */}
      </CaseStudyParagraph>
    </>
  );
};

CaseStudyDevProcess.propTypes = {};

export default CaseStudyDevProcess;
