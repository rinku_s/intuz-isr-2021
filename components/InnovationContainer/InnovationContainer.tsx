//@ts-nocheck
//@ts-nocheck
import { myLoader } from 'config/image-loader'
import gsap from 'gsap'
import Image from 'next/image'
import React from 'react'
import Paragraph from '../../containers/AIPage/Headings/Paragraph'
import Secondary from '../../containers/AIPage/Headings/Secondary'
import ListItem from './ListItem'
import classes from './styles.module.scss'
const InnovationContainer = (props) => {
    const inf = React.useRef();

    function play() {
        const tl = gsap.timeline();
        tl.from(inf.current.getElementsByTagName('h4')[0], 0.6, { y: -20, autoAlpha: 0, ease: "Power1.easeOut" })
            .from(inf.current.getElementsByTagName('p'), 0.6, { y: -20, autoAlpha: 0, ease: "Power1.easeOut" })
        tl.from(inf.current.getElementsByTagName('h4')[1], 0.6, { y: -20, autoAlpha: 0, ease: "Power1.easeOut" })
            .staggerFrom(inf.current.getElementsByTagName('li'), 0.6, { y: 20, autoAlpha: 0, ease: "Power1.easeOut" }, 0.1);
    }

    return (


        <div className={classes.InnovationContainer} ref={inf}>
            <Secondary>{props.title}</Secondary>
            <Paragraph>{props.desc}</Paragraph>

            <div className={classes.Flex} style={{ flexDirection: `${props.reverse ? "row-reverse" : ""}` }}>

                <Image src={props.image.name} alt={props.title} loader={myLoader} layout={'intrinsic'} height={450} width={530} />
                <div>
                    <Secondary>{props.listtitle}</Secondary>
                    <ul>
                        {props.listitems.list.map((li, i) => (
                            <ListItem key={i}>{li}</ListItem>
                        ))}
                    </ul>
                </div>
            </div>
        </div>
        // </WaypointOnce >
    )
}

export default InnovationContainer
