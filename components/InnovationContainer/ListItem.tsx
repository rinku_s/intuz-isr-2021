//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
const ListItem = (props) => {
    return (
        <li className={classes.ListItem}>
           {props.children} 
        </li>
    )
}

export default ListItem
