//@ts-nocheck
//@ts-nocheck
import React, { useContext } from 'react';
import { Modal } from 'react-bootstrap';
import { ProcessingContext } from '../../context/processingContext';
import { reset, SubmitForm, useForm, validator } from '../../hooks/useForm';
import FormFields from '../CommonFormFields/FormFields';
import FormButton from '../UI/FormButton/FormButton';
import classes from './styles.module.scss';

const FormPopup = (props) => {
    const [form, setForm] = useForm();
    const { processing, setProcessing } = useContext(ProcessingContext);
    async function StartFormSubmission(data) {
        setProcessing(true);
        await SubmitForm(data, setProcessing, props.thankyoupage);

    }

    function submit(evt) {
        evt.preventDefault();
        if (form.formIsValid) {
            var data = new FormData();
            data.append("f_name", form.fields.f_name.value);
            data.append("l_name", form.fields.l_name.value);
            data.append("email", form.fields.email.value);
            data.append("phone", form.fields.phone.value);
            data.append("description", form.fields.project_brief.value);
            data.append("leadsource", props.leadsource);
            data.append("url", window.location.href);
            data.append("subject", props.subject);
            if (props.currentPackage) {
                data.append("package", `${props.currentPackage} - ${props.currentPackageAmount}`)
            }

            StartFormSubmission(data);
        } else {
            alert("Please enter valid data");
        }

    }
    return (
        <>
            <Modal show={props.show} onHide={props.onHide} className={classes.Modal}>
                <Modal.Header closeButton className={classes.ModalHeader}>
                    <Modal.Title className={classes.Title}>{props.title ? props.title : 'Get Started'}</Modal.Title>
                </Modal.Header>
                <Modal.Body className={classes.ModalBody}>
                    <div>
                        <form className="row" onSubmit={submit} autoComplete="off">
                            {props.label ? <div className={classes.Label} dangerouslySetInnerHTML={{ __html: props.label }}></div> : ''}
                            <FormFields form={form} inputChangedHandler={(evt, id) => validator(evt, id, form, setForm)} />
                            <FormButton
                                onReset={() => reset(form, setForm)}
                                isValid={form.formIsValid}
                                submitBtn='Send'
                                btnText='Clear'
                                variation='contactform'
                            />
                        </form>
                    </div>
                </Modal.Body>
            </Modal>
            <style jsx>
                {`
                    .modal-content{
                        background: #fff !important;
                    }

                    `}
            </style>
        </>
    )
}
export default FormPopup