//@ts-nocheck

import React from 'react';
import styles from './styles.module.scss';

const Detail = () => {
    return(
        <div className={styles.Description}>
            <h4>FAST TRACK</h4>
            <p>Can't wait to get started? If above form is not your preferred choice, email our team directly at  
                <a href="mailto:getstarted@intuz.com"> getstarted@intuz.com </a> 
                Or give us a call on <br/> 
                <a className={styles.phone} href="tel:+16504511499">
                    +1 650.451.1499</a>
            </p>
            <br />
            <h4>Idea and IP Protection</h4>
            <p>We value your unique ideas and IP. To ensure complete privacy, you can request a non-disclosure agreement. Just mention it in the message.</p>
        </div>
    )
}

export default Detail;