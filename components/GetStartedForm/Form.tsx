import TextField, { TextAreaField } from "components/UI/InputField/TextField";
import { useFormFormik } from "hooks/useFormFormik";
import router from "next/router";
import React from "react";
import { reset } from "../../hooks/useForm";
import FormButton from "../UI/FormButton/FormButton";
import classes from "./styles.module.scss";

const Form = (props) => {
  const onSuccess = () => {
    formik.resetForm();
    router.push("/success/thankyou");
  };
  const onError = () => {};
  const formik = useFormFormik(
    "get-started",
    "Get Started Request",
    onSuccess,
    onError
  );

  function submit(evt) {
    evt.preventDefault();
    // if (form.formIsValid) {
    //   var data = new FormData();
    //   data.append("f_name", form.fields.f_name.value);
    //   data.append("l_name", form.fields.l_name.value);
    //   data.append("email", form.fields.email.value);
    //   data.append("phone", form.fields.phone.value);
    //   data.append("description", form.fields.project_brief.value);
    //   data.append("leadsource", "get-started");
    //   data.append("url", window.location.href);
    //   data.append("subject", "Get Started Request");
    //   if (file !== null) {
    //     files.forEach((element, i) => {
    //       data.append("file", element);
    //     });
    //   }
    // }
  }

  const ref = React.useRef<HTMLInputElement>(null);
  return (
    <div>
      <form
        className="grid grid-cols-2 gap-10"
        onSubmit={formik.handleSubmit}
        autoComplete="off"
      >
        <TextField
          label={"First Name"}
          touched={formik.touched.f_name}
          error={formik.errors.f_name}
          value={formik.values.f_name}
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          id="f_name"
        />
        <TextField
          label={"Last Name"}
          touched={formik.touched.l_name}
          error={formik.errors.l_name}
          value={formik.values.l_name}
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          id="l_name"
        />
        <TextField
          className="col-span-full"
          label={"Email"}
          touched={formik.touched.email}
          error={formik.errors.email}
          value={formik.values.email}
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          id="email"
        />
        <TextField
          className="col-span-full"
          label={"Phone number"}
          touched={formik.touched.phone}
          error={formik.errors.phone}
          value={formik.values.phone}
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          id="phone"
        />
        <TextAreaField
          className="col-span-full"
          label={"Project Brief"}
          touched={formik.touched.description}
          error={formik.errors.description}
          value={formik.values.description}
          onBlur={formik.handleBlur}
          onChange={formik.handleChange}
          id="description"
        />
        <div className="col-span-full flex justify-between">
          <input
            ref={ref}
            type="file"
            className="hidden"
            multiple
            onChange={(e) => {
              let currentFiles = formik.values.files || [];
              let filesToPush = Array.from(e.target.files).filter((ftp) => {
                return (
                  currentFiles.findIndex((cf) => cf.name === ftp.name) === -1
                );
              });
              currentFiles.push(...filesToPush);
              formik.setValues((v) => {
                return {
                  ...v,
                  files: currentFiles,
                };
              });
              e.target.value = "";
            }}
          />
          <div className="text-16">
            <p className="text-left">Attach File(s) (Optional)</p>
            {formik.values.files && formik.values.files.length !== 0 && (
              <ul>
                {formik.values.files.map((c, i) => (
                  <li className="py-2 text-gray-66" key={i}>
                    <span
                      onClick={() => {
                        let newFiles = [...formik.values.files];
                        newFiles.splice(i, 1);

                        formik.setValues((v) => {
                          return {
                            ...v,
                            files: newFiles,
                          };
                        });
                      }}
                      className="w-8 h-8 inline-flex items-center justify-center cursor-pointer font-bold p-2"
                    >
                      x
                    </span>
                    {c.name}
                  </li>
                ))}
              </ul>
            )}
          </div>
          <p>
            <img
              onClick={() => {
                ref.current.click();
              }}
              src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAHcAAAAmCAMAAADJNfTqAAAAYFBMVEW5ydm5ydkAAAC5ydm5ydm5ydm5ydm5ydm5ydm5ydm5ydm5ydn////e5u3a4uvQ2+b5+/zH1OHx9fj2+Prs8fW/zt3L1+PD0d+8zNvp7fPV3+jv8/fj6fD7/P3T3efl6/HRy+EpAAAAC3RSTlNmmQDe27eHMBsJaa9p8/wAAAH4SURBVFjD7djpbqMwFAVgJl3Szj0GL9jGDmne/y3HuR6rTSycqBJBlXp+oBsCfBjMYrrdbrfvHpt9Mrvd2ws9Oi9vu+79mR6f5/fulbbIa/dEW+Spo23y6+ao+OGpnRVcPyLlNFEjK7heQig3Y6TVUrtn9ugopUektVK7meUc72vwgJ5uREE03cJ6Lo3+uvGyptRruOrM9hhSOSGs69bsYUr1CW59t2YFTrToDlC9BIL/dHsDmJ64HAGMA9cilXPtNthxarkmpOW18cUNMhJFGdhNy3otbSrH82xhKrfJtlzNayCwW/DcM3Ii+jxJCZXbYO3HsssMN4drLjhyvDinIzzvQ8P116yGvenOULk2hjhaXrhG3uxXGo7cBXtHe0VxeW5xByGRInh20y1H62hr9mpNOd5qr0AY7m7vCYk8mJot3Ewcj/lL8005vzN82Suf+5zj85tdt+wy2UOoU8VygrTlyLJrFvpzn9yQFxT8m+uWy1MAYbKmYslqHfnA9pTd0V5cv/xHvn61VEmUbI1ySLVZdufce110xGwdYQAERdmN1/erqMv9ygcAszVsCa7louvKbZHZbzz7vvv8PfzflKrYdV1rcIhqEKjYdV2aZpyjHVVZz+XYIUZFjbTcn/HevtG4bKtx6Fbj7s2+M/B3lT+PzN99Mv8BfofIHeZ/m8AAAAAASUVORK5CYII="
              alt="Upload Files"
            />
          </p>
        </div>
        <FormButton
          onReset={() => {
            formik.resetForm();
          }}
          isValid={formik.isValid}
          submitBtn="Send"
          btnText="Clear"
        />
      </form>
    </div>
  );
};
export default Form;
