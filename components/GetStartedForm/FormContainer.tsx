//@ts-nocheck

import React from "react";
import Container from "../Container/Container";
import Detail from "./Detail";
import Form from "./Form";
import styles from "./styles.module.scss";

const FormContainer = (props) => {
  return (
    <div className="my-32">
      <Container
        className={`${styles.FormContainer} grid gap-x-36`}
        style={{ gridTemplateColumns: "1.3fr 1fr" }}
      >
        <Form />
        <Detail />
      </Container>
    </div>
  );
};

export default FormContainer;
