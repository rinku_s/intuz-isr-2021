//@ts-nocheck
import React from 'react';
import classes from './heading.module.scss';
const Heading = (props) => {
    return (
        <h2 className={`${classes.title} ${props.className}`} style={{ ...props.style}}>
            {props.children}
        </h2>
        )
}

export default Heading
