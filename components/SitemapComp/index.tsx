//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
import Link from 'next/link'
const SitemapComp = (props) => {
    return (
        <div className={classes.SitemapComp}>
            <h5>{props.heading}</h5>
            <ul>
                {props.pages.map(pg=>(
                <li key={pg.name}>
                    <Link href={pg.link}>
                        <a>
                            {pg.name}
                        </a>
                    </Link>
                </li>
                ))}
            </ul>
        </div>
    )
}

export default SitemapComp
