//@ts-nocheck
//@ts-nocheck
import React from 'react';
import windowWidth from '../../hooks/windowWidth';
import ImageLoader from '../ImageLoader/ImageLoader';
import style from './styles.module.scss';

const ServiceBlock = (props) => {
    let windowSize = windowWidth();
    return (
        <div className={`col-md-6 ${style.Service} ${props.className}`}>
            <div>
                <div>
                    <ImageLoader src={`${props.backImage}?auto=format&fm=pjpg${windowSize < 768 ? '&w=767' : ''}`} alt={props.alt} />
                </div>
            </div>
            <div className={style.content}>
                <h3>{props.title}</h3>
                <p>{props.subTitle}</p>
            </div>
        </div>
    )
}

export default ServiceBlock;