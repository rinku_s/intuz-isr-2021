//@ts-nocheck



import React from 'react';
import style from './styles.module.scss';

const BlogBreadcrumb = (props) => {

    if(props.knowledge){
        return (
            <div className={style.breadcrumb}>
                <a style={props.style} href="/">Intuz</a><img src={"/static/Images/right-arrow.png"} alt='right arrow' />
                <a style={props.style} href="/knowledge-base">Knowledge Base</a>
                 { props.link &&  <> <img src={"/static/Images/right-arrow.png"} alt='right arrow' /> <a style={props.style} href={props.link}>{props.label}</a> </>  }
            </div>
        )
    } else{
        return (
            <div className={style.breadcrumb}>
                <a style={props.style} href="/">Intuz</a><img src={"/static/Images/right-arrow.png"} alt='right arrow' />
                <a style={props.style} href="/blog">Blog</a><img src={"/static/Images/right-arrow.png"} alt='right arrow' />
                 { props.link && <> <img src={"/static/Images/right-arrow.png"} alt='right arrow' /> <a style={props.style} href={props.link}>{props.label}</a> </> }
            </div>
        )
    }

}

export default BlogBreadcrumb
