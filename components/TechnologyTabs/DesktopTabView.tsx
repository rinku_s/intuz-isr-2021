//@ts-nocheck

import gsap from 'gsap';
import React from 'react';
import style from './styles.module.scss';

const DesktopTabView = (props) => {

    function play() {
        const tl = gsap.timeline();
        tl.fromTo('.currenttab', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5);
        tl.play();
    }

    return (

        <div className={`${style.TabHeading} currenttab`}>
            <div onClick={() => props.setTab("web")} className={`${style.Heading} ${props.activeTab === "web" ? style.activeTag : ''}`}>Web</div>
            <div onClick={() => props.setTab("mobile")} className={`${style.Heading} ${props.activeTab === "mobile" ? style.activeTag : ''}`}>Mobile</div>
            <div onClick={() => props.setTab("database")} className={`${style.Heading} ${props.activeTab === "database" ? style.activeTag : ''}`}>Database</div>
        </div>
    )
}
export default DesktopTabView


