//@ts-nocheck
export const data = [
    {
        "id":"web",
        "content":[
            {
                "title":"Front-End",
                "images" : [
                    {
                        "name" :  "ic_angular.jpg",
                        "title" : "Angular"
                    },
                    {
                        "name" :  "ic_reactnative.jpg",
                        "title" : "React"
                    },
                    {
                        "name" :  "ic_viuejs.png",
                        "title" : "Vue.js"
                    },
                    {
                        "name" :  "ic_backbone.png",
                        "title" : "Backbone.js"
                    },
                    {
                        "name" :  "ic_html5.png",
                        "title" : "HTML5"
                    },
                    {
                        "name" :  "ic_css3.png",
                        "title" : "CSS3"
                    }
                ]
            },
            {
                "title":"Back-End",
                "images" : [
                    {
                        "name" :  "ic_nodejs.png",
                        "title" : "Node.js"
                    },
                    {
                        "name" :  "ic_reactnative.jpg",
                        "title" : "React"
                    },
                    {
                        "name" :  "ic_meteor.png",
                        "title" : "Meteor"
                    },
                    {
                        "name" :  "ic_php.png",
                        "title" : "PHP"
                    },
                    {
                        "name" :  "ic_go.png",
                        "title" : "GO"
                    }
                ]
            }
        ]
    },

    {
        "id":"mobile",
        "content":[
            {
                "title":"Android",
                "images" : [
                    {
                        "name" :  "ic_java.jpg",
                        "title" : "Java"
                    },
                    {
                        "name" :  "ic_kotin.jpg",
                        "title" : "Kotlin"
                    }
                ]
            },
            {
                "title":"IOS",
                "images" : [
                    {
                        "name" :  "ic_swift.jpg",
                        "title" : "Swift"
                    },
                    {
                        "name" :  "ic_objective.png",
                        "title" : "Objective-C"
                    }
                ]
            },
            {
                "title":"Cross Platform",
                "images" : [
                    {
                        "name" :  "ic_reactnative.jpg",
                        "title" : "React Native"
                    }
                ]
            }
        ]
    },

    {
        "id":"database",
        "content":[
            {
                "title":"",
                "images" : [
                    {
                        "name" :  "ic_mongodb.png",
                        "title" : "MongoDB"
                    },
                    {
                        "name" :  "ic_mysql.png",
                        "title" : "MySQL"
                    },
                    {
                        "name" :  "ic_postgres.png",
                        "title" : "PostgreSQL"
                    },
                    {
                        "name" :  "ic_aurora.png",
                        "title" : "AuroraDB"
                    },
                    {
                        "name" :  "ic_dynamodb.png",
                        "title" : "DynamoDB"
                    }
                ]
            }
        ]
    }
]