//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap';
import React from "react";
import { Transition } from 'react-transition-group';
import style from './styles.module.scss';

const MobileTabView = (props) => {

    return (
        <div className={style.TabHeading}>

            <div onClick={() => props.setTab("web")}
                className={`${style.Heading} ${props.activeTab === "web" ? style.activeTag : ''}`}>
                Web
            </div>
            <Transition in={props.activeTab == "web"}
                timeout={1000}
                mountOnEnter={true}
                unmountOnExit={true}
                onEntering={(n) => {
                    gsap.fromTo(n, 1, { scaleY: 0.5 }, { scaleY: 1 });
                }}
                onExiting={(n) => {
                    gsap.fromTo(n, 0.3, { scaleY: 1 }, { scaleY: 0, autoAlpha: 0 });
                }}
            >
                {state => props.children}
            </Transition>
            {/* {props.activeTab == "web" ? showContent(props.children) : ''}   */}


            <div onClick={() => props.setTab("mobile")}
                className={`${style.Heading} ${props.activeTab === "mobile" ? style.activeTag : ''}`}>
                Mobile
            </div>
            <Transition in={props.activeTab == "mobile"}
                timeout={1000}
                mountOnEnter={true}
                unmountOnExit={true}
                onEntering={(n) => {
                    gsap.fromTo(n, 1, { scaleY: 0.5 }, { scaleY: 1 });
                }}
                onExiting={(n) => {
                    gsap.fromTo(n, 0.3, { scaleY: 1 }, { scaleY: 0, autoAlpha: 0 });
                }}
            >
                {state => props.children}
            </Transition>
            {/* {props.activeTab == "mobile" ? showContent(props.children) : ''} */}


            <div onClick={() => props.setTab("database")}
                className={`${style.Heading} ${props.activeTab === "database" ? style.activeTag : ''}`}>
                Database
            </div>
            <Transition in={props.activeTab == "database"}
                timeout={1000}
                mountOnEnter={true}
                unmountOnExit={true}
                onEntering={(n) => {
                    gsap.fromTo(n, 1, { scaleY: 0.5 }, { scaleY: 1 });
                }}
                onExiting={(n) => {
                    gsap.fromTo(n, 0.3, { scaleY: 1 }, { scaleY: 0, autoAlpha: 0 });
                }}
            >
                {state => props.children}
            </Transition>
            {/* {props.activeTab == "database" ? showContent(props.children, 'database') : ''} */}
        </div>
    )
}
export default MobileTabView


