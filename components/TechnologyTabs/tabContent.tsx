//@ts-nocheck

import React from 'react';
import style from './styles.module.scss';
import Image from "next/image";
import { myLoader } from 'config/image-loader';
const TabContent = (props) => {

    // function play(){
    //     const tl = gsap.timeline();
    //     tl.fromTo('.currentcontent', 0.5, {y:-20, opacity:0, ease: "power0.ease"}, {y:0, opacity:1, ease: "power0.ease"}, 0.5);
    //     tl.play();
    // }


    return (
        <div className={`${style.TabContent} currentcontent`}>
            {props.tabcontent && props.tabcontent.content.map((tab, index) => {
                return (
                    <div className={style.contentBlock} key={index}>
                        {tab.title ? <h4>{tab.title}</h4> : ''}
                        <div className="row">
                            {tab.images.map((img, index) => {
                                return (
                                    <div className={`col-md-4 col-sm-6 ${style.imgBlock}`} key={index}>
                                        <div>
                                            {/* {<Image layout="fixed" loader={myLoader} src={img.name} width="140" height="106"/>} */}
                                            {<img src={(`/static/Images/tech_icon/${img.name}`)} />}
                                            <span>{img.title}</span>
                                        </div>

                                    </div>
                                )
                            })}
                        </div>
                    </div>
                )
            })}
        </div>
    )
}
export default TabContent