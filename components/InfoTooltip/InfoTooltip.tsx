//@ts-nocheck

import React from 'react';
import { OverlayTrigger, Tooltip } from 'react-bootstrap';
const InfoTooltip = (props) =>{
    return(
        <>
            <OverlayTrigger
                trigger="hover"
                placement='right'
                overlay={
                    <Tooltip id={`tooltip-$'right'`}>
                        {props.children}
                    </Tooltip>
                }
                >
                <img src={"/static/Images/icons/info.svg"} alt='info.svg' />
            </OverlayTrigger>
            <style jsx>
            {`
                .tooltip-inner {
                    background-color:#fff;
                    color: #000;
                    font-size: 15px;
                    padding: 20px;
                    max-width: 300px;
                    text-align: left;
                }

                img {
                    margin : 0 0 5px 10px;
                    cursor: pointer;
                    width: 20px;
                    height: 20px;
                }    
            `}
            </style>
            </>
    )
}

export default InfoTooltip
 
  