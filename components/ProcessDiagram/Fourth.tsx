//@ts-nocheck
import React from 'react';
import { Media, MediaContextProvider } from "../../config/responsiveQuery";
import ImageContainer from './ImageContainer';
import InfoTooltip from './InfoTooltip';
import Points from './Points';


const points = [
    ` <p>We always ensure that all of the system’s functionalities work properly and across all common platforms. We ensure that all of the system specifications have been developed properly and that the client’s goals for the project have been realized. Before projects conclude, clients have the opportunity to test the robustness of their new system or app as a “beta” version or Release Candidate in a testing environment. If the system requires data migration or integration with an existing system, we will strategize the process for efficient execution. </p>
    <br/>
    <strong>Following steps are expected:</strong>
    <br/>
    <ul>
        <li>We will gather all the necessary data from your company associates in order to launch the Release Candidate</li>
        <li>We will then streamline said data (make it compatible with the parameters of a web services (if applicable)</li>
        <li>We will define a strategy on how to synchronize data between the current and future system (if required)</li>
        <li>We will define a strategy on how to synchronize data between the current and future system (if required)</li>
        <li>We will define a conclusion and find most feasible solution or extracting current data (if required)</li>
        <li>Integration of current data with developed system (if required)</li>
        <li>Deployment on live environment and procure feedback from the current working team</li>
    </ul>`,


    `The Intuz team will deploy the finalized system on a live or closed server (depending on client preference and project requirements) and train the company representative to use both the user interface and the administrative system. Future plans for next-phase development and a maintenance strategy are also decided at the end of this phase. `,


    `<strong>Advance</strong>
    <p><strong>Growing the business via analysis, optimization and enhancement.</strong><br/>After deploying the system or app, we can help the client grow its business via empirical analysis, system optimization and enhancement.</p>
    <p>System usage drives the future needs and Intuz understands it very well. Intuz has a systematic approach on distinguishing system refinement and enhancement. In either case, Intuz will stand with the client as a strategic partner to surpass client’s expectations on their success and business growth.</p>
    <p>Intuz believes in constant growth and advancement based on empirical data. Empirical data can be derived from various sources and has to be analyzed intelligently. Intuz has vast experience in efficiently driving and implementing the strategy based on the conclusion from user data and analytics. Intuz’s unparalleled technological insight coupled with your system knowledge can bring a great value to your business growth.</p>
    <p>Intuz has a special division called “Agile Growth Studio” dedicated to execute ongoing enhancements after project is completed. Separate division ensures proper attention to their smaller sub-projects and cost effectiveness.</p>`,


    ` Our programmers develop the system parallel to the design process until the two merge. This approach dramatically reduces the time required to finalize and launch the project.
    <br/>
    <ul>
        <li>Intuz will develop a complete system or app and associated sub-systems for your company </li>
        <li>System or app will be carefully developed considering each domain’s requirements and feasibility </li>
        <li>Intuz will try to incorporate all the future needs of the User and Administrators </li>
        <li>At the end of this phase a completely working system will be ready for testing and deployment </li>
        <br/>
        <p>
            <strong>Project Progress</strong>
            <br/>
            Our programmers develop the system parallel to the design process until the two merge. This approach dramatically reduces the time required to finalize and launch the project.
           <br/>
           - Scheduling an in-person meeting with our project manager
           <br/>
           - Calling our project manager
           <br/>
           - Using our friendly online communication system
        </p>
    </ul>`

]


const Fourth = () => {
    return (
        <MediaContextProvider>
        <div>

        <ImageContainer mSrc={'mobile_launch.jpg'} src={'process_review.png'} heading="Implement operative objectives that fulfill an underlying goal">
            <InfoTooltip style={{top: "25%",
                        left: "3%",}}>
            <p>We always ensure that all of the system’s functionalities work properly and across all common platforms. We ensure that all of the system specifications have been developed properly and that the client’s goals for the project have been realized. Before projects conclude, clients have the opportunity to test the robustness of their new system or app as a “beta” version or Release Candidate in a testing environment. If the system requires data migration or integration with an existing system, we will strategize the process for efficient execution. </p>
            <br/>
            <strong>Following steps are expected:</strong>
            <br/>
            <ul>
                <li>We will gather all the necessary data from your company associates in order to launch the Release Candidate</li>
                <li>We will then streamline said data (make it compatible with the parameters of a web services (if applicable)</li>
                <li>We will define a strategy on how to synchronize data between the current and future system (if required)</li>
                <li>We will define a strategy on how to synchronize data between the current and future system (if required)</li>
                <li>We will define a conclusion and find most feasible solution or extracting current data (if required)</li>
                <li>Integration of current data with developed system (if required)</li>
                <li>Deployment on live environment and procure feedback from the current working team</li>
            </ul>
            </InfoTooltip>
            <InfoTooltip style={{bottom: "25%",
                                left: "48.8%",}}>
                The Intuz team will deploy the finalized system on a live or closed server (depending on client preference and project requirements) and train the company representative to use both the user interface and the administrative system. Future plans for next-phase development and a maintenance strategy are also decided at the end of this phase. 
            </InfoTooltip>
            <InfoTooltip style={{bottom:"15%", left:"48.8%"}}>
                <strong>Advance</strong>
                <br/>
                <br/>
                <p><strong>Growing the business via analysis, optimization and enhancement.</strong><br/>After deploying the system or app, we can help the client grow its business via empirical analysis, system optimization and enhancement.</p>
                <p>System usage drives the future needs and Intuz understands it very well. Intuz has a systematic approach on distinguishing system refinement and enhancement. In either case, Intuz will stand with the client as a strategic partner to surpass client’s expectations on their success and business growth.</p>
                <p>Intuz believes in constant growth and advancement based on empirical data. Empirical data can be derived from various sources and has to be analyzed intelligently. Intuz has vast experience in efficiently driving and implementing the strategy based on the conclusion from user data and analytics. Intuz’s unparalleled technological insight coupled with your system knowledge can bring a great value to your business growth.</p>
                <p>Intuz has a special division called “Agile Growth Studio” dedicated to execute ongoing enhancements after project is completed. Separate division ensures proper attention to their smaller sub-projects and cost effectiveness.</p>
            </InfoTooltip>
            <InfoTooltip style={{top: "32%",
    right: "3.5%",}}>
            Our programmers develop the system parallel to the design process until the two merge. This approach dramatically reduces the time required to finalize and launch the project.
            <br/>
            <ul>
                <li>Intuz will develop a complete system or app and associated sub-systems for your company </li>
                <li>System or app will be carefully developed considering each domain’s requirements and feasibility </li>
                <li>Intuz will try to incorporate all the future needs of the User and Administrators </li>
                <li>At the end of this phase a completely working system will be ready for testing and deployment </li>
                <br/>
                <p>
                    <strong>Project Progress</strong>
                    <br/>
                    Our programmers develop the system parallel to the design process until the two merge. This approach dramatically reduces the time required to finalize and launch the project.
                   <br/>
                   - Scheduling an in-person meeting with our project manager
                   <br/>
                   - Calling our project manager
                   <br/>
                   - Using our friendly online communication system
                </p>
            </ul>
            </InfoTooltip>

            <Media lessThan="sm">
                <Points points={points}/>
            </Media>
        </ImageContainer>
        </div>
        </MediaContextProvider>
    )
}

export default Fourth
