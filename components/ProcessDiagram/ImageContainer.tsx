//@ts-nocheck
//@ts-nocheck
import React from 'react';
import ImageBlock from '../../components/ImageBlock/ImageBlock';
import { cdn } from '../../config/cdn';
import { Media, MediaContextProvider } from "../../config/responsiveQuery";
import classes from './styles.module.scss';
const ImageContainer = (props) => {
    return (
        <MediaContextProvider>
            <div className={classes.MainContainer}>
                <h2>{props.heading}</h2>
                <div className={`${classes.ProcessDiagram} ${props.className}`}>
                    <Media greaterThanOrEqual="sm">
                        <ImageBlock src={cdn(props.src)} alt={props.alt} dwidth={1110} mwidth={767} format={'png'} />
                    </Media>
                    <Media lessThan="sm">
                        <ImageBlock src={cdn(props.mSrc)} alt={props.alt} dwidth={1110} mwidth={767} format={'pjpg'} />
                    </Media>
                    {props.children}
                </div>
            </div>
        </MediaContextProvider>
    )
}

export default ImageContainer
