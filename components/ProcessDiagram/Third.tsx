//@ts-nocheck
import React from 'react';
import { Media, MediaContextProvider } from "../../config/responsiveQuery";
import ImageContainer from './ImageContainer';
import InfoTooltip from './InfoTooltip';
import Points from './Points';
const points=[
    `The design process is comprehensive and begins with an in-depth questionnaire to ensure our understanding and the client’s vision are in sync. We work closely with the client to realize their vision. Intuz’s best-in-the industry designers give special attention to even the smallest aspects of the application or interface.`
]
const Third = () => {
    return (
        <MediaContextProvider>
            <ImageContainer mSrc={'mobile_design.jpg'} src={'process_designscope.png'} heading="Sensible design is the foundation of impregnable development.">
                <InfoTooltip style={{top:"13%", right:"3.6%"}}>
                The design process is comprehensive and begins with an in-depth questionnaire to ensure our understanding and the client’s vision are in sync. We work closely with the client to realize their vision. Intuz’s best-in-the industry designers give special attention to even the smallest aspects of the application or interface.
                </InfoTooltip>
                <Media lessThan="sm">
                    <Points points={points}/>
                </Media>
            </ImageContainer>
        </MediaContextProvider>
    )
}

export default Third
