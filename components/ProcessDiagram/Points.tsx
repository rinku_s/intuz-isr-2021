//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
import Container from '../Container/Container'
const Points = (props) => {
    return (
    <Container>

        <ol className={classes.Points}>
            {props.points.map((point,i)=>(
                <li key={i} dangerouslySetInnerHTML={{__html:point}} ></li>
                ))}
        </ol>
    </Container>
    )
}

export default Points
