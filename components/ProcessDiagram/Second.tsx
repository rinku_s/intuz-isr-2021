//@ts-nocheck
import React from 'react';
import { Media, MediaContextProvider } from "../../config/responsiveQuery";
import ImageContainer from './ImageContainer';
import InfoTooltip from './InfoTooltip';
import Points from './Points';
const points=[
    `
    This is the first step of the development process. Intuz will elaborate on each previously determined requirement and finalize the precise set of system functionalities. This might include such things as weeding out user interface issues in the client’s existing system and automating any manual or unnecessary processes. Depending on the scope of the project, this phase may also include project wireframes, layouts, or user flow diagrams. This phase will be concluded by finalizing a detailed system specification and amendments to the initial proposal. 
    <br/><br/>
    <strong>Following steps are expected:</strong>
    <ul>
        <li>Complete requirement gathering and understanding of current manual working system (if applicable)</li>
        <li>Subsequent meetings with a Intuz associate will culminate in a finalized list of features for the future system </li>
        <li>This process will require open communication concerning current manual process, pitfalls in current processes, </li>
        <li>enhancement requests, suggestion for future systems and conclusions considering web and mobile domain feasibility</li>
        <li>At the end of subsequent meetings, we will finalize the future system’s initial phase requirements </li>
        <li>Intuz will present a feasible system development plan</li>
    </ul> 
    `
]
const Second = () => {
    return (
        <MediaContextProvider>
            <ImageContainer mSrc={'mobile_discovery.jpg'} src={'process_discovery.png'} heading="Defining user interaction through mutual vision alignment.">
                <InfoTooltip style={{left:"3.8%",top:"30%"}}>
                This is the first step of the development process. Intuz will elaborate on each previously determined requirement and finalize the precise set of system functionalities. This might include such things as weeding out user interface issues in the client’s existing system and automating any manual or unnecessary processes. Depending on the scope of the project, this phase may also include project wireframes, layouts, or user flow diagrams. This phase will be concluded by finalizing a detailed system specification and amendments to the initial proposal. 
                <br/><br/>
                <strong>Following steps are expected:</strong>
                <ul>
                    <li>Complete requirement gathering and understanding of current manual working system (if applicable)</li>
                    <li>Subsequent meetings with a Intuz associate will culminate in a finalized list of features for the future system </li>
                    <li>This process will require open communication concerning current manual process, pitfalls in current processes, </li>
                    <li>enhancement requests, suggestion for future systems and conclusions considering web and mobile domain feasibility</li>
                    <li>At the end of subsequent meetings, we will finalize the future system’s initial phase requirements </li>
                    <li>Intuz will present a feasible system development plan</li>
                </ul>
                </InfoTooltip>
                <Media lessThan="sm">
                    <Points points={points}/>
                </Media>
            </ImageContainer>
        </MediaContextProvider>
    )
}

export default Second
