//@ts-nocheck
import React from 'react'
import { OverlayTrigger, Tooltip } from 'react-bootstrap'
import classes from './styles.module.scss'
const InfoTooltip = (props) => {
    return (
        <OverlayTrigger
        trigger="hover"
        overlay={
            <Tooltip className={classes.Tooltip} id="popover-contained">{props.children}</Tooltip>
        }
        placement="bottom"
        >
        <button className={`${classes.TooltipBtn} ${props.className}`} style={props.style}><img src={"/static/Images/process/info.png"} alt="Info Tooltip"/></button>
        </OverlayTrigger>
    
    )
}

export default InfoTooltip
