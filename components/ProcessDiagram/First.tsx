//@ts-nocheck
import React from 'react';
import { Media, MediaContextProvider } from "../../config/responsiveQuery";
import ImageContainer from './ImageContainer';
import InfoTooltip from './InfoTooltip';
import Points from './Points';
const First = () => {

    const points = [
        "Intuz establishes the relationship by laying out all the objectives of the project in discussion. Our business analysts, designers, and developers take the time to understand your current business model and strategic objectives along with functional or financial limitations, if any. In this phase, we also clarify the expectations of the client with regard to Intuz’s capabilities, expertise, and service models.",
        "After laying out the client’s business and strategic objectives, Intuz provides technological and functional insight to the client’s team. Intuz enlightens the client about current web and mobile technology usage trends, user interface strategies and technological possibilities and limitations. At this time we also establish a solid game plan for the project’s possible future phases. We establish a timeline for the project and determine the appropriate technologies we will employ. ",
        "Once Intuz has clarified requirements based on the client’s objectives, Intuz works internally to evaluate functional, procedural, and financial constraints. At the end of this phase Intuz proposes a comprehensive and precise solution that fits into the client’s functional and financial needs. ",
        "Intuz explains its proposed plan to the client. Upon client approval, Intuz will initiate the project implementation. "
    ]


    return (
        <MediaContextProvider>
            <ImageContainer mSrc={'mobile_projectplans.jpg'} src={'process_projectplan.png'} heading="Mutual inspiration through experience and quantitative research.">
                    <InfoTooltip style={{top:"20%", left:"4.6%"}}>
                    Intuz establishes the relationship by laying out all the objectives of the project in discussion. Our business analysts, designers, and developers take the time to understand your current business model and strategic objectives along with functional or financial limitations, if any. In this phase, we also clarify the expectations of the client with regard to Intuz’s capabilities, expertise, and service models.
                    </InfoTooltip>
                    <InfoTooltip style={{top:"43%", left:"4.6%"}}>
                        After laying out the client’s business and strategic objectives, Intuz provides technological and functional insight to the client’s team. Intuz enlightens the client about current web and mobile technology usage trends, user interface strategies and technological possibilities and limitations. At this time we also establish a solid game plan for the project’s possible future phases. We establish a timeline for the project and determine the appropriate technologies we will employ. 
                    </InfoTooltip>
                    <InfoTooltip style={{top:"56%", left:"4.6%"}}>
                        Once Intuz has clarified requirements based on the client’s objectives, Intuz works internally to evaluate functional, procedural, and financial constraints. At the end of this phase Intuz proposes a comprehensive and precise solution that fits into the client’s functional and financial needs. 
                    </InfoTooltip>
                    <InfoTooltip style={{top:"64%", left:"4.6%"}}>
                        Intuz explains its proposed plan to the client. Upon client approval, Intuz will initiate the project implementation. 
                    </InfoTooltip>
                    <div>
                        
                    </div>
                    <Media lessThan="sm">
                        <Points points={points}/>
                    </Media>
            </ImageContainer>
        </MediaContextProvider>
    )
}

export default First

