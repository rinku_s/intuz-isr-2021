//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap'
import React, { useEffect } from 'react'
import ReactDOM from 'react-dom'
import { Transition } from 'react-transition-group'

const Modal = props => {

    useEffect(() => {
        document.addEventListener('keydown', escape, false);
        return () => {
            document.removeEventListener('keydown', escape, false);
        }
    }, [])

    function escape(event) {
        if (event.keyCode === 27) {
            props.onHide();
        }
    }

    function onEnter(n) {
        let tl = gsap.timeline();
        tl.from(n, { autoAlpha: 0, duration: 0.3 })
            .from('.inner-content', { scale: 0, autoAlpha: 0, duration: 0.5 });
    }
    function onExit(n) {
        let tl = gsap.timeline();
        tl.to('.inner-content', { scale: 0, autoAlpha: 0, duration: 0.5 })
            .to(n, { autoAlpha: 0, duration: 0.3 });
    }

    if (process.browser) {
        return ReactDOM.createPortal(
            <Transition in={props.show} mountOnEnter unmountOnExit onEnter={onEnter} onExit={onExit} timeout={800}>
                {state => <div className="myModal" onClick={props.onHide}>
                    <div className="inner-content">
                        {props.children}
                    </div>
                </div>
                }
            </Transition>
            , document.querySelector('#myModal'))
    } else {
        return null
    }
}

export default Modal