//@ts-nocheck
import React, { useState } from 'react';
import classes from './styles.module.scss';
const Opening = ({ apply_link, position_name }) => {

    const [show, setShow] = useState(false);

    return (
        <div className={`${classes.Opening} opening`} onMouseOver={()=>setShow(true)} onMouseLeave={()=>setShow(false)}>
                <p>
                    {position_name}
                </p>
            {<a target="_blank" rel="noopener nofollow" href={apply_link} style={{opacity: show ? 1 : 0}}>
                Apply Now
            </a>}
        </div>
    )
}

export default Opening
