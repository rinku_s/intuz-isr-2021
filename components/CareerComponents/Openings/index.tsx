//@ts-nocheck
import React from 'react'
import Opening from './Opening';
import classes from './styles.module.scss';
const Openings = ({openings}) => {
    return (
        <div className={classes.Openings}>
            {openings.map((opening, i)=>(
                <Opening key={i} {...opening.career}/>
            ))}
        </div>
    )
}

export default Openings
