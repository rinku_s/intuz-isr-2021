//@ts-nocheck
import React from 'react';
import { cdn } from '../../../config/cdn';
import 'lazysizes';
import classes from './styles.module.scss';
const Perk = ({ icon_image, description }) => {
    return (
        <div className={classes.Perk + " col-4 col-md-3 my-5 perk"}>
            <img className="lazyload img-fluid" data-src={cdn(icon_image.name)} alt={description}/>
            <p>{description}</p>
        </div>
    )
}

export default Perk
