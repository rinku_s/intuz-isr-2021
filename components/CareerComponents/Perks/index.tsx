//@ts-nocheck
import React from 'react'
import Perk from './Perk'

const Perks = ({perks}) => {
    return (
        <div className="row" style={{marginBottom:"9rem"}}>
            {perks.map((perk,i)=>(
                // <div key={i} className="">
                    <Perk key={i} {...perk}/>
                // </div>
            ))}
        </div>
    )
}

export default Perks
