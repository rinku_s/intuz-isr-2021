//@ts-nocheck
import React from 'react';
import { cdn } from '../../../config/cdn';
import classes from './styles.module.scss';
const IntuzValue = ({ title, description,image }) => {
    return (
        <div className={classes.IntuzValue}>
            <div>
                <img className="lazyload" data-src={cdn(image.name+"?auto=format,compress")} alt={title}/>
            </div>
            <div>
            <h4>{title}</h4>
            <p>{description}</p>
            </div>
        </div>
    )
}

export default IntuzValue
