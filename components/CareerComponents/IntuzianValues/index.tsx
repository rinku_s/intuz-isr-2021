import React from "react";
import SwiperCore, { Autoplay } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.min.css";
import IntuzValue from "./IntuzValue";
SwiperCore.use([Autoplay]);
const IntuzValues = ({ intuzian_values }) => {
  const settings: Swiper = {
    autoHeight: true,
    loop: true,
    autoplay: {
      delay: 2000,
      disableOnInteraction: true,
    },
  };
  return (
    <div style={{ marginTop: "10rem" }}>
      <Swiper {...settings}>
        {intuzian_values.map((int_val) => (
          <SwiperSlide key={int_val.title}>
            <IntuzValue {...int_val} />
          </SwiperSlide>
        ))}
      </Swiper>
    </div>
  );
};

export default IntuzValues;
