//@ts-nocheck
import React from 'react';
import { cdn } from '../../../config/cdn';
import classes from './styles.module.scss';
const EmployeeQuote = ({ author, author_image, designation, quote }) => {
    return (
        <div className={classes.EmployeeQuote}>
            <div className={classes.Image}>
                <img style={{width:"100%"}} src={cdn(author_image.name)} alt={author}/>
                <div className={classes.Quote}>
                    <img src={cdn('quote_c3345e254c.png')} alt="Quote"/>
                </div>
            </div>
            <blockquote>{quote}</blockquote>
            <p><span>{author}</span></p>
            <p>{designation}</p>
        </div>
    )
}

export default EmployeeQuote
