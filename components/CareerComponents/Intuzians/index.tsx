//@ts-nocheck
import React from "react";
import Intuzian from "./Intuzian";

const Intuzians = ({ intuzians }) => {
  return (
    <div
      className="flex flex-col sm:flex-row flex-wrap sm:justify-center"
      style={{ marginTop: "5rem", marginBottom: "8rem" }}
    >
      {intuzians.map((intuzian, i) => (
        <Intuzian key={i} {...intuzian} />
      ))}
    </div>
  );
};

export default Intuzians;
