//@ts-nocheck
import React from 'react';
import { cdn } from '../../../config/cdn';
import classes from './styles.module.scss';
const Intuzian = ({ description, icon_image }) => {
    return (
        <div className={`${classes.Intuzian} intuzians`}>
            <img className="lazyload img-fluid" data-src={cdn(icon_image?.name)} alt={description}/>
            <h5>{description}</h5>
        </div>
    )
}

export default Intuzian
