//@ts-nocheck
//@ts-nocheck
import axios from "axios";
import { Formik } from "formik";
import React, { useRef, useState } from "react";
import { Modal } from "react-bootstrap";
import * as Yup from "yup";
import classes from "./styles.module.scss";
// import LinkButton from "../../UI/LinkButton/LinkButton";
const FormPopup = (props) => {
  const formdata = {
    firstname: "",
    lastname: "",
    emailid: "",
    mobile: "",
    companyname: "",
    experince: "",
    currentsalary: "",
    expectedsalary: "",
    positionapply: "",
  };
  const [form, setFormdata] = useState(formdata);
  const [files, setFiles] = useState();
  const [buttonDisabled, setButtondisabled] = useState(false);
  const [filename, setFilename] = useState("");
  const [fileError, setFileError] = useState(false);
  console.log("file", files);
  const getFileList = (event) => {
    setFiles(event.target.files[0]);
    setFilename(event.target.files[0].name);
    setFileError(false);
  };
  /*const removeFile = (event) => {
    setFiles();
  };*/
  const handleClickSubmit = (data) => {
    let valide = validation();
    if (valide) {
      setButtondisabled(true);
      let fData = new FormData();
      fData.append("data", JSON.stringify(data));
      fData.append("files.resume", files, files.name);
      StartFormSubmission(fData);
    }
  };
  const validation = () => {
    let validate = true;
    if (filename === "") {
      validate = false;
      setFileError(true);
    }
    return validate;
  };
  const showFileUpload = (event) => {
    fileUpload.current.click();
  };
  const StartFormSubmission = async (data) => {
    try {
      const result = await axios.post(
        "https://strapi.relispace.com/mail/careers",
        data,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
      console.log(result);
      props.onHide();
      setButtondisabled(false);
      setFileError(false);
      setFilename("");
      setFiles();
    } catch (error) {
      setButtondisabled(false);
      throw error;
    }
  };
  const fileUpload = useRef();
  return (
    <>
      <Modal
        show={props.show}
        onHide={props.onHide}
        className={classes.Modal}
        size="lg"
      >
        <Modal.Header closeButton className={classes.ModalHeader}>
          <Modal.Title className={classes.Title}>Apply for job</Modal.Title>
        </Modal.Header>
        <Modal.Body className={classes.ModalBody}>
          <div>
            {/* <Modal.Title className={classes.Title}>Apply for job</Modal.Title> */}
            <div className={`col-12 text-center mb-5`}>
              <input
                ref={fileUpload}
                style={{ display: "none" }}
                type="file"
                // value={files.name !== undefined ? files.name }
                onChange={getFileList}
                multiple={false}
              />
              <button onClick={showFileUpload} className={classes.uploadButton}>
                Upload Resume/CV
              </button>
              <p className={classes.filename}>{filename}</p>
              {fileError && (
                <div
                  className="errorMsg "
                  style={{
                    fontSize: "12px",
                    textAlign: "center",
                    color: "red",
                    fontweight: "600",
                  }}
                >
                  "Please upload your resume"
                </div>
              )}
            </div>
            <Formik
              initialValues={{ ...form }}
              onSubmit={handleClickSubmit}
              validationSchema={Yup.object().shape({
                firstname: Yup.string().required(
                  "Please Enter your first name"
                ),
                positionapply: Yup.string().required(
                  "Please Enter Position apply for"
                ),
                lastname: Yup.string().required("Please Enter your last name"),
                emailid: Yup.string()
                  .email()
                  .required("Please Enter your email address"),
                mobile: Yup.number()
                  .typeError("please Enter number")
                  .required("Please Enter your mobile number"),
                companyname: Yup.string().required(
                  "Please Enter your Current/Previous Company"
                ),
                experince: Yup.number().required(
                  "Please Enter your Experience"
                ),
                currentsalary: Yup.string().required(
                  "Please Enter your current salary"
                ),
                expectedsalary: Yup.string().required(
                  "Please Enter your expected salary"
                ),
              })}
            >
              {(props) => {
                const {
                  values,
                  touched,
                  errors,
                  // dirty,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  setFieldValue,
                  // handleReset,
                } = props;

                return (
                  <form noValidate onSubmit={handleSubmit} className="row mt-3">
                    <div
                      className={`col-12 col-md-6 col-sm-12 col-lg-6 col-xs-12 ${classes.InputContainer}`}
                    >
                      {/* <label>First Name</label> */}
                      <input
                        value={values.firstname}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="firstname"
                        className={classes.Input}
                        placeholder="First Name"
                        type="text"
                      />
                      {errors.firstname && touched.firstname ? (
                        <div
                          className="errorMsg "
                          style={{
                            fontSize: "12px",
                            textAlign: "left",
                            color: "red",
                            fontweight: "600",
                          }}
                        >
                          {errors.firstname}
                        </div>
                      ) : null}
                    </div>
                    <div
                      className={`col-12 col-md-6 col-sm-12 col-lg-6 col-xs-12 ${classes.InputContainer}`}
                    >
                      {/* <label>Last Name</label> */}
                      <input
                        value={values.lastname}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="lastname"
                        className={classes.Input}
                        placeholder="Last Name"
                        type="text"
                      />
                      {errors.lastname && touched.lastname ? (
                        <div
                          className="errorMsg "
                          style={{
                            fontSize: "12px",
                            textAlign: "left",
                            color: "red",
                            fontweight: "600",
                          }}
                        >
                          {errors.lastname}
                        </div>
                      ) : null}
                    </div>
                    <div
                      className={`col-12 col-md-6 col-sm-12 col-lg-6 col-xs-12 ${classes.InputContainer}`}
                    >
                      {/* <label>Mobile Number</label> */}
                      <input
                        value={values.mobile}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="mobile"
                        className={classes.Input}
                        placeholder="Mobile Number"
                        type="number"
                      />
                      {errors.mobile && touched.mobile ? (
                        <div
                          className="errorMsg "
                          style={{
                            fontSize: "12px",
                            textAlign: "left",
                            color: "red",
                            fontweight: "600",
                          }}
                        >
                          {errors.mobile}
                        </div>
                      ) : null}
                    </div>
                    <div
                      className={`col-12 col-md-6 col-sm-12 col-lg-6 col-xs-12 ${classes.InputContainer}`}
                    >
                      {/* <label>Email Address</label> */}
                      <input
                        value={values.emailid}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="emailid"
                        className={classes.Input}
                        placeholder="Email Address"
                        type="email"
                      />
                      {errors.emailid && touched.emailid ? (
                        <div
                          className="errorMsg "
                          style={{
                            fontSize: "12px",
                            textAlign: "left",
                            color: "red",
                            fontweight: "600",
                          }}
                        >
                          {errors.emailid}
                        </div>
                      ) : null}
                    </div>
                    <div
                      className={`col-12 col-md-6 col-sm-12 col-lg-6 col-xs-12 ${classes.InputContainer}`}
                    >
                      {/* <label>Current/Previous Company</label> */}
                      <input
                        value={values.companyname}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="companyname"
                        className={classes.Input}
                        placeholder="Current/Previous Company"
                        type="text"
                      />
                      {errors.companyname && touched.companyname ? (
                        <div
                          className="errorMsg "
                          style={{
                            fontSize: "12px",
                            textAlign: "left",
                            color: "red",
                            fontweight: "600",
                          }}
                        >
                          {errors.companyname}
                        </div>
                      ) : null}
                    </div>
                    <div
                      className={`col-12 col-md-6 col-sm-12 col-lg-6 col-xs-12 ${classes.InputContainer}`}
                    >
                      {/* <label>Experience (in years)</label> */}
                      <input
                        value={values.experince}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="experince"
                        className={classes.Input}
                        placeholder="Experience (in years)"
                        type="text"
                      />
                      {errors.experince && touched.experince ? (
                        <div
                          className="errorMsg "
                          style={{
                            fontSize: "12px",
                            textAlign: "left",
                            color: "red",
                            fontweight: "600",
                          }}
                        >
                          {errors.experince}
                        </div>
                      ) : null}
                    </div>
                    <div
                      className={`col-12 col-md-6 col-sm-12 col-lg-6 col-xs-12 ${classes.InputContainer}`}
                    >
                      {/* <label>Current Salary</label> */}
                      <input
                        value={values.currentsalary}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="currentsalary"
                        className={classes.Input}
                        placeholder="Current Salary"
                        type="number"
                        required
                      />
                      {errors.currentsalary && touched.currentsalary ? (
                        <div
                          className="errorMsg "
                          style={{
                            fontSize: "12px",
                            textAlign: "left",
                            color: "red",
                            fontweight: "600",
                          }}
                        >
                          {errors.currentsalary}
                        </div>
                      ) : null}
                    </div>
                    <div
                      className={`col-12 col-md-6 col-sm-12 col-lg-6 col-xs-12 ${classes.InputContainer}`}
                    >
                      {/* <label>Expected Salary</label> */}
                      <input
                        value={values.expectedsalary}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="expectedsalary"
                        className={classes.Input}
                        placeholder="Expected Salary"
                        type="number"
                      />
                      {errors.expectedsalary && touched.expectedsalary ? (
                        <div
                          className="errorMsg "
                          style={{
                            fontSize: "12px",
                            textAlign: "left",
                            color: "red",
                            fontweight: "600",
                          }}
                        >
                          {errors.expectedsalary}
                        </div>
                      ) : null}
                    </div>
                    <div
                      className={`col-12 col-md-6 col-sm-12 col-lg-6 col-xs-12 ${classes.InputContainer}`}
                    >
                      {/* <label>Expected Salary</label> */}
                      <input
                        value={values.positionapply}
                        onChange={handleChange}
                        onBlur={handleBlur}
                        name="positionapply"
                        className={classes.Input}
                        placeholder="Position apply for"
                        type="text"
                      />
                      {errors.positionapply && touched.positionapply ? (
                        <div
                          className="errorMsg "
                          style={{
                            fontSize: "12px",
                            textAlign: "left",
                            color: "red",
                            fontweight: "600",
                          }}
                        >
                          {errors.positionapply}
                        </div>
                      ) : null}
                    </div>
                    <div className={`col-12 text-center mt-4`}>
                      <button
                        type="submit"
                        className={classes.applyButton}
                        disabled={buttonDisabled}
                      >
                        Apply Now
                      </button>
                    </div>
                  </form>
                );
              }}
            </Formik>
          </div>
        </Modal.Body>
      </Modal>
    </>
  );
};
export default FormPopup;
