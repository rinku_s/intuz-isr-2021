//@ts-nocheck
import React, { useState } from "react";
import FormPopup from "./index";
// import LinkButton from "../../UI/LinkButton/LinkButton";
import classes from "./styles.module.scss";
const FormPopupButton = (props) => {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  return (
    <>
      <span className={classes.uploadbutton}  onClick={handleShow}>
        {props.children}
      </span>
      <FormPopup show={show} onHide={handleClose} />
    </>
  );
};

export default FormPopupButton;
