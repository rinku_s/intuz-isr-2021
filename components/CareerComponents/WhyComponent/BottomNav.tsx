//@ts-nocheck
import React from 'react';
import { cdn } from '../../../config/cdn';
import classes from './styles.module.scss';
const BottomNavIcon = ({ title, icon, active, setIndex }) => {
    return (
        <div className={classes.BottomNavIcon + (active ? ' ' + classes.active : '')}>
            <p>{title}</p>
            <div>
                <img src={cdn(icon?.name)} alt={title} onClick={setIndex} />
            </div>
        </div>
    )
}

export default BottomNavIcon
