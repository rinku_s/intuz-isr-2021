//@ts-nocheck
import React, { useState } from "react";
import BottomNavIcon from "./BottomNav";
import WhyComp from "./WhyComponent";
import classes from "./styles.module.scss";
import { Carousel } from 'react-bootstrap';
import NewWhyComponent from "./NewWhyComponent";
import { MediaContextProvider, Media } from "config/responsiveQuery";
import WhyComponentMobile from "./WhyComponentMobile";
// import { cdn } from "config/cdn";
// import VisibilitySensor from "react-visibility-sensor";
const WhyComponent = ({ why_component, time }) => {

  const [current, setActiveIndex] = useState(0)
  const handleSelect = (selectedIndex, e) => {
    setActiveIndex(selectedIndex);
}
  return (
    <MediaContextProvider>

    <div className={classes.Main}>
      <div className={classes.BottomNav}>
        {why_component.map((w, i) => (
          <BottomNavIcon {...w} active={current === i} key={w.title} setIndex={() => setActiveIndex(i)} />
          ))}
      </div>
      <Media greaterThanOrEqual="sm">
        <Carousel fade controls={false} indicators={false} activeIndex={current} onSelect={handleSelect} className={classes.Carousel}>
            {why_component.map((why,i)=>(
              <Carousel.Item key={i}>
              <NewWhyComponent {...why} index={i}/>
              </Carousel.Item>
            ))}
        </Carousel>
      </Media>
      <Media lessThan="sm">
      <Carousel touch={true} fade controls={false} indicators={false} activeIndex={current} onSelect={handleSelect} className={classes.Carousel}>
            {why_component.map((why,i)=>(
              <Carousel.Item key={i}>
              <WhyComponentMobile {...why} index={i}/>
              </Carousel.Item>
            ))}
        </Carousel>
      </Media>
    </div>
    </MediaContextProvider>
  );
};

export default WhyComponent;
