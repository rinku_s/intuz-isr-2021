//@ts-nocheck
import React from "react";
import Container from "components/Container/Container";
import { cdn } from "config/cdn";
import classes from "./styles.module.scss";
const NewWhyComponent = ({
  image,
  title,
  description,
  active,
  mobile_background,
  background_color,
  index,
}) => {
  let bp, bs;
  // let style = `background-size: 120% 150%;
  // background-position: -277px -341px;`
  // if(index==3) {
  //     bs = "120% 150%";
  //     bp ="-277px -341px";
  // } else if(index == 1) {
  //     bs = "120% 150%";
  //     bp = "-270px 100%";
  // } else if(index == 0) {
  //     bs = "110%";
  //     bp = "-128px -293px";
  // }  else if(index == 2) {
  //     bs = "109% 136%";
  //     bp = "-120px -270px";
  // }

  return (
    <div
      className={classes.NewWhyComponent}
      style={{
        backgroundImage: `url(${cdn(image?.name + "?auto=format,compress")})`,
        // backgroundPosition:bp,
        // backgroundSize:bs
      }}
    >
      <Container className="flex items-center">
        <div className={classes.Content}>
          <h3>{title}</h3>
          <p>{description}</p>
        </div>
      </Container>
    </div>
  );
};

export default NewWhyComponent;
