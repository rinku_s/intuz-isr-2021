//@ts-nocheck
//@ts-nocheck
import gsap from "gsap";
import React, { useEffect, useState } from "react";
import { Transition } from "react-transition-group";
import { cdn } from "../../../config/cdn";
import classes from "./styles.module.scss";
const WhyComp = ({
  image,
  title,
  description,
  active,
  mobile_background,
  background_color,
  index,
}) => {
  const [mobile, setMobile] = useState(false);

  function onEnter(n) {
    gsap.fromTo(n.get, { autoAlpha: 0 }, { autoAlpha: 1, duration: 500 });
    gsap.set(n, { opacity: 0 });
    gsap.set("#whyintuz", { backgroundColor: background_color });
  }

  function onEntering(n) {
    gsap.fromTo(
      n.getElementsByClassName("cl"),
      { autoAlpha: 0 },
      { autoAlpha: 1, duration: 0.5, ease: "power1.out" }
    );
    //    gsap.fromTo(n.getElementsByTagName('img'), { opacity:0, }, { opacity:1, duration:0.3, ease:"power1.out" });
  }

  function onExit(n) { }
  function onExiting(n) {
    gsap.set(n, { position: "absolute", opacity: 1, xPercent: -1000 });
    gsap.fromTo(n, { autoAlpha: 1 }, { autoAlpha: 0, duration: 0.3 });
  }

  useEffect(() => {
    let mobile = window.innerWidth < 768;
    setMobile(mobile);
  }, []);

  useEffect(() => {
    if (window.innerWidth < 768) {
      gsap.set("#whyintuz", { backgroundColor: background_color });
    } else {
    }
  }, [active]);

  return (
    <Transition
      in={active}
      unmountOnExit
      mountOnEnter
      onExiting={onExiting}
      // onEntering={onEntering}
      timeout={{ enter: 500, exit: 500 }}
    >
      <div
        className={
          mobile ? classes.WhyCompMobile : `container ${classes.WhyComp}`
        }
      >
        <picture>
          <source
            media="(max-width:767px)"
            srcSet={cdn(mobile_background.name + "?auto=format,compress")}
          />
          <img
            className="img"
            src={cdn(image.name + "?auto=format,compress")}
            alt={title}
          />
        </picture>
        <div className={classes[`cl content__${index}`]} id="co">
          {/* {mobile && <span style={{ backgroundColor: background_color }}></span>} */}
          <h3>{title}</h3>
          <p>{description}</p>
        </div>
      </div>
    </Transition>
  );
};

export default WhyComp;
