//@ts-nocheck
import gsap from "gsap";
import React from "react";
import classes from "./styles.module.scss";
const WhyNotIntuz = ({ title, description, isEven, i, className }) => {
  return (
    <div className={`${classes.WhyNotIntuz} ${className}`}>
      <h5>{title}</h5>
      <p>{description}</p>
    </div>
    //
  );
};

export default WhyNotIntuz;
