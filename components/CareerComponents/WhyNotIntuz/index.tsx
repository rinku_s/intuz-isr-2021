//@ts-nocheck
import React from "react";
import WhyNotIntuz from "./WhyNotIntuz";
import classes from "./styles.module.scss";

const WhyNotPoints = ({ why_not_points }) => {
  return (
    <div id="whyNot" className={classes.WhyNotPoints}>
      {why_not_points.map((why_not, i) => (
        <WhyNotIntuz key={i} isEven={i % 2 == 0} {...why_not} i={i}/>
      ))}
    </div>
  );
};

export default WhyNotPoints;
