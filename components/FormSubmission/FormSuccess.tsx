//@ts-nocheck
//@ts-nocheck
import Head from 'next/head';
import Router from 'next/router';
import React from 'react';
import LinkButton from '../UI/LinkButton/LinkButton';
import classes from './styles.module.scss';
const FormSuccess = () => {
    // gtag('event', 'conversion', {'send_to': 'AW-962036523/zs0zCN2EpaEBEKuG3soD'});
    const gtag = `gtag('event', 'conversion', {'send_to': 'AW-962036523/zs0zCN2EpaEBEKuG3soD'});`

    return (
        <div className={classes.FormSubmission}>
            <Head>
                {/* <script dangerouslySetInnerHTML={{__html:gtag}}></script> */}
            </Head>
            <h1>Thank You</h1>
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2" width="250px">
                <circle className={`${classes.path} ${classes.circle}`} fill="none" stroke="#73AF55" strokeWidth="6" strokeMiterlimit="10" cx="65.1" cy="65.1" r="62.1" />
                <polyline className={`${classes.path} ${classes.check}`} fill="none" stroke="#73AF55" strokeWidth="6" strokeLinecap="round" strokeMiterlimit="10" points="100.2,40.2 51.5,88.8 29.8,67.5 " />
            </svg>
            <p><strong>
                Thank you for requesting quote for your project requirements
                </strong></p>
            <p>
                One of our representatives will get in touch with you shortly.
                </p>
            <p>
                If you need to talk to us about your project requirements immediately, feel free to call us on <a href="tel:+16504511499">+1 650-451-1499</a>
            </p>
            <p>
                Thank You,<br />
                    INTUZ TEAM.
                </p>
            <LinkButton variation="blue" color="#FFF" onClick={() => Router.push('/')}>Go Home</LinkButton>
        </div>
    )
}

export default FormSuccess
