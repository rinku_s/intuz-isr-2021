//@ts-nocheck
//@ts-nocheck
import Router from 'next/router';
import React from 'react';
import TextBlock from '../TextBlock';
import { Button } from '../UI/Button';
import classes from './styles.module.scss';
const FormError = () => {
    return (
        <div className={classes.FormSubmission}>
            <h1 style={{ textAlign: 'center', color: 'purple', fontSize: '6rem' }}>Oh Snap!</h1>
            <svg version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 130.2 130.2" width="250px">
                <circle className={`${classes.path} ${classes.circle}`} fill="none" stroke="#D06079" strokeWidth="6" strokeMiterlimit="10" cx="65.1" cy="65.1" r="62.1" />
                <line className={`${classes.path} ${classes.line1}`} fill="none" stroke="#D06079" strokeWidth="6" strokeLinecap="round" strokeMiterlimit="10" x1="34.4" y1="37.9" x2="95.8" y2="92.3" />
                <line className={`${classes.path} ${classes.line2}`} fill="none" stroke="#D06079" strokeWidth="6" strokeLinecap="round" strokeMiterlimit="10" x1="95.8" y1="38" x2="34.4" y2="92.2" />
            </svg>
            <TextBlock><strong>
                Something Went Terribly wrong during processing your request.
                </strong></TextBlock>
            <TextBlock>
                We will fix it in a short time.
                </TextBlock>
            <TextBlock>
                Until then, If you need to talk to us about your project requirements immediately, feel free to call us on +1 650-451-1499
                </TextBlock>
            <TextBlock>
                Thank You,<br />
                    INTUZ TEAM.
                </TextBlock>
            <Button btnType="blueBtn" color="#FFF" onClick={() => Router.push('/')}>Go Home</Button>
        </div>
    )
}

export default FormError
