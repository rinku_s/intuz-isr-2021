//@ts-nocheck


import React from 'react';
import { cdn } from '../../config/cdn';

const Ethic = (props) => {
    return(
        <div className="col-md-6 col-sm-6">
            <div><img src={cdn(`${props.icon.name}?auto=format`)} alt={props.icon.name} /></div>
            <h3>{props.name}</h3>
        </div>
    )
}

export default Ethic