//@ts-nocheck

import React from 'react';
import Ethic from './Ethic';
import style from './styles.module.scss';

const Content = (props) => {
    return(
        <div className={`row ${style.EthicContent}`}>
             {props.content.map((ethic, index) => {
                return(
                    <Ethic name={ethic.name} icon={ethic.icon} key={index} />
                )
            })}
        </div>
    )
}

export default Content