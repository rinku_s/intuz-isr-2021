//@ts-nocheck
//@ts-nocheck
import React from 'react';
import { cdn } from '../../config/cdn';
import AppLink from '../AppLink/AppLink';
import ImageBlock from '../ImageBlock/ImageBlock';
import Carousel from './Carousel';
import classes from './styles.module.scss';
const JustLaunched = (props) => {
    return (
        <div className={` ${classes.JustLaunched}`}>
            <div className={` ${classes.logo}`}>
                <ImageBlock className="img-fluid" src={cdn('feature-app-live4it-logo.png')} alt="Live4it" format="png" mwidth={162} dwidth={262} />
                <AppLink center link={{ android: "https://play.google.com/store/apps/details?id=com.live4it", ios: "https://itunes.apple.com/gb/app/live4itlocations/id1364907490?mt=8&ign-mpt=uo%3D4" }} />
            </div>
            {/* <LazyLoad offset={300} height={300} > */}
            <Carousel />
            {/* </LazyLoad> */}
        </div>
    )
}

export default JustLaunched
