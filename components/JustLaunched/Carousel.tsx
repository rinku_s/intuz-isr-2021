//@ts-nocheck
//@ts-nocheck
import React, { Component } from 'react';
import Slider from 'react-slick';
import { cdn } from '../../config/cdn';
import Item from './Item';
// import 'slick-carousel/slick/slick.css';
import classes from './styles.module.scss';

export default class Carousel extends Component {

    settings = {
        dots: false,
        infinite: true,
        speed: 1000,
        arrows: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        ease: "linear",
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    infinite: true,
                }
            },
        ]

    }


    render() {
        return (
            <div className={` ${classes.Carousel}`}>
                <div className={classes.buttons}>
                    <button onClick={() => this.slider.slickPrev()}><img src={"/static/Images/left-c-arr.png"} alt="Left Arrow" /></button>
                    <button onClick={() => this.slider.slickNext()}><img src={"/static/Images/right-c-arr.svg"} alt="Right Arrow" /></button>
                </div>
                <Slider adaptiveHeight={true} ref={c => this.slider = c} {...this.settings}>
                    <Item image={cdn("just-launch-c-1.jpg")} alt="Events Detail" desc="Discover what's happening in your nearby places" />
                    <Item image={cdn("just-launch-c-2.jpg")} alt="Events Detail" desc="Get recommendations that match your location & Interests" />
                    <Item image={cdn("just-launch-c-3.jpg")} alt="Events Detail" desc="With just a click get all the details of Locations" />
                    <Item image={cdn("just-launch-c-4.jpg")} alt="Events Detail" desc="Never miss your favourite events nearby you" />
                    <Item image={cdn("just-launch-c-5.jpg")} alt="Events Detail" desc="Explore and pick what you’re interested in" />
                </Slider>
            </div>
        )
    }
}
