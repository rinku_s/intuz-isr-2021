//@ts-nocheck
//@ts-nocheck
import React from 'react';
import ImageBlock from '../ImageBlock/ImageBlock';
import classes from './styles.module.scss';
const Item = (props) => {
    return (
        <div className={classes.Item}>
            <div>
                <ImageBlock src={props.image} alt={props.alt} mwidth={205} dwidth={305} />
            </div>
            <p>{props.desc}</p>
        </div>
    )
}

export default Item
