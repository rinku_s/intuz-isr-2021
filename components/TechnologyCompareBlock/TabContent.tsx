//@ts-nocheck

import React from 'react';
import classes from './styles.module.scss';

const TabContent = (props) => {
    return (
        <div className={classes.container}>
            <table className='table table-borderless'>
                <thead>
                    <tr>
                        {
                            props.data[props.active][0]['title'].map((title, index) => {
                                return(
                                    <th key={index}>{title}</th>
                                )
                            })
                        }
                    </tr>
                </thead>
                <tbody>
                    {
                        props.data[props.active][0]['content'].map((content) => {
                            return(
                                <tr key = {content.index}>
                                    <td data-th = {props.data[props.active][0]['title'][0]}>{content.point}</td>
                                    <td data-th = {props.data[props.active][0]['title'][1]}>{content.com1}</td>
                                    <td data-th = {props.data[props.active][0]['title'][2]}>{content.com2}</td>
                                </tr>
                            )
                        })
                    }
                </tbody>
            </table>
        </div>
    )
}

export default TabContent