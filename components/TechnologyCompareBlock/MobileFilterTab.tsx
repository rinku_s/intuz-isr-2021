//@ts-nocheck
import React from "react";
import classes from "./styles.module.scss";
const MobileFiterTabs = (props) => {
  return (
    <div className={classes.FilterTabs}>
      {props.activePage == "react" ? (
        <>
          <li
            className={props.active === "rva" ? classes.active : ""}
            onClick={() => props.setFilter("rva")}
          >
            React JS vs Angular JS
          </li>
          {props.active === "rva" ? (
            <li className={classes.activeList}>{props.children}</li>
          ) : (
            ""
          )}
          <li
            className={props.active === "rvv" ? classes.active : ""}
            onClick={() => props.setFilter("rvv")}
          >
            React JS vs Vue JS
          </li>
          {props.active === "rvv" ? (
            <li className={classes.activeList}>{props.children}</li>
          ) : (
            ""
          )}
        </>
      ) : (
        <>
          <li
            className={props.active === "nva" ? classes.active : ""}
            onClick={() => props.setFilter("nva")}
          >
            Node JS vs Angular JS
          </li>
          {props.active === "nva" ? (
            <li className={classes.activeList}>{props.children}</li>
          ) : (
            ""
          )}

          <li
            className={props.active === "nvj" ? classes.active : ""}
            onClick={() => props.setFilter("nvj")}
          >
            Node JS vs JavaScript
          </li>
          {props.active === "nvj" ? (
            <li className={classes.activeList}>{props.children}</li>
          ) : (
            ""
          )}

          <li
            className={props.active === "nvp" ? classes.active : ""}
            onClick={() => props.setFilter("nvp")}
          >
            Node JS vs Python
          </li>
          {props.active === "nvp" ? (
            <li className={classes.activeList}>{props.children}</li>
          ) : (
            ""
          )}

          <li
            className={props.active === "nvph" ? classes.active : ""}
            onClick={() => props.setFilter("nvph")}
          >
            Node JS vs PHP
          </li>
          {props.active === "nvph" ? (
            <li className={classes.activeList}>{props.children}</li>
          ) : (
            ""
          )}
        </>
      )}
    </div>
  );
};

export default MobileFiterTabs;
