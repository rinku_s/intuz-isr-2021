//@ts-nocheck
//@ts-nocheck
import React, { Component } from 'react';
import { Media, MediaContextProvider } from "../../config/responsiveQuery";
import FiterTabs from './FiterTabs';
import MobileFiterTabs from './MobileFilterTab';
import TabContent from './TabContent';

export default class TechCompareBlock extends Component {

    state = {
        active: this.props.activePage == 'react' ? 'rva' : 'nva',
        nodeData: {
            nva: [
                {
                    'title': ['Comparison Pointer', 'Node JS', 'AngularJS',],
                    'content': [
                        {
                            'index': 1,
                            'point': 'Built Using',
                            'com1': 'C, C++, JavaScript',
                            'com2': 'JavaScript'
                        },
                        {
                            'index': 2,
                            'point': 'Application Support',
                            'com1': 'Client & Server Side Applications',
                            'com2': 'Single Page Client Side Application'
                        },
                        {
                            'index': 3,
                            'point': 'Suitability',
                            'com1': 'Ideal for faster, scalable and high performing application projects',
                            'com2': 'Suitable for creating real-time applications'
                        },
                        {
                            'index': 4,
                            'point': 'Flexibility',
                            'com1': 'Supports multiple frameworks like Express.js, Partial.js, etc.',
                            'com2': 'Angular JS is a web application framework in itself.'
                        },
                    ]
                }
            ],
            nvj: [
                {
                    'title': ['Comparison Pointer', 'Node JS', 'JavaScript',],
                    'content': [
                        {
                            'index': 1,
                            'point': 'Type',
                            'com1': 'Runtime environment for JavaScript with useful libraries',
                            'com2': 'Programming language'
                        },
                        {
                            'index': 2,
                            'point': 'Supported Operation Engine',
                            'com1': 'Supports Google V8 Engine for faster performance and agility',
                            'com2': 'Supports multiple engines like JavaScript Core, V8 & Spider Monkey'
                        },
                        {
                            'index': 3,
                            'point': 'Utility',
                            'com1': 'Performing non-blocking operations on any operating system',
                            'com2': 'Performing client-side activity for a web application'
                        },
                    ]
                }
            ],
            nvp: [
                {
                    'title': ['Comparison Pointer', 'Node JS', 'Python',],
                    'content': [
                        {
                            'index': 1,
                            'point': 'Type',
                            'com1': 'Open-source cross platform runtime environment',
                            'com2': 'General purpose high-level programming language'
                        },
                        {
                            'index': 2,
                            'point': 'Utility',
                            'com1': 'Specialized use case for server side application',
                            'com2': 'Can be used for general programming, gaming, desktop app development'
                        },
                        {
                            'index': 3,
                            'point': 'Real-time Support',
                            'com1': 'Highly-efficient in maintaining and managing large volumes of user data in real-time',
                            'com2': 'Useful for projects requiring real-time data processing'
                        },
                    ]
                }
            ],
            nvph: [
                {
                    'title': ['Comparison Pointer', 'Node JS', 'PHP',],
                    'content': [
                        {
                            'index': 1,
                            'point': 'Type',
                            'com1': 'JavaScript based runtime environment',
                            'com2': 'Server side scripting language'
                        },
                        {
                            'index': 2,
                            'point': 'Suitability',
                            'com1': 'High-end real-time web applications',
                            'com2': 'General web development projects'
                        },
                        {
                            'index': 3,
                            'point': 'Maintenance',
                            'com1': 'Complete frontend/backend support for easy maintenance',
                            'com2': 'Requires different backend/frontend mechanisms for complete maintenance'
                        },
                    ]
                }
            ]
        },
        reactData: {
            rva: [
                {
                    'title': ['Comparison Pointer', 'ReactJS', 'AngularJS',],
                    'content': [
                        {
                            'index': 1,
                            'point': 'Type',
                            'com1': 'Library',
                            'com2': 'Framework'
                        },
                        {
                            'index': 2,
                            'point': 'Architecture',
                            'com1': 'ReactJS only works on the ‘View’ Layer',
                            'com2': 'AngularJS has complete MVC functionality'
                        },
                        {
                            'index': 3,
                            'point': 'Suitability',
                            'com1': 'ReactJS is suitable for managing rendering on server-side apps',
                            'com2': 'AngularJS can render on both client and server side apps. (Angular 2 & above for server-side support)'
                        },
                        {
                            'index': 4,
                            'point': 'Flexibility',
                            'com1': 'There are many ways to complete the same task on ReactJS making it extremely flexible',
                            'com2': 'AngularJS has well-defined boundaries and best practices which promotes scalability but makes it less flexible.'
                        },
                    ]
                }
            ],
            rvv: [
                {
                    'title': ['Comparison Pointer', 'ReactJS', 'Vue JS',],
                    'content': [
                        {
                            'index': 1,
                            'point': 'Type',
                            'com1': 'JavaScript Library',
                            'com2': 'Open Source JavaScript Framework'
                        },
                        {
                            'index': 2,
                            'point': 'Model Architecture',
                            'com1': 'Virtual DOM',
                            'com2': 'Virtual DOM HTML-based Templates'
                        },
                        {
                            'index': 3,
                            'point': 'Suitability',
                            'com1': 'Web & Native App Development',
                            'com2': 'Primarily focused on Web App Development'
                        },
                        {
                            'index': 4,
                            'point': 'Reusability',
                            'com1': 'Only CSS',
                            'com2': 'Maximum Reusability'
                        },
                    ]
                }
            ],
        }
    }

    setFilter = (id) => {
        this.setState({
            active: id
        })
    }

    componentDidUpdate = (prevProps, prevState) => {
        //   if(prevState.active !== this.state.active) {
        //       TweenLite.fromTo(this.con, 0.5, {opacity:0.1, ease:Power1.easeIn},{opacity:1, ease:Power1.easeIn})
        //   }
    };


    render() {
        let data = this.props.activePage == 'react' ? this.state.reactData : this.state.nodeData;
        let tabContent = <TabContent data={data} active={this.state.active} />
        return (
            <MediaContextProvider>
                <Media greaterThanOrEqual="sm">
                    <FiterTabs setFilter={this.setFilter} active={this.state.active} activePage={this.props.activePage} />
                    {tabContent}
                </Media>
                <Media lessThan="sm">
                    <MobileFiterTabs setFilter={this.setFilter} active={this.state.active} activePage={this.props.activePage}>
                        {tabContent}
                    </MobileFiterTabs>
                </Media>
            </MediaContextProvider>
        )
    }
}

