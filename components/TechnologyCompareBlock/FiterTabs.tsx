//@ts-nocheck
import { gsap } from 'gsap';
import React from 'react';
import classes from './styles.module.scss';
const FiterTabs = (props) => {

    // setOpacity(`.${classes.FilterTabs}`);

    function play() {
        const tl = gsap.timeline({ paused: true });
        tl.fromTo(`.${classes.FilterTabs}`, 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.5)
        tl.play();
    }

    return (

        <div className={classes.FilterTabs}>
            {
                props.activePage == 'react' ?
                    <>
                        <li className={props.active === 'rva' ? classes.active : ''} onClick={() => props.setFilter('rva')}>React JS vs Angular JS</li>
                        <li className={props.active === 'rvv' ? classes.active : ''} onClick={() => props.setFilter('rvv')}>React JS vs Vue JS</li>
                    </> :
                    <>
                        <li className={props.active === 'nva' ? classes.active : ''} onClick={() => props.setFilter('nva')}>Node JS vs Angular JS</li>
                        <li className={props.active === 'nvj' ? classes.active : ''} onClick={() => props.setFilter('nvj')}>Node JS vs JavaScript</li>
                        <li className={props.active === 'nvp' ? classes.active : ''} onClick={() => props.setFilter('nvp')}>Node JS vs Python</li>
                        <li className={props.active === 'nvph' ? classes.active : ''} onClick={() => props.setFilter('nvph')}>Node JS vs PHP</li>
                    </>
            }
        </div>
        // </WaypointOnce >
    )
}

export default FiterTabs
