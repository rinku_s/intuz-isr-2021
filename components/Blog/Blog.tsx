//@ts-nocheck
import React from 'react'
import Container from '../Container/Container';
import 'lazysizes';
import classes from './styles.module.scss';
import './blog.css';
const Blog = (props) => {
    return (
        <Container className={classes.Blog}>
            {props.children}
            <style jsx>
                {`
                
                .rights{
                        list-style: none!important;
                        text-align: left;
                    }
                    ul.rights li::before{
                        content: "";
                        background: url("https://intuz-site.imgix.net/uploads/check_arrow-blog.png") no-repeat center center;
                        margin-top: -1rem;
                    }
                `}
            </style>
        </Container>


    )
}

export default Blog
