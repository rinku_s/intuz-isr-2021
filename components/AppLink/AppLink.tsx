//@ts-nocheck
import React from 'react';
import { cdn } from '../../config/cdn';
import classes from './styles.module.scss';

const AppLink = (props) => {
    return (
        <div className={classes.links} style={{justifyContent:props.align, marginRight: 0}}>
            {
                props.link.ios && 
                (props.link.ios == '#' ? <a className={classes.white}><img src={cdn('ios-ic.svg')} alt="iOS Icon"/></a> :
                <a href={props.link.ios} target="_blank" className={classes.white}><img src={cdn('ios-ic.svg')} alt="iOS Icon"/></a>)
            }
            {
                props.link.android && 
                (props.link.android == '#' ? <a className={classes.white}><img src={cdn('and-ic.svg')} alt="Android Icon"/></a> :
                <a href={props.link.android} target="_blank" className={classes.white}><img src={cdn('and-ic.svg')} alt="Android Icon"/></a>)
            }
            {
                props.link.amazon && 
                (props.link.amazon == '#' ? <a className={classes.white}><img src={cdn('amazon-ic-white.svg')} alt="Amazon Icon"/></a>:
                <a href={props.link.ios} target="_blank" className={classes.white}><img src={cdn('amazon-ic-white.svg')} alt="Amazon Icon"/></a>)
            }
            {
                props.link.iosgrey && 
                (props.link.iosgrey == '#' ? <a className={classes.grey}><img src={cdn('ios-ic-gray.svg')} alt="IOS Icon"/></a> :
                <a href={props.link.iosgrey} target="_blank" className={classes.grey}><img src={cdn('ios-ic-gray.svg')} alt="IOS Icon"/></a>)
            }
            {
                props.link.androidgrey && 
                (props.link.androidgrey == '#' ? <a className={classes.grey}><img src={cdn('and-ic-gray.svg')} alt="Android Icon"/></a> :
                <a href={props.link.androidgrey} target="_blank" className={classes.grey}><img src={cdn('and-ic-gray.svg')} alt="Android Icon"/></a>)
            }
            {
                props.link.amazongrey && 
                (props.link.amazongrey == '#' ? <a className={classes.grey}><img src={cdn('amazon-ic-grey.svg')} alt="Amazon Icon"/></a> :
                <a href={props.link.amazongrey} target="_blank" className={classes.grey}><img src={cdn('amazon-ic-grey.svg')} alt="Amazon Icon"/></a>)
            }
            {
                props.link.opera && 
                (props.link.opera ? <a className={classes.white}><img src={cdn('opera.svg')} alt="Opera Icon"/></a> :
                <a href={props.link.opera} target="_blank" className={classes.grey}><img src={cdn('opera.svg')} alt="Opera Icon"/></a>)
            }
            {
                props.link.operagrey && 
                (props.link.operagrey ? <a className={classes.grey}><img src={cdn('opera-grey.svg')} alt="Opera Icon"/></a> :
                <a href={props.link.operagrey} target="_blank" className={classes.grey}><img src={cdn('opera-grey.svg')} alt="Opera Icon"/></a>)
            }
        </div>
    )
}

export default AppLink
