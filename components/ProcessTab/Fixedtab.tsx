//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
const Fixedtab = (props) => {
    return (
        <ul className={classes.Fixedtab} ref={props.Fixedtabref}>
            <li className={props.active == 'first' ? classes.active : ''} onClick={()=>props.setCurrent("first")}>PROJECT PLAN</li>
            <li className={props.active == 'second' ? classes.active : ''} onClick={()=>props.setCurrent("second")}>DISCOVERY</li>
            <li className={props.active == 'third' ? classes.active : ''} onClick={()=>props.setCurrent("third")}>DESIGN & SCOPE</li>
            <li className={props.active == 'fourth' ? classes.active : ''} onClick={()=>props.setCurrent("fourth")}>IMPLEMENTATION</li>
        </ul>
    )
}
export default Fixedtab;