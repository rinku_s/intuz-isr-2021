//@ts-nocheck
import React from 'react'
import ProcessTab from './ProcessTab'
import classes from './styles.module.scss'

const Processes = (props) => {
    return (
        <div className={classes.Processes}>
            <ProcessTab active={props.active == 'first'} onClick={()=>props.setCurrent("first")}>Project Plan</ProcessTab>
            <ProcessTab active={props.active == 'second'} onClick={()=>props.setCurrent("second")}>Discovery</ProcessTab>
            <ProcessTab active={props.active == 'third'} onClick={()=>props.setCurrent("third")}>Design & Scope Finalisation</ProcessTab>
            <ProcessTab active={props.active == 'fourth'} onClick={()=>props.setCurrent("fourth")}>Implementation Review & Launch</ProcessTab>
        </div>
    )
}

export default Processes
