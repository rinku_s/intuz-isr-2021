//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
const ProcessTab = (props) => {
    return (
        <div className={`${classes.process} ${props.active ? classes.active : ''}`} onClick={props.onClick}>
            {props.children}
        </div>
    )
}

export default ProcessTab
