//@ts-nocheck

import React from 'react';
import LazyLoad from 'react-lazyload';

const TextBlock = (props) => {
    return(
        <>
            <LazyLoad height={300} offset={300} once>
                <img src={props.logo} alt={props.logoalt} />
            </LazyLoad>
            <h4>{props.title}</h4>
            <p>{props.description}</p>
            <LazyLoad height={300} offset={300} once>
                <img src={props.icon} alt={props.iconalt} />
            </LazyLoad>
        </>
    )
}

export default TextBlock