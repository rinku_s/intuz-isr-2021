//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import ImageBlock from '../../components/ImageBlock/ImageBlock';
import { cdn } from '../../config/cdn';
import style from './styles.module.scss';
import TextBlock from './TextBlock';


const Content = (props) => {

    // setOpacity('.contentCon, .contentCal')

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(`.contentCon`, 1, { x: -20, opacity: 0, ease: "Power0.ease" }, { x: 0, opacity: 1, ease: "Power0.ease" }, 0.5)
            .fromTo(`.contentCal`, 1, { x: 20, opacity: 0, ease: "Power0.ease" }, { x: 0, opacity: 1, ease: "Power0.ease" }, 0.5);
    }

    return (

        <div className={`${style.UnitContent}`}>
            <div className={`${style.left} contentCon`}>
                <TextBlock
                    title='Converter'
                    description='Intuitive unit conversion application developed specially for range of people in Finance, Plumber, Cook, Students, Scientists, Businessmen, or any person with speciality occupation who needs conversion of any kind.'
                    logo={cdn('converter-icon.png')}
                    logoalt='converter-icon'
                    icon={cdn('converter_bottom_icon.png')}
                    iconalt='converter_bottom_icon'
                />
            </div>
            <div>
                <ImageBlock format="png" src={cdn('convert_calculation_img.png')} alt="convert_calculation_img" mwidth={350} dwidth={400} className={`${style.middleImg}`} />
            </div>
            <div className='contentCal'>
                <TextBlock
                    title='Calculator'
                    description='Perform your important calculations using easy to use custom calculator interface. Attach notes to any calculation and share with your friends through different social media platforms.'
                    logo={cdn('calculator-icon.png')}
                    logoalt='calculator-icon'
                    icon={cdn('calculator_bottom_icon.png')}
                    iconalt='calculator_bottom_icon'
                />
            </div>
        </div>
        // </WaypointOnce >
    )
}

export default Content