
import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';
import style from './styles.module.scss';

const Content = (props) => {
    if(props.content.length <= 0) {
        return null;
    }else {
        return (
            <div className={`${style.Content} row`}>
                {props.content.map((data, index) => {
                    return(
                        <div className="col-md-2 col-sm-4" key={index}>
                            <div>
                                <LazyLoad height={300} offset={300} once>
                                    <img src={cdn(`${data.icon.name}?auto=format`)} alt={data.icon.name} />
                                </LazyLoad>
                            </div>
                            <h4>{data.title}</h4>
                        </div>
                    )
                })}
            </div>
        )
    }
}
export default Content;