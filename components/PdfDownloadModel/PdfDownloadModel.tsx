//@ts-nocheck

import React from 'react';
import { Modal } from 'react-bootstrap';
import { cdn } from '../../config/cdn';
import style from './styles.module.scss';

const PdfDownloadModel = (props) => {
    return (
        <>
        <Modal
        show={props.show}
        centered
        onHide={props.hide}>
            <Modal.Header closeButton={true} ></Modal.Header>
            <Modal.Body>
                <div className={style.PdfModal}>
                    <img src={cdn('pdf-download-img.png')} alt='pdf_download_icon' />
                    <h3>Don't want to miss anything? </h3>
                    <p>Download our PDF list of 500 websites that accept and want guests posts!</p>
                    <div>
                        <input type='text' placeholder='Enter your Email address here ...' />
                        <button type='submit'>SEND ME THE PDF!</button>
                    </div>
                </div>
            </Modal.Body>
        </Modal>
         <style>
             {
                `  
                 .modal-dialog { max-width: 600px; }
                 .modal-body { padding-bottom: 4rem; }
                 .close span:first-child{
                     font-size: 35px;
                     height: 40px;
                     font-weight: 300;
                     width: 40px;
                     text-align: center;
                     display: block;
                 }
                 .sr-only { display: none; }
                 .modal-header { border: none; }

                 `
             }
         </style>
         </>
    )
}

export default PdfDownloadModel