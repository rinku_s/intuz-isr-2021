//@ts-nocheck
import React, { useState } from 'react'
import { Waypoint } from 'react-waypoint'

const CustomWaypoint = ({ playAnimation, toffset, boffset }) => {
    const [entered, setEntered] = useState(false);

    function enter(){
            // if(!entered){
                playAnimation();
            // }
            // setEntered(true)
    }

    // function changePos({ currentPosition }){
    //     if(currentPosition === 'above' && !entered) {
    //         props.playAnimation();
    //     }
    // }

    return (
        <Waypoint bottomOffset={boffset ? boffset : 0} topOffset={ toffset ? toffset : 0 } onEnter={enter}>

        </Waypoint>
    )
}

export default CustomWaypoint
