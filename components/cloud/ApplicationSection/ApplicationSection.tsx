//@ts-nocheck

import { ApplicationListContext } from "context/ApplicationListContext";
import React, { Component, useState } from "react";
import { useContext } from "react";
import FilterApplication from "./FilterApplication/FilterApplication";
import ListApplication from "./ListApplication/ListApplication";
import classes from "./styles.module.scss";

const ApplicationSection = () => {
  const [query, setQuery] = useState("");
  const { applicationState, setApplicationState } = useContext(
    ApplicationListContext
  );

  return (
    <div className={classes.ApplicationDetail} id="appDetail">
      <div className="text-center">
        <input
          type="text"
          placeholder="Search Application"
          name="search"
          value={query}
          onKeyDown={(e) => {
            if (e.key === "Enter") {
              setApplicationState({
                ...applicationState,
                filter: {
                  ...applicationState.filter,
                  name_contains: query,
                },
              });
            }
          }}
          onChange={(e) => setQuery(e.target.value)}
        />
      </div>
      <div className={`${classes.Grid} block md:grid relative`}>
        <FilterApplication />
        <ListApplication />
      </div>
    </div>
  );
};

export default ApplicationSection;
