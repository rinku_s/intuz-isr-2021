//@ts-nocheck
import { ApplicationListContext } from "context/ApplicationListContext";
import { getFilterApplicationData, getUniversal } from "lib/api";
import { GetStaticProps } from "next";
import React, { useContext, useEffect, useState } from "react";
import {
  Media,
  MediaContextProvider,
} from "../../../../config/responsiveQuery";
import List from "./list";
import MobileList from "./Mobile/MobileList";

const FilterApplication = (props) => {
  const { applicationState, setApplicationState } = useContext(
    ApplicationListContext
  );
  const [categories, setCategories] = useState<string[]>([]);
  const [deliveryMethods, setDeliveryMethods] = useState<string[]>([]);
  const [pricePlans, setPricePlans] = useState<string[]>([]);

  useEffect(() => {
    setApplicationState({
      ...applicationState,
      filter: {
        ...applicationState.filter,
        Category: {
          id: categories,
        },
        delivery_method: {
          id: deliveryMethods,
        },
        price_plans: {
          id: pricePlans,
        },
      },
    });
  }, [categories, deliveryMethods, pricePlans]);

  //console.log(applicationState);
  return (
    <div
      className="hidden md:block ml-2 p-4 sticky top-40"
      style={{
        height: "500px",
        overflow: "scroll",
        boxShadow: "0 0 10px #ececec",
      }}
    >
      <List
        title={"Categories"}
        selectedCategories={categories}
        lists={applicationState.allFilters.categories}
        onListClick={(id) => {
          let cat = [...categories];
          if (cat.includes(id)) {
            cat = cat.filter((c) => c !== id);
          } else {
            cat.push(id);
          }
          setCategories(cat);
        }}
      />
      <List
        title={"Delivery Method"}
        selectedCategories={deliveryMethods}
        lists={applicationState.allFilters.deliveryMethods}
        onListClick={(id) => {
          let cat = [...deliveryMethods];
          if (cat.includes(id)) {
            cat = cat.filter((c) => c !== id);
          } else {
            cat.push(id);
          }
          setDeliveryMethods(cat);
        }}
      />
      <List
        title={"Pricing Plan"}
        selectedCategories={pricePlans}
        lists={applicationState.allFilters.pricingPlans}
        onListClick={(id) => {
          let cat = [...pricePlans];
          if (cat.includes(id)) {
            cat = cat.filter((c) => c !== id);
          } else {
            cat.push(id);
          }
          setPricePlans(cat);
        }}
      />
    </div>
  );
};

//}

export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal("/about");
  //context.params.name
  let data = await getFilterApplicationData();
  //props
  //application list

  //category filter

  return {
    props: {
      uv,
      data,
    },
  };
};

export default FilterApplication;
