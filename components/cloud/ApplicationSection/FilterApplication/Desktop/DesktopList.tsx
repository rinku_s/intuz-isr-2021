//@ts-nocheck

//@ts-nocheck
import React, { Component } from 'react';
import List from '../list';
import classes from './styles.module.scss';

export default class DesktopList extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        window.addEventListener('scroll', this.StickyFilterBar)
    }
    componentWillUnmount() {
        window.removeEventListener('scroll', this.StickyFilterBar)
    }


    StickyFilterBar = () => {
        let offset = this.scrollApplication ? this.scrollApplication.offsetTop : '';
        if (window.pageYOffset > offset) {
            this.scrollApplication && this.scrollApplication.classList.add(classes.sticky)
            let offsetBottom = document.getElementById("appDetail").offsetTop + document.getElementById("appDetail").offsetHeight

            if ((offsetBottom - window.pageYOffset) < 500) {
                this.scrollApplication && this.scrollApplication.classList.add(classes.end)
            } else {
                this.scrollApplication && this.scrollApplication.classList.remove(classes.end)
            }
        }
        if (window.pageYOffset < document.getElementById("appDetail").offsetTop) {
            this.scrollApplication && this.scrollApplication.classList.remove(classes.sticky)
        }
    }
    render() {
        return (
            <div className={`${classes.DesktopFilter}`} ref={c => this.scrollApplication = c}>
                {
                    this.props.children.map((list, index) =>
                        <List
                            data={list}
                            key={index}
                            onChange={(e, type) => this.props.onChange(e, type)}
                            filterData={this.props.filterData}
                        />
                    )
                }
            </div>
        )
    }
}