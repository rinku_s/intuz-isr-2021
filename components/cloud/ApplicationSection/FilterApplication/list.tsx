//@ts-nocheck

import { Category } from "lib/cloud/ApplicationListModel";
import React from "react";
import { useState } from "react";
import CheckboxIcon from "./CheckboxIcon";
import classes from "./styles.module.scss";

interface ListProps {
  title: string;
  lists: Category[];
  onListClick: (id: string) => void;
  selectedCategories: string[];
}

const List: React.FC<ListProps> = (props) => {
  const [readMore, setReadMore] = useState(false);
  return (
    <div className={`${classes.FilterList} filter_option mb-11`}>
      <h4 className="font-semibold text-18 text-gray-400">{props.title}</h4>
      <ul className={readMore ? classes.readmore : ""}>
        {props?.lists?.map((list) => {
          let categoriesChecked = props?.selectedCategories?.includes(list.id);
          return (
            <li className="flex items-center py-2" key={list.id}>
              <CheckboxIcon
                svgProps={{
                  width: 15,
                  height: 15,
                  className: "mr-3",
                  onClick: () => props.onListClick(list.id),
                }}
                checked={categoriesChecked}
              />
              <span className="text-14 text-gray-4f">{list.name}</span>
            </li>
          );
        })}
      </ul>

      {props.title == "Categories" && (
        <span
          onClick={() => setReadMore(!readMore)}
          className={classes.readmore}
        >
          {readMore ? "Show Less..." : "Show More..."}
        </span>
      )}
    </div>
  );
};
export default List;
