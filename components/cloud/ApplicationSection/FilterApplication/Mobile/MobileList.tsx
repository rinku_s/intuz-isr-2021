//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap';
import React, { useEffect, useState } from 'react';
import Transition from 'react-transition-group/Transition';
import List from '../list';
import classes from './styles.module.scss';

const MobileList = (props) => {
    const [showFilter, setshowFilter] = useState(false)
    const [privacy, setPrivacy] = useState(false)
    const [tempFilter, setTempFilter] = useState({
        category: [],
        cloud_provider: [],
        delivery_method: [],
        pricing: []
    })
    useEffect(() => {
        setTempFilter({
            ...tempFilter,
            category: [...props.filterData.category],
            cloud_provider: [...props.filterData.cloud_provider],
            delivery_method: [...props.filterData.delivery_method],
            pricing: [...props.filterData.pricing]
        });

    }, [showFilter])

    useEffect(() => {
        if (localStorage.getItem('acceptprivacy')) {
            setPrivacy(true)
        }
    }, [])

    const tl = gsap.timeline();

    const resetFilter = () => {
        setTempFilter({
            category: [],
            cloud_provider: [],
            delivery_method: [],
            pricing: []
        })
        props.onResetFiler()
    }
    const toggleFilter = () => { setshowFilter(!showFilter) }

    const onSave = () => {
        setshowFilter(false)
        props.tempFilter(tempFilter)
    }

    const prepareTempFilter = (e, type) => {
        const target = e.target;
        if (target.checked) {
            if (type == 'category') {
                setTempFilter({
                    ...tempFilter,
                    category: [...tempFilter.category, target.value]
                });
            } else if (type == 'cloud_provider') {
                setTempFilter({
                    ...tempFilter,
                    cloud_provider: [...tempFilter.cloud_provider, target.value]
                });
            } else if (type == 'delivery_method') {
                setTempFilter({
                    ...tempFilter,
                    delivery_method: [...tempFilter.delivery_method, target.value]
                });
            } else if (type == 'pricing') {
                setTempFilter({
                    ...tempFilter,
                    pricing: [...tempFilter.pricing, target.value]
                });
            }
        } else {
            if (type == 'category') {
                setTempFilter({
                    ...tempFilter,
                    category: tempFilter.category.filter(item => item != target.value)
                });
            } else if (type == 'cloud_provider') {
                setTempFilter({
                    ...tempFilter,
                    cloud_provider: tempFilter.cloud_provider.filter(item => item != target.value)
                });
            } else if (type == 'delivery_method') {
                setTempFilter({
                    ...tempFilter,
                    delivery_method: tempFilter.delivery_method.filter(item => item != target.value)
                });
            } else if (type == 'pricing') {
                setTempFilter({
                    ...tempFilter,
                    pricing: tempFilter.pricing.filter(item => item != target.value)
                });
            }
        }
    }
    return (
        <div className={classes.MobileFilter} id='headerCollapse'>
            {showFilter ? <img src={'/cloud/static/close-icon.png'} alt='close-icon' className={`${classes.filterIcon} ${privacy ? classes['privacy'] : ''}`} onClick={() => toggleFilter()} /> : <img src={'/cloud/static/filter-icon.png'} id='filterCat' alt='filter-icon' className={`${classes.filterIcon} ${privacy ? classes['privacy'] : ''}`} onClick={() => toggleFilter()} />}

            <Transition
                in={showFilter}
                timeout={1000}
                mountOnEnter={true}
                unmountOnExit={true}
                onEntering={(node, appearing) => {
                    tl.fromTo(node, 0.5, { scaleY: 0.5, opacity: 0, ease: "Power0.easeIn" }, { scaleY: 1, opacity: 1, ease: "Power0.easeIn" }, .1)
                        .staggerFromTo(node.getElementsByClassName('filter_option'), .4, { x: -30, opacity: 0, ease: "Power2.easeIn" }, { x: 0, opacity: 1, ease: "Power2.easeIn" });
                }}
                onEnter={(node) => { gsap.set(node, { y: 0, opacity: 0, transformOrigin: "0 0" }); }}
                onExit={(n) => { gsap.set(n, { opacity: 1, transformOrigin: "0 0" }); }}
                onExiting={(n, appearing) => { tl.fromTo(n, 0.5, { scaleY: 1, opacity: 1, ease: "Power0.easeIn" }, { scaleY: 0.5, opacity: 0, ease: "Power0.easeIn" }) }}
                addEndListener={(n, done) => {
                    if (showFilter) {
                        document.body.style.overflowY = "hidden";
                        document.getElementById("mobileHeader").setAttribute(
                            "style", "background-color: rgba(0,0,0,1); box-shadow: none;");
                        document.getElementById("topscroll").style.zIndex = '0'
                    } else {
                        document.body.style.overflowY = "visible";
                        document.getElementById("mobileHeader").setAttribute(
                            "style", "background-color: '';");
                        document.getElementById("topscroll").style.zIndex = '15'
                    }
                }}
            >
                {state =>
                    <div className={classes.NavigationView}>
                        {
                            props.children.map((list, index) =>
                                <List data={list} key={index}
                                    filterData={tempFilter}
                                    onChange={(e, type) => prepareTempFilter(e, type)}
                                />
                            )

                        }
                    </div>
                }
            </Transition>

            <Transition
                in={showFilter}
                timeout={1000}
                mountOnEnter={true}
                unmountOnExit={true}
                onEntering={(node, appearing) => { tl.fromTo(node, 0.5, { scaleY: 0.5, opacity: 0, ease: "Power0.easeIn" }, { scaleY: 1, opacity: 1, ease: "Power0.easeIn" }, .1); }}
                onEnter={(node) => { gsap.set(node, { y: 0, opacity: 0, transformOrigin: "0 0" }); }}
                onExit={(n) => { gsap.set(n, { opacity: 1, transformOrigin: "0 0" }); }}
                onExiting={(n, appearing) => { tl.fromTo(n, 0, { scaleY: 1, opacity: 1, ease: "Power0.easeIn" }, { scaleY: 0.5, opacity: 0, ease: "Power0.easeIn" }) }}
            >
                {state => <div className={classes.ButtonDiv}>
                    <button className={classes.getStarted} onClick={() => onSave()}>SAVE</button>
                    <a className={classes.reset} onClick={() => resetFilter()}>Reset</a>
                </div>}
            </Transition>
        </div>
    )
}

export default MobileList