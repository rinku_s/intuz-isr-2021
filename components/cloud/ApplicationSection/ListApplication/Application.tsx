import { cdn } from "config/cdn";
import Link from "next/link";
import React from "react";
import classes from "./styles.module.scss";
import Image from "next/image";
import { myLoader } from "config/image-loader";
import Rating from "react-rating";
const Application = (props) => {
  // let basePath = process.env.NEXT_PUBLIC_BASE_PATH ? "/cloud" : "";
  let strip;

  // let href = "/stack/[app_name]";
  if (props.link.includes("cloudformation")) {
    strip = <em>CF</em>;
  }
  if (props.link.includes("container")) {
    strip = <em>CN</em>;
  }

  return (
    <div className={`text-center md:text-left`}>
      <Link href={"/cloud" + props.link}>
        <a className="block text-center shd">
          <div className="p-10">
            <Image
              src={props?.logo?.name}
              loader={myLoader}
              width={150}
              height={150}
              layout="intrinsic"
              alt={props.name}
            />
            <p className="text-14 leading-normal text-gray-400">
              {props?.Category?.name}
            </p>
            <h3 className="text-16 text-gray-500">{props.name}</h3>
            <p className="inline-flex items-center">
              <img
                className="mr-1"
                width={15}
                height={15}
                src="/cloud/static/full-star.svg"
                alt="Rating Star"
              />
              {props.rating}
            </p>
          </div>
          <style jsx>{`
            .shd {
              transition: box-shadow 0.3s ease;
              border-radius: 10px;
            }
            .shd:hover {
              box-shadow: 0 0 20px hsl(0deg 0% 88% / 80%);
            }
          `}</style>
        </a>
      </Link>
      {/* <img src={cdn(props.logo.name)} className="max-w-full" alt="" /> */}
    </div>
  );
};

export default Application;
