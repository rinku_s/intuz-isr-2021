//@ts-nocheck

import LinkButton from "components/UI/LinkButton/LinkButton";
import { ApplicationListContext } from "context/ApplicationListContext";
import React, { useContext } from "react";
import Application from "./Application";
import classes from "../styles.module.scss";

const ListApplication = () => {
  const { applicationState, setApplicationState, loadMore } = useContext(
    ApplicationListContext
  );

  // console.log(applicationState);
  return (
    <div className={`${classes.List} grid px-3`}>
      {applicationState.app_list.length === 0 && (
        <p className="col-span-full text-20 text-center mt-5 font-bold">
          No application found for applied filter.
        </p>
      )}
      {applicationState.app_list.map((c) => (
        <Application {...c} key={c.id} />
      ))}
      {applicationState.app_list.length < applicationState.totalCount && (
        <p className="col-span-full text-20 text-center mt-5">
          <span className="cursor-pointer" onClick={loadMore}>
            Load More...
          </span>
        </p>
      )}
    </div>
  );
};

export default ListApplication;
