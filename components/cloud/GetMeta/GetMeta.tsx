//@ts-nocheck
import React from "react";
import Head from "next/head";
import HtmlParser from "react-html-parser";
import { cdn } from "config/cdn";

const GetMeta = ( { cloudPage } ) => {

  if(cloudPage.title == null || cloudPage.description == null) {
    console.log("Blank==>", cloudPage.title, cloudPage.description);   
  }

  return (
    <Head>
      <title>{cloudPage.title}</title>
      <meta name="description" content={cloudPage.description} />
      <meta property="og:locale" content="en_US"/>
      <meta name="referrer" content="origin-when-cross-origin"/>
      <meta property="og:locale" content="en_US"/>
      <meta property="og:type" content="website" />
      <meta property="og:title" content={cloudPage.title} />
      <meta property="og:description" content={cloudPage.description}/>
      <meta property="og:site_name" content="Intuz" />
     {cloudPage.image ? <meta name="og:image" content={cdn(cloudPage.image.name+"?auto=format,compress")}/> : ''}
      <meta name="twitter:card" content="summary_large_image"/>
      <meta name="twitter:description" content={cloudPage.description}/>
      <meta name="twitter:title" content={cloudPage.title}/>
      <meta name="twitter:site" content="@IntuzHQ"/>
      <meta name="twitter:creator" content="@IntuzHQ"/>
     {cloudPage.image ? <meta name="twitter:image" content={cdn(cloudPage.image.name+"?auto=format,compress")}/> : ''}
      {HtmlParser(cloudPage.meta_tags)}
      {cloudPage?.robots && <meta name="robots" content="noindex, nofollow" />}
    </Head>
  );
};

export default GetMeta;
