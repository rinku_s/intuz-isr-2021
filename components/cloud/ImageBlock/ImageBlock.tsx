//@ts-nocheck
import React from 'react'
import Imgix, { Picture, Source } from 'react-imgix'
import 'lazysizes';


const ImageBlock = ( { src, alt, className, dwidth, mwidth, format, title } ) => {
    return (
        <Picture>
            <Source
            src={src}
            width={dwidth}
            htmlAttributes={{ media: "(min-width: 768px)" }}
            attributeConfig={{
                src: "data-src",
                srcSet: "data-srcset",
                sizes: "data-sizes"
              }}
              imgixParams={{ auto:["format", "compress"], fm : format}}
            />
            <Source
            src={src}
            width={mwidth}
            htmlAttributes={{ media: "(max-width: 767px)" }}
            attributeConfig={{
                src: "data-src",
                srcSet: "data-srcset",
                sizes: "data-sizes"
              }}
              imgixParams={{ auto:["format", "compress"], fm: format}}
            />
            <Imgix 
            attributeConfig={{
                src: "data-src",
                srcSet: "data-srcset",
                sizes: "data-sizes"
              }}
            className={`${className} lazyload`} 
            src={src} 
            imgixParams={{ w: "auto", auto:["format", "compress"], fm : format }} 
            htmlAttributes={{ alt:alt, title:title}} />
        </Picture>
    )
}

export default ImageBlock
