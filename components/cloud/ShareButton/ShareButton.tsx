//@ts-nocheck
//@ts-nocheck
import React, { useEffect } from 'react';
import SVG from 'react-inlinesvg';
import { EmailShareButton, FacebookShareButton, LinkedinShareButton, PinterestShareButton, TwitterShareButton } from 'react-share';
import { detEdgeOrIE } from '../../../config/ie';
import classes from './styles.module.scss';
const ShareButton = ({ url, title, media }) => {

    let sbr = React.useRef();

    function sticky(evt) {
        if (window.pageYOffset > 612) {
            sbr.current.classList.add(classes.sticky);
            if (window.pageYOffset >= document.getElementById("blog").offsetHeight) {
                sbr.current.classList.add(classes.end);
            } else {
                sbr.current.classList.remove(classes.end);

            }
        } else {
            sbr.current.classList.remove(classes.sticky);
        }
    }


    useEffect(() => {
        window.addEventListener('scroll', sticky, false);
        return () => {
            window.removeEventListener('scroll', sticky, false);
        };
    }, [])
    return (
        <div className={classes.ShareButton} ref={sbr}>
            <TwitterShareButton url={url} title={title}>
                {!detEdgeOrIE() ? <SVG src="/static/Images/addressicons/twitter.svg" /> : <img src='/static/Images/addressicons/twitter.svg' alt='twitter' height='30px' width='30px' />}
            </TwitterShareButton>
            <FacebookShareButton
                url={url}
                quote={title}
            >
                {!detEdgeOrIE() ? <SVG src="/static/Images/addressicons/facebook.svg" /> : <img src='/static/Images/addressicons/facebook.svg' alt='facebook' height='30px' width='30px' />}
            </FacebookShareButton>
            <EmailShareButton
                url={url}
                subject={title}
                body="body"
            >
                {!detEdgeOrIE() ? <SVG src="/static/Images/addressicons/mail.svg" /> : <img src='/static/Images/addressicons/mail.svg' alt='mail' height='30px' width='30px' />}
            </EmailShareButton>
            <PinterestShareButton url={url} media={media} >
                {!detEdgeOrIE() ? <SVG src="/static/Images/addressicons/pinterest-social-logo.svg" /> : <img src='/static/Images/addressicons/pinterest-social-logo.svg' alt='pinterest-social-logo' height='30px' width='30px' />}
            </PinterestShareButton>
            <LinkedinShareButton url={url}>
                {!detEdgeOrIE() ? <SVG src="/static/Images/addressicons/linkedin-squ.svg" /> : <img src='/static/Images/addressicons/linkedin-squ.svg' alt='linkedin-squ' height='30px' width='30px' />}
            </LinkedinShareButton>
        </div>
    )
}

export default ShareButton
