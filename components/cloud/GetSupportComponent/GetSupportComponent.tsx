//@ts-nocheck
//@ts-nocheck
import { cdn } from "config/cdn";
import gsap from "gsap";
import React, { useState } from "react";
import Transition from "react-transition-group/Transition";
import Container from "../Container/Container";
import classes from "./styles.module.scss";

const GetSupportComponent = ({ open, onClose }) => {
  const [checked, setChecked] = useState(false);

  function onEnter(n) {
    gsap.fromTo(
      n,
      0.4,
      { scale: 0, autoAlpha: 0 },
      {
        scale: 1,
        autoAlpha: 1,
        ease: "power1.out",
        onComplete: () => {},
      }
    );
    document.body.style.overflowY = "hidden";
    gsap.set(n, { transform: "initial" });
  }

  function onExit(n) {
    gsap.fromTo(
      n,
      0.4,
      { scale: 1, autoAlpha: 1 },
      { scale: 0, autoAlpha: 0, ease: "power1.in" }
    );
    document.body.style.overflowY = "visible";
  }

  function onEntered(n) {
    n.removeAttribute("style");
  }

  return (
    <Transition
      in={open}
      onEnter={onEnter}
      onEntered={onEntered}
      onExit={onExit}
      timeout={500}
      mountOnEnter={true}
      unmountOnExit={true}
    >
      <div className={classes.GetSupportComponent}>
        <span onClick={onClose} className={classes.CloseBtn}>
          <img src={cdn("eva_close_fill_041dca7ebb.png")} alt="Close" />
        </span>
        <Container className={classes.Content}>
          <h3>Get Your Application Installed in 1&nbsp;Hour</h3>
          <h4>Scope</h4>
          <p className="text-aqua">The scope covers</p>
          <ul className="row justify-center">
            <li className="col-sm-6">Launch Application</li>
            <li className="col-sm-6">
              Check IAM user and it’s security best practice
            </li>
            <li className="col-sm-6">Configure Security Groups </li>
            <li className="col-sm-6">
              Show working URL and how-to login in website/portal
            </li>
          </ul>
          <div
            className={`${classes.Checkbox}${
              checked ? " " + classes.active : ""
            }`}
          >
            <div className={classes.inputGroup}>
              <span
                onClick={() => setChecked(!checked)}
                className={classes.CheckImage}
                style={{
                  backgroundImage: checked
                    ? `url(${cdn("checked_0a290adf1d.png")})`
                    : `url(${cdn("uncheck_c649c704e6.png")})`,
                  backgroundSize: "contain",
                }}
              ></span>
              <input
                type="checkbox"
                name="ad_support"
                id="ad_support"
                checked={checked}
                onChange={(e) => setChecked(e.target.checked)}
              />
              <label htmlFor="ad_support" className="ml-3">
                <p>
                  Additional Support{" "}
                  <span className="text-grey">(Optional but Recommended)</span>
                </p>
              </label>
            </div>
            <ul className="row">
              <li className="col-sm-6">Setting up of Elastic IP</li>
              <li className="col-sm-6">Life Cycle Policy (backup of server)</li>
              <li className="col-sm-6">Domain Binding</li>
              <li className="col-sm-6">SSL Setup</li>
            </ul>
          </div>
          <h4>Out of Scope</h4>
          <p>
            Scope does not cover configuring / customizing of the application
            itself. The scope will be limited to just installation.
          </p>
          <div className={`d-sm-flex ${classes.Estimate}`}>
            <p>
              Estimated time <span>{checked ? 2 : 1} hour</span>
            </p>
            <p>
              Cost US <span>{checked ? "$100" : "$50"}</span>
            </p>
          </div>
          <p className={classes.Small}>
            Note: If the efforts requires 1+ hours’, it will be billed for
            additional $50/hour as per actual hours spent by the AWS Expert.
            <br />
            Minimum charges to pay will be equal to an hour (i.e. $50)
            irrespective of the time spent on the task.
          </p>
          <h4>How It Works</h4>
          <ul>
            <li>
              We will take a remote desktop access at a mutually convenient time
              where you can login to your AWS Account and then share us the
              access of your system so we can take the remote of your system and
              install the AWS AMI / CF in front of you.
            </li>
            <li>
              We will need few details from you that is required to install the
              application.
              <ul className="row">
                <li className="col-sm-6">
                  AWS Region (eg. Virginia, London, Singapore, etc)
                </li>
                <li className="col-sm-6">AWS Marketplace AMI URL</li>
                <li className="col-sm-6">
                  EC2 instance type e.g. t3a.small, m5a.large or any other.
                </li>
                <li className="col-sm-6">
                  If AMI is paid than select type of Subscription you want to
                  subscribe: Hourly | Yearly
                </li>
              </ul>
            </li>
          </ul>
          <h4>Terms of Service</h4>
          <ul>
            <li>
              All services provided by Intuz is subject to Intuz{" "}
              <a
                href="https://payment.intuz.com/cloud/termsofservices.html"
                rel="nofollow noopener"
                target="_blank"
              >
                Terms of Service
              </a>
            </li>
            <li>
              In-case of any dispute, the maximum liability to Intuz will not be
              more than $50.
            </li>
          </ul>
        </Container>
        <div className={classes.CtaBar}>
          <h3>Get Started</h3>
          <div className={`block d-mflex justify-center items-center mt-5`}>
            <div>
              <p>
                Step-1{" "}
                <a
                  href="https://payment.intuz.com/cloud/"
                  rel="nofollow noopener"
                  target="_blank"
                >
                  Pay {checked ? "$100" : "$50"}
                </a>
              </p>
            </div>
            <div>
              <p>
                Step-2{" "}
                <a
                  href="https://calendly.com/intuz-cloud-support/60min"
                  rel="nofollow noopener"
                  target="_blank"
                >
                  Book an appointment
                </a>
              </p>
            </div>
          </div>
        </div>
      </div>
    </Transition>
  );
};

export default GetSupportComponent;
