//@ts-nocheck
import React from 'react';
import classes from './utility.module.scss';
export interface HeadingProps {
    children?: string,
    type?: string,
    className?: string,
    style?: React.CSSProperties;
}

const Heading: React.FC<HeadingProps> = (props) => {

    return (
        <h2 className={`${classes.Heading} ${classes[props.type]}${props.className ? ' ' + props.className : ''}`} style={props.style}>
            {props.children}
        </h2>
    )
}

export default Heading
