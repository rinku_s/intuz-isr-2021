//@ts-nocheck
import React from 'react'
import classes from './utility.module.scss'

const GradientHead: React.FC<any> = ({ children, className, isMid, isHf }) => {
    
    if(isHf) {

        return (
            <h4 className={`${classes.Gradient}${isMid ? ' '+classes.mid : ''}${className ? ' '+className: ''}`}>
                {children}
            </h4>
        )
    }
    return (
        <h2 className={`${classes.Gradient}${isMid ? ' '+classes.mid : ''}${className ? ' '+className: ''}`}>
            {children}
        </h2>
    )
}

export default GradientHead
