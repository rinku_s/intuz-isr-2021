//@ts-nocheck
import React from 'react';
import classes from './utility.module.scss';

export interface ParagraphProps {
    children?: string,
    type?: string,
    className?: string,
    style?: React.CSSProperties;
}


const Paragraph: React.FC<ParagraphProps> = (props) => {
    return (
        <p className={`${classes.Paragraph} ${classes[props.type]}${props.className ? ' ' + props.className : ''}`}
            style={props.style}>
            {props.children}
        </p>
    )
}

export default Paragraph
