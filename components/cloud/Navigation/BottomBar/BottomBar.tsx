//@ts-nocheck
import gsap from 'gsap';
import React, { useEffect } from 'react';
import classes from './style.module.scss';

const BottomBar = (props) => {

    useEffect(() => {
        gsap.fromTo('.bbar', 1, { width: 0 }, { width: "100%" });
        return () => {
        };
    }, [])

    return (

        <div className={`${classes.BottomBar} bbar`}>
            <ul>
                <li onClick={() => props.setFilter("mobile")}>
                    <div dangerouslySetInnerHTML={{ __html: require('../../../static/Images/icons/ic_mobile.svg?include') }} />
                    <a>MOBILE&nbsp;APP</a>
                </li>
                <li onClick={() => props.setFilter("cloud")}>
                    <div dangerouslySetInnerHTML={{ __html: require('../../../static/Images/icons/ic_cloud.svg?include') }} />
                    <a>CLOUD</a>
                </li>
                <li onClick={() => props.setFilter("web")}>
                    <div dangerouslySetInnerHTML={{ __html: require('../../../static/Images/icons/ic_web.svg?include') }} />
                    <a>WEB</a>
                </li>
                <li onClick={() => props.setFilter("more")}>
                    <div dangerouslySetInnerHTML={{ __html: require('../../../static/Images/icons/ic_more.svg?include') }} />
                    <a>MORE</a>
                </li>
            </ul>
        </div>
    )
}

export default BottomBar
