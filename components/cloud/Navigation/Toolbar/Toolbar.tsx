//@ts-nocheck
//@ts-nocheck
import Router from "next/router";
import React, { Component } from "react";
import {
  Media,
  MediaContextProvider,
} from "../../../../config/responsiveQuery";
import Container from "../../Container/Container";
import DesktopNavbar from "../NavigationItems/Desktop";
import MobileNavbar from "../NavigationItems/Mobile";
import classes from "./styles.module.scss";

export default class Toolbar extends Component {
  constructor(props) {
    super(props);
  }
  state = {
    show: false,
    content: null,
    showActiveMenu: false,
    stickMenuContent: null,
    currentPath: null,
  };

  ChangeHeaderColor = (isMenuOpen) => {
    if (isMenuOpen) {
      this.header.classList.add(classes.Menuopen);
    } else {
      this.header.classList.remove(classes.Menuopen);
    }
  };

  Mobilemenu = () => {
    return (
      <MobileNavbar
        menu={this.props.menu}
        currentPath={this.state.currentath}
        changeHeaderColor={(isMenuOpen) => this.ChangeHeaderColor(isMenuOpen)}
      >
        <button
          className={`${classes.getStarted} ${classes.Mobileview}`}
          onClick={() => {
            document.body.style.overflowY = "visible";
            Router.push("/contactus");
          }}
        >
          GET&nbsp;STARTED
        </button>
      </MobileNavbar>
    );
  };

  StickyHeader = () => {
    let offset = this.header.offsetTop;
    if (window.pageYOffset > offset) {
      this.header.classList.add(classes.sticky);
    } else {
      this.header.classList.remove(classes.sticky);
    }
  };

  componentDidMount() {
    window.addEventListener("scroll", this.StickyHeader);
    const path = Router.asPath;
    this.setState({ currentPath: path });
  }

  componentWillUnmount = () => {
    window.removeEventListener("scroll", this.StickyHeader);
  };

  showTab = (data, stickMenuIndex, menuIndex) => {
    this.setState({
      content: data,
      showActiveMenu: true,
    });
    if (menuIndex == stickMenuIndex) {
      this.setState({
        stickMenuContent: data,
      });
    }
  };

  hideTab = () => {
    this.setState({
      content: this.state.stickMenuContent,
      showActiveMenu: false,
    });
  };

  render() {
    return (
      <MediaContextProvider>
        <header
          className={`${classes.Toolbar}`}
          ref={(c) => (this.header = c)}
          onMouseLeave={this.hideTab}
          id="mobileHeader"
        >
          <Media greaterThanOrEqual="sm">
            <Container className={classes.HeaderContainer}>
              <div className={classes.Logo} onClick={() => Router.push("/")}>
                <img
                  src={require("../../../static/Images/logo.svg")}
                  alt="Intuz Logo"
                />
              </div>
              <DesktopNavbar
                menu={this.props.menu}
                hoverContent={(val, stickMenuIndex, menuIndex) =>
                  this.showTab(val, stickMenuIndex, menuIndex)
                }
                showActiveMenu={this.state.showActiveMenu}
              >
                <li className={classes.getStrd}>
                  <button
                    className={classes.getStarted}
                    onClick={() => Router.push("/contactus")}
                  >
                    CONTACT US
                  </button>
                </li>
              </DesktopNavbar>
            </Container>
            {this.state.content == null ? "" : this.state.content}
          </Media>

          <Media lessThan="sm" style={{ width: "100%" }}>
            <div className="flex justify-content-between">
              <div className={classes.Logo} onClick={() => Router.push("/")}>
                <img
                  src={require("../../../static/Images/logo.svg")}
                  alt="Intuz Logo"
                />
              </div>
              {this.Mobilemenu()}
            </div>
          </Media>
          <style jsx>
            {`
              .${classes.sticky} {
                background-color: "#fff";
              }
              .${classes.Menuopen} {
                background: rgba(0, 0, 0, 1);
              }
            `}
          </style>
        </header>
      </MediaContextProvider>
    );
  }
}
