//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap';
import React, { useEffect, useState } from 'react';
import Transition from 'react-transition-group/Transition';
import CollapseButton from './CollapseButton/CollapseButton';
import MobileNavigation from './NavigationItem/MobileNavigation';
import NavigationView from './NavigationView';
import classes from './styles.module.scss';

const MobileNavbar = (props) => {
    const [show, setShowNav] = useState(false);
    let tl = gsap.timeline();

    useEffect(() => {
        props.changeHeaderColor(show)
    }, [show])

    const toggleMenuScreen = () => {
        setShowNav(!show)
    }

    return (
        <div className={classes.Mobile} id='headerCollapse'>
            <CollapseButton show={show} onClick={() => toggleMenuScreen()} />
            <Transition
                in={show}
                timeout={1000}
                mountOnEnter={true}
                unmountOnExit={true}
                onEntering={(node, appearing) => {
                    tl.fromTo(node, 0.5, { scaleY: 0.5, opacity: 0, ease: "Power0.easeIn" }, { scaleY: 1, opacity: 1, ease: "Power0.easeIn" }, .1)
                        .staggerFromTo(node.getElementsByTagName('li'), .4, { x: -30, opacity: 0, ease: "Power2.easeIn" }, { x: 0, opacity: 1, ease: "Power2.easeIn" });
                }}
                onEnter={(node) => { gsap.set(node, { y: 0, opacity: 0, transformOrigin: "0 0" }); }}
                onExit={(n) => { gsap.set(n, { opacity: 1, transformOrigin: "0 0" }); }}
                onExiting={(n, appearing) => { tl.fromTo(n, 0.5, { scaleY: 1, opacity: 1, ease: "Power0.easeIn" }, { scaleY: 0.5, opacity: 0, ease: "Power0.easeIn" }) }}
                addEndListener={(n, done) => {
                    if (show) {
                        document.body.style.overflowY = "hidden";
                        if (document.getElementById('filterCat')) {
                            document.getElementById('filterCat').style.display = 'none';
                        }
                    } else {
                        document.body.style.overflowY = "visible";
                        if (document.getElementById('filterCat')) {
                            document.getElementById('filterCat').style.display = 'block';
                        }
                    }
                }}
            >
                {state =>
                    <NavigationView>
                        {props.menu.map((menuDetail, index) => {
                            return (
                                <MobileNavigation
                                    key={index}
                                    link={menuDetail.link}
                                    label={menuDetail.label} />
                            )
                        })}
                    </NavigationView>

                }
            </Transition>
            <Transition
                in={show}
                timeout={1000}
                mountOnEnter={true}
                unmountOnExit={true}
                onEntering={(node, appearing) => { tl.fromTo(node, 0.5, { scaleY: 0.5, opacity: 0, ease: "Power0.easeIn" }, { scaleY: 1, opacity: 1, ease: "Power0.easeIn" }, .1); }}
                onEnter={(node) => { gsap.set(node, { y: 0, opacity: 0, transformOrigin: "0 0" }); }}
                onExit={(n) => { gsap.set(n, { opacity: 1, transformOrigin: "0 0" }); }}
                onExiting={(n, appearing) => { tl.fromTo(n, 0, { scaleY: 1, opacity: 1, ease: "Power0.easeIn" }, { scaleY: 0.5, opacity: 0, ease: "Power0.easeIn" }) }}
            >
                {state => <div className={`${classes.ButtonDiv}`}>{props.children}</div>}
            </Transition>
        </div>
    )
}

export default MobileNavbar






