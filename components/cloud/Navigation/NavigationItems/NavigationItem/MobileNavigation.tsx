//@ts-nocheck
import Link from 'next/link';
import Router from 'next/router';
import React from 'react';
import classes from './styles.module.scss';

const MobileNavigation = (props) => {
    const CurrentPage = Router.asPath
    return (
        <li className={`${classes.NavigationItem}`} >
            <Link href={props.link} prefetch={false}>
                <a className={`${CurrentPage == props.link ? classes.ActivePage : ''}`}> {props.label}</a>
            </Link>
        </li>
    )
};


export default MobileNavigation
