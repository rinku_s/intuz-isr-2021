//@ts-nocheck
import Link from 'next/link';
import React from 'react';
import classes from './styles.module.scss';

const DesktopNavigation = (props) => {
    return (
        <li className={`${classes.NavigationItem} ${(props.activemenu) ? classes.active : ''}`}>
            <Link href={props.link} prefetch={false}>
                <a>{props.label}</a>
            </Link>
        </li>
    )
};

export default DesktopNavigation;
