//@ts-nocheck


import React from 'react';
import classes from './styles.module.scss';

const CollapseButton = (props) => {
    return (
        <a className={`${classes.collapseButton} ${props.show ? '' : classes.bordered}`} 
                onClick={props.onClick} >
                {props.show ? 
                    <>
                        <span className={classes.leftClose}></span>
                        <span className={classes.rightClose}></span>
                    </>
                : 
                    <>
                        <span></span>
                        <span>&nbsp;</span>
                        <span></span>
                    </>
                }
            </a>
    )
}

export default CollapseButton