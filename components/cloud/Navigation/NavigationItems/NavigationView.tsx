//@ts-nocheck
import React from 'react'

const NavigationView = (props) => {
    return(
        <>
            <div className='NavigationView'>
                <ul className="nav mr-3 mt-3">
                    {props.children}
                </ul>
            </div>

            <style jsx>
                {`
                    .NavigationView {
                        background: rgba(0, 0, 0, 1);
                        width: 100%;
                        position: fixed;
                        left: 0;
                        top: 6.5rem;
                        right: 0;
                        bottom: 0;
                        max-width: 100%;
                        overflow: scroll;
                        z-index: 10;
                    }
                    ul {
                        padding: 5rem 0 10rem 8rem;
                    }
                    @media (max-width: 520px) {
                        ul {
                            padding-left: 4rem   
                        }
                    }
                `}
            </style>
        </>
    )
}

export default NavigationView