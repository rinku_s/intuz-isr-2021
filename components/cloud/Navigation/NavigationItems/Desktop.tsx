//@ts-nocheck
//@ts-nocheck
import Router from 'next/router';
import React, { Component } from 'react';
import NavigationItem from './NavigationItem/DesktopNavigation';
import classes from './styles.module.scss';

export default class DesktopNavbar extends Component {
    constructor(props) {
        super(props)
        this.state = { path: null }
    }

    componentDidMount() {
        this.setState({ path: Router.asPath })
    }

    render() {
        return (
            <ul className={`nav ${classes.MenuList} flex-nowrap`} >
                {this.props.menu != null && this.props.menu.map((menuDetail, index) =>
                    <NavigationItem
                        activemenu={menuDetail.link == this.state.path ? menuDetail.link : null}
                        label={menuDetail.label}
                        link={menuDetail.link}
                        key={index}
                    />
                )}
                {this.props.children}
            </ul>
        )
    }
}
