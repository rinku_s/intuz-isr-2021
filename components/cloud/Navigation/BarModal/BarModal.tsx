//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap';
import React, { Component } from 'react';
import { Transition } from 'react-transition-group';
import { Media, MediaContextProvider } from "../../../../config/responsiveQuery";
import { data } from '../../../ModalInformation/data';
import ModalInformation from '../../../ModalInformation/ModalInformation';
import BottomBar from '../BottomBar/BottomBar';

export default class BarModal extends Component {
    state = {
        activeFilter: null,
        modalShowing: false,
        current: null
    }
    tl = gsap.timeline();
    setFilter = (id) => {
        if (this.state.activeFilter !== null && this.state.activeFilter === id) {
            this.setState({
                activeFilter: null,
                modalShowing: false
            })
        } else {
            this.setState({
                activeFilter: id,
                modalShowing: true
            });
        }

    }

    closeModal = () => {
        this.setState({
            activeFilter: null,
            modalShowing: false
        })
    }


    componentDidMount() {
        let current = data.find(e => e.id === this.state.activeFilter);
        this.setState({
            current: current
        });

    }

    componentDidUpdate = (prevProps, prevState) => {
        if (prevState.activeFilter !== this.state.activeFilter && this.state.activeFilter !== null) {
            let current = data.find(e => e.id === this.state.activeFilter);
            this.setState({
                current: current
            });
        }
        if (this.state.modalShowing) {
            document.body.style.overflow = "hidden";
        } else {
            document.body.style.overflow = "visible";
        }
    };




    render() {

        let modal = <ModalInformation activeFilter={this.state.activeFilter} onClose={this.closeModal} current={this.state.current} />;


        return (
            <MediaContextProvider>

                <Media greaterThanOrEqual="sm">
                    {/* <SideBar activeFilter={this.state.activeFilter} setFilter={this.setFilter} /> */}
                </Media>
                <Media lessThan="sm">
                    <BottomBar activeFilter={this.state.activeFilter} setFilter={this.setFilter} />
                </Media>
                <Transition
                    in={this.state.modalShowing}
                    timeout={1000}
                    mountOnEnter={true}
                    unmountOnExit={true}
                    addEndListener={(n, done) => {
                        if (this.state.modalShowing) {
                            if (window.innerWidth >= 767) {
                                gsap.fromTo(n, 0.5, { xPercent: -100, opacity: 0, ease: "power0.ease" }, { xPercent: 0, opacity: 1, ease: "power0.ease" })
                            } else {
                                gsap.fromTo(n, 0.5, { yPercent: 100, opacity: 0, ease: "power0.ease" }, { yPercent: 0, opacity: 1, ease: "power0.ease" })
                            }
                        } else {
                            if (window.innerWidth >= 767) {
                                gsap.fromTo(n, 0.5, { xPercent: 0, opacity: 1, ease: "power0.ease" }, { xPercent: -100, opacity: 0, ease: "power0.ease" })
                            } else {
                                gsap.fromTo(n, 0.5, { yPercent: 0, opacity: 1, ease: "power0.ease" }, { yPercent: 100, opacity: 0, ease: "power0.ease" })
                            }
                        }
                    }}
                >
                    {state => modal}
                </Transition>
            </MediaContextProvider>
        )
    }
}



