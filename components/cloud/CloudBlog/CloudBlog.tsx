//@ts-nocheck
//@ts-nocheck
import React from 'react';
import HtmlParser from 'react-html-parser';
import Container from '../Container/Container';
import classes from './styles.module.scss';
const CloudBlog = ({ data }) => {
    return (
        <article className={classes.CloudBlog}>
            <Container>
                {HtmlParser(data)}
            </Container>
        </article>
    )
}

export default CloudBlog
