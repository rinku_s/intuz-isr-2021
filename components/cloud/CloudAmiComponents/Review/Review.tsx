//@ts-nocheck
//@ts-nocheck
import LinkButton from "components/cloud/UI/LinkButton/LinkButton";
import React from "react";
import Rating from "react-rating";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.min.css";
import classes from "./styles.module.scss";
const Review = ({ title, description, rating }) => (
  <div className={classes.Review}>
    {title && <h3>{title}</h3>}
    <p>{description}</p>
    <div>
      <Rating
        emptySymbol={
          <img
            className="mr-2"
            src="/cloud/static/empty-star.svg"
            alt="Empty Star"
          />
        }
        fullSymbol={
          <img
            className="mr-2"
            src="/cloud/static/full-star.svg"
            alt="Full Star"
          />
        }
        initialRating={rating}
        readonly
      />
    </div>
  </div>
);

const Reviews = ({ reviews, link }) => {
  const settings = {
    slidesToShow: 1,
    slidesToScroll: 1,
    speed: 500,
    dots: false,
    arrows: true,
    className: "slides",
  };

  if (reviews.length < 2) {
    return (
      <div className="text-center">
        <div className="mx-auto">
          {reviews.map((review) => (
            <Review key={review.title} {...review} />
          ))}
        </div>
        <div style={{ marginTop: "5rem" }}>
          <LinkButton
            atype={true}
            newTab={true}
            variation="cloudBtnSmall"
            href={link}
          >
            Write a Review
          </LinkButton>
        </div>
      </div>
    );
  }

  return (
    <div className="text-center">
      <div className="mx-auto">
        {reviews.length !== 0 && (
          <Swiper {...settings}>
            {reviews.map((review) => (
              <SwiperSlide>
                <Review key={review.title} {...review} />
              </SwiperSlide>
            ))}
          </Swiper>
        )}
      </div>
      <div style={{ marginTop: "5rem" }}>
        <LinkButton
          atype={true}
          newTab={true}
          variation="cloudBtnSmall"
          href={link}
        >
          Write a Review
        </LinkButton>
      </div>
      <style jsx>
        {`
          .mx-auto {
            max-width: 60%;
          }
          @media screen and (max-width: 767px) {
            .mx-auto {
              max-width: 100%;
            }
          }
        `}
      </style>
    </div>
  );
};

export default Reviews;
