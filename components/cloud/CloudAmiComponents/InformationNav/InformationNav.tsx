//@ts-nocheck
//@ts-nocheck
import { cdn } from "config/cdn";
import { Media, MediaContextProvider } from "config/responsiveQuery";
import gsap from "gsap";
import React, { useEffect, useState } from "react";
import { Accordion } from "react-bootstrap";
import HtmlParser from "react-html-parser";
import { Transition } from "react-transition-group";
import NavItem from "./NavItem";
import classes from "./styles.module.scss";

const InformationNav = ({ details, how_to, resources }) => {
  const [activeNav, setActiveNav] = useState("details");

  function animateEnter(n) {
    gsap.fromTo(n, 1, { opacity: 0 }, { opacity: 1, ease: "Power0.easeIn" });
  }

  useEffect(() => {
    var coll = document.getElementsByClassName("collapsible");
    var i;

    for (i = 0; i < coll.length; i++) {
      coll[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var content = this.nextElementSibling;
        if (content.style.display === "block") {
          gsap.fromTo(
            content,
            0.5,
            { autoAlpha: 1 },
            {
              autoAlpha: 0,
              ease: "Power0.in",
              onComplete: () => {
                content.style.display = "none";
              },
            }
          );
        } else {
          content.style.display = "block";
          gsap.fromTo(
            content,
            0.5,
            { autoAlpha: 0, delay: 0.1 },
            { autoAlpha: 1, delay: 0.1, ease: "Power0.in" }
          );
        }
      });
    }
  }, []);

  function activateNav(d) {
    if (activeNav == d) {
      setActiveNav("");
    } else {
      setActiveNav(d);
    }
  }

  return (
    <>
      <MediaContextProvider>
        <Media greaterThanOrEqual="sml">
          <div className="flex flex-col sm:flex-row justify-content-between mt-5">
            <NavItem
              activeNav={activeNav}
              details={details}
              how_to={how_to}
              resources={resources}
              active={activeNav == "details"}
              onClick={() => setActiveNav("details")}
              image={cdn("cloud_details.svg")}
              title="Details"
            />
            <NavItem
              activeNav={activeNav}
              details={details}
              how_to={how_to}
              resources={resources}
              active={activeNav == "how-to"}
              onClick={() => setActiveNav("how-to")}
              image={cdn("cloud_how-to.svg")}
              title="How To"
            />
            <NavItem
              activeNav={activeNav}
              details={details}
              how_to={how_to}
              resources={resources}
              active={activeNav == "resources"}
              onClick={() => setActiveNav("resources")}
              image={cdn("cloud_resources.svg")}
              title="Resources"
            />
          </div>
          <div className={classes.information}>
            <Transition
              timeout={1000}
              in={activeNav == "details"}
              mountOnEnter={false}
              unmountOnExit={false}
              onEnter={animateEnter}
            >
              <div className={activeNav == "details" ? "fadeIn" : "sr-only"}>
                {HtmlParser(details)}
              </div>
            </Transition>
            <Transition
              timeout={1000}
              in={activeNav == "how-to"}
              mountOnEnter={false}
              unmountOnExit={false}
              onEnter={animateEnter}
            >
              <div className={activeNav == "how-to" ? "fadeIn" : "sr-only"}>
                {HtmlParser(how_to)}
              </div>
            </Transition>
            <Transition
              timeout={1000}
              in={activeNav == "resources"}
              mountOnEnter={false}
              unmountOnExit={false}
              onEnter={animateEnter}
            >
              <div className={activeNav == "resources" ? "fadeIn" : "sr-only"}>
                {HtmlParser(resources)}
              </div>
            </Transition>
          </div>
        </Media>
        <Media lessThan="sml">
          <Accordion
            className="flex flex-col sm:flex-row justify-content-between mt-5"
            defaultActiveKey="0"
          >
            <Accordion.Toggle as="div" eventKey="0">
              <NavItem
                activeNav={activeNav}
                details={details}
                how_to={how_to}
                resources={resources}
                active={activeNav == "details"}
                onClick={() => activateNav("details")}
                image={cdn("cloud_details.svg")}
                title="Details"
              />
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="0">
              <div className={classes.information}>
                <div className={activeNav == "details" ? "fadeIn" : "sr-only"}>
                  {HtmlParser(details)}
                </div>
              </div>
            </Accordion.Collapse>
            <Accordion.Toggle as="div" eventKey="1">
              <NavItem
                activeNav={activeNav}
                details={details}
                how_to={how_to}
                resources={resources}
                active={activeNav == "how-to"}
                onClick={() => activateNav("how-to")}
                image={cdn("cloud_how-to.svg")}
                title="How To"
              />
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="1">
              <div className={classes.information}>
                <div className={activeNav == "how-to" ? "fadeIn" : "sr-only"}>
                  {HtmlParser(how_to)}
                </div>
              </div>
            </Accordion.Collapse>
            <Accordion.Toggle as="div" eventKey="2">
              <NavItem
                activeNav={activeNav}
                details={details}
                how_to={how_to}
                resources={resources}
                active={activeNav == "resources"}
                onClick={() => activateNav("resources")}
                image={cdn("cloud_resources.svg")}
                title="Resources"
              />
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="2">
              <div className={classes.information}>
                <div
                  className={activeNav == "resources" ? "fadeIn" : "sr-only"}
                >
                  {HtmlParser(resources)}
                </div>
              </div>
            </Accordion.Collapse>
          </Accordion>
        </Media>
      </MediaContextProvider>
    </>
  );
};

export default InformationNav;
