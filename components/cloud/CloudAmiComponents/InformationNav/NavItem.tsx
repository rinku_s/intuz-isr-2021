//@ts-nocheck
import React from "react";
import classes from "./styles.module.scss";
import { Media, MediaContextProvider } from "config/responsiveQuery";
import { Transition } from "react-transition-group";
import gsap from "gsap";
import HtmlParser from "react-html-parser";
const NavItem = ({
  image,
  title,
  onClick,
  active,
  activeNav,
  details,
  how_to,
  resources,
}) => {
  function animateEnter(n) {
    gsap.set(n, { transformOrigin: "0 0" });
    gsap.fromTo(
      n,
      0.5,
      { opacity: 0, height: 0 },
      { opacity: 1, height: "100%", ease: "Power0.easeIn" }
    );
  }
  function animateExit(n) {
    gsap.set(n, { transformOrigin: "0 0" });
    gsap.fromTo(
      n,
      0.5,
      { opacity: 1, height: "100%" },
      { opacity: 1, height: 0, ease: "Power0.easeIn" }
    );
  }

  return (
    <div
      className={`${classes.NavItem}${active ? " " + classes.active : ""}`}
      onClick={onClick}
    >
      <div
        className={`${classes.NavBlock} flex d-sm-block items-center text-center`}
      >
        <div className={classes.image}>
          <img className="lazyload img-fluid" data-src={image} alt={title} />
        </div>
        <p className="pt-3">{title}</p>
      </div>
      {/* <MediaContextProvider>
                <Media lessThan="sml">
                    <Transition timeout={1000} in={active} mountOnEnter={false} unmountOnExit={false} onEnter={animateEnter} onExiting={animateExit}>
                    <div className={`${classes.information} ${active ? '' : 'hidden' }`}>
                        <Transition timeout={1000} in={activeNav == 'details'} mountOnEnter={false} unmountOnExit={false} onEnter={animateEnter} >
                            <div className={activeNav == 'details' ? 'fadeIn' : 'sr-only'}>{HtmlParser(details)}</div>
                        </Transition>
                        <Transition timeout={1000} in={activeNav == 'how-to'} mountOnEnter={false} unmountOnExit={false} onEnter={animateEnter} >
                            <div className={activeNav == 'how-to' ? 'fadeIn' : 'sr-only'}>{HtmlParser(how_to)}</div>
                        </Transition>
                        <Transition timeout={1000} in={activeNav == 'resources'} mountOnEnter={false} unmountOnExit={false} onEnter={animateEnter}>
                            <div className={activeNav == 'resources' ? 'fadeIn' : 'sr-only'}>{HtmlParser(resources)}</div>
                        </Transition>
                    </div>
                    </Transition>
                </Media>
            </MediaContextProvider> */}
    </div>
  );
};

export default NavItem;
