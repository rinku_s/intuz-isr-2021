//@ts-nocheck
import React, { useState } from 'react';
import { Modal } from 'react-bootstrap';

const BannerModel = (props) => {
    const [videoShowing, setVideoShowing] = useState(false)
    return (
        <React.Fragment>
        <Modal
        centered
        size="xl"
        show={props.show}
        onHide={props.hide}
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
        >
        <Modal.Header closeButton={true} />
        <Modal.Body>
            {<iframe  width="100%" height="500" src={props.link} frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen>
           </iframe>}           
        </Modal.Body>
      </Modal>
      <style>
          {`
            .modal-content{
                background: none;
                border:none;        
            }
            .close span:first-child{
                font-size: 35px;
                height: 40px;
                font-weight: 300;
                width: 40px;
                text-align: center;
                display: block;
            }
            .sr-only {
                display: none;
            }
            .modal-header {
                border: none;
            }
            `}
      </style>
    </React.Fragment>
    )
}

export default BannerModel
