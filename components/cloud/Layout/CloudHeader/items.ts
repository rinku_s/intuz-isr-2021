export  const cloudMenu = [
    {
        'label' : 'Applications',
        'link' : '/cloud' + '/stacks'
    }, {
        'label' : 'Services',
        'link' : '/cloud' + '/services'
    }, {
        'label' : 'Why Us',
        'link' : '/cloud' + '/whyus'
    }, {
        'label' : 'Case Studies',
        'link' : '/cloud' + '/case-studies'
    },
    {
        'label' : 'Docs',
        'link' : '/cloud' + '/docs'
    }
]