//@ts-nocheck
import BurgerNav from 'components/BurgerNav/BurgerNav'
import Container from 'components/Container/Container'
import Logo from 'components/Navigation/Logo'
import Link from 'next/link'
import React from 'react'
import { cloudMenu } from './items'
import NavMenu from './NavMenu'

const Header = () => {
    return (
        <header className="fixed top-0 left-0 bg-white w-full py-6 z-50" style={{boxShadow:"3px 4px 3px 1px #eee"}}>
            <Container className="flex items-center">
                <Link href="/">
                    <a className="z-50">
                        <Logo width="120px" />
                    </a>
                </Link>
                <NavMenu />
                <BurgerNav cloudLayout={cloudMenu} variant="dark" />
            </Container>
        </header>
    )
}

export default Header
