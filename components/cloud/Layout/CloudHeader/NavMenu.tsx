//@ts-nocheck
import Link from 'next/link';
import React from 'react';
import { cloudMenu } from './items';
import classes from './styles.module.scss';
const NavMenu = () => {
    return (
        <ul className="hidden md:flex list-none ml-auto items-center" style={{ gap: "5rem" }}>
            {cloudMenu.map(c => (
                <li className={classes.Item}>
                    <Link href={c.link}>
                        <a className="no-underline text-20 text-black-26">{c.label}</a>
                    </Link>
                </li>
            ))}
            <li>
                <a className="bg-brand-primary text-white py-3 px-3 text-2xl hover:bg-brand-secondary no-underline uppercase rounded-lg cursor-pointer" style={{ textDecoration: "none", transition: "all 0.3s ease" }}>Get Started</a>
            </li>
        </ul>
    )
}

export default NavMenu
