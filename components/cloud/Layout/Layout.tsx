//@ts-nocheck
import Head from "next/head";
import { useRouter } from "next/router";
import React from "react";
import Footer from "../../Footer/Footer";
import ScrollToTopButton from "../ScrollToTopButton/ScrollToTopButton";
import Header from "./CloudHeader/Header";

const Layout = (props) => {
  let basePath = process.env.NEXT_PUBLIC_BASE_PATH ? "/cloud" : "";
  let location = `https://www.intuz.com${useRouter().asPath}`;
  return (
    <>
      <Head>
        <link rel="canonical" href={location} />
        <meta name="og:url" content={location} />
      </Head>
      <Header />
      <main className="font-nunito">{props.children}</main>
      <Footer />

      <ScrollToTopButton />
      <style jsx>{``}</style>
      <style global jsx>
        {`
          body {
            overflow-y: visible;
          }
        `}
      </style>
    </>
  );
};

export default Layout;
