//@ts-nocheck
import React from 'react'
import { Spinner } from 'react-bootstrap';


const LoadingSpinner = () => {
    return (
        <div>
            
            <Spinner animation="border" variant="secondary" />
            <style jsx>
            {`
                div{
                    mav-width:100%;
                    display:flex;
                    align-items:center;
                    justify-content:center;
                    background:#FFF;
                }
            `}

            </style>
        </div>
    )
}

export default LoadingSpinner