//@ts-nocheck
//@ts-nocheck
import Container from 'components/cloud/Container/Container';
import React from 'react';
import parser from 'react-html-parser';
import classes from './styles.module.scss';
const Banner: React.FC<any> = ({ text, gradientText, paragraph, image, smallPara, gradientFirst, imageFirst, type }) => {
    return (
        <section className={`${classes.Banner} ${classes[type]}`}>
            <Container>
                {imageFirst ? <img className="mt-0" src={imageFirst} alt={gradientText} /> : ''}
                {gradientFirst ? <h1><span>{parser(gradientText)}</span> {parser(text)}</h1> : <h1>{parser(text)} <span>{parser(gradientText)}</span></h1>}
                {paragraph ? <h2>{parser(paragraph)}</h2> : ''}
                {image ? <img src={image} alt={gradientText} /> : ''}
                {smallPara && <small className={classes.Banner__Small}>{smallPara}</small>}
            </Container>
        </section>
    )
}

export default Banner
