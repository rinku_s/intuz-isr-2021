//@ts-nocheck
import gsap from 'gsap';
import Link from 'next/link';
import React from 'react';
import LazyLoad from 'react-lazyload';
import classes from './styles.module.scss';
const ResourceArticleContainer = (props) => {
    let imgb = React.useRef();
    let conb = React.useRef();


    function play() {
        const tl = gsap.timeline();
        tl.fromTo(imgb.current, 0.8, { x: -30, autoAlpha: 0 }, { x: 0, autoAlpha: 1, ease: 'Power2.easeOut' })
            .fromTo(conb.current, 0.8, { x: 30, autoAlpha: 0 }, { x: 0, autoAlpha: 1, ease: 'Power2.easeOut' }, "-=0.4");
    }

    let link = (
        <Link as={props.link} href="/[guides]" prefetch={false}>
            <a className={classes.btn} href={props.link}>Read More</a>
        </Link>
    )

    if (props.blog) {
        link = (
            <Link href="/blog/[name]" as={`/blog/${props.link}`} prefetch={false}>
                <a className={classes.btn} href={`/blog/${props.link}`}>Read More</a>
            </Link>

        )
    }


    return (

        <div className={`row ${classes.ResourceArticleContainer}`}>
            <div className="col-md-4 align-self-center text-center">
                <div className={classes.Image} ref={imgb} >
                    <LazyLoad
                        height={300}
                        offset={300}>
                        <img src={props.image + "?fm=pjpg&auto=format"} alt={props.title} />
                    </LazyLoad>
                </div>
            </div>
            <div className={`col-md-8 ${classes.content}`} ref={conb}>
                <h2><Link as={props.blog ? `/blog/${props.link}` : props.link} href={props.blog ? '/blog/[name]' : '/[guides]'} prefetch={false}><a>{props.title}</a></Link></h2>
                <p>{props.description}</p>
                {link}
            </div>
        </div>
        // </WaypointOnce >
    )
}

export default ResourceArticleContainer
