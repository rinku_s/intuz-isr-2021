//@ts-nocheck
import Head from 'next/head';
import React from 'react';
import ReactHtmlParser from 'react-html-parser';
import Layout from '../Layout/Layout';
import Scripttags from '../scripttags/scripttags';

export const HeadAndFootNew = ({data, children}) => {

    return (
        <div id="root"  className="cloud" style={{overflowX:"hidden"}}>
         <Head>
            {data && data.pages.length > 0 ? ReactHtmlParser(data.pages[0].meta_tags) : ''}
        </Head>
        <Layout>
        {children}
        </Layout>
       {data && <div id="GlobalSchema" dangerouslySetInnerHTML={{__html:data.pages[0].script_footer}}>
        </div>}
        {data && <Scripttags footertags={data.pages[0].after_load_scripts}/>}
    </div>
    )
}

export default HeadAndFootNew;