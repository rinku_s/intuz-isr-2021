//@ts-nocheck
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import Head from 'next/head';
import { useRouter } from 'next/router';
import React from 'react';
import ReactHtmlParser from 'react-html-parser';
import LoadingScreen from '../LoadingScreen/LoadingScreen';
import Scripttags from '../scripttags/scripttags';
export const HeadAndFoot = (props) => {
    const router = useRouter();
    const uq = gql`
    query{
        pages(where:{name:"Universal"}){
            script_footer
            after_load_scripts
            meta_tags
        }
    }
`

    const {  loading, error, data } =  useQuery(uq);

    if(error) return <div>Error Loading Meta</div>
    if(loading || props.loading) return <LoadingScreen/>

    return (
        <div id="root" style={{overflowX:"hidden"}}>
         <Head>
            {data.pages.length > 0 ? ReactHtmlParser(data.pages[0].meta_tags) : ''}
        </Head>
        {props.children}
        <div id="GlobalSchema" dangerouslySetInnerHTML={{__html:data.pages[0].script_footer}}>
        </div>
        <Scripttags footertags={data.pages[0].after_load_scripts}/>
    </div>
    )
}

export default HeadAndFoot;