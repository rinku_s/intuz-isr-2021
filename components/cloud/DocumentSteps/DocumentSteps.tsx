//@ts-nocheck
//@ts-nocheck
import React from 'react'
import HtmlParser from 'react-html-parser'
import Container from '../Container/Container'
import classes from './styles.module.scss'
const DocumentSteps = ({ content }) => {
    return (
        <article className={classes.DocumentSteps}>
            <Container>
                {HtmlParser(content)}
            </Container>
        </article>
    )
}

export default DocumentSteps
