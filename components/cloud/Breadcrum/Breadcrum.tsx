//@ts-nocheck
import React from 'react'

export interface BreadcrumProps {
  dt?: string,
  name: string,
  nest?: string,
  slug?: string,
  doc_page?: string
}

const Breadcrum: React.FC<any> = (props) => {

  let schema = `
  {
         "@context": "https://schema.org",
         "@type": "BreadcrumbList",
         "itemListElement": 
       [
                 {
                 "@type": "ListItem",
                  "position": 1,
                   "name": "Home",
                 "item": "https://www.intuz.com/"
                  },
                 {
                         "@type": "ListItem",
                          "position": 2,
                          "name": "Cloud Home",
                          "item": "https://www.intuz.com/cloud"
                 },
                 {
                         "@type": "ListItem",
                          "position": 3,
                          "name": "${props.name}"
                 }
       ]
 }
 `
  if (props.dt) {
    schema = `
   {
      "@context": "https://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": 
    [
              {
                "@type": "ListItem",
                "position": 1,
                "name": "Home",
                "item": "https://www.intuz.com/"
              },
              {
                      "@type": "ListItem",
                      "position": 2,
                      "name": "Cloud Home",
                      "item": "https://www.intuz.com/cloud"
              },
              {
                      "@type": "ListItem",
                      "position": 3,
                      "name": "Application list",
                      "item": "https://www.intuz.com/cloud/stacks/"
              },
              {
                "@type": "ListItem",
                "position": 4,
                "name": "${props.name}"
              }
    ]
  }
   `
    if (props.nest) {
      schema = `
    {
       "@context": "https://schema.org",
       "@type": "BreadcrumbList",
       "itemListElement": 
     [
               {
               "@type": "ListItem",
               "position": 1,
                 "name": "Home",
               "item": "https://www.intuz.com/"
               },
               {
                       "@type": "ListItem",
                       "position": 2,
                       "name": "Cloud Home",
                       "item": "https://www.intuz.com/cloud"
               },
               {
                       "@type": "ListItem",
                       "position": 3,
                       "name": "Application list",
                       "item": "https://www.intuz.com/cloud/stacks/"
               },
               {
                 "@type": "ListItem",
                 "position": 4,
                 "item": "https://www.intuz.com/cloud/stacks/${props.slug}",
                 "name": "${props.name}"
               },
               {
                "@type": "ListItem",
                "position": 5,
                "name": "${props.name}"
              }
     ]
   }
    `
    }
  }

  if (props.name == "Cloud HomePage") {
    schema = `{
      "@context": "https://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": 
    [
              {
                "@type": "ListItem",
                "position": 1,
                "name": "Home",
                "item": "https://www.intuz.com/"
               },
              {
                "@type": "ListItem",
                "position": 2,
                "name": "Cloud Home"
              }
    ]
}`
  }


  if (props.name == "AWS Database Migration") {
    schema = `{
      "@context": "https://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": 
    [
              {
              "@type": "ListItem",
               "position": 1,
                "name": "Home",
              "item": "https://www.intuz.com/"
               },
              {
                      "@type": "ListItem",
                       "position": 2,
                       "name": "Cloud Home",
                       "item": "https://www.intuz.com/cloud/"
              },
              {
                      "@type": "ListItem",
                       "position": 3,
                       "name": "Services",
                       "item": "https://www.intuz.com/cloud/services/"
              },
              {
                      "@type": "ListItem",
                       "position": 4,
                       "name": "${props.name}"
              }
    ]
}`
  }

  if (props.name == "Cloudfront CDN") {
    schema = `{
      "@context": "https://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": 
    [
              {
                "@type": "ListItem",
                "position": 1,
                "name": "Home",
                "item": "https://www.intuz.com/"
              },
              {
                      "@type": "ListItem",
                       "position": 2,
                       "name": "Cloud Home",
                       "item": "https://www.intuz.com/cloud/"
              },
              {
                      "@type": "ListItem",
                       "position": 3,
                       "name": "Documentation",
                       "item": "https://www.intuz.com/cloud/docs/"
              },
              {
                      "@type": "ListItem",
                       "position": 4,
                       "name": "${props.name}"
              }
    ]
}`
  }

  if (props.doc_page) {
    schema = `
   {
      "@context": "https://schema.org",
      "@type": "BreadcrumbList",
      "itemListElement": 
    [
              {
              "@type": "ListItem",
              "position": 1,
                "name": "Home",
              "item": "https://www.intuz.com/"
              },
              {
                      "@type": "ListItem",
                      "position": 2,
                      "name": "Cloud Home",
                      "item": "https://www.intuz.com/cloud"
              },
              {
                      "@type": "ListItem",
                      "position": 3,
                      "name": "Documentations",
                      "item": "https://www.intuz.com/cloud/docs/"
              },
              {
                "@type": "ListItem",
                "position": 4,
                "name": "${props.name}"
              }
    ]
  }
   `
  }

  return (
    <script type="application/ld+json" dangerouslySetInnerHTML={{ __html: schema }}>
    </script>
  )
}

export default Breadcrum
