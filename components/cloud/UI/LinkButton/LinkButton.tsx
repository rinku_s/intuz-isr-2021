//@ts-nocheck
import React from 'react';
import classes from './styles.module.scss';
import Link from 'next/link';

const LinkButton = ({ variation, className, href, onClick, style, title, as, children, newTab, atype, clink  }) => {
    // let variation = variation
    let basePath  = process.env.NEXT_PUBLIC_BASE_PATH ? '/cloud' : '' 
    // let basePath  = '/cloud' 
    let link = (
        <a className={`${classes.LinkButton} ${classes[variation]} ${className ? className : ''}`} href={href} onClick={onClick} title={title} style={style}>
        {children}
        </a>
    )

    if(atype) {
        link = (
            <a className={`${classes.LinkButton} ${classes[variation]} ${className ? className : ''}`} href={href} onClick={onClick} title={title} style={style}>
                {children}
            </a>
        )
        if(newTab){
            link = (
                <a target="_blank" rel="nofollow noopener" className={`${classes.LinkButton} ${classes[variation]} ${className ? className : ''}`} href={href} onClick={onClick} title={title} style={style}>
                {children}
                </a>
            )
        }
    }


    if(href !== undefined && !atype) {
        if(clink){

            let newlink = basePath + href;
            let newas = as ? basePath + as : '';
            // console.log(basePath, href, newlink);
            
            link = (
                <Link href={newlink} as={newas} prefetch={false}>
                    <a className={`${classes.LinkButton} ${classes[variation]} ${className ? className : ''}`} onClick={onClick} title={title} style={style}>
                        {children}
                    </a>
                </Link>
            )
        } else{
            link = (
                <Link href={href} as={as} prefetch={false}>
                    <a className={`${classes.LinkButton} ${classes[variation]} ${className ? className : ''}`} onClick={onClick} title={title} style={style}>
                        {children}
                    </a>
                </Link>
            )
        }
    }

    

    return (
        <>
            {link}
        </>
    )
}

export default LinkButton
