//@ts-nocheck
import React from "react";
import classes from "./styles.module.scss";
import { cdn } from "config/cdn";

const data = [
  {
    img: cdn("cloud_right_sizing.svg"),
    title: "Right Sizing",
  },
  {
    img: cdn("cloud_instances.svg"),
    title: "Reserved Instances",
  },
  {
    img: cdn("cloud_increase-elasticity.svg"),
    title: "Increase Elasticity",
  },
  {
    img: cdn("cloud_measure_new.svg"),
    title: "Measure, Monitor, & Improve",
  },
];

const CostAnalysis = () => {
  return (
    <div className={classes.CostAnalysis}>
      <h4>The Four Pillars of Cost Optimization</h4>
      <div className="flex flex-col sm:flex-row justify-content-around items-center flex-wrap">
        {data.map((dt) => (
          <div className="text-center pb-4 my-5" key={dt.title}>
            <div className={classes.image}>
              <img
                className="lazyload img-fluid"
                data-src={dt.img}
                alt={dt.title}
              />
            </div>
            <h5>{dt.title}</h5>
          </div>
        ))}
      </div>
    </div>
  );
};

export default CostAnalysis;
