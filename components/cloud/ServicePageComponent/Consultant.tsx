//@ts-nocheck
import React from 'react';
import { cdn } from 'config/cdn';
import classes from './styles.module.scss';

const data = [
    {
        img: cdn('cloud_boosted.svg'),
        title:"Boosted IT Model",
        desc:"Rouse your business with extensive flexibility, agility and productivity through tailored strategies."
    },
    {
        img: cdn('cloud_commercialization.svg'),
        title:"Commercialization",
        desc:"Intuz promises to design revenue generating cloud strategies that increase profitability."
    },
    {
        img: cdn('cloud_competitive.svg'),
        title:"Competitive Edge",
        desc:"Attain competitive advantages with tailored cloud strategies in today’s fast paced digital world."
    }
]


const Consultant = () => {
    return (
        <div className={`row ${classes.Consultant}`}>
            {data.map(dt=>(
                <div key={dt.title} className="col-sm-4 text-center">
                    <div className={classes.image}>
                        <img className="lazyload img-fluid" data-src={dt.img} alt={dt.title} />
                    </div>
                    <h3>{dt.title}</h3>
                    <p>{dt.desc}</p>
                </div>
            ))}
        </div>
    )
}

export default Consultant
