//@ts-nocheck
import React from "react";
import classes from "./styles.module.scss";
const ArrowRight = () => {
  return (
    <div className={classes.ArrowRight}>
      <img
        className="lazyload"
        data-src="/cloud/static/arrow-right.png"
        alt="Right Arrow"
      />
    </div>
  );
};

const Diagram = () => {
  return (
    <div className={classes.Diagram}>
      <div className={classes.Diagram__ARL}>
        <img
          className="lazyload"
          data-src="/cloud/static/round-left.png"
          alt="Rounded Left"
        />
      </div>

      <div className={classes.Diagram__UP}>
        <p>Migration & Validation</p>
        <img
          className="lazyload"
          data-style={{ marginRight: "1rem" }}
          src="/cloud/static/truck.png"
          alt="Truck"
        />
        <img
          className="lazyload"
          data-src="/cloud/static/tab-analysis.png"
          alt="Tab Analysis"
        />
      </div>
      <div style={{ marginBottom: "9rem" }}></div>
      <div className={classes.Diagram__DOWN}>
        <img
          className="lazyload"
          data-src="/cloud/static/phone-img.png"
          alt="Phone Image"
        />
        <p>Application Design</p>
      </div>

      <div className={classes.Diagram__ARR}>
        <img
          className="lazyload"
          data-src="/cloud/static/right-rounded-arrow.png"
          alt="Rounded Right"
        />
      </div>
    </div>
  );
};

const Migration = () => {
  return (
    <div className={`${classes.Migration} flex flex-wrap items-center mb-5`}>
      <div>
        <img
          className="lazyload"
          data-src="/cloud/static/oppurtunity-evaluation.png"
          alt="Opportunity Evaluation"
        />
        <p>Opportunity Evaluation</p>
      </div>
      <ArrowRight />
      <div>
        <img
          className="lazyload"
          data-src="/cloud/static/portfolio.png"
          alt="Portfolio Discovery and Planning"
        />
        <p>Portfolio Discovery and Planning</p>
      </div>
      <ArrowRight />
      <Diagram />
    </div>
  );
};

export default Migration;
