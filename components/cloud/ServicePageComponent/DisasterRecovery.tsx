//@ts-nocheck
import React from 'react'
import Paragraph from '../Utility/Paragraph'
import classes from './styles.module.scss';
import { cdn } from 'config/cdn';

const data = [
    {
        img: cdn('cloud_improved.svg'),
        title:"Improved Performance",
        desc:"Cloud allows for fast retrieval of files and disk-based storage by following high protection standards."
    },
    {
        img: cdn('cloud_compliance.svg'),
        title:"Compliance",
        desc:"Quick retrieval of files and data helps in avoiding fines for missing the deadlines."
    },
    {
        img: cdn('cloud_elasticity.svg'),
        title:"Elasticity",
        desc:"Insert n number of data swiftly that is easy to delete and expire without handling media."
    }
]


const DisasterRecovery = () => {
    return (
        <div className={`row ${classes.DisasterRecovery}`}>
            {data.map(dt=>(
                <div key={dt.title} className="col-sm-4 text-center px-5">
                    <div className={classes.image}>
                        <img className="lazyload img-fluid mb-3" data-src={dt.img} alt={dt.title} />
                    </div>
                    <h3>{dt.title}</h3>
                    <Paragraph type="large">{dt.desc}</Paragraph>
                </div>
            ))}
        </div>
    )
}

export default DisasterRecovery
