//@ts-nocheck
//@ts-nocheck
import React from 'react';
import LazyLoad from 'react-lazyload';
import ImageLoader from '../../../ImageLoader/ImageLoader';
import classes from './partner.module.scss';

const Partner = (props) => {
    return (
        <div className={`col-sm-12 col-md-4 col-lg-2 ${classes.Partner}`} >
            <LazyLoad height={300} offset={300}>
                <ImageLoader src={props.imgSrc} alt={props.alt} className={props.class} />
            </LazyLoad>
        </div>
    )
}

export default Partner;