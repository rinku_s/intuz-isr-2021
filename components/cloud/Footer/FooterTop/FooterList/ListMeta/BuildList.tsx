//@ts-nocheck
import React from 'react';
import styles from '../footerlist.module.scss';
import List from './List';

const BuildList = (props) => {
    return (
        <div className={`col-sm-12 col-md-6 col-lg-2`}>
            <h6 className={styles.Title}>{props.metaTitle}</h6>
            <ul>
                <List list={props.metaList} />
            </ul>
        </div>
    )
}

export default BuildList;