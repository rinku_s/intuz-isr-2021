//@ts-nocheck
import Link from 'next/link';
import React from 'react';
const List = (props) => {
    return (
       <>
            {props.list.map((item,i) => (
                <li key={i}>
                    <Link href={item.link} prefetch={false}>
                        {
                            item.externalurl ? <a target='_blank' rel="noreferrer">{item.title}</a> : <a> {item.title} </a>
                        }
                    </Link>
                </li>
            ))}
       </>
    )
}

export default List;