//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap';
import ScrollToPlugin from 'gsap/dist/ScrollToPlugin';
import React, { useEffect, useState } from 'react';
import classes from './styles.module.scss';

gsap.registerPlugin(ScrollToPlugin);

const ScrollToTopButton = () => {

    let r = React.useRef();
    const [privacy, setPrivacy] = useState(false)

    function visible() {
        if (document.body.scrollTop > 1000 || document.documentElement.scrollTop > 1000) {
            r.current.style.display = "block";
        } else {
            r.current.style.display = "none";
        }
    }

    useEffect(() => {
        if (localStorage.getItem('acceptprivacy')) {
            setPrivacy(true)
        }
    }, [])

    useEffect(() => {
        window.addEventListener('scroll', visible, false)
        return () => {
            window.removeEventListener('scroll', visible, false)
        }
    }, [])

    function scroll() {
        gsap.to(window, 1, { scrollTo: { y: 0, autoKill: false }, ease: "Power1.easeOut" });
    }

    return (
        <div className={`${classes.ScrollButton} ${privacy ? classes['privacy'] : ''}`} id='topscroll' onClick={scroll} ref={r}>
            <img src='/cloud/static/scroll-top.png' alt='scroll-top' />
        </div>
    )
}

export default ScrollToTopButton
