//@ts-nocheck
//@ts-nocheck
import React from 'react';
import classes from './styles.module.scss';
const Container: React.FC<any> = ({ className, myRef, style, children }) => {
    const styleClass = (className) ? ' ' + className : '';
    return (
        <div ref={myRef} className={`${classes.Container}${styleClass}`} style={style}>
            {children}
        </div>
    )
}

export default Container
