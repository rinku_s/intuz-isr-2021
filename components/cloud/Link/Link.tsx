//@ts-nocheck
import React from 'react'
import OLink from 'next/link'
const Link = ({ href, as, children }) => {

    const basePath = process.env.NEXT_PUBLIC_BASE_PATH ? '/cloud' : '';

    return (
        <OLink href={basePath+href} as={basePath+as}>
            {children}
        </OLink>
    )
}

export default Link
