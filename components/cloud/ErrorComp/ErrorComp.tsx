//@ts-nocheck
//@ts-nocheck
import React from 'react';
import LinkButton from '../UI/LinkButton/LinkButton';
import classes from './styles.module.scss';
const ErrorComp = (props) => {
    return (
        // <Layout>
        <div className={classes.Error}>
            <img src={require('../../static/oops.svg')} alt="Oops" />
            <h3>{props.statusMessage}</h3>
            <p>{props.description}</p>
            <LinkButton variation="purpleBtn" href="/">GO TO HOME PAGE</LinkButton>
        </div>
        // </Layout>
    )
}

export default ErrorComp
