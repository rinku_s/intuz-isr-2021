//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap';
import React, { useEffect } from 'react';
import LazyLoad from 'react-lazyload';
import { Transition } from 'react-transition-group';
import { cdn } from '../../config/cdn';
import { Media, MediaContextProvider } from "../../config/responsiveQuery";
import classes from './styles.module.scss';

const Box = (props) => {
    let { show, activeId, classname } = props;
    let cs = false;
    if (show && activeId === classname) {
        cs = true;
    }

    useEffect(() => {
        gsap.fromTo(`.${classes.active}`, 0.5, { y: -10, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" })
    }, [activeId]);

    return (
        <MediaContextProvider>
            <div className={classes.Box}>
                <LazyLoad height={50} offset={100}>
                    <img src={cdn(props.image.name)} alt={props.title} />
                </LazyLoad>
                <h6>{props.title}</h6>

                <p>{props.desc} <br /><a onClick={() => props.setActive(props.classname)}>More</a></p>
                <Media lessThan="sm">
                    <Transition in={cs}
                        timeout={500}
                        mountOnEnter={true}
                        unmountOnExit={true}
                        addEndListener={(n, done) => {
                            if (cs) {
                                gsap.fromTo(n, 0.5, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" });
                            } else {
                                gsap.fromTo(n, 0.5, { y: 0, opacity: 1, ease: "Power0.ease" }, { y: -20, opacity: 0, ease: "Power0.ease" });
                            }
                        }}
                    >
                        {state => <p className={classes.active}>{props.info}</p>}
                    </Transition>
                </Media>
            </div>
        </MediaContextProvider>
    )
}

export default Box
