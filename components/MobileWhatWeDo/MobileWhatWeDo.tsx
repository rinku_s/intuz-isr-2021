//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap';
import React, { useEffect, useState } from 'react';
import { Transition } from 'react-transition-group';
import { Media, MediaContextProvider } from "../../config/responsiveQuery";
import Box from './Box';
import classes from './styles.module.scss';

const whatcontent = [
    {
        id: "a",
        title: "Native Mobile App Development",
        image: "/static/Images/services-we-offer.png",
        desc: "Develop a native Android & IOS app with indulging UI/UX that serves a myriad of devices",
        info: "For businesses focused on user experience, native mobile apps are the best bet as they developed using specific programming languages like Java or Kotlin for Android and Objective C or Swift for iOS and are built for the device. Let our Intuz's expert mobile app developers handcraft an indulging native Android and IOS apps that provide fast performance and a high degree of reliability and security."
    },
    {
        id: "b",
        title: "React Native Mobile App Development",
        image: "/static/Images/services-we-offer1.png",
        desc: "Create a native mobile app experience with the advantage of having a single code base and the substantially less development time with the use of react native.",
        info: "One of the major advantages of creating react native for your mobile app development is to cut the development time and expenses by overcoming the limitation of native app development that is platform dependency."
    },
    {
        id: "c",
        title: "Enterprise Mobility Solutions",
        image: "/static/Images/services-we-offer.png",
        desc: "Improve your business productivity & ROI by utilizing our cutting-edge enterprise development solutions.",
        info: "Perhaps the most important decision an enterprise must make is to choose the right technology partner. Intuz's enterprise mobility solutions fuel your business growth by building enterprise-scale secure mobile apps, web apps, software, and processes that make your business future ready."
    }
]

const MobileWhatWeDo = (props) => {

    const [activeId, setActiveId] = useState("a");
    const [show, setShow] = useState(false);
    const setFilter = (id) => {
        if (id == activeId) {
            setShow(!show);
        } else {
            setActiveId(id);
            setShow(true);
        }
    }

    useEffect(() => {
        gsap.fromTo(`.${classes.info}`, 0.5, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" })

    }, [activeId]);


    return (
        <MediaContextProvider>
            <div className="row mt-5">
                {props.whatwedomobiles.map(wc => (
                    <div className="col-md-4" key={wc.title}>
                        <Box {...wc} setActive={setFilter} activeId={activeId} show={show} />
                    </div>
                ))}
                <Media greaterThanOrEqual="sm">
                    <Transition in={show}
                        timeout={1000}
                        mountOnEnter={true}
                        unmountOnExit={true}
                        onEntering={(n, appearing) => {
                            return gsap.fromTo(n, 1, { y: -20, autoAlpha: 0, ease: "Power0.ease" }, { y: 0, autoAlpha: 1, ease: "Power0.ease", onComplete: appearing }).play();
                        }}
                        onExiting={(n, disapp) => {
                            return gsap.fromTo(n, 1, { y: 0, autoAlpha: 1, ease: "Power0.ease" }, { y: -20, autoAlpha: 0, ease: "Power0.ease", onComplete: disapp }).play();
                        }}
                    >
                        {state => <div className="col-12">
                            {activeId == null ? <p className={`${classes.info} ${classes[activeId]}`}>{props.whatwedomobiles.find(id => id.classname == "a").info}</p> : <p className={`${classes.info} ${classes[activeId]}`}>{props.whatwedomobiles.find(id => id.classname == activeId).info}</p>}
                        </div>
                        }
                    </Transition>
                </Media>
            </div>
        </MediaContextProvider>
    )
}

export default MobileWhatWeDo
