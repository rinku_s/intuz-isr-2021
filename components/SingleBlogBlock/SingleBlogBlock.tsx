//@ts-nocheck


import React from 'react';
import Block from './Block';
import style from './styles.module.scss';

const SingleBlogBlock = (props) => {   
    return (
        <div className={`${style.SingleBlogBlock}`}>
            {props.blogs.map((blog, index) => <Block {...blog} key={index} length={props.length} />)}
        </div> 
    )
}

export default SingleBlogBlock
