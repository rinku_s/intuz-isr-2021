//@ts-nocheck


import gsap from 'gsap';
import Link from 'next/link';
import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';
import windowWidth from '../../hooks/windowWidth';
import BlogAuthor from '../BlogAuthor/BlogAuthor';
import Category from '../BlogCategory/Category';
import style from './styles.module.scss';

const Block = (props) => {
    let blogBlock = React.useRef();
    let windowSize = windowWidth();
    !props.filter &&// setOpacity(`.${style.blogBlock}`);

        function play() {
            const tl = gsap.timeline();
            tl.fromTo(blogBlock.current, 0.8, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power1.easeOut' })
        }

    return (

        <div className={`${style.blogBlock} ${props.length == 1 ? style['singleItem'] : ''} ${style[props.className]}`} ref={blogBlock}>
            <Link as={`/blog/${props.link}`} href='/blog/[name]' prefetch={false}>
                <a>
                    <LazyLoad height={300} offset={300}>
                        <img src={`${cdn(props.resourceBlockImage.name)}?auto=format&fm=pjpg&h=${props.length > 2 || windowSize < 901 ? '250' : '350'}`} alt={props.resourceBlockImage.name} className={style.blockImage} />
                    </LazyLoad>
                </a>
            </Link>
            {(props.blog_category && props.variation != 'hideCategory') ? <Category categories={props.blog_category} /> : ''}
            <h3>
                <Link as={`/blog/${props.link}`} href='/blog/[name]' prefetch={false}>
                    <a>{props.title}</a>
                </Link>
            </h3>
            {props.variation != 'hideAuthor' && <BlogAuthor
                name={props.blog_author && props.blog_author.name}
                date={props.updatedAt}
                image={props.blog_author && cdn(props.blog_author.image.name)}
                link={props.blog_author && props.blog_author.link}
            />}
        </div>
        // </WaypointOnce >
    )
}

export default Block
