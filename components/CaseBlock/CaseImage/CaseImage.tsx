//@ts-nocheck
//@ts-nocheck
import { myLoader } from 'config/image-loader';
import Image from 'next/image';
import React from 'react';
import { Media, MediaContextProvider } from "../../../config/responsiveQuery";
import styles from './caseimage.module.scss';

const CaseImage = (props) => {
    const block = props.blockDetail;
    const downArrow = `url(/static/Images/icons/down-arrow1.png) top center no-repeat`;
    return (
        <MediaContextProvider>
            <div className={`${styles.CaseImageBlock}`}>
                <div>
                    {/* <ImageBlock
                        src={cdn(block.backimgSrc)}
                        alt={block.backimgtitle}
                        dwidth={755}
                        mwidth={755}
                        title={block.backimgtitle} /> */}
                    <Image loader={myLoader} src={block.backimgSrc}
                        layout='fixed'
                        alt={block.backimgtitle}
                        width={755}
                        height={755} />

                </div>
                <Media lessThan="sm">
                    <div className={styles.MobileArrow} ><div style={{ background: downArrow }}></div></div>
                </Media>
            </div>
        </MediaContextProvider>
    )
}

export default CaseImage;


