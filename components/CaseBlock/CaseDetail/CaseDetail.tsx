//@ts-nocheck
//@ts-nocheck
import { myLoader } from 'config/image-loader';
import Image from 'next/image';
import React from 'react';
import { Media, MediaContextProvider } from '../../../config/responsiveQuery';
import IconBlock from '../../IconBlock/IconBlock';
import LinkButton from '../../UI/LinkButton/LinkButton';
import styles from './casedetail.module.scss';

const CaseDetail = (props) => {

    const block = props.blockDetail;
    return (
        <div className={`${styles.CaseDetailBlock} ${styles[props.blockDetail.variation]}`} >
            <Image src={block.logoSrc} loader={myLoader} layout='intrinsic' height={120} width={400} alt={block.logoTitle} title={block.logoTitle} className={styles.Logo} />
            <h3>{block.title}</h3>
            <div className='text-center'>
                <p>{block.subTitle}</p>
                <IconBlock icons={block.icons} />
            </div>
            <MediaContextProvider>
                <Media greaterThanOrEqual="sm">
                    <LinkButton as={block.btnLink} href={"/case-studies/[id]"} variation={block.btnVariation ? block.btnVariation : 'caseBlockBtn'}>View Case Study</LinkButton>
                </Media>
                <Media lessThan="sm">
                    <LinkButton as={block.btnLink} href={"/case-studies/[id]"} variation={'caseBlockBtn'}>View Case Study</LinkButton>
                </Media>
            </MediaContextProvider>
        </div>
    )
}
export default CaseDetail;