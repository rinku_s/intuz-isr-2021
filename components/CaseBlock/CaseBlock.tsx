//@ts-nocheck


import React from 'react';
import styles from './caseblock.module.scss';
import CaseDetail from './CaseDetail/CaseDetail';
import CaseImage from './CaseImage/CaseImage';

const CaseBlock = (props) => {
    let variation;
    if(props.blockDetail.variation){
        variation = props.blockDetail.variation;
        
    }
    return (
        <>
            <div className={`${styles.CaseBlock} ${styles[variation]}`} >
                <CaseImage blockDetail = {props.blockDetail} />
                <CaseDetail blockDetail = {props.blockDetail} />
            </div>
        </>
    )
}

export default CaseBlock;