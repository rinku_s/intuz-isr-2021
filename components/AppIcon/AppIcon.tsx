//@ts-nocheck
import React from 'react';
import SVG from 'react-inlinesvg';
import styles from './appicon.module.scss';

const AppIcon = (props) => {
    let variation = props.variation
    return (
        <span className={`${styles.Icon} ${styles[variation]}`}>
            {/* {props.imgSrc ?  : ''} */}
            <SVG src={props.imgSrc} />
        </span>
    )
}

export default AppIcon;