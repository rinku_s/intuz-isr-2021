import React from "react";
import SectionHeading from "./SectionHeading";
import SubHeading from "./SubHeading";

const HeadAndSub: React.FC<{ heading: string; subHeading: string }> = (
  props
) => {
  return (
    <>
      <SectionHeading>{props.heading}</SectionHeading>
      <SubHeading>{props.subHeading}</SubHeading>
    </>
  );
};

export default HeadAndSub;
