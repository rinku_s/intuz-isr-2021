//@ts-nocheck


import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';
import style from './styles.module.scss';

const Protocols = (props) => {
    return (
        <div className={`${style.ProtocolDiv} row`}>
            {
                props.content && props.content.map((content) => 
                    <div className='col-md-4 col-sm-6' key={content.id} >
                        <LazyLoad height={300} offset={300}>
                            <img src={cdn(content.icon.name + '?auto=format')} alt={content.icon.name} />
                        </LazyLoad>
                        <h4>{content.title}</h4>
                        {content.subtitle && <span>{content.subtitle}</span>}
                    </div>
                )
            }
        </div>
    )
}

export default Protocols