//@ts-nocheck

import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';
import style from './styles.module.scss';

const HighlightIcon = (props) => {
    if(props.content.length <= 0) {
        return null
    } else {
        return (
            <div className={style.iconGrp}>
            {
                props.content.map((icongrp, index) => {
                    return(
                        <div key={index}>
                            <LazyLoad height={300} offset={300}>
                                <img src={cdn(icongrp.icon.name)} alt={icongrp.icon.name} width="80px" height="80px"/>
                            </LazyLoad>
                            <span>{icongrp.name}</span>
                        </div>
                    )
                })
            }
            </div>
        )
    }
}

export default HighlightIcon