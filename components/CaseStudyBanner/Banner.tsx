//@ts-nocheck
import React from 'react';
import { cdn } from '../../config/cdn';
import AppIcon from '../AppIcon/AppIcon';
import Container from '../Container/Container';
import style from './styles.module.scss';


const TopBanner = (props) => {
    return (
        <Container className={style.BannerContent}>
                    <div className={style.leftContent}>
                        {props.content.logo ? <div><img src={cdn(props.content.logo)} alt={props.content.logo} /></div> : ''}
                        {props.content.title ? <h2>{props.content.title}</h2> : ''}
                        <div className={style.iconBlock}>
                            {props.content.icons.map((icon, index) => {
                                return(
                                    <AppIcon
                                        imgSrc= {icon.src}
                                        alt= {icon.alt}
                                        key= {index}
                                    />
                                )
                            })}
                        </div>
                    </div>
                    <div className={style.rightContent}>
                        {props.content.topImage ? <img src={cdn(props.content.topImage)} alt={props.content.topImage} /> : ''}
                    </div>
            </Container>
    )
}

export default TopBanner