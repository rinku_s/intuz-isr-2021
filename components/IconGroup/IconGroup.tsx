import { cdn } from "config/cdn";
import Image from "next/image";
import React from "react";
import style from "./styles.module.scss";

const IconGroup = (props) => {
  if (props.children.length <= 0) {
    return null;
  } else {
    return (
      <div className={`${style.Content} row`}>
        {props.children.map((data, index) => {
          return (
            <div className="col-md-3 col-sm-4" key={index}>
              <div className='mx-auto' style={{ height: '80px ', width: '80px', position: 'relative' }}>
                <Image
                  src={cdn(data.icon.name)}
                  unoptimized
                  layout={'fill'}
                  objectFit={'contain'}
                />
              </div>
              <h4 className={'mt-14'}
                style={{
                  fontWeight: 300,
                  color: "#7e889c",
                }}
              >
                {data.title}
              </h4>
              <p>{data.description}</p>
            </div>
          );
        })}
      </div>
    );
  }
};
export default IconGroup;
