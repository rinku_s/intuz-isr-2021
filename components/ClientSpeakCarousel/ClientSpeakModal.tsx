//@ts-nocheck
import React, { useState } from 'react';
import { Modal } from 'react-bootstrap';
const ClientSpeakModal = (props) => {
    const [videoShowing, setVideoShowing] = useState(false)
    return (
        <React.Fragment>
        <Modal
        centered
        size="xl"
        show={props.show}
        onHide={props.hide}
        dialogClassName="modal-90w"
        aria-labelledby="example-custom-modal-styling-title"
        >
        <Modal.Body>
            {<iframe  width="100%" height="500" src={props.link} frameBorder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowFullScreen>
           </iframe>}           
        </Modal.Body>
      </Modal>
      <style>
          {`
            .modal-content{
                background: none;
                border:none;        
            }
            `}
      </style>
    </React.Fragment>
    )
}

export default ClientSpeakModal
