//@ts-nocheck
import { myLoader } from 'config/image-loader';
import Image from 'next/image';
import React, { useState } from 'react';
import { Carousel } from 'react-bootstrap';
import windowWidth from '../../hooks/windowWidth';
import Container from '../Container/Container';
import ClientSpeakModal from './ClientSpeakModal';
import classes from './styles.module.scss';
const clientSpeak = [
    {
        id: 1,
        desc: "Working with Intuz was a great experience. They focused on the client's requirements, have and can do attitude and deal positively with challenges that emerge during the project development. All at the good price.",
        name: "Digi-Gift",
        appImage: "digigift_bg.jpg",
        link: "#digi",
        clientImage: "dig-life-csb.jpg",
        designation: "Digi-Gift, Australia",
    },
    {
        id: 2,
        desc: "I really enjoyed working with the Intuz team. They offered me great expertise and very good advises on all of my current and future projects.",
        name: "Aphos",
        appImage: "aphos_csb.jpg",
        link: "",
        clientImage: "aphos-client-csb.png",
        designation: "Founder, Aphos",
    },
    {
        id: 3,
        desc: "They impressed me so much that they are going to handle all of our digital needs going forward because of the experience I have had with them!",
        name: "Pocket Money",
        appImage: "pocket_csb.jpg",
        link: "https://www.youtube.com/embed/sPKomTrhjt8",
        clientImage: "pocketmoney-client-csb.png",
        designation: "Founder, PocketMoney Coupons",
    },
    {
        id: 4,
        desc: "They are great for your first, second, third apps and beyond. Very high communication with the same project manager has been wonderful.",
        name: "Lyricnote",
        appImage: "lyricnot_csb.jpg",
        link: "#digi",
        clientImage: "lyric-note-client.jpg",
        designation: "Founder, Lyricnote",
    },
    {
        id: 5,
        desc: "“Intuz helped me to create a beautiful app that my users love to interact with! Picit went from an idea in my head to an app that is enjoyed by thousands!",
        name: "Picit",
        appImage: "picit_csb.jpg",
        link: "#digi",
        clientImage: "picit-client-csb.jpg",
        designation: "Founder, Picit",
    },
    {
        id: 6,
        desc: "Intuz not only provided a great value but their team actually understood our goals better and introduced a creative design which saved us monies and introduced features that I thought I'd have to wait another two years for.",
        name: "1church1day",
        appImage: "chruch-csb.jpg",
        link: "https://www.youtube.com/embed/dt0UlCQtV8U",
        clientImage: "church-client-csb.png",
        designation: "Founder – Kingdom League International, USA",
    }
]

const ClientSpeakCarousel = (props) => {
    const [modalShow, setModalShow] = useState(false);
    const [activeIndex, setActiveIndex] = useState(0);
    const [direction, setDirection] = useState(null);
    const [activeLink, setActiveLink] = useState(null);


    const handleSelect = (selectedIndex, e) => {
        setActiveIndex(selectedIndex);
        // setDirection(e.direction);
    };

    const fethYoutube = (link) => {
        setActiveLink(link);
        setModalShow(!modalShow);
    }

    let windowSize = windowWidth();

    return (
        <div className={classes.ClientSpeakCarousel}>

            <Carousel controls={false} indicators={false} activeIndex={activeIndex} onSelect={handleSelect}>
                {clientSpeak.map(cs => {
                    return (
                        <Carousel.Item key={cs.id}>
                            <Image src={cs.appImage} alt="Banner 1" loader={myLoader} layout={'fill'} />
                            <Carousel.Caption>
                                <Container>
                                    <h3>{cs.desc}</h3>
                                    <p>{cs.designation}</p>
                                    {(cs.link.search("https://") < 0) ?
                                        // <a className={classes.Arrow} href={cs.link}>&nbsp;</a> 
                                        ''
                                        :
                                        <a className={classes.video} onClick={() => fethYoutube(cs.link)}>
                                            {/* <img src={cdn('video_icon.png')} alt="Video Icon" /> */}
                                            <Image src={'video_icon.png'} alt="Video Icon" loader={myLoader} layout={'fixed'} width={60} height={60} />

                                        </a>}

                                </Container>
                            </Carousel.Caption>
                        </Carousel.Item>
                    )
                })}
            </Carousel>
            <ul>
                {clientSpeak.map((cd, i) => (
                    <li key={cd.id} onClick={() => setActiveIndex(i)} className={activeIndex === i ? classes.active : ''}>
                        {/* <img src={cdn(cd.clientImage)} alt={cd.name} /> */}
                        <Image src={cd.clientImage} alt={cd.name} loader={myLoader} layout={'fixed'} height={60} width={60} />
                        <span>{cd.name}</span>
                    </li>
                ))}
            </ul>
            <ClientSpeakModal link={activeLink} show={modalShow} hide={() => setModalShow(false)} />

            <style>
                {`  .carousel{
                        overflow:hidden;
                        // max-height:88vh;
                    }
                    .carousel-caption{
                        top: 50%;
                        left:50%;
                        width:100%;
                        transform: translate(-50%,-50%);
                    }
                   
                    .carousel-item{
                        height:80vh;

                    }

                    @media only screen and (max-width: 767px) { 
                        .carousel-item{
                            height:60vh;
    
                        }
                    }

                    `}
            </style>

        </div>
    )
}

export default ClientSpeakCarousel
