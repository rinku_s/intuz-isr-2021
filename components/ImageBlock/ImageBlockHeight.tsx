import React from 'react'
import Imgix, { Picture, Source } from 'react-imgix'
import 'lazysizes';


const ImageBlockHeight = ( { src, alt, className, dheight, mheight, format, title } ) => {
    return (
        <Picture>
            <Source
            src={src}
            height={dheight}
            htmlAttributes={{ media: "(min-width: 768px)" }}
            attributeConfig={{
                src: "data-src",
                srcSet: "data-srcset",
                sizes: "data-sizes"
              }}
              imgixParams={{ auto:"format", fm : format }}
            />
            <Source
            src={src}
            height={mheight}
            htmlAttributes={{ media: "(min-width: 200px)" }}
            attributeConfig={{
                src: "data-src",
                srcSet: "data-srcset",
                sizes: "data-sizes"
              }}
              imgixParams={{ auto:"format", fm: format }}
            />
            <Imgix 
            attributeConfig={{
                src: "data-src",
                srcSet: "data-srcset",
                sizes: "data-sizes"
              }}
            className={`${className} lazyload`} src={src} imgixParams={{ w: 100, auto:"format", fm : format }} htmlAttributes={{ alt:alt, title:title }} />
        </Picture>
    )
}

export default ImageBlockHeight
