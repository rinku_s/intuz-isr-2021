//@ts-nocheck
import React from 'react';
import Container from '../Container/Container';
import Address from './Address';
import Form from './Form';
import classes from './styles.module.scss';

const FormContainer = (props) => {
    return (
        <Container className={classes.FormContainer}>
            <Address/>
            <Form 
            />
        </Container>
    )
}

export default FormContainer
