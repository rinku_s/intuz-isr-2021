//@ts-nocheck
import React from "react";
import classes from "./styles.module.scss";
const Address = (props) => {
  return (
    <div className={classes.Address}>
      <address>
        <img
          src={"/static/Images/flag_us.png"}
          alt="USA Flag"
          className="block"
        />
        <p>
          18 Bartol Street Suite #130, San Francisco <br />
          <span>CA 94133, United States</span>
        </p>
        <p>
          1172 Murphy Ave,San Jose
          <br />
          <span>CA 95131,United States</span>
        </p>
      </address>
      <address>
        <img
          src={"/static/Images/flag_ind.png"}
          alt="USA Flag"
          className="block"
        />
        <p>
          11006, Pinnacle, Corporate Road, Ahmedabad <br />
          <span>GJ 380051, India</span>
        </p>
      </address>
      <div className={classes.contact}>
        <a href="tel:+16504511499">+1 650.451.1499</a>
        <a href="mailto:getstarted@intuz.com">getstarted@intuz.com</a>
      </div>
      <ul>
        <li
          className={classes.twitter}
          dangerouslySetInnerHTML={{
            __html: "/static/Images/addressicons/twitter.svg?include",
          }}
        ></li>
        <li
          className={classes.blogger}
          dangerouslySetInnerHTML={{
            __html: "/static/Images/addressicons/el-blogger.svg?include",
          }}
        ></li>
        <li
          className={classes.linkedin}
          dangerouslySetInnerHTML={{
            __html: "/static/Images/addressicons/linkedin.svg?include",
          }}
        ></li>
        <li
          className={classes.medium}
          dangerouslySetInnerHTML={{
            __html: "/static/Images/addressicons/medium.svg?include",
          }}
        ></li>
        <li
          className={classes.github}
          dangerouslySetInnerHTML={{
            __html: "/static/Images/addressicons/github.svg?include",
          }}
        ></li>
      </ul>
    </div>
  );
};

export default Address;
