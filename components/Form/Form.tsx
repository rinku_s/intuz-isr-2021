//@ts-nocheck
import React, { Component } from 'react';
import FormFields from '../CommonFormFields/FormFields';
import FormButton from '../UI/FormButton/FormButton';
import classes from './styles.module.scss';

export default class Form extends Component {

    state={
        
    }

    constructor(props){
        super(props)
    }


    render() {
        return (
            <div className={`${classes.Form}`}>
                <h3>Reach Out To Us</h3>
                <form className="row">
                    <FormFields />  
                    <FormButton 
                            submitBtn='Send'
                            btnText='Clear'
                    />
                </form>
            </div>
        )
    }
}
