//@ts-nocheck
import { myLoader } from 'config/image-loader'
import Image from 'next/image'
import React from 'react'
import Paragraph from '../../containers/AIPage/Headings/Paragraph'
import Secondary from '../../containers/AIPage/Headings/Secondary'
import classes from './styles.module.scss'
const Offer = (props) => {
    return (
        <div className={`${classes.offer} ${props.className}`}>
            <div className={classes.image}>
                <Image src={props.image.name} alt={props.title} loader={myLoader} layout={'fixed'} height={90} width={115} />
            </div>
            <Secondary>{props.title}</Secondary>
            <Paragraph>{props.description}</Paragraph>
            <style jsx>
                {`
                    .${classes.image}{
                        background-color:${props.imagecolor};
                    }
                `}
            </style>
        </div>
    )
}

export default Offer
