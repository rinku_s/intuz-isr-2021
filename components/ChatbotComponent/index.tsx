//@ts-nocheck
import gsap from "gsap";
import React, { useEffect } from "react";
import Offer from "./Offer";
import classes from "./styles.module.scss";

const data = [
  {
    id: 1,
    image: "/static/Images/icons/gplay.svg",
    title: "Automation",
    description:
      "Empower the productivity of your sales and marketing workforce by automating the entire workflow.",
    imagecolor: "#cefafa",
  },
  {
    id: 2,
    image: "/static/Images/icons/gplay.svg",
    title: "Automation",
    description:
      "Empower the productivity of your sales and marketing workforce by automating the entire workflow.",
    imagecolor: "#cefafa",
  },
  {
    id: 3,
    image: "/static/Images/icons/gplay.svg",
    title: "Automation",
    description:
      "Empower the productivity of your sales and marketing workforce by automating the entire workflow.",
    imagecolor: "#cefafa",
  },
  {
    id: 4,
    image: "/static/Images/icons/gplay.svg",
    title: "Automation",
    description:
      "Empower the productivity of your sales and marketing workforce by automating the entire workflow.",
    imagecolor: "#cefafa",
  },
];

const ChatbotComponent = (props) => {
  useEffect(() => {
    gsap.set(
      [
        `.${classes.offer} .${classes.image}`,
        `.${classes.offer} h4`,
        `.${classes.offer} p`,
      ],
      { opacity: 1 }
    );
  }, []);
  function play() {
    const tl = gsap.timeline({ paused: true });
    tl.fromTo(
      [
        `.${classes.offer} .${classes.image}`,
        `.${classes.offer} h4`,
        `.${classes.offer} p`,
      ],
      0.6,
      { y: -20, autoAlpha: 0 },
      { y: 0, autoAlpha: 1, stagger: 0.1, ease: "Power2.easeOut" }
    );
    tl.play();
  }

  return (
    <div className="row justify-center mt-md">
      {props.offerings.map((d) => (
        <Offer key={d.id} className="col-md-6" {...d} />
      ))}
    </div>
    // </WaypointOnce >
  );
};

export default ChatbotComponent;
