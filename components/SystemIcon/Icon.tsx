//@ts-nocheck


import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';

const Icon = (props) => {
    return (
        <>
            <LazyLoad height={300} offset={300} once>
                <img src={cdn(props.children.icon.name)} alt={props.children.icon.name} />
            </LazyLoad>
            <span>{props.children.title}</span>
        </>
    )
}

export default Icon