//@ts-nocheck
import React from 'react'
import Project from './Project'
import classes from './styles.module.scss'
import { cdn } from '../../config/cdn';

const MoreProject = props => {
    return (
        <div className="row">
            {
                props.content.map((project) => {
                    return (
                        <div key={project.link} className={`col-md-4 col-sm-6 ${classes.ProjectBlock}`}>
                            <Project
                                img = {cdn(project.image.name + "?auto=format")} 
                                link = {project.link}
                                title = {project.title}
                                description = {project.description}
                            />
                        </div>      
                    )
                })
            }
                 
        </div>
    )
}

MoreProject.propTypes = {

}
export default MoreProject
