//@ts-nocheck
//@ts-nocheck
import Link from 'next/link';
import React from 'react';
import LazyImage from '../LazyImage/LazyImage';
import classes from './styles.module.scss';

const Project = props => {
    return (
        <div className={classes.Project}>

            <div className={classes.image}>
                <Link href="/case-studies/[id]" as={`/case-studies/${props.link}`} passHref>
                    <a><LazyImage src={props.img} alt="Project Image" /></a>
                </Link>
            </div>
            <Link href="/case-studies/[id]" as={`/case-studies/${props.link}`}>
                <h4>{props.title}</h4>
            </Link>
            <p>{props.description}</p>
        </div>
    )
}


export default Project


