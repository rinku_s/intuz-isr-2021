//@ts-nocheck


import Container from '../../components/Container/Container';
import React from 'react';
import { cdn } from '../../config/cdn';
import windowWidth from '../../hooks/windowWidth';
import style from './styles.module.scss';

const ChartBanner = (props) => {
    if (props.content.length <= 0) {
        return null
    } else {
        let windowSize = windowWidth();
        return (
            <>
                {props.content.map((currentContent, index) => {
                    let backImage = { background: `url(${cdn(currentContent.backImage.name)}?fm=pjpg?auto=format${windowSize < 768 ? '&w=900' : ''}) center center fixed` };
                    return (
                        <section key={index} style={backImage} className={style.CharterContent} id='CharterSection'>
                            <Container>
                                <h4>{currentContent.title}</h4>
                                {currentContent.description ? <p>{currentContent.description}</p> : ''}
                            </Container>
                        </section>
                    )
                })}
            </>
        )
    }
}

export default ChartBanner