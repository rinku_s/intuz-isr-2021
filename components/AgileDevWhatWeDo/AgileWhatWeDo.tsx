//@ts-nocheck
//@ts-nocheck
import React from 'react'
import Box from './Box';
import classes from './styles.module.scss';
import { cdn } from '../../config/cdn';
const AgileWhatWeDo = (props) => {
    return (
        <div className={`row ${classes.rowContainer}`} style={{marginTop:"5rem"}}>
            {props.whatwedoagiles.map(wg=>(
            <div key={wg.title} className="col-12 col-md-6 col-lg-3">
                <Box img={cdn(wg.image.name)} heading={wg.title} desc={wg.description} />
            </div>
            ))}
        </div>
    )
}

export default AgileWhatWeDo
