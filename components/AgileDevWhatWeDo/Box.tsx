//@ts-nocheck
//@ts-nocheck
import React from 'react';
import classes from './styles.module.scss';
import LazyLoad from 'react-lazyload';
const Box = (props) => {
    return (
        <div className={classes.Box}>
            <LazyLoad once>
                <img src={props.img} alt={props.heading}/>
            </LazyLoad>
            <h3>{props.heading}</h3>
            <p>{props.desc}</p>
        </div>
    )
}

export default Box
