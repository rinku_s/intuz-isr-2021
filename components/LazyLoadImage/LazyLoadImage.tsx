//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap';
import React, { useState } from 'react';
import { SwitchTransition, Transition } from 'react-transition-group';
import VisibilitySensor from 'react-visibility-sensor';
const LazyLoadImage = (props) => {
    const [visible, setVisible] = useState(false);
    let offset = props.offset ? props.offset : { top: -10, bottom: -10 };
    let duration = props.duration ? props.duration : 1;
    let animation = props.animation ? props.animation : { opacity: 0, ease: "Power0.easeIn" };
    return (
        <VisibilitySensor onChange={v => v ? setVisible(true) : ''} partialVisibility={true} offset={offset}>

            <SwitchTransition>
                <Transition key={visible}
                    timeout={500}
                    addEndListener={(node, done) => {
                        if (!visible) {
                            gsap.to(node, duration, {
                                ...animation,
                                onComplete: done
                            });

                        } else {
                            gsap.from(node, duration, {
                                ...animation,
                                onComplete: done
                            });
                        }
                    }}
                >

                    {!visible ? props.placeholder : props.children}


                </Transition>
            </SwitchTransition>

        </VisibilitySensor>
    )
}

export default LazyLoadImage
