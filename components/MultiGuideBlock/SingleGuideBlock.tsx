//@ts-nocheck


import gsap from 'gsap';
import Link from 'next/link';
import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';

const SingleGuideBlock = (props) => {
    // setOpacity('.guideBlock')
    let guideBlock = React.useRef();

    function play() {
        const tl = gsap.timeline();
        tl.fromTo(guideBlock.current, 0.8, { y: -20, autoAlphaL: 0 }, { y: 0, autoAlpha: 1, ease: 'Power1.easeOut' })
    }

    return (

        <div className='col-md-4 col-sm-6 guideBlock' ref={guideBlock}>
            <LazyLoad height={300} offset={300}>
                <Link href='/[guides]' as={props.link} prefetch={false}>
                    <img src={cdn(`${props.resourceBlockImage.name}?auto=format&fm=pjpg`)} alt={props.resourceBlockImage.name} />
                </Link>
            </LazyLoad>
            <Link href='/[guides]' as={props.link} prefetch={false}>
                <h3 style={{ textAlign: props.center ? "center" : 'left' }}>{props.title}</h3>
            </Link>
        </div>
        // </WaypointOnce >
    )
}

export default SingleGuideBlock