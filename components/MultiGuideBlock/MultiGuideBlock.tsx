//@ts-nocheck

import gsap from 'gsap';
import React from 'react';
import Container from '../Container/Container';
import Heading from '../Heading/Heading';
import SecondaryHeading from '../Heading/SecondaryHeading';
import SingleGuideBlock from './SingleGuideBlock';
import style from './styles.module.scss';

const MultiGuideBlock = (props) => {
    // setOpacity(`.Heading, .SubHeading`);

    function play() {
        const tl = gsap.timeline({ paused: true });
        tl.fromTo('.Heading', 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" })
            .fromTo('.SubHeading', 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.5);
        tl.play();
    }

    return (

        <section className={style.MultiGuideBlock}>
            <Container>
                <Heading className='Heading' style={{ 'fontFamily': "'Nunito Sans', sans-serif" }}>Intuz Guides</Heading>
                <SecondaryHeading className="SubHeading" style={{ 'fontFamily': "'Nunito Sans', sans-serif" }}>Using Innovation & Technology For Profitability</SecondaryHeading>
                <div className={`${style.GuideBlock} row`}>
                    {
                        props.guides.length > 0 && props.guides.map((guide, index) => <SingleGuideBlock {...guide} key={index} center />)
                    }
                </div>
            </Container>
        </section>
        // </WaypointOnce >
    )
}

export default MultiGuideBlock

