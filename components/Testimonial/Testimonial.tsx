//@ts-nocheck
//@ts-nocheck
import { myLoader } from 'config/image-loader';
import Image from 'next/image';
import React from 'react';
import { Tween } from 'react-gsap';
// import VisibilityWrapper from '../VisibilityWrapper/VisibilityWrapper';
import AnimationWrapper from '../AnimationWrapper/AnimationWrapper';
import Container from '../Container/Container';
import classes from './styles.module.scss';
const Testimonial = (props) => {
    return (
        <AnimationWrapper>
            {visible =>
                <Tween duration={2} playState={visible ? "play" : "pause"} to={{ opacity: 1, ease: "Power0.easeOut" }}>
                    <section className={`${classes.Testimonial}`} id={props.sectionId}>
                        <Container>
                            {props.image ? <div className={classes.userImg}>
                                <img src={props.image} alt={props.name} />
                            </div> : ''}
                            {/* <div className={classes.userImg}>
                    <img src={props.image} alt={props.name}/>
                </div> */}
                            <h3>{props.name}</h3>
                            <small className={classes.smallfont} dangerouslySetInnerHTML={{ __html: props.designation }}></small>
                            {/* <img className={classes.quoteImg} src="https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/whyus-inner/quote_img.png" alt="Quote" /> */}
                            <Image src={"/1-backup-s3-intuz-site/whyus-inner/quote_img.png"} loader={myLoader} alt="Quote" layout={'intrinsic'} height={64} width={400} />
                            <p dangerouslySetInnerHTML={{ __html: props.testimonial }}></p>
                        </Container>
                    </section>
                </Tween>
            }
        </AnimationWrapper>
    )
}

export default Testimonial
