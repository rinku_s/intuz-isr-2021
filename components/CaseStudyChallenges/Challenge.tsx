//@ts-nocheck
import React, { useState } from 'react'
import PropTypes from 'prop-types'
import CaseStudyParagraph from '../CaseStudyParagraph/CaseStudyParagraph'
import SVG from 'react-inlinesvg';
import classes from './styles.module.scss'
const Challenge = props => {
    
    return (
        <div className={`${classes.Challenge} ${props.className}`}>
            <SVG src={props.icon.url} />
            <h3>{props.title}</h3>
            <CaseStudyParagraph variation="small">{props.description}</CaseStudyParagraph>
        </div>
    )
}

Challenge.propTypes = {

}

export default Challenge
