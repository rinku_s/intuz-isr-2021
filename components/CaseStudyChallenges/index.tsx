//@ts-nocheck
import React from 'react'
import PropTypes from 'prop-types'
import Challenge from './Challenge'

const CaseStudyChallenges = props => {
    return (
        <div className="row justify-content-between mt-mx">
            
            {props.content.map(challenge=>(
            <Challenge key={challenge.title} className={`${props.dataLength == 2 ? 'col-md-6 col-sm-6' : 'col-md-4 col-sm-6'}`} {...challenge}  />
            ))}
            {/* <Challenge className="col-sm-4" />
            <Challenge className="col-sm-4" /> */}
        </div>
    )
}

CaseStudyChallenges.propTypes = {

}

export default CaseStudyChallenges
