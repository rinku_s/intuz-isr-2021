//@ts-nocheck
import React from 'react';
import SVG from 'react-inlinesvg';
import LazyLoad from 'react-lazyload';
import CaseStudyParagraph from '../CaseStudyParagraph/CaseStudyParagraph';
import classes from './styles.module.scss';
const Block = props => {
    return (
        <div className={`${classes.HighlightBlock} ${props.className}`}>
            <LazyLoad height={300} offset={300}>
                <SVG src={props.image.url} />
            </LazyLoad>
            <h3>{props.title}</h3>
            <CaseStudyParagraph variation="small">{props.subtitle}</CaseStudyParagraph>
        </div>
    )
}

Block.propTypes = {

}

export default Block
