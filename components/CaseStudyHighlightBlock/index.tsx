//@ts-nocheck
import React from 'react';
import Block from './Block';

const CaseStudyHighlightBlock = props => {
    if(props.content.length <= 0) {
        return null
    } else {
        return (
            <div className="row justify-content-between mt-mx">
                {props.content.map(block=>(
                    <Block key={block.title} className="col-sm-4" {...block}  />
                ))}
            </div>
        )
    }
}

CaseStudyHighlightBlock.propTypes = {

}

export default CaseStudyHighlightBlock
