//@ts-nocheck
import React from 'react';
import classes from './styles.module.scss';
const FiterTabs = (props) => {
    return (
        <ul className={classes.FilterTabs}>
            <li className={props.active === 'fcp' ? classes.active : ''} onClick={()=>props.setFilter('fcp')}>Fixed Cost Packages</li>
            <li className={props.active === 'ddt' ? classes.active : ''} onClick={()=>props.setFilter('ddt')}>Dedicated Developers</li>
        </ul>
    )
}

export default FiterTabs
