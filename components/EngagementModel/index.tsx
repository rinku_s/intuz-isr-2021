//@ts-nocheck
//@ts-nocheck
import gsap, { Power1 } from "gsap";
import React, { Component } from "react";
import LinkButton from "../../components/UI/LinkButton/LinkButton";
import FiterTabs from "./FiterTabs";
import ModelBox from "./ModelBox/ModelBox";
import classes from "./styles.module.scss";
export default class EngagementModel extends Component {
  state = {
    active: "fcp",

    data: {
      fcp: [
        {
          index: 1,
          title: "Project Analysis",
          svg: "/static/Images/engagement-model/ic-mag.svg",
          bColor: "#F7DEDE",
        },
        {
          index: 2,
          title: "Ball park cost & time estimate",
          svg: "/static/Images/engagement-model/ic-calender.svg",
          bColor: "#D4E6C3",
        },
        {
          index: 3,
          title: "Detailed scope & agreement",
          svg: "/static/Images/engagement-model/ic-note.svg",
          bColor: "#C2BED8",
        },
        {
          index: 4,
          title: "Kick start development",
          svg: "/static/Images/engagement-model/ic-embed.svg",
          bColor: "#C0D3E8",
        },
        {
          index: 5,
          title: "Milestone deliveries",
          svg: "/static/Images/engagement-model/ic-cof.svg",
          bColor: "#BBE1D9",
        },
        {
          index: 6,
          title: "Completion",
          svg: "/static/Images/engagement-model/ic-right.svg",
          bColor: "#EAE7BD",
        },
        {
          index: 7,
          title: "Sign off",
          svg: "/static/Images/engagement-model/ic-pen.svg",
          bColor: "#E8D8CA",
        },
      ],

      ddt: [
        {
          index: 1,
          title: "Requirement & Resource mapping",
          svg: "/static/Images/engagement-model/ic-mag.svg",
          bColor: "#F7DEDE",
        },
        {
          index: 2,
          title: "Monthly cost & engagement duration",
          svg: "/static/Images/engagement-model/ic-calender.svg",
          bColor: "#D4E6C3",
        },
        {
          index: 3,
          title: "Resource Interview by Client",
          svg: "/static/Images/engagement-model/ic-eye.svg",
          bColor: "#C2BED8",
        },
        {
          index: 4,
          title: "Hiring confirmation",
          svg: "/static/Images/engagement-model/ic-usr.svg",
          bColor: "#C0D3E8",
        },
        {
          index: 5,
          title: "Assignment of projects and tasks",
          svg: "/static/Images/engagement-model/ic-note.svg",
          bColor: "#BBE1D9",
        },
        {
          index: 6,
          title: "Development on your server",
          svg: "/static/Images/engagement-model/ic-embed.svg",
          bColor: "#EAE7BD",
        },
        {
          index: 7,
          title: "Daily task updates",
          svg: "/static/Images/engagement-model/ic-fetch.svg",
          bColor: "#E8D8CA",
        },
        {
          index: 8,
          title: "Daily resource reporting",
          svg: "/static/Images/engagement-model/ic-note2.svg",
          bColor: "#84c3d3",
        },
      ],
    },
    buttondata: {
      fcp: [
        {
          index: 1,
          label: "Get Quote",
          link: "/get-started",
        },
      ],
      ddt: [
        {
          index: 2,
          label: "Hire Now",
          link: "/get-started",
        },
      ],
    },
  };

  setFilter = (id) => {
    this.setState({
      active: id,
    });
  };

  componentDidUpdate = (prevProps, prevState) => {
    if (prevState.active !== this.state.active) {
      gsap.fromTo(
        this.con,
        0.5,
        { opacity: 0.1, ease: Power1.easeIn },
        { opacity: 1, ease: Power1.easeIn }
      );
    }
  };

  render() {
    let { data, active, buttondata } = this.state;
    return (
      <React.Fragment>
        <FiterTabs setFilter={this.setFilter} active={active} />
        <div className={classes.container} ref={(c) => (this.con = c)}>
          {data[active].map((el) => (
            <ModelBox
              key={el.index}
              svg={el.svg}
              title={el.title}
              index={el.index}
              bColor={el.bColor}
            />
          ))}
        </div>
        {this.props.button
          ? buttondata[active].map((el) => {
              return (
                <LinkButton
                  variation="blackBorder"
                  href={el.link}
                  key={el.index}
                >
                  {el.label}
                </LinkButton>
              );
            })
          : ""}
      </React.Fragment>
    );
  }
}
