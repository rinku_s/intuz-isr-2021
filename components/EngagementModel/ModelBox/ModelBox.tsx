//@ts-nocheck
import React from 'react';
import classes from './styles.module.scss';
import SVG from 'react-inlinesvg'
const ModelBox = (props) => {
    return (
        <div className={`${classes.box} box${props.index}`}>
            <i>
                <em>{props.index}</em>
                <SVG src={props.svg} />
            </i>
            <p>{props.title}</p>
            <style>{`
                .box${props.index}::after{
                    border-color:${props.bColor}
                }
            `}</style>
        </div> 
    )
}

export default ModelBox
