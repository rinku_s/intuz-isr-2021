//@ts-nocheck
import React from 'react';

const Expectation = (props) => {
    return (
        <div className='col-md-6'>
            <h4>{props.content.title}</h4>
            {props.content.description ? <p>{props.content.description}</p> : ''}
        </div>
    )
}

export default Expectation