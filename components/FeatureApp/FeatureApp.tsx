import { myLoader } from "config/image-loader";
import gsap from "gsap";
import Image from "next/image";
import React, { Component } from "react";
import Container from "../Container/Container";
import LinkButton from "../UI/LinkButton/LinkButton";
import styles from "./featureapp.module.scss";

interface FeatureAppProps {
  className?: string;
}

export default class FeatureApp extends Component<FeatureAppProps, {}> {
  render() {
    return (
      <Container
        className={`${styles.FeatureAppBlock} ${this.props.className}`}
      >
        <div className="my-20">
          <Image
            loader={myLoader}
            src={"featured-app-img1.png"}
            layout="intrinsic"
            width="1080"
            height="673"
          />
        </div>
        <LinkButton variation="blackBorder" href="/work">
          View Case Studies
        </LinkButton>
      </Container>
    );
  }
}
