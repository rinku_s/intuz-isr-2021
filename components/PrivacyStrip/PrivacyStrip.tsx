import React from "react";
import classes from "./styles.module.scss";
import Image from "next/image";
import { myLoader } from "config/image-loader";

const PrivacyStrip = (props) => {
  return (
    <div className={classes.Cookie}>
      <p>
        We use cookies to provide and improve our services. By using our site,
        you consent to cookies.
        <a href="/privacy-policy">Know More</a>
        <button onClick={props.onAccept}>
          <Image
            width="25px"
            height="25px"
            layout="fixed"
            loader={myLoader}
            src="close-popup.png"
            alt="Close Popup"
          />
        </button>
      </p>
    </div>
  );
};

export default PrivacyStrip;
