//@ts-nocheck


import React from 'react';
import Container from '../Container/Container';
import SecondaryHeading from '../Heading/SecondaryHeading';
import ContactDetail from './ContactDetail';
import Form from './Form';
import styles from './styles.module.scss';

const FormContainer = (props) => {
    return (
        <div>
            <Container className={styles.FormContainer}>
                <SecondaryHeading fontSize='3.2rem' color='rgba(79, 100, 121, .3)'>Let’s get in touch to discuss anything & everything tech.</SecondaryHeading>
                <Form />
                <ContactDetail />
            </Container>
        </div>
    )
}

export default FormContainer

