//@ts-nocheck
//@ts-nocheck
import React from 'react';
import { SubmitForm, useForm, validator } from '../../hooks/useForm';
import FormFields from '../CommonFormFields/FormFields';
import FormButton from '../UI/FormButton/FormButton';
import Processing from '../UI/Processing/Processing';

const Form = (props) => {
    const [form, setForm, processing, setProcessing] = useForm();

    async function StartFormSubmission(data) {
        setProcessing(true);
        await SubmitForm(data, setProcessing, 'thankyou-contact-form');

    }

    function submit(evt) {
        evt.preventDefault();
        if (form.formIsValid) {
            var data = new FormData();
            data.append("f_name", form.fields.f_name.value);
            data.append("l_name", form.fields.l_name.value);
            data.append("email", form.fields.email.value);
            data.append("phone", form.fields.phone.value);
            data.append("description", form.fields.project_brief.value);
            data.append("leadsource", "Contact Us Page");
            data.append("subject", "Contact Us");
            data.append("url", window.location.href);
            StartFormSubmission(data);
        } else {
            alert("Please enter valid data");
        }

    }
    if (processing) return <Processing />

    return (
        <div>
            <form className="row" onSubmit={submit} autoComplete="off">
                <FormFields colWidth='col-md-6' form={form} inputChangedHandler={(evt, id) => validator(evt, id, form, setForm)} />
                <FormButton
                    isValid={form.formIsValid}
                    submitBtn='Contact Now!'
                    variation='contactform'
                />
            </form>
        </div>
    )
}

export default Form;