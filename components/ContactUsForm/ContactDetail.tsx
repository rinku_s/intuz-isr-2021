//@ts-nocheck

import React from "react";
import styles from "./styles.module.scss";

const ContactDetail = () => {
  return (
    <div className={styles.Detail}>
      <address>
        <h2>Intuz, Inc.</h2>
        <div className="flex flex-col sm:flex-rows">
          <p>
            18 Bartol Street Suite #130, San Francisco <br />
            CA 94133, United States
          </p>
          <p>
            2430 Camino Ramon, San Ramon <br />
            CA 94583, United States
          </p>
          <p className={styles.Contact}>
            Call Us <a href="tel:+16504511499">+1 650.451.1499</a>
          </p>
        </div>
      </address>
      <address>
        <h2>Intuz Solutions Pvt. Ltd.</h2>
        <div className="flex flex-col sm:flex-rows">
          <p>
            1006, Pinnacle, Corporate Road, <br /> Ahmedabad <br /> GJ 380051,
            India
          </p>
          <p className={styles.Contact}>
            Call Us <a href="tel:+917948902497">+91 794.890.2497</a>
          </p>
        </div>
      </address>
    </div>
  );
};

export default ContactDetail;
