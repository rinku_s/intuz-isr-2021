//@ts-nocheck

import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import { callAPI } from 'lib/api';
import React, { useEffect } from 'react';
import { useState } from 'react';
import LoadingSpinner from '../LoadingScreen/LoadingSpinner';
import BuildTabContent from './BuildTabContent';
import style from './styles.module.scss';
export const allQuery = gql`
query ($id:ID!){
    sectiontabtitle(id:$id) {
        sectiontabcontents {
          id
          title
          description
          icon {
              name
          }
      }
    }
}
`;

const TabContent = (props) => {


    const [data, setData] = useState<any>()
    

    useEffect(() => {
        (async()=>{
            const d = await callAPI(allQuery, { id: props.activeTab });
            setData(d);
        })()    
    }, [props.activeTab])
    
    // if(loading) return <LoadingSpinner/>
    
     return(
        <>
            { data?.sectiontabtitle?.sectiontabcontents.length > 0 ? 
                <div className={`${style.tabContent} row contentEffect`}>
                    {data?.sectiontabtitle?.sectiontabcontents.map((content, index) => {
                        return(
                            <BuildTabContent 
                                icon={content.icon.name}
                                title={content.title}
                                description={content.description}
                                key={index}
                            />
                        ) 
                    })}
                </div> : '' }
        </>
    )
}
export default TabContent