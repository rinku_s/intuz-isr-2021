//@ts-nocheck
//@ts-nocheck
import React, { Component } from "react";
import { Media, MediaContextProvider } from "../../config/responsiveQuery";
import DesktopTabView from './DesktopTabView';
import MobileTabView from './MobileTabView';
import TabContent from './TabContent';

export default class SolutionTab extends Component {
    constructor(props) {
        super(props)

    }
    state = {
        activeTab: null
    }

    setTab = (id) => {
        if (id == null) {
            this.setState({ activeTab: null })
        } else {
            this.setState({ activeTab: id })
        }
    }

    setMobileTab = (id) => {
        if (id == null || this.state.activeTab == id) {
            this.setState({ activeTab: null })
        } else {
            this.setState({ activeTab: id })
        }
    }

    componentDidMount() {
        let activetab = this.props.tabTitles[0].id;
        if (activetab != null) {
            this.setState({
                activeTab: activetab
            });
        }
    }

    render() {
        let tabContent = <TabContent activeTab={this.state.activeTab} />
        return (
            <MediaContextProvider>
                <Media greaterThanOrEqual="sm">
                    <DesktopTabView
                        setTab={this.setTab}
                        activeTab={this.state.activeTab}
                        tabTitles={this.props.tabTitles}
                    />
                    {this.state.activeTab ? tabContent : ''}
                </Media>
                <Media lessThan="sm">
                    <MobileTabView
                        setTab={this.setMobileTab}
                        activeTab={this.state.activeTab}
                        tabTitles={this.props.tabTitles}
                    >
                        {this.state.activeTab ? tabContent : ''}
                    </MobileTabView>
                </Media>
            </MediaContextProvider>
        )
    }
}
