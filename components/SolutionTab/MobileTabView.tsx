//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap';
import React from "react";
import { Transition } from 'react-transition-group';
import { cdn } from '../../config/cdn';
import style from './styles.module.scss';


const MobileTabView = (props) => {
    return (
        <div className={style.TabHeading}>
            {props.tabTitles.map((tab, index) => {
                return (
                    <React.Fragment key={index}>
                        <div onClick={() => props.setTab(tab.id)}
                            className={`${style.Heading} ${props.activeTab === tab.id ? style.activeTag : ''}`} key={index}>
                            <div className={style.icon}>
                                {props.activeTab === tab.id ? <img src={cdn(tab.activeTabIcon.name)} alt={tab.activeTabIcon.name} /> : <img src={cdn(tab.icon.name)} alt={tab.icon.name} />}
                            </div>
                            {tab.title}
                        </div>
                        <Transition in={props.activeTab == tab.id}
                            timeout={1000}
                            mountOnEnter={true}
                            unmountOnExit={true}
                            onEntering={(n) => {
                                gsap.fromTo(n, 1, { scaleY: 0.5 }, { scaleY: 1 });
                            }}
                            onExiting={(n) => {
                                gsap.fromTo(n, 1, { scaleY: 1 }, { scaleY: 0.5 });
                            }}
                        >
                            {state => props.children}
                        </Transition>
                        {/* {props.activeTab == tab.id ? props.children : ''} */}
                    </React.Fragment>
                )
            })}
        </div>
    )
}
export default MobileTabView


