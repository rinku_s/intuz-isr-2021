//@ts-nocheck

import React from "react";
import { cdn } from '../../config/cdn';
import style from './styles.module.scss';

const DesktopTabView = (props) => {
    return(
        <div className={style.TabHeading}>
            {props.tabTitles.map((tab,index) => {
                return(
                    <div onClick={()=>props.setTab(tab.id)} 
                         className={`${style.Heading} ${props.activeTab === tab.id ? style.activeTag : ''}`} key={index}>
                        <div className={style.icon}>
                            {props.activeTab === tab.id ? 
                                <img src={cdn(tab.activeTabIcon.name)} alt={tab.activeTabIcon.name} /> : <img src={cdn(tab.icon.name)} alt={tab.icon.name} /> 
                            }
                        </div>
                        {tab.title}
                    </div>        
                )
            })}
        </div>
    )
}
export default DesktopTabView 


