//@ts-nocheck


import gsap from "gsap";
import React, { useEffect } from "react";
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';
import style from './styles.module.scss';

const BuildTabContent = (props) => {    
    useEffect(() => {
        gsap.fromTo('.tab', 1, {opacity:0}, {opacity:1});
    }, [props.title])
    return(
        <div className={`col-md-6 ${style.ContentCol} tab`}>
            <div className={style.icon}>
                <LazyLoad height={300} offset={300}>
                    <img src={cdn(props.icon)} alt={props.icon} />
                </LazyLoad>
            </div>
            <div className={style.content}>
                <h4 style={{textTransform: props.page == 'iot' ? 'none' : 'uppercase'}}>{props.title}</h4>
                { props.page == 'iot' ? <div dangerouslySetInnerHTML={{__html:props.description}} className={style.listContent}></div> : <p dangerouslySetInnerHTML={{__html:props.description}}></p> }     
            </div>
        </div>
    )
}
export default BuildTabContent 


