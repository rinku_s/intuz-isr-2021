import React from 'react';

const SocialLink = (props) => {
    return (
        <ul className={props.className}>
                <li><a href=""><img src={"/static/Images/icons/twitter.png"} alt="Facebook Icon"/></a></li>
                <li><a href=""><img src={"/static/Images/icons/facebook.png"} alt=""/></a></li>
                <li><a href=""><img src={"/static/Images/icons/linkedin.png"} alt=""/></a></li>
                <li><a href=""><img src={"/static/Images/icons/medium.png"} alt=""/></a></li>
                <li><a href=""><img src={"/static/Images/icons/linker.png"} alt=""/></a></li>
            </ul>
    )
}

export default SocialLink
