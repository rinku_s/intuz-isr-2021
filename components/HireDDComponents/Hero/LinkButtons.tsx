import React from 'react';
import gsap from 'gsap';
import ScrollToPlugin from 'gsap/dist/ScrollToPlugin';
import classes from './Blocks/styles.module.scss';


const LinkButtons = () => {
    gsap.registerPlugin(ScrollToPlugin);

    function scrollToSection(id){
        gsap.to(window, 0.1, {scrollTo:{y:`#${id}`, autoKill:false}})
    }

    return (
        <div className={classes.Links}>
            <a onClick={() => scrollToSection('HiringProcess')}>Hiring Process</a>
            <a onClick={() => scrollToSection('form')}>Discuss Your Requirements</a>
        </div>
    )
}

export default LinkButtons

