import React from 'react';
import classes from './styles.module.scss';
const Block = (props) => {

    return (
        <div className={classes.Block}>
            <img src={props.img} alt={props.caption} />
            <h3>{props.fig}+</h3>
            <p>{props.caption}</p>
        </div>
    )
}

export default Block
