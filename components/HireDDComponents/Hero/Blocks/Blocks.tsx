import React from 'react';
import Block from './Block';

const Blocks = (props) => {

    const stats = [
        {
            fig:12,
            caption:"Frontend Developers",
            img:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/hire-developers/frontendDevelopers.png"
        },
        {
            fig:30,
            caption:"Backend Developers",
            img:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/hire-developers/backendDevelopers.png"
        },
        {
            fig:35,
            caption:"Mobile Developers",
            img:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/hire-developers/mobileDevelopers.png"
        },
        {
            fig:8,
            caption:"Creative Designers",
            img:"https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/hire-developers/creativeDesigners.png"
        },
    ]

    return (
        <div className="row" style={{marginTop:"-10rem"}}>
            {stats.map(st=>(
                <div key={st.fig} className="col-md-3">
                    <Block {...st} />        
                </div>
            ))}
        </div>
    )
}

export default Blocks
