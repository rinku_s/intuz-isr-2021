import { myLoader } from 'config/image-loader';
import Image from 'next/image';
import React from 'react';
import classes from './styles.module.scss';
const HiringProcessDes = (props) => {
    return (
        <div className={classes.HiringProcessDesk}>
            <Image loader={myLoader} src={'hiredevprocess_img.png'} alt={props.imgName} width={1139} height={255} layout={'intrinsic'} />

            <div className={classes.content}>
                <p>
                    We Learn
                <br />
                    <span>
                        Your requirements.
                    </span>
                </p>
                <p>
                    We Offer
                <br />
                    <span>
                        Awesome candidates.
                    </span>
                </p>
                <p>
                    You Interview
                <br />
                    <span>
                        Rigorously and meticulously. Set and get high bar.
                    </span>
                </p>
                <p>
                    You Grow
                <br />
                    <span>
                        Enjoy Flexibility and power of extended team.
                    </span>
                </p>
            </div>
        </div>
    )
}

export default HiringProcessDes
