import React from 'react';
import Block from './Block';
import classes from './styles.module.scss';

const HiringProcessMob = () => {
    return (
        <div className={classes.HiringProcessDes}>
            <Block 
                number = '1' 
                imgName = 'hire_weLearn.png'
                title = 'We Learn'
                subtitle = 'Your requirements'
            />
            <Block 
                number = '2'
                imgName = 'hire_weShare.png'
                title = 'We Offer'
                subtitle = 'Awesome candidates'
            />
            <Block 
                number = '3'
                imgName = 'hire_youinterview.png'
                title = 'You Interview'
                subtitle = 'Rigorously and meticulously. Set and get high bar'
            />
            <Block 
                number = '4'
                imgName = 'hire_youGrow.png'
                title = 'You Grow'
                subtitle = 'Enjoy Flexibility and power of extended team'
            />
        </div>
    )
}

export default HiringProcessMob
