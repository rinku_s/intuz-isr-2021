import { myLoader } from 'config/image-loader';
import Image from 'next/image';
import React from 'react';
import classes from './styles.module.scss';

const Block = (props) => {
    return (
        <div className={classes.Block}>
            <div className={classes.imgContent}>
                <h3>{props.number}</h3>
                <div className={classes.Image}>

                    <Image loader={myLoader} src={props.imgName} alt={props.imgName} width={100} height={100} layout={'fixed'} />
                </div>
            </div>
            <p>{props.title} <br />
                <span>
                    {props.subtitle}
                </span>
            </p>
        </div>
    )
}

export default Block
