import gsap, { Power0 } from 'gsap';
import React, { useEffect } from 'react';
import Skill from './Skill';
import classes from './styles.module.scss';

const skills = [
    {
        id: "hmd",
        techs: [
            {
                img: 'hire_apple.svg',
                caption:"iOS",
                link:"/ios-development"
            },
            {
                img: "hire_android.svg",
                caption:"Android",
                link:"/android-development"
            },
            {
                img: "hire_react.svg",
                caption:"React Native",
                link:"/react-native-development"
            },
            {
                img: "hire_ionic.svg",
                caption:"Ionic"
            },
            
        ]
    },
    {
        id: "hwd",
        techs: [
            {
                img: "hire_nodejs.svg",
                caption:"Node JS",
                link:"/full-stack-js-development"
            },
            {
                img: "hire_angular.svg",
                caption:"AnGULAR JS",
                link:"/full-stack-js-development"
            },
            {
                img: "hire_vue.svg",
                caption:"Vue JS",
                link:"/full-stack-js-development"
            },
            {
                img: "hire_aws.svg",
                caption:"AWS",
                link:"/agile-devops-services"
            },
            {
                img: "hire_aws.svg",
                caption:"PHP",
            }
        ]
    },
]

const SkillSet = (props) => {

    useEffect(() => {
        gsap.set(`.${classes.skills}`, {transformOrigin:'100% 0'})
        gsap.fromTo(`.${classes.skills}`, 0.5, { scaleY:0.8, opacity:0, ease:Power0.easeOut },{ scaleY:1, opacity:1, ease:Power0.easeOut });
        return () => {
            gsap.set(`.${classes.skills}`, {transformOrigin:'100% 0', scaleY:0.8, opacity:0})
            
        };
    }, [props.active])

    return (
        <div className={classes.skills}>
            {skills.find(e=>e.id === props.active).techs.map(d=>(           
                <Skill key={d.caption} {...d} />
            ))}
        </div>
    )
}

export default SkillSet
