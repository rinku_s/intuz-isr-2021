import React, { Component } from "react";
import classes from "./styles.module.scss";
import SkillSet from "./SkillSet";

const tabs = [
  {
    id: "hmd",
    title: "Hire Mobile App Developers",
  },
  {
    id: "hwd",
    title: "Hire Web App Developers",
  },
];

export default class SkillsComp extends Component {
  state = {
    active: "hmd",
  };

  setActive = (id) => {
    this.setState({
      active: id,
    });
  };

  render() {
    let { active } = this.state;
    return (
      <>
        <ul className={classes.SkillTab}>
          {tabs.map((t) => (
            <li
              key={t.id}
              className={`${active === t.id ? classes.active : ""}`}
              onClick={() => this.setActive(t.id)}
            >
              {t.title}
            </li>
          ))}
        </ul>
        <SkillSet active={this.state.active} />
      </>
    );
  }
}
