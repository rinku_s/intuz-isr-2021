import { myLoader } from 'config/image-loader';
import Image from 'next/image';
import Link from 'next/link';
import React from 'react';
import classes from './styles.module.scss';

const Skill = (props) => {
    return (
        <>
            {
                props.link ?
                    <Link href={props.link}>
                        <a className={classes.Skill}>
                            <div>

                                <Image loader={myLoader} src={props.img} alt={props.caption} width={70} height={70} layout={'fixed'} />

                                <span>{props.caption}</span>
                            </div>
                        </a>
                    </Link>
                    :
                    <a className={classes.Skill}>
                        <div>
                            <Image loader={myLoader} src={props.img} alt={props.caption} width={70} height={70} layout={'intrinsic'} />

                            <span>{props.caption}</span>
                        </div>
                    </a>
            }

        </>

    )
}

export default Skill

