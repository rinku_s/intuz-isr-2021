//@ts-nocheck
//@ts-nocheck
import React, { useContext, useState } from 'react';
import checkValidity from '../../../config/checkValidity';
import DeveloperContext from '../../../context/developerContext';
import { ProcessingContext } from '../../../context/processingContext';
import { reset, SubmitForm, useForm, validator } from '../../../hooks/useForm';
import FormFields from '../../CommonFormFields/FormFields';
import Input from '../../UI/Input/Input';
import DevFigure from './DevFigure';
import classes from './styles.module.scss';



const Form = (props) => {
    const { processing, setProcessing } = useContext(ProcessingContext);

    const { dev, setDev } = useContext(DeveloperContext);
    const [form, setForm] = useForm();
    const [skill, setSkill] = useState({
        value: '',
        validation: {
            required: true
        },
        valid: false,
        touched: false
    });


    function validateSkill(evt) {
        let update = { ...skill };
        update.value = evt.target.value;
        update.valid = checkValidity(update.value, update.validation);
        update.touched = true;
        setSkill(update);
    }

    async function StartFormSubmission(data) {
        setProcessing(true);
        await SubmitForm(data, setProcessing, 'thankyou-hire-developers');

    }

    function submit(evt) {
        evt.preventDefault();
        if (form.formIsValid && skill.valid) {
            var data = new FormData();
            data.append("f_name", form.fields.f_name.value);
            data.append("l_name", form.fields.l_name.value);
            data.append("email", form.fields.email.value);
            data.append("phone", form.fields.phone.value);
            data.append("description", form.fields.project_brief.value);
            data.append("leadsource", "Hire Dedicated Developers");
            data.append("url", window.location.href);
            data.append("skill", skill.value);
            data.append("subject", "Hire Dedicated Developers Request");
            if (dev != null) {
                data.append("developer_name", `${dev.name} - ${dev.designation}`);
            }
            StartFormSubmission(data);
        } else {
            alert("Please enter valid data");
        }

    }

    function clearEverything() {
        reset(form, setForm);
        skill.value = "";
        skill.valid = false;
        skill.touched = false;
    }

    return (
        <div className={classes.Form}>
            {dev != null ? <DevFigure onClose={() => setDev(null)} {...dev} /> : ''}

            <form className={`row`} autoComplete="off" onSubmit={submit}>
                <FormFields colWidth="col-md-6" form={form} inputChangedHandler={(evt, id) => validator(evt, id, form, setForm)} />
                <Input
                    invalid={!skill.valid}
                    value={skill.value}
                    shouldValidate={skill.validation}
                    touched={skill.touched}
                    changed={(event) => validateSkill(event)}
                    style={{ resize: "none" }}
                    elementType="textarea"
                    inputContainerClass="col-md-12"
                    elementConfig={{ id: "skills", placeholder: " ", rows: "1" }}
                    label={"Specify Required Skills ?"}
                />
                <div className={`col-md-4 ${classes.Buttons}`}>
                    <button type="submit" disabled={!(form.formIsValid && skill.valid)}>Send</button>
                    <button type="reset" onClick={clearEverything}>Clear</button>
                </div>
                <div className={`col-md-8 ${classes.privacytext}`}>
                    <h3>
                        We value your unique ideas and IP
                    </h3>
                    <p>
                        To ensure complete privacy, you can request a non-disclosure agreement.  Just mention it in the message.
                    </p>
                </div>
            </form>
        </div>
    )
}

export default Form
