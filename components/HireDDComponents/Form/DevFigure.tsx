//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss';
import { cdn } from '../../../config/cdn';
const DevFigure = (props) => {
    return (
        <div className={classes.DevFigure}>
        <div className={classes.image}>
            <span onClick={props.onClose}><img src="https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/2017/close-popup.png" alt="Close"/></span>
            <div>
                <img src={cdn(props.image.name)} alt={props.name}/>
            </div>
        </div>
        <div className={classes.devname}>
            <h6>Hire - <span>{props.name}</span></h6>
            <p>{props.designation}</p>
        </div>
    </div>
    )
}

export default DevFigure
