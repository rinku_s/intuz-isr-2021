import React from 'react'

const Developer = (props) => {
    return (
        <div>
            <img src={props.image} alt={props.name}/>
            <h5>{props.name}</h5>
            <p>{props.designation}</p>
            <a onClick={props.onClick}>Profile</a>
        </div>
    )
}

export default Developer
