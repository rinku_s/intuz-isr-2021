import { myLoader } from 'config/image-loader';
import Image from 'next/image';
import React from 'react';
import classes from './styles.module.scss';
const WeGitItBlock = (props) => {
    return (
        <div className={`${props.className ? props.className : ''} ${classes.Block}`}>
            <h3>
                <Image className="ml-8 mt-8" loader={myLoader} layout="fixed" height={50} width={50} src={props.image} alt={props.title} />
                {props.title}</h3>
            <ul>
                {props.points.map(point => (
                    <li key={point}>{point}</li>
                ))}
            </ul>
        </div>
    )
}

export default WeGitItBlock
