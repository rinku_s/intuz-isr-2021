import React from 'react';
import WeGitItBlock from './WeGitItBlock';

const WeGetIt = (props) => {
    return (
        <div className={`row`} style={props.style}>
            <WeGitItBlock
                className="col-md-4"
                image={'weget_experience.png'}
                title="Experience"
                points={["You don’t just hire developer(s) with Intuz. You get the entire team’s technology strength to overcome complex, challenging problems in your project, that is 1500+ projects strong.", "We don’t just code the project. We own it. We suggest. We add value."]}
            />
            <WeGitItBlock
                className="col-md-4"
                image={'weget_commitment.png'}
                title="Commitment"
                points={["Pay for quality, not for errors.", "Money back guarantee", "Unmatched collaboration – We use tools to make your more productive"]}
            />
            <WeGitItBlock
                className="col-md-4"
                image={'weget_freedom.png'}
                title="Freedom"
                points={["You hire us because you want freedom to grow.", "Flexible process that is magical – An exemplary fusion of Waterfall, Scrum and Kanban.", "Client centric engagement model: Hourly, Monthly, Yearly."]}
            />
            <WeGitItBlock
                className="col-md-4"
                image={'weget_quality.png'}
                title="Quality"
                points={["Outstanding coding practices.", "Internal code review.", "Highest standards in the industry."]}
            />
            <WeGitItBlock
                className="col-md-4"
                image={'weget_security.png'}
                title="Security"
                points={["NDA & IP protection.", "Highly secured environment."]}
            />
            <WeGitItBlock
                className="col-md-4"
                image={'weget_availability.png'}
                title="Availability"
                points={["Real-time monitoring.", "High availability.", "Immediate response."]}
            />
        </div>
    )
}

export default WeGetIt

