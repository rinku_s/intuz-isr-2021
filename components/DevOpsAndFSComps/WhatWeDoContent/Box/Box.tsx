//@ts-nocheck
import React from "react";
import LazyLoad from "react-lazyload";
import { cdn } from "../../../../config/cdn";
import classes from "./styles.module.scss";
import Image from "next/image";
import { myLoader } from "config/image-loader";
const Box = (props) => {
  return (
    <div className={classes.Box}>
      <div className={classes.TopImg}>
        <Image
          layout="fixed"
          loader={myLoader}
          src={props.img}
          alt={props.title}
          width="50"
          height="50"
        />
      </div>
      <h6>{props.title}</h6>
      <p>{props.description}</p>
      <div className={classes.separator} />
      <div className={classes.TechStack}>
        {props.technologies.map((tec) => (
          <Image
            layout="fixed"
            loader={myLoader}
            src={tec.name}
            alt={props.title}
            width="50"
            height="50"
          />
        ))}
      </div>
    </div>
  );
};

export default Box;
