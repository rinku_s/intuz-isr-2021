//@ts-nocheck
import React from 'react';
import { cdn } from '../../../config/cdn';
import Box from './Box/Box';

const WhatWeDoContent = (props) => {
    return (

        <div className={`row ${props.className}`} style={{marginTop:"10rem"}}>
            {props.content.map(c=>(
                <div key={c.title} className="col-12 col-md-4">
                    <Box
                        title={c.title}
                        img={c.icon.name}
                        description={c.description}
                        technologies={c.technologies}
                    />
                </div>
            ))}
            {/* <div className="col-12 col-md-4">
                <Box/>
            </div>
            <div className="col-12 col-md-4">
                <Box/>
            </div> */}
        </div>
    )
}

export default WhatWeDoContent
