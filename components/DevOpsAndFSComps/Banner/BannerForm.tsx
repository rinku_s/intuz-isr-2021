//@ts-nocheck
//@ts-nocheck
import React, { useContext } from 'react';
import { ProcessingContext } from '../../../context/processingContext';
import { SubmitForm, useForm, validator } from '../../../hooks/useForm';
import FormFields from '../../CommonFormFields/FormFields';
import FormButton from '../../UI/FormButton/FormButton';
import classes from './styles.module.scss';


const BannerForm = (props) => {

    const [form, setForm] = useForm();
    const { setProcessing } = useContext(ProcessingContext);

    async function StartFormSubmission(data) {
        setProcessing(true);
        await SubmitForm(data, setProcessing, props.thankyoupage);

    }


    function submit(evt) {
        evt.preventDefault();
        if (form.formIsValid) {
            var data = new FormData();
            data.append("f_name", form.fields.f_name.value);
            data.append("l_name", form.fields.l_name.value);
            data.append("email", form.fields.email.value);
            data.append("phone", form.fields.phone.value);
            data.append("description", form.fields.project_brief.value);
            data.append("leadsource", props.leadsource);
            data.append("url", window.location.href);
            data.append("subject", props.subject);
            StartFormSubmission(data);
        } else {
            alert("Please enter valid data");
        }

    }

    return (
        <div className={classes.BannerForm} style={props.style}>
            <span>
                <img src="https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/mobileappdev/confi-ic.png" alt="Confidential" />
                Safe & Confidential
            </span>
            <h3>
                Get Started
            </h3>
            {  <form className="row" onSubmit={submit} autoComplete="off">
                <FormFields form={form} inputChangedHandler={(evt, id) => validator(evt, id, form, setForm)} />
                <FormButton isValid={form.formIsValid} className={classes.send} submitBtn="Send" />
            </form>
            }
        </div>
    )
}

export default BannerForm
