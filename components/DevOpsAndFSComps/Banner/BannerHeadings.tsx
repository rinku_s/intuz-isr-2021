//@ts-nocheck
import React from 'react';
import classes from './styles.module.scss';

const BannerHeadings = (props) => {
    return (
        <div className={`${classes.BannerHeadings} ${classes[props.variation]}`} style={props.style}>
            <h1 dangerouslySetInnerHTML={{__html:props.heading}}>
            </h1>
            {props.subheading ? <p>{props.subheading}</p> : ''}
            {props.children}

        </div>
    )
}

export default BannerHeadings
