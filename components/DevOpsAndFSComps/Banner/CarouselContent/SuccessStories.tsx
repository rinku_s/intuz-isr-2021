//@ts-nocheck
import React from 'react';
import classes from './styles.module.scss';
const SuccessStories = (props) => {
    return (
        <div className={classes.SuccessStories}>
            <img src={props.img} alt={props.alt}/>
            <h6>{props.title}</h6>
            <p>{props.story}</p>
        </div>
    )
}

export default SuccessStories
