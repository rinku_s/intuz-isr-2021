//@ts-nocheck
import React, { useRef } from "react";
import Slider from "react-slick";
import BannerForm from "./BannerForm";
import BannerHeadings from "./BannerHeadings";
import SuccessStories from "./CarouselContent/SuccessStories";
// import 'slick-carousel/slick/slick.css';
import classes from "./styles.module.scss";

const MobileApp = (props) => {
  let r = useRef();
  const settings = {
    dots: false,
    arrows: false,
    infinite: true,
    speed: 500,
    slide: "ul",
    className: classes.carousel,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
    cssEase: "linear",
  };
  const form = {
    subject: "Mobile App Development Request",
    leadsource: "Mobile App LP - Header",
    thankyoupage: "app-development-thankyou",
  };

  return (
    <div className={classes.Banner}>
      <BannerHeadings
        heading={"Success stories from Our Partners"}
        variation="mobileApp"
      >
        <div className={classes.CarouselStory}>
          <Slider
            {...settings}
            adaptiveHeight={true}
            lazyLoad="ondemand"
            ref={r}
          >
            <SuccessStories
              img="https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/mobileappdev/live4it-client.png"
              alt="Live4It Locations"
              title="Founder - Live4It Locations, UK"
              story="When I was looking around for app development companies, other companies try to explain things over the phone. Intuz was the only vendor who actually shared their screen with me and presented a PowerPoint presentation. Their organization and how professional they were stood out to me from the start."
            />
            <SuccessStories
              img="https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/mobileappdev/1c1d-client.png"
              alt="Kingdom League International"
              title="Founder – Kingdom League International, USA"
              story="Intuz not only provided a great value but their team actually understood our goals better and introduced a creative design which saved us monies and introduced features that I thought I'd have to wait another two years for."
            />
            <SuccessStories
              img="https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/mobileappdev/live4it-client.png"
              alt="Live4It Locations"
              title="Founder, PocketMoney Coupons"
              story="They impressed me so much that they are going to handle all of our digital needs going forward because of the experience I have had with them!"
            />
          </Slider>
          <div className={classes.Buttons}>
            <button onClick={() => r.current.slickPrev()}>
              <img src={"/static/Images/l-arr.png"} alt="Left Arrrow" />
            </button>
            <button onClick={() => r.current.slickNext()}>
              <img src={"/static/Images/r-arr.png"} alt="Right Arrrow" />
            </button>
          </div>
        </div>
      </BannerHeadings>
      <BannerForm {...form} />
    </div>
  );
};

export default MobileApp;
