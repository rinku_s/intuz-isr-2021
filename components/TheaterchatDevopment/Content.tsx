//@ts-nocheck


import React from 'react';
import ContentBlock from './ContentBlock';
import style from './styles.module.scss';

const Content = (props) => {
    return (
        <div className={`row ${style.DevContent}`}>
            <ContentBlock 
                count = '1'
                title = 'Research'
                desc = 'We researched on color shades for the app screen and font. We also tried different font sizes to achieve the best readability. Real time testing enables us to get exact idea about app’s look and feel.'
            />
            <ContentBlock 
                count = '2'
                title = 'iPhone Development'
                desc = 'Integration of third party app permission was no more required with the changes in project scopes. We designed and developed a dark theme based iPhone app with cool features and functionalities.'
            />
            <ContentBlock 
                count = '3'
                title = 'Android Development'
                desc = 'Client was quite impressed with the output of iPhone app and asked us to develop the same app for Android platform. Intuz built a fabulous app for Android devices and made both the apps live together.'
            />
            <ContentBlock 
                count = '4'
                title = 'App Revenue'
                desc = 'We helped client in earning bucks by developing in-app purchase option. End-user of the app have to pay if they want to access different font sizes and colors as well as pre-defined message suggestions.'
            />
        </div>
    )
}

export default Content