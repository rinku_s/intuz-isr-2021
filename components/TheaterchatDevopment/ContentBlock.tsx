//@ts-nocheck



import React from 'react';
import { cdn } from '../../config/cdn';
import style from './styles.module.scss';

const Content = (props) => {
    return (
        <div className={`col-md-6 ${style.ContentBlock}`}>
            <span>{props.count}</span>
            <div style={{background: `url(${cdn('dev-porocess-bg.png')}) center center no-repeat`}}>
                <h4>{props.title}</h4>
                <p>{props.desc}</p>
            </div>
        </div>
    )
}

export default Content