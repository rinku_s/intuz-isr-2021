//@ts-nocheck
import React from 'react';
import { cdn } from '../../config/cdn';
import style from './styles.module.scss';

const Content = (props) => {
    return(
            <div className={style.ClientEngagement} >
                <img src={"/static/Images/case-studies/theaterchat/client_eng_title.png"} />
                <p>Initially, the client wanted to achieve a reduction in iPhone’s brightness and volume through one click while chat is going on. <br /><br /> During the R&D, we identified that integration of such functionality requires third-party app permission which is not supported in iOS devices. Furthermore, It required changes in inbuilt software configurations and settings of the iPhone device. However, it was not suggestive. <br /><br /> Configuration changes are possible with Android phones, but the client wanted to go for iPhone only. So, we changed scopes of the project and suggested to create an app with a dark theme that delivers smoother chat experience to the users.</p>
                <div>
                <img src={cdn('client-eng-img.png')} alt='client-eng-img' />
                </div>
            </div>
            
    )
}

export default Content