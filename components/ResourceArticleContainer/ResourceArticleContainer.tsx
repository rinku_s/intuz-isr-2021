//@ts-nocheck
import HtmlParser from 'html-react-parser';
import Link from 'next/link';
import React from 'react';
import classes from './styles.module.scss';
const ResourceArticleContainer = (props) => {
    let imgb = React.useRef();
    let conb = React.useRef();


    let link = (
        <Link href={props.link} prefetch={false}>
            <a className={classes.btn} href={props.link}>Read More</a>
        </Link>
    )

    if (props.knowledge) {
        link = (
            <Link href={`/knowledge-base/${props.link}`} prefetch={false}>
                <a className={classes.btn}>Read More</a>
            </Link>

        )
    }


    return (

        <div className={`row ${classes.ResourceArticleContainer}`}>
            <div className="col-md-4 align-self-center text-center">
                <div className={classes.Image} ref={imgb} >
                    <img src={props.image + "?fm=pjpg&auto=format"} alt={props.title} />
                </div>
            </div>
            <div className={`col-md-8 ${classes.content}`} ref={conb}>
                <h2><Link href={props.knowledge ? `/knowledge-base/${props.link}` : props.link} prefetch={false}><a>{HtmlParser(`${props.title}`)}</a></Link></h2>
                <p>{props.description}</p>
                {link}
            </div>
        </div>
        // </WaypointOnce >
    )
}

export default ResourceArticleContainer
