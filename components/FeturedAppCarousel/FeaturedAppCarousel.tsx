import React from "react";
import Slider from "react-slick";

import FeturedApp from "./FeturedApp";
// import './slick-dots.css';
const featuredApp = [
  {
    image: "feature-app-live4it.png",
    alt: "Live4it",
    right: true,
    logo: "feature-app-live4it-logo.png",
    altLogo: "Live4it Logo",
    desc: "Live4It Locations is a revolutionary directory app that helps you find and explore your favorite hobbies and nearby activities.",
    links: [
      {
        id: "iOS",
        link: "https://itunes.apple.com/us/app/bite-almuerzos-caseros/id1246683187?ls=1&mt=8",
        image: "ios-f-ic.png",
        backgroundColor: "orange",
      },
      {
        id: "Android",
        link: "https://play.google.com/store/apps/details?id=com.live4it",
        image: "android-f-ic.png",
        backgroundColor: "orange",
      },
    ],
  },
  {
    image: "feature-app-biteimg.png",
    alt: "Bite",
    right: false,
    logo: "feature-app-bitelogo.png",
    altLogo: "Bite Logo",
    desc: "Swipe, Choose, & Bite - Discovering your next meal has never been easier.",
    links: [
      {
        id: "iOS",
        link: "https://itunes.apple.com/us/app/bite-almuerzos-caseros/id1246683187?ls=1&mt=8",
        image: "ios-f-ic.png",
        backgroundColor: "red",
      },
      {
        id: "Android",
        link: "https://play.google.com/store/apps/details?id=com.bite.customer",
        image: "android-f-ic.png",
        backgroundColor: "red",
      },
    ],
  },
];

const FeaturedAppCarousel = () => {
  return (
    <Slider
      centerMode={false}
      centerPadding={0}
      lazyLoad={true}
      dots={true}
      dotsClass="dots"
      adaptiveHeight={false}
    >
      {featuredApp.map((fa) => (
        <FeturedApp key={fa.image} app={fa} />
      ))}
    </Slider>
  );
};

export default FeaturedAppCarousel;
