import { myLoader } from 'config/image-loader';
import Image from 'next/image';
import React from 'react';
import { cdn } from '../../config/cdn';
import classes from './styles.module.scss';
const FeturedApp = ({ app }) => {
    return (
        <div className={classes.FeturedApp}>
            <div className={classes.image}>

                <Image loader={myLoader} layout='fixed' src={app.image} alt={app.alt} height={470} width={500} />

            </div>
            <div className={`${classes.caption} ${app.right ? classes.right : classes.left}`}>
                <div className={classes.logo}>
                    <Image loader={myLoader} layout='fixed' src={app.logo} alt={app.altLogo} height={110} width={190} />

                </div>
                <p>{app.desc}</p>
                <div className={classes.link}>
                    {app.links.map(al => (
                        <a href={al.link} key={al.id} style={{ backgroundColor: al.backgroundColor }}><img src={cdn(al.image)} alt={al.id} /></a>
                    ))}
                </div>
            </div>
        </div>
    )
}

export default FeturedApp
