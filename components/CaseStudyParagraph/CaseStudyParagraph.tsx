import React from 'react'
import classes from './styles.module.scss'

const CaseStudyParagraph = props => {
    let variation = props.variation
    return (
        <p className={`${classes[variation]} ${props.className}`} style={props.style} dangerouslySetInnerHTML={{__html: props.children}}  id={props.id}></p>
    )
}

export default CaseStudyParagraph
