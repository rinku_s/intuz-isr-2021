//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap';
import Router from 'next/router';
import React, { Component } from 'react';
import Transition from 'react-transition-group/Transition';
// import NavigationView from '../../Navigation/NavigationMenu/';
import style from './styles.module.scss';

export default class BlogCategory extends Component {

    constructor(props) {
        super(props)
        this.state = { showCategory: false, currentPath: '' }
    }

    componentDidMount() {
        this.setState({ currentPath: Router.asPath })
    }

    toggleCategory = () => {
        this.setState({ showCategory: !this.state.showCategory })
    }

    render() {
        let tl = gsap.timeline();

        return (
            <>

                {this.state.showCategory ? <img src={"/static/Images/close-icon.png"} alt='close-icon' className={`${style.filterIcon} filter_icon`} onClick={() => this.toggleCategory()} /> : <img src={"/static/Images/filter-icon.png"} id='filterCat' alt='filter-icon' className={`${style.filterIcon} filter_icon`} onClick={() => this.toggleCategory()} />}

                <Transition
                    in={this.state.showCategory}
                    timeout={1000}
                    mountOnEnter={true}
                    unmountOnExit={true}
                    onEntering={(node, appearing) => {
                        tl.fromTo(node, 0.5, { scaleY: 0.5, opacity: 0, ease: "Power0.easeIn" }, { scaleY: 1, opacity: 1, ease: "Power0.easeIn" }, .1)
                            .staggerFromTo(node.getElementsByTagName('li'), .4, { x: -30, opacity: 0, ease: "Power2.easeIn" }, { x: 0, opacity: 1, ease: "Power2.easeIn" });
                    }}
                    onEnter={(node) => { gsap.set(node, { y: 0, opacity: 0, transformOrigin: "0 0" }); }}
                    onExit={(n) => { gsap.set(n, { opacity: 1, transformOrigin: "0 0" }); }}
                    onExiting={(n, appearing) => { tl.fromTo(n, 0.5, { scaleY: 1, opacity: 1, ease: "Power0.easeIn" }, { scaleY: 0.5, opacity: 0, ease: "Power0.easeIn" }) }}
                    addEndListener={(n, done) => {
                        if (this.state.showCategory) {
                            document.body.style.overflowY = "hidden";
                            document.getElementById('mobileHeader').style.backgroundColor = 'rgba(0,0,0,1)';
                            document.getElementById('headerCollapse').style.display = 'none';
                        } else {
                            document.body.style.overflowY = "visible";
                            document.getElementById('mobileHeader').style.backgroundColor = '';
                            document.getElementById('headerCollapse').style.display = 'block';
                        }
                    }}
                >
                    {/* <NavigationView>
                        <li className={style.categoryItem}>Filter by Category</li>
                        <li className={style.categoryItem}>
                            <Link href="/blog" prefetch={false} >
                                <a className={this.state.currentPath == '/blog' ? style.ActivePage : ''}>Latest Articles</a>
                            </Link>
                        </li>
                        {
                            this.props.categories && this.props.categories.map((category, index) =>
                                <li className={style.categoryItem} key={index}>
                                    <Link href={`/blog/category/${category.link}`} prefetch={false}>
                                        <a className={this.state.currentPath.indexOf(category.link) !== -1 ? style.ActivePage : ''}>{category.name}</a>
                                    </Link>

                                </li>
                            )
                        }
                    </NavigationView> */}
                </Transition>
            </>
        )
    }
}