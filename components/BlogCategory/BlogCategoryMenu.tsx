//@ts-nocheck

//@ts-nocheck

import Link from 'next/link';
import Router from 'next/router';
import React, { Component } from 'react';
import { Media, MediaContextProvider } from "../../config/responsiveQuery";
import Container from '../Container/Container';
import DropDown from './DropDown';
import BlogCategory from './Mobile/BlogCategory';
import style from './styles.module.scss';
export default class BlogCategoryMenu extends Component<any, { currentPath: any }> {
    constructor(props) {
        super(props)
        this.state = { currentPath: null };
    }

    StickyHeader = () => {
        let offset = this.catref ? this.catref.offsetTop : '';
        if (offset) {
            if (window.pageYOffset > offset) {
                this.catref.classList.add(style.sticky);
            }
            if (window.pageYOffset < 500) {
                this.catref.classList.remove(style.sticky);
            }
        }
    }

    componentDidMount() {
        if (window.innerWidth > 767) {
            window.addEventListener('scroll', () => this.StickyHeader());
        }
        this.setState({ currentPath: Router.asPath })
    }

    componentWillUnmount() {
        if (window.innerWidth > 767) {
            window.removeEventListener('scroll', () => this.StickyHeader());
        }
    }

    render() {
        if (this.props.categories.length <= 0) {
            return null
        } else {
            let moreCatArray = [];
            return (
                <MediaContextProvider>
                    <Media greaterThanOrEqual="sm">
                        <section className={`${style.Categories}`} ref={c => this.catref = c} >
                            <Container>
                                <Link href="/blog" prefetch={false} >
                                    <a className={this.state.currentPath === '/blog' ? `${style.active}` : ''}>Latest Articles</a>
                                </Link>
                                {/* {
                                    this.props.categories && this.props.categories.map((category, index) => 
                                        <Link href={`/blog/category/${category.link}`} prefetch={false} key={index}>
                                            <a className={`${this.state.currentPath}`.indexOf(category.link) !== -1 ? `${style.active}` : ''}>{category.name}</a>
                                        </Link>
                                    )
                                } */}

                                {/* Code wil execute for multiple categories */}
                                {
                                    this.props.categories && this.props.categories.map((category, index) => {
                                        if (index < 3) {
                                            return (
                                                <Link href="/blog/category/[name]" as={`/blog/category/${category.link}`} prefetch={false} key={index}>
                                                    <a className={`${this.state.currentPath}`.indexOf(category.link) !== -1 ? `${style.active}` : ''}>{category.name}</a>
                                                </Link>
                                            )
                                        } else {
                                            moreCatArray.push({ 'value': index, 'name': category.name, 'link': category.link })
                                        }
                                    })
                                }
                                {(moreCatArray.length > 0 ? <DropDown options={moreCatArray} /> : '')}
                            </Container>
                        </section>
                    </Media>
                    <Media lessThan="sm">
                        <BlogCategory categories={this.props.categories} />
                    </Media>
                </MediaContextProvider>
            )

        }
    }
}
