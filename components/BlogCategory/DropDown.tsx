//@ts-nocheck
//@ts-nocheck
import React, { Component } from 'react';
import classes from './styles.module.scss';
export default class Dropdown extends Component {

  constructor(props) {
    super(props)
  }

  state = { isOpen: false };


  render() {
    return (
      <div className={`dropdown ${classes.Dropdown}`} onClick={() => this.setState({ isOpen: !this.state.isOpen })}>
        <button
          type="button"
          id="dropdownMenuButton"
          data-toggle="dropdown"
          aria-haspopup="true"
        >
          More +
          </button>

        <div className={`dropdown-menu ${classes.DropdownMenu}${this.state.isOpen ? " show" : ""}`} aria-labelledby="dropdownMenuButton">
          {this.props.options.map((option) => (
            <a href={`/blog/category/${option.link}`} className="dropdown-item" key={option.value}>{option.name}</a>
          ))}

        </div>
        <style jsx>
          {`
                button {
                    font-size: 16px;
                    color: #6f6f6f;
                    padding: 0;
                    background: none;
                    border: none;
                }
                button:hover {
                  background: none;
                  color: #6f6f6f;
                  border: none;
                  box-shadow: none;
                }
                
                .dropdown-menu.show {
                    opacity: 1 !important;
                }
                    
            `}
        </style>
      </div>
    );
  }
}