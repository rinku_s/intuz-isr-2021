//@ts-nocheck

import Link from 'next/link';
import React from 'react';

const Category = (props) => {
    return (
        <>
            {
                props.categories.map((category, index) => (
                    <Link as={`/blog/category/${category.link}`} href='/blog/category/[name]' prefetch={false} key={index}>
                        <a className='category'>{category.name}</a>
                    </Link>
                ))
            }
            <style jsx> 
            {`
                .category {
                    color: #714581;
                    font-size: 16px;
                    text-transform: uppercase;
                    display:Block;
                    text-decoration: none;
                    font-weight: bold;
                }   
            `}
            </style>
        </>
    )
}

export default Category