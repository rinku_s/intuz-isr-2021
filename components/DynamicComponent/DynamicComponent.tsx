//@ts-nocheck
import React from 'react'

const DynamicComponent = (props) => {
    
    const TagName = props.tag;
    return <TagName />
}

export default DynamicComponent;