//@ts-nocheck

import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../config/cdn';
import Point from './Points';
import style from './styles.module.scss';

const WaterPumpApp = (props) => {
    return(
        <div className={style.WaterPumpFeature}>
            <h3>Controlling Water Pump Jet IoT Application Solution</h3>
            <p><strong>Technology Stack:</strong> NodeJS, ReactJS, ReactNative, MQTT, ESP32, AWS Cloud</p>
            <div>
                <div>
                    <h4>Web & Mobile Application Features</h4>
                    <ul>
                        <Point>Device onboarding</Point>
                        <Point>Wifi provisioning</Point>
                        <Point>MQTT based communication</Point>
                        <Point>Start / Stop device</Point>
                        <Point>Timer</Point>
                        <Point>Schedule</Point>
                        <Point>Time zone support</Point>
                        <Point>Multiple hardware device integration</Point>
                        <Point>Device sharing with other users’ app</Point>
                        <Point>Full Over The Air (OTA) update</Point>
                        <Point>AWS Cloud deployment</Point>
                    </ul>
                </div>
                <div>
                    <LazyLoad height={300} offset={300}>
                        <img src={cdn('iot-waterpump-casestudy-img.png?auto=format&w=700')} alt='iot-waterpump-casestudy-img' />
                    </LazyLoad>
                </div>
            </div>
        </div>
    )
}

export default WaterPumpApp;