import { useQuery } from "@apollo/react-hooks";
import Error from "next/error";
import React from "react";
import { HeadAndFoot } from "../HeadAndFoot/HeadAndFoot";
// import LoadingScreen from '../LoadingScreen/LoadingScreen';
const QueryComp = ({ query, variables, customScript, children }) => {
  const { loading, error, data } = useQuery(query, {
    variables: variables,
  });

  if (error) return <Error statusCode={500} />;
  else if (loading) return <HeadAndFoot loading={true} />;

  return (
    <HeadAndFoot loading={false} customScript={customScript}>
      {children(data)}
    </HeadAndFoot>
  );
};
export default QueryComp;
