//@ts-nocheck
//@ts-nocheck
import React from 'react';
import AppLink from '../AppLink/AppLink';
import ImageBlock from '../ImageBlock/ImageBlock';
const FeaturedApp = (props) => {
    return (
        <div className="featuredapp">
            <div className="logo">
                <ImageBlock src={props.logo} alt={props.title} format="png" mwidth={props.logoM} dwidth={props.logoD} />
                {/* <img /> */}
                <div className="appavailable">
                    <AppLink link={{ android: "#", ios: "" }} />
                </div>
            </div>
            <div className="appimage"> <ImageBlock src={props.appimage} alt={props.title} format="png" mwidth={props.appM} dwidth={props.appD} /> </div>
            <style jsx>
                {`
                    .featuredapp{
                        display: flex;
                        justify-content:space-evenly;
                        align-items:center;
                        flex: 1;
                        background:url(${props.background + "?fm=pjpg&auto=format"}) no-repeat;
                        background-size: cover;
                        padding-top:5rem;
                    }
                    
                    .appavailable{
                        padding:1rem 0;
                    }
                    
                    .logo{
                        margin-bottom:3rem;
                    }
                    .appimage{
                         align-self:${props.endalign ? "flex-end" : ''}
                          
                        
                    }
                    .featuredapp img, .appimage img{
                        max-width:100%;
                    }
                    @media only screen and (max-width: 767px) { 
                        .featuredapp{
                            flex-direction:column;
                        }
                        .appimage{
                         justify-self:${props.endalign ? "flex-end" : ''}; 
                         align-self:center;  
                        }
                     };
                `}
            </style>
        </div>
    )
}

export default FeaturedApp
