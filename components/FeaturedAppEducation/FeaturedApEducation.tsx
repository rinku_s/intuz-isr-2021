//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
import FeaturedApp from './FeaturedApp';
import { cdn } from '../../config/cdn';
const FeaturedApEducation = () => {
    return (
        <div className={classes.FeaturedApEducation}>
            <FeaturedApp
                logo={cdn("obgy-logo.png")} 
                title="Obgy"
                logoM={117}
                logoD={227}
                appM={142}
                appD={342}
                appimage={cdn("obgy-app.png")} 
                endalign 
                background={cdn("ed-fp-l.jpeg")} />
            <FeaturedApp 
                logoM={117}
                logoD={227}
                appM={152}
                appD={352}
                logo={cdn("dr-logo.png")} 
                title="Dr Mentor" 
                appimage={cdn("drmentorapp.png")} 
                background={cdn("ed-fp-r.jpeg")} />
        </div>
    )
}

export default FeaturedApEducation
