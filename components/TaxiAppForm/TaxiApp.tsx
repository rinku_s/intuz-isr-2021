//@ts-nocheck
import React from 'react';
import Form from './Form';
import Information from './Information';
import style from './styles.module.scss';

const TaxiApp = () => {
    return(
        <div className={style.TaxiAppForm}>
            <Form />
            <Information />
        </div>
    )
}

export default TaxiApp;