//@ts-nocheck
import React from 'react';
import style from './styles.module.scss';

const Information = () => {
    return(
        <div className={style.Info}>
            <h4>Idea and IP Protection</h4>
            <p>We value your unique ideas and IP. To ensure complete privacy, you can request a non- disclosure agreement. Just mention it in the message.</p>
        </div>
    )
}

export default Information;