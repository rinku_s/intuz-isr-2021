//@ts-nocheck
import React from 'react';
import FormFields from '../CommonFormFields/FormFields';
import FormButton from '../UI/FormButton/FormButton';

const Form = () => {
    return(
        <form className="row" autoComplete="off">
            <FormFields />
            <FormButton 
                submitBtn='Send'
                btnText='Clear'
            />
        </form>
    )
}

export default Form;