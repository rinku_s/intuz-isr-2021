import React, { Component } from 'react';
import LinkButton from '../UI/LinkButton/LinkButton';
import style from './styles.module.scss';

interface CustomAppProps {
    title?: string;
    subTitle?: string;
    description?: string;
    buttonLink?: string;
    buttonLabel?: string;
}

export default class CustomApp extends Component<CustomAppProps, {}> {

    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className={style.Content}>
                {this.props.title ? <h1 className='z-10'>{this.props.title}</h1> : ''}
                {this.props.subTitle ? <span className='z-10'>{this.props.subTitle}</span> : ''}
                <ul className='z-10' dangerouslySetInnerHTML={{ __html: this.props.description }}></ul>
                {this.props.buttonLabel ? <LinkButton className='z-10' href={this.props.buttonLink} variation="customAppMobile">{this.props.buttonLabel}</LinkButton> : ''}
            </div>
        )
    }
}