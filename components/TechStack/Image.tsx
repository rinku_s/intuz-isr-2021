//@ts-nocheck

import React from 'react';

const Image = (props) => {
    return (
        <div>
            { <img src={(`/static/Images/partner/TechStack/${props.src}`)} alt={props.alt} />}
            <span>{props.title}</span>
        </div>
    )
}

export default Image
