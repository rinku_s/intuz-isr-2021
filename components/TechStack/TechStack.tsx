//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import Image from './Image';
import style from './styles.module.scss';

const TechStack = () => {
    // setOpacity(`.left, .right, .bottom`);
    function play() {
        const tl = gsap.timeline();
        tl.fromTo('.left', 1, { x: -20, opacity: 0, ease: "power0.ease" }, { x: 0, opacity: 1, ease: "power0.ease" }, 0.5)
            .fromTo('.right', 1, { x: 20, opacity: 0, ease: "power0.ease" }, { x: 0, opacity: 1, ease: "power0.ease" }, 0.5)
            .fromTo('.bottom', 1, { y: 20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5);
        tl.play();
    }
    return (
        <>
            {/*  */}
            <div className={style.topBlocks}>
                <div className={`${style.leftBlock} left`}>
                    <img src={"/static/Images/partner/TechStack/apple-ic.png"} alt="apple-ic.png" />
                    <h2>iOS Development</h2>
                    <div className={style.iconBlock}>
                        <Image src='ic_swift.jpg' alt='ic_swift' title='Swift' />
                        <Image src='ic_reactnative.jpg' alt='ic_reactnative' title='React Native' />
                        <Image src='ic_objectivec.jpg' alt='ic_objectivec' title='Native iOS' />
                    </div>
                </div>

                <div className={`${style.rightBlock} right`}>
                    <img src={"/static/Images/partner/TechStack/android-ic.png"} alt="/android-ic.png" />
                    <h2>Android Development</h2>
                    <div className={style.iconBlock}>
                        <Image src='ic_java.jpg' alt='ic_java' title='Java' />
                        <Image src='ic_kotin.jpg' alt='ic_kotin.jpg' title='Kotlin' />
                        <Image src='ic_ionic.jpg' alt='ic_ionic' title='Ionic' />
                        <Image src='ic_reactnative.jpg' alt='ic_reactnative' title='React Native' />
                    </div>
                </div>
            </div>
            {/*  */}
            <div className={style.bottomBlocks}>
                <div className={`${style.leftBlock} left`}>
                    <img src={"/static/Images/partner/TechStack/cloud-ic.png"} alt="cloud-ic.png" />
                    <h2>Cloud Technology</h2>
                    <div className={style.iconBlock}>
                        <Image src='ic_aws.jpg' alt='ic_aws' title='AWS services' />
                        <Image src='ic_cloud.jpg' alt='ic_cloud' title='Cloud Formations' />
                    </div>
                    <div className={style.iconBlock}>
                        <Image src='ic_stack.jpg' alt='ic_stack' title='Stack Development' />
                        <Image src='ic_ami.jpg' alt='ic_ami' title='AMI Development' />
                    </div>
                </div>
                <div className={`${style.middleBlock} bottom`}>
                    <img src={"/static/Images/partner/TechStack/web_tech.png"} alt="web_tech.png" />
                    <h2>Web Technology</h2>
                    <div className={style.iconBlock}>
                        <Image src='ic_angular.jpg' alt='ic_angular' title='Angular Js' />
                        <Image src='ic_node.jpg' alt='ic_node' title='Node Js' />
                        <Image src='magento.jpg' alt='magento' title='Magento' />
                        <Image src='ic_wordpress.jpg' alt='ic_wordpress' title='Wordpress' />
                    </div>
                    <div className={style.iconBlock}>
                        <Image src='ic_reactnative.jpg' alt='ic_reactnative' title='React JS' />
                        <Image src='ic_python.jpg' alt='ic_python' title='Python' />
                        <Image src='ic_php.jpg' alt='ic_php' title='PHP' />
                        <Image src='ic_aspnet.jpg' alt='ic_aspnet' title='Asp.Net' />
                    </div>
                </div>
                <div className={`${style.rightBlock} right`}>
                    <img src={"/static/Images/partner/TechStack/data-service.png"} alt="data-service.png" />
                    <h2>Data Services</h2>
                    <div className={style.iconBlock}>
                        <Image src='ic_mongodb.jpg' alt='ic_mongodb' title='MongoDB' />
                        <Image src='sqlite.jpg' alt='sqlite' title='SQLite' />
                        <Image src='ic_aurora.png' alt='ic_aurora' title='AWS aurora' />
                    </div>
                    <div className={style.iconBlock}>
                        <Image src='ic_dynamodb.jpg' alt='ic_dynamodb' title='ic_dynamodb' />
                        <Image src='ic_mysql.jpg' alt='ic_mysql' title='My Sql' />
                        <Image src='ic_mssql.jpg' alt='ic_mssql' title='MS SQL' />
                    </div>
                </div>
            </div>
        </>
    )
}

export default TechStack;