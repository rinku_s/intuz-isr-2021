import { getCloudApplicationCount, getCloudApplicationLists } from "lib/api";
import {
  Category,
  CloudApplicationList,
  DeliveryMethod,
  PricingPlan,
} from "lib/cloud/ApplicationListModel";
import React, { useEffect, useState } from "react";

const ist = {
  app_list: [],
  start: 0,
  totalCount: 0,
  firstList: true,
  filter: {
    ready: true,
  },
  allFilters: {
    categories: [],
    deliveryMethods: [],
    pricingPlans: [],
  },
};

export const ApplicationListContext = React.createContext<{
  applicationState: ApplicationListState;
  setApplicationState: React.Dispatch<
    React.SetStateAction<ApplicationListState>
  >;
  loadMore: () => void;
}>({
  applicationState: ist,
  setApplicationState: () => null,
  loadMore: () => null,
});

interface ApplicationListState {
  app_list: CloudApplicationList[];
  filter: any;
  start: number;
  totalCount: number;
  firstList: boolean;
  allFilters: {
    categories: Category[];
    deliveryMethods: DeliveryMethod[];
    pricingPlans: PricingPlan[];
  };
}

const ApplicationListProvider: React.FC<{
  initialState: ApplicationListState;
}> = (props) => {
  const [applicationState, setApplicationState] =
    useState<ApplicationListState>(props.initialState);

  useEffect(() => {
    if (!applicationState.firstList) {
      (async () => {
        const list = await getCloudApplicationLists(
          15,
          0,
          applicationState.filter
        );
        const count = await getCloudApplicationCount(applicationState.filter);

        setApplicationState({
          ...applicationState,
          app_list: list.cloudApplicationLists,
          start: 0,
          totalCount: count.cloudApplicationsConnection.aggregate.totalCount,
        });
      })();
    }
    setApplicationState({
      ...applicationState,
      firstList: false,
    });
  }, [applicationState.filter]);

  const loadMore = async () => {
    if (applicationState.app_list.length < applicationState.totalCount) {
      let start = applicationState.start + 15 + 1;
      const list = await getCloudApplicationLists(
        15,
        start,
        applicationState.filter
      );

      setApplicationState({
        ...applicationState,
        app_list: [...applicationState.app_list, ...list.cloudApplicationLists],
        start: start,
      });
    }
  };

  return (
    <ApplicationListContext.Provider
      value={{ applicationState, setApplicationState, loadMore }}
    >
      {props.children}
    </ApplicationListContext.Provider>
  );
};

export default ApplicationListProvider;
