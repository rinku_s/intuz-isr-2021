import axios from "axios";
import { emailRegex } from "config/checkValidity";
import { useProcessing } from "context/processingContext";
import { useFormik } from "formik";
import { useEffect } from "react";

interface FormFields {
  f_name: string;
  l_name: string;
  email: string;
  phone: string;
  description: string;
  leadsource: string;
  url: string;
  subject: string;
  files?: File[];
}

export function useFormFormik(
  leadsource: string,
  subject: string,
  onSuccess: () => void,
  onError: () => void
) {
  const { setProcessing } = useProcessing();
  const formik = useFormik<FormFields>({
    initialValues: {
      f_name: "",
      l_name: "",
      email: "",
      phone: "",
      description: "",
      leadsource: leadsource,
      url: "",
      subject: subject,
      files: null,
    },
    onSubmit: async (values) => {
      try {
        setProcessing(true);
        const data = new FormData();
        Object.keys(values).forEach((c) => {
          if (c === "files" && values[c]) {
            values[c].forEach((f) => {
              data.append("file", f);
            });
          } else {
            data.append(`${c}`, values[c]);
          }
        });
        const response = await axios.post(
          `https://strapimail-prod-aws.intuz.com/mail`,
          data
        );
        setProcessing(false);
        onSuccess();
      } catch (error) {
        setProcessing(false);
        onError();
      }
    },
    validate: (values) => {
      const errors: any = {};
      if (!values.f_name) {
        errors.f_name = "First name is required";
      }
      if (!values.l_name) {
        errors.l_name = "last name is required";
      }
      if (!values.email) {
        errors.email = "Email is required";
      } else if (!emailRegex.test(values.email)) {
        errors.email = "Email is invalid";
      }
      if (!values.phone) {
        errors.phone = "Phone is required";
      }
      if (!values.description) {
        errors.description = "Description is required";
      }
      return errors;
    },
  });

  useEffect(() => {
    formik.setValues((v) => {
      return {
        ...v,
        url: window.location.href,
      };
    });
  }, []);

  return formik;
}
