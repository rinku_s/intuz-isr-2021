import { useEffect, useState } from 'react';
import webpClass from './webpClass';

export default function webp() {
    const [webp, setWebp] = useState(true);
    useEffect(() => {   
            setWebp(webpClass());
    }, []);
    return webp;
}