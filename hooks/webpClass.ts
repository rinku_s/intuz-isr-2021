import {isWebpSupported} from 'react-image-webp/dist/utils';


// import dynamic from 'next/dynamic'
// const Modernizer = dynamic(import('modernizr'), {
//     ssr: false
//   });

export default function webpClass() {
    if(process.browser){
        return isWebpSupported();
    }
}