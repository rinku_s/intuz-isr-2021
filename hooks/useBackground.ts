//@ts-nocheck

const { useEffect } = require("react");

/**
 * @param {Boolean} webm - The date
 * @param {string} videoId - The string
 * @param {string} canvasId - The string
 */
export function useBackground(webm, videoId, canvasId ) {

    useEffect(() => {
        if(webm){

            var vid = document.getElementById(videoId) as HTMLVideoElement;
    
            var wrapper = document.getElementById('main');
            var canvas = document.getElementById(canvasId) as HTMLCanvasElement;
            var ctx = canvas.getContext('2d');
            
            var ratio = window.devicePixelRatio || 1;
            var vidWidth;
            var vidHeight;
            
            
            function drawingLoop() {
                window.requestAnimationFrame(drawingLoop)
                ctx.drawImage(vid, 0, 0, vidWidth, vidHeight, 0, 0, canvas.width, canvas.height);
            }
    
            vid.addEventListener('loadedmetadata', function(e){
                console.log('onloadedmetadata');
                vid.style.transform = `scale(${ratio})`;
            
                vidWidth = vid.videoWidth;
                vidHeight = vid.videoHeight;
            
                canvas.width = vid.offsetWidth * ratio;
                canvas.height = vid.offsetHeight * ratio;
            
                canvas.style.width = vid.offsetWidth + "px";
                canvas.style.height = vid.offsetHeight + "px";
            
                drawingLoop(ctx);
            });
    
            vid.addEventListener('loadeddata', function(e){
                console.log('onloadeddata');
                setVideoBgColor(vid, wrapper);
                vid.play();
            });
    
    
            window.onresize = function (event) {
                vidWidth = vid.videoWidth;
                vidHeight = vid.videoHeight;
            
                canvas.width = vid.offsetWidth * ratio;
                canvas.height = vid.offsetHeight * ratio;
            
                canvas.style.width = vid.offsetWidth + "px";
                canvas.style.height = vid.offsetHeight + "px";
            
                //redraw canvas after resize
                ctx.drawImage(vid, 0, 0, vidWidth, vidHeight, // source rectangle
                    0, 0, canvas.width, canvas.height); // destination rectangle);
            };
            
            
            //add src
            vid.src = vid.dataset.src;
            vid.play();
            let sto = setTimeout(() => {
                vid.style.visibility="hidden"
                vid.style.height="0px"
            }, 1000)
        }
        
        
        
    }, []);
    
    return;
    
    
}
      export function setVideoBgColor(vid, backgroundElement) {
            // draw first four pixel of video to a canvas
            // then get pixel color from that canvas
            var canvas = document.createElement("canvas");
            canvas.width = 8;
            canvas.height = 8;
        
            var ctx = canvas.getContext("2d");
            ctx.drawImage(vid, 0, 0, 8, 8);
        
            var p = ctx.getImageData(0, 0, 8, 8).data;
            //dont take the first but fourth pixel [r g b a]
            backgroundElement.style.backgroundColor = "rgb(" + p[60] + "," + p[61] + "," + p[62] + ")";
        }