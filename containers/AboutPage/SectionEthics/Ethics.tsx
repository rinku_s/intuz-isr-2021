//@ts-nocheck

import React from 'react';
import Content from '../../../components/EthicsContent/Content';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';

const Ethics = (props) => {
    return(
        <>
            <SecondaryHeading>Growth is the key element that reflects in our actions and decisions. It defines the core set for our team to empower and enable to solve even the toughest problems.</SecondaryHeading>
            <Content content = {props.content} />
        </>
    )
}

export default Ethics;