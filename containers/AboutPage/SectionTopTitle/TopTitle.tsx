//@ts-nocheck


import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import { cdn } from '../../../config/cdn';
import windowWidth from '../../../hooks/windowWidth';
import style from './styles.module.scss';


const TopTitle = (props) => {
    var backImage;
    if (props.backImage !== undefined) {
        let windowSize = windowWidth();
        backImage = { background: `url(${cdn(props.backImage)}?fm=pjpg${windowSize < 768 ? '&w=767' : ''}&auto=format) center center no-repeat` }
    }

    return (
        <section className={`${style.sectionTitle} ${style[props.variation]}`} style={backImage}>
            <Container>
                {props.title ?
                    <PrimaryHeading>{props.title}</PrimaryHeading>
                    : ''}
                {props.borderBottom ? <div className='text-center'><img src={"/static/Images/about/border-icon.png"} alt='border-icon.png' /></div> : ''}
                {props.children}
            </Container>
        </section>
    )
}

export default TopTitle;