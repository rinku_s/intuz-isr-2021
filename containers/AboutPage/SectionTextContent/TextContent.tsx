//@ts-nocheck

import React from "react";
import style from './styles.module.scss';

const TextContent = (props) => {
    return(
        <p className={style.TextContent}>{props.children}</p>
    )
}

export default TextContent;

