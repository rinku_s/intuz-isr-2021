//@ts-nocheck

import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../../config/cdn';
import styles from './style.module.scss';


const CustomerValue = (props) => {
    return(
        <div className={`row ${styles.CustomerValue}`}>
            <div className='col-md-6'>
                <p>Values that establish us as true development partner</p>
                <LazyLoad height={300} offset={300} once>
                    <img src={cdn(`true_Partner.svg`)} alt='true_Partner' />
                </LazyLoad>
            </div>
            <div className='col-md-6'>
                <p>Satisfaction is not enough, admiration is strived for</p>
                <LazyLoad height={300} offset={300} once>
                    <img src={cdn(`Admiration.svg`)} alt='Admiration' />
                </LazyLoad>
            </div>
        </div>
    )
}

export default CustomerValue