//@ts-nocheck
import React from "react";
import Container from '../../../components/Container/Container';
import Content from '../../../components/MissionContant/Content';
import style from './styles.module.scss';

const Mission = (props) => {
    return(
        <section className={style.SectionMission}>
            <Container className={style.fullPage}>        
                <Content />
            </Container>
        </section>
    )
}

export default Mission