//@ts-nocheck


import React from 'react';
import { cdn } from '../../../config/cdn';
import style from './styles.module.scss';

const NotablePoints = (props) => {
    if (props.children.length <= 0) {
        return null;
    } else {
        return (
            <div className={style.PointContent}>
                {props.children.map((data, index) => {
                    return (
                        <div className={style.Block} key={index}>
                            <div>
                                <img height={180} width={180} src={cdn(data.icon.name)} alt={data.icon.name} />
                            </div>
                            <div>
                                {data.title ? <h4>{data.title}</h4> : ''}
                                {data.description ? <p dangerouslySetInnerHTML={{ __html: data.description }}></p> : ''}
                            </div>
                        </div>
                    )
                })}
            </div>
        )
    }
}

export default NotablePoints