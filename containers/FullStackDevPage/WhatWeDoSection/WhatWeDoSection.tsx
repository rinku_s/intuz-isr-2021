//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import Container from '../../../components/Container/Container';
import WhatWeDoContent from '../../../components/DevOpsAndFSComps/WhatWeDoContent/WhatWeDoContent';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import classes from './styles.module.scss';



const WhatWeDoSection = (props) => {

    // setOpacity(`.${classes.WhatWeDoSection} h2, .${classes.WhatWeDoSection} p`, { opacity:0 })

    function play() {
        const tl = new gsap.timeline({ paused: true });
        tl.fromTo(`.${classes.WhatWeDoSection} h2`, 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" })
            .fromTo(`.${classes.WhatWeDoSection} p`, 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" })
        tl.play();
    }

    return (

        <section className={classes.WhatWeDoSection}>
            <Container>
                <PrimaryHeading>What we do?</PrimaryHeading>
                <SecondaryHeading fontSize="1.8rem">Get end-to-end JavaScript development services for all your business with the use of state-of-the-art JavaScript frameworks and libraries.</SecondaryHeading>
                <WhatWeDoContent content={props.content} />
            </Container>
        </section>
        // </WaypointOnce >
    )
}

export default WhatWeDoSection
