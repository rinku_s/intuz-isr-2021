//@ts-nocheck
import React from 'react';
import HtmlParser from 'html-react-parser';
import WhyComponent from '../../../../components/CareerComponents/WhyComponent';
import Container from '../../../../components/Container/Container';
import Heading from '../../../../components/HeadingNunito/Heading';
import classes from './styles.module.scss';
import gsap from 'gsap';

const ComponentCareerWhyIntuzSection = ({ title, description, why_component }) => {

    React.useEffect(()=>{
        let tl = gsap.timeline({ scrollTrigger:{
            trigger:`.${classes.ComponentCareerWhyIntuzSection}`,
            start:'top 70%',
            toggleActions:"play none none reverse"
        }});
    
        tl.fade("#whyintuz h2", { duration: 0.5 })
            .fade(`#whyintuz .${classes.description}`, { duration: 0.5 },"-=0.2");
        
      },[])


    return (
        <section className={classes.ComponentCareerWhyIntuzSection} id="whyintuz">
            <Container>
                <Heading>{title}</Heading>
                <div className={classes.description}>
                    {HtmlParser(`${description}`)}
                </div>
            </Container>
                <WhyComponent why_component={why_component} time={4000} />
        </section>
    )
}

export default ComponentCareerWhyIntuzSection
