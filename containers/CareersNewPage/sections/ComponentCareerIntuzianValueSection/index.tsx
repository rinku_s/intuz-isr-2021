//@ts-nocheck
import React from 'react';
import classes from './styles.module.scss';
import Container from '../../../../components/Container/Container';
import Heading from '../../../../components/HeadingNunito/Heading';
import IntuzValues from '../../../../components/CareerComponents/IntuzianValues';
import { Media, MediaContextProvider } from 'config/responsiveQuery';
import IntuzianScrollAnimation from './IntuzianScrollAnimation';
const ComponentCareerIntuzianValueSection = ({ title, subtitle, intuzian_values }) => {
    return (
        <section className={classes.ComponentCareerIntuzianValueSection} id="intuzValues">
            <Container>
                <Heading>{title}</Heading>
                <p className="desc">{subtitle}</p>
            </Container>
            <MediaContextProvider>
                <Media lessThan="sm">
                    <IntuzValues intuzian_values={intuzian_values}/>
                </Media>
                <Media greaterThanOrEqual="sm">
                    <IntuzianScrollAnimation intuzian_values={intuzian_values}/>
                </Media>
            </MediaContextProvider>
        </section>
    )
}

export default ComponentCareerIntuzianValueSection
