//@ts-nocheck
import React, { useEffect } from 'react'
import IntuzValue from 'components/CareerComponents/IntuzianValues/IntuzValue'
import classes from './styles.module.scss';
import gsap from 'gsap'
const IntuzianScrollAnimation = ({intuzian_values}) => {

    useEffect(()=>{
        let tl = gsap.timeline({
            scrollTrigger:{
                trigger:'#intuzValues',
                pin:'#intuzValues',
                start:"top 10%",
                end:`+=${(385 + 300)* intuzian_values.length}`,
                // end:"bottom 10%",
                scrub:0.9
            },
            
    });
        
        tl.fromTo(`.${classes.wrapper}`, { x:1500, ease:"none" }, { x:-385*intuzian_values.length, duration:8, ease:"none" })
    }, [])

    return (
        <div className={classes.IntuzValuesScroll} style={{width:`${((385 + 100)* intuzian_values.length)}px`}}>
            <div className={classes.wrapper}>
            {intuzian_values.map(int_val=>(
                <IntuzValue key={int_val.title} {...int_val}/>
                ))}      
            </div>
        </div>
    )
}

export default IntuzianScrollAnimation
