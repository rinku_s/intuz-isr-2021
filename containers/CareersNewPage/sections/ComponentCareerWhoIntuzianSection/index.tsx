//@ts-nocheck
import React from 'react';
import Intuzians from '../../../../components/CareerComponents/Intuzians';
import Container from '../../../../components/Container/Container';
import gsap from 'gsap';
import classes from './styles.module.scss';

const ComponentCareerWhoIntuzianSection = ({ title, first_description, last_description, intuzians }) => {


    React.useEffect(()=>{
        let tl = gsap.timeline({ scrollTrigger:{
            trigger:`.${classes.ComponentCareerWhoIntuzianSection}`,
            start:'top 70%',
            scrub:1,
            end:"bottom 70%",
            once:true
        }});
    
        tl.from(`.${classes.ComponentCareerWhoIntuzianSection} h3`, { y:20, autoAlpha:0, ease:"power1.out", duration:0.5 })
          .from(`.${classes.ComponentCareerWhoIntuzianSection} .description`, { y:20, autoAlpha:0, ease:"power1.out", duration:0.5 },"-=0.3")
          .from(`.${classes.ComponentCareerWhoIntuzianSection} .intuzians`, { x:50, autoAlpha:0, ease:"power1.out", duration:0.5, stagger:0.1 })
          .from(`.${classes.ComponentCareerWhoIntuzianSection} .last-description`, { y:20, autoAlpha:0, ease:"power1.out", duration:0.5 });
      },[])

    return (
        <section className={classes.ComponentCareerWhoIntuzianSection}>
            <Container>
                <h3>{title}</h3>
                <p className="description">{first_description}</p>
                <Intuzians intuzians={intuzians}/>
                <p className="last-description">{last_description}</p>
            </Container>
        </section>
    )
}

export default ComponentCareerWhoIntuzianSection
