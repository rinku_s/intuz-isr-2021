//@ts-nocheck
import gsap from 'gsap';
import HtmlParser from 'html-react-parser';
import React from 'react';
import Container from '../../../../components/Container/Container';
import classes from './styles.module.scss';

const ComponentUiBlockquote = ({ author, quote  }) => {
    React.useEffect(()=>{
        let tl = gsap.timeline({
            scrollTrigger:{
                trigger:`.${classes.ComponentUiBlockquote}`,
                start:"top center",
                end:"bottom 70%",
                scrub: 1,
                once:true
            }
        });
        tl.fade(`.${classes.ComponentUiBlockquote} blockquote`, { duration: 0.8 })
        .fade(`.${classes.ComponentUiBlockquote} .author`, { duration: 0.4 },"-=0.4")
    },[])
    return (
        <section className={classes.ComponentUiBlockquote}>
           <Container>
               <blockquote>
                   {HtmlParser(`${quote}`)}
               </blockquote>
                <p className="author">{author}</p>
           </Container>
        </section>
    )
}

export default ComponentUiBlockquote
