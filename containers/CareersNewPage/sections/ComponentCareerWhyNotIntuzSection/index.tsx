//@ts-nocheck
import React from 'react';
import HtmlParser from 'html-react-parser';
import WhyNotPoints from '../../../../components/CareerComponents/WhyNotIntuz';
import Container from '../../../../components/Container/Container';
import classes from './styles.module.scss';
import gsap from 'gsap';

const ComponentCareerWhyNotIntuzSection = ({ title, description, last_para, why_not_points }) => {
    React.useEffect(()=>{
        let tl = gsap.timeline({ scrollTrigger:{
            trigger:`.${classes.ComponentCareerWhyNotIntuzSection}`,
            start:'top 70%',
        }});
    
        tl.from(`.${classes.ComponentCareerWhyNotIntuzSection} h3`, { y:20, autoAlpha:0, ease:"power1.out", duration:0.5 })
          .from(`.${classes.ComponentCareerWhyNotIntuzSection} .${classes.description}`, { y:20, autoAlpha:0, ease:"power1.out", duration:0.5 },"-=0.3");
      },[])
    return (
        <section className={classes.ComponentCareerWhyNotIntuzSection}>
            <Container>
                <h3>{title}</h3>
                <p className={classes.description}>{description}</p>
                <WhyNotPoints why_not_points={why_not_points}/>
                <div className={classes.last_para}>
                    {HtmlParser(`${last_para}`)}
                </div>
            </Container>
        </section>
    )
}

export default ComponentCareerWhyNotIntuzSection
