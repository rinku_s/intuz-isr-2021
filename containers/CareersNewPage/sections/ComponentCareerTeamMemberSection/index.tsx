//@ts-nocheck
import React, { useState } from 'react';
import Container from '../../../../components/Container/Container';
import Heading from '../../../../components/HeadingNunito/Heading';
import HireDeveloper from '../../../../components/HireDeveloper/HireDeveloper';
import DeveloperContext from '../../../../context/developerContext';
import classes from './styles.module.scss';

const ComponentCareerTeamMemberSection = ({ title, subtitle, developers }) => {
    const [dev, setDev] = useState();
    let data = { developers: developers.map(dev=>({ ...dev.developer }))}
    console.log(data);
    return (
        <DeveloperContext.Provider value={{dev, setDev}}>

        <section className={`${classes.ComponentCareerTeamMemberSection} pd-8`}>
            <Container>
                <Heading>{title}</Heading>
                <p>{subtitle}</p>
                <HireDeveloper data={data} disableStackIcon/>
            </Container>
        </section>
        </DeveloperContext.Provider>
    )
}

export default ComponentCareerTeamMemberSection
