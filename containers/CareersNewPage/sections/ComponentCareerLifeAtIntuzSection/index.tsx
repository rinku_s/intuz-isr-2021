//@ts-nocheck
//@ts-nocheck
import gsap from "gsap";
import React from "react";
import Heading from "../../../../components/HeadingNunito/Heading";
import { cdn } from "../../../../config/cdn";
import ImageCard from "./ImageCard";
import classes from "./styles.module.scss";
const ComponentCareerLifeAtIntuzSection = ({
  showcases,
  title,
  link_text,
  link,
  video_embed,
}) => {

  React.useEffect(() => {
    let tl = gsap.timeline({
      scrollTrigger: {
        trigger: `.${classes.ComponentCareerLifeAtIntuzSection}`,
        start: "top 60%"
      },

    });

    tl.fade(`.${classes.ComponentCareerLifeAtIntuzSection} h2`, { duration: 0.5 })
      .from(`.${classes.ComponentCareerLifeAtIntuzSection} figure`, { scale: 0.98, autoAlpha: 0, stagger: { each: 0.05, from: "center" }, duration: 0.3 });
  }, [])

  return (
    <section className={classes.ComponentCareerLifeAtIntuzSection}>
      <Heading>{title}</Heading>
      <div className={classes.ImageList}>
        {showcases.map((showcase, i) => {
          return (
            <ImageCard
              className={`grid_item__${i + 1}`}
              key={showcase.image.name}
              src={cdn(showcase.image.name)}
              link={showcase.link}
              label={showcase.link_text}
              video={showcase.video_embed}
              alt={showcase.image.name}
            />
          );
        })}
      </div>
    </section>
  );
};

export default ComponentCareerLifeAtIntuzSection;
