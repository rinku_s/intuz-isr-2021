//@ts-nocheck
//@ts-nocheck
import Modal from 'components/Modal/Modal';
import HtmlParser from 'html-react-parser';
import React, { Component } from 'react';
import classes from './styles.module.scss';
export default class ImageCard extends Component {

    state = {
        showModal: false
    }

    render() {
        return (
            <>
                <figure className={this.props.className}>
                    {this.props.label && <a href={this.props.link}>{this.props.label}</a>}
                    {this.props.video && <button onClick={() => this.setState({ showModal: true })}></button>}
                    <img
                        ref={this.imageRef}
                        src={this.props.src}
                        alt={this.props.alt}
                    />
                    <Modal show={this.state.showModal} onHide={() => this.setState({ showModal: false })}>
                        {/* <p className="modelbuttonstyle" onClick={() => this.setState({ showModal: false })}>Close</p> */}
                        {HtmlParser(`${this.props.video}`)}

                        {/* </Modal.Body> */}
                    </Modal>
                </figure>
            </>
        )
    }
}



function ModalNew({ show, children }) {
    if (true) {
        return (
            <div className={classes.Modal}>
                {children}
            </div>
        )
    }
    return (
        ""
    )
}
