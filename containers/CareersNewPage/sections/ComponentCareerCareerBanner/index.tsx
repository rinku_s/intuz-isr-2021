//@ts-nocheck
import React from "react";
import HtmlParser from "html-react-parser";
import Container from "../../../../components/Container/Container";
import { cdn } from "../../../../config/cdn";
import gsap from "gsap";
import classes from "./styles.module.scss";

const ComponentCareerCareerBanner = ({ image, description, title }) => {
  React.useEffect(() => {
    let tl = gsap.timeline({ delay: 0.5 });

    tl.from(`.${classes.ComponentCareerCareerBanner} h1`, {
      y: 20,
      autoAlpha: 0,
      ease: "power1.out",
      duration: 0.5,
    })
      .from(
        `.${classes.ComponentCareerCareerBanner} .description`,
        { y: 20, autoAlpha: 0, ease: "power1.out", duration: 0.5 },
        "-=0.2"
      )
      .from(
        `.${classes.ComponentCareerCareerBanner} img`,
        { scale: 1.2, autoAlpha: 0, ease: "power1.out", duration: 0.5 },
        "-=0.2"
      );
  }, []);

  return (
    <section
      className={`${classes.ComponentCareerCareerBanner} flex items-center`}
    >
      <Container>
        <div className="flex flex-col-reverse md:flex-row items-center justify-center">
          <div
            style={{ flexBasis: "45%" }}
            className="text-center md:text-left"
          >
            <h1 className="inline-block mt-5 mt-md-0">{title}</h1>
            <div className="description text-center md:text-left mt-3 mt-md-0">
              {HtmlParser(`${description}`)}
            </div>
          </div>
          <div style={{ flexBasis: "50%", marginLeft: "5rem" }}>
            <img className="img-fluid" src={cdn(image?.name)} alt="Careers" />
          </div>
        </div>
      </Container>
    </section>
  );
};

export default ComponentCareerCareerBanner;
