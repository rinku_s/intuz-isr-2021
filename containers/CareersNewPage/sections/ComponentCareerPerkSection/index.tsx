//@ts-nocheck
import React from 'react';
import Perks from '../../../../components/CareerComponents/Perks';
import Container from '../../../../components/Container/Container';
import Heading from '../../../../components/HeadingNunito/Heading';
import LinkButton from '../../../../components/UI/LinkButton/LinkButton';
import classes from './styles.module.scss';
import gsap from 'gsap'

const ComponentCareerPerkSection = ({ linkButton, perks, title }) => {

    React.useEffect(()=>{
        let tl = gsap.timeline({
            scrollTrigger:{
                start:"top center",
                end:"60% center",
                toggleActions:"restart reverse play reverse",
                trigger:`.${classes.ComponentCareerPerkSection}`,
                scrub:2.5,
                once:true
            }
        });

        tl.from(`.${classes.ComponentCareerPerkSection} h2`, { y:50, autoAlpha:0, ease:"power2.out", duration:0.5 })
            .from(`.perk`, { y:50,scale:0.8, autoAlpha:0, ease:"power2.out", duration:0.5, stagger:0.1 })
            .from(`.${classes.ComponentCareerPerkSection} a`, { y:50, autoAlpha:0, ease:"power2.out", duration:0.5, stagger:0.1 });
    },[])


    return (
        <section className={classes.ComponentCareerPerkSection}>
            <Container>
                <Heading>{title}</Heading>
                <Perks perks={perks}/>
                <LinkButton variation={"blueSqured"} href={linkButton.btn_link}>
                    {linkButton.btn_label}
                </LinkButton>
            </Container>
        </section>
    )
}

export default ComponentCareerPerkSection
