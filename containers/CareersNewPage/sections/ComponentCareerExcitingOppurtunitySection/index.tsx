//@ts-nocheck
//@ts-nocheck
import gsap from "gsap";
import HtmlParser, { domToReact } from "html-react-parser";
import React from "react";
import { SwiperSlide, Swiper } from "swiper/react";
import FormButton from "../../../../components/CareerComponents/ApplyForm/FormButton";
import EmployeeQuote from "../../../../components/CareerComponents/EmployeeQuote/EmployeeQuote";
import Openings from "../../../../components/CareerComponents/Openings";
import Container from "../../../../components/Container/Container";
import Heading from "../../../../components/HeadingNunito/Heading";
import classes from "./styles.module.scss";
import "swiper/swiper.min.css";
const ComponentCareerExcitingOppurtunitySection = ({
  employee_word,
  final_text,
  title,
  openings,
}) => {
  function transform(node, index) {
    if (
      node.type === "tag" &&
      node.name === "span" &&
      node.attribs.id === "formbutton"
    ) {
      return <FormButton key={index}>{"Drop us the resume"}</FormButton>;
    }
  }
  const settings = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplaySpeed: 5000,
    autoplay: true,
    lazyLoad: true,
    arrows: false,
  };

  const options = {
    replace: ({ attribs, children }) => {
      if (!attribs) {
        return;
      }

      if (attribs.id === "formbutton") {
        return <FormButton>{domToReact(children, options)}</FormButton>;
      }
    },
  };

  React.useEffect(() => {
    let tl = gsap.timeline({
      scrollTrigger: {
        trigger: `.${classes.ComponentCareerExcitingOppurtunitySection}`,
        start: "top 60%",
      },
    });

    tl.fade(`.${classes.ComponentCareerExcitingOppurtunitySection} h2`, {
      duration: 0.5,
    }).from(`.${classes.ComponentCareerExcitingOppurtunitySection} .opening`, {
      scale: 0.98,
      autoAlpha: 0,
      stagger: { each: 0.05, from: "center" },
      duration: 0.3,
    });
  }, []);

  return (
    <section className={classes.ComponentCareerExcitingOppurtunitySection}>
      <Heading>{title}</Heading>
      <div className="row p-3">
        <div className="col-12 col-md-9">
          <Openings openings={openings} />
        </div>
        {/* <div className="col-12 col-md-3 mt-5 mt-sm-0">
        <EmployeeQuote {...employee_word[0]} />
        </div> */}
        <div className="col-12 col-md-3 mt-5 mt-sm-0">
          <Swiper {...settings}>
            {employee_word.map((data, i) => {
              return (
                <SwiperSlide key={i}>
                  <EmployeeQuote {...data} />
                </SwiperSlide>
              );
            })}
          </Swiper>
        </div>
      </div>
      <Container className={classes.finalText}>
        {HtmlParser(`${final_text}`, options)}
      </Container>
    </section>
  );
};

export default ComponentCareerExcitingOppurtunitySection;
