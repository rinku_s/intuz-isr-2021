//@ts-nocheck
import React from 'react';
import Container from '../../../../components/Container/Container';
import Heading from '../../../../components/HeadingNunito/Heading';
import classes from './styles.module.scss';
import gsap from 'gsap'

const ComponentCareerRequirementSection = ({ description, title }) => {

    React.useEffect(()=>{
        let tl = gsap.timeline({
            scrollTrigger:{
                trigger:`.${classes.ComponentCareerRequirementSection}`,
                start:"top center",
                once:true,
            }
        });
        tl.fade(`.${classes.ComponentCareerRequirementSection} h2`, { duration: 0.8 })
        .fade(`.${classes.ComponentCareerRequirementSection} p`, { duration: 0.8 },"-=0.3");
    },[])


    return (
        <section className={classes.ComponentCareerRequirementSection}>
            <Container>
                <Heading>{title}</Heading>
                <p dangerouslySetInnerHTML={{__html:description}}></p>
            </Container>
        </section>
    )
}

export default ComponentCareerRequirementSection
