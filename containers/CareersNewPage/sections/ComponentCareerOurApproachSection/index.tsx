//@ts-nocheck
import React from 'react';
import HtmlParser from 'html-react-parser';
import Container from '../../../../components/Container/Container';
import Heading from '../../../../components/HeadingNunito/Heading';
import classes from './styles.module.scss';
import gsap from 'gsap';

const ComponentCareerOurApproachSection = ({ title, subtitle, description, focus_point }) => {

    React.useEffect(()=>{
        let tl = gsap.timeline({ scrollTrigger:{
            trigger:`.${classes.ComponentCareerOurApproachSection}`,
            start:'top 60%',
            once:true
            // toggleActions:"play none none reverse"
        }});
    
        tl.fade(`.${classes.ComponentCareerOurApproachSection} h2`, { duration: 0.5 })
            .fade(`.${classes.ComponentCareerOurApproachSection} .${classes.subtitle}`, { duration: 0.5 },"-=0.2")
            .fade(`.${classes.ComponentCareerOurApproachSection} .${classes.description}`, { duration: 0.5 },"-=0.1")
            .from(`.${classes.ComponentCareerOurApproachSection} .${classes.focus_point}`, { autoAlpha:0, scale:0.95, duration: 0.5 },"-=0.1")
            ;
        
      },[])

    return (
        <section className={classes.ComponentCareerOurApproachSection}>
            <Container>
                <Heading>{title}</Heading>
                <div className={classes.subtitle}>
                    <p>{subtitle}</p>
                </div>
                <div className={classes.description}>
                    <p>{description}</p>
                </div>    
                <div className={classes.focus_point}>{HtmlParser(`${focus_point}`)}</div>
            </Container>
        </section>
    )
}

export default ComponentCareerOurApproachSection
