//@ts-nocheck
import React from "react";
import ComponentCareerCareerBanner from "./sections/ComponentCareerCareerBanner";
import ComponentCareerExcitingOppurtunitySection from "./sections/ComponentCareerExcitingOppurtunitySection";
import ComponentCareerIntuzianValueSection from "./sections/ComponentCareerIntuzianValueSection";
import ComponentCareerLifeAtIntuzSection from "./sections/ComponentCareerLifeAtIntuzSection";
import ComponentCareerOurApproachSection from "./sections/ComponentCareerOurApproachSection";
import ComponentCareerPerkSection from "./sections/ComponentCareerPerkSection";
import ComponentCareerRequirementSection from "./sections/ComponentCareerRequirementSection";
import ComponentCareerTeamMemberSection from "./sections/ComponentCareerTeamMemberSection";
import ComponentCareerWhoIntuzianSection from "./sections/ComponentCareerWhoIntuzianSection";
import ComponentCareerWhyIntuzSection from "./sections/ComponentCareerWhyIntuzSection";
import ComponentCareerWhyNotIntuzSection from "./sections/ComponentCareerWhyNotIntuzSection";
import ComponentUiBlockquote from "./sections/ComponentUiBlockquote";

const CareerSliceZone = ({ content }) => {
  return (
    <>
      {content.map((c) => {
        switch (c.__typename) {
          case "ComponentCareerCareerBanner":
            return <ComponentCareerCareerBanner key={c.__typename} {...c} />;

          case "ComponentCareerPerkSection":
            return <ComponentCareerPerkSection key={c.__typename} {...c} />;

          case "ComponentUiBlockquote":
            return <ComponentUiBlockquote key={c.__typename} {...c} />;

          case "ComponentCareerWhyIntuzSection":
            return <ComponentCareerWhyIntuzSection key={c.__typename} {...c} />;

          case "ComponentCareerRequirementSection":
            return (
              <ComponentCareerRequirementSection key={c.__typename} {...c} />
            );

          case "ComponentCareerWhoIntuzianSection":
            return (
              <ComponentCareerWhoIntuzianSection key={c.__typename} {...c} />
            );

          case "ComponentCareerIntuzianValueSection":
            return (
              <ComponentCareerIntuzianValueSection key={c.__typename} {...c} />
            );

          case "ComponentCareerWhyNotIntuzSection":
            return (
              <ComponentCareerWhyNotIntuzSection key={c.__typename} {...c} />
            );

          case "ComponentCareerOurApproachSection":
            return (
              <ComponentCareerOurApproachSection key={c.__typename} {...c} />
            );

          case "ComponentCareerLifeAtIntuzSection":
            return (
              <ComponentCareerLifeAtIntuzSection key={c.__typename} {...c} />
            );

          case "ComponentCareerTeamMemberSection":
            return (
              <ComponentCareerTeamMemberSection key={c.__typename} {...c} />
            );

          case "ComponentCareerExcitingOppurtunitySection":
            return (
              <ComponentCareerExcitingOppurtunitySection
                key={c.__typename}
                {...c}
              />
            );

          default:
            return "";
        }
      })}
    </>
  );
};

export default CareerSliceZone;
