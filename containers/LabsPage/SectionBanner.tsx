//@ts-nocheck
import gsap from "gsap";
import React from "react";
import Container from "../../components/Container/Container";
import IconBlock from "../../components/IconBlock/IconBlock";
import LinkButton from "../../components/UI/LinkButton/LinkButton";
import { cdn } from "../../config/cdn";
import { Media, MediaContextProvider } from "../../config/responsiveQuery";
import windowWidth from "../../hooks/windowWidth";
import style from "./styles.module.scss";

const SectionBanner = (props) => {
  let windowSize = windowWidth();
  const variation = props.variation ? props.variation : "";

  function gotoContent(link) {
    gsap.to(window, 1, {
      scrollTo: {
        y: "#CompleteBlock_content",
        offsetY: "300",
        autoKill: false,
      },
      ease: "Power0.easeOut",
    });
  }

  return (
    <MediaContextProvider>

      <section
        className={`${style.SectionBanner}`}
        style={{ ...props.style, background: props.backcolor }}
      >
        <Container className={`${style.BannerContent} ${style[variation]}`}>
          <div className={`${style.LeftBlock}`} id={`${props.id}_content`}>
            {props.isFirst ? <h1>{props.title}</h1> : <h2>{props.title}</h2>}
            <h4>{props.description}</h4>
            <IconBlock icons={props.icons} />
            {variation ? (
              <LinkButton href={props.buttonLink} variation="blackBorder">
                {" "}
                {props.buttonLabel}{" "}
              </LinkButton>
            ) : (
              <LinkButton href={props.buttonLink} variation="whiteBorder">
                {props.buttonLabel}
              </LinkButton>
            )}
          </div>
          <div className={`${style.RightBlock}`} id={`${props.id}_image`}>
            <img
              src={cdn(
                `${props.image}?fm=png&auto=format&w=${windowSize < 768 ? "400" : "500"
                }`
              )}
              alt={props.image}
            />
          </div>
        </Container>
        {variation == "topBlock" ? (
          <Media greaterThanOrEqual="sm">
            <div className={style.bottomInfo}>
              <span>Explore More Apps</span>
              <a className={style.Arrow} onClick={gotoContent.bind(this)}>
                &nbsp;
                </a>
            </div>
          </Media>
        ) : (
          ""
        )}
      </section>

    </MediaContextProvider>
  );
};
export default SectionBanner;
