//@ts-nocheck
import React from 'react';
import Container from '../../../../components/Container/Container';
import PrimaryHeading from '../../../../components/Heading/Heading';
import Features from '../../../../components/LabsComponent/Features/Features';
import style from './styles.module.scss';
// const features=["Unique gesture driven interface", "Get rain probability directly on the forecast screen for coming days", "Easy-to-use slide-side navigation", "Temperature comparison chart for the past 3 years of a day", "Push notifications- Daily weather condition alerts"]

const FeatureSection = (props) => {
    return (
        <section className={style.FeatureSection}>
            <Container>
                <PrimaryHeading> Features </PrimaryHeading>
                <Features features={props.fetures}/>
            </Container>
        </section>
    )
}

export default FeatureSection
