//@ts-nocheck
import React from 'react';
import Container from '../../../../components/Container/Container';
import PrimaryHeading from '../../../../components/Heading/Heading';
import SecondaryHeading from '../../../../components/Heading/SecondaryHeading';
import ImageBlock from '../../../../components/ImageBlock/ImageBlock';
import classes from './styles.module.scss';

const NextGenerationSection = (props) => {


    return (
        <section className={classes.NextGenerationSection}>
            <Container>

                <PrimaryHeading className='Heading'>
                    {props.title}
                </PrimaryHeading>

                <SecondaryHeading fontSize="2.4rem" style={{ color: "#888" }} className='Subheading'>{props.description}</SecondaryHeading>
                <div className={classes.AppImages}>
                    {props.images.map((image, index) => {
                        return (

                            <div key={index}>
                                <ImageBlock key={index} src={image.src} alt={image.alt} mwidth={170} dwidth={270} />
                            </div>

                        )
                    })}
                </div>

            </Container>
        </section >
    )
}

export default NextGenerationSection
