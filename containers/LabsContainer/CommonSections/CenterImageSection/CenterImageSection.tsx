//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import Container from '../../../../components/Container/Container';
import PrimaryHeading from '../../../../components/Heading/Heading';
import SecondaryHeading from '../../../../components/Heading/SecondaryHeading';
import style from './styles.module.scss';

const CenterImageSection = (props) => {
    // setOpacity(`.Imgheading, .Imgsubheading, .Centerimg`);

    function play() {
        const tl = new gsap.timeline({ paused: true });
        tl.fromTo('.Imgheading', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" })
            .fromTo('.Imgsubheading', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5)
            .fromTo('.Centerimg', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5);
        tl.play();
    }

    return (
        <section className={`${style.CenterImage} ${style[props.variation]}`}>
            <Container>

                <PrimaryHeading className='Imgheading'>
                    {props.title}
                </PrimaryHeading>

                <SecondaryHeading fontSize="2.4rem" style={{ color: "#888" }} className='Imgsubheading'>
                    {props.description}
                </SecondaryHeading>
                <div className="text-center mt-mx Centerimg">
                    <img className="max-w-full h-auto" src={props.image} alt={props.alt} mwidth={360} dwidth={props.dwidth || 660} />
                </div>
            </Container>
        </section >
    )
}

export default CenterImageSection
