//@ts-nocheck


import gsap from 'gsap';
import React from 'react';
import LazyLoad from 'react-lazyload';
import Container from '../../../../components/Container/Container';
import PrimaryHeading from '../../../../components/Heading/Heading';
import SecondaryHeading from '../../../../components/Heading/SecondaryHeading';
import LanguageIcons from '../../../../components/LanguageIcons/LanguageIcons';
import { cdn } from '../../../../config/cdn';
import style from './styles.module.scss';


const Languages = (props) => {

    // setOpacity('.lanHeading, .lansubHeading')

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(`.lanHeading`, 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.1)
            .fromTo(`.lansubHeading`, 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.1);
    }

    return (
        <section className={style.LanguageSection}>

            <Container>
                <LazyLoad height={300} offset={300} once>
                    <img src={cdn('calvertr2_getitnow_logo.png?auto=format&fm=png')} alt='calvertr2_getitnow_logo' />
                </LazyLoad>
                <PrimaryHeading className='lanHeading'>Languages</PrimaryHeading>
                <SecondaryHeading className='lansubHeading'>Support for most popular languages around the world</SecondaryHeading>
                <LanguageIcons />
            </Container>

        </section >
    )
}

export default Languages