//@ts-nocheck


import gsap from 'gsap';
import React from 'react';
import CategoryTab from '../../../../components/CategoryTab/CategoryTab';
import Container from '../../../../components/Container/Container';
import PrimaryHeading from '../../../../components/Heading/Heading';
import SecondaryHeading from '../../../../components/Heading/SecondaryHeading';
import style from './styles.module.scss';

const Categories = (props) => {

    // setOpacity(`.catHeading, .catSubheading`);

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(`.catHeading`, 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.1)
            .fromTo(`.catSubheading`, 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.1);
    }

    return (
        <section className={style.Categories}>
            <Container>
                <PrimaryHeading className='catHeading'>Categories</PrimaryHeading>
                <SecondaryHeading className='catSubheading'>Inclusion of over 1400 UNITS in range of 52 CATEGORIES</SecondaryHeading>
                <CategoryTab />
            </Container>
        </section>
    )
}

export default Categories