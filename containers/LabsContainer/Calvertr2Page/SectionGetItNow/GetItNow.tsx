//@ts-nocheck


import React from 'react';
import LazyLoad from 'react-lazyload';
import Container from '../../../../components/Container/Container';
import { cdn } from '../../../../config/cdn';
import windowWidth from '../../../../hooks/windowWidth';
import style from './styles.module.scss';

const GetItNow = (props) => {
    let windowSize = windowWidth();
    let backImage = { background: `#d18e00 url(${cdn('calvertr2_get_it_now_bg.png?auto=format&fm=png')}${windowSize < 768 ? '&w=767' : ''}) center center no-repeat` }

    return (
        <section className={style.GetItNow} style={backImage}>
            <Container>
                <LazyLoad height={300} offset={300} once>
                    <div>
                        <img src={cdn(`calvertr2_getitnow_logo.png?auto=format&fm=png${windowSize < 768 ? '&w=200' : ''}`)} alt='calvertr2_getitnow_logo' />
                    </div>
                </LazyLoad>
                <a href='https://apps.apple.com/us/app/converter-unit-conversion/id1453497685' target='_blank'>GET IT NOW</a>
            </Container>
        </section>
    )
}

export default GetItNow
