//@ts-nocheck

import React from 'react';
import Container from '../../../../components/Container/Container';
import Content from '../../../../components/ConverterContent/Content';
import PrimaryHeading from '../../../../components/Heading/Heading';
import SecondaryHeading from '../../../../components/Heading/SecondaryHeading';
import style from './styles.module.scss';


const ConverterCalculator = (props) => {

    return (
        <section className={style.ConverterSection}>
            <Container>
                <PrimaryHeading className='calHeading'>Fastest Unit Converter & Calculator</PrimaryHeading>
                <SecondaryHeading className='calSubheading'>Calvertr2 is the comprehensive, simplest, customizable and most usable unit converter and calculator app in the market</SecondaryHeading>
                <Content />
            </Container>
        </section>
    )
}

export default ConverterCalculator