//@ts-nocheck


import gsap from 'gsap';
import React, { useEffect } from 'react';
import Container from '../../../../components/Container/Container';
import ImageBlock from '../../../../components/ImageBlock/ImageBlock';
import { cdn } from '../../../../config/cdn';
import { Media, MediaContextProvider } from "../../../../config/responsiveQuery";
import style from './styles.module.scss';


const SectionBanner = (props) => {
    const tl = new gsap.timeline({ delay: 0.5 });
    useEffect(() => {
        tl.fromTo(`.img1`, 1, { x: -20, opacity: 0, ease: "power0.ease" }, { x: 0, opacity: 1, ease: "power0.ease" }, 0.1)
            .fromTo(`.img2`, 1, { y: 20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.1)
            .fromTo(`.img3`, 1, { x: 20, opacity: 0, ease: "power0.ease" }, { x: 0, opacity: 1, ease: "power0.ease" }, 0.1);
    })
    return (
        <MediaContextProvider>
            <section className={style.Banner}>
                <Container>
                    <div className={style.TopContent}>
                        <img src={cdn('calvertr2_banner_logo.png')} alt='calvertr2_banner_logo' />
                        <h1>Calvertr2</h1>
                    </div>
                    <div className={style.BottomContent}>
                        <div>
                            <ImageBlock format="png" dwidth={525} mwidth={300} src={cdn("calvertr2_banner_img1.png")} alt="calvertr2_banner_img1" className='img1' />
                        </div>
                        <div>
                            <ImageBlock format="png" dwidth={535} mwidth={300} src={cdn("calvertr2_banner_image2.png")} alt="calvertr2_banner_image2" className='img2' />
                        </div>
                        <Media greaterThanOrEqual="sm">
                            <ImageBlock format="png" dwidth={535} mwidth={535} src={cdn("calvertr2_banner_img3.png")} alt="calvertr2_banner_img3" className='img3' />
                        </Media>
                    </div>
                </Container>
            </section>
        </MediaContextProvider>
    )
}

export default SectionBanner






