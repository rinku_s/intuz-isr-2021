//@ts-nocheck


import gsap from 'gsap';
import React from 'react';
import LazyLoad from 'react-lazyload';
import Container from '../../../../components/Container/Container';
import PrimaryHeading from '../../../../components/Heading/Heading';
import { cdn } from '../../../../config/cdn';
import style from './styles.module.scss';


const Widgets = (props) => {

    // setOpacity('.widHeading')

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(`.widHeading`, 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.1);
    }

    return (
        <section className={style.WidgetSection}>

            <Container>
                <PrimaryHeading className='widHeading'>Widgets & Apple Watch Support</PrimaryHeading>
                <div className={`row ${style.imgBlock}`}>
                    <div className='col-md-6'>
                        <LazyLoad height={300} offset={300} once>
                            <img src={cdn('widget_watchimg.png?auto=format&w=550&fm=png')} alt='widget_watchimg.png' />
                        </LazyLoad>
                    </div>
                    <div className='col-md-6'>
                        <LazyLoad height={300} offset={300} once>
                            <img src={cdn('widget_mobileimg.png?auto=format&w=500&fm=png')} alt='widget_mobileimg.png' />
                        </LazyLoad>
                    </div>
                </div>
            </Container>

        </section >
    )
}

export default Widgets