//@ts-nocheck


import gsap from 'gsap';
import React from 'react';
import Container from '../../../../components/Container/Container';
import Content from '../../../../components/FavoritesContent/Content';
import PrimaryHeading from '../../../../components/Heading/Heading';
import style from './styles.module.scss';

const Favorites = (props) => {

    // setOpacity('.favHeading');

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(`.favHeading`, 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.1);
    }

    return (
        <section className={style.FavpriteSection}>

            <Container>
                <PrimaryHeading className='favHeading'>Favorites</PrimaryHeading>
                <Content />
            </Container>

        </section >
    )
}

export default Favorites