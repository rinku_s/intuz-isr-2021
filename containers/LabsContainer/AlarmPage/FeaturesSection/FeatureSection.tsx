//@ts-nocheck

import gsap from 'gsap';
import React from 'react';
import Container from '../../../../components/Container/Container';
import PrimaryHeading from '../../../../components/Heading/Heading';
import Features from '../../../../components/LabsComponent/Features/Features';
import classes from './styles.module.scss';

const FeatureSection = (props) => {

    // setOpacity(`.Heading, .Subheading, .FeatureHeading`);

    function play() {
        let tl = gsap.timeline();
        tl.fromTo('.Heading', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5)
            .fromTo('.Subheading', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5)
            .fromTo('.FeatureHeading', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5);
    }

    return (
        <section className={classes.FeatureSection}>

            <Container>
                <PrimaryHeading className='Heading'>
                    Enjoy Beautiful Mornings
                    </PrimaryHeading>
                <p className='Subheading'>Set as many alarms as you like and select favorite background for each alarm. Experience the best wake-up experience with calming sounds. Perfect blend of form, functionality and design.</p>
                <PrimaryHeading className='FeatureHeading'>
                    Features
                    </PrimaryHeading>
                <Features features={props.fetures} />
            </Container>

        </section >
    )
}

export default FeatureSection
