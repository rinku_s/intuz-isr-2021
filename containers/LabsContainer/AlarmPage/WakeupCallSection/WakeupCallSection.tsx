//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import Container from '../../../../components/Container/Container';
import PrimaryHeading from '../../../../components/Heading/Heading';
import SecondaryHeading from '../../../../components/Heading/SecondaryHeading';
import Content from '../../../../components/WakeupCallContent/Content';
import classes from './styles.module.scss';

const WakeupCallSection = () => {

    // setOpacity(`.heading, .subheading`);

    function play() {
        let tl = gsap.timeline();
        tl.fromTo('.heading', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5)
            .fromTo('.subheading', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5);
    }

    return (
        <section className={classes.WakeupCallCenter}>

            <Container className={classes.flex}>
                <PrimaryHeading className='heading'>Never miss a wake-up call again</PrimaryHeading>
                <SecondaryHeading fontSize="2.4rem" className='subheading'>Innovative random challenges with your alarms ensure that you get off the bed.</SecondaryHeading>
                <Content />
            </Container>

        </section >
    )
}

export default WakeupCallSection
