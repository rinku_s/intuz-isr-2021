//@ts-nocheck
import gsap from 'gsap';
import React, { useEffect } from 'react';
import AppLink from '../../../../components/AppLink/AppLink';
import Container from '../../../../components/Container/Container';
import ImageBlock from '../../../../components/ImageBlock/ImageBlock';
import { cdn } from '../../../../config/cdn';
import { Media, MediaContextProvider } from "../../../../config/responsiveQuery";
import classes from './styles.module.scss';

const HeroSection = () => {
    const tl = new gsap.timeline({ delay: 0.5 });

    useEffect(() => {
        tl.fromTo(`.${classes.image}`, 1, { x: -50, opacity: 0, ease: "power0.ease" }, { x: 0, opacity: 1, ease: "power0.ease" }, 0.1)
            .fromTo(`.${classes.Content}`, 1, { x: 50, opacity: 0, ease: "power0.ease" }, { x: 0, opacity: 1, ease: "power0.ease" }, 0.1);
    })

    return (
        <MediaContextProvider>
            <section className={classes.HeroSection}>
                <Container>
                    <div className={classes.FlexContainer}>
                        <div className={`${classes.image}`}>
                            <ImageBlock format="png" dwidth={320} mwidth={160} src={cdn("alarm_baner_top_img.png")} alt="Header Phone" />
                        </div>
                        <div className={`${classes.Content}`}>
                            <ImageBlock format="png" dwidth={100} mwidth={60} src={cdn("alarm_banner_logo.png")} alt='Alarm Logo' />

                            <h1>Alarmr Lite</h1>
                            <p>Remarkably simple and easy-to-use alarm app with <b> ahost of features such as weather, news, tweet and world clock. </b> </p>
                            <Media greaterThanOrEqual="sm">
                                <AppLink align='left' link={{ android: "https://play.google.com/store/apps/details?id=com.r3app.alarmr", ios: "https://apps.apple.com/us/app/alarmr-daily-alarm-clock/id850284623" }} />
                            </Media>
                            <Media lessThan="sm">
                                <AppLink link={{ android: "https://play.google.com/store/apps/details?id=com.r3app.alarmr", ios: "https://apps.apple.com/us/app/alarmr-daily-alarm-clock/id850284623" }} />
                            </Media>
                        </div>
                    </div>
                </Container>
            </section>
        </MediaContextProvider>
    )
}


export default HeroSection
