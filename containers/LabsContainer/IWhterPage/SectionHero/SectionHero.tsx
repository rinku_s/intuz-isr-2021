//@ts-nocheck
import gsap from 'gsap';
import React, { useEffect } from 'react';
import AppLink from '../../../../components/AppLink/AppLink';
import Container from '../../../../components/Container/Container';
import ImageBlock from '../../../../components/ImageBlock/ImageBlock';
import { cdn } from '../../../../config/cdn';
import { Media, MediaContextProvider } from "../../../../config/responsiveQuery";
import classes from './styles.module.scss';

const HeroSection = () => {

    useEffect(() => {
        
        const tl = new gsap.timeline({ delay:0.5 });
        tl.from(`.${classes.image}`, 1, { x: -50, autoAlpha:0, ease:"Power0.easeOut" })
            .from(`.${classes.Content}`, 1, { x: 50, autoAlpha:0, ease:"Power0.easeOut" },"-=0.8")
    }, [])

    return (
        <MediaContextProvider>
            <section className={classes.HeroSection}>
                <Container>
                    <div className={classes.FlexContainer}>
                        <div className={classes.image}>
                            <ImageBlock format="png" dwidth={451} mwidth={251} src={cdn("iweather-banner.png")} alt="Header Phone"/>
                        </div>
                        <div className={classes.Content}>
                            <ImageBlock format="png" dwidth={78} mwidth={48} src={cdn('iweather_logoBanner.png')} alt="Wther Logo"/>
                            <h1>iWeather</h1>
                            <p>Get detailed weather conditions and <b> accurate forecast </b>  for USA and <b>  location across</b> the globe from the source you trust.</p>
                            
                            <Media greaterThanOrEqual="sm">
                                <AppLink align='left' link={{android:"https://play.google.com/store/apps/details?id=com.r3app.iweatherfree", ios:"https://apps.apple.com/us/app/iweather-forecast/id823413326"}}/>
                            </Media>
                            <Media lessThan="sm">
                                <AppLink link={{android:"https://play.google.com/store/apps/details?id=com.r3app.iweatherfree", ios:"https://apps.apple.com/us/app/iweather-forecast/id823413326"}}/>
                            </Media>    
                        </div>
                    </div>
                </Container>
            </section>
        </MediaContextProvider>
    )
}

export default HeroSection
