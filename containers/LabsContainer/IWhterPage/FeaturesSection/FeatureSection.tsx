//@ts-nocheck
import React from 'react';
import Container from '../../../../components/Container/Container';
import PrimaryHeading from '../../../../components/Heading/Heading';
import Features from '../../../../components/LabsComponent/Features/Features';
import classes from './styles.module.scss';



const FeatureSection = (props) => {
    return (
        <section className={classes.FeatureSection}>
            <Container>
                <PrimaryHeading>
                     An Everyday Weather App
                </PrimaryHeading>
                <p>
                Gives you the basics that you need to know on a daily basis such as Humidity, Wind Speed, Wind Chill, Pressure and Visibility details. For those wanting more iWeather provides radar
                </p>
                <PrimaryHeading>
                    Features
                </PrimaryHeading>
                <Features features={props.fetures}/>
            </Container>
        </section>
    )
}

export default FeatureSection
