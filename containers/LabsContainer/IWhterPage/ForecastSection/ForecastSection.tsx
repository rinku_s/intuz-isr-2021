//@ts-nocheck
import React from 'react';
import Container from '../../../../components/Container/Container';
import PrimaryHeading from '../../../../components/Heading/Heading';
import SecondaryHeading from '../../../../components/Heading/SecondaryHeading';
import ImageBlock from '../../../../components/ImageBlock/ImageBlock';
import { cdn } from '../../../../config/cdn';
import classes from './styles.module.scss';
const ForecastSection = () => {



    return (


        <section className={classes.ForecastSection}>
            <Container>
                <PrimaryHeading style={{ color: "#fff" }}>
                    Real-world look at weather forecast
                </PrimaryHeading>
                <SecondaryHeading style={{ color: "#fff" }} fontSize="2.4rem">
                    Simply scroll down and find information on next 5 days forecast. Contextual visualization based on time and temperature. Meticulous and elegant representation of current weather conditions.
                </SecondaryHeading>
                <ImageBlock mwidth={203} dwidth={603} src={cdn('forecast-image.png')} alt="forecast-image" format="png" />
            </Container>
        </section>
        // </WaypointOnce >
    )
}

export default ForecastSection
