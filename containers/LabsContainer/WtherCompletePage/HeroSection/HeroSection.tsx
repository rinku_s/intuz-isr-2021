//@ts-nocheck
import gsap from 'gsap';
import React, { useEffect } from 'react';
import AppLink from '../../../../components/AppLink/AppLink';
import Container from '../../../../components/Container/Container';
import ImageBlock from '../../../../components/ImageBlock/ImageBlock';
import { cdn } from '../../../../config/cdn';
import { Media, MediaContextProvider } from "../../../../config/responsiveQuery";
import classes from './styles.module.scss';

const HeroSection = () => {
    var backImage = { background: `url(${cdn('wthe-complete_banner.png?auto=format,compress')}) top center no-repeat` };

    const tl = new gsap.timeline({ delay: 0.5, repeat: 0 });
    useEffect(() => {
        tl.fromTo(`.left`, 1, { x: -50, opacity: 0, ease: "power0.ease" }, { x: 0, opacity: 1, ease: "power0.ease" }, 0.1)
            .fromTo(`.right`, 1, { x: 50, opacity: 0, ease: "power0.ease" }, { x: 0, opacity: 1, ease: "power0.ease" }, 0.1);
    }, [])

    const content = (
        <Container>
            <div className={classes.FlexContainer}>
                <div className={`${classes.image} left`}>
                    <ImageBlock format="png" src={cdn('wthe-complete_bannertop.png')} alt="Header Phone" mwidth={120} dwidth={320} />
                </div>

                <div className={`${classes.Content} right`}>
                    <ImageBlock format="png" src={cdn('wthe-complete_logo.png')} alt="Wther Logo" mwidth={38} dwidth={68} />
                    <h1>wther Complete</h1>
                    <p>Accurate and detailed weather information,<b> forecast, and alerts that makes planning ahead an easy task.</b></p>
                    <Media greaterThanOrEqual="sm">
                        <AppLink align='left' link={{ android: "https://play.google.com/store/apps/details?id=com.r3app.wthrcomplete&hl=en", ios: "https://apps.apple.com/us/app/wther-weather-forecast/id945337437" }} />
                    </Media>
                    <Media lessThan="sm">
                        <AppLink link={{ android: "https://play.google.com/store/apps/details?id=com.r3app.wthrcomplete&hl=en", ios: "https://apps.apple.com/us/app/wther-weather-forecast/id945337437" }} />
                    </Media>
                </div>
            </div>
        </Container>
    )
    return (
        <MediaContextProvider>
            <Media greaterThanOrEqual="sm">
                <section className={classes.HeroSection} style={backImage}>
                    {content}
                </section>
            </Media>
            <Media lessThan="sm">
                <section className={classes.HeroSection} style={{ 'background': '#0F304E' }}>
                    {content}
                </section>
            </Media>
        </MediaContextProvider>
    )
}

export default HeroSection

