//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import Container from '../../../../components/Container/Container';
import PrimaryHeading from '../../../../components/Heading/Heading';
import SecondaryHeading from '../../../../components/Heading/SecondaryHeading';
import ImageBlock from '../../../../components/ImageBlock/ImageBlock';
import { cdn } from '../../../../config/cdn';
import classes from './styles.module.scss';

const RadioCenter = () => {

    // setOpacity(`.RedioHeading, .RedioSubheading`);

    function play() {
        const tl = new gsap.timeline({ paused: true });
        tl.fromTo('.RedioHeading', 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" })
            .fromTo('.RedioSubheading', 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.1);
        tl.play()
    }

    return (
        <section className={classes.RadioCenter}>
            <Container className={classes.flex}>
                <div>

                    <PrimaryHeading style={{ color: "#fff" }} className='RedioHeading'>
                        Stay tuned to NOAA Radio
                    </PrimaryHeading>

                    <SecondaryHeading style={{ color: "#fff" }} fontSize="2.4rem" className='RedioSubheading'>
                        Flawless streaming of NOAA Radio for latest weather alerts directly on your device. Around 150 stations continuously broadcasted.
                </SecondaryHeading>
                </div>

                <div className={classes.img}>
                    <ImageBlock dwidth={585} mwidth={285} format={"png"} src={cdn('wthe-complete_radio.png')} alt="wther-complete-radio" className='radioimage' />
                </div>
            </Container>
        </section >
    )
}

export default RadioCenter
