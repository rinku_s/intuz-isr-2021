//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
import Container from '../../../../components/Container/Container';
import AppLink from '../../../../components/AppLink/AppLink';
import { Media, MediaContextProvider } from "../../../../config/responsiveQuery";
import ImageBlock from '../../../../components/ImageBlock/ImageBlock';
import { cdn } from '../../../../config/cdn';
const HeroSection = () => {
    return (
        <MediaContextProvider>
            <section className={classes.HeroSection}>
                <Container>
                    <div className={classes.FlexContainer}>
                        <div className={classes.image}>
                            <ImageBlock src={cdn("wther-phone-hd.png")} format="png" mwidth={312} dwidth={512} alt="Header Phone"/>
                        </div>
                        <div className={classes.Content}>
                            <ImageBlock src={cdn("wther-logo.png")} format="png" mwidth={38} dwidth={68} alt="Wther Logo"/>
                            <h1>Wther</h1>
                            <p>Wther provides the most accurate <b> weather forecast </b> for <b> locations across the globe.</b></p>
                            <Media greaterThanOrEqual="sm">
                                <AppLink align='left' link={{ios:"https://apps.apple.com/us/app/intuitive-weather-update/id868050791", android:"https://play.google.com/store/apps/details?id=com.r3app.wthr"}}/>
                            </Media>
                            <Media lessThan="sm">
                                <AppLink link={{ios:"https://apps.apple.com/us/app/intuitive-weather-update/id868050791", android:"https://play.google.com/store/apps/details?id=com.r3app.wthr"}}/>
                            </Media>  
                            
                        </div>
                    </div>
                </Container>
            </section>
        </MediaContextProvider>
    )
}

export default HeroSection

