//@ts-nocheck
import React from 'react'
import Container from '../../../../components/Container/Container';
import PrimaryHeading from '../../../../components/Heading/Heading';
import SecondaryHeading from '../../../../components/Heading/SecondaryHeading';
import Forecast from '../../../../components/LabsComponent/Forecast/Forecast';
import style from './styles.module.scss';
// import  
const WhetherForecast = () => {
    return (
        <section className={style.Forcast}>
            <Container>
                <PrimaryHeading>
                    Interactive weather forecast
                </PrimaryHeading>
                <SecondaryHeading color='#fff'>
                    Get the detailed weather forecast for the next seven days. Check out unique temperature comparison chart for the past 3 years for any selected day. Know what to expect.
                </SecondaryHeading>
                <Forecast/>
            </Container>
        </section>
    )
}

export default WhetherForecast
