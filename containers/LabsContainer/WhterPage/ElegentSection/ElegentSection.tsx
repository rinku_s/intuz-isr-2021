//@ts-nocheck
import React from 'react';
import SectionFlex from '../../../../components/LabsComponent/SectionFlex/SectionFlex';
import { cdn } from '../../../../config/cdn';
const ElegentSection = (props) => {
    return (
        <SectionFlex 
            heading="Elegant and inspiring interface"
            description="Exquisite dial interface for hourly forecast that you’ve never seen before. Wther combines the imaginative beauty of dial interface with the precise forecast. Gorgeous animations and elegant interface display comprehensive breakdown of weather conditions that matters at a glance. No clutter."
            image={cdn('labs-wther-elegent.png')}
            alt="Labs Wther"
            variation='spaceBlock'
            />
    )
}



export default ElegentSection
