//@ts-nocheck
import React from 'react'
import PrimaryHeading from '../../../../components/Heading/Heading';
import Container from '../../../../components/Container/Container';
import Reviews from '../../../../components/LabsComponent/Reviews/Reviews';
import style from './styles.module.scss';

const ReviewSection = () => {
    return (
        <section className={style.ReviewSection}>
            <Container>
                <PrimaryHeading>Seeing is believing. Hear from our users.</PrimaryHeading>
                <Reviews/>
            </Container>
        </section>
    )
}

export default ReviewSection
