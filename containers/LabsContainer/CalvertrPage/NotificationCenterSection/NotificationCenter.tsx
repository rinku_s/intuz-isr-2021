//@ts-nocheck
import React from 'react'
import Container from '../../../../components/Container/Container';
import PrimaryHeading from '../../../../components/Heading/Heading';
import SecondaryHeading from '../../../../components/Heading/SecondaryHeading';
import classes from './styles.module.scss'
import ImageBlock from '../../../../components/ImageBlock/ImageBlock';
import { cdn } from '../../../../config/cdn';
const NotificationCenter = () => {
    return (
        <section className={classes.NotificationCenter}>
            <Container className={classes.flex}>
                <div>
                <PrimaryHeading style={{color:"#fff"}}>
                Widget Notifications Center
                </PrimaryHeading>
                <SecondaryHeading style={{color:"#fff"}} fontSize="2.4rem">
                Get your preferred converter categories and mathematics calculations for fast access through notification center as Widget. Experience the power of widget exclusively designed for iOS 8.
                </SecondaryHeading>
                </div>

                <div className={classes.img}>
                <ImageBlock src={cdn("calvetr-notifcation.png")} alt="Calvetr Notification" mwidth={200} dwidth={400} format="png" />
                </div>
            </Container>
        </section>
    )
}

export default NotificationCenter
