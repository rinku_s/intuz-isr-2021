//@ts-nocheck
import React from 'react';
import AppLink from '../../../../components/AppLink/AppLink';
import Container from '../../../../components/Container/Container';
import ImageBlock from '../../../../components/ImageBlock/ImageBlock';
import { cdn } from '../../../../config/cdn';
import { Media, MediaContextProvider } from "../../../../config/responsiveQuery";
import classes from './styles.module.scss';


const HeroSection = () => {
    return (
        <MediaContextProvider>
            <section className={classes.HeroSection}>
                <Container>
                    <div className={classes.FlexContainer}>
                        <div className={classes.image}>
                            <ImageBlock src={cdn("calvertr-phone.png")} format={"png"} mwidth={273} dwidth={473}   alt="Header Phone" />
                            {/* <img src="https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/lab/calvertr/header_phone.png" alt=""/> */}
                        </div>
                        <div className={classes.Content}>
                            <ImageBlock src={cdn("calvetr-logo.png")} format={"png"} mwidth={38} dwidth={98}   alt="Calvetr Logo" />
                            <h1>Calvertr</h1>
                            <p>Feature rich <b> unit converter and calculator </b> to handle any conversion you could possibly imagine at one go. Enjoy the new face of <b> conversion and calculations.</b></p>
                            <Media greaterThanOrEqual="sm">
                                <AppLink align='left' link={{ios:"https://apps.apple.com/us/app/calvertr-lite/id938033085", android:"https://play.google.com/store/apps/details?id=com.r3app.convertrlite"}}/>
                            </Media>
                            <Media lessThan="sm">
                                <AppLink link={{ios:"https://apps.apple.com/us/app/calvertr-lite/id938033085", android:"https://play.google.com/store/apps/details?id=com.r3app.convertrlite"}}/>
                            </Media>                
                        </div>
                    </div>
                </Container>
            </section>
        </MediaContextProvider>
    )
}

export default HeroSection
