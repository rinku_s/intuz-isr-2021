//@ts-nocheck

import React from 'react';
import FormContainer from '../../../components/ContactUsForm/FormContainer';
import styles from './sectionform.module.scss';

const SectionForm = () => {
    return (
        <section className={styles.SectionForm}>
            <FormContainer />   
        </section>
    )
}

export default SectionForm;