//@ts-nocheck


import React, { useState } from 'react';
import Container from '../../../components/Container/Container';
import TerminologySearch from './TerminologySearch';
import Terms from './Terms';

    const Terminology = ({slug, terminologies, totalTerms}) => {

    return (
        <Container>
            <Terms terminologies={terminologies} slug={slug} totalTerms={totalTerms}/>
        </Container>
        )
}
export default Terminology
