//@ts-nocheck


import { gql, request } from 'graphql-request';
import React, { useState } from 'react';
import LinkButton from '../../../components/UI/LinkButton/LinkButton';
import { alphabetArr } from '../../../hooks/decorator';
import classes from './styles.module.scss';
import TerminologySearch from './TerminologySearch';
import InfiniteScroll from 'react-infinite-scroller';
import Loader from '../../../components/LoadingScreen/Loader';



const Terms = ({ terminologies, slug, totalTerms }) => {

    const [terms, setTerms] = useState(terminologies);
    const [loading, setLoading] = useState(false);
    const [searching, setSearching] = useState(false)
    const [disableInput, setDisableInput] = useState(false)
    let areMoreTerms = terms.length < totalTerms;

    async function loadMorePosts(){
        if(searching){
            return;
        }
        setLoading(true);
        console.log("GEtting called");
        const query = gql`
                {
                    knowledgeBases(where:{ slug:"${slug}" }){
                        terminologies(start:${terms.length}, limit:30){
                          heading
                          content
                        }
                      }
                }
                `
          const data = await request("https://strapi-prod-aws.intuz.com/graphql", query).then((data) => data);
        //   console.log(data);
          setTerms([...terms, ...data.knowledgeBases[0].terminologies]); 
          setLoading(false)
    }

    async function filterSearchTerms(e) {
        if(e.target.value.length <= 2) {
            setTerms(terminologies);
            setSearching(false)
            console.log("Not Searching");
        } else{
            console.log("Searching");
            setSearching(true);
            // setDisableInput(true)
            const query = gql`
                {
                    knowledgeBases(where:{ slug:"${slug}" }){
                        terminologies(where:{
                            heading_contains: "${e.target.value}"
                        }){
                          heading
                          content
                        }
                      }
                }
                `

          const data = await request("https://strapi-prod-aws.intuz.com/graphql", query).then(data => {
            // setDisableInput(false)
            return data
          });
        //   console.log(data);
          setTerms(data.knowledgeBases[0].terminologies); 
        }
    }
    // console.log("Terms Update", terms);
    let hasMore = areMoreTerms && !searching;
    // console.log(hasMore);
    return (
        <>
        <TerminologySearch filterTerms={filterSearchTerms} disableInput={disableInput} />
        
            <div className={classes.Content}>
                <InfiniteScroll
                    pageStart={0}
                    loadMore={()=>loadMorePosts()}
                    hasMore={hasMore}
                    loader={<div className="text-center"><Loader/></div>}
                >

                {alphabetArr(terms).map((term,i)=>(
                    <React.Fragment key={i}>
                        <h3>{term.alphabet}</h3>
                        {term.record.map((t,i)=>(
                            <React.Fragment key={i}>
                            <h2>{t.heading}</h2>
                            <div dangerouslySetInnerHTML={{__html:t.content}}></div>
                            </React.Fragment>
                        ))}
                    </React.Fragment>
                ))}

                    {/* {areMoreTerms && !searching && (
                        <LinkButton variation="blackBorder" onClick={() => loadMorePosts()} disabled={loading}>
                        {loading ? 'Loading...' : 'View More'}
                        </LinkButton>
                    )} */}
                </InfiniteScroll>
            </div>
            </>
        )
}
export default Terms
