//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
const TerminologySearch = ({ filterTerms,disableInput }) => {
    return (
        <div className={classes.TerminologySearch}>
            <p>Easy access to learn about your most favourite Mobile App Development Terminology</p>
            <div className="mx-auto" style={{maxWidth:"48rem"}}>
                <input  disabled={disableInput} type="search" name="search" id="search" placeholder="Search" onChange={(e)=>filterTerms(e)}/>
            </div>
        </div>
    )
}

export default TerminologySearch
