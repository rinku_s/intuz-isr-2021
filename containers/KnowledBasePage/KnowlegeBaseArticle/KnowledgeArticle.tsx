//@ts-nocheck
import React from 'react';
import Container from '../../../components/Container/Container';
import ResourceArticleContainer from '../../../components/ResourceArticleContainer/ResourceArticleContainer';
import { cdn } from '../../../config/cdn';

const KnowledgeArticle = (props) => {
    
    return (
        <section>
            <Container>
                {props.resources.map(resource=>(
                <ResourceArticleContainer
                    key={resource.id}
                    link={resource.slug}
                    image={cdn(resource.listing_image.name)}
                    title={resource.title}
                    description={resource.description}
                    knowledge={props.knowledge}
                />
                ))}
            </Container>
        </section>
    )
}

export default KnowledgeArticle
