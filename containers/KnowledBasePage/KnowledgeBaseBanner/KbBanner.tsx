//@ts-nocheck
import React from 'react';
import BlogBreadcrumb from '../../../components/BlogBreadcrumb/BlogBreadcrumb';
import Container from '../../../components/Container/Container';
import classes from './styles.module.scss';

const KBBanner:React.FC<any> = ({ variation, style, link, heading, children, backImage }) => {
    return (
        <section className={`${classes.HeroBlog} ${classes[variation]}`} style={{ ...style}}>
            <Container>
                <BlogBreadcrumb knowledge={true} link={link ? `/knowledge/${link}` : ''} />
                <h1>{heading}</h1>
                {children}
            </Container>
            <style jsx>
                {`
                    section{
                        background-image:linear-gradient(to bottom, rgba(0, 0, 0, 0.30), rgba(0, 0, 0, 0.40)), url(${backImage}?auto=format);
                    }

                    @media only screen and (max-width: 767px) { 
                        section{
                            background-image:linear-gradient(to bottom, rgba(0, 0, 0, 0.30), rgba(0, 0, 0, 0.40)), url(${backImage}?auto=format&w=800);
                        }
                     };
                `}
            </style>
        </section>
    )
}

export default KBBanner   
