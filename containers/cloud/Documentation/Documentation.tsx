
import Container from "components/cloud/Container/Container";
import LoadingSpinner from "components/cloud/LoadingScreen/LoadingSpinner";
import gql from "graphql-tag";
import { callAPI } from "lib/cloud/api";
import React, { useEffect, useState } from "react";
import classes from "./styles.module.scss";

const Documentation = () => {
  const [search, setSearch] = useState("");
  const [data, setData] = useState<any>();

  const query = gql`
    query($wht: JSON, $whn: JSON) {
      general: cloudDocumentationCategories(where: { name: "General" }) {
        name
        slug
        cloud_documentations(where: $wht) {
          title
          slug
        }
      }
      applicationSpecific: cloudDocumentationCategories(where: $whn) {
        name
        slug
        cloud_documentations(where: $wht) {
          title
          slug
        }
      }
    }
  `;

  

  useEffect(() => {
    if(search.length > -1) {
      (async () => {
        let allQueryVar = {
          wht: { title_contains: search },
          whn: { name_ne: "General", name_contains: search },
        };
        let tempData = await callAPI(query, allQueryVar);
        console.log(tempData);   
        setData(tempData);
      })()
    }
  }, [search])

  let a = (
    <ul className={`row justify-content-between ${classes.list}`}>
      {data?.general[0]?.cloud_documentations?.map((doc) => (
        <li key={doc.slug} className="col-sm-5">
          <a href={`/cloud/docs/${data.general.slug}/${doc.slug}`}>{doc.title}</a>
        </li>
      ))}
    </ul>
  );

  let b = (
    <div className="row">
      {data?.applicationSpecific?.map((as) => {
        if (as.cloud_documentations.length !== 0) {
          return (
            <div key={as.name} className="col-sm-6">
              <h4>{as.name}</h4>
              <ul>
                {as.cloud_documentations.map((docs) => (
                  <li key={docs.title}>
                    <a href={`/cloud/docs/${as.slug}/${docs.slug}`}>{docs.title}</a>
                  </li>
                ))}
              </ul>
            </div>
          )
        }
      }
      )}
    </div>
  )
  //}


  return (
    <section className={classes.Documentation}>
      <Container>
        <div className={classes.Search}>
          <h5>Search in our documentation</h5>
          <div className={classes.inputGroup}>
            <span>&nbsp;</span>
            <input
              value={search}
              onChange={(e)=>{
                setSearch(e.target.value)
              }}
              type="text"
              placeholder="Ex. Find WordPress credentials"
            />
          </div>
        </div>
        <div className={classes.docList}>
          <h3>General Documentation</h3>
          {a}
        </div>
        <div className={classes.docList}>
          <h3>Application Specific Documents</h3>
          {b}
        </div>
      </Container>
    </section>
  );
};

export default Documentation;
