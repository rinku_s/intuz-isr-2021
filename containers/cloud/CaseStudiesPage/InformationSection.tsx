//@ts-nocheck
import React from "react";
import Container from "components/cloud/Container/Container";
import Heading from "components/cloud/Utility/Heading";
import { cdn } from "config/cdn";
import classes from "./styles.module.scss";
import Technology from "./Technology";

const InformationSection = ({
  backColor,
  deployment_type,
  logo,
  name,
  description,
  cloud_case_study_tech_descs,
  cloud_case_study_results,
}) => {
  return (
    <section
      className={classes.InformationSection}
      style={{ backgroundColor: backColor }}
    >
      <Container>
        <Heading type="light-white">{deployment_type}</Heading>
        <div className="row justify-content-between mt-5">
          <div
            className={`col-sm-12 col-md-6 text-center md:text-left ${classes.InformationSection__AboutUs}`}
          >
            <h4>About Project</h4>
            <img
              className="lazyload img-fluid"
              data-src={cdn(logo.name)}
              alt={name}
            />
            <p className="mx-auto mx-md-0">{description}</p>
          </div>
          <div className="col-sm-12 col-md-6 text-center md:text-left mt-5 mt-md-1">
            <h4>Technology Used</h4>
            <div className="row justify-content-between items-center text-left">
              {cloud_case_study_tech_descs.map((desc, i) => (
                <div key={i} className="col-lg-6">
                  <Technology
                    image={cdn(desc.cloud_case_study_technology.icon.name)}
                    title={desc.cloud_case_study_technology.title}
                    desc={desc.description}
                  />
                </div>
              ))}
            </div>
          </div>
          <div className="col-12 text-center md:text-left mt-5 mt-md-1">
            <h4>Result</h4>
            <ul className="row justify-content-between text-left">
              {cloud_case_study_results.map((res, i) => (
                <li className="col-sm-6" key={i}>
                  {res.result}
                </li>
              ))}
            </ul>
          </div>
        </div>
      </Container>
    </section>
  );
};

export default InformationSection;
