//@ts-nocheck
import React from "react";
import classes from "./styles.module.scss";
const Technology = ({ image, desc, title }) => {
  return (
    <div className={classes.Technology}>
      <div className={`${classes.Technology__image} flex`}>
        <img className="lazyload m-auto" data-src={image} alt={title} />
      </div>
      <div className={classes.Technology__content}>
        <h5>{title}</h5>
        <p>{desc}</p>
      </div>
    </div>
  );
};

export default Technology;
