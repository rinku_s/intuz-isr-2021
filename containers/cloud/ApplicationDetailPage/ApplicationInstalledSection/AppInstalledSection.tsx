//@ts-nocheck
import React from "react";
import Container from "components/cloud/Container/Container";
import Heading from "components/cloud/Utility/Heading";
import Paragraph from "components/cloud/Utility/Paragraph";
import App from "./App";
import { cdn } from "config/cdn";
import classes from "./styles.module.scss";

const AppInstalledSection = ({ headings, paidApps, freeApps }) => {
  let app = (
    <div className="col-12">
      <div className="flex flex-wrap justify-center">
        {paidApps.map((freeApp) => (
          <App
            key={freeApp.name}
            title={freeApp.name}
            image={cdn(freeApp.image.name + "?auto=format,compress")}
          />
        ))}
      </div>
    </div>
  );
  if (freeApps.length !== 0 && paidApps.length == 0) {
    app = (
      <div className="col-12">
        <div className="flex flex-wrap justify-center">
          {freeApps.map((freeApp) => (
            <App
              key={freeApp.name}
              title={freeApp.name}
              image={cdn(freeApp.image.name + "?auto=format,compress")}
            />
          ))}
        </div>
      </div>
    );
  }

  if (freeApps.length !== 0 && paidApps.length !== 0) {
    app = (
      <>
        <div className="col-sm-5 text-center mt-5">
          <h6 style={{ color: "#08BD50" }}>Free Version</h6>
          <div className="row justify-center">
            {freeApps.map((freeApp) => (
              <div key={freeApp.name} className="col-6 col-md-4">
                <App
                  title={freeApp.name}
                  image={cdn(freeApp.image.name + "?auto=format,compress")}
                />
              </div>
            ))}
          </div>
        </div>
        <div className="col-sm-5 text-center mt-5">
          <h6 style={{ color: "#CB0821" }}>Paid Version</h6>
          <div className="row justify-center">
            {paidApps.map((paidApp) => (
              <div className="col-6 col-md-4" key={paidApp.name}>
                <App
                  title={paidApp.name}
                  image={cdn(paidApp.image.name + "?auto=format,compress")}
                />
              </div>
            ))}
          </div>
        </div>
      </>
    );
  }

  return (
    <section className="sp">
      <Container>
        <Heading type="light">{headings.main_heading}</Heading>
        <Paragraph className="text-center mt-4" type="small-dark">
          {headings.sub_heading}
        </Paragraph>
        <div className={`row justify-content-between ${classes.AppContainer}`}>
          {app}
        </div>
      </Container>
    </section>
  );
};

export default AppInstalledSection;
