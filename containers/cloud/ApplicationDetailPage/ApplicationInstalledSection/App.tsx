//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
const App = ({ title, image }) => {
    return (
        <div className={classes.App}>
            <div>
                <img src={image} alt={title}/>
            </div>
            <span>{title}</span>
        </div>
    )
}

export default App
