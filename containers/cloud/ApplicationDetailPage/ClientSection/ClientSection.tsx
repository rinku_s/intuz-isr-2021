//@ts-nocheck
import React from "react";
import Container from "components/cloud/Container/Container";
import Heading from "components/cloud/Utility/Heading";
import Paragraph from "components/cloud/Utility/Paragraph";
import { cdn } from "config/cdn";
import classes from "./styles.module.scss";
const logos = [
  {
    title: "Ares sports wear",
    classname: "col-6 col-md-2",
    logo: cdn("Ares_Sportswear_ab8c0d6068.png?auto=format,compress"),
  },
  {
    title: "Book4time",
    classname: "col-6 col-md-2",
    logo: cdn("book4time_e5f45990ca.png?auto=format,compress"),
  },
  {
    title: "Cloudminds",
    classname: "col-4 col-md-2",
    logo: cdn("Cloudminds_6b6274f1fe.png?auto=format,compress"),
  },
  {
    title: "Command Alkon",
    classname: "col-4 col-md-2",
    logo: cdn("Command_Alkon_776ae23fff.png?auto=format,compress"),
  },
  {
    title: "MDR",
    classname: "col-4 col-md-2",
    logo: cdn("MDRP_harm_32cb438c45.png?auto=format,compress"),
  },
  {
    title: "Reach",
    classname: "col-4 col-md-2",
    logo: cdn("Reach_9c31ab7017.png?auto=format,compress"),
  },
  {
    title: "Strategix",
    classname: "col-4 col-md-2",
    logo: cdn("Strategix_e1535dca60.png?auto=format,compress"),
  },
  {
    title: "Wheels Up",
    classname: "col-4 col-md-2",
    logo: cdn("wheels_up_b536a2d602.png?auto=format,compress"),
  },
  {
    title: "Yotpo",
    classname: "col-4 col-md-2",
    logo: cdn("Yotpo_1de84645c9.png?auto=format,compress"),
  },
];

const ClientSection = ({ heading, sub_heading, image }) => {
  return (
    <section className={classes.ThirdSection}>
      <Container>
        <Heading type="light">{heading}</Heading>
        <Paragraph
          type="large"
          className="text-center"
          style={{ marginTop: "1.8rem" }}
        >
          {sub_heading}
        </Paragraph>
        <div className="icons flex justify-center items-center flex-wrap">
          {image.map((logo) => (
            <div key={logo.icon.name} className={`${classes.Logo}`}>
              <img
                className="img-fluid lazyload"
                data-src={cdn(logo.icon.name)}
                alt={logo.icon.name}
              />
            </div>
          ))}
        </div>
      </Container>
      <style jsx>
        {`
          .icons {
            padding-top: 4rem;
          }
        `}
      </style>
    </section>
  );
};

export default ClientSection;
