//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
import Link from 'next/link'
const App = ({ link, name, src }) => {

    let basePath = process.env.NEXT_PUBLIC_BASE_PATH ? '/cloud' : '' 
    let href = "/stack/[app_name]";
    let strip;
    if(link.includes('cloudformation')) {
        href="/stack/[app_name]/cloudformation"
        strip=( <em>CF</em> )
    }
    if(link.includes('container')) {
        href="/stack/[app_name]/container"
        strip=( <em>CN</em> )
    }

    return (
        <div className={classes.App}>
            {strip}
            <Link href={basePath + href} as={basePath + link}>
                <a>
                    <img className="lazyload" data-src={src} alt={name} />
                </a>
            </Link>
        </div>
    )
}

export default App
