//@ts-nocheck
import Container from "components/cloud/Container/Container";
import Heading from "components/cloud/Utility/Heading";
import Paragraph from "components/cloud/Utility/Paragraph";
import { cdn } from "config/cdn";
import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.min.css";
import App from "./App";

const InterestSection = ({ headings, applications, currentLink }) => {
  let apps = applications.filter((app) => app.link !== currentLink);

  return (
    <section className={`sp`}>
      <Container>
        <Heading type="light">{headings.main_heading}</Heading>
        <Paragraph className="text-center mt-4" type="small-dark">
          {headings.sub_heading}
        </Paragraph>
        <Swiper
          breakpoints={{
            0: {
              slidesPerView: 1,
            },
            768: {
              slidesPerView: 3,
            },
            1200: {
              slidesPerView: 4,
            },
          }}
        >
          {apps.map((app) => (
            <SwiperSlide key={app.link} className="text-center">
              <App src={cdn(app.logo.name)} name={app.name} link={app.link} />
            </SwiperSlide>
          ))}
        </Swiper>
      </Container>
    </section>
  );
};

export default InterestSection;
