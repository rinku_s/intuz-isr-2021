//@ts-nocheck
import React from 'react'
import Container from 'components/cloud/Container/Container'
import Heading from 'components/cloud/Utility/Heading'
import Paragraph from 'components/cloud/Utility/Paragraph'
import Reviews from 'components/cloud/CloudAmiComponents/Review/Review'

const ReviewSection = ({ headings, reviews }) => {
    return (
        <section className="sp">
            <Container>
                <Heading type="light">{headings.main_heading}</Heading>
                <Paragraph className="text-center mt-4" type="small-dark">{headings.sub_heading}</Paragraph>
            </Container>
            <Reviews reviews={reviews} link={headings.link}/>
        </section>
    )
}

export default ReviewSection
