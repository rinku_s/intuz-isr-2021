//@ts-nocheck
import React from "react";
import Container from "components/cloud/Container/Container";
import classes from "./styles.module.scss";
const SubscriptionSection = () => {
  return (
    <section className={`sp ${classes.SubscriptionSection}`}>
      <Container>
        <div className="flex justify-center flex-col">
          <h2>Subscribe to get the application information updates</h2>
          <form>
            <div className={classes.Inputs}>
              <input
                type="email"
                placeholder="Enter Your Email Address"
                name=""
                id=""
              />
              <button type="submit">Subscribe</button>
            </div>
          </form>
        </div>
      </Container>
    </section>
  );
};

export default SubscriptionSection;
