//@ts-nocheck
import Container from 'components/cloud/Container/Container'
import Heading from 'components/cloud/Utility/Heading'
import Paragraph from 'components/cloud/Utility/Paragraph'
import Link from 'next/link'
import { useRouter } from 'next/router'
import React from 'react'
import classes from './styles.module.scss'

const OfferingSection = ({ headings, offerings }) => {
    let basePath = process.env.NEXT_PUBLIC_BASE_PATH ? '/cloud' : '';
    const router = useRouter();
    const offers = [
        {
            image: "/cloud/static/stack.svg",
            title: "Stack",
            checker: "AMI",
            as: basePath + "/stack/[app_name]",
            href: "/cloud/stack/" + router.query.app_name
        },
        {
            image: "/cloud/static/cloud-formation.svg",
            title: "CloudFormation",
            checker: "Cloud Formation",
            as: basePath + "/stack/[app_name]/cloudformation",
            href: "/cloud/stack/" + router.query.app_name + "/cloudformation"
        },
        {
            image: "/cloud/static/container.svg",
            title: "Container",
            checker: "Container",
            as: basePath + "/stack/[app_name]/container",
            href: "/cloud/stack/" + router.query.app_name + "/container"
        },
    ];


    const newoffer = offers.filter(element => {
        if (offerings.some(offer => offer.name === element.checker)) {
            return element
        }
    });
    // console.log(newoffer);


    return (
        <section style={{ padding: "8rem 0 0rem" }}>
            <Container>
                <Heading type="light">{headings.main_heading}</Heading>
                <Paragraph className="text-center mt-4" type="small-dark">{headings.sub_heading}</Paragraph>
                <ul className={classes.Offers}>
                    {newoffer.map(offer => (
                        <li key={offer.title} className={router.asPath === offer.href ? classes.active : ''}>
                            <Link href={offer.href} >
                                <a>
                                    <img className="lazyload" data-src={offer.image} alt={offer.title} />
                                    <p>{offer.title}</p>
                                </a>
                            </Link>
                        </li>
                    ))}
                </ul>
            </Container>
        </section>
    )
}

export default OfferingSection
