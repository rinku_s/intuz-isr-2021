//@ts-nocheck
import React from "react";
import HtmlParser from "react-html-parser";
import Container from "components/cloud/Container/Container";
import LinkButton from "components/cloud/UI/LinkButton/LinkButton";
import { cdn } from "config/cdn";
// import classes from './styles.module.scss';
const NewDescription = ({ description, aws_btn_label, aws_btn_link }) => {
  let awsLink = (
    <div className="text-center mr-sm-5">
      <div className="mb-4">
        <img
          style={{ maxWidth: "25rem" }}
          className="lazyload img-fluid"
          data-src={cdn("cloud-whyintuz.png")}
          alt="AWS"
        />
      </div>
      <LinkButton atype newTab href={aws_btn_link} variation="cloudBtnSmall">
        {aws_btn_label}
      </LinkButton>
    </div>
  );

  return (
    <section className={`py-4`}>
      <Container>
        <div className="detail">{HtmlParser(description)}</div>
        <div className="launch flex justify-center items-center">{awsLink}</div>
      </Container>
      <style jsx>
        {`
          .launch {
            padding-top: 4rem;
          }
        `}
      </style>
    </section>
  );
};

export default NewDescription;
