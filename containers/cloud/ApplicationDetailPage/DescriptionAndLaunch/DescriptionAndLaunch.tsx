//@ts-nocheck
import React from "react";
import Container from "components/cloud/Container/Container";
import LinkButton from "components/cloud/UI/LinkButton/LinkButton";
import HtmlParser from "react-html-parser";
import { cdn } from "config/cdn";
// import classes from './styles.module.scss';
const DescriptionAndLaunch: React.FC<any> = ({
  description,
  links,
  name,
  cf_desc,
  ami_desc,
  container_desc,
}) => {
  let googleLink = "";
  let awsLink = "";
  let azureLink = "";

  if (links) {
    if (links.google_cloud) {
      googleLink = (
        <div className="text-center mr-sm-5">
          <div className="mb-4">
            <img
              style={{ height: "6.4rem" }}
              className="lazyload img-fluid"
              data-src="/cloud/static/google-cloud.png"
              alt="Google Cloud Platform"
            />
          </div>
          <LinkButton href="" variation="cloudBtnSmall">
            {links.launch_text ? links.launch_text : "Launch " + name}
          </LinkButton>
        </div>
      );
    }
    if (links.aws) {
      awsLink = (
        <div className="text-center mr-sm-5">
          <div className="mb-4">
            <img
              style={{ maxWidth: "25rem" }}
              className="lazyload img-fluid"
              data-src={cdn("cloud-whyintuz.png")}
              alt="AWS"
            />
          </div>
          <LinkButton atype newTab href={links.aws} variation="cloudBtnSmall">
            {links.launch_text ? links.launch_text : "Launch " + name}
          </LinkButton>
        </div>
      );
    }

    if (links.azure) {
      azureLink = (
        <div className="text-center mr-sm-5">
          <div className="mb-4">
            <img
              style={{ height: "6.4rem" }}
              className="lazyload img-fluid"
              data-src="/cloud/static/azure.png"
              alt="Azure"
            />
          </div>
          <LinkButton href="" variation="cloudBtnSmall">
            {links.launch_text ? links.launch_text : "Launch " + name}
          </LinkButton>
        </div>
      );
    }
  }

  return (
    <section className={`py-4`}>
      <Container>
        <div className="detail">
          {HtmlParser(description)}
          {HtmlParser(ami_desc)}
          {HtmlParser(cf_desc)}
          {HtmlParser(container_desc)}
        </div>
        <div className="launch flex justify-center items-center">
          {googleLink}
          {awsLink}
          {azureLink}
        </div>
      </Container>
      <style jsx>
        {`
          .launch {
            padding-top: 4rem;
          }
        `}
      </style>
    </section>
  );
};

export default DescriptionAndLaunch;
