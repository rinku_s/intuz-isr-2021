//@ts-nocheck
import React from "react";
import Container from "components/cloud/Container/Container";
import Rating from "react-rating";
import LinkButton from "components/cloud/UI/LinkButton/LinkButton";
import classes from "./styles.module.scss";
import { useContext } from "react";
import PopupContext from "context/popupContext";
const AppDetailBannerSection = ({
  name,
  image,
  rating,
  category,
  version,
  last_update,
}) => {
  const { open, setOpen } = useContext(PopupContext);

  return (
    <section className={`${classes.AppDetailBannerSection}`}>
      <Container>
        <div
          className={`flex flex-col sm:flex-row justify-center items-center ${classes.AppDetailBanner}`}
        >
          <div className={classes.AppDetailBanner__left}>
            <div className={classes.AppDetailBanner__left__image}>
              <img src={image} alt={name} />
            </div>
            <div className="mt-4 text-center">
              {rating > 0 && (
                <Rating
                  initialRating={rating}
                  stop="5"
                  emptySymbol={
                    <img
                      className="mr-2"
                      src="/cloud/static/empty-star.svg"
                      alt="Empty Star"
                    />
                  }
                  fullSymbol={
                    <img
                      className="mr-2"
                      src="/cloud/static/full-star.svg"
                      alt="Full Star"
                    />
                  }
                  readonly
                />
              )}
            </div>
          </div>
          <div
            className={`${classes.AppDetailBanner__right} text-center sm:text-left my-5`}
          >
            <h1>{name}</h1>
            <p>
              Category: <span>{category}</span>
            </p>
            <div
              className="flex flex-col sm:flex-row items-center"
              style={{ margin: "1rem 0" }}
            >
              <p>
                Version: <span>{version}</span>
              </p>
              <p className={classes.update}>
                Last Updated: <span>{last_update}</span>
              </p>
            </div>
            <div className="pl-3">
              <LinkButton
                variation="cloudBtnUn"
                href="#"
                onClick={() => setOpen(true)}
              >
                Get Installation Done in 1 Hour
              </LinkButton>
            </div>
          </div>
        </div>
      </Container>
    </section>
  );
};

export default AppDetailBannerSection;
