//@ts-nocheck
import { useQuery } from '@apollo/react-hooks';
import gql from 'graphql-tag';
import React from 'react';
import Slider from 'react-slick'
import Container from 'components/cloud/Container/Container';
import PrimaryHeading from 'components/cloud/Heading/Heading';
import Spinner from 'components/cloud/UI/Spinner/Spinner';
import { cdn } from 'config/cdn';
import ClientLogo from './ClientLogo';
import classes from './styles.module.scss';

export const query = gql`

    query{
        sectionclients(limit:8){
            id
            title
            image{
              name
            }
          }
    }
`;


const TrustedSection = (props) => {
  
    const { error, loading, data } = useQuery(query);

    const setting = {
      slidesToShow: 4,
      dots:false,
      variableWidth:true,
      useCSS:true,
      className: `${classes.trusted_slider}`,
      arrows:false,
      slidesToScroll: 1,
      autoplay:true,
      centerMode:false,
      centerPadding:"30px",
        responsive: [
            {
              breakpoint: 990,
              settings: {
                slidesToShow: 4,
                slidesToScroll: 4,
              }
            },
            {
              breakpoint: 600,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
              }
            },
            {
                breakpoint: 500,
                settings: {
                  slidesToShow: 2,
                  slidesToScroll: 2,
                }
            },
          {
              breakpoint: 400,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
              }
          }
        ]
    }

    if(error) return <div>Error Occured</div>
    if(loading) return <Spinner/>


    let nonslider = (
        <div className={classes.ClientLogos}> 
          {data.sectionclients.map(sc=>(
            <ClientLogo key={sc.title} image={cdn(sc.image.name)} alt={sc.title} />
            ))}
        </div> 
    )

   let slider = (
    <Slider {...setting}>
    {/* <div className={classes.ClientLogos}> */}
        {data.sectionclients.map(sc=>(
          <ClientLogo key={sc.title} image={cdn(sc.image.name)} alt={sc.title} />
          ))}
    {/* </div> */}
    </Slider>
   )         

    return (
        <section className={classes.TrustedSection}>
            <Container>
                <PrimaryHeading font={props.f} style={{fontSize:'13px', textTransform: 'Uppercase'}}>
                    Trusted By
                </PrimaryHeading> 
                  {props.nonslider ? nonslider : slider}  
            </Container>
        </section>
    )
}


export default TrustedSection


