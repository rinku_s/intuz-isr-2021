//@ts-nocheck

import React from "react";
import Container from "components/cloud/Container/Container";
import style from "./styles.module.scss";
import { cdn } from "config/cdn";

const trusted = [
  {
    title: "AMG",
    image: cdn("cloud-trusted_amg.png"),
  },
  {
    title: "Holiday Inn",
    image: cdn("cloud-trusted_holiday-inn.png"),
  },
  {
    title: "SGC",
    image: cdn("cloud-trusted_sgc.png"),
  },
  {
    title: "JLL",
    image: cdn("cloud-trusted_jll.png"),
  },
  {
    title: "RFL",
    image: cdn("cloud-trusted_rfl.png"),
  },
];

const SectionBanner = () => {
  return (
    <section className={style.Banner}>
      <Container>
        <h1>
          Intuz <span>Cloud Applications</span>
        </h1>
        <h2>
          Click, launch & Deploy Cloud Applications &{" "}
          <br className="hidden lg:block" /> Infrastructure with Our Exclusive
          Apps.
        </h2>
        <div className={style.TrustedBy}>
          <span>TRUSTED BY</span>
          <div className={`${style.Content} flex items-center flex-wrap`}>
            {trusted.map((icon) => (
              <div key={icon.title}>
                <img
                  className="lazyload img-fluid"
                  data-src={icon.image}
                  alt={icon.title}
                />
              </div>
            ))}
          </div>
        </div>
      </Container>
    </section>
  );
};

export default SectionBanner;
