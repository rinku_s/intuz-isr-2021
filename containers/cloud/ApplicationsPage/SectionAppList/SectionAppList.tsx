//@ts-nocheck

import ApplicationSection from 'components/cloud/ApplicationSection/ApplicationSection';
import Container from 'components/cloud/Container/Container';
import Heading from 'components/cloud/Utility/Heading';
import Paragraph from 'components/cloud/Utility/Paragraph';
import React from 'react';
import classes from './styles.module.scss';

function SectionAppList() {
    return (
        <section className={classes.SectionAppList}>
            <Container>
                <Heading type="light" style={{ marginBottom: "2rem" }}>
                    Cloud Application Catalog
                 </Heading>
                <Paragraph type="small-dark" className="text-center">
                    Wide range of tailored AMIs and Cloud Formation Stack, which run seamlessly on cloud.
                </Paragraph>
            </Container>
            <ApplicationSection />
        </section>
    )
}

export default SectionAppList
