//@ts-nocheck
import React from "react";
import Container from "components/cloud/Container/Container";
import Heading from "components/cloud/Utility/Heading";
import GradientHead from "components/cloud/Utility/GradientHeading";
import classes from "../../CommonSection/WhySection/styles.module.scss";
import Paragraph from "components/cloud/Utility/Paragraph";
const GlanceSection = () => {
  return (
    <section className={classes.GlanceSection}>
      <Container>
        <Heading type="light">Intuz @ a Glance</Heading>
        <div className="row justify-center items-center mt-5">
          <div className="col-md-6">
            <GradientHead className="text-center sm:text-left" isHf isMid>
              We work as your true partner than a company
            </GradientHead>
            <Paragraph
              type="small-dark"
              className="text-left"
              style={{ paddingBottom: "3rem" }}
            >
              Since 2008, Intuz offers an exclusive range of IT enabled
              solutions. Being a Consulting Partner of AWS, we optimize the
              performance, flexibility, agility and productivity of the
              organization by providing reliable cloud services.
            </Paragraph>
            <Paragraph className="text-left" type="small-dark">
              End-to-end cloud computing solution for business management,
              collaboration, administration, automation and integration from
              world-class multi-folded AWS cloud platform.{" "}
            </Paragraph>
          </div>
          <div className="col-md-6">
            <div className="row">
              <div className="col-6 p-5">
                <h4>15+</h4>
                <h5>Years of Tech Experience</h5>
              </div>
              <div className="col-6 p-5">
                <h4>100+</h4>
                <h5>Experts Team</h5>
              </div>
              <div className="col-6 p-5">
                <h4>90%</h4>
                <h5>Client Retention</h5>
              </div>
              <div className="col-6 p-5">
                <h4>1500+</h4>
                <h5>Technology Projects</h5>
              </div>
            </div>
          </div>
        </div>
      </Container>
    </section>
  );
};

export default GlanceSection;
