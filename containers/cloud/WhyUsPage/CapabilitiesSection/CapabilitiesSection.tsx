//@ts-nocheck
import React from 'react';
import Container from 'components/cloud/Container/Container';
import Heading from 'components/cloud/Utility/Heading';
import classes from './styles.module.scss';
import { cdn } from 'config/cdn';

const data=[
    {
        image:cdn('cloud_consulting-partner.svg'),
        desc:"AWS Consulting Partner"
    },
    {
        image:cdn('cloud_cloud-professionals.svg'),
        desc:"AWS Certified Cloud Professionals"
    },
    {
        image:cdn('cloud_adopter.svg'),
        desc:"Early Adopter of AWS Updates"
    },
    {
        image:cdn('cloud_exclusive.svg'),
        desc:"Exclusive Cross-industry Experience"
    },
    {
        image:"/cloud/static/authenticated-aws-services.svg",
        desc:"Authenticated AWS Services"
    },
    {
        image:"/cloud/static/standard-security.svg",
        desc:"A High Standard Security & Compliance"
    },
    {
        image:"/cloud/static/network-connectivity.svg",
        desc:"Extensive Networking & Connectivity Experience"
    },
    {
        image:"/cloud/static/cloud-strategy.svg",
        desc:"Custom Cloud Strategy Formation"
    },
]

const CapabilitiesSection = () => {
    return (
        <section className={`${classes.CapabilitiesSection} sp`}>
            <Container>
                <Heading type="light">Our Capabilities</Heading>
                <div className="row">
                    {data.map(cap=>(
                        <div key={cap.desc} className={`col-6 col-sm-4 col-md-3 text-center ${classes.CapabilitiesSection__Content}`}>
                            <div>
                                <img className="img-fluid lazyload" data-src={cap.image} alt={cap.desc}/>
                            </div>
                            <p>{cap.desc}</p>
                        </div>
                    ))}
                </div>
            </Container>

        </section>
    )
}

export default CapabilitiesSection
