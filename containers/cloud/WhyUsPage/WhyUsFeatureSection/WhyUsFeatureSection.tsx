//@ts-nocheck
import React from 'react'
import Paragraph from 'components/cloud/Utility/Paragraph'
import Container from 'components/cloud/Container/Container'
import { cdn } from 'config/cdn'

const data = [
    {
        img:cdn('cloud_cloud-professionals.svg'),
        title: "AWS Certified Professionals"
    },
    {
        img:cdn('cloud_managed.svg'),
        title: "600+ Instances Managed"
    },
    {
        img:cdn('cloud_billings-managed.svg'),
        title: "$50,000+ AWS Billings Managed"
    },
    {
        img:cdn('cloud_application-Migration.svg'),
        title: "Expertise in Enterprise Application Migration"
    },
    {
        img:cdn('cloud_deployments.svg'),
        title: "35+ AWS Deployments"
    },
    {
        img:cdn('cloud_disaster-recovery.svg'),
        title: "Successful DR Practices"
    },
]

const WhyUsFeatureSection = () => {
    return (
        <section className={`sp`}>
            <Container>

            <Paragraph className="text-center" type="small-dark">Our AWS-Certified Cloud Professionals enable you to automate cloud monitoring, optimization, and analytics. We design tailored strategies that streamline your cloud spend and consumption to drive growth for your business.</Paragraph>
            <div className={`row`}>
                 {data.map(feature=>(
                     <div key={feature.title} style={{margin:"5rem 0"}} className="col-6 col-sm-4 text-center">
                         <div className="image">
                            <img className="img-fluid lazyload" data-src={feature.img} alt={feature.title}/>
                         </div>
                        <Paragraph type="small" style={{fontWeight:"normal", maxWidth:"80%", margin:"1rem auto"}}>{feature.title}</Paragraph>
                     </div>
                 ))}
            </div>
            </Container>
            <style jsx>
                    {`
                        .image{
                            width:6.5rem;
                            height:6.5rem;
                            display:inline-flex;
                            justify-content:center;
                            align-items:center;
                        }
                    `}
            </style>
        </section>
    )
}

export default WhyUsFeatureSection
