//@ts-nocheck
import { cdn } from "config/cdn";

export const data = [
    {
        icon:cdn('cloud_migration.svg'),
        title:"Migration",
        desc:"Experience secure and seamless migration of your application on AWS cloud without data loss.",
    },
    {
        icon:cdn('cloud_consultation.svg'),
        title:"Consultation",
        desc:"Define infallible and tailored cloud strategies with the help of our AWS certified professionals.",
    },
    {
        icon:cdn('cloud_deployment.svg'),
        title:"Deployment",
        desc:"Leverage scalable AWS cloud deployment to attain better IT management and business growth.",
    },
    {
        icon:cdn('cloud_costanalysis.svg'),
        title:"Cost Analysis",
        desc:"Inclusive guidance to manage your complex IT infrastructure on AWS cloud at optimal investment.",
    },
    {
        icon:cdn('cloud_disaster-recovery.svg'),
        title:"Disaster Recovery",
        desc:"Intuz defines disaster recovery plan for your massive application data with high security standards.",
    },
    {
        icon:cdn('cloud_dev-ops.svg'),
        title:"DevOps",
        desc:"Speed up and automate your organizational processes with the implementation of DevOps approach.",
    },
    
]