//@ts-nocheck
import React from "react";
import classes from "./styles.module.scss";
import Heading from "components/cloud/Utility/Heading";
import Paragraph from "components/cloud/Utility/Paragraph";
import Container from "components/cloud/Container/Container";
import { data } from "./data";
import { useEffect } from "react";
// import gsap from 'gsap';
// import { ScrollTrigger } from 'gsap/dist/ScrollTrigger'

// gsap.registerPlugin(ScrollTrigger)
const SecondSection = () => {
  useEffect(() => {
    // let tl = gsap.timeline({scrollTrigger:{
    //     trigger:".secondsection",
    //     start:"0 20%",
    //     end:"+=800",
    //     pin:true,
    //     scrub:2,
    //     markers:true,
    //     toggleActions:"restart pause resume pause"
    // }});
    // tl.from('.secondsection h2', { y:100, autoAlpha:0, duration:1, ease:"power0.out" })
    // .from('.secondsection > div > p',{ y:100, autoAlpha:0,duration:0.5, ease:"power0.out" }, "-=.5")
  }, []);

  return (
    <section className={`sp secondsection`}>
      <Container>
        <Heading type="light">End-to-End Cloud Experience</Heading>
        <Paragraph type="small-dark" className="text-center">
          Accelerate Enterprise Grade Connectivity, Faster Performance and
          Improved Productivity with Agile Cloud Services
        </Paragraph>
        <div className="row mt-5 items-center">
          {data.map((col) => (
            <div className="col-md-4" key={col.title}>
              <div className={classes.Content}>
                <div>
                  <img
                    className="lazyload"
                    data-src={col.icon}
                    alt={col.title}
                  />
                </div>
                <h3>{col.title}</h3>
                <p>{col.desc}</p>
              </div>
            </div>
          ))}
        </div>
      </Container>
    </section>
  );
};

export default SecondSection;
