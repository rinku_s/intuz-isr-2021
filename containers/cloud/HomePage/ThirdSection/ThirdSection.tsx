//@ts-nocheck
import React from "react";
import Container from "components/cloud/Container/Container";
import Heading from "components/cloud/Utility/Heading";
import Paragraph from "components/cloud/Utility/Paragraph";
import { cdn } from "config/cdn";
import classes from "./styles.module.scss";
const logos = [
  {
    title: "Speck Pump",
    classname: "col-6 col-md-2",
    logo: cdn("cloud_Specklogo.png?auto=format,compress&w=140"),
  },
  {
    title: "Bella Logo",
    classname: "col-4 col-md-2",
    logo: cdn("cloud_bella.png?auto=format,compress&w=80"),
  },
  {
    title: "SGC Logo",
    classname: "col-4 col-md-2",
    logo: cdn("cloud_sgc.png?auto=format,compress&w=80"),
  },
  {
    title: "Brussel Airport",
    classname: "col-6 col-md-2",
    logo: cdn("Brussels_Airport_e15b9a46ec.png?auto=format,compress&w=140"),
  },
  {
    title: "Cognizant",
    classname: "col-6 col-md-2",
    logo: cdn("Cognizant_5846a9547f.png?auto=format,compress&w=140"),
  },
  {
    title: "Ministry of education",
    classname: "col-4 col-md-2",
    logo: cdn(
      "Ministry_of_Education_17921b2b68.png?auto=format,compress&w=200"
    ),
  },
  {
    title: "UNCS",
    classname: "col-4 col-md-2",
    logo: cdn("uncs_bbcf9975a6.png?auto=format,compress&w=120"),
  },
  {
    title: "UW Tacoma",
    classname: "col-4 col-md-2",
    logo: cdn("UW_tacoma_52016c7e47.png?auto=format,compress&w=100"),
  },
  {
    title: "YALE University",
    classname: "col-4 col-md-2",
    logo: cdn("Yale_University_874be52329.png?auto=format,compress&w=120"),
  },
];

const ThirdSection = () => {
  return (
    <section className={classes.ThirdSection}>
      <Container>
        <Heading type="dark">
          Don’t Just Believe What We Say,
          <br />
          <span>Go Through What We Did</span>
        </Heading>
        <Paragraph
          type="large"
          className="text-center"
          style={{ marginTop: "1.8rem" }}
        >
          Our commitments matter a lot for us. So we deliver what we promise
          for. See all the real-time cases that ascertain the proven excellence
          of Intuz team in the realm of cloud technology and services.
        </Paragraph>
        <div className="icons flex justify-center items-center flex-wrap">
          {logos.map((logo) => (
            <div key={logo.title} className={`${classes.Logo}`}>
              <img
                className="img-fluid lazyload"
                data-src={logo.logo}
                alt={logo.title}
              />
            </div>
          ))}
        </div>
      </Container>
      <style jsx>
        {`
          .icons {
            padding-top: 4rem;
          }
        `}
      </style>
    </section>
  );
};

export default ThirdSection;
