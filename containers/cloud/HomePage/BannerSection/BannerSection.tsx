//@ts-nocheck
import React from "react";
import classes from "./styles.module.scss";
import Container from "components/cloud/Container/Container";
import { cdn } from "config/cdn";
const BannerSection = () => {
  return (
    <section className={classes.BannerSection}>
      <Container>
        <h1>
          Unify Your
          <br />
          <span>Cloud Computing Services</span>
        </h1>
        <h2>
          Intuz is an AWS certified cloud computing company delivering fast and
          cost-effective AWS managed services.
        </h2>
        <div className="flex flex-col sm:flex-row justify-center items-center">
          <a
            rel="nofollow noopener"
            href="https://aws.amazon.com/marketplace/seller-profile?id=9bd42dbf-c4c2-4322-9ca2-df1c6a4a50f9"
            target="_blank"
          >
            <img
              className="lazyload"
              data-src={cdn("cloud-banner-icon-homepage.png")}
              alt="Banner Icon"
            />
          </a>
          <a
            rel="nofollow noopener"
            href="https://aws.amazon.com/partners/find/partnerdetails/?id=001E000001TnqcgIAB"
            target="_blank"
          >
            <img
              className="lazyload"
              data-src={cdn("cloud-amazon-partner.png?auto=compress,format")}
              alt="Banner Icon"
            />
          </a>
        </div>
      </Container>
    </section>
  );
};

export default BannerSection;
