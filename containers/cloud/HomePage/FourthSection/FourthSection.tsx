//@ts-nocheck
import React from 'react';
import Container from 'components/cloud/Container/Container';
import Heading from 'components/cloud/Utility/Heading';
import Paragraph from 'components/cloud/Utility/Paragraph';
const FourthSection = () => {
    return (
        <section className={`sp`}>
            <Container>
                <Heading type="light" style={{marginBottom:"4.2rem"}}>
                    Enterprise Cloud Solutions
                </Heading>
                <Paragraph type="small-dark" className="mb-5 text-center" style={{color:"#262626", marginBottom:"6.8rem"}}>
                We enable enterprises to boost up their agility by leveraging greater accessibility, improved mobility, increased collaboration, enhanced scalability, reduced cost, deeper data insights, and more. Fuel your business growth by leveraging end-to-end cloud computing solutions of Intuz. We do highly scalable, flexible, and secure cloud applications development to enable our clients to outperform competitors.
                </Paragraph>
                <Paragraph type="small-dark" className="text-center">
                Intuz defines a customized cloud service providers approach driven by your business goals. We build technology-driven and result-oriented applications using mature processes and proven technologies.
                </Paragraph>
            </Container>
        </section>
    )
}

export default FourthSection
