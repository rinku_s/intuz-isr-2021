//@ts-nocheck
import gsap from "gsap";
import { ScrollToPlugin } from "gsap/dist/ScrollToPlugin";
import React, { useEffect } from "react";
import SVG from "react-inlinesvg";
import Scrollspy from "react-scrollspy";
import { data } from "./data";
import classes from "./styles.module.scss";
import Container from "components/cloud/Container/Container";
gsap.registerPlugin(ScrollToPlugin);

const SectionNavigation = () => {
  let section = React.useRef();
  useEffect(() => {
    // gsap.fromTo("."+classes.sticky, 0.5, { y:-20 }, { y: 0, ease:"Power0.easeIn" });

    window.addEventListener("scroll", addSticky, false);
    return () => {
      window.removeEventListener("scroll", addSticky, false);
    };
  }, []);

  function addSticky(e) {
    let sticky = section.current.offsetTop + 80;
    // console.log(window.pageYOffset);

    if (window.pageYOffset > sticky) {
      section.current.classList.add(classes.sticky);
    }
    if (window.pageYOffset < 460) {
      section.current.classList.remove(classes.sticky);
    }
  }

  const navigate = (id) => {
    gsap.to(window, {
      duration: 0.8,
      scrollTo: { y: `#${id}`, offsetY: 100 },
      ease: "Power0.out",
    });
  };

  let links = data.map((d) => d.id);
  // console.log(links);

  return (
    <section
      ref={section}
      className={`${classes.SectionNavigation} hidden d-mblock`}
    >
      <Container>
        <Scrollspy
          className={`hidden d-mflex flex-wrap justify-content-between items-center`}
          offset={-130}
          items={links}
          currentClassName={classes.active}
        >
          {data.map((d) => (
            <li key={d.id} onClick={() => navigate(d.id)}>
              <div>
                <SVG src={d.icon} alt={d.title} />
              </div>
              <p>{d.title}</p>
            </li>
          ))}
        </Scrollspy>
      </Container>
      {/* <ul className="hidden d-mflex flex-wrap justify-content-between items-center">
               {data.map(d=>(
                   <li key={d.id} onClick={()=>navigate(d.id)}>
                       <div>
                        <img src={d.icon} alt={d.title}/>
                       </div>
                        <p>{d.title}</p>
                   </li>
               ))}
            </ul> */}
    </section>
  );
};

export default SectionNavigation;
