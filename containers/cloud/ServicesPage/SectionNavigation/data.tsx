//@ts-nocheck
import { cdn } from "config/cdn";

export const data = [
    {

        id:"migration",
        icon:cdn('cloud_migration.svg'),
        title:"Migration",
    },
    {
        id:"consultant",
        icon:cdn('cloud_consultation.svg'),
        title:"Consultation",
    },
    {
        id:"deployment",
        icon:cdn('cloud_deployment.svg'),
        title:"Deployment",
    },
    {
        id:"cost-analysis",
        icon:cdn('cloud_costanalysis.svg'),
        title:"Cost Analysis",
    },
    {
        id:"disaster-recovery",
        icon:cdn('cloud_disaster-recovery.svg'),
        title:"Disaster Recovery",
    },
    {
        id:"devops",
        icon:cdn('cloud_dev-ops.svg'),
        title:"DevOps",
    },
]