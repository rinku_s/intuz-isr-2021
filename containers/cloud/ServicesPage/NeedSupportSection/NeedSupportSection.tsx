//@ts-nocheck
import React from "react";
import Container from "components/cloud/Container/Container";
import LinkButton from "components/cloud/UI/LinkButton/LinkButton";
import classes from "./styles.module.scss";
const NeedSupportSection: React.FC<any> = ({
  mainText,
  linkText,
  link,
  popupType,
  onClick,
}) => {
  let lb = popupType ? (
    <LinkButton variation="purpleBtn" onClick={onClick}>
      {linkText}
    </LinkButton>
  ) : (
    <LinkButton href={"/contactus"} variation="purpleBtn">
      {linkText}
    </LinkButton>
  );

  return (
    <section className={`${classes.NeedSupportSection}`}>
      <Container className="flex flex-wrap justify-center items-center">
        <h3>{mainText}</h3>
        {lb}
      </Container>
    </section>
  );
};

export default NeedSupportSection;
