//@ts-nocheck
import React from 'react'
import Heading from 'components/cloud/Utility/Heading'
import Container from 'components/cloud/Container/Container'
import LinkButton from 'components/cloud/UI/LinkButton/LinkButton'
import Paragraph from 'components/cloud/Utility/Paragraph'
import { cdn } from 'config/cdn'

const SectionDeployment = () => {
    return (
        <section className="sp" id="deployment">
            <Container className="text-center">
                <img className="lazyload img-fluid" data-src={cdn('cloud_deployment.svg')} alt="Deployment"/>
                <Heading className="pb-4" type="light">Cloud Deployment Services</Heading>
                <Paragraph type="small-dark">Energize your business processes by deploying next generation application on cutting-edge cloud platforms. Intuz guarantees to deliver scalable, secure and cost-effective cloud deployment. Attain greater operational efficiencies, enhanced business flexibility, and better management of remote workforce with exclusive cloud deployment services of our AWS certified professionals.</Paragraph>
            </Container>
        </section>
    )
}

export default SectionDeployment
