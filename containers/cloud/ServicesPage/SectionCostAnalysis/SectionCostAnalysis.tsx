//@ts-nocheck
import React from 'react'
import Container from 'components/cloud/Container/Container'
import Heading from 'components/cloud/Utility/Heading'
import Paragraph from 'components/cloud/Utility/Paragraph'
import CostAnalysis from 'components/cloud/ServicePageComponent/CostAnalysis'
import { cdn } from 'config/cdn'

const SectionCostAnalysis = () => {
    return (
        <section className="sp" id="cost-analysis">
            <Container className="text-center">
                <img className="lazyload img-fluid" data-src={cdn('cloud_costanalysis.svg')} alt="Cost Analysis"/>
                <Heading className="pb-4" type="light">Cost Analysis</Heading>
                <Paragraph type="small-dark">Leverage AWS cloud platform to monitor and control your cloud spend. Intuz allows organizations to track their AWS cloud usage whenever they want. Analyze and identify unused and over-used AWS resources. Exclude the unused resources, manage cloud resources smartly and optimize saving with Intuz AWS cost analysis services.</Paragraph>
                <CostAnalysis/>
            </Container>
        </section>
    )
}

export default SectionCostAnalysis
