//@ts-nocheck
import React from 'react'
import Container from 'components/cloud/Container/Container'
import Heading from 'components/cloud/Utility/Heading'
import Paragraph from 'components/cloud/Utility/Paragraph'
import DisasterRecovery from 'components/cloud/ServicePageComponent/DisasterRecovery'
import { cdn } from 'config/cdn'

const SectionDisasterRecovery = () => {
    return (
        <section className="sp" id="disaster-recovery">
            <Container className="text-center">
                <img className="lazyload img-fluid" data-src={cdn('cloud_disaster-recovery.svg')} alt="Disaster Recovery"/>
                <Heading className="pb-4" type="light">Disaster Recovery</Heading>
                <Paragraph type="small-dark">AWS holds outstanding capabilities for disaster recovery for any critical IT crashes. Being an AWS Cloud Consulting Partner, Intuz designs ingenious disaster recovery plans to omit data loss and ensure the highest level of availability for applications. We assure about the safety of data or application by following industry best security standards.</Paragraph>
                <DisasterRecovery/>
            </Container>
        </section>
    )
}

export default SectionDisasterRecovery
