//@ts-nocheck
import React from 'react'
import Container from 'components/cloud/Container/Container'
import Heading from 'components/cloud/Utility/Heading'
import Paragraph from 'components/cloud/Utility/Paragraph'
import { cdn } from 'config/cdn'

const SectionDevops = () => {
    return (
        <section className="sp" id="devops">
            <Container className="text-center">
                <img className="lazyload img-fluid" data-src={cdn('cloud_dev-ops.svg')} alt="DevOps"/>
                <Heading className="pb-4" type="light">Cloud DevOps Services</Heading>
                <Paragraph type="small-dark">DevOps is not merely a collaboration platform or a set of tools. It prioritizes amazing customer experiences and great products over cautious product development and complex processes. Intuz practices DevOps approach to leverage continuous innovation, integration, and development by managing our internal processes and integrating your internal IT teams.</Paragraph>
            </Container>
        </section>
    )
}

export default SectionDevops
