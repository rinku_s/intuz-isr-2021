//@ts-nocheck
import React from 'react'
import Heading from 'components/cloud/Utility/Heading'
import Container from 'components/cloud/Container/Container'
import LinkButton from 'components/cloud/UI/LinkButton/LinkButton'
import Paragraph from 'components/cloud/Utility/Paragraph'
import Migration from 'components/cloud/ServicePageComponent/Migration'
import { cdn } from 'config/cdn'

const SectionMigration = () => {
    // let basePath
    return (
        <section className="sp" id="migration">
            <Container className="text-center">
                <img className="lazyload img-fluid" data-src={cdn('cloud_migration.svg')} alt="Migration"/>
                <Heading className="pb-4" type="light">Cloud Migration Services</Heading>
                <Paragraph type="small-dark">Harness improved connectivity and address performance issues by moving your existing or new application on AWS cloud platform. We offer an exclusive range of frameworks, tools, processes and skilled professionals to speed up your time to value. Experience secure and seamless migration of your application, data, Infrastructure, and platform using Intuz cloud migration services.</Paragraph>
                <Migration/>
                <LinkButton clink={true} variation="cloudBtn" href="/services/aws-database-migration">Database Migration Service</LinkButton>

            </Container>
        </section>
    )
}

export default SectionMigration
