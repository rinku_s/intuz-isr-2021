//@ts-nocheck
import React from 'react'
import Heading from 'components/cloud/Utility/Heading'
import Container from 'components/cloud/Container/Container'
import LinkButton from 'components/cloud/UI/LinkButton/LinkButton'
import Paragraph from 'components/cloud/Utility/Paragraph'
import Consultant from 'components/cloud/ServicePageComponent/Consultant'
import { cdn } from 'config/cdn'

const SectionConsultant = () => {
    return (
        <section className="sp" id="consultant">
            <Container className="text-center">
                <img className="lazyload img-fluid" data-src={cdn('cloud_consultation.svg')} alt="Consultation"/>
                <Heading className="pb-4" type="light">Cloud Consultation Services</Heading>
                <Paragraph type="small-dark">Selection of the best cloud platform, architecture, technology, and services requires thorough industry insights. Intuz cloud specialists explore your business nature, identify the crux, suggest substitute strategies and propose a smart cloud solution. We help in quick adoption of cloud to drive complete transformation of your IT with reduced operational costs.</Paragraph>
                <Consultant/>
                <LinkButton variation="cloudBtn" href="/cloud/docs/aws/cloudfront">Amazon CloudFront CDN</LinkButton>
            </Container>
        </section>
    )
}

export default SectionConsultant
