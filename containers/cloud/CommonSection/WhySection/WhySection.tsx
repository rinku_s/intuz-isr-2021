//@ts-nocheck
import React from "react";
import classes from "./styles.module.scss";
import Container from "components/Container/Container";
import Heading from "components/cloud/Utility/Heading";
import { cdn } from "config/cdn";

const WhySection = () => {
  return (
    <section className={`sp ${classes.WhySection}`}>
      <Container>
        <Heading type="light">Why Intuz</Heading>
        <div className="row justify-content-between items-center text-center md:text-left mt-5">
          <div className="col-md-6">
            <img
              data-src={cdn("cloud-whyintuz.png")}
              className="lazyload img-fluid mb-4"
              alt="Why Intuz"
            />
            <ul className="text-left pl-5">
              <li>
                Intuz offers an exclusive range of Cloud solutions. Being a
                Consulting Partner of AWS, we optimize the performance,
                flexibility, agility and productivity of the organization by
                providing reliable cloud services.
              </li>
              <li>
                We have managed $50,000+ AWS Billings, 100+ Instances, 35+ AWS
                Deployments, Successful done DR Practices
              </li>
              <li>
                Reap better management, collaboration, administration,
                automation, and integration of the applications by harnessing
                world’s most secure and scalable cloud platform Amazon Web
                Services.
              </li>
            </ul>
          </div>
          <div className="col-md-6">
            <div className="grid grid-cols-2">
              <div className="p-12">
                <h4>15+</h4>
                <h5>CloudFormation Stacks</h5>
              </div>
              <div className="p-12">
                <h4>2100+</h4>
                <h5>Total Subscribers</h5>
              </div>
              <div className="p-12">
                <h4>600+</h4>
                <h5>AMI Instances</h5>
              </div>
              <div className="p-12">
                <h4>100+</h4>
                <h5>Total AMIs</h5>
              </div>
            </div>
          </div>
        </div>
      </Container>
    </section>
  );
};

export default WhySection;
