//@ts-nocheck

import React, { Component } from 'react';
import Container from '../../../components/Container/Container';
import style from './styles.module.scss';

export default class Tabs extends Component {
    render() {
        return (
            <section className = {style.Tabs}>
                <Container>
                    <div>
                        <a onClick={()=>this.props.onClick(1)} className={this.props.activeTab == 1 ? style.active : ''}>Approach</a>
                    </div>
                    <div>
                        <a onClick={()=>this.props.onClick(2)} className={this.props.activeTab == 2 ? style.active : ''}>IP Protection</a>
                    </div>
                    <div>
                        <a onClick={()=>this.props.onClick(3)} className={this.props.activeTab == 3 ? style.active : ''}>Proven Process</a>
                    </div>
                </Container>
            </section>
        )
    }
}