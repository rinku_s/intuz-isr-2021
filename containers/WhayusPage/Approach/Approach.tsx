//@ts-nocheck
import React from 'react';
import TextContent from '../SectionTextContent/TextContent';
import TopTitle from '../SectionTopTitle/TopTitle';
import Experience from './SectionExperience/Experience';
import Productivity from './SectionProductivity/Productivity';
import SystemIcon from './SectionSystemIcon/SystemIcon';



const Why = (props) => {

  return (
    <>
      <TopTitle
        title="Effective Productivity"
        variation="sectionOnly">
        <TextContent icon="quote_icon.png">
          We invent value for our clients. We are not stubborn but insightful in our approach. We are flexible, accommodative and lively. Dozens of <span> real admirers </span> can’t be wrong.
        </TextContent>
      </TopTitle>
      <Productivity>
        {props.apppage.sectiontolicongroups}
      </Productivity>
      <TopTitle
        title="It’s all about experience"
        icon="experience_icon.png"
        id="iconDetail"
        backColor="#f8f8f8">
        <Experience>
          {props.apppage.sectiontopicondetails}
        </Experience>
        <SystemIcon>
          {props.apppage.sectionbottomicongroups}
        </SystemIcon>
      </TopTitle>
      <TopTitle
        title="Impossible motivates us"
        icon="motivation_icon.png"
        backImage="whyus_impossible_bg.jpg"
        variation="maxPadding"
        id="motivation">
        <TextContent color="#fff">
          We seek groundbreaking new projects that will open doors in the market. If it hasn’t been done or been rumored impossible, we will be the first to do it.
                  </TextContent>
      </TopTitle>
      <TopTitle
        title="Usability accounted"
        icon="usability_iocn.png"
        id="usability">
        <TextContent>
          We are one of the few companies that can both produce stunning, usable designs and create complex, powerful applications. This balanced combination can help you grow your business at a faster pace.
        </TextContent>
      </TopTitle>
      <TopTitle
        title="We get it"
        icon="weget_it_icon.png"
        backColor="#f8f8f8"
        id="weget">
        <TextContent>
          While it is easier said than done, we take time to learn your project in and out. We are not bulk project company. We put in the same passion for the project as you do. This is the difference between getting the job done and creating something we have personally invested in. If you find that other companies have a hard time understanding your idea or don’t seem too enthusiastic about your project, let us end your quest to find the right company.
                  </TextContent>
      </TopTitle>

      <TopTitle
        title="Success formula, mastered"
        icon="success_formula_icon.png"
        backImage="whyus_success_bg.jpg"
        variation="maxPadding"
        id="success">
        <TextContent color="#fff">
          The secret to getting projects done on time and within budget lies heavily within our streamlined project execution process. While other companies may leave you out of the loop during a project’s development, we have carefully crafted a process that relies on client participation. We invest in our clients’ vision and work closely with them to produce an extraordinary product. We will provide practical insight on how to prevent common project execution pitfalls such as endless rounds of revisions, communication gaps, and enhancements without empirical data.
                </TextContent>
      </TopTitle>
      <TopTitle
        title="Consulting beyond technical"
        icon="consulting_icon.png"
        id="consulting">
        <TextContent>
          Our unique approach of “listen” allows us to understand your business and provide precise technical solutions coupled with business insights based on our experience with hundreds of projects and dozens of different industries. We work with you, not for or against you, to understand the expectations of the project. We hear what you don’t say and propose technological solutions you may not have thought of. We use our vast development experience to work with you one-on-one as a partner.
                  </TextContent>
      </TopTitle>

      <TopTitle
        title="Proven it, again and again"
        icon="provenit_icon.png"
        backColor="#f8f8f8"
        id="proven_it">
        <TextContent>
          Knowing how something works is not the same as knowing how to build it. Technical expertise is simply not enough when it comes to successful project execution. Business approach is essential for project’s success. It’s the difference between flying in clear skies and flying blind.
                  </TextContent>
      </TopTitle>
      <TopTitle
        title="IP protection"
        icon="protection_icon.png"
        id="protection"
        backImage="whyus_ip_protection_bg.jpg"
        variation="maxPadding">
        <TextContent color="#fff">
          We believe in transparent communication with all the stakeholders, be it client, employee or any other entity. We understand and respect privacy policy of our clients/partners and the same is clearly conveyed to the entire channel associated with client projects. Executing a mutual bilateral NDA is the first step we follow towards protecting your idea and your IP, and follow strict processes of sharing, storing, using and handling any data that client identifies as confidential.
                  </TextContent>
      </TopTitle>

      <TopTitle
        title="We love fast growth; for you, for us"
        icon="fastgrowth_icon.png"
        backColor="#f8f8f8"
        id="fastgrowth">
        <TextContent>
          We are a process oriented company. The core objective of our processes is to make sure we execute and successfully complete projects on time within budget.<br /><br />Like you, most businesses and start-ups face challenges towards achieving productivity and growth. Our aim is to empower you and your business with the right tools and support that can help you not just to sustain your business but also identify new business opportunities and have your customers stay engaged.<br /><br />Our agile development process allows start-ups and businesses to get their solution in the market ahead of time. Right from initial consultation, validating your idea and recommending tailored solution leaving competitors behind, our fast pace execution process has the flexibility to accommodate new ideas for your project as per the market demand so you can get the best value out of it when it is most needed.
                      </TextContent>
      </TopTitle>

      <TopTitle
        title="Diverse ideas and vast experience provide limitless possibilities"
        icon="diverse_ideas_icon.png"
        id="diverse">
        <TextContent>
          We have acquired and successfully developed groundbreaking and one-of-a-kind projects. Most of our projects are pioneering concepts in their respective domains. One look through our portfolio proves we can build it all. From one business realm to another and from individuals to corporations, we create new ideas irrespective of business type.<br /><br />We strive for perfection but are not a group of sticklers. We work with you to find the right way, and have fun doing it.
                  </TextContent>
      </TopTitle>
    </>
  )
}

export default Why