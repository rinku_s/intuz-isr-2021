//@ts-nocheck


import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import { cdn } from '../../../config/cdn';
import style from './styles.module.scss';

const TopTitle = (props) => {
    
    let backImage = (props.backImage ? { background:  `url(${cdn(props.backImage + '.jpg?auto=format')}) center center no-repeat`} : null)     

    const variation = props.variation ? props.variation : '';
    return (
        <section className={`${style.sectionTitle} ${style[variation]}`} style={backImage}>
            <Container>
                    {props.icon ? <img src={require(`../../../../static/Images/whyus/${props.icon}`)} /> : ''}
                    {props.title ? <PrimaryHeading>{props.title}</PrimaryHeading> : ''}
                {props.children}
            </Container>
        </section>
    )
}

export default TopTitle;