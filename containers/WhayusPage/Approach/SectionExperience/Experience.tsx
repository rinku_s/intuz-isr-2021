//@ts-nocheck

import React from 'react';
import Content from '../../../../components/ExperienceContent/Content';

const Experience = (props) => {
    if(props.children.length <= 0) {
        return null;
    } else {
        return(
            <div className = 'row'>
                 {props.children.map((data, index) => {
                    return ( <Content key={index} content = {data} /> )
                })}
            </div>
        )
    }
}

export default Experience