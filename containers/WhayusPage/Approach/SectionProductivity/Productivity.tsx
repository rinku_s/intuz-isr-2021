//@ts-nocheck


import gsap from 'gsap';
import ScrollToPlugin from 'gsap/dist/ScrollToPlugin';
import React, { Component } from "react";
import Container from '../../../../components/Container/Container';
import style from './styles.module.scss';

gsap.registerPlugin(ScrollToPlugin)
export default class Productivity extends Component {
    constructor(props) {
        super(props)
    }

    gotoSection = (link) => {
        
        return gsap.to(window, 0.1, {scrollTo:{y:link, autoKill:false}})
    }

    StickyHeader = () => {
        let offset = this.scrollheader.offsetTop;
        if(window.pageYOffset > offset) {
            this.scrollheader.classList.add(style.sticky);
        }  
        if(window.pageYOffset < 917){
            this.scrollheader.classList.remove(style.sticky);
        }
    }

    componentDidMount() {
        window.addEventListener('scroll', this.StickyHeader);
    }

    componentWillUnmount = () => {
      window.removeEventListener('scroll', this.StickyHeader);
    };

    render() {
        if(this.props.children.length <= 0) {
            return null;
        } else {
            return (
                <div ref={c=>this.scrollheader=c}>
                    <Container className={`${style.Content}`}>
                        {this.props.children.map((data, index) => {
                            return(
                                <div key={index} onClick={this.gotoSection.bind(this, data.link)}>
                                    <svg>
                                        <use xlinkHref={`/static/Images/icons/productivity.svg#` + data.svgspritid}/>
                                    </svg>
                                    <h4>{data.title}</h4>
                                </div>
                            )
                        })}
                    </Container>
                </div>
            )
        }
    }

}

