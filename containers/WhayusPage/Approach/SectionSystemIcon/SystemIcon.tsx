//@ts-nocheck

import React from 'react';
import LazyLoad from 'react-lazyload';
import Icon from '../../../../components/SystemIcon/Icon';
import style from './styles.module.scss';

const SystemIcon = (props) => {
    if(props.children.length <= 0) {
        return null;
    } else {
        return(
            <div className={`${style.SystemIcon} row`}>
                <div className='col-md-6'>
                    <LazyLoad height={300} offset={300} once>
                        <img src={"/static/Images/about/dedicated_icon.png"} alt='dedicated_icon' />
                    </LazyLoad>
                    <span className={style.titleSpan}>Dedicated</span>
                    <div className = {`row ${style.leftContent}`}>
                        {props.children.map((icon, index) => {
                            if(index <= 3) {
                                return (
                                    <div className={`col-md-6 col-sm-6 ${style.iconBlock}`} key={index}>
                                        <Icon>{icon}</Icon>
                                    </div>
                                )
                            }
                        })}
                    </div>
                    <p style={{paddingTop: '3rem'}}>for each project, regardless of the size.</p>
                </div>
                <div className='col-md-6'>
                    <LazyLoad height={300} offset={300} once>
                        <img src={"/static/Images/about/communication_icon.png"} alt='communication_icon' />
                    </LazyLoad>
                    <span className={style.titleSpan}>Communication</span>
                    <div className = 'row'>
                        {props.children.map((icon, index) => {
                            if(index >= 4) {
                                return (
                                    <div className={`col-md-4 col-sm-4 ${style.iconBlock}`} key={index}>
                                        <Icon>{icon}</Icon>
                                    </div>
                                )
                            }
                        })}
                    </div>
                    <p>Objective is to be most productive on both sides. No loose ends.</p>
                </div>
            </div>
        )
    }
}

export default SystemIcon