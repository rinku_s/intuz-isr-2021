//@ts-nocheck

import gsap from 'gsap';
import ScrollToPlugin from 'gsap/dist/ScrollToPlugin';
import React from 'react';
import TextContent from '../SectionTextContent/TextContent';
import TopTitle from '../SectionTopTitle/TopTitle';
import BusinessCharter from './SectionBusinessCharter/BusinessCharter';

const Protection = (props) => {
    gsap.registerPlugin(ScrollToPlugin);

    function scrollToSection() {
        gsap.to(window, 0.1, { scrollTo: { y: `#CharterSection`, autoKill: false } })
    }

    return (
        <>
            <TopTitle
                title="We believe businesses run on trust that builds reputation!">
                <TextContent>
                    Protecting ideas is of the essence where IP theft is accelerating each year. At INTUZ, IP protection is one of our core priority! Reasons are many why should you trust INTUZ with your Intellectual Property:
                    </TextContent>
            </TopTitle>
            <TopTitle title="Our business charter"
                backColor="#f8f8f8">
                <img src={"/static/Images/whyus/whyus_arrow_dwon.png"} alt='whyus_arrow_dwon' onClick={() => scrollToSection()} style={{ cursor: 'pointer' }} />
            </TopTitle>
            <BusinessCharter sectionbusinesscharters={props.bsdata.sectionbusinesscharters} />
        </>
    )
}

export default Protection