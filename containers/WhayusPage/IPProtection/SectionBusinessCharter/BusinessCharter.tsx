//@ts-nocheck

import React from 'react';
import ChartBanner from '../../../../components/BusinessCharter/ChartBanner';



const BusinessCharter = (props) => {
    return (
        <ChartBanner content={props.sectionbusinesscharters} />
    )
}

export default BusinessCharter