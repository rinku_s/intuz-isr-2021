//@ts-nocheck
import Link from 'next/link';
import React from 'react';
import TextContent from '../SectionTextContent/TextContent';
import TopTitle from '../SectionTopTitle/TopTitle';

const Process = (props) => {
    return (
        <>
            <TopTitle
                title="We turn incredible ideas to successful businesses with proven process"
                icon="diverse_ideas_icon.png">
                <TextContent>
                    We work with disruptive startups & enterprises as their technology partner, designing and developing need based astounding products. Following a well-defined development process, we turn a high level app concept briefs to a world class work product solution.<br /><br />
                    <Link href='/process'>
                        <a>Learn More </a>
                    </Link>
                        about our incredibly efficient and success driven process
                    </TextContent>
            </TopTitle>
            <style jsx>
                {
                    `
                    a {
                        cursor: pointer;
                        color: #684574;
                        font-weight: 600;
                    }
                    a:hover {
                        text-decoration: none;
                        color: #3bebeb;
                    }
                    
                    `
                }
            </style>
        </>
    )
}

export default Process