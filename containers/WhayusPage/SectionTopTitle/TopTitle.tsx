//@ts-nocheck


import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import { cdn } from '../../../config/cdn';
import windowWidth from '../../../hooks/windowWidth';
import style from './styles.module.scss';

const TopTitle = (props) => {
    let windowSize = windowWidth();
    let backImage = (props.backImage) ? { background: `url(${cdn(props.backImage)}?fm=pjpg&auto=format${windowSize < 768 ? '&w=990' : ''}) center center no-repeat fixed` } : null;
    let backColor = props.backColor ? { background: props.backColor } : null;
    const variation = props.variation ? props.variation : '';


    return (
        <section className={`${style.sectionTitle} ${style[variation]}`} style={backColor ? backColor : backImage} id={props.id}>
            <Container>
                {props.icon && <img src={(`/static/Images/whyus/${props.icon}`)} alt={props.icon} />}
                {props.title ? <PrimaryHeading>{props.title}</PrimaryHeading> : ''}
                {props.children}
            </Container>
        </section>
    )
}

export default TopTitle;