//@ts-nocheck

import React from "react";
import style from './styles.module.scss';

const TextContent = (props) => {
    return (
        <>
            {props.icon && <img src={(`/static/Images/about/${props.icon}`)} alt={props.icon} />}
            <p className={style.TextContent} style={{ color: props.color ? props.color : '' }}>{props.children}</p>
        </>
    )
}

export default TextContent;

