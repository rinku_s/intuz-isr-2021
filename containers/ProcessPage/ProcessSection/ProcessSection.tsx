//@ts-nocheck
import gsap from 'gsap'
import React, { useEffect, useState } from 'react'
import { Container } from 'react-bootstrap'
import { Transition } from 'react-transition-group'
import DynamicComponent from '../../../components/DynamicComponent/DynamicComponent'
import First from '../../../components/ProcessDiagram/First'
import Fourth from '../../../components/ProcessDiagram/Fourth'
import Second from '../../../components/ProcessDiagram/Second'
import Third from '../../../components/ProcessDiagram/Third'
import Processes from '../../../components/ProcessTab'
import Fixedtab from '../../../components/ProcessTab/Fixedtab'
import ScrollToPlugin from'gsap/dist/ScrollToPlugin'
import style from './styles.module.scss'


const ProcessSection = (props) => {
    gsap.registerPlugin(ScrollToPlugin);
    const fixtabref = React.useRef();
    const [current, setCurrent] = useState("first");
    const [showFixTab, setShowFixTab] = useState(false)
    const data = {
        first: First,
        second: Second,
        third: Third,
        fourth: Fourth
    }
    const navigationText = {
        first : 'Project Plan',
        second: 'Discovery',
        third: 'Design',
        fourth: 'Implement'
    }
    let prevText = '';
    let nextText = '';
    setNavigationText();

    function showFixedTab(){
        if(window.pageYOffset > 717) {
            setShowFixTab(true);
        } else{
            setShowFixTab(false);
        }
    }

    useEffect(() => {
        window.addEventListener("scroll",showFixedTab);
        return () => {
            window.removeEventListener("scroll",showFixedTab);
        };
    }, [])

    function scrollToProcess(id) {
        setCurrent(id)
        gsap.to(window, 0.1, {scrollTo:{y:`#processSection`, autoKill:false}})
    }

    function slideNext(currentId) {
        switch(currentId) {
            case 'first' : scrollToProcess('second'); break;
            case 'second' : scrollToProcess('third'); break;
            case 'third' : scrollToProcess('fourth'); break;
            default: scrollToProcess('first')
        }
    }

    function slidePrev(currentId) {
        switch(currentId) {
            case 'second' : scrollToProcess('first'); break;
            case 'third' : scrollToProcess('second'); break;
            case 'fourth' : scrollToProcess('third'); break;
            default: scrollToProcess('first')
        }
    }

    function setNavigationText() {
        switch(current) {
            case 'first' : 
                nextText = navigationText['second']
                break;
            case 'second' : 
                prevText = navigationText['first']
                nextText = navigationText['third']
                break;
            case 'third' : 
                prevText = navigationText['second']
                nextText = navigationText['fourth']
                break;
            case 'fourth' : 
                prevText = navigationText['third']
                break;
            default: 
                prevText = ''
                nextText = ''
        }
    }

    return (
        <section>
            <Transition 
                timeout={1000}
                in={showFixTab}
                mountOnEnter={true}
                unmountOnExit={true}
                onEntering={(node, appearing)=>{
                    gsap.fromTo(node, 1, {y:-20, autoAlpha:0}, {y:0, autoAlpha:1, ease:"Power2.easeOut"});
                }}
                onExiting={(node, appearing)=>{
                    gsap.to(node, 1, {y:-20, autoAlpha:0, ease:"Power2.easeOut"});
                }}
            >
                {state=> <Fixedtab active={current} setCurrent={(id)=>scrollToProcess(id)} Fixedtabref={fixtabref}/>}
            </Transition>
            <Container id="processSection">
                <Processes active={current} setCurrent={(id)=>setCurrent(id)}/>
                <DynamicComponent tag={data[current]} />
                <div className={style.navigation_arrow}> 
                    {current == 'first' ? '' : 
                        <div className={style.prev_arrow} onClick={() => slidePrev(current)}>
                            <img src={"/static/Images/l-arr.svg"} alt="Prev navigation" />
                            <div>{prevText}</div>
                        </div>}
                    {current == 'fourth' ? '' : 
                        <div className={style.next_arrow} onClick={() => slideNext(current)}>
                            <div className={style.next_arrow}>{nextText}</div>
                            <img src={"/static/Images/r-arr.svg"} alt="Next navigation" />
                        </div>
                    }
                </div>
            </Container>
            <style jsx>
                {`
                    section{
                        padding:10rem 0;
                    }
                `}
            </style>
        </section>
    )
}

export default ProcessSection

