//@ts-nocheck
import React from 'react';
import ArrowCarousel from '../../../components/UI/Carousel/ArrowCarousel';
import { cdn } from '../../../config/cdn';
import style from './styles.module.scss';
const Libraries = (props) => {
    const Libraries = [
        {
            "title" : "Push Notification",
            "image" : "push-notification-img.png",
            "text"  : "It’s a way for an app to send a message or to notify without actually opening the app. It helps in increasing engagement with the app and improves user retention rates. Use Amazon SNS or FCM with or without deep linking to send notifications in the apps."
        },
        {
            "title" : "Mobile Verification",
            "image" : "mobie-verification-img.png",
            "text"  : "Mobile Verification checks the identity of the users or new registrees based on their access to a phone number. It increases the quality of the userbase while decreasing fraud, spam and other unwanted registrations. Get components from GitHub, Twilio, Sinch or others."
        },
        {
            "title" : "Onboarding",
            "image" : "onboarding-img.png",
            "text"  : "A critical step for app success. A good onboarding screen increases the likelihood of the first-time users becoming full-time users. It includes app tutorial screens, sign-in, signup, forgot password and social integration."
        },
        {
            "title" : "Social",
            "image" : "social-img.png",
            "text"  : "Chat, create groups and communicate with one another or with businesses. Our readymade libraries will allow integration of chat-based features on your mobile application that provides extensive support to communicate via text, image, audio, and video transfer. Add this service without the complexity with Sinch, Quickblox or other custom components."
        },
        {
            "title" : "Media Management",
            "image" : "media-management-img.png",
            "text"  : "Record the audio or video or select a file and quickly truncate it as per your requirement. Integrate Intuz’s custom-designed audio and video trimmer or editing components in your next Mobile applications."
        },
        {
            "title" : "App Advertisement",
            "image" : "app-advertisements-img.png",
            "text"  : "Monetize your mobile app through in-app advertising. Ads can be displayed as a banner, interstitial, or in other ad formats. Get this component in your app with Firebase, StartApp or get a custom component developed."
        },
        {
            "title" : "App Analytics & User Engagement",
            "image" : "app-analytics-user-engagement-img.png",
            "text"  : "Analytics tool and reports help you understand the user behavior, which helps in making informed decisions leading to performance optimizations. This web service lets you store required data from mobile app to a database."
        },
        {
            "title" : "Crash Analytics",
            "image" : "crash-analytics-img.png",
            "text"  : "You don't want your app to crash, but it happens. And when it does, you'd better have fast and precise info about what went wrong and why. With Firebase, HockeyApp, Splunk Mint and other custom services, get meaningful crash reports."
        }
    ]
    
    const setting = {
        slidesToShow: 1,
        autoplaySpeed: 1000
    }

    return (
            <div className={style.LibrarySlider}>
                <ArrowCarousel settings = {setting}>
                    {Libraries.map((library, index) => (
                        
                            <div className={style.sliderBlock} key={index}>
                                <div className={style.content}>
                                    <div>
                                        <img src={cdn(library.image)} alt={library.image} />
                                    </div>
                                    <div className={style.textblock}>
                                        <h4>{library.title}</h4>
                                        <p>{library.text}</p>
                                    </div>
                                </div>
                            </div>
                        
                    ))}
                </ArrowCarousel>
            </div>
    )
}

export default Libraries

