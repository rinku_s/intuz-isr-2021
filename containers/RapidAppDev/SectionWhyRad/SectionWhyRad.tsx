//@ts-nocheck
import React from 'react';
import LazyLoad from 'react-lazyload';
import { cdn } from '../../../config/cdn';
import style from './styles.module.scss';


const SectionWhyRad = (props) => {
    return (
        <div className={`${style.RadDetail}`}>
             <div className={`${style.detailBlock}`}>
                <div>
                    <LazyLoad height={300} offset={300} once>
                        <img src={cdn('adv-rad.jpg')} alt='adv-rad' />
                    </LazyLoad>
                </div>
                <div className={style.textBlock}>
                    <h3>Why Startups choose RAD?</h3>
                    <p>We help startups grow faster, with less risk</p>
                    <ul>
                        <li>A quick go-to-the-market app development solution</li>
                        <li>Innovative mobile apps on a budget</li>
                        <li>Hands-on involvement in the development process from start to finish</li>
                        <li>Increase ROI with value-driven development</li>
                    </ul>
                </div>
            </div>
            <div className={`${style.detailBlock}`}>
                <div>
                    <LazyLoad height={300} offset={300} once>
                        <img src={cdn('advantages-model-img.png')}  alt='advantages-model-img' />
                    </LazyLoad>
                </div>
                <div className={style.textBlock}>
                    <h3>Advantages of the RAD model</h3>
                    <ul>
                        <li>Apps typically developed and delivered within weeks</li>
                        <li>Reduces the overall project risk and cost</li>
                        <li>Uses readily available tools for quicker and better app integration</li>
                        <li>Encourages user feedback throughout the development cycle</li>
                    </ul>
                </div>
            </div>
        </div>
    )
}

export default SectionWhyRad
