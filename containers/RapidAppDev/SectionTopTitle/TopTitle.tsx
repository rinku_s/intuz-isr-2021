//@ts-nocheck


import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import style from './styles.module.scss';

const TopTitle = (props) => {
    return (
        <section className={`${style.sectionTitle}`}>
            <Container>
                    {props.title ? <h2 dangerouslySetInnerHTML={{__html: props.title}}></h2> : ''}
                    {props.description ? <SecondaryHeading>{props.description}</SecondaryHeading> : ''}
                    {props.children}
            </Container>
        </section>
    )
}

export default TopTitle;