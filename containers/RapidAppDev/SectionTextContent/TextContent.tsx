//@ts-nocheck

import React from "react";
import { cdn } from '../../../config/cdn';
import style from './styles.module.scss';

const TextContent = (props) => {
    return(
            <p className={style.TextContent}>{props.children}<br />{props.image ? <img src={cdn(props.image)} alt={props.image} />: ''}</p>
    )
}

export default TextContent;

