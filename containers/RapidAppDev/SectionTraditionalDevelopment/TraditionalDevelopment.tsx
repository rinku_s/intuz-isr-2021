//@ts-nocheck

import React from 'react';
import style from './styles.module.scss';

const TraditionalDevelopment = (props) => {
    return (
        <div className={style.TraditionalDev}>
            <table className='table'>
              <thead>
                  <tr>
                      <th>Parameter</th>
                      <th>RAD</th>
                      <th>Traditional Development</th>
                  </tr>
              </thead>
              <tbody>
                  <tr>
                      <td data-th="Parameter">Application Development Process</td>
                      <td data-th="RAD">Incremental and iterative. The objective is to deliver an application quickly with complete & functional components. Implying that the stages of the development process can be revisited later.</td>
                      <td data-th="Traditional Development">It uses a linear approach to the software development process. Here, a sequence for software development is to be followed.</td>
                  </tr>
                  <tr>
                      <td data-th="Parameter">Productivity and Flexibility</td>
                      <td data-th="RAD">High due to the use of pre-defined libraries leading to faster turnaround time.</td>
                      <td data-th="Traditional Development">Visualize and define the project details before starting. Leading to customization at each stage implying high development cycle times for the project.</td>
                  </tr>
                  <tr>
                      <td data-th="Parameter">Time and Cost Estimation</td>
                      <td data-th="RAD">Short duration projects with low development and maintenance costs.</td>
                      <td data-th="Traditional Development">Medium to long duration projects with high development and maintenance costs.</td>
                  </tr>
                  <tr>
                      <td data-th="Parameter">Testing</td>
                      <td data-th="RAD">Performed at every iteration.</td>
                      <td data-th="Traditional Development">Performed after coding phase.</td>
                  </tr>
                  <tr>
                      <td data-th="Parameter">End User Interaction</td>
                      <td data-th="RAD">Extensive.</td>
                      <td data-th="Traditional Development">Involved at the beginning to gather project requirements and at the end to accept the project.</td>
                  </tr>
                  <tr>
                      <td data-th="Parameter">Predefined Elements</td>
                      <td data-th="RAD">Use of predefined, tested and ready-to-use applications, themes, etc.</td>
                      <td data-th="Traditional Development">The application and the elements are customized and designed according to the requirements of the end user. Hence, are exclusive to the project.</td>
                  </tr>
              </tbody>
            </table>
        </div>
    )
}

export default TraditionalDevelopment
