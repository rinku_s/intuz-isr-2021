//@ts-nocheck
import React from 'react';
import LazyLoad from 'react-lazyload';
import { Media, MediaContextProvider } from "../../../config/responsiveQuery";
import style from './styles.module.scss';

const RadProcess = (props) => {
    return (
        <MediaContextProvider>
        <div className = {`row ${style.RadProcess}`}>
            <div className = 'col-md-3'>
                <div className = {style.imageBlock}>
                    <div className={style.image}>
                        <LazyLoad height={300} offset={300} once>
                            <img src = {"/static/Images/rapid-app-dev/find-optimize-the-libraries.svg"} alt='find-optimize-the-libraries' />
                        </LazyLoad>
                    </div>
                    <Media greaterThanOrEqual="sm"><img src={"/static/Images/icons/arrow.svg"} alt='arrow' /></Media>
                </div>
                <span>Consult & Evaluate</span>
            </div>
            <div className = 'col-md-3'>
                <div className = {style.imageBlock}>
                    <div className={style.image}>
                        <LazyLoad height={300} offset={300} once>
                            <img src = {"/static/Images/rapid-app-dev/analysis-quick-design.svg"} alt='analysis-quick-design' />
                        </LazyLoad>
                    </div>
                    <Media greaterThanOrEqual="sm"><img src={"/static/Images/icons/arrow.svg"} alt='arrow' /></Media>
                </div>
                <span>Design</span>
            </div>
            <div className = 'col-md-3'>
                <div className = {style.imageBlock}>
                    <div className={style.image}>
                        <LazyLoad height={300} offset={300} once>
                            <img src = {"/static/Images/rapid-app-dev/development-based-on-libraries.svg"} alt='development-based-on-libraries' />
                        </LazyLoad>
                    </div>
                    <Media greaterThanOrEqual="sm"><img src={"/static/Images/icons/arrow.svg"} alt='arrow' /></Media>
                </div>
                <span>Development</span>
            </div>
            <div className = 'col-md-3'>
                <div className = {style.imageBlock}>
                    <div className={style.image}>
                        <LazyLoad height={300} offset={300} once>
                            <img src = {"/static/Images/rapid-app-dev/launch-maintenance.svg"} alt='launch-maintenance' />
                        </LazyLoad>
                    </div>
                </div>
                <span>Launch</span>
            </div>
        </div>
        </MediaContextProvider>
    )
}

export default RadProcess