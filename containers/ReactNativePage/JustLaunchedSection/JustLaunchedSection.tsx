//@ts-nocheck
import LinkButton from "components/UI/LinkButton/LinkButton";
import { cdn } from "config/cdn";
import { myLoader } from "config/image-loader";
import gsap from "gsap";
import Image from "next/image";
import React from "react";
import Container from "../../../components/Container/Container";
import JustLaunched from "../../../components/JustLaunched/JustLaunched";
import classes from "./styles.module.scss";

const JustLaunchedSection = () => {
  return (
    <section className={`relative`}>
      <Container className="md:flex gap-x-32 items-center">
        <div className="flex-1 mb-16 md:mb-0">
          <Image
            objectFit="contain"
            loader={myLoader}
            src={"featured-app-img1.png"}
            layout="responsive"
            height={300}
            width={369}
          />
        </div>
        <div className="text-center md:text-left flex-1">
          <Image
            loader={myLoader}
            src={"live4it_block_logo.png"}
            layout="fixed"
            height={49.47}
            width={96}
          />
          <h3 className="text-5xl leading-normal mb-11">
            Find & Host Sports and Events
          </h3>
          <p className="text-16 leading-loose mb-8 md:mb-24">
            Intuz created a go-to platform to explore all nearby events wherein
            organizers can list activities and users can set their holidays or
            weekends with easy filtering and search via locations, event types,
            and much more.
          </p>
          <LinkButton href="/case-studies/live4it" variation="whiteCorner">
            View Case Study
          </LinkButton>
        </div>
      </Container>
    </section>
  );
};

export default JustLaunchedSection;

/*

<style jsx>
                {`
                        section{
                            background-image: ${visible ? `url("${cdn('rn-just-launched.jpg?fm=pjpg&auto=format')}")` : ''};
                        }

                        @media only screen and (max-width: 767px){
                            section{
                                background-image: ${visible ? `url("${cdn('rn-just-launched.jpg?fm=pjpg&auto=format&w=680')}")` : ''};
                            }
                        }
                    `}
            </style>

            */
