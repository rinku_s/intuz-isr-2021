//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import Container from '../../../components/Container/Container';
import LinkButton from '../../../components/UI/LinkButton/LinkButton';
import classes from './styles.module.scss';
const CtaSection = (props) => {

    // setOpacity(`.ctaHeader, .ctaSubheader`, { opacity: 0 })

    function play() {
        const tl = new gsap.timeline({ paused: true });
        tl.fromTo(`.ctaHeader`, 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.5)
            .fromTo(`.ctaSubheader`, 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.5);
        tl.play();
    }
    return (
        <section className={classes.CtaSection}>
            <Container>

                <h3 dangerouslySetInnerHTML={{ __html: props.title }} className='ctaHeader'></h3>

                <p dangerouslySetInnerHTML={{ __html: props.description }} className='ctaSubheader'></p>
                <LinkButton variation="blackBorder" href="/partner-with-us">
                    Partner with Us
                </LinkButton>
            </Container>
        </section >
    )
}

export default CtaSection
