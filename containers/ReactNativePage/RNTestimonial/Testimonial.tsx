//@ts-nocheck
import { myLoader } from 'config/image-loader';
import Image from 'next/image';
import React from 'react';
import classes from './styles.module.scss';

const Testimonial = (props) => {
    return (
        <div className={`${classes.Testimonial} ${props.className}`}>
            <p>
                {/* <img src='https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/2017/q1.png' alt='Quote' style={{'width': '30px','marginRight': '20px'}} /> */}
                <Image src={'/1-backup-s3-intuz-site/2017/q1.png'} alt={'Quote'} loader={myLoader} layout={'fixed'} height={20} width={30} />

                {props.data.description}
                {/* <img src='https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/2017/q2.png' alt='Quote' style={{ 'width': '30px', 'marginLeft': '20px' }} /> */}
                <Image src={'/1-backup-s3-intuz-site/2017/q2.png'} alt={'Quote'} loader={myLoader} layout={'fixed'} height={20} width={30} />
            </p>

            <figure>
                {/* <ImageBlock src={cdn(props.data.clientLogo)} alt="Live4it Client" mwidth={100} dwidth={100} format="png" /> */}

                <Image src={props.data.clientLogo} alt={"Live4it Client"} loader={myLoader} layout={'fixed'} height={100} width={100} />
                <figcaption dangerouslySetInnerHTML={{ __html: props.data.clientCaption }}></figcaption>
            </figure>

        </div>
    )
}

export default Testimonial
