//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import classes from './styles.module.scss';
import Testimonial from './Testimonial';

const Testimonialsection = (props) => {

    // setOpacity(`.testHeader, .testimonialContent, .testHeader ~ p`)

    function play() {
        const tl = new gsap.timeline({ paused: true });
        tl.fromTo(`.testHeader`, 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.5)
            .fromTo(`.testHeader ~ p`, 1, { y: 20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.5)
            .fromTo(`.testimonialContent`, 1, { y: 20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.5);
        tl.play();
    }

    return (

        <section className={classes.Testimonialsection}>
            <Container>
                <PrimaryHeading font="s" className='testHeader'>Client Testimonials</PrimaryHeading>
                <SecondaryHeading font="s" fontSize="2rem">
                    {props.subtitle}
                </SecondaryHeading>
                <Testimonial data={props} className='testimonialContent' />
            </Container>
        </section>
        // </WaypointOnce >
    )
}

export default Testimonialsection
