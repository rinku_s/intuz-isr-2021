//@ts-nocheck

import React from 'react';
import Container from '../../../components/Container/Container';
import { cdn } from '../../../config/cdn';
import Image from './Image';
import style from './styles.module.scss';

const ReportNumber = (props) => {
    return(
        <section className={style.ReportSection}>
            <Container>
                <div className="row">
                    <Image 
                        src={cdn('ic_stores.png')}  
                        alt="Stores" 
                        title = "Stores"    
                        value={4}
                    />
                    <Image 
                        src={cdn('ic_millians_apps.png')}  
                        alt="Million Apps" 
                        title="Million Apps"
                        value="6+"
                    />
                    <Image 
                        src={cdn('ic_app_download.png')}  
                        alt="Billion App Downloads" 
                        title="Billion App Downloads"    
                        value="250+"
                    />                    
                    <Image 
                        src={cdn('ic_dollar_revanue.png')}  
                        alt="Billion Dollar Revenue" 
                        title="Billion Dollar Revenue"
                        value="150+"
                    />              
                </div>
            </Container>
        </section>
    )
}

export default ReportNumber;