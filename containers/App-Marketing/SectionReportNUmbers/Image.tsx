//@ts-nocheck

import React from 'react';
import LazyLoad from 'react-lazyload';
import style from './styles.module.scss';

const Image = (props) => {
    return(
        <div className={`col-md-3 col-sm-6 ${style.imgBlock}`}>
            <LazyLoad height={300} offset={300} once>
                <img src={`${props.src}?auto=format`}  alt={props.alt} />
            </LazyLoad>
            <span>{props.value}</span>    
            <p>{props.title}</p>
        </div>
    )
}

export default Image;