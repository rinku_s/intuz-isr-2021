//@ts-nocheck

import React from 'react';
import Container from '../../../components/Container/Container';
import LinkButton from '../../../components/UI/LinkButton/LinkButton';
import PrimaryHeading from '../../../components/Heading/Heading';
import style from './styles.module.scss';

const PackageNavigation = (props) => {
    return(
        <section className={style.PackageNavigation}>
            <Container>
                <PrimaryHeading>Check our App Marketing Packages</PrimaryHeading>
                <LinkButton href="/app-marketing-packages" variation="btnPackage" title="Check our App Marketing Packages">Our Packages</LinkButton>
            </Container>
        </section>
    )
}

export default PackageNavigation