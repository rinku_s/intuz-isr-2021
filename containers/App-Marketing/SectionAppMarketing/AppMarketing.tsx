//@ts-nocheck
import React from 'react';
import Container from '../../../components/Container/Container';
import Content from '../../../components/MarketingContent/Content';
import { cdn } from '../../../config/cdn';
import style from './styles.module.scss';

const Marketing = (props) => {
    return(
        <section className={style.Marketing}>
            <Container>
                <img src={cdn(`${props.topImage}?auto=format`)} alt={props.topImage} />
                <h2>{props.title}</h2>
                <Content>
                    {props.children}
                </Content>
            </Container>
        </section>
    )
}

export default Marketing;