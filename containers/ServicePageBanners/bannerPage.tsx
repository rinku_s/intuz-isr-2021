import BackgroundImage from "components/BackgroundImage/BackgroundImage";
import Container from "components/Container/Container";
import LinkButton from "components/UI/LinkButton/LinkButton";
import HtmlParser from "html-react-parser";
import React from "react";
import classes from "./styles.module.scss";
interface BannerProps {
  title: string;
  subTitle?: string;
  description?: string;
  link?: string;
  buttonLabel?: string;
  backImage: string;
  headingClass?: string;
}

const NewBanner: React.FC<BannerProps> = (props) => {
  return (
    <>
      <section className="relative h-[700px] ">
        <BackgroundImage
          objectPosition="top center"
          alt={props.title}
          backImage={props.backImage}
        />
        <Container
          className="flex text-center md:text-left items-center h-full"
          style={{ zIndex: 10 }}
        >
          <div className="mt-16">
            {props.title && (
              <h1
                className={`text-[5rem] font-semibold text-white leading-normal ${
                  props.headingClass ? props.headingClass : "md:max-w-[65%]"
                }`}
              >
                {HtmlParser(`${props.title}`)}
              </h1>
            )}
            {props.subTitle && (
              <h3 className="text-5xl font-light leading-normal text-white md:max-w-60">
                {props.subTitle}
              </h3>
            )}
            {props.description && (
              <div
                className={`${classes.list} text-4xl my-10 font-light leading-normal text-white md:max-w-60`}
              >
                {HtmlParser(`${props.description}`)}
              </div>
            )}
            {props.link && (
              <LinkButton
                className="my-10"
                variation="bannerBtn"
                href={props.link}
              >
                {props.buttonLabel}
              </LinkButton>
            )}
          </div>
        </Container>
      </section>
    </>
  );
};

export default NewBanner;
