
import { myLoader } from 'config/image-loader';
import Image from 'next/image';
import React from 'react';
import Container from '../../components/Container/Container';
import CustomApp from '../../components/CustomAppBannner/CustomAppBanner';
import Blocks from '../../components/HireDDComponents/Hero/Blocks/Blocks';
import LinkButtons from '../../components/HireDDComponents/Hero/LinkButtons';
import ServicePageContent from '../../components/ServicePageContent/ServicePageContent';
import LinkButton from '../../components/UI/LinkButton/LinkButton';
import windowWidth from '../../hooks/windowWidth';
import style from './styles.module.scss';

const ServiceBanner = (props) => {
    let windowSize = windowWidth();
    let backImage = { minHeight: `${props.variation == "contactus" ? '60vh' : ''}` }

    return (
        <>
            <section style={backImage} className={style.SectionBanner}>
                <div className="absolute h-full w-full" style={{ zIndex: -1 }}>
                    <Image objectPosition={'top center'} className="z-0" layout="fill" objectFit="cover" loader={myLoader} src={props.content.backImage.name} alt='aibanner' />
                </div>
                <Container className={`${style.Content} ${props.variation == 'customApp' ? '' : style.ServiceContent} ${style[props.variation]}`}>
                    {props.variation == 'customApp' ? <CustomApp {...props.content} /> : <ServicePageContent {...props} />}
                    {props.variation == 'hireApp' ? <LinkButtons /> : ''}
                    {props.careers ? <LinkButton rel="nofollow" target="_blank" variation="bannerBtn" href="https://intuz.kekahire.com/">Current Openings</LinkButton> : ''}
                </Container>
            </section>
            {props.variation == 'hireApp' ? <Container><Blocks /></Container> : ''}
        </>
    )
}

export default ServiceBanner