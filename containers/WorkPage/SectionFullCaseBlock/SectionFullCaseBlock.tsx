//@ts-nocheck

import React from 'react';
import CaseFullBlock from '../../../components/CaseFullBlock/CaseFullBlock';
import style from './styles.module.scss';

const SectionFullCaseBlock = (props) => {
    const variation = props.sectionVariation ? props.sectionVariation : 'healthblock';
    return (
        <section className={`${style[variation]}`} id={props.id}  >
          <CaseFullBlock blockInfo = {props} />
        </section>
    )
}

export default SectionFullCaseBlock;