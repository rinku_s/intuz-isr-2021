//@ts-nocheck
import React from "react";
import SecondaryHeading from "../../../components/Heading/SecondaryHeading";
import WorkClient from "../../../components/WorkClient/WorkClient";
import { cdn } from "../../../config/cdn";
import styles from "./sectionclients.module.scss";
import Container from "components/Container/Container";

const SectionClient = (props) => {
  const ClientInfo = props.sectionclients;
  return (
    <section>
      <Container>
        <div className={styles.ClientSection}>
          <SecondaryHeading>
            Some of the great companies we have worked with
          </SecondaryHeading>
          <div
            className={`grid grid-cols-2 md:grid-cols-3 lg:grid-cols-4 ${styles.ImgContainer}`}
          >
            {ClientInfo.map((logo, index) => {
              return (
                <WorkClient
                  src={logo.image.name}
                  alt={logo.image.name}
                  link={logo.link}
                  title={logo.title ? logo.title : logo.image.name}
                  key={index}
                />
              );
            })}
          </div>
        </div>
      </Container>
    </section>
  );
};
export default SectionClient;
