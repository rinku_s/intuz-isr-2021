//@ts-nocheck

import React from 'react';
import CaseBlock from '../../../components/CaseBlock/CaseBlock';

const SectionCaseBlock = (props) => {
    return (
        <section id={props.id}>
            <CaseBlock direction={props.direction} blockDetail = {props} />
        </section>
    )
}

export default SectionCaseBlock;