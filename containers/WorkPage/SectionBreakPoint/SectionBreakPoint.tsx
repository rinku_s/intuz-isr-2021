//@ts-nocheck
import React from 'react';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import LinkButton from '../../../components/UI/LinkButton/LinkButton';
import { cdn } from '../../../config/cdn';
import styles from './sectionbreakpoint.module.scss';
import Container from '../../../components/Container/Container';

const SectionBreakPoint = (props) => {
    const backStyle = { backgroundImage: `url(${cdn(props.backImage)})`}
    return (
        <section style={backStyle} className={styles.BreakPoints}>
            <Container>
                <SecondaryHeading fontSize='3.5rem'>{props.title}</SecondaryHeading>
                <LinkButton href={props.btnLink} variation="blackBorder" title={props.btnLabel}>{props.btnLabel}</LinkButton>
            </Container>
            <style jsx>
                {`
                    background-size:100% 100%;
                    background-repeat:no-repeat;
                    background-position:center center;
                `}
            </style>
        </section>
    )
}
export default SectionBreakPoint


