import BackgroundImage from "components/BackgroundImage/BackgroundImage";
import { myLoader } from "config/image-loader";
import Image from "next/image";
import Link from "next/link";
import React, { useEffect, useState } from "react";
import AndroidIcon from "./AndroidIcon";
import IosIcon from "./IosIcon";
import MacIcon from "./MacIcon";
import WebIcon from "./WebIcon";
import WindowsIcon from "./WindowsIcon";
interface ComponentWorkProps {
  desktopImage?: string;
  mobileImage: string;
  version: "full" | "half";
  variant: "left" | "right";
  logo: string;
  title?: string;
  description: string;
  caseStudyLink: string;
  contentCenter?: boolean;
  headingTag?: "h1" | "h2" | "h3";
  theme: "dark" | "light";
  icons: {
    android?: boolean;
    ios?: boolean;
    web?: boolean;
    mac?: boolean;
    windows?: boolean;
  };
  backgroundColor?: string;
}

const ComponentWork: React.FC<ComponentWorkProps> = (props) => {
  const [mobile, setMobile] = useState(false);
  useEffect(() => {
    if (window.innerWidth < 768) {
      setMobile(true);
    }
  }, []);

  let Tag = props.headingTag || "h3";
  let textClass =
    props.theme === "dark" ? "text-black-00" : "text-black-00 md:text-white";
  let border =
    props.theme === "dark"
      ? "border-black-00"
      : "border-black-header md:border-white";
  return (
    <section
      className={`min-h-[95vh] md:min-h-full md:h-[95vh] relative grid grid-rows-2 md:grid-rows-none bg-[#e9ecf5] ${props.version === "full" ? "md:bg-transparent" : ""
        }  ${props.version === "half" || props.variant === "left"
          ? "md:grid-cols-2"
          : ""
        } items-center ${textClass}`}
      style={{
        backgroundColor: props.backgroundColor,
        direction:
          props.variant === "left" && props.version !== "full"
            ? "rtl"
            : "unset",
      }}
    >
      {props.version === "full" && (
        <BackgroundImage
          className="hidden md:block"
          backImage={props.desktopImage}
          alt={props.description}
        />
      )}
      <div
        className={`${props.version === "full" ? "md:hidden" : ""
          } relative h-full`}
      >
        <Image
          className="hover:scale-110 transition-transform duration-300 ease-linear"
          layout="fill"
          src={props.mobileImage}
          loader={myLoader}
        />
      </div>
      <div
        className={`${props.version === "full" && props.variant === "right"
          ? "ml-auto md:w-1/2"
          : ""
          } py-10 w-full md:py-0 text-center ${props.contentCenter ? "" : "md:text-left"
          } px-16`}
      >
        <div
          className="md:mb-16"
          style={{
            height: "100px",
            width: "100%",
            position: "relative",
            textAlign: "left",
          }}
        >
          <Image
            layout="fill"
            objectFit="contain"
            className={
              props.contentCenter
                ? "object-center"
                : "object-center md:object-left"
            }
            src={props.logo}
            loader={myLoader}
          />
        </div>
        {props.title && (
          <Tag className="text-5xl leading-tight font-semibold">
            {props.title}
          </Tag>
        )}
        <p className="text-[2.4rem] py-4 font-medium">{props.description}</p>
        <div
          className={`flex gap-x-7 flex-wrap justify-center fill ${props.contentCenter ? "" : "md:justify-start"
            } py-4`}
        >
          {props.icons.web && (
            <span
              className={`inline-flex items-center justify-center ${border}`}
              style={{
                border: `1px solid`,
                height: "50px",
                width: "50px",
                borderRadius: "50%",
              }}
            >
              <WebIcon
                className={`text-header ${props.theme === "dark" ? "" : "md:text-white"
                  }`}
              />
            </span>
          )}
          {props.icons.ios && (
            <span
              className={`inline-flex items-center justify-center ${border}`}
              style={{
                border: `1px solid`,
                height: "50px",
                width: "50px",
                borderRadius: "50%",
              }}
            >
              {" "}
              <IosIcon
                className={`text-header ${props.theme === "dark" ? "" : "md:text-white"
                  }`}
              />
            </span>
          )}
          {props.icons.android && (
            <span
              className={`inline-flex items-center justify-center ${border}`}
              style={{
                border: `1px solid`,
                height: "50px",
                width: "50px",
                borderRadius: "50%",
              }}
            >
              {" "}
              <AndroidIcon
                className={`text-header ${props.theme === "dark" ? "" : "md:text-white"
                  }`}
              />
            </span>
          )}
          {props.icons.mac && (
            <span
              className={`inline-flex items-center justify-center ${border}`}
              style={{
                border: `1px solid`,
                height: "50px",
                width: "50px",
                borderRadius: "50%",
              }}
            >
              {" "}
              <MacIcon
                className={`text-header ${props.theme === "dark" ? "" : "md:text-white"
                  }`}
              />
            </span>
          )}
          {props.icons.windows && (
            <span
              className={`inline-flex items-center justify-center ${border}`}
              style={{
                border: `1px solid`,
                height: "50px",
                width: "50px",
                borderRadius: "50%",
              }}
            >
              {" "}
              <WindowsIcon
                className={`text-header ${props.theme === "dark" ? "" : "md:text-white"
                  }`}
              />
            </span>
          )}
        </div>
        {props.caseStudyLink && (
          <Link href="" prefetch={false}>
            <a
              className={
                props.theme === "dark"
                  ? "text-black-26"
                  : "md:text-white" + "text-black-26"
              }
              style={{
                display: "inline-block",
                border: "1px solid currentColor",
                borderRadius: "2.5rem",
                padding: "1rem 2.5rem",
                fontSize: "1.7rem",
                margin: "1rem 0",
              }}
              href={props.caseStudyLink}
            >
              View Case Study
            </a>
          </Link>
        )}
      </div>
    </section>
  );
};

export default ComponentWork;
