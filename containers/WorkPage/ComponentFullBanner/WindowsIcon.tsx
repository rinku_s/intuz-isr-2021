import * as React from "react";

function WindowsIcon(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width={32}
      height={32}
      viewBox="0 0 32 32"
      {...props}
    >
      <path
        className="fill-current"
        d="M15.458 5.962v9.201h12.398v-11L15.458 5.962zM4.145 15.164h9.376V6.119L4.145 7.474v7.69zm0 9.084l9.376 1.475V16.32H4.145v7.928zm11.313 1.639l12.398 1.949V16.32H15.458v9.567z"
      />
    </svg>
  );
}

export default WindowsIcon;
