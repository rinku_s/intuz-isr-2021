//@ts-nocheck

import gsap from "gsap";
import React from "react";
import ImageBlock from "../../../components/ImageBlock/ImageBlock";
import { cdn } from "../../../config/cdn";
import style from "./styles.module.scss";
import Image from "next/image";
import { myLoader } from "config/image-loader";
const TextContent = (props) => {
  // setOpacity(`.Text`);

  function play() {
    const tl = gsap.timeline();
    tl.fromTo(
      ".Text",
      1,
      { y: -20, opacity: 0, ease: "power0.easeIn" },
      { y: 0, opacity: 1, ease: "power0.easeIn" }
    );
    tl.play();
  }

  return (
    <div className={`${style.TextContent}`}>
      <p className="Text">{props.children}</p>
      {props.imgsrc ? (
        <div className="mt-4">
          <Image
            layout="responsive"
            loader={myLoader}
            src={props.imgsrc}
            width="1224"
            height="412"
          />{" "}
        </div>
      ) : (
        ""
      )}
    </div>
    // </WaypointOnce >
  );
};

export default TextContent;

{
  /* <div><ImageBlock src={cdn()} alt={props.imgsrc} className={'img-fluid'} dwidth={1224} mwidth={680} format={"png"} /></div> */
}
