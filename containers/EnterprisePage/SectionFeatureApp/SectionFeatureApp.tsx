//@ts-nocheck

import gsap from 'gsap';
import React from 'react';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import ImageBlock from '../../../components/ImageBlock/ImageBlock';
import { cdn } from '../../../config/cdn';
import Image from 'next/image';
import { myLoader } from 'config/image-loader';
const SectionFeatureApp = (props) => {
    // setOpacity(`.Title`);

    function play() {
        const tl = gsap.timeline();
        tl.fromTo('.Title', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5);
        tl.play();
    }

    return (

        <>
            {props.content.title ? <SecondaryHeading className='Title'>{props.content.title}</SecondaryHeading> : ''}
            {/* <div><ImageBlock src={cdn(props.content.image.name)} alt={props.content.image.name} className={'img-fluid'} dwidth={1099} mwidth={680} format={"png"} /></div> */}
            {<div><Image quality={100} sizes="50vw" layout="responsive" loader={myLoader} src={props.content.image.name} width="1099" height="549"/></div>}
            <style jsx>
                {`
                    img {
                        max-width: 100%;
                    }
                `}
            </style>
        </>
        // </WaypointOnce >
    )
}

export default SectionFeatureApp