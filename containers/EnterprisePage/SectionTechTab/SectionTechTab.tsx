//@ts-nocheck



import React, { Component } from "react";
import { data } from '../../../components/TechnologyTabs/data';
import DesktopTabView from '../../../components/TechnologyTabs/DesktopTabView';
import MobileTabView from '../../../components/TechnologyTabs/MobileTabView';
import TabContent from '../../../components/TechnologyTabs/tabContent';
import { Media, MediaContextProvider } from "../../../config/responsiveQuery";

export default class SolutionTab extends Component {
    constructor(props) {
        super(props)  
    }
    state = {
        activeTab: "web",
        content: null
    }

    setTab = (id) => {
        if(this.state.activeTab !== null && this.state.activeTab !== id){
            this.setState({ activeTab: id })
        } 
    }

    componentDidMount() {
        let content = data.find(e=>e.id=== this.state.activeTab);
        if(content != null) {
           this.setState({
            content: content
           }); 
        }
    }

    componentDidUpdate = (prevProps, prevState) => {
        if(prevState.activeTab !== this.state.activeTab && this.state.activeTab !==null) {
            let content = data.find(e=>e.id===this.state.activeTab);
            this.setState({
                content: content
            });
        }  
    };
    

    render() {
        let tabContent = <TabContent tabcontent={this.state.content} />
        return(
            <MediaContextProvider>
                <Media greaterThanOrEqual="sm">
                    <DesktopTabView 
                        setTab={this.setTab} 
                        activeTab={this.state.activeTab} 
                    />
                    {tabContent}
                </Media>
                <Media lessThan="sm">
                    <MobileTabView 
                        setTab={this.setTab} 
                        activeTab={this.state.activeTab}
                        tabTitles={this.props.tabTitles} > 
                        {tabContent}
                    </MobileTabView>
                </Media>
            </MediaContextProvider>
        )
    }
}



