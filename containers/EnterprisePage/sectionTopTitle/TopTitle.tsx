//@ts-nocheck

import gsap from "gsap";
import React from "react";
import Container from "../../../components/Container/Container";
import PrimaryHeading from "../../../components/Heading/Heading";
import SecondaryHeading from "../../../components/Heading/SecondaryHeading";
import style from "./styles.module.scss";

const TopTitle = (props) => {
  // setOpacity(`.ServiceHeading, .ServiceSecond`)
  let backStyle = props.backColor ? { background: props.backColor } : null;

  function play() {
    const tl = new gsap.timeline({ paused: true });
    tl.fromTo(
      ".ServiceHeading",
      1,
      { y: -20, opacity: 0, ease: "power0.ease" },
      { y: 0, opacity: 1, ease: "power0.ease" }
    ).fromTo(
      ".ServiceSecond",
      1,
      { y: -20, opacity: 0, ease: "power0.ease" },
      { y: 0, opacity: 1, ease: "power0.ease" },
      0.5
    );
    tl.play();
  }

  return (
    <section className={style.sectionTitle} style={backStyle}>
      <Container>
        {props.title ? (
          <PrimaryHeading className="ServiceHeading">
            {props.title}
          </PrimaryHeading>
        ) : (
          ""
        )}
        {props.description ? (
          <SecondaryHeading className="ServiceSecond">
            {props.description}
          </SecondaryHeading>
        ) : (
          ""
        )}
        {props.children}
      </Container>
    </section>
    // </WaypointOnce >
  );
};

export default TopTitle;
