//@ts-nocheck

import React from "react";
import DetailBlock from "./DetailBlock";
import style from "./styles.module.scss";

const AppDetail = (props) => {
  return (
    <div className={`${style.AppDetail}`} style={props.style}>
      {props.children.map((content, index) => {
        content.description = content.priceblockcontent
          ? content.priceblockcontent
          : content.description;
        return (
          <DetailBlock
            index={index}
            variation={props.variation}
            content={content}
            key={index}
          />
        );
      })}
    </div>
  );
};

export default AppDetail;
