//@ts-nocheck
import { myLoader } from "config/image-loader";
import gsap from "gsap";
import Image from "next/image";
import React from "react";
import LazyLoad from "react-lazyload";
import { cdn } from "../../../config/cdn";
import style from "./styles.module.scss";

const DetailBlock = ({ content, variation, index }) => {
  let desc = React.useRef();
  let title = React.useRef();
  // setOpacity(`.title, .desc`);

  function play() {
    const tl = gsap.timeline();
    tl.fromTo(
      title.current,
      0.8,
      { y: -20, autoAlpha: 0 },
      { y: 0, autoAlpha: 1, ease: "Power1.easeOut" }
    ).fromTo(
      desc.current,
      0.8,
      { y: -20, autoAlpha: 0 },
      { y: 0, autoAlpha: 1, ease: "Power1.easeOut" }
    );
  }

  return (
    <div
      className={`${style.detailBlock} ${style[variation]}`}
      id={`appdetail_${index}`}
    >
      <div className={style.textBlock}>
        <h3 className="title" ref={title}>
          {content.title}
        </h3>
        <div
          className={`${style.descBlock} desc`}
          ref={desc}
          dangerouslySetInnerHTML={{ __html: content.description }}
        ></div>
      </div>
      <div className={style.imageBlock}>
        <Image layout="intrinsic" loader={myLoader} src={content.icon.name} width={360} height={400} />
        {/* <LazyLoad height={300} offset={300}>
                    <img src={cdn(`${content.icon.name}?auto=format`)} alt={content.icon.name} />
                </LazyLoad> */}
      </div>
    </div>
    // </WaypointOnce >
  );
};

export default DetailBlock;
