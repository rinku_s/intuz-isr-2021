//@ts-nocheck
import React from 'react';
import ResourcesSectionStyle from '../../../components/ReusableSectionStyle/ResourceSection';
import { cdn } from '../../../config/cdn';
const HeroSectionResources: React.FC<any> = (props) => {
    return (
        <ResourcesSectionStyle 
            backImage={cdn(props.backImage)}
            heading={props.heading}
            subtitle={props.subtitle}
            breadcrumb = {props.breadcrumb}
        />
    )
}

export default HeroSectionResources
