//@ts-nocheck

import React, { useState } from 'react';
import PdfDownloadModel from '../../../components/PdfDownloadModel/PdfDownloadModel';

const SectionPdf = (props) => {
    const [modalShow, setModalShow] = useState(false)
    
    return (
        <>
            <button onClick={() => setModalShow(!modalShow)}>Download</button>
            <PdfDownloadModel show={modalShow} hide={()=>setModalShow(false)} />
            <style jsx>
            {
                `
                    button {
                        border: none;
                        padding: .8rem 2.5rem;
                        margin-left: 1rem;
                        font-weight: 400;
                        color: #fff;
                        background: #3cecec;
                        font-size: 1.8rem;
                    }
                `
            }
            </style>
        </>
    )
}

export default SectionPdf

