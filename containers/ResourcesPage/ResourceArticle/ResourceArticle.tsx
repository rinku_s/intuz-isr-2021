//@ts-nocheck
import React from 'react';
import Container from '../../../components/Container/Container';
import ResourceArticleContainer from '../../../components/ResourceArticleContainer/ResourceArticleContainer';
import { cdn } from '../../../config/cdn';

const ResourceArticle = (props) => {
    return (
        <section>
            <Container>
                {props.resources.map(resource=>(
                <ResourceArticleContainer
                    key={resource.id}
                    link={resource.link}
                    image={cdn(resource.image.name)}
                    title={resource.title}
                    description={resource.description}
                    blog={props.blog}
                />
                ))}
            </Container>
        </section>
    )
}

export default ResourceArticle
