//@ts-nocheck
import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import HiringProcessDes from '../../../components/HireDDComponents/HiringProcess/HiringProcessDes';
import HiringProcessMob from '../../../components/HireDDComponents/HiringProcess/HiringProcessMob';
import { Media, MediaContextProvider } from "../../../config/responsiveQuery";
import style from './styles.module.scss';

const HiringProcessSection = () => {
    return (
        <MediaContextProvider>
            <section style={{paddingBottom:"8rem"}} id='HiringProcess' className={style.HireProcessSection}>
                <Container>
                    <PrimaryHeading font="s" fontSize="6.5rem" style={{fontSize:"6.5rem", fontWeight:300}}>
                        Hiring Process
                    </PrimaryHeading>
                    <SecondaryHeading font="s" fontSize="2.4rem">
                            We keep it simple.
                    </SecondaryHeading>
                    <Media greaterThanOrEqual="sm">
                        <HiringProcessDes/>
                    </Media>
                    <Media lessThan="sm">
                        <HiringProcessMob />
                    </Media>
                </Container>

            </section>
        </MediaContextProvider>
    )
}

export default HiringProcessSection
