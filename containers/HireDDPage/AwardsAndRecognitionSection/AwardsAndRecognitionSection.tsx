//@ts-nocheck
import React from 'react';
import PrimaryHeading from '../../../components/Heading/Heading';
import classes from './styles.module.scss';
import Awards from '../../../components/Awards';
import Container from '../../../components/Container/Container';
const AwardsAndRecognitionSection = () => {
    return (
        <section className={classes.Awards}>
            <Container>
              <PrimaryHeading font="s" fontSize="6.5rem" style={{fontSize:"6.5rem", fontWeight:300}}>
                         Awards and Recognition
                </PrimaryHeading>
                <Awards/>
            </Container>
        </section>
    )
}

export default AwardsAndRecognitionSection
