//@ts-nocheck
import gsap from 'gsap';
import ScrollToPlugin from 'gsap/dist/ScrollToPlugin';
import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import SkillsComp from '../../../components/HireDDComponents/SkillsComp/SkillsComp';
import LinkButton from '../../../components/UI/LinkButton/LinkButton';
import classes from './styles.module.scss';
const SkillSection = (props) => {
    gsap.registerPlugin(ScrollToPlugin);

    function scrollToSection(id){
        gsap.to(window, 0.1, {scrollTo:{y:`#${id}`, autoKill:false}})
    }
    return (
        <section className={classes.SkillSection}>
             <Container>
                <PrimaryHeading font="s" fontSize="6.5rem" style={{fontSize:"6.5rem", fontWeight:300}}>
                        Hire for Specific Skill
                </PrimaryHeading>
                <SecondaryHeading font="s" fontSize="2.4rem">
                We at Intuz are always passionate about what we can produce with the technologies and tools to enable disruptive innovations.
                </SecondaryHeading>
                <SkillsComp/>

                <LinkButton variation="blue-sand" style={{marginTop:"8rem"}} onClick={() => scrollToSection('form')}>
                    Request an interview
                </LinkButton>
            </Container>
        </section>
    )
}

export default SkillSection


