//@ts-nocheck
import gsap from 'gsap';
import ScrollToPlugin from 'gsap/dist/ScrollToPlugin';
import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import WeGetIt from '../../../components/HireDDComponents/WeGetIt/WeGetIt';
import LinkButton from '../../../components/UI/LinkButton/LinkButton';
import classes from './styles.module.scss';
const WeGetitSection = () => {
    gsap.registerPlugin(ScrollToPlugin);

    function scrollToSection(id) {
        gsap.to(window, 0.1, { scrollTo: { y: `#${id}`, autoKill: false } })
    }

    return (
        <section className={classes.Wegetit}>
            <Container>
                <PrimaryHeading font="s" fontSize="6.5rem" style={{ fontSize: "6.5rem", fontWeight: 300 }}>
                    We Get it
                </PrimaryHeading>
                <SecondaryHeading font="s" fontSize="2.4rem">
                    Unlike hiring through marketplace like Upwork, Toptal and more, Intuz brings a lot of value to the table.
                </SecondaryHeading>
                <WeGetIt style={{ marginTop: "10rem", }} />
                <div className='text-center'>
                    <LinkButton variation="blue-sand" onClick={() => scrollToSection('form')}>
                        Discuss Your Specific Requirements
                </LinkButton>
                </div>
            </Container>
        </section>
    )
}

export default WeGetitSection

