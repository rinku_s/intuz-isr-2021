//@ts-nocheck
import React from 'react'
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import HireDeveloper from '../../../components/HireDeveloper/HireDeveloper';
import style from './styles.module.scss';


const MeetTheDeveloperSection = () => {
    return (
        <section className={style.MeetDevSection}>
            <Container>
                <PrimaryHeading font="s" fontSize="6.5rem" style={{fontSize:"6.5rem", fontWeight:300}}>
                    Meet The Developers
                </PrimaryHeading>
                <SecondaryHeading font="s" fontSize="2.4rem">
                    On-Demand mobile and web application developers at your disposal
                </SecondaryHeading>
                <HireDeveloper/>
            </Container>
        </section>
    )
}

export default MeetTheDeveloperSection
