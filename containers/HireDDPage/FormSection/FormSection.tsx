//@ts-nocheck
import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import classes from './styles.module.scss';
import Form from '../../../components/HireDDComponents/Form/Form';
const FormSection = () => {
    return (
        <section className={classes.FormSection} id="form">
            <Container>
                <PrimaryHeading font="s" fontSize="6.5rem" style={{fontSize:"6.5rem", fontWeight:300}}>
                    Ready to Hire Developers?
                </PrimaryHeading>
                <Form/>
            </Container>
        </section>
    )
}

export default FormSection
