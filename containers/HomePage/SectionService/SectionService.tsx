import HeadAndSub from "components/Utility/HeadAndSub";
import React from "react";
import Container from "../../../components/Container/Container";
import Service from "../../../components/Services/service";
import classes from "./styles.module.scss";
const SectionService = (props) => {
  return (
    <section className={"bg-gray-100 text-center py-32"}>
      <Container>
        <HeadAndSub
          heading="Services We Offer"
          subHeading="Concept. UI/UX. Code. Test. Launch. Market. Upgrade"
        />
        <Service sectionservices={props.sectionservices} />
      </Container>
    </section>
  );
};

export default SectionService;
