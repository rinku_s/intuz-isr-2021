import { cdn } from "config/cdn";
import Image from "next/image";
import React from "react";
import HeroCarousel from "../../../components/HeroCarousel/HeroCarousel";
const SectionHero = () => {
  return (
    <section className="h-[700px] flex items-center justify-center relative">
      <Image
        priority={true}
        layout="fill"
        unoptimized
        objectFit="cover"
        src={cdn("banner01.jpg?auto=format,compress")}
      />
      <HeroCarousel />
    </section>
  );
};

export default SectionHero;
