import HeadAndSub from "components/Utility/HeadAndSub";
import React from "react";
import Container from "../../../components/Container/Container";
import WorkPartner from "../../../components/WorkPartner/WorkPartner";
import classes from "./styles.module.scss";
const SectionClients = (props) => {
  //setOpacity(`.ClientHeading, .ClientSecondary`);

  return (
    <section className={"py-32 text-center"}>
      <Container>
        <HeadAndSub
          heading="We work with amazing companies"
          subHeading="Companies who used the products developed by us or utilized our strategy, design & development services."
        />
        <WorkPartner sectionclients={props.sectionclients} />
      </Container>
    </section>
  );
};

export default SectionClients;
