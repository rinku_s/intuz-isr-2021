import React from "react";
import Container from "../../../components/Container/Container";
import classes from "./styles.module.scss";
const SectionInformation = () => {
  return (
    <section className={classes.SectionInformation}>
      <Container className={classes.flexContainer}>
        <h2>
          Application Development
          <span> Partner You Can Trust </span>
          <span>Mobile </span>
          <span className={classes.circle}>Web</span>
          <span className={classes.circle}>IoT</span>
          <span className={classes.circle}>Blockchain</span>
          <span className={classes.circle}>Cloud</span>
        </h2>
        <p>
          At Intuz we’re a mobile app development company that seeks to conquer
          the impossible with our well-rounded mobile app services. Our goal is
          to open new doors concerning the market for all of our clients. We’re
          the mobile app design company aimed to produce intelligible and
          stunning designs. Our mobile application developers are capable of
          developing the most complex applications, seamlessly. Consider us to
          be your app marketing agency for promising mobile solutions helping
          your business to grow at a faster pace.
        </p>
      </Container>
    </section>
  );
};

export default SectionInformation;
