//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
import PrimaryHeading from '../../../components/Heading/Heading';
import FormContainer from '../../../components/Form/FormContainer';
const SectionForm = (props) => {
    return (
        <section className={classes.SectionForm} id="sectionForm">
            <PrimaryHeading style={{textAlign:'center'}}>Contact Us</PrimaryHeading>
            <FormContainer formTitle="Reach Out To Us" />
        </section>
    )
}

export default SectionForm
