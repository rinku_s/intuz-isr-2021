import HeadAndSub from "components/Utility/HeadAndSub";
import React from "react";
import FeatureApp from "../../../components/FeatureApp/FeatureApp";
import styles from "../homeCommonStyle.module.scss";

const SectionFeatureApp = (props) => {
  //setOpacity(`.featureHeading, .featureSecondary`);

  return (
    // <VisibilityWrapper tween={tl}>
    <section className={`${styles.FeatureApp} featureapp`}>
      <HeadAndSub
        heading="Featured App"
        subHeading="Live4It Locations - Find & Host Sports and Events"
      />
      <FeatureApp className="fetureapp mt-4" />
    </section>
    // </VisibilityWrapper>
  );
};

export default SectionFeatureApp;
