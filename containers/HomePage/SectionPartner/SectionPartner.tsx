import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import LinkButton from '../../../components/UI/LinkButton/LinkButton';
import styles from '../homeCommonStyle.module.scss';

const SectionPartner = (props) => {

    return (
        <section className={styles.Partners}>
            <Container>
                <PrimaryHeading className="ctaHeading">Digital Agency? Partner with Intuz</PrimaryHeading>
                <SecondaryHeading className="secondarycta" fontSize='1.8rem'>Hire full stack development teams for creating cutting edge web and mobile application.
                    </SecondaryHeading>
                <LinkButton href="/partner-with-us" variation="blue">Partner with Us</LinkButton>
            </Container>
        </section>
    )
}

export default SectionPartner