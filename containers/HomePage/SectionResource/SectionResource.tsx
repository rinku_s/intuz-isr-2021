import HeadAndSub from "components/Utility/HeadAndSub";
import React from "react";
import Container from "../../../components/Container/Container";
import Resources from "../../../components/Resources/Resources";
import LinkButton from "../../../components/UI/LinkButton/LinkButton";
import classes from "./styles.module.scss";
const SectionResource = (props) => {
  //setOpacity(`.resourceHeading, .resourceSecondary`);

  let sectionresources =
    props.sectionresources &&
    props.sectionresources.map((obj) => {
      obj.image = obj.resourceBlockImage ? obj.resourceBlockImage : obj.image;
      return obj;
    });

  return (
    <section className={classes.SectionResource}>
      <Container className="text-center">
        <HeadAndSub
          heading={props.changeUI ? "More Guides" : "Intuz Resources"}
          subHeading={
            props.changeUI
              ? ""
              : "Insights on latest technology trends, enterprise mobility solutions, & company updates"
          }
        />
        {/* <div className='content-center'>
                    <h2 className="text-5xl mb-16 text-center lg:text-48 text-black-2f font-opensans font-semibold">{props.changeUI ? 'More Guides' : 'Intuz Resources'}</h2>
                    <p className="text-3xl   leading-normal mx-auto  md:max-w-[68%] mb-16 text-center lg:text-30 text-black-6b font-opensans font-normal">{props.changeUI ? '' : 'Insights on latest technology trends, enterprise mobility solutions, & company updates'}</p>
                </div> */}

        <Resources
          sectionresources={sectionresources}
          changeUI={props.changeUI}
        />
        {props.changeUI ? (
          ""
        ) : (
          <div className="flex justify-center m-12 ">
            <LinkButton variation="blackBorder" href="/resources">
              View More
            </LinkButton>
          </div>
        )}
      </Container>
    </section>
  );
};

export default SectionResource;
