//@ts-nocheck
import React from 'react';
import AgileWhatWeDo from '../../../components/AgileDevWhatWeDo/AgileWhatWeDo';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import classes from './styles.module.scss';
const WhatWeDoAgile = (props) => {
    return (
        <section className={classes.WhatWeDoAgile}>
            <Container>
                <PrimaryHeading font="s">What We Do?</PrimaryHeading>
                <SecondaryHeading fontSize="1.6rem">We are offering dedicated DevOps professionals hiring service to startups, small businesses and IT service providers.</SecondaryHeading>
            <AgileWhatWeDo whatwedoagiles={props.whatwedoagiles}/>
            </Container>
        </section>
    )
}

export default WhatWeDoAgile
