
import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import Content from '../../../components/SolutionContent/Content';
import commonStyle from '../styles.module.scss';

const SectionSolution = (props) => {
    return(
        <section className={commonStyle.SolutionSection}>
            <Container>
                <PrimaryHeading>Our Solution Expertise</PrimaryHeading>
                <Content content={props.content} />
            </Container>
        </section>
    )
}

export default SectionSolution;