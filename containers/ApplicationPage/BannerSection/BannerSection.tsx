import { myLoader } from "config/image-loader";
import Image from "next/image";
import React from "react";
import Container from "../../../components/Container/Container";
import CustomApp from "../../../components/CustomAppBannner/CustomAppBanner";
// import ServicePageContent from "../../../components/ServicePageContent/ServicePageContent";
import CustomIDevlopmentBanner from "../../../components/CustomIDevlopmentBanner/CustomIDevlopmentBanner";
import Blocks from "../../../components/HireDDComponents/Hero/Blocks/Blocks";
import LinkButtons from "../../../components/HireDDComponents/Hero/LinkButtons";
import LinkButton from "../../../components/UI/LinkButton/LinkButton";
import style from "./styles.module.scss";

const ApplicationPageBanner = (props) => {
  return (
    <>

      {/* <NewBanner /> */}
      <section
        className={`${style.SectionBanner} ${props.variation && style[props.variation]
          } relative`}
        id="banner"
      >
        <Image layout="fill" objectFit="cover" loader={myLoader} src={props.content.backImage.name} />
        <Container
          className={`${style.Content} ${props.variation == "customApp" ? "" : style.ServiceContent
            } ${style[props.variation]} }`}
        >
          {props.variation == "customApp" ? (
            <CustomApp {...props.content} />
          ) : (
            <CustomIDevlopmentBanner {...props} />
          )}
          {props.variation == "hireApp" ? <LinkButtons /> : ""}
          {props.careers ? (
            <LinkButton
              rel="nofollow"
              target="_blank"
              variation="bannerBtn"
              href="https://intuz.kekahire.com/"
            >
              Current Openings
            </LinkButton>
          ) : (
            ""
          )}
        </Container>
      </section>
      {props.variation == "hireApp" ? (
        <Container>
          <Blocks />
        </Container>
      ) : (
        ""
      )}
    </>
  );
};

export default ApplicationPageBanner;
