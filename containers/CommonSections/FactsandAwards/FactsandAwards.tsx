import { myLoader } from "config/image-loader";
import Image from "next/image";
import React from "react";
import Container from "../../../components/Container/Container";
import { Swiper, SwiperSlide } from "swiper/react";
import classes from "./styles.module.scss";
const FactsandAwards = (props) => {
  const settings: Swiper = {
    breakpoints: {
      0: {
        slidesPerView: 1,
      },
      768: {
        slidesPerView: 3,
      },
      990: {
        slidesPerView: 5,
      },
    },
  };
  return (
    <div>
      {
        <section className={`${classes.FactsandAwards} classes.bg`}>
          <Container>
            <Swiper {...settings}>
              {props.factandawards.map((factaward, i) => (
                <SwiperSlide key={i}>
                  <Image
                    loader={myLoader}
                    src={factaward.image.name}
                    width={205}
                    height={177}
                    layout={"fixed"}
                  />
                </SwiperSlide>
              ))}
            </Swiper>
          </Container>
        </section>
      }
    </div>
  );
};

export default FactsandAwards;
