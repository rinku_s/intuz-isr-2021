import React from 'react';
import Container from '../../../components/Container/Container';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import LinkButton from '../../../components/UI/LinkButton/LinkButton';
import classes from './styles.module.scss';
const CTAStripe = (props) => {

    return (
        <div>
            {<section className={`${classes.CTAStripe} ${classes.bg}`} style={{ ...props.style, backgroundImage: props.background }}>

                <Container>
                    <SecondaryHeading className="mb-5">{props.title}</SecondaryHeading>
                    <LinkButton onClick={props.onClick} href={props.link} variation="blackBorder">{props.LinkButtontxt}</LinkButton>
                </Container>

            </section>
            }
        </div>
    )
}

export default CTAStripe
