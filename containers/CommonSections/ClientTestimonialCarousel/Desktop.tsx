import { myLoader } from 'config/image-loader';
import Image from 'next/image';
import React from 'react';
import Container from '../../../components/Container/Container';
import classes from './styles.module.scss';

const Desktop = (props) => {

    return (
        <>
            <Image loader={myLoader} className="w-100" layout={'fill'} src={props.appImage} alt="Banner 1" />
            <div className={`${classes.Caption} flex h-full items-center`}>
                <Container>
                    <blockquote>{props.desc}</blockquote>
                    <p className="relative"><strong>{props.name}</strong></p>
                    <p className="relative">{props.designation}</p>
                </Container>
            </div>
        </>
    )
}

export default Desktop

