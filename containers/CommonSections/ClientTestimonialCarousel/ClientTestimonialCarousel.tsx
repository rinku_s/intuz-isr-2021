// import React, { useState } from "react";
// import { Swiper, SwiperSlide } from "swiper/react";
// import "swiper/swiper.min.css";
// import { MediaContextProvider } from "../../../config/responsiveQuery";
// import Desktop from "./Desktop";
// // import './override.css';
// import classes from "./styles.module.scss";
// // const clientSpeak = [
// //     {
// //         id:1,
// //         desc: "I really enjoyed working with the Intuz team they offered me great expertise and very good advises on all of my current and future projects. ",
// //         name: "Patrick Mimran",
// //         appImage: "https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/bannerImg01.jpg",
// //         clientImage: "https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/hire-developers/ransoft-limited.png",
// //         designation: "Digi-Gift, Australia",
// //     },
// //     {
// //         id:2,
// //         desc: "I really enjoyed working with the Intuz team they offered me great expertise and very good advises on all of my current and future projects. ",
// //         name: "Patrick Mimran",
// //         appImage: "https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/bannerImg02.jpg",
// //         clientImage: "https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/hire-developers/tim.png",
// //         designation: "Digi-Gift, Australia",
// //     },
// //     {
// //         id:3,
// //         desc: "I really enjoyed working with the Intuz team they offered me great expertise and very good advises on all of my current and future projects. ",
// //         name: "Patrick Mimran",
// //         appImage: "https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/bannerImg03.jpg",
// //         clientImage: "https://intuz-site.imgix.net/uploads/1-backup-s3-intuz-site/hire-developers/user2.png",
// //         designation: "Digi-Gift, Australia",
// //     },
// // ]

// const ClientTestimonialCarousel = (props) => {
//   const pagination = {
//     clickable: true,
//     renderBullet: function (index, className) {
//       return '<span class="' + className + '">' + (index + 1) + "</span>";
//     },
//   };
//   const [activeIndex, setActiveIndex] = useState(0);

//   const handleSelect = (selectedIndex, e) => {
//     setActiveIndex(selectedIndex);
//   };
//   return (
//     <MediaContextProvider>
//       <section className={`${classes.ClientTestimonialCarousel} h-[70vh]  `}>
//         <Swiper
//           style={{ height: "100%" }}
//           pagination={pagination}
//           className="mySwiper"
//         >
//           {props.content.map((cs) => (
//             <SwiperSlide>
//               <Desktop
//                 appImage={cs.app_image.name}
//                 desc={cs.description}
//                 name={cs.name}
//                 designation={cs.designation}
//               />
//             </SwiperSlide>
//           ))}
//         </Swiper>

//         {/*} <LazyLoad offset={100} height={100}>
//                         <ul className={classes.ImageIndicators}>
//                             {props.content.map((cd,i)=>(
//                                 <li key={cd.id} onClick={()=>setActiveIndex(i)} className={activeIndex === i ? classes.active : ''}>
//                             <img src={cdn(cd.client_image.name)} alt={cd.name}/>
//                             </li>
//                             ))}
//                         </ul>
//                             </LazyLoad>*/}

//         {/* <ul className={classes.ImageIndicators}>
//                         {props.content.map((cd, i) => (
//                             <li key={cd.id} onClick={() => setActiveIndex(i)} className={activeIndex === i ? classes.active : ''}>

//                                 <Image layout={'fixed'} loader={myLoader} height={90} width={90} src={cd.client_image.name} alt={cd.name} />
//                             </li>
//                         ))}
//                     </ul> */}
//       </section>
//     </MediaContextProvider>
//   );
// };

// export default ClientTestimonialCarousel;

import ComponentClientsSays from "containers/IotNewPage/sections/ComponentClientsSays";
import React from "react";

const ClientTestimonialCarousel = (props) => {
  return <ComponentClientsSays />;
};

export default ClientTestimonialCarousel;
