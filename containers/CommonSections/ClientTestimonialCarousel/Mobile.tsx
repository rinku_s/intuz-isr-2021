import { myLoader } from 'config/image-loader';
import Image from 'next/image';
import React from 'react';
import classes from './styles.module.scss';
const Mobile = (props) => {
    return (

        <div className={classes.MobileCaption}>

            <Image loader={myLoader} src={props.clientImage} alt={props.name} layout={'fixed'} width={100} height={100} />
            <blockquote>{props.desc}</blockquote>
            <p><strong>{props.name}</strong></p>
            <p>{props.designation}</p>
        </div>

    )
}

export default Mobile
