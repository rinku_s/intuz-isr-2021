//@ts-nocheck
import React from 'react';
import ChForm from '../../../components/ChForm/ChForm';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import classes from './styles.module.scss';
const CheckBoxForm = () => {
    return (
        <section className={classes.CheckBoxForm}>
            <Container>

            <PrimaryHeading font="s">
                Ready to Get Started?
            </PrimaryHeading>
            <ChForm/>
            </Container>
            
        </section>
    )
}

export default CheckBoxForm
