import React from 'react';
import Awards from '../../../components/Awards';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import classes from './styles.module.scss';
const AwardsAndRecognition = (props) => {



    return (


        <section className={classes.AwardsAndRecognition}>
            <Container>
                <PrimaryHeading font={props.f ? "" : "s"} className='awardHeader'>
                    Awards and Recognition
                    </PrimaryHeading>
                <SecondaryHeading fontSize="1.6rem" className='awardSubheader'>
                    We are proud to share some recent accolades and awards we have earned.
                </SecondaryHeading>
                <Awards awardandrecognitions={props.awardandrecognitions} />
            </Container>

        </section>

    )
}

export default AwardsAndRecognition

