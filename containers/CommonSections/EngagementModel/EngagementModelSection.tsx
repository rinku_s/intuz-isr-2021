//@ts-nocheck
//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import Container from '../../../components/Container/Container';
import EngagementModel from '../../../components/EngagementModel';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import classes from './styles.module.scss';


const EngagementModelSection = (props) => {

    // setOpacity(`.devHeader, .devSubheader`, {opacity:0})

    function play() {
        const tl = gsap.timeline({ paused: true });
        tl.fromTo(`.devHeader`, 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" })
            .fromTo(`.devSubheader`, 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.5);
        tl.play();
    }

    return (

        <section className={classes.EngagementModelSection} style={props.style}>
            <Container>
                <PrimaryHeading font="s" className='devHeader'>{props.title ? props.title : 'Engagement Models'}</PrimaryHeading>
                <SecondaryHeading color="#4a4a4a" variation='UpdateText' className='devSubheader'>
                    {props.description}
                </SecondaryHeading>
                <EngagementModel button={props.button} />
            </Container>
        </section>
        // </WaypointOnce >
    )
}

export default EngagementModelSection
