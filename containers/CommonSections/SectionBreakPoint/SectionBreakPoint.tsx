import { myLoader } from "config/image-loader";
import Image from "next/image";
import React from "react";
import SecondaryHeading from "../../../components/Heading/SecondaryHeading";
import LinkButton from "../../../components/UI/LinkButton/LinkButton";
import WaypointOnce from "../../../components/WaypointOnce/WaypointOnce";
import { cdn } from "../../../config/cdn";
import styles from "./sectionbreakpoint.module.scss";

const SectionBreakPoint = (props) => {
  if (props.content.length <= 0) {
    return null;
  } else {
    const varition = props.variation ? props.variation : "";
    let backStyle = {
      background: `url(${cdn(
        props.content[0].backImage.name + "?fm=pjpg&auto=format"
      )}) center center no-repeat`,
    };

    return (
      <section className={`relative ${styles.BreakPoints} ${styles[varition]}`}>
        <div className="z-10 relative">
          <SecondaryHeading fontSize="3rem">
            {props.content[0].title}
          </SecondaryHeading>
          {props.content[0].buttonLabel ? (
            <LinkButton
              className="opacity-100"
              href={props.content[0].buttonLink}
              variation="blackBorder"
              title="Let’s Discuss"
            >
              {props.content[0].buttonLabel}
            </LinkButton>
          ) : (
            ""
          )}
        </div>
        <Image
          src={props.content[0].backImage.name}
          layout={"fill"}
          loader={myLoader}
        />
      </section>
    );
  }
};
export default SectionBreakPoint;
