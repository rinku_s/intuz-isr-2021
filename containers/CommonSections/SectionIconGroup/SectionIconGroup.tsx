import gsap, { Power0 } from "gsap";
import React from "react";
import Container from "../../../components/Container/Container";
import PrimaryHeading from "../../../components/Heading/Heading";
import SecondaryHeading from "../../../components/Heading/SecondaryHeading";
import IconGroup from "../../../components/IconGroup/IconGroup";
import WaypointOnce from "../../../components/WaypointOnce/WaypointOnce";
import setOpacity from "../../../hooks/setOpacity";
import commonStyle from "./styles.module.scss";

const SectionIconGroup = (props) => {
  return (
    <section
      className={`${commonStyle.IconGroup} ${commonStyle[props.variation]}`}
    >
      <Container>
        {props.title ? (
          <PrimaryHeading className="Heading">{props.title}</PrimaryHeading>
        ) : (
          ""
        )}
        {props.description ? (
          <SecondaryHeading className="Secondary">
            {props.description}
          </SecondaryHeading>
        ) : (
          ""
        )}
        <IconGroup>{props.children}</IconGroup>
      </Container>
    </section>
  );
};

export default SectionIconGroup;
