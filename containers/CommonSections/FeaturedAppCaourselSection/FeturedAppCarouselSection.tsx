import React from 'react'
import classes from './styles.module.scss'
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import FeaturedAppCarousel from '../../../components/FeturedAppCarousel/FeaturedAppCarousel';
const FeturedAppCarouselSection = () => {
    return (
        <section className={classes.FeatureApp}>
            <Container>
                <PrimaryHeading className="mb-mx">
                    Featured Apps
                </PrimaryHeading>
                <FeaturedAppCarousel/>
            </Container>
        </section>
    )
}

export default FeturedAppCarouselSection
