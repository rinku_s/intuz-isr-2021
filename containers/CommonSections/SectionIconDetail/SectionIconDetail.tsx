import { myLoader } from "config/image-loader";
import Image from "next/image";
import React from "react";
import Container from "../../../components/Container/Container";
import PrimaryHeading from "../../../components/Heading/Heading";
import Content from "../../../components/IconDetailBlock/Block";
import commonStyle from "./styles.module.scss";

const SectionIconDetail = (props) => {
  let backColor = props.backColor ? { background: `${props.backColor}` } : null;
  return (
    <section
      className={`${commonStyle.AdaptiveSection} ${commonStyle[props.variation]
        }`}
      style={backColor}
      id="iconDetail"
    >
      <Container className={commonStyle.FullPage}>
        {props.title ? (
          <div className={`content-center ${commonStyle.imgCenter}`}>
            {props.topImage ? (
              <Image
                layout={"fixed"}
                loader={myLoader}
                src={props.topImage}
                alt="userImage"
                width={-1}
                height={110}
              />
            ) : (
              ""
            )}
            <PrimaryHeading>{props.title}</PrimaryHeading>
          </div>
        ) : (
          ""
        )}
        <Content buttonLabel={props.buttonLabel} buttonLink={props.buttonLink}>
          {props.children}
        </Content>
      </Container>
    </section>
  );
};

export default SectionIconDetail;
