import { cdn } from "config/cdn";
import { myLoader } from "config/image-loader";
import Image from "next/image";
import React from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.min.css";
import Container from "../../../components/Container/Container";
import PrimaryHeading from "../../../components/Heading/Heading";
import classes from "./styles.module.scss";
const iconList = [
  {
    icon: "mercedes_72a7c97803.png",
    title: "Mercedes",
  },
  {
    icon: "Bitmap_cf3a5a6460.png",
    title: "Bitmap",
  },
  {
    icon: "Bosch_0993ce56a1.png",
    title: "Bosch",
  },
  {
    icon: "Frame_7cce53693c.png",
    title: "Frame",
  },
  {
    icon: "jll_d2f4c0cc1b.png",
    title: "JLL",
  },
  {
    icon: "RFI_a5397ac11f.png",
    title: "RFL",
  },
];

const TrustedSection = (props) => {
  const rightArr = React.useRef();
  const leftArr = React.useRef();
  let r = React.useRef<any>();
  return (
    <section className={`${classes.TrustedSection} mt-20`}>
      <Container>
        {props.heading ? (
          <PrimaryHeading
            className="mb-5"
            font={props.f}
            style={{ fontWeight: props.light ? 300 : 600 }}
          >
            Trusted By {props.heading}
          </PrimaryHeading>
        ) : (
          ""
        )}
        {props.subHeading && (
          <p className={classes.paragraphHeding}>{props.subHeading}</p>
        )}
        <Swiper
          className="mt-5"
          navigation={{
            nextEl: "#arrow-left",
            prevEl: "#arrow-right",
          }}
          onSwiper={(swiper) => {
            r.current = swiper;
          }}
          slidesPerView={6}
          breakpoints={{
            320: {
              slidesPerView: 2,
              spaceBetween: 20,
              navigation: {
                nextEl: rightArr?.current,
                prevEl: leftArr?.current,
                lockClass: "hidden",
              },
            },
            480: {
              slidesPerView: 3,
              spaceBetween: 30,
            },
            640: {
              slidesPerView: 4,
              spaceBetween: 40,
            },
            990: {
              slidesPerView: 6,
              spaceBetween: 40,
            },
          }}
        >
          {iconList.map((sc) => (
            <SwiperSlide className="text-center" key={sc.title}>
              <div
                style={{
                  display: "inline-flex",
                  width: 110,
                  height: 80,
                  position: "relative",
                  alignItems: "flex-end",
                  justifyContent: "center",
                  padding: "5px",
                }}
              >
                <Image
                  src={cdn(sc.icon)}
                  alt={sc.title}
                  loader={myLoader}
                  sizes="30vw"
                  unoptimized
                  // width="100"
                  // height="20"
                  objectFit="contain"
                  layout={"fill"}
                />
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
        <div
          className="block md:hidden"
          style={{ marginTop: "3rem", textAlign: "center" }}
        >
          <img
            id="arrow-left"
            className="mr-3"
            onClick={() => {
              r?.current?.slidePrev();
            }}
            src={"/images/arrow-left-circle.svg"}
            alt="Previous"
          />
          <img
            onClick={() => {
              r?.current?.slideNext();
            }}
            id="arrow-right"
            src={"/images/arrow-right-circle.svg"}
            alt="Next"
          />
        </div>
      </Container>
    </section>
  );
};
export default TrustedSection;

// import React from "react";

// const TrustedSection = (props) => {
//   return <div></div>;
// };

// export default TrustedSection;
