//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
import LazyLoad from 'react-lazyload'
const ClientLogo = (props) => {
    return (
        <div className={classes.ClientLogo}>
            <LazyLoad offset={200} height={150}>
                <img src={props.image} alt={props.alt}/>
            </LazyLoad>
        </div>
    )
}

export default ClientLogo
