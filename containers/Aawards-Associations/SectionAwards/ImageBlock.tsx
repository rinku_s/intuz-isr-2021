//@ts-nocheck
import React from "react";
import LazyLoad from "react-lazyload";
import { cdn } from "../../../config/cdn";
import style from "../styles.module.scss";

const ImageBlock = (props) => {
  return (
    <div
      className={`col-sm-6 col-md-3 ${style.imgContainer} ${style[props.className]
        }`}
    >
      <div className={style.imgBlock}>
        <LazyLoad height={300} offset={300} once>
          <img
            src={cdn(props.imgSrc)}
            alt={props.imgSrc}
            className="img-fluid"
          />
        </LazyLoad>
      </div>
      <div className={style.hoverBlock}>
        {props.rating ? (
          <div className={style.rating}>
            {props.rating} / <em> 5 </em>
            <img
              src={"../../../static/Images/awards-associations/star-rating.png"}
              alt="rating"
            />
            <img
              src={"../../../static/Images/awards-associations/star-rating.png"}
              alt="rating"
            />
            <img
              src={"../../../static/Images/awards-associations/star-rating.png"}
              alt="rating"
            />
            <img
              src={"../../../static/Images/awards-associations/star-rating.png"}
              alt="rating"
            />
            {props.rating == "5" ? (
              <img
                src={"../../../static/Images/awards-associations/star-rating.png"}
                alt="rating"
              />
            ) : (
              <img
                src={"../../../static/Images/awards-associations/half-star-rating.png"}
                alt="rating"
              />
            )}
          </div>
        ) : (
          ""
        )}
        <div dangerouslySetInnerHTML={{ __html: props.hoverBlockDetail }}></div>
      </div>
    </div>
  );
};

export default ImageBlock;
