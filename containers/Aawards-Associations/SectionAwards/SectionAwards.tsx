//@ts-nocheck
import React from "react";
import Container from "../../../components/Container/Container";
import style from "../styles.module.scss";
import ImageBlock from "./ImageBlock";

const SectionAwards = (props) => {
  return (
    <section className={`${style.SectionAwards}`}>
      <Container>
        <p>
          For the past decade, Intuz has been winning several prestigious awards
          and accolades for its exceptional mobile development expertise,
          innovation, and maturing and thriving ideas into award-winning digital
          products.
        </p>
      </Container>
      <div>
        <div className="row">
          <ImageBlock
            imgSrc={"iso_award.png"}
            hoverBlockDetail="<p>Intuz is an ISO 9001:2015 certified company for upkeeping high-quality standards.</p>"
            className="grey"
          />
          <ImageBlock
            imgSrc={"clutch.png"}
            hoverBlockDetail="<p>Rated as a top mobile app development company with 4.6/5 on Clutch </p><a href='https://clutch.co/profile/intuz' title='More' target='_blank'>More</a>"
            rating="4.6"
          />
          <ImageBlock
            imgSrc={"aws_awards.png"}
            hoverBlockDetail='<p>We are an AWS certified consulting partner and a DevOps implementation company.</p><a href="http://www.aws-partner-directory.com/PartnerDirectory/PartnerDetail?Name=Intuz+Inc" target="_blank" title="More">More</a>'
            className="grey"
          />
          <ImageBlock
            imgSrc={"appfutura.png"}
            hoverBlockDetail='<p>Rated as a global top app developer with 5/5 rating on AppFutura.</p><a href="https://www.appfutura.com/developers/intuz" target="_blank" title="More">More</a>'
            className="grey"
          />

          <ImageBlock
            imgSrc={"agency_awards.png"}
            hoverBlockDetail=' <p>Featured by AgencySpotter with 5/5 client ratings.</p><a href="https://www.agencyspotter.com/intuz" target="_blank" title="More">More</a>'
            rating="5"
          />

          <ImageBlock
            imgSrc={"msme.png"}
            hoverBlockDetail="<p>Intuz is a proud member of Micro, Small & Medium Enterprises association.</p>"
            className="grey"
          />

          <ImageBlock
            imgSrc={"mobile-app-developer.png"}
            hoverBlockDetail=' <p>UpCIty recognized us as top mobile app developers in San Jose and San Francisco, USA.</p><a href="https://upcity.com/local-marketing-agencies/profiles/intuz-inc-1" target="_blank" title="More">More</a>'
          />

          <ImageBlock
            imgSrc={"mobile-app-daily.png"}
            hoverBlockDetail=' <p>Ranked #4 in top React native developers of 2019 by MobileAppsDaily.</p><a href="https://www.mobileappdaily.com/reports/top-react-native-developers-2018" target="_blank" title="More">More</a>'
          />

          <ImageBlock
            imgSrc={"gcci.png"}
            hoverBlockDetail="<p>Intuz is an esteemed member of Gujarat chamber of commerce & industry.</p>"
            className="grey"
          />

          <ImageBlock
            imgSrc={"goodfirms.png"}
            hoverBlockDetail='<p>GoodFirms have featured us as top mobile app developer across the globe.</p><a href="https://www.goodfirms.co/companies/view/6658/intuz" target="_blank" title="More">More</a>'
          />

          <ImageBlock
            imgSrc={"app-developer.png"}
            hoverBlockDetail='<p>We were recognized as top app mobile developers of 2018 by The Manifest.</p><a href="https://themanifest.com/app-development/companies" target="_blank" title="More">More</a>'
            className="grey"
          />

          <ImageBlock
            imgSrc={"design_rush_1_e52c0f0ac9.png"}
            hoverBlockDetail='<p>Design Rush recognized Intuz as one of the most popular software development agency in San Francisco, USA.</p><a href="https://www.designrush.com/agency/software-development/california/san-francisco" target="_blank" title="More">More</a>'
          />
          <ImageBlock
            imgSrc={"top_app_dev_logo_2840e83682.png"}
            hoverBlockDetail='<p> Intuz Rated as Top App Development Companies 2021 by Business Of Apps Research.</p><a href="https://www.businessofapps.com/app-developers" target="_blank" title="More">More</a>'
          />
          <ImageBlock
            imgSrc={"mobileapp_daily_logo_1_bb81b83894.png"}
            hoverBlockDetail='<p>Intuz Featured in the Report of Top React Native App Development Companies 2021 conducted by Mobile App Daily.</p><a href="https://www.mobileappdaily.com/mobile-app-development/react-native/companies" target="_blank" title="More">More</a>'
          />
        </div>
      </div>
    </section>
  );
};

export default SectionAwards;
