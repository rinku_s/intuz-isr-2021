//@ts-nocheck
import React from 'react';
import ClientSpeakCarousel from '../../../components/ClientSpeakCarousel/ClientSpeakCarousel';
import classes from './styles.module.scss';
const ClientSpeakSection = (props) => {
    return (
       <section className={classes.ClientSpeakSection}>
           <ClientSpeakCarousel/>
       </section>
    )
}

export default ClientSpeakSection
