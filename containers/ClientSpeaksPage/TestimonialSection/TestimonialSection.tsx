//@ts-nocheck
//@ts-nocheck
import LinkButton from 'components/UI/LinkButton/LinkButton';
import { getClientSpeakData } from 'lib/api';
import React, { useState } from 'react';
import Testimonial from '../../../components/Testimonial/Testimonial';
import { cdn } from '../../../config/cdn';
import classes from './styles.module.scss';

export const allPostsQueryVars = {
  start: 0,
  limit: 30
}
const TestimonialSection = (props) => {
  const [data, setData] = useState(props.tesimonials);
  const [loading, setLoading] = useState(false);

  const [page, setPage] = useState(30);


  let areMoreTestimonials = data.length < props.tesimonialsConnection.aggregate.count

  return (
    <article className={classes.Testimonials}>
      <h1>Featured Client Speak</h1>
      <h3>Intuz is proud of the trust it has earned from some of the most reputed organizations in the world</h3>
      {data?.map((tm, i) => (
        <Testimonial
          key={i}
          image={tm.client_image ? cdn(tm.client_image.name) : false}
          name={tm.name}
          designation={tm.designation}
          testimonial={tm.testimonial}
        />
      ))}
      <div style={{ padding: "5rem", textAlign: "center" }}>
        {areMoreTestimonials && (
          <LinkButton variation="blackBorder" onClick={async () => {
            setLoading(true)
            const d = await getClientSpeakData(page + 1);
            setData([...data, ...d.tesimonials]);
            setPage(page + 30);
            setLoading(false)
          }} disabled={loading}>
            {loading ? 'Loading...' : 'View More'}
          </LinkButton>
        )}
      </div>
    </article>
  )
}
export default TestimonialSection
