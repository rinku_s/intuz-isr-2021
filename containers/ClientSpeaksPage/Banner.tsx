import React from "react";
import Container from "components/Container/Container";
const ClientSpeakBanner = () => {
  return (
    <section className="h-[720px] flex items-center justify-center">
      <Container>
        <h1 className="text-5xl md:text-7xl lg:text-[7.2rem] leading-normal text-center font-bold text-[#141414]">
          Glimpses of&nbsp;
          <span
            className="bg-clip-text text-transparent bg-gradient-to-r from-[#9A32AB] to-[#3CECEC]"
            style={
              {
                //   background:
                //     "linear-gradient(226.05deg, #9A32AB 0%, #3CECEC 91.48%)",
              }
            }
          >
            What Our Clients Think
          </span>
          &nbsp; About The Services We Delivered
        </h1>
        <p className=" text-[2.4rem] text-center leading-normal text-[#525252]">
          Clients love working with the talented pool of the Intuz team,
          Testimonials are just the Highlights of their Happiness &
          Satisfaction!
        </p>
      </Container>
    </section>
  );
};

export default ClientSpeakBanner;
