//@ts-nocheck
import React from 'react'
import HtmlParser from 'html-react-parser'
import Container from '../../components/Container/Container'
import classes from './styles.module.scss'

const PrivacyPage = (props) => {
    return (
        <article className={classes.PrivacyPage}>
            <Container>
                <h1>{props.title}</h1>
                {HtmlParser(`${props.content}`)}
            </Container>
        </article>
    )
}

export default PrivacyPage
