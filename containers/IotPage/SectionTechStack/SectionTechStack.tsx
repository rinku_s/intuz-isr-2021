//@ts-nocheck


import gsap from 'gsap';
import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import { cdn } from '../../../config/cdn';
import style from './styles.module.scss';

const SectionTechStack = (props) => {
    // setOpacity(`.${style.TechStack} h2, .${style.TechStack} p`);

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(`.${style.TechStack} h2`, 1, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' })
            .fromTo(`.${style.TechStack} p`, 0.6, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' }, 0.4);
    }

    return (
        <section className={style.TechStack}>

            <Container>
                <PrimaryHeading>Technology Stack</PrimaryHeading>
                <SecondaryHeading>Developing Internet of Things Solutions with modern technology stack</SecondaryHeading>
                <div className={style.TechGroup}>
                    {
                        props.content && props.content.map((content) =>
                            <div key={content.id}>
                                <img src={cdn(`${content.icon.name}?auto=format`)} alt={content.icon.name} />
                                {content.tech_brief_intro && <span>{content.tech_brief_intro}</span>}
                            </div>
                        )
                    }
                </div>
            </Container>

        </section >
    )
}

export default SectionTechStack