//@ts-nocheck
import React from 'react'
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import IotRoadMapComponent from '../../../components/IOTRoadMapComponent/IotRoadMapComponent';
import classes from './styles.module.scss';
const RoadMapSection = () => {
    return (
        <section className={classes.RoadMapSection}>
            <Container>
                <PrimaryHeading>Roadmap of our IoT App Development Process </PrimaryHeading>
                <SecondaryHeading>Build your journey towards developing a successful end user centric IoT Mobile Applications</SecondaryHeading>
                <IotRoadMapComponent/>
                <div className={`${classes.Link} text-center mt-5 pt-5`}>
                <p>Detail Guide on IoT Product Development Stages</p>
                <a href="/iot-product-development-guide">Click Here</a>
                </div>

            </Container>
        </section>
    )
}

export default RoadMapSection
