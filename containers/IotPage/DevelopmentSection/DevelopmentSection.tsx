//@ts-nocheck

import gsap from "gsap";
import React from "react";
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import IconBlock from './IconBlock';
import style from './styles.module.scss';

const DevelopmentSection = (props) => {
    // setOpacity(`.${style.DevSection} h2, .${style.DevSection} p`);

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(`.${style.DevSection} h2`, 1, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' })
            .fromTo(`.${style.DevSection} p`, 0.6, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' }, 0.4);
    }

    return (
        <section className={style.DevSection}>

            <Container>
                <PrimaryHeading>IoT Development Company Experts in Web, Mobile Apps with Firmware Development</PrimaryHeading>
                <SecondaryHeading>IoT based Firmware Consulting, Development and Support Services</SecondaryHeading>
                <div className={`${style.ProductDiv} row`}>
                    {props.wifiProducts.length > 0 && <IconBlock title='WiFi Enable Product' content={props.wifiProducts} />}
                    {props.bluetoothProducts.length > 0 && <IconBlock title='Bluetooth Enabled Product' content={props.bluetoothProducts} />}
                </div>
            </Container>

        </section >
    )
}

export default DevelopmentSection;

