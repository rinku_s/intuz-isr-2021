//@ts-nocheck

import React from "react";
import LazyLoad from 'react-lazyload';
import { cdn } from '../../../config/cdn';
import style from './styles.module.scss';

const IconBlock = (props) => {
    return(
        <div className='col-md-6'>
            <h3>{props.title}</h3>
            <div className='row'>
                {
                    props.content.map((content, index) =>
                        <div className={`${style.IconBlock} col-md-6 col-sm-6`} key={index}>
                            <LazyLoad height={300} offset={300}>
                                <img src={cdn(`${content.icon.name}?auto=format`)} alt={content.icon.name} />
                            </LazyLoad>
                            <h4>{content.name}</h4>
                        </div>    
                    )
                }
            </div>
        </div>
    )
}

export default IconBlock;

