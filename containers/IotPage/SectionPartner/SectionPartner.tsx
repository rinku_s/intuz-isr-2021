//@ts-nocheck




import gsap from 'gsap';
import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import { cdn } from '../../../config/cdn';
import style from './styles.module.scss';

const SectionPartner = (props) => {
    // setOpacity(`.${style.Partners} h2, .${style.Partners} p`);

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(`.${style.Partners} h2`, 1, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' })
            .fromTo(`.${style.Partners} p`, 0.6, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' }, 0.4);
    }

    return (
        <section className={style.Partners}>

            <Container>
                <PrimaryHeading>Partners We Work With</PrimaryHeading>
                <SecondaryHeading>Let us join hands to develop Smart IoT Application </SecondaryHeading>
                <div className={style.PartnerGroup}>
                    {
                        props.content && props.content.map((content) =>
                            <div key={content.id}>
                                <img src={cdn(content.logo.name)} alt={content.logo.name} />
                                {content.name && <h4>{content.name}</h4>}
                            </div>
                        )
                    }
                </div>
            </Container>

        </section >
    )
}

export default SectionPartner