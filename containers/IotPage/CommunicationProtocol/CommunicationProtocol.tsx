//@ts-nocheck

import gsap from 'gsap';
import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import Protocols from '../../../components/Protocols/Protocols';
import style from './styles.module.scss';

const CommunicationProtocol = (props) => {
    // setOpacity(`.${style.CommunicationProtocol} h2, .${style.CommunicationProtocol} p`);

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(`.${style.CommunicationProtocol} h2`, 1, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' })
            .fromTo(`.${style.CommunicationProtocol} p`, 0.6, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' }, 0.4);
    }

    return (
        <section className={style.CommunicationProtocol}>

            <Container>
                <PrimaryHeading>Communication Protocols</PrimaryHeading>
                <SecondaryHeading>Custom development & implementation based on objectives and goals</SecondaryHeading>
                <Protocols {...props} />
            </Container>

        </section >
    )
}

export default CommunicationProtocol;