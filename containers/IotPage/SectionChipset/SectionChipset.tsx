//@ts-nocheck

import gsap from 'gsap';
import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import { cdn } from '../../../config/cdn';
import windowWidth from '../../../hooks/windowWidth';
import style from './styles.module.scss';

const SectionChipset = (props) => {
    let windowSize = windowWidth();
    // setOpacity(`.${style.Chipset} h2, .${style.Chipset} p`);

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(`.${style.Chipset} h2`, 1, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' })
            .fromTo(`.${style.Chipset} p`, 0.6, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' }, 0.4);
    }


    return (
        <section className={style.Chipset}>

            <Container>
                <PrimaryHeading>Chipsets for Consideration</PrimaryHeading>
                <SecondaryHeading>Identification of the right chipset will help build scalable products</SecondaryHeading>
                <img src={cdn('chipset.png?auto=format&windth=' + `${windowSize > 767 ? '1000' : '767'}`)} alt='chipset.png' />
            </Container>

        </section >
    )
}

export default SectionChipset