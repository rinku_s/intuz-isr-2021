//@ts-nocheck


import gsap from "gsap";
import ScrollTrigger from 'gsap/dist/ScrollTrigger';
import React, { useEffect } from "react";
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import IotCasestudy from '../../../components/IotCasestudy/IotCasestudy';
import IotCaseStudy2 from "../../../components/IotCaseStudy2/IotCaseStudy2";
import IotCaseStudy3 from "../../../components/IotCaseStudy2/IotCaseStudy3";
import style from './styles.module.scss';

gsap.registerPlugin(ScrollTrigger);

const FeatureSection = (props) => {
    // let windowSize = windowWidth();
    //// setOpacity(`.${style.FeatureSection} h2, .${style.FeatureSection} p`);

    const whiteWebIcon = [{
        'src': '/static/Images/icons/ic-web-white.svg',
        'alt': 'Available on Web',
        'title': 'Available on Web',
        'variation': "circle_img",
    }];

    const whiteAndroidIcon = [{
        'src': '/static/Images/icons/and-ic.svg',
        'alt': "Available on android",
        'title': "Available on android",
        'variation': "circle_img",
    }];

    const whiteAppleIcon = [{
        'src': '/static/Images/icons/ios-ic.svg',
        'alt': 'Available on IOS',
        'title': 'Available on IOS',
        'variation': "circle_img",
    }];


    function backgroundColor() {
        const scrollColorElems = document.querySelectorAll(".cst");
        scrollColorElems.forEach((colorSection, i) => {
            // console.log(colorSection.offsetTop);
            let bo = colorSection.offsetTop + colorSection.offsetHeight
            if ((window.pageYOffset + 500) >= colorSection.offsetTop && (window.pageYOffset + 500) < bo) {
                onEnter(colorSection.dataset.scrollcolor);
            }
        });
    }


    useEffect(() => {

        window.addEventListener('scroll', backgroundColor)

        return () => {
            window.removeEventListener('scroll', backgroundColor)
        }

    }, [])

    function onEnter(color) {
        // console.log("OnEnter==>", color);
        gsap.to("#ftiot", 0.5, { backgroundColor: color, overwrite: 'auto', ease: "power3.out" })
    }

    function onLeaveBack(color) {
        // console.log("onLeaveBack==>", color);
        gsap.to("#ftiot", { backgroundColor: color, overwrite: 'auto' })
    }

    function onEnterBack(color) {
        // console.log("onEnterBack==>", color);
        gsap.to("#ftiot", { backgroundColor: color, overwrite: 'auto' })
    }


    return (
        // <Waypoint onEnter={handleElementEnter} onLeave={handleElementExit}>
        <section className={`${style.FeatureSection}`} id="ftiot">
            <Container>
                <PrimaryHeading style={{ color: '#fff' }}>Featured IoT Application Project</PrimaryHeading>
                <SecondaryHeading style={{ color: '#fff' }}>Firmware development, Hardware integration, Web Application & Mobile Application Development with Cloud Deployment</SecondaryHeading>
            </Container>
            <IotCasestudy
                id={"spec-pump"}
                backColor='#02AACA'
                image='iotpage-speckpump.png'
                logo='speckpump-logo.png'
                logoTitle="Speck pumps"
                desc="IoT Enabled Smart Pool Equipments Controlling & Automation System Development"
                IconArray={[whiteWebIcon[0], whiteAppleIcon[0], whiteAndroidIcon[0]]}
                href="/case-studies/speck-pumps"
            />
            <IotCasestudy
                id={"sgc"}
                backColor='#594441'
                image='iot-sgc.png'
                logo='sgc-case-logo.png'
                logoTitle="sgc"
                desc="Enterprise Level Fuel Station Operations Automation App for a Large Oil Marketing Company."
                IconArray={[whiteAppleIcon[0], whiteAndroidIcon[0]]}
                href="/case-studies/sgc"
                variation='sgc'
            />
            <IotCaseStudy2 />
            <IotCaseStudy3 />
        </section>
        // </Waypoint>
    )
}

export default FeatureSection;

