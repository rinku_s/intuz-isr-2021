//@ts-nocheck


import gsap from 'gsap';
import React from 'react';
import CommunicationLayers from '../../../components/CommunicationLayers/CommunicationLayers';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import style from './styles.module.scss';

const SectionCommunicationLayer = (props) => {

    // setOpacity(`.${style.SectionCommLayer} h2, .subtitle`);

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(`.${style.SectionCommLayer} h2`, 1, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' })
            .fromTo(`.subtitle`, 0.6, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' }, 0.4);
    }

    return (
        <section className={style.SectionCommLayer}>

            <Container>
                <PrimaryHeading>Communication (Transport) Layers</PrimaryHeading>
                <SecondaryHeading className='subtitle'>Identifying the business need. Validating the concept with POC and MVP development</SecondaryHeading>
                <p className={style.ShortDesc}>The transport layer is responsible for an end to end communication between the systems including the management of the error correction, providing quality and reliability to the end-user.</p>
                <CommunicationLayers {...props} />
            </Container>

        </section >
    )
}

export default SectionCommunicationLayer;