//@ts-nocheck




import gsap from "gsap";
import React from "react";
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import BuildTabContent from '../../../components/SolutionTab/BuildTabContent';
import style from './styles.module.scss';


const SolutionSection = (props) => {
    // setOpacity(`#solution h2, #solution p`);

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(`#solution h2`, 1, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' })
            .fromTo(`#solution p`, 0.6, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' }, 0.4);
    }

    return (

        <section className={style.SolutionSection} id="solution">
            <Container>
                <PrimaryHeading>IoT App Development Solutions</PrimaryHeading>
                <SecondaryHeading>Redefining User Experience. Improve Business Productivity & Efficiency</SecondaryHeading>
                <div className='row'>
                    {
                        props.content.map(content =>
                            <BuildTabContent
                                icon={content.icon.name}
                                title={content.title}
                                description={content.description}
                                key={content.id}
                                page='iot'
                            />)
                    }
                </div>
            </Container>
        </section>
        // </WaypointOnce >
    )
}

export default SolutionSection;

