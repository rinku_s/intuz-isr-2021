//@ts-nocheck
import React from 'react';
import Container from '../../../components/Container/Container';
import MobileApp from '../../../components/DevOpsAndFSComps/Banner/MobileAppBanner';
import classes from './styles.module.scss';
const HeroSection = () => {


    return (
        <section className={`${classes.HeroSection}`}>
            <Container>
                <MobileApp/>
            </Container>
        </section>
    )
}

export default HeroSection
