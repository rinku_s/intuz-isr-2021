//@ts-nocheck
import React from 'react';
import Container from '../../../components/Container/Container';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import LinkButton from '../../../components/UI/LinkButton/LinkButton';
import classes from './styles.module.scss';
const EstimateCta = () => {
    return (
        <section className={classes.EstimateCta}>
            <Container style={{textAlign:"center"}}>
                <SecondaryHeading fontSize="3.0rem" style={{color:"#000", fontWeight:"300"}}>
                Having exclusive project requirements? We would be glad to serve you.
                </SecondaryHeading>
                <LinkButton href="/get-started" variation="purpleBtn" className="mt-sm">
                    Request An Estimate
                </LinkButton>
            </Container>
        </section>
    )
}

export default EstimateCta
