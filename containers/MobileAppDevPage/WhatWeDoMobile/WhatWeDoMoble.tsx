//@ts-nocheck
import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import MobileWhatWeDo from '../../../components/MobileWhatWeDo/MobileWhatWeDo';
import classes from './styles.module.scss';

const WhatWeDoMoble = (props) => {
    return (
        <section className={classes.WhatWeDoMoble}>
            <Container>
            <PrimaryHeading>
                What We Do?
            </PrimaryHeading>
            <MobileWhatWeDo whatwedomobiles={props.whatwedomobiles}/>
            </Container>
        </section>
    )
}

export default WhatWeDoMoble
