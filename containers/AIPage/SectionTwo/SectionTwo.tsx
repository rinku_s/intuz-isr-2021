//@ts-nocheck
import gsap from 'gsap'
import React, { useState } from 'react'
import Container from '../../../components/Container/Container'
import FormPopup from '../../../components/GetStartedForm/FormPopup'
import LinkButton from '../../../components/UI/LinkButton/LinkButton'
import Main from '../Headings/Main'
import Paragraph from '../Headings/Paragraph'
import classes from './styles.module.scss'
export const SectionTwo = () => {

    // const anime = animation(abc);
    const [show, setShow] = useState(false)
    function play() {
        const tl = gsap.timeline({ paused: true });
        tl.from(`.${classes.SectionTwo} h3`, 1, { y: -30, autoAlpha: 0, ease: "Power2.easeOut" })
            .from(`.${classes.SectionTwo} p`, 0.8, { y: -30, autoAlpha: 0, ease: "Power2.easeOut" }, "-=0.4")
            .from(`.${classes.SectionTwo} a`, 0.8, { y: -30, autoAlpha: 0, ease: "Power2.easeOut" }, "-=0.4");
        tl.play()
    }
    return (

        <section className={classes.SectionTwo}>
            <Container>
                <Main>Take A Demo</Main>
                <Paragraph>Would you like to try our readily available chatbot application? Explore the wow features of the product that automates your sales and marketing processes.</Paragraph>
                <LinkButton variation="purpleBtn" onClick={() => setShow(true)}>Request a Demo</LinkButton>
            </Container>
            <FormPopup
                thankyoupage="ai-thankyou"
                subject="AI Contact Us Request"
                leadsource="AI Demo Request"
                show={show}
                onHide={() => setShow(false)}

            />
        </section>
        // </WaypointOnce >
    )
}
