//@ts-nocheck
import gsap from 'gsap';
import React, { useEffect } from 'react';
import Container from '../../../components/Container/Container';
import InnovationContainer from '../../../components/InnovationContainer/InnovationContainer';
import Main from '../Headings/Main';
import classes from './styles.module.scss';

const SectionFive = (props) => {

    useEffect(() => {
        gsap.set(`.${classes.SectionFive} h3`, { opacity: 1 })
    }, [])

    function play() {
        const tl = gsap.timeline({ paused: true })
        tl.fromTo(`.${classes.SectionFive} h3`, 0.8, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: "Power2.easeOut" })
        tl.play();
    }

    return (

        <section className={classes.SectionFive}>
            <Container>
                <Main style={{ textAlign: "center" }}>Connecting Dots Taking AI Innovations to New Heights</Main>
                {props.innovations.map(inv => (
                    <InnovationContainer
                        key={inv.title}
                        {...inv}
                    />
                ))}
            </Container>
        </section>
    )
}

export default SectionFive
