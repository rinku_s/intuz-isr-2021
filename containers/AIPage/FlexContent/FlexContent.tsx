//@ts-nocheck
//@ts-nocheck
import { myLoader } from 'config/image-loader';
import gsap from 'gsap';
import Image from 'next/image';
import React, { useEffect } from 'react';
import Main from '../Headings/Main';
import Paragraph from '../Headings/Paragraph';
import classes from './styles.module.scss';
const FlexContent = (props) => {
    const con = React.useRef();

    useEffect(() => {
        gsap.set(con.current.getElementsByTagName('h3'), { opacity: 1 })
        gsap.set(con.current.getElementsByTagName('p'), { opacity: 1 })
    }, [])


    function play() {
        const tl = new gsap.timeline({ paused: true });
        tl.fromTo(con.current.getElementsByTagName('h3'), 0.8, { y: 20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: "Power3.easeOut" })
            .fromTo(con.current.getElementsByTagName('p'), 0.8, { y: 20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: "Power3.easeOut" });
        tl.play();
    }
    return (

        <div className={classes.FlexContent} ref={con}>
            <div>
                <Main>{props.heading}</Main>
                <Paragraph>{props.paragraph}</Paragraph>
            </div>
            <div>
                <Image src={props.image.name} alt={props.heading} loader={myLoader} layout={'intrinsic'} height={409} width={486} />
            </div>
            <style jsx>
                {`
                    .${classes.FlexContent} {
                        flex-direction:${props.reverse ? "row-reverse" : ""};
                    }
                `}
            </style>
        </div>
    )
}

export default FlexContent
