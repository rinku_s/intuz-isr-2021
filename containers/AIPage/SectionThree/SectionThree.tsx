//@ts-nocheck
import React from 'react';
import ChatbotComponent from '../../../components/ChatbotComponent';
import Container from '../../../components/Container/Container';
import Main from '../Headings/Main';

const SectionThree = (props) => {
    return (
        <section>
            <Container>
                <Main style={{textAlign:'center'}}>
                    Exceptional Offerings of Intuz Chatbot
                </Main>
                <ChatbotComponent offerings={props.offerings}/>
            </Container>
            <style jsx>
                {`
                    section{
                        padding: 10rem 0;
                    }
                `}
            </style>
        </section>
    )
}

export default SectionThree
