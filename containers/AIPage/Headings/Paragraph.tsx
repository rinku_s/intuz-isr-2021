//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
const Paragraph = (props) => {
    return (
        <p className={classes.paragraph}>
            {props.children}
        </p>
    )
}

export default Paragraph
