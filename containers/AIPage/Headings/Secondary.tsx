//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
const Secondary = (props) => {
    return (
        <h4 className={classes.secondary}>
           {props.children} 
        </h4>
    )
}

export default Secondary
