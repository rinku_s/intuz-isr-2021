//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
const Main = (props) => {
    return (
        <h3 className={classes.mainheading} style={props.style}>
            {props.children}
        </h3>
    )
}

export default Main
