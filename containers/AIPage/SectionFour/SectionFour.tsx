//@ts-nocheck
import React from 'react'
import Container from '../../../components/Container/Container'
import FlexContent from '../FlexContent/FlexContent'
import classes from './styles.module.scss'
const SectionFour = (props) => {
    return (
        <section className={classes.SectionFour}>
            <Container>
                {props.content.map(cc=>(
                <FlexContent
                    key={cc.heading}
                    {...cc}
                />
                ))}
            </Container>
        </section>
    )
}

export default SectionFour
