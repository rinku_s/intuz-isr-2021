//@ts-nocheck
import { myLoader } from 'config/image-loader';
import gsap from 'gsap';
import Image from 'next/image';
import React from 'react';
import { cdn } from '../../../config/cdn';
import windowWidth from '../../../hooks/windowWidth';
import classes from './styles.module.scss';
export const Hero = () => {

    //  animation(abc);
    function play() {
        const tl = gsap.timeline({ paused: true, delay: 0.8 })
        tl.from(`.${classes.Hero} h1`, 0.8, { y: -10, autoAlpha: 0, ease: "Power2.easeOut" })
            .fromTo(`.${classes.Hero}`, 0.8, { backgroundPosition: "-600% -100%" }, { backgroundPosition: "center center", ease: "Power2.easeOut" }, "-=0.4")
            .from(`.${classes.Hero} h2`, 0.8, { y: -20, autoAlpha: 0, ease: "Power2.easeOut" }, "-=0.4")
            .from(`.${classes.Hero} p`, 0.8, { y: -30, autoAlpha: 0, ease: "Power2.easeOut" }, "-=0.4")
        tl.play();
    }

    let windowSize = windowWidth();
    let backImage = { background: `url(${cdn('aibanner.png')}?fm=png&auto=format${windowSize < 768 ? '&w=1300' : ''}) no-repeat center center` }

    return (


        <section className={`${classes.Hero} relative h-[900px] `} >

            <div className="absolute h-full w-full" style={{ zIndex: -1 }}>
                <Image objectPosition={'top center'} className="z-0" layout="fill" objectFit="cover" loader={myLoader} src={'aibanner.png'} alt='aibanner' />
            </div>
            <div className={classes.Content}>
                <h1>
                    Artificial Intelligence
                </h1>
                <h2>
                    Amalgamation of Mobility and AI Based
                    Bots Transforming Business Landscape
                </h2>
                <p>Embracing the disruptive artificial intelligence technology, Intuz builds
                intuitive and intelligent chatbots to enhance client engagement and
boost up the operational efficiency of organizations.</p>
            </div>
        </section>
        // </WaypointOnce >
    )
}