//@ts-nocheck
import React from 'react';
import Moment from 'react-moment';
import BlogBreadcrumb from '../../../components/BlogBreadcrumb/BlogBreadcrumb';
import Container from '../../../components/Container/Container';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import { cdn } from '../../../config/cdn';
import classes from './styles.module.scss';

const ResourcesSectionStyle = (props) => {
    return (
        <section className={`${classes.HeroBlog} ${classes[props.variation]}`} style={{ ...props.style}}>
            <Container>
                <BlogBreadcrumb link={props.link ? `/blog/category/${props.link}` : ''} label={props.blogCategory} />
                <h1>{props.heading}</h1>
                {props.subtitle && <SecondaryHeading font="s" style={{color:'#fff'}}>{props.subtitle}</SecondaryHeading>}
                {props.authorData && <div className={classes.authorData}>
                    <img src={cdn(props.authorData.image)} alt={props.authorData.image} />
                    <span>{props.authorData.name}</span>
                    <span className={classes.date}><Moment format="MMM DD, YYYY">{props.authorData.date}</Moment></span>
                </div>
                }
                {props.children}
            </Container>
            <style jsx>
                {`
                    section{
                        background-image:linear-gradient(to bottom, rgba(0, 0, 0, 0.30), rgba(0, 0, 0, 0.40)), url(${props.backImage}?auto=format);
                    }

                    @media only screen and (max-width: 767px) { 
                        section{
                            background-image:linear-gradient(to bottom, rgba(0, 0, 0, 0.30), rgba(0, 0, 0, 0.40)), url(${props.backImage}?auto=format&w=800);
                        }
                     };
                `}
            </style>
        </section>
    )
}

export default ResourcesSectionStyle   
