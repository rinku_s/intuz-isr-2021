//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
import Container from '../../../components/Container/Container'
const Subscription = () => {
    return (
        <section className={classes.Subscription}>
            <Container>
                <h2>Industry insights you won’t delete. Delivered to your inbox weekly.</h2>
                <form>
                    <input type="email" placeholder="Your Email Address"/>
                    <button type="submit">Subscribe</button>
                </form>
            </Container>
        </section>
    )
}

export default Subscription
