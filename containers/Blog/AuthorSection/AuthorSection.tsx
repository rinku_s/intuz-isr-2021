//@ts-nocheck
import React from 'react';
import SVG from 'react-inlinesvg';
import { cdn } from '../../../config/cdn';
import classes from './styles.module.scss';
const AuthorSection = ({ author }) => {
    return (
        <section className={classes.AuthorSection}>
            <div className="container">
                <div className={classes.Flex}>
                    
                <img src={cdn(author.image.name)} alt={author.name}/>
                <div>
                    <h3>{author.name}</h3>
                    <p dangerouslySetInnerHTML={{__html:author.authorInfo}}></p>
                    { author.twitterUrl ? <a href={author.twitterUrl} target="_blank">
                        <SVG src="/static/Images/addressicons/twitter.svg"/></a> : '' }

                    { author.linkedinUrl ? <a href={author.linkedinUrl} target="_blank">
                        <SVG src="/static/Images/addressicons/linkedin-squ.svg"/></a>
                     : ''}
                </div>
                </div>
            </div>
        </section>
    )
}

export default AuthorSection
