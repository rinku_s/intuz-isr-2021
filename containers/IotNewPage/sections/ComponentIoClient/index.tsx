import React from "react";
import Container from "components/Container/Container";
import styles from "./styles.module.scss";
const index = () => {
  const iconList = [
    {
      icon: "mercedes",
    },
    {
      icon: "Bitmap",
    },
    {
      icon: "Bosch",
    },
    {
      icon: "Frame",
    },
    {
      icon: "jll",
    },
    {
      icon: "RFI",
    },
  ];
  return (
    <section className={styles.main}>
      <Container>
        <p>Trusted By </p>
        <div className={styles.logoList}>
          {iconList.map((list, i) => (
            <div key={i}>
              <img src={`/images/iotClient/${list.icon}.png`} alt={list.icon} />
            </div>
          ))}
        </div>
      </Container>
    </section>
  );
};

export default index;
