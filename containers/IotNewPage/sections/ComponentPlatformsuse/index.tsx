import React from "react";
import Container from "../../../../components/Container/Container";
import Styles from "./styles.module.scss";
const index = () => {
  const PlatformsList = [
    {
      id: 1,
      image: 1,
      title: "AWS IoT",
    },
    {
      id: 2,
      image: 2,
      title: "SigFox",
    },
    {
      id: 3,
      image: 3,
      title: "IBM Bluemix",
    },
    {
      id: 4,
      image: 4,
      title: "Google Cloud IoT",
    },
    {
      id: 5,
      image: 5,
      title: "Microsoft Azure IoT",
    },
  ];
  return (
    <section className={Styles.sectionPlatform}>
      <Container>
        <h2>Platforms We Use</h2>
        <p>Because a strong product needs a strong foundation</p>
        <div className={Styles.listPlatform}>
          {PlatformsList.map((list) => (
            <div key={list.id}>
              <img src={(`/static/IotPlatforms/${list.image}.png`)} alt="platforms-use" />
              <p>{list.title}</p>
            </div>
          ))}
        </div>
      </Container>
    </section>
  );
};

export default index;
