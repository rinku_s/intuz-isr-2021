import React from "react";
import Container from "../../../../components/Container/Container";
import Styles from "./styles.module.scss";
import { MediaContextProvider, Media } from "config/responsiveQuery";
import ArrowTab from "components/ArrowTab/ArrowTab";
const index = () => {
  const SensorsItem = [
    {
      id: 1,
      name: "Temperature Sensors",
      icon: "Frame2062",
    },
    {
      id: 2,
      name: "Humidity Sensors",
      icon: "Frame2062-1",
    },
    {
      id: 3,
      name: "Proximity Sensors",
      icon: "Frame2062-2",
    },
    {
      id: 4,
      name: "Pressure Sensors",
      icon: "Frame2062-3",
    },
    {
      id: 5,
      name: "Motion & Velocity Sensors",
      icon: "Frame2062-4",
    },
    {
      id: 6,
      name: "Accelerometers",
      icon: "Frame2062-5",
    },
    {
      id: 7,
      name: "Optical Sensors",
      icon: "Frame2062-6",
    },
    {
      id: 8,
      name: "Light Sensors",
      icon: "Frame2062-7",
    },
    {
      id: 9,
      name: "Magnetic Sensors",
      icon: "Frame2062-8",
    },
    {
      id: 10,
      name: "Water Quality Sensors",
      icon: "Frame2062-9",
    },
    {
      id: 11,
      name: "Smock Sensors",
      icon: "Frame2062-10",
    },
    {
      id: 12,
      name: "Chemical Sensors",
      icon: "Frame2062-11",
    },
    {
      id: 13,
      name: "Infrared Sensors",
      icon: "Frame2062-12",
    },
    {
      id: 14,
      name: "Flow & Gas",
      icon: "Frame2062-13",
    },
    {
      id: 15,
      name: "Acoustics & Noise Sensors",
      icon: "Frame2062-14",
    },
    {
      id: 16,
      name: "Radiation Sensors",
      icon: "Frame2062-15",
    },
  ];

  return (
    <section className={Styles.mainSensors}>
      <Container>
        <MediaContextProvider>
          <h2>Sensors We Work With</h2>
          <p className="text-center">
            We indeed make it possible to connect everyday things to the
            internet
          </p>
          <Media greaterThan="sm">
            <div className={`${Styles.sensorsLists}`}>
              {SensorsItem.map((list) => (
                <div key={list.id} className={Styles.sensorsItem}>
                  <img
                    src={`/images/Iotsensors/${list.icon}.png`}
                    alt="list-icon"
                  />
                  <p className="text-left">{list.name}</p>
                </div>
              ))}
            </div>
          </Media>
          <Media lessThan="sm">
            <ArrowTab
              containerClass={Styles.sensorsLists}
              arrayData={SensorsItem}
              slice={4}
              buttonContainerClass={"text-center"}
            >
              {(list) => (
                <div key={list.id} className={Styles.sensorsItem}>
                  <img
                    src={`/images/Iotsensors/${list.icon}.png`}
                    alt="list-icon"
                  />
                  <p className="text-left">{list.name}</p>
                </div>
              )}
            </ArrowTab>
          </Media>
        </MediaContextProvider>
      </Container>
    </section>
  );
};

export default index;
