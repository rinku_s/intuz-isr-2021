import React from "react";
import Container from "../../../../components/Container/Container";
import styles from "./styles.module.scss";
import { Swiper, SwiperSlide } from "swiper/react";
import SwiperCore, { Pagination } from "swiper/core";
// // import "swiper/swiper.c";

SwiperCore.use([Pagination]);

const ComponentClientsSays = () => {
  const feedbackList = [
    {
      id: 1,
      article:
        "Intuz quickly delivered products that would take other agencies months to develop. They followed a transparent workflow and adapted to changes to the project scope. The entire team was friendly and highly skilled, making them an outstanding partner.",
      image: "image 68",
      name: "Bruce Francois",
      address: "President, myPurpose NETWORK",
      country: "USA",
      slide: "slide-1",
    },
    {
      id: 2,
      article:
        "Intuz did a fabulous job completing the cxo-talk.com website. It was a complicated project and the end-result is truly excellent. Thank you, Intuz!",
      image: "image 67",
      name: "Michael Krigsman",
      address: "Founder, CXOTALK",
      country: "",
      slide: "slide-2",
    },
    {
      id: 3,
      article:
        "I really enjoyed working with the Intuz team they offered me great expertise and very good advises on all of my current and future projects.",
      image: "image 66",
      name: "Patrick Mimran",
      address: "Founder, Aphos",
      country: "",
      slide: "slide-3",
    },
  ];
  return (
    <section className={styles.clientsSection}>
      <Container>
        <h2>What our clients Says</h2>
        <p>We changed the way they do business, and they have no complaints</p>
        <Swiper
          spaceBetween={40}
          autoHeight={true}
          pagination={{
            clickable: true,
            bulletActiveClass: `${styles.active}`,
            bulletClass: `${styles.bullet}`,
            clickableClass: `${styles.clickable}`,
          }}
          className={`${styles.swiperContainer} mySwiper`}
        >
          {feedbackList.map((item) => (
            <SwiperSlide key={item.id} className={styles.itemList}>
              <div className="text-center md:text-left">
                <span className={styles.singltext}>“</span>
                <p className={styles.ptextClient}>{item.article}</p>
                <div className="flex flex-col md:flex-row items-center justify-center md:justify-start">
                  <img
                    src={`/images/Clientdetails/${item.image}.png`}
                    alt="clients-says"
                  />
                  <div className="flex flex-col ">
                    <p>{item.name}</p>
                    <span>{item.address}</span>
                    <span>{item.country}</span>
                  </div>
                </div>
              </div>
            </SwiperSlide>
          ))}
        </Swiper>
      </Container>
    </section>
  );
};

export default ComponentClientsSays;
