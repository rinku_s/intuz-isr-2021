export const tabdata = {
  data: [
    {
      tab_title: "Connectivity",
      icons: [
        {
          icon_url: "Connectivity/4G-LTE",
          title: "4g LTE",
        },
        {
          icon_url: "Connectivity/Wi-Fi",
          title: "WI-FI",
        },
        {
          icon_url: "Connectivity/Bluetooth",
          title: "Bluethooth",
        },
        {
          icon_url: "Connectivity/NFC",
          title: "NFC",
        },
        {
          icon_url: "Connectivity/GSM",
          title: "GSM",
        },
        {
          icon_url: "Connectivity/Ethernet",
          title: "Ethernet",
        },
      ],
    },
    {
      tab_title: "Hardware",
      icons: [
        {
          icon_url: "Hardaware/RaspberryPI",
          title: "Raspberry PI",
        },
        {
          icon_url: "Hardaware/Ardunio",
          title: "Ardunio",
        },
        {
          icon_url: "Hardaware/Beacons",
          title: "Beacons",
        },
        {
          icon_url: "Hardaware/NodeMCU",
          title: "NodeMCU",
        },
        {
          icon_url: "Hardaware/KNX",
          title: "KNX",
        },
        {
          icon_url: "Hardaware/Crestron",
          title: "Crestron",
        },
        {
          icon_url: "Hardaware/Microcontrollers",
          title: "Microcontrollers s(ESP32, ESP8266)",
        },
        {
          icon_url: "Hardaware/Miniature",
          title: "Miniature Boards",
        },
      ],
    },
    {
      tab_title: "Protocols & Standards",
      icons: [
        {
          icon_url: "Protocols&Standards/MQTT",
          title: "MQTT",
        },
        {
          icon_url: "Protocols&Standards/Modbus",
          title: "Modbus",
        },
        {
          icon_url: "Protocols&Standards/SNMP",
          title: "SNMP",
        },
        {
          icon_url: "Protocols&Standards/AMQP",
          title: "AMQP",
        },
        {
          icon_url: "Protocols&Standards/http,HTTPS",
          title: "http,HTTPS",
        },
        {
          icon_url: "Protocols&Standards/Z-Wave",
          title: "Z-Wave",
        },
        {
          icon_url: "Protocols&Standards/ZigBee",
          title: "ZigBee",
        },
        {
          icon_url: "Protocols&Standards/LoRAWAN",
          title: "LoRAWAN",
        },
        {
          icon_url: "Protocols&Standards/iBeacon",
          title: "iBeacon",
        },
        {
          icon_url: "Protocols&Standards/TCP & UDP",
          title: "TCP & UDP",
        },
        {
          icon_url: "Protocols&Standards/CoAP",
          title: "CoAp",
        },
        {
          icon_url: "Protocols&Standards/BLE",
          title: "BLE",
        },
        {
          icon_url: "Protocols&Standards/Cellular 2G,3G,4G,5G",
          title: "Cellular 2G,3G,4G,5G",
        },
        {
          icon_url: "Protocols&Standards/UDS",
          title: "UDS",
        },
      ],
    },
    {
      tab_title: "Programming & Frameworks",
      icons: [
        {
          icon_url: "Programming&Frameworks/Node.js",
          title: "Node.js",
        },
        {
          icon_url: "Programming&Frameworks/Python",
          title: "Python",
        },
        {
          icon_url: "Programming&Frameworks/React Js",
          title: "React Js",
        },
        {
          icon_url: "Programming&Frameworks/C++",
          title: "C/C++",
        },
        {
          icon_url: "Programming&Frameworks/NextJs",
          title: "Next js",
        },
        {
          icon_url: "Programming&Frameworks/Node-RED",
          title: "Node-RED",
        },
      ],
    },
    {
      tab_title: "API & Web Services",
      icons: [
        {
          icon_url: "Api&Web/JSON",
          title: "JSON",
        },
        {
          icon_url: "Api&Web/REST",
          title: "REST",
        },
        {
          icon_url: "Api&Web/GraphQL",
          title: "GraphQL",
        },
        {
          icon_url: "Api&Web/SOAP",
          title: "SOAP",
        },
        {
          icon_url: "Api&Web/XML",
          title: "XML",
        },
        {
          icon_url: "Api&Web/WSDL",
          title: "WSDL",
        },
      ],
    },
    {
      tab_title: "Data Analytics & ML",
      icons: [
        {
          icon_url: "DataAnalytice/Python",
          title: "Python",
        },
        {
          icon_url: "DataAnalytice/Grafana",
          title: "Grafana",
        },
        {
          icon_url: "DataAnalytice/Spark ML",
          title: "Spark ML",
        },
        {
          icon_url: "DataAnalytice/TenserFlow",
          title: "TenserFlow",
        },
        {
          icon_url: "DataAnalytice/Tableau",
          title: "Tableau",
        },
        {
          icon_url: "DataAnalytice/Power BI",
          title: "Power BI",
        },
        {
          icon_url: "DataAnalytice/Apache Hadoop",
          title: "Apache Hadoop",
        },
        {
          icon_url: "DataAnalytice/Apache Kafka",
          title: "Apache Kafka",
        },
        {
          icon_url: "DataAnalytice/OpenTSDB",
          title: "OpenTSDB",
        },
      ],
    },
  ],
};
