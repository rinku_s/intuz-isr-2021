import React from "react";
import ArrowTab from "components/ArrowTab/ArrowTab";
import Container from "components/Container/Container";
import { Media, MediaContextProvider } from "config/responsiveQuery";
import { tabdata } from "./data";
import Panel from "./Panel";
import Styles from "./styles.module.scss";
import Tabs from "./Tabs";
const ComponentToolsTechnologies = () => {
  return (
    <section className={Styles.ToolsTechnologies}>
      <Container>
        <MediaContextProvider>
          <h2>Tools & Technologies We Use</h2>
          <p>
            Intuz uses only the latest stack to envision and build smart and
            connected devices
          </p>
          <Media greaterThanOrEqual="sm">
            <Tabs>
              {tabdata.data.map((tab, i) => (
                <Panel title={tab.tab_title} key={i}>
                  {tab.icons.map((event, i) => {
                    return (
                      <div key={i}>
                        <img
                          src={`/images/IntuzSiteIoTIcons/${event.icon_url}.svg`}
                          alt="tools-technologies"
                        />
                        <p className={Styles.toolText}>{event.title}</p>
                      </div>
                    );
                  })}
                </Panel>
              ))}
            </Tabs>
          </Media>

          <Media lessThan="sm">
            <Tabs>
              {tabdata.data.map((tab, i) => (
                <ArrowTab
                  key={i}
                  buttonContainerClass={"text-center"}
                  containerClass={Styles.Panel}
                  arrayData={tab.icons}
                  title={tab.tab_title}
                  slice={6}
                >
                  {(item) => (
                    <div key={i}>
                      <img
                        src={`/images/IntuzSiteIoTIcons/${item.icon_url}.svg`}
                        alt="tools-technologies"
                      />
                      <p className={Styles.toolText}>{item.title}</p>
                    </div>
                  )}
                </ArrowTab>
              ))}
            </Tabs>
          </Media>
        </MediaContextProvider>
      </Container>
    </section>
  );
};

export default ComponentToolsTechnologies;
