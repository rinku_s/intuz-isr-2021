import React, { Component } from "react";
import Styles from "./styles.module.scss";

class Tabs extends Component {
  state = {
    selected: this.props.selected || 0
  };

  handleChange(index) {
    this.setState({ selected: index });
  }

  render() {
    return (
      <>
        <ul className={Styles.tabsUl}>
          {this.props.children.map((elem, index) => {
            return (
              <li
                key={index}
                className={`${Styles.liTabs} ${index === this.state.selected ? Styles.selected : ''}`}
                onClick={() => this.handleChange(index)}
              >
                {elem.props.title}
              </li>
            );
          })}
        </ul>
        <div className={Styles.tab}>{this.props.children[this.state.selected]}</div>
      </>
    );
  }
}

export default Tabs;
