import React from "react";
import styles from "./styles.module.scss";
export default function Panel(props) {
  return <div className={styles.Panel}>{props.children}</div>;
}
