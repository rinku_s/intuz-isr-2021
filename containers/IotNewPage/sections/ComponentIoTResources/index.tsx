import React from "react";
import Container from "../../../../components/Container/Container";
import Styles from "./styles.module.scss";

const Index = () => {
  const iotResources = [
    {
      id: 1,
      title: "Complete Guide for IoT Product Development Stages",
      image: "2",
      blogURL: "https://www.intuz.com/iot-product-development-guide",
    },
    {
      id: 2,
      title: "Connected Healthcare: Transforming Lives for Betterment",
      image: "1",
      blogURL:
        "https://www.intuz.com/blog/connected-healthcare-transforming-lives-for-betterment",
    },
    {
      id: 3,
      title: "A Complete Guide On IOT And Industrial Internet of Things [IIOT]",
      image: "3",
      blogURL: "https://www.intuz.com/blog/guide-on-iot-and-iiot",
    },
  ];
  return (
    <section className={Styles.ResourcesMain}>
      <Container>
        <h2>Explore Our IoT Resources & Insights</h2>
        <p>
          Explore articles on the latest technology trends, enterprise mobility
          solutions, and company updates
        </p>

        <div className={Styles.iotResourcesList}>
          {iotResources.map((list) => (
            <div key={list.id}>
              <a href={list.blogURL}>
                <img
                  src={`/static/iotResources/${list.image}.png`}
                  className={Styles.imgIot}
                  alt="iot-resources"
                />
              </a>
              <a href={list.blogURL} className={Styles.ptitle}>
                {list.title}
              </a>
            </div>
          ))}
        </div>
      </Container>
    </section>
  );
};

export default Index;
