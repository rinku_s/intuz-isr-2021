import React from "react";
import Container from "../../../../components/Container/Container";
import styles from "./styles.module.scss";

const ComponentIOTIntro = () => {
  return (
    <section className={styles.ComponentIOTIntro}>
      <Container>
        <h1>
          We'll Build & Manage <span>IoT Solutions</span> For
          You
        </h1>
        <p>
          We partner with small businesses and mid-size enterprises to build,
          manage and scale their operations with custom IoT development services
          and solutions.
        </p>
      </Container>
    </section>
  );
};

export default ComponentIOTIntro;
