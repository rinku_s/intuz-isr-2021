import React from "react";
import ArrowTab from 'components/ArrowTab/ArrowTab';
import Container from "components/Container/Container";
import { Media, MediaContextProvider } from 'config/responsiveQuery';
import Styles from "./styles.module.scss";
const index = () => {
  const AcrossList = [
    {
      title: "Healthcare",
      content:
        "Boost the efficiency of healthcare provision and patient monitoring with Intuz.",
      icon: "Healthcare",
    },
    {
      title: "Manufacturing",
      content:
        "Breed a new range of IoT solutions that work in tandem with your machinery.",
      icon: "Manufacturing",
    },
    {
      title: "Automotive",
      content:
        "Gain a competitive edge in the connected vehicle ecosystem with our support.",
      icon: "Automotive",
    },
    {
      title: "Oil & Gas",
      content:
        "Drive progress and reduce unnecessary expenses with scalable IoT solutions.",
      icon: "Oil & Gas",
    },
    {
      title: "Transportation",
      content:
        "Intuz designs and delivers a range of IoT tools and services for transportation.",
      icon: "Transportation",
    },
    {
      title: "FinTech",
      content:
        "Boost your operations when it comes to security and payment processing with IoT.",
      icon: "FinTech",
    },
    {
      title: "Agriculture",
      content:
        "Build smart farms and equip farmers with insights with AI-based equipment.",
      icon: "Agriculture",
    },
    {
      title: "Utilities & Energy",
      content:
        "Rethink energy and utility management with innovative IoT development.",
      icon: "Utilities & Energy",
    },
    {
      title: "Logistics",
      content:
        "Optimize your SCM and add new revenue streams with IoT development services.",
      icon: "Logistics",
    },
    {
      title: "Hospitality",
      content:
        "Reduce operational costs and deliver a fantastic guest experience with IoT apps.",
      icon: "Hospitality",
    },
    {
      title: "Banking & Insurance",
      content:
        "Enable transparency in your client accounts. Build a safe banking experience with IoT.",
      icon: "Banking & Insurance",
    },
    {
      title: "Education",
      content:
        "Unlock the full potential of IoT and use data to drive success in the education sector.",
      icon: "Education",
    },
  ];
  return (
    <section className={Styles.AcrossIndustries}>
    <MediaContextProvider>
      <Container>
        <h2>Our IoT Expertise Across Industries</h2>
        <p>
          Regardless of business niche, Intuz delivers top-notch IoT software
          solutions
        </p>
      <Media greaterThan="sm">
        <div className={Styles.AcrossIndustriesList}>
          {AcrossList.map((list, j) => (
            <div key={j} className={Styles.listItem}>
                <img
                  className={Styles.ImageCaption}
                  src={`/images/icon/${list.icon}.png`}
                  alt="across-industries"
                />
              <h3>{list.title}</h3>
              <p>{list.content}</p>
            </div>
          ))}
        </div>
      </Media>
        <Media lessThan="sm">
            <ArrowTab containerClass={Styles.AcrossIndustriesList} arrayData={AcrossList} slice={4} buttonContainerClass={"text-center"}>
              {list =>  (
                <div className={Styles.listItem}>
                   <img
                     className={Styles.ImageCaption}
                     src={`/images/icon/${list.icon}.png`}
                     alt="across-industries"
                   />
                 <h3>{list.title}</h3>
                 <p>{list.content}</p>
               </div>
              )}
            </ArrowTab>
        </Media>
      </Container>
      </MediaContextProvider>

    </section>
  );
};

export default index;
