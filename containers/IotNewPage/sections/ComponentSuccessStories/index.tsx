//@ts-nocheck
import React from "react";
import Container from "../../../../components/Container/Container";
import styles from "./styles.module.scss";
import FlexBlock from "./FlexBlock";
const ComponentSuccessStories = () => {
  const storieList = [
    {
      id: 1,
      image: "Frame 2016",
      icon: "updated logo",
      title: "IoT-Enabled Smart Pool Equipments Controlling & Automation ",
      content:
        "Discover how we developed a custom solution to help the business manage water pumps, lighting and other accessories through the internet.",
      buttonAvailable: true,
      buttonUrl: "https://www.intuz.com/case-studies/speck-pumps",
      imageRightAlign: "",
      list: [],
    },
    {
      id: 2,
      image: "Frame 2021",
      icon: "image 69",
      title:
        "Enterprise-Level Fuel Station Operations Automation App for a Large Oil Corporation",
      content:
        "This Zambian petrol pump business learned to efficiently track customer activities,easing the task of managing fuel purchases thanks to Intuz.",
      buttonAvailable: true,
      buttonUrl: "https://www.intuz.com/case-studies/sgc",
      imageRightAlign: "row",
      list: [],
    },
    {
      id: 3,
      isSVG: true,
      image: "Frame 2023",
      icon: "",
      title: "IoT enabled Enterprise Mobile (POS) Solutions for Retails",
      content:
        "We lent our expertise to a US-based fintech enterprise to build for them a dynamic IoT-based mobile POS for accepting card payments in just three clicks. Some benefits they enjoy:",
      buttonAvailable: false,
      buttonUrl: "",
      imageRightAlign: "",
      list: [
        "A highly secure and PCI-DSS-compliant app",
        "Extremely safe payment options for faster checkout",
        "A posh and seamless user experience for customers",
        "An integrated credit card harward swiper for Android and iOS apps",
      ],
    },
  ];
  return (
    <section className={styles.DevelopmentProcessSection}>
      <Container>
        <h2>Client Success Stories</h2>
        <p>
          Find out how our IoT solutions have helped businesses take a
          successful leap in their growth journey
        </p>
        <div className={styles.storieList}>
          {storieList.map((list) => (
            <FlexBlock {...list} key={list.id} />
          ))}
        </div>
      </Container>
    </section>
  );
};

export default ComponentSuccessStories;
