import React from "react";
import styles from "./styles.module.scss";
import Router from "next/router";
import SVG from "react-inlinesvg";
import { cdn } from "config/cdn";

const FlexBlock = ({
  image,
  icon,
  title,
  content,
  buttonAvailable,
  imageRightAlign,
  buttonUrl,
  list,
  isSVG,
}) => {
  return (
    <div
      className={styles.storieListItem}
      style={{ flexDirection: imageRightAlign ? "row" : "row-reverse" }}
    >
      {isSVG && (
        <SVG className="img-fluid" src={cdn("iot_cs_2_6a7d9ca62c.svg")} />
      )}

      {!isSVG && <img src={`/images/ClientStory/${image}.png`} alt={image} />}
      <div>
        {icon && <img src={`/images/ClientStory/${icon}.png`} alt={icon} />}
        <h3>{title}</h3>
        <p>{content}</p>
        {buttonAvailable && (
          <a
            style={{
              display: "inline-flex",
              alignItems: "center",
              justifyContent: "center",
            }}
            href={buttonUrl}
          >
            View Case Study
          </a>
        )}
        <ul className={styles.ulList}>
          {list.length > 0 &&
            list.map((l) => (
              <li key={l} className={styles.liList}>
                {l}
              </li>
            ))}
        </ul>
      </div>
    </div>
  );
};

export default FlexBlock;
