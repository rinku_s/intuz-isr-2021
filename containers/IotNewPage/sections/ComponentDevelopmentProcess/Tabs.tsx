//@ts-nocheck
import React, { Component } from "react";
import Styles from "./styles.module.scss";

class Tabs extends Component {
  state = {
    selected: this.props.selected || 0,
  };

  handleChange(index) {
    this.setState({ selected: index });
  }

  render() {
    return (
      <>
        <ul className={Styles.tabsUl}>
          {this.props.children.map((elem, index) => {
            return (
              <React.Fragment key={index}>
                <li
                  key={index}
                  className={`${Styles.liTabs} ${
                    index === this.state.selected ? Styles.selected : ""
                  }`}
                  onClick={() => this.handleChange(index)}
                >
                  <div className="flex items-center">
                    <svg>
                      <text
                        x="30%"
                        y="75%"
                        style={{ stroke: "#714581", fill: "#fff" }}
                      >
                        {index + 1}
                      </text>
                    </svg>
                    {index % 2 === 0 &&
                      index + 1 < this.props.children.length && (
                        <img
                          className={`img-fluid mx-2 ${Styles.lines}`}
                          src="/images/downborder.svg"
                          alt="downarrow"
                        />
                      )}
                    {index % 2 === 1 &&
                      index + 1 < this.props.children.length && (
                        <img
                          className={`img-fluid mx-2 ${Styles.lines}`}
                          src="/images/upborder.svg"
                          alt="uparrow"
                        />
                      )}
                  </div>
                  <p>{elem.props.title}</p>
                </li>
              </React.Fragment>
            );
          })}
        </ul>
        <div className={Styles.tab}>
          {this.props.children[this.state.selected]}
        </div>
      </>
    );
  }
}

export default Tabs;
