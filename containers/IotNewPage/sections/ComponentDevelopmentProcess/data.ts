export const tabdata = {
  data: [
    {
      tab_title: "Research & Ideation",
      data: [
        {
          image: "Research",
          title: "Research & Ideation",
          time: "(2-3 Weeks)",
          description:
            "We do not start anything before we have had a sit-down with you to understand your business requirements and the existing workflow. Based on our chat, we conduct thorough research and brainstorm potential solutions helpful in meeting your objective.",
        },
      ],
    },
    {
      tab_title: "User Centric Design (POC)",
      data: [
        {
          image: "App development",
          title: "User-Centric Design (POC)",
          time: "(4-6 Weeks)",
          description:
            "Once the solution is decided, we will work closely with you to design clickable prototypes of the architecture that impress all your project stakeholders. This step will help us refine your use case and build something tangible in the later stages.",
        },
      ],
    },
    {
      tab_title: "Development & Integration",
      data: [
        {
          image: "Support and maintenance",
          title: "Development & Integration",
          time: "(12-14 Weeks)",
          description:
            "This is the most exciting part of the process as here, our expert developers get to work to code your solution. That could include designing the mobile app and web interface. We also take care of firmware and hardware integration for you.",
        },
      ],
    },
    {
      tab_title: "Deployment & Testing",
      data: [
        {
          image: "Testing",
          title: "Deployment & Testing",
          time: "(2-3 Weeks)",
          description:
            "After the solution has been developed, we get our hands dirty, removing bugs and glitches that hamper user experience. Once we are through with that, we hand it over to you for your review. The solution is deployed once you have given your green signal.",
        },
      ],
    },
    {
      tab_title: "Maintenance & Enhancements",
      data: [
        {
          image: "Design",
          title: "Maintenance Support & Enhancements",
          time: "(On-going/Quarterly)",
          description:
            "Our level of service does not stop at deployment. Instead, we offer maintenance support for a defined period to ensure the IoT solution works as smoothly as butter. We also make enhancements to the app as per your user requirements. We strive to delight you throughout!",
        },
      ],
    },
  ],
};