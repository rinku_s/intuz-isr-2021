import React from "react";
import Container from "../../../../components/Container/Container";
import styles from "./styles.module.scss";
import Tabs from "./Tabs";
import Panel from "./Panel";
import { tabdata } from "./data";
import { MediaContextProvider, Media } from "config/responsiveQuery";
import ArrowTab from "components/ArrowTab/ArrowTab";
const index = () => {
  const tabdata2 = [
    {
      image: "Research",
      title: "Research & Ideation",
      time: "(2-3 Weeks)",
      description:
        "We do not start anything before we have had a sit-down with you to understand your business requirements and the existing workflow. Based on our chat, we conduct thorough research and brainstorm potential solutions helpful in meeting your objective.",
    },
    {
      image: "App development",
      title: "User-Centric Design (POC)",
      time: "(4-6 Weeks)",
      description:
        "Once the solution is decided, we will work closely with you to design clickable prototypes of the architecture that impress all your project stakeholders. This step will help us refine your use case and build something tangible in the later stages.",
    },
    {
      image: "Support and maintenance",
      title: "Development & Integration",
      time: "(12-14 Weeks)",
      description:
        "This is the most exciting part of the process as here, our expert developers get to work to code your solution. That could include designing the mobile app and web interface. We also take care of firmware and hardware integration for you.",
    },
    {
      image: "Testing",
      title: "Deployment & Testing",
      time: "(2-3 Weeks)",
      description:
        "After the solution has been developed, we get our hands dirty, removing bugs and glitches that hamper user experience. Once we are through with that, we hand it over to you for your review. The solution is deployed once you have given your green signal.",
    },
    {
      image: "Design",
      title: "Maintenance Support & Enhancements",
      time: "(On-going/Quarterly)",
      description:
        "Our level of service does not stop at deployment. Instead, we offer maintenance support for a defined period to ensure the IoT solution works as smoothly as butter. We also make enhancements to the app as per your user requirements. We strive to delight you throughout!",
    },
  ];

  return (
    <section className={styles.StoriesSection}>
      <Container>
        <MediaContextProvider>
          <h2>Our IoT Development Process</h2>
          <p>
            A roadmap for guiding a successful, user-focused IoT application
            development process
          </p>
          <Media greaterThanOrEqual="lg">
            <Tabs>
              {tabdata.data.map((tab, i) => (
                <Panel title={tab.tab_title} key={i}>
                  {tab.data.map((event, i) => {
                    return (
                      <div key={i} className={styles.DevelopmentProcess}>
                        <img
                          src={`/images/DevelopmentProcess/${event.image}.png`}
                          alt="development-process"
                        />
                        <div className={styles.DevelopmentProcessblock}>
                          <h3 className={styles.toolText}>{event.title}</h3>
                          <span>{event.time}</span>
                          <p>{event.description}</p>
                        </div>
                      </div>
                    );
                  })}
                </Panel>
              ))}
            </Tabs>
          </Media>
          <Media lessThan="lg">
            <ArrowTab
              containerClass={styles.Panel}
              arrayData={tabdata2}
              slice={1}
              buttonContainerClass={"text-center"}
            >
              {(item) => (
                <div className={styles.DevelopmentProcess}>
                  <h4 className={`block md:hidden ${styles.toolText}`}>
                    {item.title}
                  </h4>
                  <span className="block md:hidden">{item.time}</span>

                  <img
                    src={`/images/DevelopmentProcess/${item.image}.png`}
                    alt="development-process"
                  />
                  <div className={styles.DevelopmentProcessblock}>
                    <h4 className={`d-mblock hidden ${styles.toolText}`}>
                      {item.title}
                    </h4>
                    <span className="d-mblock hidden">{item.time}</span>
                    <p>{item.description}</p>
                  </div>
                </div>
              )}
            </ArrowTab>
          </Media>
        </MediaContextProvider>
      </Container>
    </section>
  );
};

export default index;
