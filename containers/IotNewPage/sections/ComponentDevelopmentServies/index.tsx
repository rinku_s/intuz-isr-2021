import React from "react";
import Container from "components/Container/Container";
import Styles from "./styles.module.scss";
const index = () => {
  const serviceList = [
    {
      title: "IoT Consulting",
      content:
        "From devices and sensors to lifecycle management and connectivity, strategize your IoT development process with the help of Intuz’s experienced teams.",
    },
    {
      title: "PoC & Prototype Development",
      content:
        "Intuz aims to develop products which align with your core functionality so that you can fetch maximum output from your IoT product. That is a guarantee!.",
    },
    {
      title: "Industrial IoT Solutions",
      content:
        "Glean actionable insights from your supply chain monitoring and manufacturing performance for higher efficiency by deploying IIoTs.",
    },
    {
      title: "IoT Firmware Development & Integration",
      content:
        "Let Intuz experts collaborate with your hardware designers to launch IoT firmware and IoT-embedded apps for IoT devices. Whatever your requirement, we will bring your IoT project to life faster.",
    },
    {
      title: "IoT Mobile & Web Apps",
      content:
        "Our robust and scalable IoT applications for the mobile and web are engineered to streamline your business. We use the latest digital technologies to deliver products that are timeless.",
    },
    {
      title: "IP Product Development & Integration",
      content:
        "Make the communication between sensors and IoT components stronger by utilizing the software interface architecture. Intuz is a pro at strengthening the IP strategy.",
    },
    {
      title: "IOT Wearable Apps",
      content:
        "Bank on the health and fitness bandwagon with incredible wearables. Give your customers the ultimate goal to get fit. Engineer shareable experiences.",
    },
    {
      title: "IoMT Solutions",
      content:
        "Is your goal to improve patient care, staff productivity and asset use? Intuz will help you develop and integrate innovative IoMT apps into your internal processes.",
    },
    {
      title: "IoT Analytics & Data Visualization",
      content:
        "Data management is a priority at Intuz. We specialize in designing IoT data visualization and analytics platforms that come in handy to businesses.",
    },
  ];
  return (
    <section className={Styles.serviceMain}>
      <Container>
        <h2>
          Our IoT Development Services for Businesses&nbsp;&&nbsp;Enterprises
        </h2>
        <p>
          Let us optimize your operational core to build connected systems for
          your end customers
        </p>
        <div className={Styles.GridList}>
          {serviceList.map((list, j) => (
            <div key={j} className={Styles.listService}>
              <h3>{list.title}</h3>
              <p>{list.content}</p>
            </div>
          ))}
        </div>
      </Container>
    </section>
  );
};

export default index;
