import React from "react";
import ComponetIotIntro from "./sections/ComponentIotIntro";
import TrustedSection from "../CommonSections/TrustedSection/TrustedSection";
import ComponentDevelopmentServies from "./sections/ComponentDevelopmentServies";
import ComponentAcrossIndustries from "./sections/ComponentAcrossIndustries";
import ComponentToolsTechnologies from "./sections/ComponentToolsTechnologies/ComponentToolsTechnologies";
import ComponentPlatformsuse from "./sections/ComponentPlatformsuse";
import ComponentIoTResources from "./sections/ComponentIoTResources";
import ComponentSensors from "./sections/ComponentSensors";
import ComponentSuccessStories from "./sections/ComponentSuccessStories";
import ComponentClientsSays from "./sections/ComponentClientsSays";
import ComponentDevelopmentProcess from "./sections/ComponentDevelopmentProcess";
const index = () => {
  return (
    <>
      <ComponetIotIntro />
      <TrustedSection subHeading="Trusted by our beloved clients" />
      <ComponentDevelopmentServies />
      <ComponentAcrossIndustries />
      <ComponentSuccessStories />
      <ComponentToolsTechnologies />
      <ComponentPlatformsuse />
      <ComponentSensors />
      <ComponentDevelopmentProcess />
      <ComponentClientsSays />
      <ComponentIoTResources />
    </>
  );
};

export default index;
