//@ts-nocheck
import React from "react";
import 'lazysizes';
import 'lazysizes/plugins/attrchange/ls.attrchange';
import ComponentLighthouseBanner from "./sections/ComponentLighthouseBanner/index";
import ComponentLighthouseWhyDose from "./sections/ComponentLighthouseWhyspeed/index";
import ComponentWhatMaking from "./sections/ComponentWhatMaking/index";
import ComponentLighthouseAudit from "./sections/ComponentAudit/index";
import ComponentLighthouseJamstasck from "./sections/ComponentJamstack/index";
import ComponentWhatJamstack from "./sections/ComponetnWhatJamstack/index";
import ComponentWhyJamstack from "./sections/ComponetnWhyJamstack/index";
import ComponentFeatures from "./sections/ComponentFeatures/index";
import ComponentTools from "./sections/ComponentTools/index";
import ComponentFaq from "./sections/ComponentFaq/index";
import ComponentLetsConnect from "./sections/ComponentLetsConnect/index";
import ComponentGetinTouch from "./sections/ComponentGetinTouch/index";
const Lighthousezone = () => {
  return (
    <>
      <ComponentLighthouseBanner />
      <ComponentLighthouseWhyDose />
      <ComponentGetinTouch />
      <ComponentWhatMaking />
      <ComponentLighthouseAudit />
      <ComponentLighthouseJamstasck />
      <ComponentWhatJamstack />
      <ComponentWhyJamstack />
      <ComponentLetsConnect />
      <ComponentFeatures />
      <ComponentTools />
      <ComponentFaq />
    </>
  );
};
export default Lighthousezone;
