//@ts-nocheck
import React from "react";
import Container from "../../../../components/Container/Container";
import classes from "./styles.module.scss";
import LinkButton from "../../../../components/UI/LinkButton/LinkButton";
import Router from "next/router";
import { scrollAnimation } from "hooks/scrollAnimation";
const ComponentGetinTouch = () => {
  scrollAnimation(`.${classes.ComponentGetinTouch}`, (tl) => {
    tl.fade(`.${classes.ComponentGetinTouch} .first`);
  });

  return (
    <>
      <section className={`${classes.ComponentGetinTouch} items-center`}>
        <Container>
          <div className={`${classes.boxstyle} first`}>
            <div className="pb-3">
              <p>Is Your Website Built for Speed?</p>
              <span>Get in touch with our tech team</span>
            </div>
            <LinkButton
              href="/get-started"
              onClick={() => Router.push("/get-started")}
              variation="purpleBtn"
            >
              Get Started
            </LinkButton>
          </div>
        </Container>
      </section>
    </>
  );
};
export default ComponentGetinTouch;
