//@ts-nocheck
import React from "react";
import Container from "../../../../components/Container/Container";
import classes from "./styles.module.scss";
import WhyJamstackBlock from "../../../../components/Lighthouse/WhyJamstack/index";
import { scrollAnimation } from "hooks/scrollAnimation";
const ComponentWhyJamstack = () => {
  scrollAnimation(`.${classes.ComponentWhyJamstack}`, (tl) => {
    tl.fade(`.${classes.ComponentWhyJamstack} .first`).fade(
      `.${classes.ComponentWhyJamstack} .second`
    );
  });

  return (
    <>
      <section className={`${classes.ComponentWhyJamstack}  items-center`}>
        <Container>
          <h3 className="first">Why JAMstack?</h3>
          <p className={`pt-5 pb-5 second text-center`}>
            From large eCommerce sites to enterprise SaaS applications to a mere
            blog—the JAMstack architecture has universal benefits.
          </p>
          <WhyJamstackBlock />
        </Container>
      </section>
    </>
  );
};
export default ComponentWhyJamstack;
