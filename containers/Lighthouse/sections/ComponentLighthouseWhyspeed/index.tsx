//@ts-nocheck
import React from "react";
import { scrollAnimation } from "hooks/scrollAnimation";
import Container from "../../../../components/Container/Container";
import Heading from "../../../../components/HeadingNunito/Heading";
import WhyDoesBlock from "../../../../components/Lighthouse/WhyDoes/index";
import classes from "./styles.module.scss";
const LightHouseWhyDose = () => {
  scrollAnimation(`.${classes.ComponentLightHouseWhyDose}`, (tl) => {
    tl.fade(`.${classes.ComponentLightHouseWhyDose} .first`)
      .fade(`.${classes.ComponentLightHouseWhyDose} .second`, "-=0.2")
      .from(
        `.${classes.ComponentLightHouseWhyDose} .fourth`,
        {
          y: 40,
          autoAlpha: 0,
          ease: "power2.out",
          stagger: 0.31,
          duration: 0.5,
        },
        "-=0.3"
      );
  });

  return (
    <>
      <section className={`${classes.ComponentLightHouseWhyDose} items-center`}>
        <Container>
          <Heading className="mt-5 mt-md-0 first">
            Why Does <span>Site&nbsp;Speed</span> &{" "}
            <span>Lighthouse&nbsp;Score </span>
            Matters?
          </Heading>
          <p className={`${classes.extrapadding} mb-5 second`}>
            53% of mobile users abandon websites that take longer than three
            seconds to load. Do you want that for your site?
          </p>
          <WhyDoesBlock />
        </Container>
      </section>
    </>
  );
};
export default LightHouseWhyDose;
