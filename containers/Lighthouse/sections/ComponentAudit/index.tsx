//@ts-nocheck
import React from "react";
import Container from "../../../../components/Container/Container";
import classes from "./styles.module.scss";
import Heading from "../../../../components/HeadingNunito/Heading";
import { scrollAnimation } from "hooks/scrollAnimation";
import Audit from "components/Lighthouse/Audit";
const ComponentAudit = () => {
  scrollAnimation(`.${classes.ComponentLightHouseAudit}`, (tl) => {
    tl.fade(`.${classes.ComponentLightHouseAudit} .first`).fade(
      `.${classes.ComponentLightHouseAudit} .second`
    );
  });

  return (
    <>
      <section className={`${classes.ComponentLightHouseAudit} items-center`}>
        <Container>
          <Heading className="mt-5 mt-md-0 first">
            Get Your <span>Free</span> Website <span>Performance Audit</span>
          </Heading>
          <p className={`mb-5 second`}>
            Get an in-depth website performance audit report from our technical
            team.
          </p>
          <div>
            <Audit />
          </div>
        </Container>
      </section>
    </>
  );
};
export default ComponentAudit;
