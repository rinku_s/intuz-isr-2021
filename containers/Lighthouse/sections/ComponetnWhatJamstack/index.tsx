//@ts-nocheck
import React from "react";
import Container from "../../../../components/Container/Container";
import classes from "./styles.module.scss";
// import Heading from "../../../../components/HeadingNunito/Heading";
import Approch from "../../../../components/Lighthouse/Approch/index";
import { scrollAnimation } from "hooks/scrollAnimation";
const ComponentWhatJamstack = () => {
  scrollAnimation(`.${classes.ComponentLightHouseWhatIsJamstack}`, (tl) => {
    tl.fade(`.${classes.ComponentLightHouseWhatIsJamstack} .first`).fade(
      `.${classes.ComponentLightHouseWhatIsJamstack} .second`
    );
  });
  return (
    <>
      <section
        className={`${classes.ComponentLightHouseWhatIsJamstack}  items-center`}
      >
        <Container>
          <h3 className="first">What is JAMstack?</h3>
          <p className={`pt-5 pb-5 second text-center`}>
            JAM, a synonym for JavaScript, APIs and Markup, is the new standard
            web architecture. Pre-rendered content is served to a CDN with Git
            workflows and software build tools. Utilizing the strengths of APIs
            and serverless functions is core to making a JAMstack app dynamic.
            Its main components are JavaScript, CDNs, API-first CMSes and Static
            Site Generators.
          </p>
          <Approch />
        </Container>
      </section>
    </>
  );
};
export default ComponentWhatJamstack;
