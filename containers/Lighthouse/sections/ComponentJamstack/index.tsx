//@ts-nocheck
import React from "react";
import { scrollAnimation } from "hooks/scrollAnimation";
import Container from "../../../../components/Container/Container";
import Heading from "../../../../components/HeadingNunito/Heading";
import classes from "./styles.module.scss";

const ComponentWhatMaking = () => {
  scrollAnimation(`.${classes.ComponentLightHouseJamStack}`, (tl) => {
    tl.fade(`.${classes.ComponentLightHouseJamStack} .first`).fade(
      `.${classes.ComponentLightHouseJamStack} .second`
    );
  });

  return (
    <>
      <section
        className={`${classes.ComponentLightHouseJamStack}  items-center`}
      >
        <Container>
          <Heading className="mt-5 mt-md-0 first">
            A Modern-Day <span>JAMstack Web Development</span> Service
          </Heading>
          <p className={`${classes.extrapadding} mb-md-5 second`}>
            A revolutionary way to build websites and progressive apps that
            delivers maximum performance and unmatched scale
          </p>
        </Container>
      </section>
    </>
  );
};
export default ComponentWhatMaking;
