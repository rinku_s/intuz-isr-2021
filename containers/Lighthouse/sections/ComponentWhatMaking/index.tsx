//@ts-nocheck
import React from "react";
import { cdn } from "../../../../config/cdn";
import Container from "../../../../components/Container/Container";
import classes from "./styles.module.scss";
import Heading from "../../../../components/HeadingNunito/Heading";
import WhatMakingblock from "../../../../components/Lighthouse/WhatMakingblock/index";
import SVG from "react-inlinesvg";
import { scrollAnimation } from "hooks/scrollAnimation";
const ComponentWhatMaking = () => {
  scrollAnimation(`.${classes.ComponentLightHouseWhyDose}`, (tl) => {
    tl.fade(`.${classes.ComponentLightHouseWhyDose} .first`).fade(
      `.${classes.ComponentLightHouseWhyDose} .second`
    );
  });

  return (
    <>
      <section
        className={`${classes.ComponentLightHouseWhyDose}  items-center`}
      >
        <Container>
          <Heading className="mt-5 mt-md-0 first">
            What Is Making <span>Your Website Slow?</span>
          </Heading>
          <p className={` mb-5 second text-center`}>
            Reasons could vary but these factors most commonly hamper the
            website loading time and lighthouse score
          </p>
          <div className="row pt-5">
            <div className="col-12 col-md-6 col-sm-12 mb-5 mb-md-0">
              <SVG src={cdn("poor_web1_1_228ddff04c.svg")} />
            </div>
            <div className="col-12 col-md-6 col-sm-12" id="what_slow">
              <WhatMakingblock />
            </div>
          </div>
        </Container>
      </section>
    </>
  );
};
export default ComponentWhatMaking;
