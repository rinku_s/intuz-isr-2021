//@ts-nocheck
import React, { useEffect } from "react";
import { cdn } from "../../../../config/cdn";
import Container from "../../../../components/Container/Container";
import classes from "./styles.module.scss";
import gsap from "gsap";
import SVG from "react-inlinesvg";
import Image from "next/image";
import { myLoader } from "config/image-loader";
const ComponentLightHouseBanner = () => {
  useEffect(() => {
    let tl = gsap.timeline({ delay: 1 });

    tl.fade(`.${classes.ComponentLighthouseBanner} .first`)
      .fade(`.${classes.ComponentLighthouseBanner} .second`, "-=0.2")
      .fade(`.${classes.ComponentLighthouseBanner} .third`, "-=0.3");
    // .from(`.${classes.ComponentLighthouseBanner} .fourth img`, { scale:0.95, autoAlpha:0, ease:"power2.out", stagger:0.3 },"-=0.3");
  }, []);

  return (
    <>
      {" "}
      <section
        className={`${classes.ComponentLighthouseBanner} flex items-center`}
      >
        <Container>
          {/* <div className="flex flex-col-reverse md:flex-row items-center justify-center"> */}
          <div className="row items-center">
            <div
              // style={{ flexBasis: "60%" }}
              className={`text-center md:text-left col-12 col-md-7 col-sm-12 ${classes.titlecontainer}`}
            >
              <p className="mb-md-0 first">
                Take Your <strong>Digital Experience</strong> to the{" "}
                <strong>Next-Level </strong>
                With
              </p>
              <h1 className="inline-block mt-md-0 second">Google Lighthouse</h1>
              <p className="third">
                <strong>Optimized</strong> JAMstack{" "}
                <strong>web Development</strong> Services
              </p>
            </div>
            <div
              // style={{ marginLeft: "5rem" }}
              className={`col-12 col-md-5 col-sm-12 ${classes.mtphone} fourth`}
            >
              <Image
                layout="responsive"
                loader={myLoader}
                src="google_lighthouse_1_0c4af16144.png"
                alt="lighthouse"
                height="333"
                width="528"
              />
              {/* <img
                className="img-fluid pt-3 pl-5 pr-5"
                src={cdn("Group_123_2c8bcc52d4.png")}
                alt="lighthouse"
              /> */}
              <SVG
                className={`pr-5 pl-5 ${classes.svgwidth}`}
                src={cdn("lighthouse1_5d2a6457c7.svg")}
              />
            </div>
          </div>
          {/* </div> */}
        </Container>
      </section>
    </>
  );
};
export default ComponentLightHouseBanner;
