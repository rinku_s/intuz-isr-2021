//@ts-nocheck
import React from "react";
import Container from "../../../../components/Container/Container";
import classes from "./styles.module.scss";
import LinkButton from "../../../../components/UI/LinkButton/LinkButton";
import { scrollAnimation } from "hooks/scrollAnimation";
const ComponentLetsConnect = () => {
  scrollAnimation(`.${classes.ComponentLetsConnect}`, (tl) => {
    tl.from(`.${classes.boxstyle}`, {
      scale: 0.96,
      autoAlpha: 0,
      duration: 0.8,
    });
  });

  return (
    <>
      <section className={`${classes.ComponentLetsConnect} items-center`}>
        <Container>
          <div className={`${classes.boxstyle}`}>
            <div className="pb-5">
              <span>
                Interested in Leveraging JAMStack Technology for your Business?
              </span>
            </div>
            <LinkButton variation={"blueSqured"} herf="/get-started">
              Let's Connect
            </LinkButton>
          </div>
        </Container>
      </section>
    </>
  );
};
export default ComponentLetsConnect;
