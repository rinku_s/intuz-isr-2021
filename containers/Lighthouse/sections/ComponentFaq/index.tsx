//@ts-nocheck
import React from "react";
import Container from "../../../../components/Container/Container";
import classes from "./styles.module.scss";
import Heading from "../../../../components/HeadingNunito/Heading";
import FaqAccordian from "../../../../components/Lighthouse/Faq/index";
import { scrollAnimation } from "hooks/scrollAnimation";
const ComponentFaq = () => {
  scrollAnimation(`.${classes.ComponentFaq}`, (tl) => {
    tl.fade(`.${classes.ComponentFaq} .first`)
      .fade(`.${classes.ComponentFaq} .second`)
      .from(`.${classes.ComponentFaq} .card`, {
        y: 20,
        autoAlpha: 0,
        stagger: 0.15,
        ease: "power1.out",
      });
  });

  return (
    <>
      <section className={`${classes.ComponentFaq} items-center`}>
        <Container>
          <Heading className="mt-5 mt-md-0 first">
            <span>FAQs</span>
          </Heading>
          <p className={`mb-5 second`}>
            Check out most commonly asked questions about our service
          </p>
          <FaqAccordian />
        </Container>
      </section>
    </>
  );
};
export default ComponentFaq;
