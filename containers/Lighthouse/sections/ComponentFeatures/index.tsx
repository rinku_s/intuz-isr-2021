//@ts-nocheck
import React from "react";
import Container from "../../../../components/Container/Container";
import classes from "./styles.module.scss";
import Heading from "../../../../components/HeadingNunito/Heading";
import CaseStudy from "../../../../components/Lighthouse/Casestudy/index";
import { scrollAnimation } from "hooks/scrollAnimation";
const ComponentFeatures = () => {
  scrollAnimation(`.${classes.ComponentFeatures}`, (tl) => {
    tl.fade(`.${classes.ComponentFeatures} .first`)
      .fade(`.${classes.ComponentFeatures} .second`)
      .fade(`.${classes.ComponentFeatures} .feature`);
  });

  return (
    <>
      <section className={`${classes.ComponentFeatures} items-center`}>
        <Container>
          <Heading className="mt-5 mt-md-0 first">
            Featured <span>Case Study</span>
          </Heading>
          <p className={`mb-5 second`}>
            The Jamstack architecture has many benefits, whether you’re building
            a large e-commerce site, an enterprise SaaS application, or a simple
            blog.
          </p>
          <CaseStudy />
        </Container>
      </section>
    </>
  );
};
export default ComponentFeatures;
