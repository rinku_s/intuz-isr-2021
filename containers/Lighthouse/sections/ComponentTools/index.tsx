//@ts-nocheck
import React from "react";
import Container from "../../../../components/Container/Container";
import classes from "./styles.module.scss";
import Heading from "../../../../components/HeadingNunito/Heading";
import PresentationLayer from "../../../../components/Lighthouse/ToolsLayer/Presentationlayer";
import Api from "../../../../components/Lighthouse/ToolsLayer/API";
import Deploy from "../../../../components/Lighthouse/ToolsLayer/Deploy";
import { scrollAnimation } from "hooks/scrollAnimation";
const ComponentToolsTech = () => {
  scrollAnimation(`.${classes.ComponentToolsTech}`, (tl) => {
    tl.fade(`.${classes.ComponentToolsTech} .first`).fade(
      `.${classes.ComponentToolsTech} .second`
    );
  });

  return (
    <>
      <section className={`${classes.ComponentToolsTech} items-center`}>
        <Container>
          <Heading className="mt-5 mt-md-0 first">
            <span>Tools & Technologies</span> We Use
          </Heading>
          <p className={`mb-5 second`}>
            Access our comprehensive tech stack for building future-ready
            JAMStack applications
          </p>
          <PresentationLayer className="pt-5" />
          <Api className="pt-5" />
          <Deploy className="pt-5" />
        </Container>
      </section>
    </>
  );
};
export default ComponentToolsTech;
