//@ts-nocheck
//@ts-nocheck
import React from "react";
import Accordian from "../../../components/CareerAccordian/Accordian";
import Container from "../../../components/Container/Container";
import style from "./styles.module.scss";

const SectionCareerTeam = (props) => {
  //   console.log("Careers Data", props.data);
  return (
    <section className={style.CareerTeam}>
      <Container>
        <h2>Exciting Opportunities @ Intuz for You</h2>
        <Accordian data={props.data} />
      </Container>
    </section>
  );
};
export default SectionCareerTeam;
