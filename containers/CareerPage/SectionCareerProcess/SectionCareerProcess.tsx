//@ts-nocheck

import React from 'react';
import Container from '../../../components/Container/Container';
import { cdn } from '../../../config/cdn';
import style from './styles.module.scss';

const SectionCareerProcess = () => {
    
    return (
        <section className={style.CareerProcess}>
            <Container>
                <div className="row">
                    <div className={`col-sm-6 col-md-3 ${style.careerBlock}`}>
                        <img src={cdn('create.png')} alt='create.png' />
                        <h2>Create</h2>
                        <p>We help you in attaining a roaring career success.</p>
                    </div>
                    <div className={`col-sm-6 col-md-3 ${style.careerBlock}`}>
                        <img src={cdn('learn.png')} alt='learn.png' />
                        <h2>Learn</h2>
                        <p>Experience an unlimited freedom to learn and grow.</p>
                    </div>
                    <div className={`col-sm-6 col-md-3 ${style.careerBlock}`}>
                        <img src={cdn('environment.png')} alt='environment.png' />
                        <h2>Environment</h2>
                        <p>Though we are techies, fun is always around the corner.</p>
                    </div>
                    <div className={`col-sm-6 col-md-3 ${style.careerBlock}`}>
                        <img src={cdn('play.png')} alt='play.png' />
                        <h2>Play</h2>
                        <p>Jollity at inside and outside the workplace makes us better.</p>
                    </div>
                </div>
            </Container>
        </section>
    )
}
export default SectionCareerProcess;