//@ts-nocheck
import React from 'react'
import Container from '../../components/Container/Container'
import SitemapComp from '../../components/SitemapComp'
import classes from './styles.module.scss'
const Sitemap = (props) => {
    return (
        <section className={classes.Sitemap}>
            <Container>
                <h1>Sitemap</h1>
                {props.content.map(smp=>(
                <SitemapComp key={smp.type} heading={smp.heading}  pages={smp.pages}/>
                ))}
            </Container>
        </section>
    )
}

export default Sitemap
