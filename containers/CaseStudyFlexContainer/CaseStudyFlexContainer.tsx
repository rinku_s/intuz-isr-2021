//@ts-nocheck
//@ts-nocheck
import React from "react";
import CaseStudyParagraph from "../../components/CaseStudyParagraph/CaseStudyParagraph";
import { Primary, Secondary } from "../../components/CaseStudyTitles";
import LazyImage from "../../components/LazyImage/LazyImage";
import { cdn } from "../../config/cdn";
import { Media, MediaContextProvider } from "../../config/responsiveQuery";
import classes from "./styles.module.scss";

const CaseStudyFlexContainer = (props) => {
  let video = props.image.includes(".mp4");
  let vid = (
    <video
      src={`${cdn(props.image)}`}
      muted
      playsInline
      autoPlay
      crossOrigin="anonymus"
    />
  );
  return (
    <MediaContextProvider>
      <div
        className={`${classes.CaseStudyFlexContainer} ${classes[props.reverse ? "rowreverse" : ""]
          }`}
      >
        <Media greaterThanOrEqual="md">
          <div className={classes.Content}>
            <Secondary>{props.title}</Secondary>
            <Primary>{props.subtitle}</Primary>
            <CaseStudyParagraph variation="large">
              {props.description}
            </CaseStudyParagraph>
          </div>
          <div className={classes.image}>
            {video && props.image && vid}
            {props.image && !video && (
              <LazyImage
                src={`${cdn(props.image)}?auto=format,compress`}
                alt={props.title}
              />
            )}
          </div>
        </Media>
        <Media lessThan="md">
          <div className={classes.Content}>
            <Secondary>{props.title}</Secondary>
            <Primary>{props.subtitle}</Primary>
            <CaseStudyParagraph variation="large">
              {props.description}
            </CaseStudyParagraph>
            <div className={classes.image}>
              {video && props.image && vid}
              {props.image && !video && (
                <LazyImage
                  src={`${cdn(props.image)}?auto=format,compress&fm=png8`}
                  alt={props.title}
                />
              )}
            </div>
          </div>
        </Media>
      </div>
    </MediaContextProvider>
  );
};

CaseStudyFlexContainer.propTypes = {};

export default CaseStudyFlexContainer;
