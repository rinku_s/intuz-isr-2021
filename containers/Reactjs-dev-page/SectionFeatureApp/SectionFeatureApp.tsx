import { myLoader } from "config/image-loader";
import Image from "next/image";
import React, { useEffect } from "react";
import SwiperCore, { Autoplay } from "swiper";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.min.css";
// import 'slick-carousel/slick/slick.css';
import PrimaryHeading from "../../../components/Heading/Heading";
import SecondaryHeading from "../../../components/Heading/SecondaryHeading";
import { cdn } from "../../../config/cdn";
import style from "./styles.module.scss";
SwiperCore.use([Autoplay]);
const SectionFeatureApp = (props) => {
  const settings: Swiper = {
    autoHeight: true,
    loop: true,
    autoplay: {
      delay: 2000,
      disableOnInteraction: true,
    },
  };
  // var leftBackImage = { background: `url(${cdn('reactjs-featured-bg.png?fm=png&auto=format')}) bottom left no-repeat`};
  // var rightBackImage = { background: `url(${cdn('reactjs-featured-right-img.png?fm=png&auto=format')}) center right no-repeat`};

  const data = [
    {
      alt: "React Slide Image 1",
      dwidth: 600,
      mwidth: 400,
      src: "react-slide-img1.png",
    },
    {
      alt: "React Slide Image 1",
      dwidth: 600,
      mwidth: 400,
      src: "react-slide-img2.png",
    },
    {
      alt: "React Slide Image 1",
      dwidth: 600,
      mwidth: 400,
      src: "react-slide-img3.png",
    },
  ];

  return (
    <section className={style.FeatureApp}>
      <div>
        <div className={style.FeatureAppLeft}>
          <PrimaryHeading>Featured ReactJs App</PrimaryHeading>
          <SecondaryHeading>
            Intuz is a pioneer in developing apps using ReactJS. Over the years,
            our experts have successfully worked on apps developed with ReactJS.
            Check out our featured ReactJS app to gauge our abilities.
          </SecondaryHeading>
        </div>
        <div className={style.FeatureAppRight}>
          <div className={style.imgSlider}>
            <Swiper {...settings}>
              {data.map((d) => (
                <SwiperSlide>
                  <Image
                    src={d.src}
                    layout="responsive"
                    width={600}
                    loader={myLoader}
                    height={500}
                  />
                </SwiperSlide>
              ))}
            </Swiper>
          </div>
          <div className={style.textblock}>
            <Image
              className="mt-2"
              layout="fixed"
              loader={myLoader}
              src={"reactjs-audio-hosting-logo.png"}
              alt="reactjs-audio-hosting-logo"
              height={45}
              width={309}
            />
            <p className="mt-4">
              Audio Hosting is a modern music streaming app explicitly targeted
              at musicians, artists and composers looking to host their original
              creations and records to reach out to their followers and fans on
              their own.
            </p>
          </div>
        </div>
      </div>
      <style jsx>
        {`
          .${style.FeatureAppLeft} {
            background: url(${cdn(
                "reactjs-featured-bg.png?fm=png&auto=format&w=1000"
              )})
              bottom left no-repeat;
            background-size: cover;
          }
          .${style.FeatureAppRight} {
            background: url(${cdn(
                "reactjs-featured-right-img.png?fm=png&auto=format&w=1000"
              )})
              center right no-repeat;
            background-size: cover;
          }

          @media only screen and (max-width: 990px) {
            .${style.FeatureAppLeft} {
              background: url(${cdn(
                  "reactjs-featured-bg.png?fm=png&auto=format&w=700"
                )})
                bottom left no-repeat;
            }
            .${style.FeatureAppRight} {
              background: url(${cdn(
                  "reactjs-featured-right-img.png?fm=png&auto=format&w=700"
                )})
                center right no-repeat;
            }
          }
          @media only screen and (max-width: 767px) {
            .${style.FeatureAppLeft} {
              background: url(${cdn(
                  "reactjs-featured-bg.png?fm=png&auto=format&w=500"
                )})
                bottom left no-repeat;
            }
            .${style.FeatureAppRight} {
              background: url(${cdn(
                  "reactjs-featured-right-img.png?fm=png&auto=format&w=500"
                )})
                center right no-repeat;
            }
          }
        `}
      </style>
    </section>
  );
};

export default SectionFeatureApp;
