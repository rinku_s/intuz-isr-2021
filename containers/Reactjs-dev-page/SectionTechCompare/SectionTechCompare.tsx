//@ts-nocheck

import React from 'react';
import Container from '../../../components/Container/Container';
import TechCompareBlock from '../../../components/TechnologyCompareBlock/index';

const SectionTechCompare = (props) => {
    return (
        <section>
            <Container>
                <TechCompareBlock activePage = {props.activePage} />
            </Container>
            <style jsx>
            {`
                section {
                    padding: 8rem 0;        
                }
            `}
            </style>
        </section>
    )
}

export default SectionTechCompare