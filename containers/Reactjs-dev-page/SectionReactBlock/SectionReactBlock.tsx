//@ts-nocheck
import React from 'react';
import Container from '../../../components/Container/Container';

const SectionReactBlock = (props) => {
    return(
        <section className='DetailBlock'>
            <Container>
                {props.children}
            </Container>
            <style jsx>
            {`
                .DetailBlock {
                    background-color: #fafafa!important;
                    padding: 8rem 0;
                }
                @media (max-width:767px) {
                    .DetailBlock {
                        padding-top: 6rem;
                        padding-bottom: 0;
                    }
                }
            
            `}
            </style>
        </section>
    )
}

export default SectionReactBlock