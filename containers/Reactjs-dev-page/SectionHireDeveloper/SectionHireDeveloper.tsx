//@ts-nocheck

import gsap from 'gsap';
import React from 'react';
import Container from '../../../components/Container/Container';
import EngagementModel from '../../../components/EngagementModel';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import { cdn } from '../../../config/cdn';
import style from './styles.module.scss';


const SectionHireDeveloper = (props) => {

    // setOpacity(`.devHeader, .devSubheader`);

    function play() {
        const tl = new gsap.timeline({ paused: true });
        tl.fromTo(`.devHeader`, 1, { y: -20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.5)
            .fromTo(`.devSubheader`, 1, { y: 20, opacity: 0, ease: "Power0.ease" }, { y: 0, opacity: 1, ease: "Power0.ease" }, 0.5);
        tl.play();
    }

    var backImage = { background: `url(${cdn(`${props.backImage}.png?fm=png&auto=format`)}) center center no-repeat` };


    return (

        <section className={style.HireDev} style={backImage}>

            <Container>
                <PrimaryHeading className='devHeader'>{props.title}</PrimaryHeading>
                <SecondaryHeading className='devSubheader'>{props.description}</SecondaryHeading>
                <EngagementModel button={true} />
            </Container>
        </section>
        // </WaypointOnce >
    )
}

export default SectionHireDeveloper