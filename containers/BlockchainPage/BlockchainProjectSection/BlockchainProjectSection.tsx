//@ts-nocheck
import React from 'react';
import BlockProject from '../../../components/BlockchainComponents/BlockchainProject/BlockProject';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import classes from './styles.module.scss';
import SecurityandTechPoints from '../../../components/BlockchainComponents/BlockchainProject/SecurityandTechPoints/SecurityandTechPoints';
const BlockchainProjectSection = () => {
    return (
        <section className={classes.BlockchainProjectSection}>
            <Container>

            <PrimaryHeading>
               Our Blockchain Projects
            </PrimaryHeading>
            <SecondaryHeading>
                Some of the recent projects we have worked on
            </SecondaryHeading>
            <BlockProject/>
            </Container>
            <SecurityandTechPoints/>

        </section>
    )
}

export default BlockchainProjectSection
