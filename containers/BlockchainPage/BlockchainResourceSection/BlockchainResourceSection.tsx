//@ts-nocheck
import { myLoader } from "config/image-loader";
import Image from "next/image";
import React from "react";
import LazyLoad from "react-lazyload";
import Container from "../../../components/Container/Container";
import PrimaryHeading from "../../../components/Heading/Heading";
import SecondaryHeading from "../../../components/Heading/SecondaryHeading";
import Resources from "../../../components/Resources/Resources";
import { cdn } from "../../../config/cdn";
import classes from "./styles.module.scss";
const BlockchainResourceSection = (props) => {
  return (
    <section className={classes.BlockchainResourceSection}>
      <Container>
        <div className="flex flex-col md:flex-row justify-content-between items-center">
          <div>
            <PrimaryHeading style={{ textAlign: "left" }}>
              Intuz Resources
            </PrimaryHeading>
            <SecondaryHeading style={{ textAlign: "left" }}>
              Insights on latest technology trends, enterprise mobility
              solutions, & company updates
            </SecondaryHeading>
          </div>
          <LazyLoad height={300} offset={300} once>
            <div className="text-center md:text-right">
              <span
                style={{
                  display: "inline-block",
                  marginRight: "1.5rem",
                  marginBottom: "2rem",
                }}
              >
                FEATURED ON
              </span>
              <br />
              <Image
                layout="intrinsic"
                loader={myLoader}
                src={"startup-logo-rs.png"}
                alt="Startup logo"
                width="148"
                height="24"
              />
              <Image
                layout="intrinsic"
                loader={myLoader}
                src={"medium-logo-rs.png"}
                alt="Medium logo"
                width="90"
                height="24"
              />
              <Image
                layout="intrinsic"
                loader={myLoader}
                src={"hackernoon-logo-rs.png"}
                alt="Hackernoon logo"
                width="148"
                height="24"
              />
            </div>
          </LazyLoad>
        </div>
        <Resources sectionresources={props.sectionresources} />
      </Container>
    </section>
  );
};

export default BlockchainResourceSection;
