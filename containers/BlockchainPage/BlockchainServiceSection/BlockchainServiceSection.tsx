//@ts-nocheck
import React from 'react'
import PrimaryHeading from '../../../components/Heading/Heading';
import Container from '../../../components/Container/Container';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import BlockchainServices from '../../../components/BlockchainComponents/BlockchainServices/BlockchainServices';

const BlockchainServiceSection = (props) => {
    return (
        <section>
            <Container>
                <PrimaryHeading>Services</PrimaryHeading>
                <SecondaryHeading>Intricate and reliable blockchain solutions for all kind of applications</SecondaryHeading>
                <BlockchainServices blockchainservices={props.blockchainservices}/>
            </Container>
            
        </section>
    )
}

export default BlockchainServiceSection
