//@ts-nocheck
import React from 'react'
import classes from './styles.module.scss'
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import IndustryUseCase from '../../../components/BlockchainComponents/IndustryUseCase';
const IndustryUseCaseSection = (props) => {
    return (
        <section className={classes.IndustryUseCaseSection}>
            <Container>
                <PrimaryHeading>
                    Industry Use Case
                </PrimaryHeading>
                <SecondaryHeading>
                Disrupting the way businesses used to work before
                </SecondaryHeading>
                <IndustryUseCase blockchainusecases={props.blockchainusecases}/>
            </Container>
        </section>
    )
}

export default IndustryUseCaseSection
