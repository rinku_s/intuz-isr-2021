//@ts-nocheck
import React from 'react'
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import WhyChoseIntuz from '../WhyChoseIntuz/WhyChoseIntuz';
import classes from './styles.module.scss';
const WhyChoseIntuzSection = () => {
    return (
        <section className={classes.WhyChoseIntuzSection}>
            <Container>
                <PrimaryHeading>
                Why Choose Intuz for Blockchain Development?
                </PrimaryHeading>
                <WhyChoseIntuz/>
            </Container>
        </section>
    )
}

export default WhyChoseIntuzSection
