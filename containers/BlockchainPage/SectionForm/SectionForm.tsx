//@ts-nocheck
import React from 'react'
import { Container } from 'react-bootstrap';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import Form from '../../../components/BlockchainComponents/Form/Form'
import classes from './styles.module.scss';

const SectionFormBlockChain = () => {
    return (
        <section className={classes.SectionFormBlockChain}>
            <Container>
                <PrimaryHeading>
                Book A Free 1 on 1 Consultation
                </PrimaryHeading>
                <SecondaryHeading>
                Please fill in the form below to schedule a call or request an estimate
                </SecondaryHeading>
                <Form/>
            </Container>
        </section>
    )
}

export default SectionFormBlockChain
