//@ts-nocheck
import React from 'react';
import TechnologyStack from '../../../components/BlockchainComponents/TechnologyStack/TechnologyStack';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import classes from './styles.module.scss';
const TechnologyStackSection = (props) => {
    return (
        <section className={classes.TechnologyStackSection}>
            <Container>
                <PrimaryHeading>
                     Our Technology Stack
                </PrimaryHeading>
                <SecondaryHeading>
                    Various branches of blockchain development that we use
                </SecondaryHeading>
                <TechnologyStack blockchaintechstacks={props.blockchaintechstacks}/>
            </Container>
        </section>
    )
}

export default TechnologyStackSection
