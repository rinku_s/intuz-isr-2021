//@ts-nocheck
import React from "react";
import Point from "../../../components/BlockchainComponents/BlockchainProject/Point";
import ImageBlock from "../../../components/ImageBlock/ImageBlock";
import { cdn } from "../../../config/cdn";
import { myLoader } from "config/image-loader";
import Image from "next/image";
const WhyChoseIntuz = () => {
  return (
    <div className="flex flex-col sm:flex-row justify-content-between items-center mt-4">
      <ul>
        <Point>
          An expert team of highly-experienced blockchain and smart contract
          developers.
        </Point>
        <Point>
          Customizable and highly-scalable development solutions based on
          business requirement.
        </Point>
        <Point>
          Established track record of developing business solutions based on
          disruptive technologies.
        </Point>
        <Point>
          Faster turnaround time and highly-responsive team for quick project
          deployment.
        </Point>
        <Point>
          End-to-end business solutions including migration of existing
          processes and development of new solutions.
        </Point>
      </ul>
      <div>
        <Image
          layout="intrinsic"
          loader={myLoader}
          src={"blockchain-solution.png"}
          width="540"
          height="390"
          alt="BlockChain_solution"
        />
      </div>
      <style jsx>
        {`
          @media only screen and (max-width: 767px) {
            .flex-col {
              flex-direction: column !important;
            }
          }
        `}
      </style>
    </div>
  );
};

export default WhyChoseIntuz;
