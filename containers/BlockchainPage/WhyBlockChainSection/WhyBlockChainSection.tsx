//@ts-nocheck
import React from 'react';
import WhyBlockChain from '../../../components/BlockchainComponents/WhyBlockChainComp/WhyBlockChain';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';

const WhyBlockChainSection = () => {
    return (
        <section>
            <Container>
            <PrimaryHeading>
                 Why Blockchain?
            </PrimaryHeading>
            <SecondaryHeading>
               Technology that makes businesses future-ready
            </SecondaryHeading>
            <WhyBlockChain/>
            </Container>
        </section>
    )
}

export default WhyBlockChainSection
