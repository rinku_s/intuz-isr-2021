//@ts-nocheck
import React from "react";
import LazyLoad from "react-lazyload";
import CaseStudyParagraph from "../../../components/CaseStudyParagraph/CaseStudyParagraph";
import { Primary, Secondary } from "../../../components/CaseStudyTitles";
import Container from "../../../components/Container/Container";
import { cdn } from "../../../config/cdn";
// import CaseStudyFlexContainer from "../../CaseStudyFlexContainer/CaseStudyFlexContainer";
import classes from "./styles.module.scss";

const DesignChallengeSectionnew = (props) => {
  return (
    <section
      className={`${classes.SingleProjectChallenge} ${
        props.content.fullpageImage && classes.FullImg
      }`}
      id="dchallenge"
    >
      <Container>
        {props.content.fullpageImage ? (
          <>
            <Secondary>Design Challenges</Secondary>
            <Primary>{props.content.subtitile}</Primary>
            <CaseStudyParagraph variation="large">
              {props.content.description}
            </CaseStudyParagraph>
            <LazyLoad offset={300} height={300}>
              <img
                src={cdn(
                  `${props.content.image?.name}?auto=format,compress&w=900`
                )}
                alt={props.content.image?.name}
              />
            </LazyLoad>
          </>
        ) : (
          <></>
        )}
      </Container>
    </section>
  );
};

export default DesignChallengeSectionnew;
