//@ts-nocheck
import React from "react";
import { cdn } from "../../../config/cdn";
import Container from "../../../components/Container/Container";
const FourbVideosection = (content) => {
  // console.log("content ", content);
  return (
    <div>
      <Container className="text-center">
        <video
          style={{ maxWidth: "90%" }}
          src="/4bHighlighsection.mp4"
          muted
          playsInline
          autoPlay
        />
      </Container>
    </div>
  );
};

export default FourbVideosection;
