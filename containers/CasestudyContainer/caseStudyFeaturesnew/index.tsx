//@ts-nocheck
// import { from } from "apollo-boost";
import React from "react";
import CaseStudyFeature from "../../../components/CaseStudyFeature/CaseStudyFeature";
import CaseStudyParagraph from "../../../components/CaseStudyParagraph/CaseStudyParagraph";
import { Primary, Secondary } from "../../../components/CaseStudyTitles";
import Container from "../../../components/Container/Container";
import style from "./styles.module.scss";


const CaseStudyFeatureSectionnew = ({ content }) => {


  return (
    <section className={style.Feature} id="feature">
      <Container>
        <Secondary>Exciting App Features</Secondary>
        <Primary>{content.title}</Primary>
        <CaseStudyParagraph variation="large">
          {content.description}
        </CaseStudyParagraph>
        <CaseStudyFeature list={content.featurelist}/>
      </Container>
    </section>
  );
};

export default CaseStudyFeatureSectionnew;
