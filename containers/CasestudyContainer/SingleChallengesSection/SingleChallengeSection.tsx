//@ts-nocheck
//@ts-nocheck
import React from "react";
import LazyLoad from "react-lazyload";
import CaseStudyParagraph from "../../../components/CaseStudyParagraph/CaseStudyParagraph";
import { Primary, Secondary } from "../../../components/CaseStudyTitles";
import Container from "../../../components/Container/Container";
import { cdn } from "../../../config/cdn";
import CaseStudyFlexContainer from "../../CaseStudyFlexContainer/CaseStudyFlexContainer";
import classes from "./styles.module.scss";

const SingleChallengeSection = (props) => {
  return (
    <section
      className={`${classes.SingleProjectChallenge} ${props.content.fullpageimage && classes.FullImg
        }`}
      id="pchallenge"
    >
      <Container>
        {props.content.fullpageimage ? (
          <>
            <Secondary>Project Challenges</Secondary>
            <Primary>{props.content.title}</Primary>
            <CaseStudyParagraph variation="large">
              {props.content.description}
            </CaseStudyParagraph>
            <LazyLoad offset={300} height={300}>
              <img
                src={cdn(
                  `${props.content.icon.name}?auto=format,compress&w=900`
                )}
                alt={props.content.icon.name}
              />
            </LazyLoad>
          </>
        ) : (
          <CaseStudyFlexContainer
            image={props.content.icon?.name}
            title="Project Challenges"
            subtitle={props.content.title}
            isFourB={props.isFourB}
            description={props.content.description}
          />
        )}
      </Container>
    </section>
  );
};

export default SingleChallengeSection;
