//@ts-nocheck
import React from 'react';
import CaseStudyTechnical from '../../../components/CaseStudyTechnical/CaseStudyTechnical';
import { Primary } from '../../../components/CaseStudyTitles';
import Container from '../../../components/Container/Container';
import classes from './styles.module.scss';
const TechnicalSpecs = props => {
    return (
        <section className={classes.TechnicalSpecs} id="techstack">
            <Container>
            <Primary>Technical Specifications</Primary>
            <CaseStudyTechnical content={props.content}/>
            {/* <Secondary>Translating business requirements</Secondary> */}
            </Container>
        </section>
    )
}


export default TechnicalSpecs
