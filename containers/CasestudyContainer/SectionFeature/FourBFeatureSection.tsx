//@ts-nocheck
//@ts-nocheck

import React from "react";
import CaseStudyParagraph from "../../../components/CaseStudyParagraph/CaseStudyParagraph";
import { Primary, Secondary } from "../../../components/CaseStudyTitles";
import Container from "../../../components/Container/Container";
import LazyImage from "../../../components/LazyImage/LazyImage";
import { cdn } from "../../../config/cdn";
import windowWidth from "../../../hooks/windowWidth";
import style from "./styles.module.scss";

const FourBFeatureSection = (props) => {
  let windowSize = windowWidth();
  return (
    <section className={style.Feature}>
      <Container>
        <Secondary>Exciting App Features</Secondary>
        <Primary>{props.content.title}</Primary>
        <CaseStudyParagraph variation="large">
          {props.content.description}
        </CaseStudyParagraph>
        <div className={`row`}>
          <div className={`col-12 col-md-6 col-sm-12`}>
            <video
              src={`${cdn(props.content.video.name)}`}
              muted
              playsInline
              autoPlay
            />
          </div>
          <div className={`col-12 col-md-6 col-sm-12 mt-3`}>
            <div className={style.Feature_image}>
              <LazyImage
                src={cdn(
                  `${props.content.image.name}?fm=png8&auto=format${windowSize < 768 ? "&w=767" : ""
                  }`
                )}
                alt={props.content.image.name}
              />
            </div>
          </div>
        </div>
      </Container>
    </section>
  );
};

export default FourBFeatureSection;
