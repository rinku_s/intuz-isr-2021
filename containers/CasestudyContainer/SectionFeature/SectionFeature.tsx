//@ts-nocheck
//@ts-nocheck

import React from 'react';
import CaseStudyParagraph from '../../../components/CaseStudyParagraph/CaseStudyParagraph';
import { Primary, Secondary } from '../../../components/CaseStudyTitles';
import Container from '../../../components/Container/Container';
import LazyImage from '../../../components/LazyImage/LazyImage';
import { cdn } from '../../../config/cdn';
import windowWidth from '../../../hooks/windowWidth';
import SectionVideoFeature from './SectionVideoFeature';
import style from './styles.module.scss';

const SectionFeature = (props) => {
    let windowSize = windowWidth();

    return (
        <section className={style.Feature} id="feature">
            {
                props.content.animatedImage ? <SectionVideoFeature content={props.content} /> : <Container>
                    {props.content.fullpageimage ? <><Secondary>Exciting App Features</Secondary>
                        <Primary>{props.content.title}</Primary>
                        <CaseStudyParagraph variation="large">{props.content.description}</CaseStudyParagraph> </>
                        : ''}
                    <div className={`${style.Content} ${props.content.description ? style.flexcontent : ''}`}>
                        {props.content.fullpageimage ? '' :
                            <div>
                                <Secondary>Exciting App Features</Secondary>
                                <Primary>{props.content.title}</Primary>
                                <CaseStudyParagraph variation="large">{props.content.description}</CaseStudyParagraph>
                            </div>
                        }{
                            props.content.image &&
                            <div className={style.Feature_image}>
                                <LazyImage src={cdn(`${props.content.image.name}?fm=png8&auto=format${windowSize < 768 ? '&w=767' : ''}`)} alt={props.content.image.name} />
                            </div>
                        }
                    </div>

                </Container>
            }
        </section>
    )
}

export default SectionFeature