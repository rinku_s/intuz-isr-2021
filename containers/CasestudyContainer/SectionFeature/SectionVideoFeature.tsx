//@ts-nocheck
//@ts-nocheck

import React, { useEffect } from "react";
import CaseStudyParagraph from "../../../components/CaseStudyParagraph/CaseStudyParagraph";
import { Primary, Secondary } from "../../../components/CaseStudyTitles";
import Container from "../../../components/Container/Container";
import { cdn } from "../../../config/cdn";
import { setVideoBgColor } from "../../../hooks/useBackground";
import windowWidth from "../../../hooks/windowWidth";
import style from "./styles.module.scss";

const SectionVideoFeature = (props) => {
  let windowSize = windowWidth();

  useEffect(() => {
    var vid = document.getElementById("video");

    var wrapper = document.getElementById("main");
    var canvas = document.getElementById("myCanvas");
    var ctx = canvas.getContext("2d");

    var ratio = window.devicePixelRatio || 1;
    var vidWidth;
    var vidHeight;

    function drawingLoop() {
      window.requestAnimationFrame(drawingLoop);
      ctx.drawImage(
        vid,
        0,
        0,
        vidWidth,
        vidHeight,
        0,
        0,
        canvas.width,
        canvas.height
      );
    }

    vid.addEventListener("loadedmetadata", function (e) {
      console.log("onloadedmetadata");
      vid.style.transform = `scale(${ratio})`;

      vidWidth = vid.videoWidth;
      vidHeight = vid.videoHeight;

      canvas.width = vid.offsetWidth * ratio;
      canvas.height = vid.offsetHeight * ratio;

      canvas.style.width = vid.offsetWidth + "px";
      canvas.style.height = vid.offsetHeight + "px";

      drawingLoop(ctx);
    });

    vid.addEventListener("loadeddata", function (e) {
      console.log("onloadeddata");
      setVideoBgColor(vid, wrapper);
      vid.play();
    });

    window.onresize = function (event) {
      vidWidth = vid.videoWidth;
      vidHeight = vid.videoHeight;

      canvas.width = vid.offsetWidth * ratio;
      canvas.height = vid.offsetHeight * ratio;

      canvas.style.width = vid.offsetWidth + "px";
      canvas.style.height = vid.offsetHeight + "px";

      //redraw canvas after resize
      ctx.drawImage(
        vid,
        0,
        0,
        vidWidth,
        vidHeight, // source rectangle
        0,
        0,
        canvas.width,
        canvas.height
      ); // destination rectangle);
    };

    //add src
    vid.src = vid.dataset.src;
    vid.play();
    let sto = setTimeout(() => {
      vid.style.visibility = "hidden";
      vid.style.height = "0px";
    }, 3000);
  }, []);

  return (
    <>
      <Container>
        {props.content.fullpageimage ? (
          <>
            <Secondary>Exciting App Features</Secondary>
            <Primary>{props.content.title}</Primary>
            <CaseStudyParagraph variation="large">
              {props.content.description}
            </CaseStudyParagraph>{" "}
          </>
        ) : (
          ""
        )}
      </Container>
      <div
        className={`${style.Content} ${
          props.content.description ? style.flexcontent : ""
        }`}
      >
        <canvas className="hidden d-mblock" id="myCanvas"></canvas>
        <video
          playsinline="true"
          crossOrigin="Anonymous"
          id="video"
          width="100%"
          autoPlay
          preload="true"
          muted="muted"
          loop
          className="hidden d-mblock"
          data-src="/static/Images/case-studies/speckpump/speckpump-feature.mp4"
          // poster={cdn('speckpump-feature-posterimg.png?auto=format')}
        >
          {/* <source src="/static/Images/case-studies/speckpump/speckpump-feature.mp4" type="video/mp4" />
                                    <source src="/static/Images/case-studies/speckpump/speckpump-feature.webm" type="video/webm" /> */}
          Sorry, your browser doesn't support embedded videos.
        </video>

        <img
          className="md:hidden"
          src={cdn("speckpump-feature-img.png?auto=format&w=760")}
          alt="speckpump-feature-img"
        />
      </div>
    </>
  );
};

export default SectionVideoFeature;
