//@ts-nocheck
//@ts-nocheck
import React from 'react'
import CaseStudyDevProcess from '../../../components/CaseStudyDevProcess'
import { Primary, Secondary } from '../../../components/CaseStudyTitles'
import Container from '../../../components/Container/Container'
import classes from './styles.module.scss'
const DevProcessSection: React.FC<anys> = props => {
    return (
        <section className={classes.DevProcessSection} id="process">
            <Container>
                <div className='text-center'>
                    <Secondary>Developement Process</Secondary>
                    <Primary>Implement operative objectives that fulfill an underlying goal</Primary>
                </div>
                <CaseStudyDevProcess process={props.process} descriptions={props.descriptions} />
            </Container>
        </section>
    )
}

DevProcessSection.propTypes = {

}

export default DevProcessSection
