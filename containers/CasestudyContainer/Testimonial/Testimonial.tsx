//@ts-nocheck
//@ts-nocheck

import React from 'react';
import LazyImage from '../../../components/LazyImage/LazyImage';
import { cdn } from '../../../config/cdn';
import classes from './styles.module.scss';
const Testimonial = (props) => {
    return (
        <div className={classes.Testimonial}>
            <p className={classes.desc}>{props.testimonial}</p>
            <figure>
                {
                    props.image != null ? <LazyImage width="99" height="99" src={cdn(props.image.name)}
                        alt={props.name} /> : ''
                }
                <figcaption>
                    <p>{props.name}</p>
                    <p dangerouslySetInnerHTML={{ __html: props.status }}></p>
                </figcaption>
            </figure>
        </div>
    )
}

export default Testimonial
