//@ts-nocheck
import React from 'react';
import { Primary, Secondary } from '../../../components/CaseStudyTitles';
import Container from '../../../components/Container/Container';
import classes from './styles.module.scss';
import Testimonial from './Testimonial';
const Testimonialsection = (props) => {
    return (
        <section className={classes.Testimonialsection} id="testimonial">
            <Container>
            <Primary>
            Client Testimonials
            </Primary>
            <Secondary>
                Acknowledgement & appreciation from our clients drive us
            </Secondary>
            <Testimonial  
                {...props.content}
            />
            </Container>
        </section>
    )
}

export default Testimonialsection
