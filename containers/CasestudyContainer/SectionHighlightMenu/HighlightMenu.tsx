//@ts-nocheck

import gsap from 'gsap';
import ScrollToPlugin from 'gsap/dist/ScrollToPlugin';
import React from 'react';
import Scrollspy from 'react-scrollspy';
import Container from '../../../components/Container/Container';
import style from './styles.module.scss';

gsap.registerPlugin(ScrollToPlugin)
const HighlightMenu = (props) => {
    

    const goToId = (e, link) => {
        e.preventDefault();
        let offset;
        if(link === 'highlightsection'){
            offset = 400;            
        } else {
            offset = 150;
        }
        gsap.to(window, {duration: 2, scrollTo: {y: `#${link}`, offsetY: offset}});
    }

    const link = props.content.map(e=>(
        e.link
    ))
    link.unshift('casestudyBanner');
    return (
        
        <section className={`${style.HighlightMenu}`} >
            <Container>
                {/* <span className={style.active} ><a onClick={(e) => gotoPageTop(e)}>{props.casestudyname}</a></span> */}
                <Scrollspy className={style.menu} items={ link } currentClassName={style.active} offset={-150}>
                    <li className={style.active}><a onClick={(e)=>goToId(e, 'casestudyBanner')}>{props.casestudyname}</a></li>
                    {props.content.map((menu, index) => {
                        return (
                            <li key={index}><a onClick={(e)=>goToId(e, menu.link)} >{menu.name}</a></li>
                        )
                    })}              
                </Scrollspy>
                
            </Container>
            <style jsx>
                {`
                    .${style.HighlightMenu}{
                        background: ${props.stickycolor};
                    }
                `}
            </style>
        </section>
    )
}

export default HighlightMenu