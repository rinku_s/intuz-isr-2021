//@ts-nocheck
//@ts-nocheck
import React from "react";
import CaseStudyParagraph from "../../../components/CaseStudyParagraph/CaseStudyParagraph";
import { Primary, Secondary } from "../../../components/CaseStudyTitles";
import Container from "../../../components/Container/Container";
import { cdn } from "../../../config/cdn";
// import CaseStudyFlexContainer from "../../CaseStudyFlexContainer/CaseStudyFlexContainer";
import classes from "./styles.module.scss";
const IconBoxChallengeSection = (props) => {
  return (
    <section className={`${classes.IconboxProjectChallenge} `} id="pchallenge">
      <Container>
        <>
          <Secondary>Project Challenges</Secondary>
          <Primary>{props.content.title}</Primary>
          <CaseStudyParagraph
            variation="large"
            className={`${classes.marginbottomdescription}`}
          >
            {props.content.description}
          </CaseStudyParagraph>

          {props.content.projectchallengesiconbox.map((data, index) => {
            let indexnu = index % 2 == 0;
            if (indexnu === true) {
              return (
                <Container
                  id={index}
                  className={
                    `${classes.iconbox} ${classes.boxeven} ` +
                    (index !== 0 ? `mt-n4` : "")
                  }
                >
                  <div
                    className={`row ${classes.mobilebottom} ${index === 4 ? `${classes.margintopn65}` : ""
                      }`}
                  >
                    <div className="col-md-10  col-sm-8 col-8">
                      <p
                        className={`text-right mr-4 ${index === 4 ? `${classes.extrapaddingp}` : ""
                          }`}
                      >
                        {data.description}
                      </p>
                    </div>
                    <div className="col-md-2  col-sm-4 col-4">
                      <div className={`${classes.diamond}`}>
                        <img
                          src={cdn(`${data.icon.name}?auto=format,compress`)}
                          alt={data.icon.name}
                          className=""
                        />
                      </div>
                    </div>
                  </div>
                </Container>
              );
            } else {
              return (
                <Container
                  id={index}
                  className={
                    `${classes.iconbox}  ${classes.boxodd} ` +
                    (index !== 0 ? `mt-n4` : "")
                  }
                >
                  <div
                    className={`row  ${classes.mobilebottom} ${index === 3 ? `${classes.margintopn65}` : ""
                      }`}
                  >
                    <div className={`col-md-2 col-sm-4 col-4 `}>
                      <div
                        className={`${classes.diamond} ${classes.diamondmobileleft}`}
                      >
                        <img
                          src={cdn(`${data.icon.name}?auto=format,compress`)}
                          alt={data.icon.name}
                          className=""
                        />
                      </div>
                    </div>
                    <div className={`col-md-10 col-sm-8 col-8 `}>
                      <p className="text-left ml-4">{data.description}</p>
                    </div>
                  </div>
                </Container>
              );
            }
          })}
        </>
      </Container>
    </section>
  );
};

export default IconBoxChallengeSection;
