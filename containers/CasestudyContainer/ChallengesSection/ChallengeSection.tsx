//@ts-nocheck
//@ts-nocheck
import React from 'react';
import CaseStudyChallenges from '../../../components/CaseStudyChallenges';
import { Secondary } from '../../../components/CaseStudyTitles';
import Container from '../../../components/Container/Container';
import classes from './styles.module.scss';


const ChallengeSection = (props) => {
    return (
        <section className={classes.ChallengeSection} id="pchallenge">
            <Container>
                <div className='text-center'>
                    <Secondary>Project Challenges</Secondary>
                    {/* <Primary>Objectives that fulfill an underlying&nbsp;goal</Primary> */}
                </div>
                <CaseStudyChallenges content={props.content} dataLength={props.dataLength} />
            </Container>
        </section>
    )
}

export default ChallengeSection
