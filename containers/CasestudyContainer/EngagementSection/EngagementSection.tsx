//@ts-nocheck
//@ts-nocheck
import React from 'react';
import Container from '../../../components/Container/Container';
import CaseStudyFlexContainer from '../../CaseStudyFlexContainer/CaseStudyFlexContainer';
import classes from './styles.module.scss';
const EngagementSection = props => {
    return (
        <section className={classes.EngagementSection} id="engagement">
            <Container>
                <CaseStudyFlexContainer
                    reverse
                    image={props.content.image.name}
                    title={props.content.title}
                    subtitle={props.content.subtitile}
                    description={props.content.description}
                />
            </Container>
        </section>
    )
}


export default EngagementSection

