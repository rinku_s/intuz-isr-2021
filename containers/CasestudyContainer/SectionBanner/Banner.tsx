//@ts-nocheck
//@ts-nocheck
import React from "react";
import Container from "../../../components/Container/Container";
import LazyImage from "../../../components/LazyImage/LazyImage";
import { cdn } from "../../../config/cdn";
import style from "./styles.module.scss";

const Banner = (props) => {
  return (
    <section className={style.Banner} id="casestudyBanner">
      <Container>
        <div className={style.Content}>
          <LazyImage
            className={style.logo}
            src={cdn(props.content.logo.name)}
            alt={props.content.logo.name}
          />
          <h1>{props.content.title}</h1>
          <div className={`${style.Icongrp} flex items-center`}>
            {props.content.icons.map((icon, index) => {
              return (
                <LazyImage
                  style={{ width: "3rem", height: "4rem" }}
                  key={index}
                  src={cdn(icon.name)}
                  alt={icon.name}
                />
              );
            })}
          </div>
        </div>
      </Container>
    </section>
  );
};

export default Banner;
