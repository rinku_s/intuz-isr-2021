//@ts-nocheck

//@ts-nocheck
import React, { useEffect } from "react";
import SVG from "react-inlinesvg";
import LazyLoad from "react-lazyload";
import CaseStudyHighlightBlock from "../../../components/CaseStudyHighlightBlock/index";
import CaseStudyParagraph from "../../../components/CaseStudyParagraph/CaseStudyParagraph";
import { Primary, Secondary } from "../../../components/CaseStudyTitles";
import Container from "../../../components/Container/Container";
import HighlightIcon from "../../../components/HighlightIcons/HighlightIcon";
import LazyImage from "../../../components/LazyImage/LazyImage";
import { cdn } from "../../../config/cdn";
import { detEdgeOrIE } from "../../../config/ie";
import windowWidth from "../../../hooks/windowWidth";
import FourbVideosection from "../FourbVideosection/FourbVideosection";
import style from "./styles.module.scss";

const HighlightMenu = (props) => {
  let windowSize = windowWidth();
  let gif = props.content.image?.name.includes("gif");
  let webm =
    props.content.image?.name.includes("webm") ||
    props.content.image?.name.includes("mp4");
  let mp4 = props.content.image?.name.includes("mp4");

  useEffect(() => {
    // if(window.innerWidth >= 768){
    // play();
    // }
  }, []);

  async function play() {
    var canvas = document.getElementById("myCanvas");
    var context = canvas.getContext("2d");
    let p = props.content.frame_folder.split(";");
    // console.log(p);
    let framefolder = p[0];
    let counter = 1;
    let framecount = parseInt(p[1]);

    var fpsInterval, startTime, now, then, elapsed;

    function startAnimating(fps) {
      fpsInterval = 1000 / fps;
      then = Date.now();
      startTime = then;
      animate();
    }

    function animate() {
      window.requestAnimationFrame(animate);
      now = Date.now();
      elapsed = now - then;
      if (elapsed > fpsInterval) {
        then = now - (elapsed % fpsInterval);
        var image_1 = new Image();
        image_1.src =
          "/images/frames/" +
          framefolder +
          "/ezgif-frame-" +
          counter.toString().padStart(3, "0") +
          ".jpg";
        image_1.onload = function () {
          context.clearRect(0, 0, context.canvas.width, context.canvas.height);
          context === null || context === void 0
            ? void 0
            : context.drawImage(image_1, 0, 0);
        };
        if (counter == framecount) {
          counter = 1;
        }
        counter++;
      }
    }

    startAnimating(21);
  }

  let video = (


    < canvas
      id="myCanvas"
      height="650"
      width="1440"
    >

    </canvas >

  );
  // console.log(gif);
  return (
    <section className={style.Highlight} id="highlight">
      <Container>
        <Secondary>Highlights</Secondary>
        <Primary>{props.content.title}</Primary>
        <CaseStudyParagraph variation="small" id="highlightsection">
          {props.content.subtitle}
        </CaseStudyParagraph>
      </Container>
      <Container className={style.containerwidthchange}>
        <HighlightIcon content={props.content.casestudyhighlightsectionicons} />
      </Container>
      <Container>
        <CaseStudyHighlightBlock
          content={props.content.casestudyhighlightblocks}
        />
      </Container>
      <Container>
        {props.content.AnimatedImage && windowSize > 767 && !webm && (
          <LazyLoad offset={100}>
            {!detEdgeOrIE() ? (
              <SVG src="/static/Images/case-studies/speckpump/speckpump-highlight.svg" />
            ) : (
              <img
                src="/static/Images/case-studies/speckpump/speckpump-highlight.svg"
                alt="speckpump-highlight"
                className={style.AnimatedImage}
              />
            )}
          </LazyLoad>
        )}
        {props.content.AnimatedImage && windowSize < 768 && !webm && (
          <LazyLoad offset={300} height={300}>
            <img
              src={cdn("speckpump-highlight-img.png?auto=format&width=760")}
              alt="speckpump-highlight-img"
              className={style.centerImg}
            />
          </LazyLoad>
        )}
        {props.content.image && !webm ? (
          <LazyImage
            src={cdn(
              `${props.content.image.name}${windowSize < 768 ? (gif ? "" : "?w=767") : ""
              }`
            )}
            alt={props.content.image.name}
            className={style.centerImg}
          />
        ) : (
          ""
        )}
        {props.content.frame_folder && video}
        {props.isFourB && <FourbVideosection />}
      </Container>
    </section>
  );
};

export default HighlightMenu;
