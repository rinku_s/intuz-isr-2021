//@ts-nocheck
import React from "react";
import LazyLoad from "react-lazyload";
import CaseStudyParagraph from "../../../components/CaseStudyParagraph/CaseStudyParagraph";
import { Primary, Secondary } from "../../../components/CaseStudyTitles";
import Container from "../../../components/Container/Container";
import { cdn } from "../../../config/cdn";
// import CaseStudyFlexContainer from "../../CaseStudyFlexContainer/CaseStudyFlexContainer";
import classes from "./styles.module.scss";

const SolutionSection = (props) => {
  return (
    <section
      className={`${classes.SingleProjectChallenge} ${
        props.content.fullpageImage && classes.FullImg
      } `}
      id="solutionsection"
    >
      <Container>
        <>
          <Secondary>{props.content.title}</Secondary>
          <Primary>{props.content.subtitle}</Primary>
          <CaseStudyParagraph variation="large">
            {props.content.description}
          </CaseStudyParagraph>
        </>
      </Container>
      <Container>
        <div className="row">
          {props.content.solution.map((data, index) => {
            
            let indexnu = index % 2 == 0;
            if (indexnu === true) {
              return (
                <div
                  key={index}
                  className={`${classes.box} ${classes.margintopodd} col-12 col-md col-sm-12 my-5`}
                >
                  <img
                    src={cdn(`${data.icon.name}?auto=format,compress`)}
                    alt={data.icon.name}
                    className=""
                  />
                  <p className="my-3">{data.description}</p>
                </div>
              );
            } else {
              return (
                <div key={index} className={`${classes.box}  col-12 col-md col-sm-12 my-5`}>
                  <img
                    src={cdn(`${data.icon.name}?auto=format,compress`)}
                    alt={data.icon.name}
                    className={`${classes.mobileview} m-auto`}
                  />
                  <p className="my-3">{data.description}</p>
                  <img
                    src={cdn(`${data.icon.name}?auto=format,compress`)}
                    alt={data.icon.name}
                    className={`${classes.desktop} m-auto`}
                  />
                </div>
              );
            }
          })}
        </div>
      </Container>
    </section>
  );
};

export default SolutionSection;
