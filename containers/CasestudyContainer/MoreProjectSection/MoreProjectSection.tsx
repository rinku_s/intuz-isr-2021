//@ts-nocheck
//@ts-nocheck
import React from 'react';
import { Primary } from '../../../components/CaseStudyTitles';
import MoreProject from '../../../components/MoreProjectComponent';
import classes from './styles.module.scss';
const MoreProjectSection = props => {
    return (
        <section className={classes.MoreProjectSection} id="more">
            <Primary>
                More Projects
            </Primary>
            <MoreProject content={props.content} />
        </section>
    )
}

export default MoreProjectSection
