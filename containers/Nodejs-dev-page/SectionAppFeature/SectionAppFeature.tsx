//@ts-nocheck

import React from "react";
import LazyLoad from "react-lazyload";
import Container from "../../../components/Container/Container";
import PrimaryHeading from "../../../components/Heading/Heading";
import SecondaryHeading from "../../../components/Heading/SecondaryHeading";
import ImageBlock from "../../../components/ImageBlock/ImageBlock";
import { cdn } from "../../../config/cdn";
import style from "./styles.module.scss";
import Image from "next/image";
import { myLoader } from "config/image-loader";
const SectionAppFeature = () => {
  return (
    <section className={style.FeatureBlock}>
      <Container>
        <PrimaryHeading>Featured Node JS Application</PrimaryHeading>
        <SecondaryHeading>
          Over the years, Intuz has successfully delivered hundreds of apps
          developed with NodeJS. Here is a glimpse of what we can develop using
          NodeJS.
        </SecondaryHeading>
        <div className={style.content}>
          <div>
            <ImageBlock
              mwidth={525}
              dwidth={725}
              src={cdn("nodejs-featured-img.png")}
              alt="nodejs-featured-img"
              format="png"
            />
          </div>
          <div className={style.text}>
            <Image
              layout="intrinsic"
              src="/static/Images/icons/nodejs-featured-logo.png"
              alt="nodejs-featured-logo"
              height="126"
              width="295"
            />
            <p>
              FSInsight is a comprehensive financial management solution
              designed for a NBFC to streamline routine loan management, client
              management and money management processes.
            </p>
          </div>
        </div>
      </Container>
    </section>
  );
};

export default SectionAppFeature;
