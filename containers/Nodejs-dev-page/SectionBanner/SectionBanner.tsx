//@ts-nocheck
import React from 'react';
import Container from '../../../components/Container/Container';
import LinkButton from '../../../components/UI/LinkButton/LinkButton';
import { cdn } from '../../../config/cdn';
import style from './styles.module.scss';

const SectionBanner = (props) => {
    let imageFile = props.content.backImage.hash + '.jpg?auto=format';
    let backImage = `url(${cdn(imageFile)}) center center/cover no-repeat`;
    
    return(
        <section className={`${style.PageBanner} ${style[props.variation]}`} >
            <Container>
                {props.content.description ? <p className={style.desc}>{props.content.description}</p> : ''}
                {props.content.title ? <h1>{props.content.title}</h1> : ''}
                {props.content.subTitle ? <span>{props.content.subTitle}</span> : ''}
                {props.content.buttonLabel && <LinkButton href={props.content.buttonLink} variation='bannerBtn' style={{marginTop: '2rem'}}>{props.content.buttonLabel}</LinkButton>}
            </Container>
            <style jsx>
                {`
                    section{
                        background: ${backImage};
                    }
                    @media only screen and (max-width: 990px){
                        section{
                        background: ${backImage};
                        }

                    } 
                    @media only screen and (max-width: 767px){
                        section{
                        background: ${backImage};
                        }

                    } 
                `}
            </style>
        </section>
    )
}

export default SectionBanner;