//@ts-nocheck

import React from "react";
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import TaxiApp from '../../../components/TaxiAppForm/TaxiApp';
import style from '../styles.module.scss';

const SectionForm = () => {
    return (
        <section className={style.FormSection} id="formSection">
            <Container>
                <div className='text-center'>
                    <PrimaryHeading>NEED HELP WITH YOUR TAXI BOOKING APP DEVELOPMENT?</PrimaryHeading>
                    <SecondaryHeading>Please fill in the form below to schedule a call or request an estimate.</SecondaryHeading>
                </div>
                <TaxiApp />
            </Container>

        </section>
    )
}

export default SectionForm;

