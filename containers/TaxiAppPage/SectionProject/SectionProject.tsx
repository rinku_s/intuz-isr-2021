//@ts-nocheck



import gsap from 'gsap';
import React from "react";
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import Project from '../../../components/Project/Project';
import style from '../styles.module.scss';

const SectionProject = (props) => {

    // setOpacity(`.PriHeading, .SecHeading`)

    function play() {
        const tl = gsap.timeline();
        tl.fromTo(`.PriHeading`, 1, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' })
            .fromTo(`.SecHeading`, 1, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' }, 0.5);

    }

    return (

        <section className={style.ProjectSection} id="projects">
            <Container>
                <div className='text-center'>
                    <PrimaryHeading className="PriHeading">{props.title}</PrimaryHeading>
                    <SecondaryHeading className="SecHeading">{props.subtitle}</SecondaryHeading>
                </div>
            </Container>
            <Project sectionrecentworks={props.sectionrecentworks} />
        </section>
        // </WaypointOnce >
    )
}

export default SectionProject;

