//@ts-nocheck



import gsap from "gsap";
import React from "react";
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import ImageBlock from '../../../components/ImageBlock/ImageBlock';
import { cdn } from '../../../config/cdn';
import style from '../styles.module.scss';


const SectionCustomApp = (props) => {

    // setOpacity("#customApp h2");

    function play() {
        let tl = new gsap.timeline({ delay: 0.5 });
        tl.fromTo(`#customApp h2`, 1, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: "Power0.easeIn" });
        tl.play();
    }

    return (


        <section className={style.CustomAppSection} id="customApp" style={{ paddingBottom: "8rem" }}>
            <Container className={style.FullPage} >
                <div className='text-center'>
                    <PrimaryHeading>{props.content.title}</PrimaryHeading>
                    <ImageBlock className="img-fluid" src={cdn(props.content.image.hash + '.png')} format="png" dwidth={1143} mwidth={643} alt="App Screen" />
                </div>
            </Container>
        </section>
        // </WaypointOnce >
    )
}

export default SectionCustomApp;

