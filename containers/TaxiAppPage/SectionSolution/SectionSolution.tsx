//@ts-nocheck



import gsap from "gsap";
import React from "react";
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import SolutionTab from '../../../components/SolutionTab/SolutionTab';
import style from '../styles.module.scss';

const SectionSolution = (props) => {
    let backColor = (props.backColor) ? { background: `${props.backColor}` } : null;

    // setOpacity(`#solution h2, #solution p`);

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(`#solution h2`, 1, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' })
            .fromTo(`#solution p`, 0.6, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' }, 0.4);
    }

    return (
        // 

        <section className={style.SolutionSection} id="solution" style={backColor}>
            <Container className={style.FullPage}>
                <div className='text-center'>
                    <PrimaryHeading>{props.title}</PrimaryHeading>
                    <SecondaryHeading>
                        {props.subtitle}
                    </SecondaryHeading>
                </div>
                {props.tabTitles.length > 0 ? <SolutionTab tabTitles={props.tabTitles} /> : ''}
            </Container>
        </section>
        // 
    )
}

export default SectionSolution;

