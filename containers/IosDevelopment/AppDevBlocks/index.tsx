import React from "react";
import AppDevBlock from "./AppDevBlock";
import { cdn } from "../../../config/cdn";
import PrimaryHeading from "components/Heading/Heading";
import SecondaryHeading from "components/Heading/SecondaryHeading";
import Container from "components/Container/Container";
const data = [
  {
    src: cdn("portfolio_a9d328c8c5.svg"),
    title: "Portfolio Versatility",
    desc: "Hundreds of applications developed and launched across verticals for different business purposes.  ",
  },
  {
    src: cdn("support_612a2e5cc0.svg"),
    title: "Constant Support",
    desc: "With you at each step — from app conceptualization and design to development and deployment.",
  },
  {
    src: cdn("flexiblerate_6cfd74cf55.svg"),
    title: "Flexible Pricing",
    desc: "Whether you have a long term goal or simply want to enhance an existing app, we are cost-effective.",
  },
];

const AppDevBlocks = ({ title, description }) => {
  return (
    <section style={{ padding: "8rem 0" }}>
      <Container>
        <PrimaryHeading>{title}</PrimaryHeading>
        <SecondaryHeading style={{ color: "#6B6B6B", fontWeight: 300 }}>
          {description}
        </SecondaryHeading>
        <div className="row justify-center" style={{ marginTop: "5rem" }}>
          {data.map((d, i) => (
            <div key={i} className="col-12 col-md-6 col-lg-4">
              <AppDevBlock key={i} {...d} />
            </div>
          ))}
        </div>
      </Container>
    </section>
  );
};

export default AppDevBlocks;
