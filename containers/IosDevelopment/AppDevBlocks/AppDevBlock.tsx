import { myLoader } from "config/image-loader";
import Image from "next/image";
import React from "react";
import classes from "./styles.module.scss";
const AppDevBlock = ({ src, title, desc }) => {
  return (
    <div className={`${classes.AppDevBlock} text-center mb-5 mb-md-0`}>
      {/* <img className="lazyload" data-src={src} alt={title} /> */}
      <Image layout='fixed' height={60} width={60} loader={myLoader} src={src.split("https://intuz-site.imgix.net/uploads/")[1]} alt={title} />

      <h3>{title}</h3>
      <p>{desc}</p>
    </div>
  );
};

export default AppDevBlock;
