import { myLoader } from "config/image-loader";
import Image from "next/image";
import React from "react";
import classes from "./styles.module.scss";
const AppDevBlock = ({ src, title, desc }) => {
  return (
    <div className={`${classes.IosAppDevSection} text-center p-2 m-2`}>

      {/*<img className="lazyload img-fluid" data-src={src} alt={title} />*/}
      <Image layout={'intrinsic'} width={285} height={260} loader={myLoader} src={src.split("https://intuz-site.imgix.net/uploads/")[1]} alt={title} />

      <h3>{title}</h3>
      <p>{desc}</p>
    </div>
  );
};

export default AppDevBlock;
