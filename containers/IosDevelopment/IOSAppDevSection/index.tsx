import Container from "components/Container/Container";
import SectionHeading from "components/Utility/SectionHeading";
import SubHeading from "components/Utility/SubHeading";
import React from "react";
import { cdn } from "../../../config/cdn";
import AppDevBlock from "./AppDevBlock";
import classes from "./styles.module.scss";

const data = [
  {
    src: cdn("customerdevelopment_4818e8b4bc.svg"),
    title: "Custom iOS app development",
    desc:
      "Let our iPhone app developers develop you next-gen iOS apps for you with advanced technologies and stack, Objective-C and Swift.",
  },
  {
    src: cdn("iosintegration_432ecb5144.svg"),
    title: "iOS integration",
    desc:
      "We will help you enhance your existing iOS apps' functionality by integrating them with the latest technologies and other software.",
  },
  {
    src: cdn("appconstruction_b6bc8557d6.svg"),
    title: "iOS app consultation",
    desc:
      "If you have a rough app idea about what you want to achieve through your iOS app, come to our iPhone app development company. They will understand your requirements and suggest the next step accordingly.",
  },
  {
    src: cdn("on_demand_ios_c6c18d694b.svg"),
    title: "On-demand iOS developers",
    desc:
      "When it comes to iOS application development, Intuz stands apart. You want us to build an app from scratch? No problem! We take your idea and transform it into a cutting-edge app — within budget.",
  },
];

const IOSAppDevSection = ({ title, description }) => {
  return (
    <section style={{ padding: "8rem 0" }}>
      <Container>
        <SectionHeading className='text-center z-10'>{title}</SectionHeading>
        <SubHeading className='font-light mx-auto text-center max-w-80'

        >
          {description}
        </SubHeading>
      </Container>
      <div
        className={`row justify-content-between ${classes.AppDevBlocksRow}`}
      // style={{ marginTop: "5rem", padding: "0 5rem" }}
      >
        {data.map((d, i) => (
          <div key={i} className="col-12 col-lg-6 col-xl-3 p-md-0">
            <AppDevBlock key={i} {...d} />
          </div>
        ))}
      </div>
    </section>
  );
};
export default IOSAppDevSection;
