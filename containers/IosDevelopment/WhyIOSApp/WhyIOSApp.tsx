import React from "react";

import PrimaryHeading from "components/Heading/Heading";
import Container from "../../../components/Container/Container";
import classes from "./styles.module.scss";
function WhyIOSApp() {
  return (
    <section className={classes.WhyIOSApp}>
      <Container>
        <div className="row">
          <div className="text-left col-md-6 col-sm-12">
            <PrimaryHeading className=" text-left">
              Why iOS application development?
            </PrimaryHeading>
            <p data-para className="my-2 md:text-left mr-4">
              There are five good reasons why you must launch an iOS app:
            </p>
          </div>
          <div className="col-md-6 col-sm-12 text-left">
            <p data-para className="my-2 py-2 p-md-5 pt-md-0 mt-md-0">
              iPhone apps have a more significant ROI than Android apps, which
              means greater revenues.
            </p>
            <p data-para className="my-2 py-2 p-md-5">
              Build more scalable iOS apps due to low fragmentation and ease of
              testing.
            </p>
            <p data-para className="my-2 py-2 p-md-5">
              Keep your app data safe through stringent security measures by
              Apple.
            </p>
            <p data-para className="my-2 py-2 p-md-5">
              Ready an iOS app in nearly 28% less time than Android — save
              costs!
            </p>
            <p data-para className="my-2 py-2 p-md-5">
              Enthrall your customers with a fantastic app experience and
              scintillating UX.
            </p>
          </div>
        </div>
      </Container>
    </section>
  );
}

export default WhyIOSApp;
