import { myLoader } from "config/image-loader";
import SectionDetailText from "containers/IosDevelopment/SectionDetailText/SectionDetailText";
import Image from "next/image";
import React from "react";
import Container from "../../../components/Container/Container";
import classes from "./styles.module.scss";
const data = [
  {
    src: "iphone_f7ee4d4cbd.svg",
    title: "iPhone",
    description:
      "Our iPhone app developers have only one job — to build you an iPhone app as per your business requirements and within your budget and decided timescales.",
  },
  {
    src: "ipad_bbf2ddddd7.svg",
    title: "iPad",
    description:
      "Build high-performance apps for iPad users with the help of Intuz. Our developers have a deep understanding of native iPad app development requirements.",
  },
  {
    src: "watch_cc04e6b6de.svg",
    title: "Apple Watch",
    description:
      "Tap the potential of wearables technology and deliver a deeper user engagement by building unique Apple watch apps with our dedicated support.",
  },
  {
    src: "tv_c938e4799b.svg",
    title: "Apple TV",
    description:
      " We love developing scalable tvOS apps that ensure your audience always receives high-quality AV content with minimal latency and enjoys an optimal experience, always.",
  },
];

function AllDevice() {
  return (
    <section className={classes.AllDevice} style={{ padding: "10rem 0" }}>
      <Container>
        <SectionDetailText
          title="iOS development for all Apple devices"
          description="Let us help you establish your foothold across all Apple platforms.
            Why stick to just iPhones when you can conquer all iOS devices?
            "
          variation="topPadding"
          className="mt-5 pt-5"
        />

        <div className="row justify-content-between">
          {data.map((d, i) => (
            <div key={i} className="col-sm-6 row my-5">
              <div className="col-sm-12 col-md-2">
                {/*} <img src={d.src} className="smalllogo card-img" alt={d.title} />*/}
                <Image src={d.src} loader={myLoader} layout={'fixed'} height={80} width={100} />
              </div>
              <div className="col-sm-12 col-md-10">
                {/* <b> */}
                <h3 className="card-title text-center md:text-left mt-3 mt-md-0">
                  <>{d.title}</>
                </h3>
                {/* </b> */}
                <p
                  data-para
                  className="m-2 text-center md:text-left mt-3 mt-md-0"
                >
                  {d.description}
                </p>
              </div>
            </div>
          ))}
        </div>
      </Container>
    </section>
  );
}

export default AllDevice;
