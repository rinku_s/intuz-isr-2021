import React from "react";
import Container from "../../../components/Container/Container";
import PrimaryHeading from "../../../components/Heading/Heading";
import SecondaryHeading from "../../../components/Heading/SecondaryHeading";
import style from "./styles.module.scss";

const SectionDetailText = (props) => {
  let variation = props.variation;
  let backColor = props.backColor ? { background: `${props.backColor}` } : null;
  return (
    <section
      className={`${style.DetailTextSection} ${style[variation]}`}
      style={backColor}
    >
      <Container>
        {props.title ? <PrimaryHeading>{props.title}</PrimaryHeading> : ""}
        {props.description ? (
          <SecondaryHeading>{props.description}</SecondaryHeading>
        ) : (
          ""
        )}
        {props.children}
      </Container>
    </section>
  );
};

export default SectionDetailText;
