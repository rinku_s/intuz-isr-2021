import SectionHeading from "components/Utility/SectionHeading";
import SubHeading from "components/Utility/SubHeading";
import React from "react";
import CaseStudyDevProcess from "../../../components/CaseStudyDevProcess";
import Container from "../../../components/Container/Container";
const Slider = () => {
  const descriptions = {
    current: '',
    discovery:
      "Our iPhone developers hold a series of preliminary to understand your business vision and outline the project scope, including technicalities, procurement details, cost estimations, and timescales.",
    design:
      "This involves building a design sketch that describes the app's feel n' flow. We design a variety of wireframes and then use established software architecture patterns to construct a prototype.",
    development:
      "Once you approve, our iPhone app developers follow the best development practices to write the application code. The output of this phase is testable and functional software.",
    maintenance:
      "We believe in quality, which is why this step is so important to us. We check for the application's code quality, security, overall performance, and integration support. We test until we reach perfection!",
  };


  const process = [
    {
      title: "Discovery",
      icon: {
        url: "discovery_fab0babdcf.svg",
      },
    },
    {
      title: "Design",
      icon: {
        url: "case_designing_737ec3b92d.svg",
      },
    },
    {
      title: "Development",
      icon: {
        url: "case_development_a4bbdcffff.svg",
      },
    },
    {
      title: "Maintenance",
      icon: {
        url: "carbon_license_maintenance_c17ad59065.svg",
      },
    },
  ];

  return (
    <section style={{ padding: "10rem 0" }}>
      <Container>
        <SectionHeading className='text-center'>iOS app development process</SectionHeading>
        <SubHeading className='text-center'>
          We love building apps with the right mix of tech and experience in
          phases.
        </SubHeading>
        <CaseStudyDevProcess
          dark
          process={process}
          descriptions={descriptions}
          className="text-dark"
        />
      </Container>
    </section>
  );
};

export default Slider;
