import React from 'react'
import Container from '../../../components/Container/Container'
import PrimaryHeading from '../../../components/Heading/Heading'
import SecondaryHeading from '../../../components/Heading/SecondaryHeading'
import Resources from '../../../components/Resources/Resources'

const IOSResourceSection = (props) => {
    return (
        <section style={{margin:"5rem 0"}}>
            <Container>
                <PrimaryHeading>Intuz Resources</PrimaryHeading>
                <SecondaryHeading>Insights on latest technology trends, enterprise mobility solutions, & company updates</SecondaryHeading>
                <Resources  sectionresources={props.sectionresources}/>
            </Container>
        </section>
    )
}

export default IOSResourceSection
