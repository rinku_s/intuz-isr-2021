import React from "react";

function IOSDevService() {
  return (
    <section className="m-5 p-5">
      <div className="row">
        <div className="col-lg col-sm-12">
          <div className="card">
            <div className={'text-center'}>
              <img
                src={'customerdevelopment'}
                className="photo card-img"
                alt="..."
              />
            </div>
            <div className="card-body text-center">
              <p className="myhead card-title text-center">
                <b>
                  <strong>Custom iOS app development </strong>
                </b>
              </p>
              <p className="text-wrap">
                Let our iPhone app developers develop you next-gen iOS apps for
                you with advanced technologies and stack, Objective-C and Swift.
              </p>
            </div>
          </div>
        </div>
        <div className="col-lg col-sm-12">
          <div className="card">
            <div className={'text-center'}>
              <img
                src={'iosintegration'}
                className="photo card-img-top"
                alt="..."
              />
            </div>

            <div className="card-body text-center">
              <p className="card-title text-center">
                <b>
                  <strong>iOS integration</strong>
                </b>{" "}
              </p>
              <p className="text-wrap">
                We will help you enhance your existing iOS apps' functionality
                by integrating them with the latest technologies and other
                software.
              </p>
            </div>
          </div>
        </div>
        <div className="col-lg col-sm-12">
          <div className="card">
            <div className={'text-center'}>
              <img
                src={'appconstruction'}
                className="photo card-img-top"
                alt="..."
              />
            </div>

            <div className="card-body text-center">
              <p className="card-title text-center">
                <b>
                  <strong>iOS app consultation</strong>
                </b>
              </p>
              <p className="text-wrap">
                If you have a rough app idea about what you want to achieve
                through your iOS app, come to our iPhone app development
                company. They will understand your requirements and suggest the
                next step accordingly.
              </p>
            </div>
          </div>
        </div>
        <div className="col-lg col-sm-12">
          <div className="card">
            <div className={'text-center'}>
              <img
                src={'ondemandchanges'}
                className="photo card-img-top"
                alt="..."
              />
            </div>

            <div className="card-body">
              <p className="card-title text-center">
                <b>
                  <strong> On-demand iOS developers</strong>
                </b>
              </p>
              <p className="text-wrap">
                When it comes to iOS application development, Intuz stands
                apart. You want us to build an app from scratch? No problem! We
                take your idea and transform it into a cutting-edge app — within
                budget.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
}

export default IOSDevService;
