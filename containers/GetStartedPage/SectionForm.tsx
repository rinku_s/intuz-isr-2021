import HeadAndSub from "components/Utility/HeadAndSub";
import React from "react";
import Container from "../../components/Container/Container";
import FormContainer from "../../components/GetStartedForm/FormContainer";
import PrimaryHeading from "../../components/Heading/MainHeading";
import SecondaryHeading from "../../components/Heading/SecondaryHeading";
import styles from "./sectionform.module.scss";

const SectionForm = (props) => {
  return (
    <section className="my-52">
      <Container>
        <div className="text-center">
          <HeadAndSub
            heading="Tell us a brief about your project"
            subHeading="We're eager to work with you to make your vision come to life."
          />
        </div>
        <FormContainer />
      </Container>
    </section>
  );
};

export default SectionForm;
