//@ts-nocheck
import React from "react";
import Container from "../../components/Container/Container";
import Block from "../../components/SingleBlogBlock/Block";
import style from "./styles.module.scss";
const BlogFilterSection = (props) => {
  return (
    <section className={`${style.BlogFilter}`}>
      <Container className="grid grid-cols-2 sm:grid-cols-3 gap-10">
        {props?.blogs?.map((blog, index) => (
          <div key={index}>
            <Block
              {...blog}
              className="filterBlog"
              filter={true}
              variation={props.blogAuthor ? "hideAuthor" : "hideCategory"}
            />
          </div>
        ))}
      </Container>
    </section>
  );
};

export default BlogFilterSection;
