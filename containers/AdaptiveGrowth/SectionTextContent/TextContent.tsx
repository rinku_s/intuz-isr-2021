//@ts-nocheck

import React from "react";
import LazyLoad from 'react-lazyload';
import { cdn } from '../../../config/cdn';
import style from './styles.module.scss';

const TextContent = (props) => {
    return(
        <LazyLoad height={300} offset={300} once>
            <p className={style.TextContent}>{props.children}<br />{props.image ? <img src={cdn(props.image)} alt={props.image} />: ''}</p>
        </LazyLoad>
    )
}

export default TextContent;

