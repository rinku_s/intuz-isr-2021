//@ts-nocheck

import React from 'react';
import Container from '../../../components/Container/Container';
import Expectation from '../../../components/Expectation/Expectation';
import PrimaryHeading from '../../../components/Heading/Heading';
import style from './styles.module.scss';

const ExpectAdaptiveGrowth = (props) => {
    if(props.children.length <= 0 ){
        return null
    } else {
        return(
            <section className={`${style.ExpectAdaptiveGrowth}`}>
                <Container>
                    <PrimaryHeading>What to Expect from Our Adaptive Growth?</PrimaryHeading>
                    <div className = 'row'>
                        {props.children.map((expectation, index) => {
                            return (
                                <Expectation content = {expectation} key={index} />
                            )
                        })}
                    </div>
                </Container>
            </section>
        )
    }
}

export default ExpectAdaptiveGrowth