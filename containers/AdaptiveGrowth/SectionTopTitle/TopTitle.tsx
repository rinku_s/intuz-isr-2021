//@ts-nocheck


import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import style from './styles.module.scss';

const TopTitle = (props) => {
    const variation = props.variation ? props.variation : '' ;
    return (
        <section className={`${style.sectionTitle} ${style[variation]}`}>
            <Container>
                {props.title ? <PrimaryHeading>{props.title}</PrimaryHeading> : ''}
                {props.children}
            </Container>
        </section>
    )
}

export default TopTitle;