//@ts-nocheck
import React from 'react';
import Container from '../../../components/Container/Container';
import Branch from '../../../components/PartnerBranch/Branch';
import { cdn } from '../../../config/cdn';
import windowWidth from '../../../hooks/windowWidth';
import style from './styles.module.scss';

const SectionBranch = (props) => {
    let windowSize = windowWidth();
    let backImage = { background : `url(${cdn(`branch_back.jpg?fm=pjpg&auto=format${windowSize < 768 ? '&w=767' : ''}`)}) center center no-repeat`}

    return (
        <section className={style.Branch} style={backImage}>
            <Container>
                <Branch />
            </Container>
        </section>
    )
}

export default SectionBranch
