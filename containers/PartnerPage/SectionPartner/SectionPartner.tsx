//@ts-nocheck
import React from 'react';
import Container from '../../../components/Container/Container';
import PartnerBlock from '../../../components/Partner-with-us/PartnerBlock';
import style from './styles.module.scss';

const SectionPartner = (props) => {
    return (
        <section className={style.Partner}>
           <Container>
               <PartnerBlock />
            </Container>
        </section>
        )
}

export default SectionPartner
