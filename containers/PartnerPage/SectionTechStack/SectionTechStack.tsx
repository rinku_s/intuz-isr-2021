//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import TechStack from '../../../components/TechStack/TechStack';
import style from './styles.module.scss';

const SectionTechStack = () => {

    // useEffect(() => gsap.set('.techStack', {opacity:0}))

    // setOpacity(`.techStack`);

    function play() {
        const tl = gsap.timeline();
        tl.fromTo('.techStack', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" });
        tl.play();
    }

    return (
        <section className={style.Techstack}>
            <Container>
                {/*  */}
                <div className={`${style.Heading} techStack`}>
                    <PrimaryHeading>Our Technology Expertise</PrimaryHeading>
                    <SecondaryHeading>Specialized in emerging technologies and tools to develop cutting-edge business solutions</SecondaryHeading>
                </div>
                {/*  */}
                <TechStack />
            </Container>
        </section>
    )
}

export default SectionTechStack;