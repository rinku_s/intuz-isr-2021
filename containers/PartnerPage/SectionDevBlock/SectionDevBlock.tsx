//@ts-nocheck

import gsap from 'gsap';
import React from 'react';
import Container from '../../../components/Container/Container';
import DevBlock from '../../../components/PartnerDevBlock/DevBlock';


const SectionDevBlock = (props) => {
    // setOpacity(`.DevBlock`);

    function play() {
        const tl = gsap.timeline();
        tl.fromTo('.DevBlock', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5);
        tl.play();
    }

    return (

        <section id="partnerblock">
            <Container>
                <DevBlock className="DevBlock" content={props.content} />
            </Container>
            <style jsx>
                {
                    `
                    section {
                        padding : 8rem 0;
                    }
                    
                    `
                }
            </style>
        </section>
        // </WaypointOnce >
    )
}

export default SectionDevBlock;