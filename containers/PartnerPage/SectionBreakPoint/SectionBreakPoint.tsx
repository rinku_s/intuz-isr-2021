//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import LinkButton from '../../../components/UI/LinkButton/LinkButton';
import { cdn } from '../../../config/cdn';
import styles from './sectionbreakpoint.module.scss';

const SectionBreakPoint = (props) => {
    if (props.content.length <= 0) {
        return null;
    } else {

        // setOpacity('.SecondaryHeading');
        function play() {
            const tl = gsap.timeline();
            tl.fromTo('.SecondaryHeading', 1, { y: -20, opacity: 0 }, { y: 0, opacity: 1, ease: "power0.ease" });

        }

        return (
            <>
                {props.content.map((section, index) => {
                    var backStyle = { background: `url(${cdn(section.backImage.name)}) center center no-repeat` }
                    return (
                        <section key={index}>
                            <div className={styles.BreakPoints} style={backStyle}>

                                <SecondaryHeading fontSize='3.5rem' className="SecondaryHeading">{section.title}</SecondaryHeading>

                                {section.buttonLabel ?
                                    <LinkButton href={section.buttonLink} variation="blackBorder">{section.buttonLabel}</LinkButton>
                                    : ''}
                            </div>
                        </section>
                    );
                })
                }
            </>
        )
    }
}
export default SectionBreakPoint


