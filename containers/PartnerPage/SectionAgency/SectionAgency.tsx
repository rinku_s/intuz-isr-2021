//@ts-nocheck

import gsap from 'gsap';
import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import Agency from '../../../components/PartnerAgency/Agency';
import style from './styles.module.scss';

const SectionAgency = () => {
    // setOpacity(`.AgencyHeading`);

    function play() {
        const tl = gsap.timeline();
        tl.fromTo('.AgencyHeading', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5);
        tl.play();
    }

    return (
        <section className={style.AgencyBlock}>
            <Container>

                <PrimaryHeading className="AgencyHeading">Intuz is best suited for agencies looking for</PrimaryHeading>

                <Agency />
            </Container>
        </section >
    )
}
export default SectionAgency;