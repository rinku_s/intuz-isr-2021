//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import ServiceBlock from '../../../components/PartnerService/ServiceBlock';
import { cdn } from '../../../config/cdn';
import style from './styles.module.scss';

const SectionService = (props) => {

    // setOpacity(`.ServiceHeading, .ServiceSecond, .leftBlock, .rightBlock`);

    function play() {
        const tl = gsap.timeline();
        tl.fromTo('.ServiceHeading', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5)
            .fromTo('.ServiceSecond', 1, { y: -20, opacity: 0, ease: "power0.ease" }, { y: 0, opacity: 1, ease: "power0.ease" }, 0.5)
            .fromTo('.leftBlock', 1, { x: -20, opacity: 0, ease: "power0.ease" }, { x: 0, opacity: 1, ease: "power0.ease" }, 0.5)
            .fromTo('.rightBlock', 1, { x: 20, opacity: 0, ease: "power0.ease" }, { x: 0, opacity: 1, ease: "power0.ease" }, 0.5);

        tl.play();
    }

    if (props.content.length <= 0) {
        return null;
    } else {
        return (
            <section className={style.ServiceBlock}>
                <Container>
                    <div className={style.Heading}>

                        <PrimaryHeading className="ServiceHeading">Services</PrimaryHeading>

                        <SecondaryHeading className="ServiceSecond">We at Intuz are always passionate about what we can produce with the technologies and tools to enable disruptive innovations</SecondaryHeading>
                    </div>
                </Container>
                <div>
                    <div className={`row ${style.ServiceRow}`}>
                        {props.content.map((service, index) => {
                            return (
                                <ServiceBlock
                                    className={(index % 2 ? 'rightBlock' : 'leftBlock')}
                                    title={service.title}
                                    subTitle={service.description}
                                    backImage={cdn(service.backImage.name)}
                                    alt={service.backImage.name}
                                    key={index}
                                />
                            )
                        })}
                    </div>
                </div>
            </section >
        )
    }
}

export default SectionService;