//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import FeaturedApEducation from '../../../components/FeaturedAppEducation/FeaturedApEducation';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';

const FeaturedSection = (props) => {

    // setOpacity(`.ftsec h2, .ftsec p`)


    function play() {
        const tl = gsap.timeline();
        tl.fromTo(`.ftsec h2`, 1, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' })
            .fromTo(`.ftsec p`, 1, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, ease: 'Power0.easeIn' }, 0.5);
    }

    return (

        <section className={`${props.className ? props.className : ''} ftsec`}>
            <PrimaryHeading>
                Featured Apps
                </PrimaryHeading>
            <SecondaryHeading>
                Some of the recent education and training mobile app solutions
                </SecondaryHeading>
            <FeaturedApEducation />
        </section>
        // </WaypointOnce >
    )
}

export default FeaturedSection
