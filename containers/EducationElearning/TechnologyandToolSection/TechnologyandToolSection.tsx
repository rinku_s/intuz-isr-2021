//@ts-nocheck
import React from 'react'
import Container from '../../../components/Container/Container';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import classes from './styles.module.scss'
import Technologies from '../../../components/EducationComponent/Technologies';
const TechnologyandToolSection = () => {
    return (
        <section className={classes.TechnologyandToolSection}>
            <Container>
                <PrimaryHeading>
                    Technology & Tool We Use
                </PrimaryHeading>
                <SecondaryHeading fontSize="2.4rem">
                Blend of modern tools & Technologies to build next-generation educational app solutions
                </SecondaryHeading>
                <Technologies/>
            </Container>
        </section>
    )
}

export default TechnologyandToolSection
