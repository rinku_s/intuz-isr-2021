//@ts-nocheck
import gsap from 'gsap';
import React from 'react';
import Container from '../../../components/Container/Container';
import ElearningSolutions from '../../../components/EducationComponent/ElearningSolutions/ElearningSolutions';
import PrimaryHeading from '../../../components/Heading/Heading';
import SecondaryHeading from '../../../components/Heading/SecondaryHeading';
import classes from './styles.module.scss';
const ElearningSolutionsSection = (props) => {

    // setOpacity(`.${classes.Elearning} .head1 , .${classes.Elearning} .head2 `, { opacity:0 });

    function play() {
        let tl = gsap.timeline();
        tl.fromTo(`.${classes.Elearning} .head1 , .${classes.Elearning} .head2 `, 1, { y: -20, autoAlpha: 0 }, { y: 0, autoAlpha: 1, stagger: 0.3, ease: 'Power0.easeIn' })
    }

    return (


        <section className={classes.Elearning}>
            <Container>
                <PrimaryHeading className="head1">
                    Education & E-Learning Development Solutions
                </PrimaryHeading>
                <SecondaryHeading fontSize="2.4rem" className="head2">
                    Bringing years of experience & latest technologies for crafting bespoke e-learning solutions
                </SecondaryHeading>
                <ElearningSolutions elearningsolutions={props.elearningsolutions} />
            </Container>
        </section>
        // </WaypointOnce >
    )
}

export default ElearningSolutionsSection
