//@ts-nocheck

import gsap from 'gsap';
import React, { Component } from 'react';
import Container from '../../../components/Container/Container';
import FormPopup from '../../../components/GetStartedForm/FormPopup';
import PrimaryHeading from '../../../components/Heading/Heading';
import InfoTooltip from '../../../components/InfoTooltip/InfoTooltip';
import style from './styles.module.scss';

export default class FeatureBlock extends Component {
    constructor(props) {
        super(props)
    }
    state = {
        show: false,
        currentPackage: null,
        currentPackageAmount: null
    }

    handleClose = () => {
        this.setState({ show: false });
    }

    handleShow = (name, amount) => {
        this.setState({
            show: true,
            currentPackage: name,
            currentPackageAmount: amount
        })
    }
    gotoSection = (link) => {
        return (gsap.to(window, 0.1, { scrollTo: { y: link, autoKill: false } }))
    }
    render() {
        const Open_plan = <img src={"/static/Images/icons/plan_open.svg"} alt='plan_open' />;
        const Close_plan = <img src={"/static/Images/icons/plan_close.svg"} alt='plan_open' />;
        return (
            <section className={style.FeatureBlock}>
                <Container>
                    <PrimaryHeading>Our Packages</PrimaryHeading>
                    <div className="table-responsive">
                        <table className='table'>
                            <thead>
                                <tr>
                                    <th></th>
                                    <th><div className={style.discovery}>DISCOVERY <span>$1799 </span></div></th>
                                    <th><div className={style.engagement}>ENGAGEMENT <span> $3899</span></div></th>
                                    <th><div className={style.growth}>GROWTH <span> $6399 </span></div></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colSpan={4}>
                                        <h4>App Discovery</h4>
                                        <InfoTooltip>
                                            Boost your app's visibility and build a solid presence for your app with our ASO, promotional website and video creation services to get in front of your targeted users.
                                        </InfoTooltip>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a onClick={() => this.gotoSection('#appdetail_0')} title='ASO'>ASO</a></td>
                                    <td>{Open_plan}</td>
                                    <td>{Open_plan}</td>
                                    <td>{Open_plan}</td>
                                </tr>
                                <tr>
                                    <td><a onClick={() => this.gotoSection('#appdetail_1')} title='Promotional Website'>Promotional Website</a></td>
                                    <td>{Open_plan}</td>
                                    <td>{Open_plan}</td>
                                    <td>{Open_plan}</td>
                                </tr>
                                <tr>
                                    <td><a onClick={() => this.gotoSection('#appdetail_1')} title='Promotional Video'>Promotional Video</a></td>
                                    <td>{Open_plan}</td>
                                    <td>{Open_plan}</td>
                                    <td>{Open_plan}</td>
                                </tr>
                                <tr>
                                    <td><a onClick={() => this.gotoSection('#appdetail_1')} title='Non-promotional Video'>Non-promotional Video</a></td>
                                    <td>{Open_plan}</td>
                                    <td>{Open_plan}</td>
                                    <td>{Open_plan}</td>
                                </tr>
                                <tr>
                                    <td colSpan={4}>
                                        <h4>Engagement</h4>
                                        <InfoTooltip>
                                            Getting engagement is a very crucial part of any successful app. We’ll get your app reviewed by top editors and also run CPI campaigns to give you genuine app installs & boost your app visibility.
                                        </InfoTooltip>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a onClick={() => this.gotoSection('#appdetail_4')} title='Editorial App Review'>Editorial App Review</a></td>
                                    <td>{Close_plan}</td>
                                    <td>{Open_plan}</td>
                                    <td>{Open_plan}</td>
                                </tr>
                                <tr>
                                    <td><a title='Guaranteed App Installs' onClick={() => this.gotoSection('#appdetail_3')} >Guaranteed App Installs <span>(250 Non-Incent installs for each Android & IOS)</span></a></td>
                                    <td>{Close_plan}</td>
                                    <td>{Open_plan}</td>
                                    <td>{Open_plan}</td>
                                </tr>
                                <tr>
                                    <td colSpan={4}><h4>Growth</h4>
                                        <InfoTooltip>
                                            In the era of internet social media is the key to successful marketing. So, We’ll run successful social media, social bookmarks, and PR distribution campaigns to get you found everywhere.
                                        </InfoTooltip>
                                    </td>
                                </tr>
                                <tr>
                                    <td><a onClick={() => this.gotoSection('#appdetail_6')} title='Social Media Marketing'>Social Media Marketing</a></td>
                                    <td>{Close_plan}</td>
                                    <td>{Close_plan}</td>
                                    <td>{Open_plan}</td>
                                </tr>
                                <tr>
                                    <td><a onClick={() => this.gotoSection('#appdetail_6')} title='Social Bookmarking'>Social Bookmarking</a></td>
                                    <td>{Close_plan}</td>
                                    <td>{Close_plan}</td>
                                    <td>{Open_plan}</td>
                                </tr>
                                <tr>
                                    <td><a onClick={() => this.gotoSection('#appdetail_5')} title='PR Distribution'>PR Distribution</a></td>
                                    <td>{Close_plan}</td>
                                    <td>{Close_plan}</td>
                                    <td>{Open_plan}</td>
                                </tr>
                                <tr className={style.optional}>
                                    <td>
                                        <label>Optional</label><br />
                                        <input type="checkbox" name="localization" value="localization" title='App Localization' />App Localization<br />
                                        <input type="checkbox" name="ppc" value="ppc" />Paid Advertising (PPC)<br />
                                    </td>
                                    <td><div><a className={style.modellink} onClick={() => this.handleShow('Discovery', '$1799')}>Select Package</a></div></td>
                                    <td><div><a className={style.modellink} onClick={() => this.handleShow('Engagement', '$3899')}>Select Package</a></div></td>
                                    <td><div><a className={style.modellink} onClick={() => this.handleShow('Growth', '$6399')}>Select Package</a></div></td>
                                </tr>
                                <tr>
                                    <td colSpan={4}><p className={style.contact}>* <a href='/get-started' target='blank' className={style.contact}>Contact us </a>for custom packages</p></td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                    <FormPopup
                        thankyoupage="thankyou-package"
                        subject="App Marketing Package Request"
                        leadsource="App Marketing Packages"
                        show={this.state.show}
                        onHide={this.handleClose}
                        currentPackage={this.state.currentPackage}
                        currentPackageAmount={this.state.currentPackageAmount}
                        title={`${this.state.currentPackage} - Package`}
                        label={`Selected Package: <span> ${this.state.currentPackage} - ${this.state.currentPackageAmount} </span>`}
                    />
                </Container>
            </section>
        )
    }
}