//@ts-nocheck
import React from 'react';
import Container from '../../../components/Container/Container';
import style from './styles.module.scss';

const SectionPriceBlock = (props) => {
    return(
        <section className={style.PriceBlock}>
            <Container>
                {props.children}
            </Container>
        </section>
    )
}

export default SectionPriceBlock