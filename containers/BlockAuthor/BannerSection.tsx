//@ts-nocheck

import React from 'react';
import SVG from 'react-inlinesvg';
import BlogBreadcrumb from '../../components/BlogBreadcrumb/BlogBreadcrumb';
import Container from '../../components/Container/Container';
import { cdn } from '../../config/cdn';
import style from './styles.module.scss';

const BannerSection = (props) => {
    return (
        <section className={style.BlogAuthorBanner}>
            <Container>
                {props.breadcrumb ? <BlogBreadcrumb {...props.breadcrumb} style={{'color': '#515151', 'top' : '15%'}} /> : ''} 
                <img src={cdn(`${props.image.name}?w=190&h=190`)} alt={props.image.name} />
                <div>
                    <h1>{props.name}</h1>
                    {props.designation ? <span>{props.designation}</span> : ''}
                    <p dangerouslySetInnerHTML={{__html:props.authorInfo}}></p>
                    { props.twitterUrl ? <a href={props.twitterUrl} target="_blank">
                        <SVG src="/static/Images/addressicons/twitter.svg"/></a> : '' }

                    { props.linkedinUrl ? <a href={props.linkedinUrl} target="_blank">
                        <SVG src="/static/Images/addressicons/linkedin-squ.svg"/></a>
                     : ''}
                </div>
            </Container>
        </section>
    )
}

export default BannerSection
