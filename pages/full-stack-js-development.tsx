import NewBanner from "containers/ServicePageBanners/bannerPage";
import { getFullStackPageData, getUniversal } from "lib/api";
import { GetStaticProps } from "next";
import React from "react";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import AwardsAndRecognition from "../containers/CommonSections/AwardandRecognition/AwardsAndRecognition";
import ClientTestimonialCarousel from "../containers/CommonSections/ClientTestimonialCarousel/ClientTestimonialCarousel";
import CTAStripe from "../containers/CommonSections/CTAStripe/CTAStripe";
import EngagementModelSection from "../containers/CommonSections/EngagementModel/EngagementModelSection";
import FactsandAwards from "../containers/CommonSections/FactsandAwards/FactsandAwards";
import TrustedSection from "../containers/CommonSections/TrustedSection/TrustedSection";
import WhatWeDoSection from "../containers/FullStackDevPage/WhatWeDoSection/WhatWeDoSection";
import loaded from "../hooks/loaded";

const FullStackDevPage = ({ uv, data }) => {
  let loader = loaded();

  return (
    <Layout>
      <NewBanner
        title="Full Stack JS Development Services"
        backImage={"Fullstack_f284d19770.png"}
        buttonLabel="Our Work"
        link="/work"
        description="<p>Build Enterprise-Scale JavaScript Applications</p>"
      />
      <WhatWeDoSection content={data.whatwedofullstacks} />
      <FactsandAwards factandawards={data.factandawards} />
      <EngagementModelSection
        title=" Engagement Models - Hire Full Stack JavaScript Developers"
        description="Hire remote full stack JavaScript developers and agile JS development team according to your project needs and budget."
      />
      <CTAStripe
        link="/get-started"
        title="Have a Project in Mind?"
        LinkButtontxt="Let's Discuss"
      />
      <ClientTestimonialCarousel content={data.testimonialcarouselcontents} />
      <TrustedSection heading="Trusted By" />
      <AwardsAndRecognition awardandrecognitions={data.awardandrecognitions} />
      <LetsTalkComp />
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal("/full-stack-js-development");
  let data = await getFullStackPageData();

  return {
    props: {
      uv,
      data,
    },
  };
};

export default FullStackDevPage;
