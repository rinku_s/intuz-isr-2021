import NewBanner from 'containers/ServicePageBanners/bannerPage';
import { getPartnerPageData, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React from "react";
import Layout from '../components/Layout/Layout';
import LetsTalkComp from '../components/LetsTalkComp/LetsTalkComp';
import SectionAgency from '../containers/PartnerPage/SectionAgency/SectionAgency';
import SectionBranch from '../containers/PartnerPage/SectionBranch/SectionBranch';
import SectionBreakPoint from '../containers/PartnerPage/SectionBreakPoint/SectionBreakPoint';
import SectionDevBlock from '../containers/PartnerPage/SectionDevBlock/SectionDevBlock';
import SectionPartner from '../containers/PartnerPage/SectionPartner/SectionPartner';
import SectionService from '../containers/PartnerPage/SectionService/SectionService';
import SectionTechStack from '../containers/PartnerPage/SectionTechStack/SectionTechStack';

const Partner = ({ uv, data }) => {
    return (

        <Layout>
            {/* <ServicePageBanner content={data.apppage.sectionbanner} /> */}
            <NewBanner title="Looking for a Development Agency Partner?" backImage={"partner-with-uspage-banner.jpg"} buttonLabel='Let’s Connect' link='/get-started' description='<p>NodeJs, Python, AngularJs, ReactJs, React Native, and Blockchain Development</p>' />

            <SectionDevBlock content={data.apppage.sectionservices} />
            <SectionAgency />
            <SectionService content={data.sectionpartnerbigserviceblocks} />
            <SectionBreakPoint content={data.apppage.sectionbreakpoints} />
            <SectionTechStack />
            <SectionBranch />
            <SectionPartner />
            <LetsTalkComp />
        </Layout>

    )
}


export const getStaticProps: GetStaticProps = async (context) => {
    // console.log(context);
    let uv = await getUniversal('/partner-with-us');
    let data = await getPartnerPageData();

    return {
        props: {
            uv,
            data
        }
    }
}

export default Partner