import NewBanner from "containers/ServicePageBanners/bannerPage";
import { getReactNativePageData, getUniversal } from "lib/api";
import { GetStaticProps } from "next";
import React from "react";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import { cdn } from "../config/cdn";
import AwardsAndRecognition from "../containers/CommonSections/AwardandRecognition/AwardsAndRecognition";
import EngagementModelSection from "../containers/CommonSections/EngagementModel/EngagementModelSection";
import TrustedSection from "../containers/CommonSections/TrustedSection/TrustedSection";
import CtaSection from "../containers/ReactNativePage/CTASection/CtaSection";
import JustLaunchedSection from "../containers/ReactNativePage/JustLaunchedSection/JustLaunchedSection";
import Testimonialsection from "../containers/ReactNativePage/RNTestimonial/Testimonialsection";



const ReactNativePage = ({ uv, data }) => {
  return (

    <Layout>
      {/* <SectionBanner content={data.apppage.sectionbanner} /> */}
      {/* <ApplicationPageBanner
        content={data.apppage.sectionbanner}
        variation="reactnative"
      /> */}
      <NewBanner title="React Native Development Services" backImage={"React_Native_29874dea1f.png"} buttonLabel='Our Work' link='/work' description="<p>Improving Your Business's Efficiency & Reliability<p>" />
      <EngagementModelSection
        style={{
          background: `url(${cdn(
            "reactnative_eng_bg.png?auto=format"
          )}) center center no-repeat #FAFAFA`,
        }}
        description="Intuz works as a <span> backbone for B2B & B2C companies</span> by providing high quality experienced developers to work remotely on client projects on <span> dedicated monthly</span> or <span>on fixed project-to-project basis.</span>"
        button={true}
        title="Engagement Models - React Native Development"
      />
      <CtaSection
        title="Digital Agency? <strong>Partner with Intuz</strong>"
        description="Hire full stack development teams for creating <br/> cutting edge web and mobile application."
      />
      <JustLaunchedSection />
      <TrustedSection heading f="s" />
      <Testimonialsection
        subtitle="Acknowledgement & appreciation from our clients drives us."
        description="I really appreciated their designs, because they showcased our company's image in an excellent way."
        clientLogo="react-native-client.png"
        clientCaption="<br />Matthew Freeman<br/><span>Founder - Live4It Locations, UK</span>"
      />
      <AwardsAndRecognition
        awardandrecognitions={data.awardandrecognitions}
      />
      <LetsTalkComp />
    </Layout>

  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal('/react-native-development');
  let data = await getReactNativePageData();

  return {
    props: {
      uv,
      data
    }
  }
}

export default ReactNativePage;
