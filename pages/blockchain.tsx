import React from "react";
import HeadAndFootNew from "../components/HeadAndFoot/HeadAndFootNew";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import BlockchainProjectSection from "../containers/BlockchainPage/BlockchainProjectSection/BlockchainProjectSection";
import BlockchainResourceSection from "../containers/BlockchainPage/BlockchainResourceSection/BlockchainResourceSection";
import BlockchainServiceSection from "../containers/BlockchainPage/BlockchainServiceSection/BlockchainServiceSection";
import IndustryUseCaseSection from "../containers/BlockchainPage/IndustryUseCaseSection/IndustryUseCaseSection";
import TechnologyStackSection from "../containers/BlockchainPage/TechnologyStackSection/TechnologyStackSection";
import WhyBlockChainSection from "../containers/BlockchainPage/WhyBlockChainSection/WhyBlockChainSection";
import WhyChoseIntuzSection from "../containers/BlockchainPage/WhyChoseIntuzSection/WhyChoseIntuzSection";
import CTAStripe from "../containers/CommonSections/CTAStripe/CTAStripe";
import TrustedSection from "../containers/CommonSections/TrustedSection/TrustedSection";
import SectionBanner from "../containers/Nodejs-dev-page/SectionBanner/SectionBanner";
import { getBlockchainPageData, getUniversal } from "../lib/api";

const blockchain = ({ data, uv }) => {
  return (
    <HeadAndFootNew data={uv}>
      <Layout>
        <SectionBanner
          content={data.apppage.sectionbanner}
          variation="blockchain"
        />
        <TrustedSection heading />
        {/* <TrustedSection nonslider heading/> */}
        <BlockchainServiceSection
          blockchainservices={data.blockchainservices}
        />
        <CTAStripe
          title="Have Specific Requirements?"
          LinkButtontxt="Let's Discuss"
          link="/get-started"
        />
        <IndustryUseCaseSection blockchainusecases={data.blockchainusecases} />
        <CTAStripe
          title="Experience Creativity & Engineering at Its Best"
          LinkButtontxt="Talk to Our Experts"
          link="/get-started"
        />
        <TechnologyStackSection
          blockchaintechstacks={data.blockchaintechstacks}
        />
        <BlockchainProjectSection />
        <WhyBlockChainSection />
        <WhyChoseIntuzSection />
        <CTAStripe
          title="Interested in Leveraging Blockchain Technology for your Business?"
          LinkButtontxt="Let's Discuss"
          link="/get-started"
        />
        <BlockchainResourceSection sectionresources={data.sectionresources} />
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};
export async function getStaticProps(context) {
  console.log(context);
  let uv = await getUniversal("/blockchain");
  let data = await getBlockchainPageData();

  return {
    props: {
      uv,
      data,
    },
  };
}
export default blockchain;
