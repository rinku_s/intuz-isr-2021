import ComponentWork from "containers/WorkPage/ComponentFullBanner/ComponentWork";
import React from "react";
import HeadAndFootNew from "../components/HeadAndFoot/HeadAndFootNew";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import SectionBreakPoint from "../containers/WorkPage/SectionBreakPoint/SectionBreakPoint";
import SectionClients from "../containers/WorkPage/SectionClients/SectionClients";
import { getUniversal, getWorkPageData } from "../lib/api";
const Work = ({ data, uv }) => {
  const greyAndroidIcon = [
    {
      src: "../../../static/Images/icons/and-ic-gray.svg",
      alt: "Available on android",
      title: "Available on android",
      variation: "circle_img_grey",
    },
  ];
  const greyAppleIcon = [
    {
      src: "../../../static/Images/icons/ios-ic-gray.svg",
      alt: "Available on IOS",
      title: "Available on IOS",
      variation: "circle_img_grey",
    },
  ];
  const whiteMacIcon = [
    {
      src: "../../../static/Images/icons/mac.svg",
      alt: "Available on mac",
      title: "Available on mac",
      variation: "circle_img",
      imgSrcXs: "../../../static/Images/icons/mac-gray.svg",
      variationXs: "circle_img_grey",
    },
  ];
  const whiteWindowsIcon = [
    {
      src: "../../../static/Images/icons/windows.svg",
      alt: "Available on windows",
      title: "Available on windows",
      variation: "circle_img",
      imgSrcXs: "../../../static/Images/icons/windows-gray.svg",
      variationXs: "circle_img_grey",
    },
  ];
  const whiteAppleIcon = [
    {
      src: "../../../static/Images/icons/ios-ic.svg",
      alt: "Available on IOS",
      title: "Available on IOS",
      variation: "circle_img",
      imgSrcXs: "../../../static/Images/icons/ios-ic-gray.svg",
      variationXs: "circle_img_grey",
    },
  ];

  const whiteWebIcon = [
    {
      src: "../../../static/Images/icons/ic-web-white.svg",
      alt: "Available on Web",
      title: "Available on Web",
      variation: "circle_img",
      imgSrcXs: "../../../static/Images/icons/ic-web-grey.svg",
      variationXs: "circle_img_grey",
    },
  ];

  const whiteAndroidIcon = [
    {
      src: "../../../static/Images/icons/and-ic.svg",
      alt: "Available on android",
      title: "Available on android",
      variation: "circle_img",
      imgSrcXs: "../../../static/Images/icons/and-ic-gray.svg",
      variationXs: "circle_img_grey",
    },
  ];

  return (
    <HeadAndFootNew data={uv}>
      <Layout whiteBackground>
        {/* <SectionFullCaseBlock
          backImageSrc="bubbabooking_big_63efd9d445.png"
          backMobileSrc="bubbabooking_sm_6aac718f9a.jpeg"
          logoSrc="bubbabooking_logo_afc21d5a18.png"
          logoSrcXs="bubbabooking_logo_afc21d5a18.png"
          logoTitle="Bubba Booking"
          desc="Book Last Minute Tours & Adventure Activities Deals"
          IconArray={[whiteWebIcon[0], whiteAppleIcon[0], whiteAndroidIcon[0]]}
          linkBtnHref="/case-studies/bubba-booking"
          variation="topBlock"
          sectionVariation="topBlock"
          isFirstBanner={true}
        /> */}
        <ComponentWork
          contentCenter
          version="full"
          variant="right"
          desktopImage="bubbabooking_big_63efd9d445.png"
          mobileImage="bubbabooking_sm_6aac718f9a.jpeg"
          theme="light"
          description="Book Last Minute Tours & Adventure Activities Deals"
          logo="bubbabooking_logo_afc21d5a18.png"
          icons={{ web: true, android: true, ios: true }}
          caseStudyLink="/case-studies/bubba-booking"
        />
        <ComponentWork
          contentCenter
          version="half"
          variant="left"
          title="SPECK PUMP"
          mobileImage="speckpump-sm-work.jpg"
          theme="dark"
          description="IoT Enabled Smart Pool Equipments Controlling & Automation System Development"
          logo="badu_logo_3ec68a4799.png"
          icons={{ web: true, android: true, ios: true }}
          caseStudyLink="/case-studies/bubba-booking"
        />
        <ComponentWork
          version="full"
          variant="right"
          title="Healthcare-Tech Platform"
          desktopImage="junobaby_top_banner.jpg"
          mobileImage="junobaby_sm_banner.jpg"
          theme="dark"
          description="Marketplace App - Find & Hire Local Prenatal, Labor and Postpartum Experts"
          logo="hellomeela-logon.png"
          icons={{ web: true, android: true, ios: true }}
          caseStudyLink="/case-studies/bubba-booking"
        />
        <ComponentWork
          contentCenter
          version="half"
          variant="right"
          title="Travel & Local"
          mobileImage="live4it_casestudy_banner.png"
          theme="dark"
          description="Directory App - Find Nearby Sports, Events & Activities"
          logo="live4it_block_logo.png"
          icons={{ web: true, android: true, ios: true }}
          caseStudyLink="/case-studies/bubba-booking"
        />
        <ComponentWork
          contentCenter
          version="half"
          variant="left"
          title="Oil & Gas"
          mobileImage="sgc-case-xs.png"
          theme="dark"
          description="Enterprise Level Fuel Station Operations Automation App for a Large Oil Marketing Company."
          logo="sgc-case-logo.png"
          icons={{ web: true, android: true, ios: true }}
          caseStudyLink="/case-studies/bubba-booking"
        />
        <ComponentWork
          contentCenter
          version="half"
          variant="right"
          title="On-Demand Food Delivery App"
          mobileImage="food-delivery-bg-block.png"
          theme="dark"
          description="Swipe, Choose, & Bite - Discovering Your Next Meal Has Never Been Easier."
          logo="bite_logo_block.png"
          icons={{ web: true, android: true, ios: true }}
          caseStudyLink="/case-studies/bubba-booking"
        />
        <SectionBreakPoint
          backImage="break2.png"
          title="Experience Creativity & Engineering At Its Best."
          btnLink="/get-started"
          btnLabel="Get Started"
        />
        <ComponentWork
          version="full"
          variant="left"
          title="Health & Fitness"
          desktopImage="phyzyou-case.jpg"
          mobileImage="phyzyou-case-xs.jpg"
          theme="light"
          description="On-Desk Exercise App Quick & Easy Workout Routines"
          logo="phyzyou-logo.png"
          icons={{ web: true, android: true, ios: true }}
          caseStudyLink="/case-studies/bubba-booking"
        />
        <ComponentWork
          contentCenter
          version="half"
          variant="right"
          title="Education & E-Learning"
          mobileImage="mcqs-case.png"
          theme="dark"
          description="Online Learning & Exam Preparation App Solution For Medical & Healthcare Graduates"
          logo="mcqs-case-logo.png"
          icons={{ web: true, android: true, ios: true }}
          caseStudyLink="/case-studies/bubba-booking"
        />

        <SectionBreakPoint
          backImage="break1.png"
          title="Let’s Get You The Best Seat In The House."
          btnLink="/get-started"
          btnLabel="Say Hi!"
        />
        <ComponentWork
          desktopImage="lyricnote-case.jpg"
          version="full"
          variant="right"
          title="Music & Entertainment"
          mobileImage="lyricnote-case-xs.jpg"
          theme="dark"
          description="Write Lyrics, Record &amp; Organize App for Song Writers &amp; Musicians"
          logo="lyricnote-logo.png"
          icons={{ web: true, android: true, ios: true }}
          caseStudyLink="/case-studies/bubba-booking"
        />
        <ComponentWork
          contentCenter
          version="half"
          variant="right"
          title="Lifestyle"
          mobileImage="wateeny-case.png"
          theme="dark"
          description="Online Marketplace App Solution to Buy, Sell and Bid on Old Used Items."
          logo="wateeny-case-logo.png"
          icons={{ web: true, android: true, ios: true }}
          caseStudyLink="/case-studies/bubba-booking"
        />

        <SectionBreakPoint
          backImage="break3.png"
          title="Still Thinking? Let’s Talk and Get Things Moving!"
          btnLink="/get-started"
          btnLabel="Let’s Talk"
        />
        <ComponentWork
          version="full"
          desktopImage="alw-case-bg-1.jpg"
          variant="right"
          title="Social media app"
          mobileImage="alw-case-xs.jpg"
          theme="light"
          description="Find Friends, Groups, Chat, Followers, Events Listing"
          logo="alw-case-logo.png"
          icons={{ web: true, android: true, ios: true }}
          caseStudyLink="/case-studies/bubba-booking"
        />
        <ComponentWork
          contentCenter
          version="half"
          variant="right"
          title="ONLINE MARKETPLACE"
          mobileImage="helparu-case.png"
          theme="dark"
          description="On Demand App Solution for Local Household Services Post Your Job. Find & Hire Professionals"
          logo="helparu-case-logo.png"
          icons={{ web: true, android: true, ios: true }}
          caseStudyLink="/case-studies/bubba-booking"
        />
        <SectionBreakPoint
          backImage="break2.png"
          title="Experience Creativity & Engineering At Its Best."
          btnLink="/get-started"
          btnLabel="Get Started"
        />
        <ComponentWork
          version="full"
          contentCenter
          desktopImage="1churche-1day.jpg"
          variant="right"
          title="1CHURCH 1DAY"
          mobileImage="1c1d-banner.jpg"
          theme="light"
          description="Transform Your Prayer Life"
          logo="1day.png"
          icons={{ web: true, android: true, ios: true }}
          caseStudyLink="/case-studies/bubba-booking"
        />
        <ComponentWork
          backgroundColor="#447af3"
          contentCenter
          version="half"
          variant="left"
          title="Lifestyle"
          mobileImage="picit-case.png"
          theme="light"
          description="Snap & Share Pictures Get Opinions & Comments from Friends"
          logo="picit-logo.png"
          icons={{ web: true, android: true, ios: true }}
          caseStudyLink="/case-studies/bubba-booking"
        />
        {/* <SectionFullCaseBlock
          backImageSrc="1churche-1day.jpg"
          backMobileSrc="1c1d-banner.jpg"
          logoSrc="1day.png"
          logoTitle="1churche 1day"
          title="1CHURCH 1DAY"
          desc="Transform Your Prayer Life"
          IconArray={[whiteAppleIcon[0], whiteAndroidIcon[0]]}
          linkBtnHref="/case-studies/1church1day"
          variation="churcheblock"
          sectionVariation="down"
        /> */}
        <SectionBreakPoint
          backImage="break3.png"
          title="Still Thinking? Let’s Talk and Get Things Moving!"
          btnLink="/get-started"
          btnLabel="Let’s Talk"
        />
        <ComponentWork
          version="full"
          contentCenter
          desktopImage="pocket-money-case-bg.jpg"
          variant="right"
          title="Retail"
          mobileImage="pocket-case-xs.jpg"
          theme="light"
          description="Location Based Mobile App For Daily Deals and Discount Coupons."
          logo="pocket-case-logo.png"
          icons={{ web: true, android: true, ios: true }}
          caseStudyLink="/case-studies/bubba-booking"
        />
        <ComponentWork
          contentCenter
          version="half"
          variant="left"
          title="Media & Communication"
          mobileImage="lifeofu-case.png"
          theme="dark"
          description="Video Sharing Marketplace App Record, Post, Share, & Sell Your Videos"
          logo="lifeofu-case-logo.png"
          icons={{ web: true, android: true, ios: true }}
          caseStudyLink="/case-studies/bubba-booking"
        />
        {/* <SectionFullCaseBlock
          backImageSrc="pocket-money-case-bg.jpg"
          backMobileSrc="pocket-case-xs.jpg"
          logoSrc="pocket-case-logo.png"
          logoSrcXs="pocket-case-logo-xs.png"
          logoTitle="Pocket Money"
          title="Retail"
          desc="Location Based Mobile App For Daily Deals and Discount Coupons."
          IconArray={[whiteAppleIcon[0], whiteAndroidIcon[0]]}
          linkBtnHref="/case-studies/pocketmoney"
          variation="pocketblock"
          sectionVariation="down"
        /> */}

        {/* <SectionCaseBlock
          logoSrc="lifeofu-case-logo.png"
          logoTitle="Life of U"
          title="Media & Communication"
          subTitle="Video Sharing Marketplace App Record, Post, Share, & Sell Your Videos"
          icons={greyAppleIcon}
          btnLink="/case-studies/lifeofu"
          backimgSrc="lifeofu-case.png"
          backimgtitle="Media & Communication - Life of U"
        /> */}

        <SectionBreakPoint
          backImage="break2.png"
          title="Experience Creativity & Engineering At Its Best."
          btnLink="/get-started"
          btnLabel="Get Started"
        />

        <SectionClients sectionclients={data.apppage.sectionclients} />
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};
export async function getStaticProps(context) {
  let uv = await getUniversal("/work");
  let data = await getWorkPageData();

  return {
    props: {
      uv,
      data,
    },
  };
}
export default Work;
