
import { getFoodDeliveryDate, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';
import FaqComponent from '../components/FAQComponent/FaqComponent';
import FaqSchema from '../components/FAQComponent/FaqSchema';
import Layout from '../components/Layout/Layout';
import LetsTalkComp from '../components/LetsTalkComp/LetsTalkComp';
import SectionBreakPoint from '../containers/CommonSections/SectionBreakPoint/SectionBreakPoint';
import ServicePageBanner from '../containers/ServicePageBanners/ServicePageBanner';
import SectionCustomApp from '../containers/TaxiAppPage/SectionCustomApp/SectionCustomApp';
import SectionProject from '../containers/TaxiAppPage/SectionProject/SectionProject';
import SectionSolution from '../containers/TaxiAppPage/SectionSolution/SectionSolution';

const FoodDel = ({ uv, data }) => {
    return (

        <Layout>
            <ServicePageBanner content={data.apppage.sectionbanner} form={{ subject: "Food Delivery App Development Quote Request", leadsource: "Food LP - Popup", thankyoupage: "food-delivery-app-development-thankyou" }} />
            <SectionCustomApp content={data.apppage.featuresection} />
            <SectionSolution
                title="Solution overview"
                subtitle="A feature loaded and a fully customizable on-demand food delivery app development solution."
                tabTitles={data.apppage.sectiontabtitles}
                backColor='#fafafa'
            />
            <SectionBreakPoint content={data.apppage.sectionbreakpoints} />
            <SectionProject
                title="Featured Apps"
                subtitle="Check some of the food ordering development platforms we have created in the past."
                sectionrecentworks={data.apppage.sectionpagehalfblocks} />
            <FaqComponent faqs={data.apppage.faqs[0].Faqs} />
            <LetsTalkComp />
            <FaqSchema faqs={data.apppage.faqs[0].Faqs} />
        </Layout>

    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    // console.log(context);
    let uv = await getUniversal('/food-delivery-app-development');
    let data = await getFoodDeliveryDate();

    return {
        props: {
            uv,
            data
        }
    }
}
export default FoodDel

