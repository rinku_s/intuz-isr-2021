import Head from "next/head";
import React from "react";
// import SwiperCore, { Navigation, Pagination } from "swiper";
import FaqComponent from "../components/FAQComponent/FaqComponent";
import FaqSchema from "../components/FAQComponent/FaqSchema";
import HeadAndFootNew from "../components/HeadAndFoot/HeadAndFootNew";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import IOTSliceZone from "../containers/IotNewPage";
import { getIotDevelopmentData, getUniversal } from "../lib/api";
// SwiperCore.use([Navigation, Pagination]);
const IotApplication = ({ uv, data }) => {
  return (
    <HeadAndFootNew data={uv}>
      <Layout nunito={true} whiteBackground>
        <Head>
          <link rel="preconnect" href="https://fonts.googleapis.com" />
          <link
            rel="preconnect"
            href="https://fonts.gstatic.com"
            crossOrigin="anonymous"
          />
          <link
            href="https://fonts.googleapis.com/css2?family=Lato&display=swap"
            rel="stylesheet"
          />
        </Head>
        <IOTSliceZone />
        <FaqComponent faqs={data.apppage.faqs[0].Faqs} />
        <FaqSchema faqs={data.apppage.faqs[0].Faqs} />
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};

export async function getStaticProps(context) {
  let uv = await getUniversal("/iot-application-development");
  const data = await getIotDevelopmentData();
  return {
    props: {
      uv,
      data,
    },
  };
}

export default IotApplication;
