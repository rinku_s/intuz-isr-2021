import React from "react";
import HeadAndFootNew from "../../components/HeadAndFoot/HeadAndFootNew";
import Layout from "../../components/Layout/Layout";
import LetsTalkComp from "../../components/LetsTalkComp/LetsTalkComp";
import CasestudySolution from "../../containers/CasestudyContainer/Casestudysolution/index";
import ChallengeSection from "../../containers/CasestudyContainer/ChallengesSection/ChallengeSection";
import DesignChallengeSectionnew from "../../containers/CasestudyContainer/DesignChallangesnew/index";
import DesignChallengeSection from "../../containers/CasestudyContainer/DesignChallengeSection/DesignChallengeSection";
import DevProcessSection from "../../containers/CasestudyContainer/DevProcessSection/DevProcessSection";
import EngagementSection from "../../containers/CasestudyContainer/EngagementSection/EngagementSection";
import IconBoxChallengeSection from "../../containers/CasestudyContainer/IconboxchallangeSection/index";
import MoreProjectSection from "../../containers/CasestudyContainer/MoreProjectSection/MoreProjectSection";
import Banner from "../../containers/CasestudyContainer/SectionBanner/Banner";
import FourBFeatureSection from "../../containers/CasestudyContainer/SectionFeature/FourBFeatureSection";
import Highlight from "../../containers/CasestudyContainer/SectionHighLight/HighLight";
import HighlightMenu from "../../containers/CasestudyContainer/SectionHighlightMenu/HighlightMenu";
import SingleChallengeSection from "../../containers/CasestudyContainer/SingleChallengesSection/SingleChallengeSection";
import TechnicalSpecs from "../../containers/CasestudyContainer/TechnicalSpecs/TechnicalSpecs";
import Testimonialsection from "../../containers/CasestudyContainer/Testimonial/Testimonialsection";
import { getUniversal } from "../../lib/api";
import Data from "../../lib/data/4bdata.json";
const Testpage = ({ uv }) => {
  let data = Data;
  let casestudy = data.casestudies[0];
  return (
    <HeadAndFootNew data={uv}>
      <Layout background={casestudy.backcolor} maven>
        <HighlightMenu
          stickycolor={casestudy.backcolor}
          content={casestudy.casestudyhighlightmenus}
          casestudyname={casestudy.name}
        />
        <Banner content={casestudy.casestudybanner} />
        <Highlight
          content={casestudy.casestudyhighlightsection}
          isFourB={true}
        />
        {/* <FourbVideosection content="/Annimation1.mp4" /> */}
        {/* {casestudy.casestudyfeaturessection.featurelist.length > 0 ? (
        <CaseStudyFeatureSectionnew
          content={casestudy.casestudyfeaturessection}
        />
      ) : (
        <SectionFeature content={casestudy.casestudyfeaturessection} />
      )} */}
        <FourBFeatureSection content={casestudy.casestudyfeaturessection} />
        {casestudy.casestudyprojectchallenges.length > 0 ? (
          casestudy.casestudyprojectchallenges.length > 1 ? (
            <ChallengeSection
              content={casestudy.casestudyprojectchallenges}
              dataLength={casestudy.casestudyprojectchallenges.length}
            />
          ) : casestudy.casestudyprojectchallenges[0].projectchallengesiconbox
            .length === 0 ? (
            <SingleChallengeSection
              content={casestudy.casestudyprojectchallenges[0]}
              isFourB={true}
            />
          ) : (
            <IconBoxChallengeSection
              content={casestudy.casestudyprojectchallenges[0]}
            />
          )
        ) : (
          ""
        )}
        {casestudy.casestudyengagement ? (
          <EngagementSection content={casestudy.casestudyengagement} />
        ) : (
          ""
        )}
        {casestudy.casestudydesignchallenge ? (
          casestudy.casestudydesignchallenge.fullpageImage !== true ? (
            <DesignChallengeSection
              content={casestudy.casestudydesignchallenge}
            />
          ) : (
            <DesignChallengeSectionnew
              content={casestudy.casestudydesignchallenge}
            />
          )
        ) : (
          ""
        )}
        {casestudy.case_study_solution ? (
          <CasestudySolution content={casestudy.case_study_solution} />
        ) : (
          ""
        )}
        {casestudy.casestudydevprocesses.length > 0 ? (
          <DevProcessSection
            process={casestudy.casestudydevprocesses}
            descriptions={casestudy.process}
          />
        ) : (
          ""
        )}
        {casestudy.casestudytechnicalspecs.length > 0 ? (
          <TechnicalSpecs content={casestudy.casestudytechnicalspecs} />
        ) : (
          ""
        )}
        {casestudy.casestudytestimonial ? (
          <Testimonialsection content={casestudy.casestudytestimonial} />
        ) : (
          ""
        )}
        <MoreProjectSection content={casestudy.casestudymoreprojects} />
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};
export async function getStaticProps(context) {
  let uv = await getUniversal("/case-studies/4b-world");
  return {
    props: {
      uv,
    },
  };
}

export default Testpage;
