import LoadingScreen from "components/LoadingScreen/LoadingScreen";
import { getCaseStudyData, getUniversal } from "lib/api";
import { GetStaticPaths, GetStaticProps } from "next";
import { useRouter } from "next/router";
import React from "react";
import Layout from "../../components/Layout/Layout";
import LetsTalkComp from "../../components/LetsTalkComp/LetsTalkComp";
import CaseStudyFeatureSectionnew from "../../containers/CasestudyContainer/caseStudyFeaturesnew/index";
import CasestudySolution from "../../containers/CasestudyContainer/Casestudysolution/index";
import ChallengeSection from "../../containers/CasestudyContainer/ChallengesSection/ChallengeSection";
import DesignChallengeSectionnew from "../../containers/CasestudyContainer/DesignChallangesnew/index";
import DesignChallengeSection from "../../containers/CasestudyContainer/DesignChallengeSection/DesignChallengeSection";
import DevProcessSection from "../../containers/CasestudyContainer/DevProcessSection/DevProcessSection";
import EngagementSection from "../../containers/CasestudyContainer/EngagementSection/EngagementSection";
import IconBoxChallengeSection from "../../containers/CasestudyContainer/IconboxchallangeSection/index";
import MoreProjectSection from "../../containers/CasestudyContainer/MoreProjectSection/MoreProjectSection";
import Banner from "../../containers/CasestudyContainer/SectionBanner/Banner";
import SectionFeature from "../../containers/CasestudyContainer/SectionFeature/SectionFeature";
import Highlight from "../../containers/CasestudyContainer/SectionHighLight/HighLight";
import HighlightMenu from "../../containers/CasestudyContainer/SectionHighlightMenu/HighlightMenu";
import SingleChallengeSection from "../../containers/CasestudyContainer/SingleChallengesSection/SingleChallengeSection";
import TechnicalSpecs from "../../containers/CasestudyContainer/TechnicalSpecs/TechnicalSpecs";
import Testimonialsection from "../../containers/CasestudyContainer/Testimonial/Testimonialsection";
//import Error from "../_error";

const Testpage = ({ uv, data }) => {
  let router = useRouter();

  if (router.isFallback) return <LoadingScreen />

  //  const { error, loading, data } =  useQuery(query);
  // if(loading) return <LoadingScreen/>

  // let casestudy = data.casestudies[0];
  // if(casestudy == undefined) return <Error statusCode={404} />
  let casestudy = data.casestudies[0];
  return (


    //if (casestudy == undefined) return <Error statusCode={404} />;

    <Layout background={casestudy.backcolor} maven>
      <HighlightMenu
        stickycolor={casestudy.backcolor}
        content={casestudy.casestudyhighlightmenus}
        casestudyname={casestudy.name}
      />
      <Banner content={casestudy.casestudybanner} />
      <Highlight content={casestudy.casestudyhighlightsection} />
      {casestudy.casestudyfeaturessection.featurelist.length > 0 ? (
        <CaseStudyFeatureSectionnew
          content={casestudy.casestudyfeaturessection}
        />
      ) : (
        <SectionFeature content={casestudy.casestudyfeaturessection} />
      )}

      {casestudy.casestudyprojectchallenges.length > 0 ? (
        casestudy.casestudyprojectchallenges.length > 1 ? (
          <ChallengeSection
            content={casestudy.casestudyprojectchallenges}
            dataLength={casestudy.casestudyprojectchallenges.length}
          />
        ) : casestudy.casestudyprojectchallenges[0]
          .projectchallengesiconbox.length === 0 ? (
          <SingleChallengeSection
            content={casestudy.casestudyprojectchallenges[0]}
          />
        ) : (
          <IconBoxChallengeSection
            content={casestudy.casestudyprojectchallenges[0]}
          />
        )
      ) : (
        ""
      )}
      {casestudy.casestudyengagement ? (
        <EngagementSection content={casestudy.casestudyengagement} />
      ) : (
        ""
      )}
      {casestudy.casestudydesignchallenge ? (
        casestudy.casestudydesignchallenge.fullpageImage !== true ? (
          <DesignChallengeSection
            content={casestudy.casestudydesignchallenge}
          />
        ) : (
          <DesignChallengeSectionnew
            content={casestudy.casestudydesignchallenge}
          />
        )
      ) : (
        ""
      )}
      {casestudy.case_study_solution ? (
        <CasestudySolution content={casestudy.case_study_solution} />
      ) : (
        ""
      )}
      {casestudy.casestudydevprocesses.length > 0 ? (
        <DevProcessSection
          process={casestudy.casestudydevprocesses}
          descriptions={casestudy.process}
        />
      ) : (
        ""
      )}
      {casestudy.casestudytechnicalspecs.length > 0 ? (
        <TechnicalSpecs content={casestudy.casestudytechnicalspecs} />
      ) : (
        ""
      )}
      {casestudy.casestudytestimonial ? (
        <Testimonialsection content={casestudy.casestudytestimonial} />
      ) : (
        ""
      )}
      <MoreProjectSection content={casestudy.casestudymoreprojects} />
      <LetsTalkComp />
    </Layout>
  );

};

export const getStaticProps: GetStaticProps = async (context) => {
  console.log(context);
  let uv = await getUniversal(`/case-studies/${context.params.id}`);
  //context.params.name
  let data = await getCaseStudyData(context.params.id) as any;

  if (data.casestudies.length === 0) {
    return {
      notFound: true
    }
  }

  return {
    props: {
      uv,
      data
    }
  }
}

export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [
      { params: { id: "bubba-booking" } }
    ],
    fallback: true
  }
}


export default Testpage;
