import NewBanner from "containers/ServicePageBanners/bannerPage";
import { getReactJsPageData, getUniversal } from "lib/api";
import { GetStaticProps } from "next";
import React from "react";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import AwardsAndRecognition from "../containers/CommonSections/AwardandRecognition/AwardsAndRecognition";
import TrustedSection from "../containers/CommonSections/TrustedSection/TrustedSection";
import AppDetail from "../containers/EnterprisePage/SectionAppDetail/AppDetail";
import SectionFeatureApp from "../containers/Reactjs-dev-page/SectionFeatureApp/SectionFeatureApp";
import SectionHireDeveloper from "../containers/Reactjs-dev-page/SectionHireDeveloper/SectionHireDeveloper";
import SectionReactBlock from "../containers/Reactjs-dev-page/SectionReactBlock/SectionReactBlock";
import SectionTechCompare from "../containers/Reactjs-dev-page/SectionTechCompare/SectionTechCompare";
import CtaSection from "../containers/ReactNativePage/CTASection/CtaSection";
import Testimonialsection from "../containers/ReactNativePage/RNTestimonial/Testimonialsection";

const ReactDev = ({ uv, data }) => {
  return (

    <Layout>
      {/* <SectionBanner content={data.apppage.sectionbanner} /> */}
      <NewBanner title="React JS App Development Services" backImage={"React_d7f405c19e.png"} buttonLabel='Our Work' link='/work' description='<p>Crafting Interactive and Indulging Web Applications<p>' />
      {/* <ApplicationPageBanner
        content={data.apppage.sectionbanner}
        variation="reactjsDevlopment"
      /> */}
      <SectionHireDeveloper
        title="Engagement Models - React JS Development"
        description="Harness the experience and expertise of our professional ReactJS developers by availing our reactJS services. We provide flexible engagement models that allow you to hire dedicated developers on a monthly basis or engage experts on a fixed cost project basis."
        backImage="reactjs-hire-dev_bg"
      />
      <CtaSection
        title="Are You a Digital Agency?  <strong>Partner with Intuz</strong>"
        description="If you are a digital agency looking for a reliable partner for ReactJS development services, Intuz is a professional ReactJS development company that can offer exclusive support. Hire ReactJS developers from Intuz and shed off your development woes."
      />
      <SectionReactBlock>
        <AppDetail style={{ marginTop: "0" }}>
          {data.apppage.sectiontopicondetails}
        </AppDetail>
      </SectionReactBlock>
      <SectionTechCompare activePage="react" />
      <SectionFeatureApp />
      <TrustedSection heading />
      <Testimonialsection
        subtitle="Our clients appreciate our ReactJS app development capabilities"
        description="I really enjoyed working with the Intuz team they offered me great expertise and very good advises on all of my current and future projects."
        clientLogo="reactjs_client.png"
        clientCaption="<br />Patrick Mimran<br /><span>Founder, Aphos</span>"
      />
      <AwardsAndRecognition
        awardandrecognitions={data.awardandrecognitions}
      />
      <LetsTalkComp />
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal('/react-js-development');
  let data = await getReactJsPageData();

  return {
    props: {
      uv,
      data
    }
  }
}

export default ReactDev;
