
import NewBanner from 'containers/ServicePageBanners/bannerPage';
import { getNodeJsData, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';
import FaqComponent from '../components/FAQComponent/FaqComponent';
import FaqSchema from '../components/FAQComponent/FaqSchema';
import Layout from '../components/Layout/Layout';
import LetsTalkComp from '../components/LetsTalkComp/LetsTalkComp';
import AwardsAndRecognition from '../containers/CommonSections/AwardandRecognition/AwardsAndRecognition';
import TrustedSection from '../containers/CommonSections/TrustedSection/TrustedSection';
import AppDetail from '../containers/EnterprisePage/SectionAppDetail/AppDetail';
import SectionAppFeature from '../containers/Nodejs-dev-page/SectionAppFeature/SectionAppFeature';
import SectionHireDeveloper from '../containers/Reactjs-dev-page/SectionHireDeveloper/SectionHireDeveloper';
import SectionReactBlock from '../containers/Reactjs-dev-page/SectionReactBlock/SectionReactBlock';
import SectionTechCompare from '../containers/Reactjs-dev-page/SectionTechCompare/SectionTechCompare';
import Testimonialsection from '../containers/ReactNativePage/RNTestimonial/Testimonialsection';


const NodeDev = ({ uv, data }) => {
    return (
        <Layout>
            {/* <SectionBanner content={data.apppage.sectionbanner} variation="nodejs" /> */}
            <NewBanner title="Node JS App Development Services" backImage={"nodejs_top_banner.jpg"} buttonLabel='Our Work' link='/work' description='<p>Build Fast, Scalable & Robust Server-side Applications</p>' />
            <SectionAppFeature />
            <TrustedSection heading />
            <SectionHireDeveloper
                title='Engagement Models - Node JS Development'
                description='Planning to harness NodeJS for your next project? Hire NodeJS developers from Intuz and get the best minds to help you transform your valuable idea into reality. You can hire NodeJS developers with flexible engagement options such as ‘Fixed Cost Project’ basis or ‘Monthly Dedicated Hiring’ model according to your requirement.'
                backImage='nodejs-hire-dev_bg'
            />
            <SectionReactBlock>
                <AppDetail>
                    {data.apppage.sectiontopicondetails}
                </AppDetail>
            </SectionReactBlock>
            <SectionTechCompare activePage='node' />
            <Testimonialsection
                subtitle='Acknowledgement & appreciation from our clients drives us.'
                description='Intuz quickly delivered products that would take other agencies months to develop. They followed a transparent workflow and adapted to changes to the project scope. The entire team was friendly and highly skilled, making them an outstanding partner.'
                clientLogo='nodejs_client.jpg'
                clientCaption='<br />Bruce Francois<br /><span>President, myPurpose NETWORK, USA</span>'
            />
            <AwardsAndRecognition awardandrecognitions={data.awardandrecognitions} />
            <FaqComponent faqs={data.apppage.faqs[0].Faqs} />
            <LetsTalkComp />
            <FaqSchema faqs={data.apppage.faqs[0].Faqs} />
        </Layout>

    )
}


export const getStaticProps: GetStaticProps = async (context) => {
    // console.log(context);
    let uv = await getUniversal('/node-js-development');
    let data = await getNodeJsData();

    return {
        props: {
            uv,
            data
        }
    }
}

export default NodeDev