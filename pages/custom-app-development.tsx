import NewBanner from "containers/ServicePageBanners/bannerPage";
import React from "react";
import CaseStudyCarousel from "../components/CaseStudyCarousel/CaseStudyCarousel";
import HeadAndFootNew from "../components/HeadAndFoot/HeadAndFootNew";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import SectionSolution from "../containers/ApplicationPage/SectionSolution/SectionSolution";
import ClientTestimonialCarousel from "../containers/CommonSections/ClientTestimonialCarousel/ClientTestimonialCarousel";
import SectionBreakPoint from "../containers/CommonSections/SectionBreakPoint/SectionBreakPoint";
import SectionIconDetail from "../containers/CommonSections/SectionIconDetail/SectionIconDetail";
import SectionIconGroup from "../containers/CommonSections/SectionIconGroup/SectionIconGroup";
import TrustedSection from "../containers/CommonSections/TrustedSection/TrustedSection";
import { CustomAppDevelopment, getUniversal } from "../lib/api";

const Application = ({ data, uv }) => {
  return (
    <HeadAndFootNew data={uv}>
      <Layout>
        {/* <ApplicationPageBanner
          content={data.apppage.sectionbanner}
          variation="customApp"
        /> */}

        <NewBanner
          headingClass="md:max-w-[80%]"
          title="Custom App  Development"
          backImage={"csa_832c1a9164.png"}
          buttonLabel="Our Work"
          link="/work"
          subTitle="Design Led Engineering"
          description="<ul><li>We Audit Your APP IDEA</li>
<li>We Consult to Add Value</li>
<li>We Develop to Make It Happen!</li></ul>"
        />

        <SectionSolution content={data.apppage.sectiontolicongroups} />
        <CaseStudyCarousel />
        <TrustedSection />
        <SectionIconDetail variation="addspace">
          {data.apppage.sectiontopicondetails}
        </SectionIconDetail>
        <SectionBreakPoint content={data.apppage.sectionbreakpoints} />
        <ClientTestimonialCarousel content={data.testimonialcarouselcontents} />
        <SectionIconGroup
          title="Custom App Domain Expertise"
          description="Intuz app developers have great experience in developing <br/> mobile apps across diverse industry segments."
        >
          {data.apppage.sectionbottomicongroups}
        </SectionIconGroup>
        <SectionIconDetail
          title="Adaptive Growth"
          buttonLabel="Learn more"
          buttonLink="/adaptive-growth"
        >
          {data.apppage.sectionbottomicondetails}
        </SectionIconDetail>
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};
export async function getStaticProps(context) {
  console.log(context);
  let uv = await getUniversal("/custom-app-development");
  let data = await CustomAppDevelopment();

  return {
    props: {
      uv,
      data,
    },
  };
}
export default Application;
