import React from 'react'
import Head from 'next/head'

const feedback = () => {
    return (
        <div>
            <Head>
            <meta name="robots" content="NOINDEX, NOFOLLOW" />
            <title>Intuz Client Feedback</title>
            <script src="https://form.jotform.me/jsform/61321606098453"></script>
            </Head>
        </div>
    )
}

export default feedback
