import NewBanner from "containers/ServicePageBanners/bannerPage";
import { getEnterprisePageData, getUniversal } from "lib/api";
import { GetStaticProps } from "next";
import React from "react";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import SectionBreakPoint from "../containers/CommonSections/SectionBreakPoint/SectionBreakPoint";
import SectionIconGroup from "../containers/CommonSections/SectionIconGroup/SectionIconGroup";
import AppDetail from "../containers/EnterprisePage/SectionAppDetail/AppDetail";
import SectionFeatureApp from "../containers/EnterprisePage/SectionFeatureApp/SectionFeatureApp";
import SectionTechTab from "../containers/EnterprisePage/SectionTechTab/SectionTechTab";
import TextContent from "../containers/EnterprisePage/SectionTextContent/TextContent";
import TopTitle from "../containers/EnterprisePage/sectionTopTitle/TopTitle";
import SectionResource from "../containers/HomePage/SectionResource/SectionResource";

const Enterprise = ({ uv, data }) => {
  return (
    <Layout>
      {/* <ServicePageBanner content={data.apppage.sectionbanner} /> */}
      {/* <ApplicationPageBanner
        content={data.apppage.sectionbanner}
        variation="enterprice"
      /> */}

      <NewBanner
        title="Enterprise Software Development Services"
        backImage={"Enterprise_292a4ea71b.png"}
        buttonLabel="Let’s Connect"
        link="/get-started"
        subTitle="Mobile, Web & Custom Software Development Solutions To Enable Business Transformation"
      />

      <SectionIconGroup
        title="Industries We Serve"
        description="Intuz has hands-on experience in versatile industry verticals by injecting its deep technical insights into leading-edge business strategies."
        variation="Enterprise_icon"
      >
        {data.apppage.sectiontolicongroups}
      </SectionIconGroup>

      <TopTitle
        title="Enterprise Web & Mobile Applications"
        description="Digitally transform & redefine your business strategy for increased productivity"
      >
        <TextContent>
          Intuz is a next-generation enterprise application development company.
          It has proven enterprise app development capabilities of delivering
          excellent web and mobile apps products for ERP, CRM, Booking
          Management, Stock Management, Location Tracking, Bug Tracking,
          Marketing & Promotion, HRMS, Accounts Management, and many other
          innovative solutions designed for specific business goals.
        </TextContent>
        <AppDetail variation="largeBlock">
          {data.apppage.sectiontopicondetails}
        </AppDetail>
      </TopTitle>

      <TopTitle
        title="Technology & Tool We Use"
        description="Blend of modern tools & Technologies to build next-generation enterprise solutions"
        backColor="#fafafa"
      >
        <SectionTechTab />
      </TopTitle>

      <TopTitle title="Featured Enterprise Web Application">
        <SectionFeatureApp content={data.apppage.featuresection} />
      </TopTitle>

      <TopTitle
        title="Product Engineering Services"
        description="Innovate or upgrade products with trending technology concepts"
      >
        <TextContent imgsrc="product-engineering-img.png">
          Re-engineer your traditional IT/non-IT products by shaking hands with
          emerging information technology concepts. Design a new or upgrade
          products with the concepts of the Internet of Things (IoT), AR/VR,
          Big-data, AI, Machine Learning, Cloud services etc. Even a combination
          of these in order to develop a most advanced, feature-rich,
          competitive and modest range of products with added benefits.
        </TextContent>
        <AppDetail>{data.apppage.sectionbottomicondetails}</AppDetail>
      </TopTitle>
      <SectionBreakPoint content={data.apppage.sectionbreakpoints} />
      <SectionResource sectionresources={data.guides} />
      <LetsTalkComp />
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal("/enterprise");
  let data = await getEnterprisePageData();

  return {
    props: {
      uv,
      data,
    },
  };
};

export default Enterprise;
