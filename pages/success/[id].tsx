import HeadAndFootNew from "components/HeadAndFoot/HeadAndFootNew";
import LoadingScreen from "components/LoadingScreen/LoadingScreen";
import ReactHtmlParser from "html-react-parser";
import { getSuccessIdPageData, getUniversal } from "lib/api";
import { GetStaticPaths, GetStaticProps } from "next";
import Head from "next/head";
import { useRouter } from "next/router";
import React from "react";
import Container from "../../components/Container/Container";
import FormSuccess from "../../components/FormSubmission/FormSuccess";
const thankyouc = ({ uv, data }) => {
  let router = useRouter();

  if (router.isFallback) return <LoadingScreen />
  return (
    <>
      <HeadAndFootNew data={uv}>
        <Container>
          <Head>
            <meta name="ROBOTS" content="NOINDEX, NOFOLLOW" />
            <title>{data.thankyoupages[0].title}</title>
            {ReactHtmlParser(`${data.thankyoupages[0].meta_tags}`)}
          </Head>
          <FormSuccess />
        </Container>
      </HeadAndFootNew>
      <script
        dangerouslySetInnerHTML={{ __html: data.thankyoupages[0].content }}
      />
    </>
  );
};


export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal('/');
  //context.params.name
  let data = await getSuccessIdPageData(context.params.id);

  if (data.thankyoupages.length === 0) {
    return {
      notFound: true
    }
  }

  return {
    props: {
      uv,
      data
    }
  }
}

export const getStaticPaths: GetStaticPaths = () => {
  return {
    paths: [
      { params: { id: "thankyou" } }
    ],
    fallback: true
  }
}

export default thankyouc;
