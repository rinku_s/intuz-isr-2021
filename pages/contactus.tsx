import React from "react";
import HeadAndFootNew from "../components/HeadAndFoot/HeadAndFootNew";
import Layout from "../components/Layout/Layout";
import SectionForm from "../containers/ContactUsPage/SectionForm/SectionForm";
import ServiceBanner from "../containers/ServicePageBanners/ServicePageBanner";
import { getUniversal } from "../lib/api";
const Contact = ({ uv }) => {
  let bannerContent = {
    title: "Contact Us",
    backImage: {
      name: "Contactuspage-banner.jpg",
    },
  };

  return (
    <HeadAndFootNew data={uv}>
      <Layout>
        <ServiceBanner content={bannerContent} variation="contactus" />
        <SectionForm />
      </Layout>
    </HeadAndFootNew>
  );
};
export async function getStaticProps(context) {
  console.log(context);
  let uv = await getUniversal("/contactus");
  return {
    props: {
      uv,
    },
  };
}
export default Contact;
