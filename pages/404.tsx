import React from "react";
import ErrorComp from "components/ErrorComp/ErrorComp";

const NotFound = () => {
  return (
    <ErrorComp
      statusMessage={"404 - PAGE NOT FOUND "}
      description="The Page you are looking for might have been removed had its name changed or is temporarily unavailable. "
    />
  );
};

export default NotFound;
