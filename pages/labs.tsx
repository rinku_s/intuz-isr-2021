import HeadAndFootNew from "components/HeadAndFoot/HeadAndFootNew";
import gsap from "gsap";
import ScrollToPlugin from "gsap/dist/ScrollToPlugin";
import { getLabsData, getUniversal } from "lib/api";
import { GetStaticProps } from "next";
import React, { useEffect } from "react";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import SectionBanner from "../containers/LabsPage/SectionBanner";

gsap.registerPlugin(ScrollToPlugin);

const whiteAppleIcon = {
  src: "/static/Images/icons/ios-ic.svg",
  alt: "Available on IOS",
  variation: "circle_img",
};

const whiteAndroidIcon = {
  src: "/static/Images/icons/and-ic.svg",
  alt: "Available on android",
  variation: "circle_img",
};

const whiteAmazonIcon = {
  src: "/static/Images/icons/amazon-ic-white.svg",
  alt: "Available on amazon",
  variation: "circle_img",
};

const blackAmazonIcon = {
  src: "/static/Images/icons/amazon-ic-black.svg",
  alt: "Available on amazon",
  variation: "circle_img_black",
};

const blackOperaIcon = {
  src: "/static/Images/icons/Opera-black.svg",
  alt: "Available on Opera",
  variation: "circle_img_black",
};

const BlackAndroidIcon = {
  src: ("/static/Images/icons/and-ic-black.svg"),
  alt: "Available on android",
  variation: "circle_img_black",
};

const blackAppleIcon = {
  src: ("/static/Images/icons/ios-ic-black.svg"),
  alt: "Available on Ios",
  variation: "circle_img_black",
};

// function callback(evt){
//     // evt.preventDefault();
//     evt.stopPropagation();
//     var delta = evt.wheelDelta / 30 || -evt.detail;

//     let ofs = window.pageYOffset;
//     if(delta < -1){
//         if(window.pageYOffset < 4000) {
//             ofs = window.pageYOffset + 900;
//             // ofs = window.pageYOffset + 500;
//         } else{
//             ofs = window.pageYOffset + 500;
//             // ofs = window.pageYOffset + 500;
//         }
//     } else{
//         if(window.pageYOffset > 4000) {
//             ofs = window.pageYOffset - 600;
//             // ofs = window.pageYOffset - 500;
//         } else{
//             ofs = window.pageYOffset - 950;
//             // ofs = window.pageYOffset - 500;
//         }
//     }

//         let r = gsap.to(window, 1, {scrollTo:{y:ofs}});

//         if(r.isActive()){
//             r.pause();
//         } else{
//             r.resume();
//         }

//     return false;
// }

const Lab = ({ uv, data }) => {
  useEffect(() => {
    // if(window.innerWidth > 767) {
    //     document.body.style.overflowY = "hidden";
    //     window.addEventListener('mousewheel',  _.throttle(callback, 500));
    // }
    //     return () => {
    //         // document.body.style.overflowY = "visible";
    //         window.removeEventListener('mousewheel',  _.throttle(callback, 500));
    // };
  }, []);

  return (
    <HeadAndFootNew data={uv}>
      <Layout>
        {/* <Controller> */}
        {/* <Scene triggerHook={0} pin> */}
        {/* <div> */}
        <SectionBanner
          title="Calvertr2"
          description="Fastest unit converter & calculator"
          icons={[
            {
              ...whiteAppleIcon,
              link:
                "https://apps.apple.com/us/app/converter-unit-conversion/id1453497685",
            },
            {
              ...whiteAndroidIcon,
              link:
                "https://play.google.com/store/apps/details?id=com.r3app.calvertr2",
            },
          ]}
          buttonLabel="View More"
          buttonLink="/labs/calvertr2-ios"
          image={"Calvertr2.png"}
          backcolor="#3A3470"
          id="Calvertr2Block"
          variation="topBlock"
          isFirst={true}
        />
        {/* </div> */}
        {/* </Scene> */}
        {/* <Scene triggerHook={0} pin> */}
        {/* <div> */}

        <SectionBanner
          title="Wther Complete"
          description="Comprehensive weather forecasting from anywhere in the world"
          icons={[
            {
              ...blackAppleIcon,
              link:
                "https://apps.apple.com/us/app/wther-weather-forecast/id945337437",
            },
            {
              ...BlackAndroidIcon,
              link:
                "https://play.google.com/store/apps/details?id=com.r3app.wthrcomplete&hl=en",
            },
          ]}
          buttonLabel="View More"
          buttonLink="/labs/wther-complete"
          image={"cloud_computing_solutions_labs.png"}
          backcolor="#ffffff"
          variation="reverse-block"
          id="CompleteBlock"
        />
        {/* </div> */}
        {/* </Scene> */}

        {/* <Scene triggerHook={0} pin> */}
        {/* <div> */}
        <SectionBanner
          title="Alarmr"
          description="Enjoy beautiful mornings"
          icons={[
            {
              ...whiteAppleIcon,
              link:
                "https://apps.apple.com/us/app/alarmr-daily-alarm-clock/id850284623",
            },
            {
              ...whiteAndroidIcon,
              link:
                "https://play.google.com/store/apps/details?id=com.r3app.alarmr",
            },
          ]}
          buttonLabel="View More"
          buttonLink="/labs/alarmr"
          image={"alarm-r-dev.png"}
          backcolor="#4A90E2"
          id="AlarmrBlock"
        />
        {/* </div> */}
        {/* </Scene> */}

        {/* <Scene triggerHook={0} pin> */}
        {/* <div> */}

        <SectionBanner
          title="Wther"
          description="Your own personal weather station with most beautiful visualizations you've ever seen"
          icons={[
            {
              ...blackAppleIcon,
              link:
                "https://apps.apple.com/us/app/intuitive-weather-update/id868050791",
            },
            {
              ...BlackAndroidIcon,
              link:
                "https://play.google.com/store/apps/details?id=com.r3app.wthr",
            },
          ]}
          buttonLabel="View More"
          buttonLink="/labs/wther"
          image={"weater-c-dev.png"}
          backcolor="#ffffff"
          variation="reverse-block"
          id="WtherBlock"
        />
        {/* </div> */}
        {/* </Scene> */}

        {/* <Scene triggerHook={0} pin> */}
        {/* <div> */}

        <SectionBanner
          title="Calvertr"
          description="Built in intelligent calculator and unit converter"
          icons={[
            {
              ...whiteAppleIcon,
              link: "https://apps.apple.com/us/app/calvertr-lite/id938033085",
            },
            {
              ...whiteAndroidIcon,
              link:
                "https://play.google.com/store/apps/details?id=com.r3app.convertrlite",
            },
          ]}
          buttonLabel="View More"
          buttonLink="/labs/calvertr"
          image={"calvrter-dev.png"}
          backcolor="#072542"
          id="CalvertrBlock"
        />

        {/* </div> */}
        {/* </Scene> */}
        {/* <Scene triggerHook={0}> */}
        {/* <div> */}

        <SectionBanner
          style={{ zIndex: 9, position: "relative" }}
          title="iWeather"
          description="Most beautiful and simple free weather app with intuitive interface"
          icons={[
            {
              ...blackAppleIcon,
              link:
                "https://apps.apple.com/us/app/iweather-forecast/id823413326",
            },
            {
              ...BlackAndroidIcon,
              link:
                "https://play.google.com/store/apps/details?id=com.r3app.iweatherfree",
            },
          ]}
          buttonLabel="View More"
          buttonLink="/labs/iweather"
          image={"iweather-dev.png"}
          backcolor="#ffffff"
          variation="reverse-block"
          id="iWeatherBlock"
        />
        {/* </div> */}
        {/* </Scene> */}
        <LetsTalkComp />
        {/* </Controller> */}
      </Layout>
    </HeadAndFootNew>
  );
};


export const getStaticProps: GetStaticProps = async (context) => {


  let uv = await getUniversal('/labs');
  let data = await getLabsData("/labs");

  return {
    props: {
      uv,
      data,
    }
  }
}

export default Lab;
