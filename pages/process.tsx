import HeadAndFootNew from 'components/HeadAndFoot/HeadAndFootNew';
import NewBanner from 'containers/ServicePageBanners/bannerPage';
import { getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';
import Layout from '../components/Layout/Layout';
import LetsTalkComp from '../components/LetsTalkComp/LetsTalkComp';
import ProcessSection from '../containers/ProcessPage/ProcessSection/ProcessSection';
const process = ({ uv }) => {

  let bannerContent = {
    'title': 'App Development Process',
    'backImage': {
      'name': 'process-page-banner.jpg'
    },
    'subTitle': 'An Exemplary fusion of Waterfall, Scrum, and Kanban; just magical'
  }

  return (
    <HeadAndFootNew data={uv}>
      <Layout>
        {/* <ServicePageBanner content={bannerContent} /> */}
        <NewBanner title="App Development Process" backImage={"process-page-banner.jpg"} subTitle='An Exemplary fusion of Waterfall, Scrum, and Kanban; just magical' />

        <ProcessSection />
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  )
}

export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal('/process');

  return {
    props: {
      uv,
    }
  }
}

export default process

