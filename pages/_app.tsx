import "tailwindcss/tailwind.css";
// import "slick-carousel/slick/slick.css";
// import "slick-carousel/slick/slick-theme.css";
// import "../sass/slick-dots.css";
// import gsap from 'gsap'
import React, { useEffect, useState } from "react";
// import { Transition, TransitionGroup } from 'react-transition-group';
import "../sass/main.scss";
import "../sass/utilities.css";
import Cookies from "universal-cookie";
import PrivacyStrip from "components/PrivacyStrip/PrivacyStrip";
import "lazysizes";
import BurgerProvider from "context/burgerContext";
import Script from "next/script";
import ProcessingProvider from "context/processingContext";
const MyApp = ({ Component, pageProps }) => {
  const cookies = new Cookies();
  const [cookie, setCookie] = useState(false);

  useEffect(() => {
    // collectChat(window, document);
    let privacy = cookies.get("acceptprivacy") as boolean;
    setCookie(privacy);
  }, []);

  function collectChat(w, d) {
    console.log("Collect chat");

    w.CollectId = "5b7285c57254ec18beb0cd90";
    var h = d.head || d.getElementsByTagName("head")[0];
    var element = d.getElementById("chatLunchar");
    element && element.parentNode.removeChild(element);
    var s = d.createElement("script");
    s.setAttribute("type", "text/javascript");
    s.setAttribute("src", "https://collectcdn.com/launcher.js");
    s.setAttribute("id", "chatLunchar");
    h.appendChild(s);
  }
  const setPrivacy = () => {
    let currDate = new Date();
    cookies.set("acceptprivacy", true, {
      expires: new Date(
        currDate.getFullYear() + 1,
        currDate.getMonth(),
        currDate.getDate()
      ),
    });
    setCookie(true);
  };
  return (
    <ProcessingProvider>
      <BurgerProvider>
        <Script strategy="lazyOnload">
          {`
            (function(w, d) {
                w.CollectId = "5cb8725d5ee548701b81b874";
                var h = d.head || d.getElementsByTagName("head")[0];
                var element = d.getElementById("chatLunchar");
                element && element.parentNode.removeChild(element);
                var s = d.createElement("script");
                s.setAttribute("type", "text/javascript");
                s.setAttribute("src", "https://collectcdn.com/launcher.js");
                s.setAttribute("id", "chatLunchar");
                h.appendChild(s);
                
            })(window, document)
        `}
        </Script>
        <Script
          strategy="afterInteractive"
          src="https://www.googletagmanager.com/gtag/js?id=AW-962036523"
        />
        <Script
          strategy="afterInteractive"
          src="https://www.googletagmanager.com/gtag/js?id=UA-58136920-1"
        />
        <Component {...pageProps} />
        {!cookie && <PrivacyStrip onAccept={setPrivacy} />}
      </BurgerProvider>
    </ProcessingProvider>
  );
};

export default MyApp;
