import NewBanner from "containers/ServicePageBanners/bannerPage";
import { getAboutPageData, getUniversal } from "lib/api";
import { GetStaticProps } from "next";
import React from "react";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
// import IconGroup from '../components/IconGroup/IconGroup';
import CustomerValue from "../containers/AboutPage/SectionCustomerValue/CustomerValue";
import Ethics from "../containers/AboutPage/SectionEthics/Ethics";
import Mission from "../containers/AboutPage/SectionMission/Mission";
import NotablePoints from "../containers/AboutPage/SectionNotablePoints/NotablePoints";
import TextContent from "../containers/AboutPage/SectionTextContent/TextContent";
import TopTitle from "../containers/AboutPage/SectionTopTitle/TopTitle";
import SectionBreakPoint from "../containers/CommonSections/SectionBreakPoint/SectionBreakPoint";
import SectionResource from "../containers/HomePage/SectionResource/SectionResource";

const About = ({ uv, data }) => {
  return (
    <Layout>
      {/* <ServicePageBanner content={data.apppage.sectionbanner} /> */}
      <NewBanner
        title="About Us"
        backImage={"about-page-banner.jpg"}
        buttonLabel="Let’s Connect"
        link="/get-started"
        subTitle="Your True Development Partner for Advanced IT Services"
      />

      <TopTitle variation="topPadding">
        <TextContent>
          Intuz is the copper-bottomed software development company established
          in 2008. We are the industry leading mobile app development and cloud
          consulting company for high-end technology solutions. Our team
          consists of industry consultants with innovative minds, creative
          designers, and expert developers.
        </TextContent>
      </TopTitle>
      <TopTitle title="Mission Statement" borderBottom={true}>
        <TextContent>
          Achieve Growth by Inventing Incredible Value for Clients.
        </TextContent>
      </TopTitle>
      <Mission />

      <TopTitle title="Some notable points about Intuz">
        <NotablePoints>{data.apppage.sectiontopicondetails}</NotablePoints>
      </TopTitle>
      <TopTitle title="Customer Values">
        <CustomerValue />
      </TopTitle>
      <TopTitle title="Team">
        <TextContent>
          We’re a passionate team of application makers who breath, drink, eat,
          play, think and talk, in addition to design and code, the apps. Our
          passion for work reflects our values and produces a delightful
          experience for our clients. We delivered 1500+ apps in various
          industries and 40+ countries, world wide. The unmatched diversity of
          verticals and impeccable experience make us the best team to overcome
          any project development challenge. Our team thoroughly listens,
          carefully understands and meticulously produces. The seamless
          integration of our values makes us an empathic partner for our
          clients. The experience is always nonpareil.
        </TextContent>
      </TopTitle>
      <TopTitle
        title="Work Ethics"
        backImage="our_values_bg.jpg"
        variation="topPadding"
      >
        <Ethics content={data.sectionethics} />
      </TopTitle>
      <SectionBreakPoint
        content={data.apppage.sectionbreakpoints}
        variation="addspace"
      />
      <SectionResource sectionresources={data.guides} />
      <LetsTalkComp />
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal("/about");
  //context.params.name
  let data = await getAboutPageData();

  return {
    props: {
      uv,
      data,
    },
  };
};

export default About;
