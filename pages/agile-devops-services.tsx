// import gql from "graphql-tag";
import React from "react";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
// import QueryComp from "../components/Query/QueryComp";
// import { withApollo } from "../config/apollo";
import WhatWeDoAgile from "../containers/AgileDevPage/WhatWeDoAgileSection/WhatWeDoAgile";
import AwardsAndRecognition from "../containers/CommonSections/AwardandRecognition/AwardsAndRecognition";
import ClientTestimonialCarousel from "../containers/CommonSections/ClientTestimonialCarousel/ClientTestimonialCarousel";
import CTAStripe from "../containers/CommonSections/CTAStripe/CTAStripe";
import EngagementModelSection from "../containers/CommonSections/EngagementModel/EngagementModelSection";
import FactsandAwards from "../containers/CommonSections/FactsandAwards/FactsandAwards";
import TrustedSection from "../containers/CommonSections/TrustedSection/TrustedSection";
import SectionBanner from "../containers/Nodejs-dev-page/SectionBanner/SectionBanner";
import { getUniversal, getAgileDevopsServicesPageData } from "../lib/api";
import HeadAndFootNew from "../components/HeadAndFoot/HeadAndFootNew";
import ApplicationPageBanner from "containers/ApplicationPage/BannerSection/BannerSection";
// export const allQuery = gql`
//   query($id: ID!) {
//     apppage(id: $id) {
//       sectionbanner {
//         id
//         title
//         subTitle
//         description
//         backImage {
//           name
//           hash
//         }
//         buttonLabel
//         buttonLink
//       }
//     }
//     whatwedoagiles {
//       title
//       description
//       image {
//         name
//       }
//     }

//     factandawards {
//       image {
//         name
//       }
//       caption
//     }

//     testimonialcarouselcontents {
//       id
//       name
//       app_image {
//         name
//       }
//       client_image {
//         name
//       }
//       designation
//       description
//     }

//     awardandrecognitions {
//       caption
//       image {
//         name
//       }
//       description
//     }
//   }
// `;

const AgileDev = ({ data, uv }) => {
  return (
    <HeadAndFootNew data={uv}>
      <Layout>
        {/* <SectionBanner
          content={data.apppage.sectionbanner}
          variation="devopes"
        /> */}
        <ApplicationPageBanner
          content={data.apppage.sectionbanner}
          variation="devopes"
        />
        <WhatWeDoAgile whatwedoagiles={data.whatwedoagiles} />
        <FactsandAwards factandawards={data.factandawards} />
        <EngagementModelSection description="Our flexible engagement models to cover all your business needs without long-term contracts and deal breaking pricing." />
        <CTAStripe
          link="get-started"
          title="Have a Project in Mind?"
          LinkButtontxt="Let's Discuss"
        />
        <ClientTestimonialCarousel content={data.testimonialcarouselcontents} />
        <TrustedSection />
        <AwardsAndRecognition
          awardandrecognitions={data.awardandrecognitions}
        />
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};
export async function getStaticProps(context) {
  let uv = await getUniversal("/agile-devops-services");
  let data = await getAgileDevopsServicesPageData();

  return {
    props: {
      uv,
      data,
    },
  };
}
export default AgileDev;
