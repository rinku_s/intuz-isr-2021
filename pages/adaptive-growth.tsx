// import gql from 'graphql-tag';
import React from "react";
import HeadAndFootNew from "../components/HeadAndFoot/HeadAndFootNew";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
// import QueryComp from "../components/Query/QueryComp";
// import { withApollo } from "../config/apollo";
import ExpectAdaptiveGrowth from "../containers/AdaptiveGrowth/SectionExpectAdaptiveGrowth/ExpectAdaptiveGrowth";
import TextContent from "../containers/AdaptiveGrowth/SectionTextContent/TextContent";
import TopTitle from "../containers/AdaptiveGrowth/SectionTopTitle/TopTitle";
import ServicePageBanner from "../containers/ServicePageBanners/ServicePageBanner";
import { getAdaptivegrowthPageData, getUniversal } from "../lib/api";

// export const allQuery = gql`
// query ($id:ID!){
//     apppage(id:$id){
//         sectionbanner {
//             title
//             subTitle
//             backImage {
//                 name
//                 hash
//             }
//             buttonLabel
//             buttonLink
//         }
//     }
//     sectiongrowthexpectations {
//         title
//         description
//     }
//   }
// `;

const AdaptiveGrowth = ({ data, uv }) => {
  return (
    <HeadAndFootNew data={uv}>
      <Layout>
        <ServicePageBanner content={data.apppage.sectionbanner} />
        <TopTitle title="Solution Driven Approach" variation="topMargin">
          <TextContent>
            In place of adopting any standardized approach for a product
            development or enhancement, Intuz offers customized treatment for
            each project. Opt for task-based engagement model or project-based
            engagement model as per your tailored business demand!
          </TextContent>
        </TopTitle>
        <TopTitle title="Collective Team Efforts">
          <TextContent image="collective-team-efforts.png">
            Intuz follows a well-designed task-based engagement model that helps
            you in exploring your project-based possibilities. To improve
            time-to-market of the product, we set up a seamless collaboration
            thread with your internal team. It helps us in understanding your
            business operations quickly.
          </TextContent>
        </TopTitle>
        <TopTitle title="Scale On-Demand">
          <TextContent>
            We enable you to ramp up or down the resources. We are always ready
            to accept your evolving needs and accordingly scale up or down your
            project scopes. Intuz maintains transparency throughout the project
            development by allowing you to track project progress at any point
            in time.
          </TextContent>
        </TopTitle>
        <TopTitle title="Exclusive Business Models">
          <TextContent image="business-models.png">
            Fixed Cost, Dedicated Hiring, or Time & Material – go for the most
            suitable business model that helps you in attaining your ultimate
            organizational goals. Selecting a right business model, you can
            easily control your IT resources and generate the best output in
            every phase of project development.
          </TextContent>
        </TopTitle>
        <ExpectAdaptiveGrowth>
          {data.sectiongrowthexpectations}
        </ExpectAdaptiveGrowth>
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};
export async function getStaticProps(context) {
  console.log(context);
  let uv = await getUniversal("/adaptive-growth");
  let data = await getAdaptivegrowthPageData();

  return {
    props: {
      uv,
      data,
    },
  };
}
export default AdaptiveGrowth;
