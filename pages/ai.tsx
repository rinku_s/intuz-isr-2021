import { getAIPageData, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';
import Layout from '../components/Layout/Layout';
import LetsTalkComp from '../components/LetsTalkComp/LetsTalkComp';
import { Hero } from '../containers/AIPage/Hero/Hero';
import SectionFive from '../containers/AIPage/SectionFive/SectionFive';
import SectionFour from '../containers/AIPage/SectionFour/SectionFour';
import SectionThree from '../containers/AIPage/SectionThree/SectionThree';
import { SectionTwo } from '../containers/AIPage/SectionTwo/SectionTwo';

const ai = ({ uv, data }) => {
  return (

    <Layout whiteBackground>
      <Hero />
      <SectionTwo />
      <SectionThree offerings={data.aiofferings} />
      <SectionFour content={data.aisectionfours} />
      <SectionFive innovations={data.aiinnovations} />
      <LetsTalkComp />
    </Layout>

  )
}
export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal('/ai');
  let data = await getAIPageData();

  return {
    props: {
      uv,
      data
    }
  }
}
export default ai;