import HeadAndFootNew from "components/HeadAndFoot/HeadAndFootNew";
import LoadingScreen from "components/LoadingScreen/LoadingScreen";
import { getBlogCategoryData, getUniversal } from "lib/api";
import {
  getAllCategories,
  getBlogsForCategory,
  getCategory,
} from "lib/api/blog";
import { GetStaticPaths, GetStaticProps } from "next";
import { useRouter } from "next/router";
import React from "react";
import BlogCategory from "../../../components/BlogCategory/BlogCategoryMenu";
import CategoriesNav from "../../../components/CategoriesNav/CategoriesNav";
import Layout from "../../../components/Layout/Layout";
import LetsTalkComp from "../../../components/LetsTalkComp/LetsTalkComp";
import BlogFilterSection from "../../../containers/BlogFilterSection/BlogFilterSection";
import HeroSectionResources from "../../../containers/ResourcesPage/HeroSectionResources/HeroSectionResources";

// import cf from '../../config/disqus';

const CategoryBlog = ({ uv, categoryData, blogs, allCategoryList }) => {
  const router = useRouter();
  if (router.isFallback) return <LoadingScreen />;
  console.log(allCategoryList);

  return (
    <HeadAndFootNew data={uv}>
      <Layout>
        <HeroSectionResources
          backImage={categoryData?.blogCategories[0].banner.name}
          heading={categoryData?.blogCategories[0].name}
          subtitle={categoryData?.blogCategories[0].subTitle}
          breadcrumb={{
            label: categoryData?.blogCategories[0].name,
            link: `/blog/category/${categoryData?.blogCategories[0].link}`,
          }}
        />
        <CategoriesNav categories={allCategoryList?.blogCategories} />
        {/* <BlogCategory categories={data.blogCategories} /> */}
        <BlogFilterSection blogs={blogs.blogs} />
        {/* <Subscription /> */}
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal(`/blog/category/${context.params.name}`);
  let categoryData = (await getCategory(context.params.name as string)) as any;
  let blogs = await getBlogsForCategory(context.params.name as string, 100);
  let allCategoryList = await getAllCategories();
  if (categoryData?.blogCategories?.length === 0) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      uv,
      categoryData,
      allCategoryList,
      blogs,
    },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [{ params: { name: "product-development" } }],
    fallback: true,
  };
};

export default CategoryBlog;
