// import cf from '../../config/disqus';
import LoadingScreen from 'components/LoadingScreen/LoadingScreen';
import ReactHtmlParser from 'html-react-parser';
import { getBlogPageData } from 'lib/api';
import { GetStaticPaths, GetStaticProps } from 'next';
import Head from 'next/head';
import { useRouter } from 'next/router';
import React from 'react';
import Layout from '../../components/Layout/Layout';
import LetsTalkComp from '../../components/LetsTalkComp/LetsTalkComp';
import NewBlog from '../../components/NewBlog/NewBlog';
import ShareButton from '../../components/ShareButton/ShareButton';
import { cdn } from '../../config/cdn';
import AuthorSection from '../../containers/Blog/AuthorSection/AuthorSection';
// import ResourcesSectionStyle from '../../components/ReusableSectionStyle/ResourceSection';
import HeroSection from '../../containers/Blog/HeroSection';
import SectionResource from '../../containers/HomePage/SectionResource/SectionResource';


const FindHireRemoteDeveloper = ({ uv, data }) => {
  const router = useRouter();

  if (router.isFallback) return <LoadingScreen />

  let authorData = null;
  if (data.blogs[0].blog_author) {
    authorData = { 'name': data.blogs[0].blog_author.name, 'date': data.blogs[0].updatedAt, 'image': data.blogs[0].blog_author.image.name }
  }

  return (


    // if(data.blogs.length  <= 0 ) return <Error statusCode={404}/>

    <Layout>
      <Head>
        {ReactHtmlParser(`${data.blogs[0].head}`)}
        {data.blogs[0].metaTitle && <title>{data.blogs[0].metaTitle}</title>}
        {data.blogs[0].metaDescription && <meta name="description" content={data.blogs[0].metaDescription} />}
        {data.blogs[0].metaTitle ? (
          <>
            <meta property="og:locale" content="en_US" />
            <meta property="og:type" content="website" />
            <meta property="og:title" content={data.blogs[0].metaTitle} />
            <meta property="og:description" content={data.blogs[0].metaDescription} />
            <meta property="og:site_name" content="Intuz" />
            <meta property="og:image:type" content="image/png" />
            <meta property="og:image:alt" content={data.blogs[0].metaTitle} />
            <meta property="og:image" content={cdn(data.blogs[0].socialImage.name + '?auto=format,compress')} />
            <meta name="twitter:card" content="summary_large_image" />
            <meta name="twitter:description" content={data.blogs[0].metaDescription} />
            <meta name="twitter:title" content={data.blogs[0].metaTitle} />
            <meta name="twitter:site" content="@IntuzHQ" />
            <meta name="twitter:creator" content="@IntuzHQ" />
            <meta name="twitter:image:width" content="600" />
            <meta name="twitter:image:height" content="600" />
            <meta name="twitter:image" content={cdn(data.blogs[0].socialImage.name + '?auto=format,compress')} />
            <meta name="referrer" content="origin-when-cross-origin" />
            <meta name="referrer" content="origin" />
          </>
        ) : ''}
      </Head>

      <HeroSection
        link={data.blogs[0].blog_category.length > 0 ? data.blogs[0].blog_category[0].link : ''}
        blogCategory={data.blogs[0].blog_category.length > 0 ? data.blogs[0].blog_category[0].name : ''}
        backImage={cdn(data.blogs[0].banner.name)}
        heading={data.blogs[0].title}
        subtitle={data.blogs[0].subtitle}
        authorData={authorData}
      ></HeroSection>

      <article style={{ position: "relative" }} id="blog">
        <ShareButton url={`https://www.intuz.com/blog/${router.query.name}`} title={data.blogs[0].title} media={cdn(data.blogs[0].banner.name)} />
        <NewBlog link={router.query.name}>
          {ReactHtmlParser(`${data.blogs[0].blog}`)}
        </NewBlog>
      </article>

      { data.blogs[0].blog_author ? <AuthorSection author={data.blogs[0].blog_author} /> : ''}

      {/* <div id="disqus" className="container">
              <DiscussionEmbed shortname="intuz-1" config={{url:"https://www.intuz.com/"+router.query.name, identifier:data.blogs[0].title, title:data.blogs[0].title}} />
            </div> */}

      {data.blogs[0].moreBlogs.length > 0 ? <SectionResource sectionresources={data.blogs[0].moreBlogs} changeUI={true} /> : ''}
      {/* <Subscription/> */}
      <LetsTalkComp />
      {data.blogs[0].scripts ? <div dangerouslySetInnerHTML={{ __html: data.blogs[0].scripts }} ></div> : ''}
    </Layout>


  )

}

export const getStaticProps: GetStaticProps = async (context) => {

  let data = await getBlogPageData(context.params.name) as any;
  if (data.blogs.length === 0) {
    return {
      notFound: true
    }
  }

  return {
    props: {
      data
    }
  }
}


export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [
      { params: { name: "iot-solutions-to-overcome-construction-labour-shortages" } }
    ],
    fallback: true
  }
}

export default FindHireRemoteDeveloper
