import {
  getBlogsForCategory,
  getLatestBlogs,
  getAllCategories,
} from "lib/api/blog";
import { getLatestGuides } from "lib/api/guide";
import React from "react";
import BlogCategoryMenu from "../../components/BlogCategory/BlogCategoryMenu";
import HeadAndFootNew from "../../components/HeadAndFoot/HeadAndFootNew";
import Layout from "../../components/Layout/Layout";
import LetsTalkComp from "../../components/LetsTalkComp/LetsTalkComp";
import MultiBlogBlock from "../../components/MultiBlogBlock/MultiBlogBlock";
import MultiGuideBlock from "../../components/MultiGuideBlock/MultiGuideBlock";
import HeroSectionResources from "../../containers/ResourcesPage/HeroSectionResources/HeroSectionResources";
import { getBlogData, getUniversal } from "../../lib/api";
import CategoriesNav from "components/CategoriesNav/CategoriesNav";
import { CategoryListModel } from "lib/api/CategoryListModel";
const resources: React.FC<{
  uv: any;
  latestBlog: any;
  latestGuides: any;
  mobile: any;
  product: any;
  iot: any;
  allCategoryList: CategoryListModel["data"];
}> = ({
  uv,
  latestBlog,
  latestGuides,
  mobile,
  product,
  iot,
  allCategoryList,
}) => {
  console.log(allCategoryList);

  return (
    <HeadAndFootNew data={uv}>
      <Layout>
        <HeroSectionResources
          backImage="blog-page-banner.jpg"
          heading="The Intuz Blog"
          subtitle="Innovation & Technology For Profitability"
        />
        <CategoriesNav categories={allCategoryList.blogCategories} />
        <MultiBlogBlock blogs={latestBlog.blogs} title="Latest" />
        <MultiGuideBlock guides={latestGuides.guides} />
        <MultiBlogBlock
          blogs={mobile.blogs}
          title={"Mobile Development"}
          link={"mobile"}
        />
        <MultiBlogBlock
          blogs={product.blogs}
          title={"Product Development"}
          link={"product-development"}
        />
        <MultiBlogBlock
          blogs={iot.blogs}
          title={"Internet of Things"}
          link={"internet-of-things"}
        />
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};
export async function getStaticProps(context) {
  let uv = await getUniversal("/blog");
  let latestBlog = await getLatestBlogs();
  let latestGuides = await getLatestGuides();
  let allCategoryList = await getAllCategories();
  let mobile = await getBlogsForCategory("mobile", 4);
  let product = await getBlogsForCategory("product-development", 4);
  let iot = await getBlogsForCategory("internet-of-things", 4);

  return {
    props: {
      uv,
      latestBlog,
      latestGuides,
      allCategoryList,
      mobile,
      product,
      iot,
    },
  };
}
export default resources;
