import LoadingScreen from "components/LoadingScreen/LoadingScreen";
import { getUniversal } from "lib/api";
import { getAllCategories, getBlogAuthorData } from "lib/api/blog";
import { GetStaticPaths, GetStaticProps } from "next";
import { useRouter } from "next/router";
import React from "react";
import CategoriesNav from "../../../components/CategoriesNav/CategoriesNav";
import Layout from "../../../components/Layout/Layout";
import LetsTalkComp from "../../../components/LetsTalkComp/LetsTalkComp";
import BannerSection from "../../../containers/BlockAuthor/BannerSection";
import BlogFilterSection from "../../../containers/BlogFilterSection/BlogFilterSection";

const AuthorBlog = ({ uv, data, allCategoryList }) => {
  const router = useRouter();
  if (router.isFallback) return <LoadingScreen />;

  return (
    <Layout whiteBackground>
      <BannerSection
        {...data.blogAuthors[0]}
        breadcrumb={{
          label: data.blogAuthors[0].name,
          link: `/blog/author/${data.blogAuthors[0].link}`,
        }}
      />
      <CategoriesNav categories={allCategoryList?.blogCategories} />
      {/* <BlogCategoryMenu categories={data.blogCategories} /> */}
      <BlogFilterSection blogs={data.blogAuthors[0].blog} blogAuthor={true} />
      {/* <Subscription /> */}
      <LetsTalkComp />
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal(`/blog/author/${context.params.name}`);
  //context.params.name
  let data = (await getBlogAuthorData(context.params.name as string)) as any;
  let allCategoryList = await getAllCategories();
  if (data.blogAuthors.length === 0) {
    return {
      notFound: true,
    };
  }

  return {
    props: {
      uv,
      data,
      allCategoryList,
    },
  };
};

export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [{ params: { name: "nilay-d" } }],
    fallback: true,
  };
};

export default AuthorBlog;
