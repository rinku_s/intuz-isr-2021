import React from "react";
import HeadAndFootNew from "../components/HeadAndFoot/HeadAndFootNew";
import Layout from "../components/Layout/Layout";
import LightHouseContainer from "../containers/Lighthouse/index";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import { getNewCareerData, getUniversal } from "../lib/api";
import gsap from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { useEffect } from 'react';
gsap.registerPlugin(ScrollTrigger);
gsap.registerEffect({
    name:'fade',
    extendTimeline: true,
    effect:(target,config) => {
        return gsap.from(target, { y:config.y, autoAlpha:0, ease:"power1.out", duration:config.duration });
    },
    defaults: {duration: 0.5, y: 30},
})
const Lighthouse = ({ uv, data }) => {
  useEffect(() => {

      return () => {
          ScrollTrigger.getAll().forEach((instance) => {
              instance.kill();
            });
            gsap.killTweensOf(window);
      }
  }, [])

  return (
    <HeadAndFootNew data={uv}>
      <Layout nunito={true} whiteBackground>
        <LightHouseContainer />
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};

export async function getStaticProps(context) {
  let uv = await getUniversal("/jam-stack-development");
  let data = await getNewCareerData();

  return {
    props: {
      uv,
      data,
    },
  };
}

export default Lighthouse;
