import React from "react";
import HeadAndFootNew from "../components/HeadAndFoot/HeadAndFootNew";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import SectionAwards from "../containers/Aawards-Associations/SectionAwards/SectionAwards";
import ServicePageBanner from "../containers/ServicePageBanners/ServicePageBanner";
import { getUniversal } from "../lib/api";

const Award = ({ uv }) => {
  let bannerContent = {
    title: "Accreditations and Associations",
    backImage: {
      name: "awardspage-banner.jpg",
    },
  };

  return (
    <HeadAndFootNew data={uv}>
      <Layout>
        <ServicePageBanner content={bannerContent} />
        <SectionAwards />
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};
export async function getStaticProps(context) {
  console.log(context);
  let uv = await getUniversal("/awards-associations");
  return {
    props: {
      uv,
    },
  };
}
export default Award;
