import { getPrivacyPageData, getUniversal } from 'lib/api'
import { GetStaticProps } from 'next'
import React from 'react'
import Layout from '../components/Layout/Layout'
import PrivacyPage from '../containers/PrivacyPage/PrivacyPage'


const privacy = ({ uv, data }) => {
    return (

        <Layout whiteBackground>
            <PrivacyPage title={data.privacypolicies[0].title} content={data.privacypolicies[0].Content} />
        </Layout>

    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    // console.log(context);
    let uv = await getUniversal('/partner-with-us');
    let data = await getPrivacyPageData();

    return {
        props: {
            uv,
            data
        }
    }
}

export default privacy
