import { getSitemapPageData, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';
import Layout from '../components/Layout/Layout';
import Sitemap from '../containers/Sitemap/Sitemap';


const sitemap = ({ uv, data }) => {
    return (

        <Layout whiteBackground>
            <Sitemap content={data.pagetypes} />
        </Layout>

    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    // console.log(context);
    let uv = await getUniversal('/sitemap');
    let data = await getSitemapPageData();

    return {
        props: {
            uv,
            data
        }
    }
}

export default sitemap;
