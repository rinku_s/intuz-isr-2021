import LoadingScreen from 'components/LoadingScreen/LoadingScreen';
import gql from 'graphql-tag';
import ReactHtmlParser from 'html-react-parser';
import { callAPI } from 'lib/api';
import { GetStaticPaths, GetStaticProps } from 'next';
import Head from 'next/head';
import { useRouter } from 'next/router';
import React from 'react';
import Layout from '../../components/Layout/Layout';
import LetsTalkComp from '../../components/LetsTalkComp/LetsTalkComp';
import NewBlog from '../../components/NewBlog/NewBlog';
import ResourcesSectionStyle from '../../components/ReusableSectionStyle/ResourceSection';
import ShareButton from '../../components/ShareButton/ShareButton';
import { cdn } from '../../config/cdn';
import SectionResource from '../../containers/HomePage/SectionResource/SectionResource';



const Guides = ({ data }) => {
  const router = useRouter();

  if (router.isFallback) return <LoadingScreen />

  return (
    <Layout>
      <Head>
        {ReactHtmlParser(`${data.guides[0].head}`)}
      </Head>
      <ResourcesSectionStyle
        backImage={cdn(data.guides[0].banner.name)}
        heading={data.guides[0].title}
        subtitle={data.guides[0].subtitle}
      ></ResourcesSectionStyle>
      <article style={{ position: "relative" }} id="blog">
        <ShareButton url={`https://www.intuz.com/${router.query.guides}`} title={data.guides[0].title} media={cdn(data.guides[0].banner.name)} />
        <NewBlog link={router.query.guides}>
          {ReactHtmlParser(data.guides[0].blog)}
        </NewBlog>
      </article>

      {data.guides[0].more_blogs.length > 0 ? <SectionResource sectionresources={data.guides[0].more_blogs} /> : ''}

      <LetsTalkComp />
    </Layout>
  )


}

export const getStaticProps: GetStaticProps = async (ctx) => {

  const allQuery = gql`
  query {
      guides(where:{
          link:"${ctx.params.guides}"
      }){
          blog
          title
          id
          subtitle
          banner{
          name
          }
          head
          more_blogs(limit:3) {
            id
            title
            image{
              name
            }
            link
          }          
      }
    }
  `;


  const data = await callAPI(allQuery) as any;
  if (data.guides.length === 0) {
    return {
      notFound: true
    }
  }
  return {
    props: {
      data: data
    }
  }
}

export const getStaticPaths: GetStaticPaths = async () => {
  return {
    paths: [
      { params: { guides: "guide-to-full-stack-development" } }
    ],
    fallback: true
  }
}

export default Guides

