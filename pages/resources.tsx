import gql from "graphql-tag";
import React from "react";
import HeadAndFootNew from "../components/HeadAndFoot/HeadAndFootNew";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import HeroSectionResources from "../containers/ResourcesPage/HeroSectionResources/HeroSectionResources";
import ResourceArticle from "../containers/ResourcesPage/ResourceArticle/ResourceArticle";
import { getGuideData, getUniversal } from "../lib/api";
const query = gql`
  query {
    guides(sort: "updatedAt:desc", where: { ready: true }) {
      id
      title
      description
      image {
        name
      }
      link
    }
  }
`;

const resources = ({ uv, data }) => {
  return (
    <HeadAndFootNew data={uv}>
      <Layout>
        <HeroSectionResources
          backImage="resourcepage-topbanner.jpg"
          heading="Intuz Resources"
          subtitle="Using Innovation & Technology For Profitability"
        />
        <ResourceArticle resources={data.guides} />
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};

export async function getStaticProps(ctx) {
  let uv = await getUniversal("/resources");
  let data = await getGuideData();

  return {
    props: {
      uv,
      data,
    },
  };
}

export default resources;
