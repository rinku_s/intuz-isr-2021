import { getKnowledgeBaseData, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';
import Layout from '../../components/Layout/Layout';
// import ResourceArticle from '../../containers/ResourcesPage/ResourceArticle/ResourceArticle';
import LetsTalkComp from '../../components/LetsTalkComp/LetsTalkComp';
import KnowledgeArticle from '../../containers/KnowledBasePage/KnowlegeBaseArticle/KnowledgeArticle';
import HeroSectionResources from '../../containers/ResourcesPage/HeroSectionResources/HeroSectionResources';

const resources = ({ uv, data }) => {
  return (
    <Layout>
      <HeroSectionResources
        backImage='resourcepage-topbanner.jpg'
        heading="Intuz Knowledge Base"
        subtitle="Terminologies for commonly searched terms"

      />
      <KnowledgeArticle resources={data.knowledgeBases} knowledge={true} />
      <LetsTalkComp />
    </Layout>

  )
}

export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal('/knowledge-base');
  //context.params.name
  let data = await getKnowledgeBaseData();

  return {
    props: {
      uv,
      data
    }
  }
}

export default resources
