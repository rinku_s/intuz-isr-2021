// import cf from '../../config/disqus';
import LoadingScreen from 'components/LoadingScreen/LoadingScreen';
import { getSlugData } from 'lib/api';
import { GetStaticPaths, GetStaticProps } from 'next';
import Head from 'next/head';
import { useRouter } from 'next/router';
import React from 'react';
import Layout from '../../components/Layout/Layout';
import LetsTalkComp from '../../components/LetsTalkComp/LetsTalkComp';
import { cdn } from '../../config/cdn';
import KBBanner from '../../containers/KnowledBasePage/KnowledgeBaseBanner/KbBanner';
import Terminologies from '../../containers/KnowledBasePage/Terminology/Terminologies';


const FindHireRemoteDeveloper = ({ uv, data, slug }) => {
  let router = useRouter();

  if (router.isFallback) return <LoadingScreen />
  return (
    <Layout>
      <Head>
        {data.knowledgeBases[0].meta_title && <title>{data.knowledgeBases[0].meta_title}</title>}
        {data.knowledgeBases[0].meta_description && <meta name="description" content={data.knowledgeBases[0].meta_description} />}

        <meta property="og:locale" content="en_US" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content={data.knowledgeBases[0].meta_title} />
        <meta property="og:description" content={data.knowledgeBases[0].meta_description} />
        <meta property="og:site_name" content="Intuz" />
        <meta property="og:image:type" content="image/png" />
        <meta property="og:image:alt" content={data.knowledgeBases[0].meta_title} />
        <meta property="og:image" content={cdn(data.knowledgeBases[0].social_image.name + '?auto=format,compress')} />
        <meta name="twitter:card" content="summary_large_image" />
        <meta name="twitter:description" content={data.knowledgeBases[0].meta_description} />
        <meta name="twitter:title" content={data.knowledgeBases[0].meta_title} />
        <meta name="twitter:site" content="@IntuzHQ" />
        <meta name="twitter:creator" content="@IntuzHQ" />
        <meta name="twitter:image:width" content="600" />
        <meta name="twitter:image:height" content="600" />
        <meta name="twitter:image" content={cdn(data.knowledgeBases[0].social_image.name + '?auto=format,compress')} />
        <meta name="referrer" content="origin-when-cross-origin" />
        <meta name="referrer" content="origin" />
      </Head>

      <KBBanner heading={data.knowledgeBases[0].heading} backImage={cdn(data.knowledgeBases[0].banner_image.name)} />

      <article style={{ position: "relative" }} id="knowledge">
        <Terminologies slug={slug} terminologies={data.knowledgeBases[0].terminologies} totalTerms={data.terminologiesConnection.aggregate.count} />
      </article>


      {/* <Subscription/> */}
      <LetsTalkComp />
    </Layout>
  )

}

export const getStaticProps: GetStaticProps = async (context) => {
  //context.params.name
  let data = await getSlugData(context.params.slug) as any;
  if (data.knowledgeBases.length === 0) {
    return {
      notFound: true
    }
  }
  let slug = context.params.slug;
  return {
    props: {
      data,
      slug
    }
  }
}

export const getStaticPaths: GetStaticPaths = () => {
  return {
    paths: [
      { params: { slug: "mobile-development-terms" } }
    ],
    fallback: true
  }
}

export default FindHireRemoteDeveloper
