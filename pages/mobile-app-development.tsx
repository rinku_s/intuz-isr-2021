import { getMobileAppData, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';
import Layout from '../components/Layout/Layout';
import LetsTalkComp from '../components/LetsTalkComp/LetsTalkComp';
import AwardsAndRecognition from '../containers/CommonSections/AwardandRecognition/AwardsAndRecognition';
import FactsandAwards from '../containers/CommonSections/FactsandAwards/FactsandAwards';
import FeturedAppCarouselSection from '../containers/CommonSections/FeaturedAppCaourselSection/FeturedAppCarouselSection';
import TrustedSection from '../containers/CommonSections/TrustedSection/TrustedSection';
import EstimateCta from '../containers/MobileAppDevPage/EstimateCTA/EstimateCta';
import HeroSection from '../containers/MobileAppDevPage/HeroSection/HeroSection';
import WhatWeDoMoble from '../containers/MobileAppDevPage/WhatWeDoMobile/WhatWeDoMoble';

const MobileAppDev = ({ uv, data }) => {


  return (

    <Layout>
      <HeroSection />
      <TrustedSection />
      <WhatWeDoMoble whatwedomobiles={data.whatwedomobiles} />
      <FactsandAwards factandawards={data.factandawards} />
      <FeturedAppCarouselSection />
      <EstimateCta />
      <AwardsAndRecognition awardandrecognitions={data.awardandrecognitions} />
      <LetsTalkComp />
    </Layout>

  )
}
export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal('/custom-app-development');
  let data = await getMobileAppData();

  return {
    props: {
      uv,
      data
    }
  }
}

export default MobileAppDev
