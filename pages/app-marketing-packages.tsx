import React from "react";
import HeadAndFootNew from "../components/HeadAndFoot/HeadAndFootNew";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import FeatureBlock from "../containers/AppMarketingPackage/SectionFeatureBlock/FeatureBlock";
import SectionPriceBlock from "../containers/AppMarketingPackage/SectionPriceBlock/SectionPriceBlock";
import AppDetail from "../containers/EnterprisePage/SectionAppDetail/AppDetail";
import ServicePageBanner from "../containers/ServicePageBanners/ServicePageBanner";
import { getAppMarketingPackagesPageData, getUniversal } from "../lib/api";

const appMarketing = ({ data, uv }) => {
  return (
    <HeadAndFootNew data={uv}>
      <Layout>
        <ServicePageBanner content={data.apppage.sectionbanner} />
        <FeatureBlock />
        <SectionPriceBlock>
          <AppDetail variation="priceblock">
            {data.apppage.sectiontopicondetails}
          </AppDetail>
        </SectionPriceBlock>
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};
export async function getStaticProps(context) {
  console.log(context);
  let uv = await getUniversal("/app-marketing-packages");
  let data = await getAppMarketingPackagesPageData();

  return {
    props: {
      uv,
      data,
    },
  };
}

export default appMarketing;
