
import { getTaxiAppData, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';
import FaqComponent from '../components/FAQComponent/FaqComponent';
import FaqSchema from '../components/FAQComponent/FaqSchema';
import Layout from '../components/Layout/Layout';
import LetsTalkComp from '../components/LetsTalkComp/LetsTalkComp';
import SectionBreakPoint from '../containers/CommonSections/SectionBreakPoint/SectionBreakPoint';
import ServicePageBanner from '../containers/ServicePageBanners/ServicePageBanner';
import SectionCustomApp from '../containers/TaxiAppPage/SectionCustomApp/SectionCustomApp';
import SectionProject from '../containers/TaxiAppPage/SectionProject/SectionProject';
import SectionSolution from '../containers/TaxiAppPage/SectionSolution/SectionSolution';

const TaxiappDev = ({ uv, data }) => {
    return (
        <Layout>
            <ServicePageBanner content={data.apppage.sectionbanner} form={{ subject: "Taxi App Development Quote Request", leadsource: "Taxi App - Popup", thankyoupage: "taxi-app-development-thankyou" }} />
            <SectionCustomApp content={data.apppage.featuresection} />
            <SectionSolution
                title="SOLUTION OVERVIEW"
                subtitle="A comprehensive taxi app development solution that caters to the needs of all the stakeholders involved. Our solution comes with a host of features for passengers, drivers, admins and a set of advanced features."
                tabTitles={data.apppage.sectiontabtitles}
                backColor='#fafafa'
            />
            <SectionBreakPoint content={data.apppage.sectionbreakpoints} />
            <SectionProject
                title="Projects"
                subtitle="Below are a few custom taxi app development solutions we had the opportunity to work upon."
                sectionrecentworks={data.apppage.sectionpagehalfblocks} />
            <FaqComponent faqs={data.apppage.faqs[0].Faqs} />
            <LetsTalkComp />
            <FaqSchema faqs={data.apppage.faqs[0].Faqs} />
        </Layout>
    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    // console.log(context);
    let uv = await getUniversal('/taxi-app-development');
    let data = await getTaxiAppData();

    return {
        props: {
            uv,
            data
        }
    }
}

export default TaxiappDev

