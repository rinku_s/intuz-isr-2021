//@ts-nocheck
import Protection from 'containers/WhayusPage/IPProtection/Protection';
import Process from 'containers/WhayusPage/Process/Process';
import { getUniversal, getWhyUsData } from 'lib/api';
import { GetStaticProps } from 'next';
import React, { Component } from 'react';
import Layout from '../components/Layout/Layout';
import LetsTalkComp from '../components/LetsTalkComp/LetsTalkComp';
import ServicePageBanner from '../containers/ServicePageBanners/ServicePageBanner';
import Approach from '../containers/WhayusPage/Approach/Approach';
import Tabs from '../containers/WhayusPage/SectionTabs/Tabs';


class Why extends Component {
  constructor(props) {
    super(props)
  }

  state = { activeTab: 1 }

  setTab = (id) => {
    if (id == null) {
      this.setState({ activeTab: 1 })
    }
    else {
      this.setState({ activeTab: id })
    }
  }

  // const approach = () => <Approach />
  // const protection = () => <Protection />
  // const process = () => <Process />

  renderTab = () => {
    switch (this.state.activeTab) {
      case 1:
        return <>
          <Approach apppage={this.props.data.apppage} />
        </>

      case 2:
        return <>
          <Protection bsdata={this.props.bsdata} />

        </>

      case 3:
        return <>
          <Process />
        </>
      default:
        return <>
          <Approach apppage={this.props.data.apppage} />
        </>
    }
  }
  render() {
    return (
      <Layout>
        <ServicePageBanner content={this.props.data.apppage.sectionbanner} variation='whyus' />
        <Tabs onClick={this.setTab} activeTab={this.state.activeTab} />
        {this.renderTab()}
        <LetsTalkComp />
      </Layout>

    )
  }
}


export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal('/why-us');
  let { data, bsdata } = await getWhyUsData();

  return {
    props: {
      uv,
      data,
      bsdata
    }
  }
}

export default Why