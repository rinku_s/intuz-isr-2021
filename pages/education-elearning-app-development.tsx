import { getELearningPageData, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React, { useState } from 'react';
import FaqComponent from '../components/FAQComponent/FaqComponent';
import FaqSchema from '../components/FAQComponent/FaqSchema';
import FormPopup from '../components/GetStartedForm/FormPopup';
import Layout from '../components/Layout/Layout';
import LetsTalkComponent from '../components/LetsTalkComp/LetsTalkComp';
import AwardsAndRecognition from '../containers/CommonSections/AwardandRecognition/AwardsAndRecognition';
import CTAStripe from '../containers/CommonSections/CTAStripe/CTAStripe';
import ElearningSolutionsSection from '../containers/EducationElearning/ElearningSolutionsSection/ElearningSolutionsSection';
import FeaturedSection from '../containers/EducationElearning/FeaturedSection/FeaturedSection';
import SectionTechTab from '../containers/EnterprisePage/SectionTechTab/SectionTechTab';
import TopTitle from '../containers/EnterprisePage/sectionTopTitle/TopTitle';
import ServicePageBanner from '../containers/ServicePageBanners/ServicePageBanner';


const EducationElearning = ({ uv, data }) => {
    const [show, setShow] = useState(false)

    let bannerContent = {
        'title': 'E-Learning & Education App Development Solution',
        'subTitle': 'Providing Advanced E-Learning Web & Mobile App Solutions for Educational Institutes, Non-Profit Organisation & Education Startups.',
        'backImage': { 'name': 'elearning-page-top-banner.jpg' },
        'buttonLabel': 'Lets Discuss'
    }

    return (

        <Layout>
            <ServicePageBanner content={bannerContent} form={{ subject: "Education App Development Quote Request", leadsource: "EduSolution - Header", thankyoupage: "education-app-development-thankyou" }} />
            <ElearningSolutionsSection elearningsolutions={data.elearningsolutions} />
            <TopTitle
                title="Technology & Tool We Use"
                description="Blend of modern tools & Technologies to build next-generation enterprise solutions"
                backColor="#fafafa">
                <SectionTechTab />
            </TopTitle>
            {/* <TechnologyandToolSection/> */}
            <CTAStripe title="Looking For A Similar Solution?" LinkButtontxt="Request A Free Quote Now" onClick={() => setShow(true)} />
            <FormPopup
                thankyoupage="education-app-development-thankyou"
                subject="Education App Development Quote Request"
                leadsource="EduSolution - Header"
                show={show}
                onHide={() => setShow(false)}
            />
            <FeaturedSection className="mt-mx" />
            <AwardsAndRecognition
                awardandrecognitions={data.awardandrecognitions}
                f />
            <FaqComponent faqs={data.faq.Faqs} />
            <LetsTalkComponent />
            <FaqSchema faqs={data.faq.Faqs} />
        </Layout>

    )
}
export const getStaticProps: GetStaticProps = async (context) => {
    // console.log(context);
    let uv = await getUniversal('/case-studies/obgy-mcqs');
    let data = await getELearningPageData();

    return {
        props: {
            uv,
            data
        }
    }
}
export default EducationElearning

