import Breadcrum from "components/cloud/Breadcrum/Breadcrum";
import GetMeta from "components/cloud/GetMeta/GetMeta";
import GetSupportComponent from "components/cloud/GetSupportComponent/GetSupportComponent";
import Layout from "components/cloud/Layout/Layout";
import LetsTalkComp from "components/LetsTalkComp/LetsTalkComp";
import LoadingScreen from "components/LoadingScreen/LoadingScreen";
import { cdn } from "config/cdn";
import AppInstalledSection from "containers/cloud/ApplicationDetailPage/ApplicationInstalledSection/AppInstalledSection";
import AppDetailBannerSection from "containers/cloud/ApplicationDetailPage/BannerSection/AppDetailBannerSection";
import DescriptionAndLaunch from "containers/cloud/ApplicationDetailPage/DescriptionAndLaunch/DescriptionAndLaunch";
import NewDescription from "containers/cloud/ApplicationDetailPage/DescriptionAndLaunch/NewDescription";
import InformationSection from "containers/cloud/ApplicationDetailPage/InformationSection/InformationSection";
import InterestSection from "containers/cloud/ApplicationDetailPage/InterestSection/InterestSection";
import OfferingSection from "containers/cloud/ApplicationDetailPage/OfferingSection/OfferingSection";
import ReviewSection from "containers/cloud/ApplicationDetailPage/ReviewSection/ReviewSection";
import NeedSupportSection from "containers/cloud/ServicesPage/NeedSupportSection/NeedSupportSection";
import PopupContext from "context/popupContext";
import { getCloudStackContainerData, getUniversal } from "lib/api";
import { GetStaticPaths, GetStaticProps } from "next";
import { useRouter } from "next/router";
import React, { useState } from "react";

const index = ({ data }) => {
  const [open, setOpen] = useState(false);

  const router = useRouter();

  if (router.isFallback) return <LoadingScreen />

  let cloudApp = data.containers[0];
  let {
    free_app_description,
    container_desc,
  } = cloudApp.cloud_application_descriptions[0];
  let freeAppLinks = cloudApp.application_launch_links.filter(
    (link) => link.free_version == true
  );
  let paidAppLinks = cloudApp.application_launch_links.filter(
    (link) => link.free_version == false || link.free_version == null
  );

  let guide = cloudApp.cloud_application_guides[0];
  let { free_apps, paid_apps } = cloudApp.installed_applications[0];

  let mh = cloudApp.cloud_detail_main_heading;
  let sh = cloudApp.cloud_detail_secondary_heading;
  let rss;
  if (cloudApp.reviews.length > 0) {
    const rs = `
      {
        "@context":"http://schema.org",
        "@type": "SoftwareApplication",
        "name": "${cloudApp.name}",
        "operatingSystem": "AWS",
        "applicationCategory": "DeveloperApplication",
        "aggregateRating": {
               "@type": "AggregateRating",
               "ratingValue": "${cloudApp.rating}",
               "ratingCount": "${cloudApp.reviews.length}"
          }
      }
      `;
    rss = (
      <script
        type="application/ld+json"
        dangerouslySetInnerHTML={{ __html: rs }}
      ></script>
    );
  }

  return (
    <Layout>
      <PopupContext.Provider value={{ open, setOpen }}>
        <GetMeta cloudPage={cloudApp.meta} />
        <AppDetailBannerSection
          category={cloudApp.category.name}
          name={cloudApp.name}
          image={cdn(cloudApp.logo.name)}
          version={cloudApp.version}
          rating={cloudApp.rating}
          last_update={cloudApp.last_updated}
        />
        <OfferingSection
          headings={{
            main_heading: mh !== null ? mh.we_offer : "We Offer",
            sub_heading: sh.we_offer,
          }}
          offerings={cloudApp.offering.offerings}
        />
        {!cloudApp.cloud_description_new && (
          <DescriptionAndLaunch
            name={cloudApp.name.toLowerCase()}
            container_desc={container_desc}
            links={paidAppLinks[0]}
          />
        )}
        {cloudApp.cloud_description_new?.description_section.map(
          (c, i) => (
            <NewDescription key={i} {...c} />
          )
        )}
        {!cloudApp.cloud_description_new && free_app_description && (
          <DescriptionAndLaunch
            name={cloudApp.name.toLowerCase()}
            description={free_app_description}
            links={freeAppLinks[0]}
          />
        )}
        <InformationSection
          headings={{
            main_heading:
              mh !== null
                ? mh.include_with_application
                : "Included With Application",
            sub_heading: sh.included_with_application,
          }}
          guide={guide}
        />
        <NeedSupportSection
          onClick={() => setOpen(true)}
          mainText={sh.need_support}
          linkText={"Get Support"}
          popupType
        />
        <AppInstalledSection
          headings={{
            main_heading:
              mh !== null
                ? mh.application_installed
                : "Applications Installed",
            sub_heading: sh.application_installed,
          }}
          paidApps={paid_apps}
          freeApps={free_apps}
        />
        <ReviewSection
          reviews={cloudApp.reviews}
          headings={{
            main_heading: mh !== null ? mh.reviews : "Reviews",
            sub_heading: sh.reviews,
            link: cloudApp.review_url,
          }}
        />
        {/* <SubscriptionSection/> */}
        <InterestSection
          currentLink={`/stack/${router.query.app_name}/container`}
          applications={cloudApp.category.Applications}
          headings={{
            main_heading:
              mh !== null
                ? mh.interested_in
                : "You might be interested in",
            sub_heading: sh.interested_in,
            link: cloudApp.review_url,
          }}
        />
        <LetsTalkComp />
        <Breadcrum
          name={cloudApp.name}
          slug={router.query.app_name}
          dt
          nest
        />
        {rss}
        <GetSupportComponent open={open} onClose={() => setOpen(false)} />
      </PopupContext.Provider>
    </Layout>
  );
};


export const getStaticProps: GetStaticProps = async (context) => {

  let uv = await getUniversal('/');
  let data = await getCloudStackContainerData(context.params.app_name) as any;

  if (data.containers.length === 0) {
    return {
      notFound: true
    }
  }
  return {
    props: {
      uv,
      data
    }
  }
}

export const getStaticPaths: GetStaticPaths = () => {
  return {
    paths: [
      { params: { app_name: "lamp" } }
    ],
    fallback: true
  }
}

export default index;
