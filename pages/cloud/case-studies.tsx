import Breadcrum from 'components/cloud/Breadcrum/Breadcrum'
import Banner from 'components/cloud/CloudReusableSection/Banner'
import GetMeta from 'components/cloud/GetMeta/GetMeta'
import Layout from 'components/cloud/Layout/Layout'
import LetsTalkComp from 'components/LetsTalkComp/LetsTalkComp'
import InformationSection from 'containers/cloud/CaseStudiesPage/InformationSection'
import { getCloudCaseStudyData, getUniversal } from 'lib/api'
import { GetStaticProps } from 'next'
import React from 'react'



const CaseStudy = ({ uv, data }) => {

  return (
    <Layout>
      <GetMeta cloudPage={data.cloudPage} />
      <Banner type="h-100" text="Seize enhanced cloud efficiency <br> with our " gradientText="Cloud&nbsp;Expertise" paragraph="Intuz optimizes your enterprise IT environment by offering secure and user-friendly cloud platforms, technologies, infrastructure and managed services." />
      <div id="casestudy">

        {data.cloudCaseStudies.map((CaseStudy, i) => {
          return (
            <InformationSection key={i} {...CaseStudy} />
          )
        })}
      </div>
      <LetsTalkComp />
      <Breadcrum name={"Case Study"} />
    </Layout>
  )
}

export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal('/cloud/case-studies');
  //context.params.name
  let data = await getCloudCaseStudyData();

  return {
    props: {
      uv,
      data
    }
  }
}

export default CaseStudy
