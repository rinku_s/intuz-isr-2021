import Breadcrum from 'components/cloud/Breadcrum/Breadcrum';
import GetMeta from 'components/cloud/GetMeta/GetMeta';
import HeadAndFootNew from 'components/cloud/HeadAndFoot/HeadAndFootNew';
import LetsTalkComp from 'components/LetsTalkComp/LetsTalkComp';
import WhySection from 'containers/cloud/CommonSection/WhySection/WhySection';
import BannerSection from 'containers/cloud/HomePage/BannerSection/BannerSection';
import FourthSection from 'containers/cloud/HomePage/FourthSection/FourthSection';
import SecondSection from 'containers/cloud/HomePage/SecondSection/SecondSection';
import ThirdSection from 'containers/cloud/HomePage/ThirdSection/ThirdSection';
import { getUniversal } from 'lib/api';
import { getHomePageData } from 'lib/cloud/api';
import React from 'react';

const index = ({ universal, data }) => {
  return (
    <HeadAndFootNew data={universal}>
      {data && <GetMeta cloudPage={data.cloudPage} />}
      <BannerSection />
      <SecondSection />
      <ThirdSection />
      <FourthSection />
      <WhySection />
      <LetsTalkComp />
      {/* <div className="p-12"></div> */}
      <Breadcrum name={"Cloud HomePage"} />
      {data && <div dangerouslySetInnerHTML={{ __html: data.cloudPage.footer_scripts }}>
      </div>}
    </HeadAndFootNew>
  )
}

export async function getStaticProps(context) {
  const universal = await getUniversal('/cloud');
  const data = await getHomePageData();
  return {
    props: {
      universal,
      data
    }
  }
}

export default index
