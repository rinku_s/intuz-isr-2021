import Breadcrum from 'components/cloud/Breadcrum/Breadcrum';
import CloudBlog from 'components/cloud/CloudBlog/CloudBlog';
import Banner from 'components/cloud/CloudReusableSection/Banner';
import GetMeta from 'components/cloud/GetMeta/GetMeta';
import Layout from 'components/cloud/Layout/Layout';
import LetsTalkComp from 'components/LetsTalkComp/LetsTalkComp';
import { cdn } from 'config/cdn';
import { getDatabaseMigrationData, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';

const DatabaseMigration: React.FC<{ uv: any; data: any; }> = (props) => {
  return (

    <Layout>
      <GetMeta cloudPage={props.data.cloudPage} />
      <Banner imageFirst={cdn(props.data.cloudGuide.logo.name)} text={props.data.cloudGuide.subtitle} gradientText={props.data.cloudGuide.title} image={cdn("cloud-amazon-partner.png?auto=compress,format")} gradientFirst={props.data.cloudGuide.title_first} />
      <CloudBlog data={props.data.cloudGuide.content} />
      <LetsTalkComp />
      <Breadcrum name={"AWS Database Migration"} />
    </Layout>

  )
}

export const getStaticProps: GetStaticProps = async (context) => {

  let uv = await getUniversal('/cloud/services');
  let data = await getDatabaseMigrationData();

  return {
    props: {
      uv,
      data
    }
  }
}

export default DatabaseMigration
