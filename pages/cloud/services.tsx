import Breadcrum from 'components/cloud/Breadcrum/Breadcrum'
import Banner from 'components/cloud/CloudReusableSection/Banner'
import GetMeta from 'components/cloud/GetMeta/GetMeta'
import HeadAndFootNew from 'components/cloud/HeadAndFoot/HeadAndFootNew'
import GradientHead from 'components/cloud/Utility/GradientHeading'
import LetsTalkComp from 'components/LetsTalkComp/LetsTalkComp'
import NeedSupportSection from 'containers/cloud/ServicesPage/NeedSupportSection/NeedSupportSection'
import SectionConsultant from 'containers/cloud/ServicesPage/SectionConsultant/SectionConsultant'
import SectionCostAnalysis from 'containers/cloud/ServicesPage/SectionCostAnalysis/SectionCostAnalysis'
import SectionDeployment from 'containers/cloud/ServicesPage/SectionDeployment/SectionDeployment'
import SectionDevops from 'containers/cloud/ServicesPage/SectionDevops/SectionDevops'
import SectionDisasterRecovery from 'containers/cloud/ServicesPage/SectionDisasterRecovery/SectionDisasterRecovery'
import SectionMigration from 'containers/cloud/ServicesPage/SectionMigration/SectionMigration'
import SectionNavigation from 'containers/cloud/ServicesPage/SectionNavigation/SectionNavigation'
import { getUniversal } from 'lib/api'
import { getServicePage } from 'lib/cloud/api'
import React from 'react'
const services = ({ universal, data }) => {

  return (
    <HeadAndFootNew data={universal}>
      { data && <GetMeta cloudPage={data.cloudPage} />}
      <Banner text="Improved Productivity <br> with Agile " gradientText="Cloud&nbsp;Services" />
      <SectionNavigation />
      <GradientHead isMid className="text-center sp">Cloud Consulting Services Intuz Excel At</GradientHead>
      <SectionMigration />
      <SectionConsultant />
      <SectionDeployment />
      <NeedSupportSection mainText="Having exclusive cloud project requirements?" linkText="Let’s Connect" link="/contactus" />
      <SectionCostAnalysis />
      <SectionDisasterRecovery />
      <SectionDevops />
      <LetsTalkComp />
      <Breadcrum name={"Services"} />
    </HeadAndFootNew>

  )
}


export async function getStaticProps(context) {
  const universal = await getUniversal('/cloud/services');
  const data = await getServicePage();
  return {
    props: {
      universal,
      data
    }
  }
}

export default services
