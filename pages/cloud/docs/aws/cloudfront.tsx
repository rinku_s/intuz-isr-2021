import Breadcrum from 'components/cloud/Breadcrum/Breadcrum';
import CloudBlog from 'components/cloud/CloudBlog/CloudBlog';
import Banner from 'components/cloud/CloudReusableSection/Banner';
import GetMeta from 'components/cloud/GetMeta/GetMeta';
import Layout from 'components/cloud/Layout/Layout';
import LetsTalkComp from 'components/LetsTalkComp/LetsTalkComp';
import { cdn } from 'config/cdn';
import { getCloudFrontData, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';

const DatabaseMigration = ({ uv, data }) => {
  return (

    <Layout>
      <GetMeta cloudPage={data.cloudPage} />
      <Banner imageFirst={cdn(data.cloudGuide.logo.name)} text={data.cloudGuide.subtitle} gradientText={data.cloudGuide.title} image="/cloud/static/amazon-partner.png" gradientFirst={data.cloudGuide.title_first} />
      <CloudBlog data={data.cloudGuide.content} />
      <LetsTalkComp />
      <Breadcrum name={"Cloudfront CDN"} />
    </Layout>

  )
}

export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal('/');
  let data = await getCloudFrontData();

  return {
    props: {
      uv,
      data
    }
  }
}

export default DatabaseMigration
