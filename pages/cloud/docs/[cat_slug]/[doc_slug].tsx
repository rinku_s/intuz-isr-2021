import Breadcrum from 'components/cloud/Breadcrum/Breadcrum';
import Banner from 'components/cloud/CloudReusableSection/Banner';
import DocumentSteps from 'components/cloud/DocumentSteps/DocumentSteps';
import GetMeta from 'components/cloud/GetMeta/GetMeta';
import Layout from 'components/cloud/Layout/Layout';
import LetsTalkComp from 'components/LetsTalkComp/LetsTalkComp';
import LoadingScreen from 'components/LoadingScreen/LoadingScreen';
import { getCloudDocSlugData } from 'lib/api';
import { GetStaticPaths, GetStaticProps } from 'next';
import { useRouter } from 'next/router';
import React from 'react';
const Document = ({ uv, data }) => {
  const router = useRouter();
  if (router.isFallback) return <LoadingScreen />

  console.log();


  const cloudDocumentation = data.cloudDocumentations[0];
  return (



    <Layout>
      <GetMeta cloudPage={cloudDocumentation.meta} />
      <Banner text={cloudDocumentation.text} gradientText={cloudDocumentation.gradientText} paragraph={cloudDocumentation.paragraph} />
      <DocumentSteps content={cloudDocumentation.content} />
      <LetsTalkComp />
      <Breadcrum name={cloudDocumentation.title} doc_page />
    </Layout>


  )
}

export const getStaticProps: GetStaticProps = async (context) => {


  let data = await getCloudDocSlugData(context.params.cat_slug, context.params.doc_slug) as any;
  if (data.cloudDocumentations.length === 0) {
    return {
      notFound: true
    }
  }
  return {
    props: {
      data
    }
  }
}


export const getStaticPaths: GetStaticPaths = () => {
  return {
    paths: [
      { params: { cat_slug: "how-to", doc_slug: "how-to-setup-cloudformation" } }
    ],
    fallback: true
  }
}


export default Document
