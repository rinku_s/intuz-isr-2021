import Breadcrum from 'components/cloud/Breadcrum/Breadcrum'
import Banner from 'components/cloud/CloudReusableSection/Banner'
import GetMeta from 'components/cloud/GetMeta/GetMeta'
import Layout from 'components/cloud/Layout/Layout'
import LetsTalkComp from 'components/LetsTalkComp/LetsTalkComp'
import Documentation from 'containers/cloud/Documentation/Documentation'
import { getCloudDocData, getUniversal } from 'lib/api'
import { GetStaticProps } from 'next'
import React from 'react'

const index = ({ uv, data }) => {



    return (
        <Layout>
            <GetMeta cloudPage={data.cloudPage} />
            <Banner text="Intuz " gradientText="Cloud Documentation" paragraph={"Documents, Guides, resources and Tutorials to <br/> launch and manage your AMI swiftly and smoothly"} />
            <Documentation />
            <LetsTalkComp />
            <Breadcrum name={"Documentations"} />
        </Layout>
    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    // console.log(context);
    let uv = await getUniversal('/cloud/docs');
    //context.params.name
    let data = await getCloudDocData();

    return {
        props: {
            uv,
            data
        }
    }
}

export default index
