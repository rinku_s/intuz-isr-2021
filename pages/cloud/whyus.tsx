import Breadcrum from 'components/cloud/Breadcrum/Breadcrum'
import Banner from 'components/cloud/CloudReusableSection/Banner'
import GetMeta from 'components/cloud/GetMeta/GetMeta'
import Layout from 'components/cloud/Layout/Layout'
import LetsTalkComp from 'components/LetsTalkComp/LetsTalkComp'
import { cdn } from 'config/cdn'
import CapabilitiesSection from 'containers/cloud/WhyUsPage/CapabilitiesSection/CapabilitiesSection'
import GlanceSection from 'containers/cloud/WhyUsPage/GlanceSection/GlanceSection'
import WhyUsFeatureSection from 'containers/cloud/WhyUsPage/WhyUsFeatureSection/WhyUsFeatureSection'
import { getCloudWhyUsData, getUniversal } from 'lib/api'
import { GetStaticProps } from 'next'
import React from 'react'

const whyus = ({ uv, data }) => {

  return (

    <Layout>
      <GetMeta cloudPage={data.cloudPage} />
      <Banner text="Innovate and Transform the Business <br> with End-to-End " gradientText="AWS&nbsp;Services" image={cdn('cloud-amazon-partner.png?auto=compress,format')} />
      <WhyUsFeatureSection />
      <GlanceSection />
      <CapabilitiesSection />
      <LetsTalkComp />
      <Breadcrum name={"Why Us"} />
    </Layout>

  )
}

export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal('/cloud/whyus');
  //context.params.name
  let data = await getCloudWhyUsData();

  return {
    props: {
      uv,
      data
    }
  }
}

export default whyus
