import Breadcrum from "components/cloud/Breadcrum/Breadcrum";
import GetMeta from "components/cloud/GetMeta/GetMeta";
import Layout from "components/cloud/Layout/Layout";
import LetsTalkComp from "components/LetsTalkComp/LetsTalkComp";
import SectionAppList from "containers/cloud/ApplicationsPage/SectionAppList/SectionAppList";
import SectionBanner from "containers/cloud/ApplicationsPage/SectionBanner/SectionBanner";
import WhySection from "containers/cloud/CommonSection/WhySection/WhySection";
import ApplicationListProvider from "context/ApplicationListContext";
import { AggregateCount } from "lib/AggregateCountCloudApp";
import {
  getApplicationCategories,
  getCloudApplicationCount,
  getCloudApplicationLists,
  getCloudStackData,
  getDeliveryTypeFilters,
  getPricingPlans,
  getUniversal,
} from "lib/api";
import {
  ApplicationListModel,
  CategoryModel,
  DeliveryMethodModel,
  PricingPlanModel,
} from "lib/cloud/ApplicationListModel";
import { GetStaticProps } from "next";
import React from "react";

const Stacks: React.FC<{
  uv: any;
  data: any;
  list: ApplicationListModel;
  deliveryTypes: DeliveryMethodModel;
  categories: CategoryModel;
  pricingPlans: PricingPlanModel;
  count: AggregateCount;
}> = (props) => {
  return (
    <ApplicationListProvider
      initialState={{
        app_list: props.list.cloudApplicationLists,
        start: 0,
        totalCount:
          props.count.cloudApplicationsConnection.aggregate.totalCount,
        filter: {
          ready: true,
        },
        allFilters: {
          categories: props.categories.cloudAppCategories,
          deliveryMethods: props.deliveryTypes.cloudAppDeliveryMethods,
          pricingPlans: props.pricingPlans.cloudAppPricePlans,
        },
        firstList: false,
      }}
    >
      <Layout>
        <GetMeta cloudPage={props.data.cloudPage} />
        <SectionBanner />
        {/* <TrustedSection /> */}
        <SectionAppList />
        <WhySection />
        <LetsTalkComp />
        <Breadcrum name={"Application List"} />
      </Layout>
    </ApplicationListProvider>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  let uv = await getUniversal("/cloud/stacks");
  let data = await getCloudStackData();
  let list = await getCloudApplicationLists(15, 0, { ready: true });
  let deliveryTypes = await getDeliveryTypeFilters();
  let categories = await getApplicationCategories();
  let pricingPlans = await getPricingPlans();
  let count = await getCloudApplicationCount({ ready: true });

  return {
    props: {
      uv,
      data,
      list,
      deliveryTypes,
      categories,
      pricingPlans,
      count,
    },
  };
};

export default Stacks;
