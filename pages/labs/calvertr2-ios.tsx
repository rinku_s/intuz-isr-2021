import { getLabCalverts2Data, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';
import Layout from '../../components/Layout/Layout';
import LetsTalkComp from '../../components/LetsTalkComp/LetsTalkComp';
import { cdn } from '../../config/cdn';
import SectionBanner from '../../containers/LabsContainer/Calvertr2Page/SectionBanner/SectionBanner';
import Categories from '../../containers/LabsContainer/Calvertr2Page/SectionCategories/Categories';
import ConverterCalculator from '../../containers/LabsContainer/Calvertr2Page/SectionConverterCalculator/ConverterCalculator';
import Favorites from '../../containers/LabsContainer/Calvertr2Page/SectionFavorites/Favorites';
import GetItNow from '../../containers/LabsContainer/Calvertr2Page/SectionGetItNow/GetItNow';
import Languages from '../../containers/LabsContainer/Calvertr2Page/SectionLanguages/Languages';
import Widgets from '../../containers/LabsContainer/Calvertr2Page/SectionWidgets/Widgets';
import CenterImageSection from '../../containers/LabsContainer/CommonSections/CenterImageSection/CenterImageSection';


const alarmr = (props) => {
    return (

        <Layout>
            <SectionBanner />
            <ConverterCalculator />
            <Categories />
            <Favorites />
            <CenterImageSection
                title="Saved Conversions"
                description="Swipe left in conversion screen and add your preferred unit combinations into saved conversions list for future reference Automatically get list of your conversions performed on Widget & Apple Watch and access them with fewest taps"
                image={cdn('calvertr2_conversional_img.png')}
                alt='conversion_img'
                variation='padding' />
            <Widgets />
            <Languages />
            <GetItNow />
            <LetsTalkComp />
        </Layout>

    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    // console.log(context);
    let uv = await getUniversal('/labs/calvertr2-ios');
    //context.params.name
    let data = await getLabCalverts2Data();

    return {
        props: {
            uv,
            data
        }
    }
}

export default alarmr
