import HeadAndFootNew from 'components/HeadAndFoot/HeadAndFootNew';
import { getIWtherData, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';
import AppLink from '../../components/AppLink/AppLink';
import GetNow from '../../components/LabsComponent/GetNow/GetNow';
import SectionFlex from '../../components/LabsComponent/SectionFlex/SectionFlex';
import Layout from '../../components/Layout/Layout';
import LetsTalkComp from '../../components/LetsTalkComp/LetsTalkComp';
import FeatureSection from '../../containers/LabsContainer/IWhterPage/FeaturesSection/FeatureSection';
import ForecastSection from '../../containers/LabsContainer/IWhterPage/ForecastSection/ForecastSection';
import HeroSection from '../../containers/LabsContainer/IWhterPage/SectionHero/SectionHero';

const iweather = ({ uv, data }) => {
    return (
        <HeadAndFootNew data={uv}>

            <Layout>
                <HeroSection />
                <FeatureSection fetures={data.lab.labfeatures} />
                <SectionFlex
                    heading="Displays weather conditions for multiple locations"
                    description="Get current weather as live tiles for up to 4 different locations. Tap on a location to go to a deeper level. Easy to access detailed weather information for your current location and for the places you care about."
                    image={("/static/Images/casestudycar/iwheather.png")}
                    alt="Labs Weather 1"
                    variation='spaceBlock'
                />
                <SectionFlex
                    heading="HD radar images like never before"
                    description="Enjoy wealth of radar and in-depth information such as Location, Wind Speed, Temperature, and Humidity directly on the radar screen."
                    image={("/static/Images/casestudycar/iweather-2.png")}
                    alt="Labs Weather 2"
                    backcolor="#fff"
                    direction="row-reverse"
                />
                <ForecastSection />
                <GetNow
                    variation="whiteback"
                    subtitle="Know the plans of nature with iWeather app so that you can plan your day better!">
                    <AppLink link={{
                        iosgrey: "https://apps.apple.com/us/app/id823413326?ls=1",
                        androidgrey: "https://play.google.com/store/apps/details?id=com.r3app.iweatherfree",
                    }} />
                </GetNow>
                <LetsTalkComp />
            </Layout>
        </HeadAndFootNew>


    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    // console.log(context);
    let uv = await getUniversal('/labs/iweather');
    //context.params.name
    let data = await getIWtherData();

    return {
        props: {
            uv,
            data
        }
    }
}

export default iweather
