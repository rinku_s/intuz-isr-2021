import { getUniversal, getWtherLabData } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';
import AppLink from '../../components/AppLink/AppLink';
import GetNow from '../../components/LabsComponent/GetNow/GetNow';
import SectionFlex from '../../components/LabsComponent/SectionFlex/SectionFlex';
import Layout from '../../components/Layout/Layout';
import LetsTalkComp from '../../components/LetsTalkComp/LetsTalkComp';
import { cdn } from '../../config/cdn';
import FeatureSection from '../../containers/LabsContainer/CommonSections/FeatureSection/FeatureSection';
import NextGenerationSection from '../../containers/LabsContainer/CommonSections/NextGenerationSection/NextGenerationSection';
import ElegentSection from '../../containers/LabsContainer/WhterPage/ElegentSection/ElegentSection';
import WhetherForecast from '../../containers/LabsContainer/WhterPage/Forecast/Forecast';
import HeroSection from '../../containers/LabsContainer/WhterPage/HeroSection/HeroSection';
import ReviewSection from '../../containers/LabsContainer/WhterPage/ReviewSection/ReviewSection';



const NextGenImages = [
    {
        'src': cdn('wther-watch-1.jpg'),
        'alt': 'Wther 01'
    },
    {
        'src': cdn('wther-watch-2.jpg'),
        'alt': 'Wther 02'
    },
    {
        'src': cdn('wther-watch-3.jpg'),
        'alt': 'Wther 03'
    },
    {
        'src': cdn('wther-watch-4.jpg'),
        'alt': 'Wther 04'
    }
]
const WhterLab = ({ uv, data }) => {
    return (


        <Layout>
            <HeroSection />
            <NextGenerationSection
                title="Generation Next Weather App"
                description="Get the latest conditions for favorite locations on the go. Don’t just check the forecast, feel it and experience the weather come to life on your iOS device!"
                images={NextGenImages}
            />
            <FeatureSection fetures={data.lab.labfeatures} />
            <ElegentSection />
            <SectionFlex
                heading="Incredibly smooth HD Radar"
                description="Stunning animated HD weather radar and beautiful world map with exciting layers like Precipitation, Clouds, Temperature & Sea level pressure. Keep a tab on what the weather has in store for you. Never get caught off-guard."
                image={"/static/Images/casestudycar/labs-wther-2.png"}
                alt="Labs Wther 2"
                direction="row-reverse"
                backcolor="#fff"
            />
            <WhetherForecast />
            <ReviewSection />
            <GetNow subtitle="Download Weather app for free to get your own personal weather data.">
                <AppLink link={{ ios: "https://apps.apple.com/us/app/id868050791?ls=1", android: "https://play.google.com/store/apps/details?id=com.r3app.wthr" }} />
            </GetNow>
            <LetsTalkComp />
        </Layout>

    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    // console.log(context);
    let uv = await getUniversal('/labs/wther');
    //context.params.name
    let data = await getWtherLabData();

    return {
        props: {
            uv,
            data
        }
    }
}

export default WhterLab
