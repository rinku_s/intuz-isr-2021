import { getUniversal, getWtherCompleteData } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';
import AppLink from '../../components/AppLink/AppLink';
import GetNow from '../../components/LabsComponent/GetNow/GetNow';
import SectionFlex from '../../components/LabsComponent/SectionFlex/SectionFlex';
import Layout from '../../components/Layout/Layout';
import LetsTalkComp from '../../components/LetsTalkComp/LetsTalkComp';
import { cdn } from '../../config/cdn';
import CenterImageSection from '../../containers/LabsContainer/CommonSections/CenterImageSection/CenterImageSection';
import FeatureSection from '../../containers/LabsContainer/CommonSections/FeatureSection/FeatureSection';
import NextGenerationSection from '../../containers/LabsContainer/CommonSections/NextGenerationSection/NextGenerationSection';
import HeroSection from '../../containers/LabsContainer/WtherCompletePage/HeroSection/HeroSection';
import RadioCenter from '../../containers/LabsContainer/WtherCompletePage/RadioCenterSection/RadioCenter';



const NextGenImages = [
    {
        'src': cdn('iweather-watch-1.jpg'),
        'alt': 'Wther 01'
    },
    {
        'src': cdn('iweather-watch-2.jpg'),
        'alt': 'Wther 02'
    },
    {
        'src': cdn('iweather-watch-3.jpg'),
        'alt': 'Wther 03'
    },
    {
        'src': cdn('iweather-watch-4.jpg'),
        'alt': 'Wther 04'
    }
]

const WhterCompLab = ({ uv, data }) => {
    return (

        <Layout>
            <HeroSection />
            <NextGenerationSection
                title="Comprehensive Weather Data Engine"
                description="Get all important data at one place. Enjoy weather summary for the day and experience powerful radar & map layers."
                images={NextGenImages}
            />
            <FeatureSection fetures={data.lab.labfeatures} />
            <RadioCenter />
            <SectionFlex
                heading="Severe weather warnings"
                description="Your trusted weather forecast specialist. Stay safe with severe thunderstorm & flash flood warnings."
                image={cdn('weather_warnings.jpg')}
                alt="weather warnings"
                backcolor="#fff"
                center
                direction="row-reverse"
            />
            <CenterImageSection
                title="Slick interface and innovative animations"
                description="Stunningly beautiful interface to see what the weather has in store. Smooth and fluid animations."
                image={cdn('inno_animations.png')}
                dwidth={850}
                alt='Maths Expression'
            />
            <GetNow subtitle="Download Wther complete to get comprehensive live weather with Radar from anywhere in the world">
                <AppLink link={{
                    ios: "https://apps.apple.com/us/app/id945337437?ls=1",
                    android: "https://play.google.com/store/apps/details?id=com.r3app.wthrcomplete"
                }} />
            </GetNow>
            <LetsTalkComp />
        </Layout>

    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    let uv = await getUniversal('/labs/wther-complete');
    //context.params.name
    let data = await getWtherCompleteData();

    return {
        props: {
            uv,
            data
        }
    }
}

export default WhterCompLab
