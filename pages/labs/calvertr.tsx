import SectionFlex from 'components/LabsComponent/SectionFlex/SectionFlex';
import { cdn } from 'config/cdn';
import CenterImageSection from 'containers/LabsContainer/CommonSections/CenterImageSection/CenterImageSection';
import { getLabCalvertsData, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';
import AppLink from '../../components/AppLink/AppLink';
import GetNow from '../../components/LabsComponent/GetNow/GetNow';
import Layout from '../../components/Layout/Layout';
import LetsTalkComp from '../../components/LetsTalkComp/LetsTalkComp';
import FeatureSection from '../../containers/LabsContainer/CalvertrPage/FeatureSection/FeatureSection';
import NotificationCenter from '../../containers/LabsContainer/CalvertrPage/NotificationCenterSection/NotificationCenter';
import HeroSection from '../../containers/LabsContainer/CalvertrPage/SectionHero/HeroSection';



const calvertr = ({ uv, data }) => {
    return (

        <Layout>
            <HeroSection />
            <FeatureSection fetures={data.lab.labfeatures} />
            <NotificationCenter />
            <SectionFlex
                heading="Sharing Made Easy"
                description="Add personal notes to saved conversions or share complete calculation with your friends through Email or Airdrop or Print it."
                image={cdn("calvetr-sharing.png")}
                alt="Labs Weather 2"
                backcolor="#fff"
                center
                variation='spaceBlock'
            />
            <CenterImageSection
                title="Live Math Expression"
                description="Create live math expression. Automatically save your complete expression for future use."
                image={cdn("calvetr-math.png")}
                alt='Maths Expression'
            />
            <GetNow subtitle="Download free Calvertr app for Android & IOS to enjoy beautiful mornings.">
                <AppLink link={{ ios: "https://apps.apple.com/us/app/calvertr-units-currency-conversion/id920041144?ls=1", android: "https://play.google.com/store/apps/details?id=com.r3app.convertrlite" }} />
            </GetNow>
            <LetsTalkComp />
        </Layout>

    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    // console.log(context);
    let uv = await getUniversal('/labs/calvertr');
    //context.params.name
    let data = await getLabCalvertsData();

    return {
        props: {
            uv,
            data
        }
    }
}

export default calvertr
