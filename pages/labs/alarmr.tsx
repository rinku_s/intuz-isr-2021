import { getLabAlarmrData, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';
import AppLink from '../../components/AppLink/AppLink';
import GetNow from '../../components/LabsComponent/GetNow/GetNow';
import SectionFlex from '../../components/LabsComponent/SectionFlex/SectionFlex';
import Layout from '../../components/Layout/Layout';
import LetsTalkComp from '../../components/LetsTalkComp/LetsTalkComp';
import FeatureSection from '../../containers/LabsContainer/AlarmPage/FeaturesSection/FeatureSection';
import HeroSection from '../../containers/LabsContainer/AlarmPage/SectionHero/SectionHero';
import WakeupCallSection from '../../containers/LabsContainer/AlarmPage/WakeupCallSection/WakeupCallSection';
import CenterImageSection from '../../containers/LabsContainer/CommonSections/CenterImageSection/CenterImageSection';


const alarmr = ({ uv, data }) => {
    return (

        <Layout>
            <HeroSection />
            <FeatureSection fetures={data.lab.labfeatures} />
            <CenterImageSection
                title="Your companion for the day"
                description="Get breaking news, weather updates, and events from your calendar on Automatic Landscape Screen. All the information you need on a single screen."
                image={"/static/Images/casestudycar/alarm_companion.png"}
                alt='Campanion'
                variation='padding'
            />
            <SectionFlex
                heading="Understands your taste and style"
                description="Lets you select from a range of gorgeous themes. Choose the look you love. Gesture-driven interface and minimalist approach makes the user experience more enjoyable than ever before."
                image={"/static/Images/casestudycar/alarm_taste_style.png"}
                alt="Labs Alarm"
                backcolor="#fff"
                direction="row-reverse" />
            <WakeupCallSection />
            <GetNow variation="whiteback"
                subtitle="Download free Alarm app for Android & IOS to enjoy beautiful mornings.">
                <AppLink link={{
                    iosgrey: "https://apps.apple.com/us/app/alamrr/id850284623?ls=1",
                    androidgrey: "https://play.google.com/store/apps/details?id=com.r3app.alarmr"
                }} />
            </GetNow>
            <LetsTalkComp />
        </Layout>

    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    // console.log(context);
    let uv = await getUniversal('/labs/alarmr');
    //context.params.name
    let data = await getLabAlarmrData();

    return {
        props: {
            uv,
            data
        }
    }
}

export default alarmr
