
import { getRapidAppData, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React from 'react';
import Accordian from '../components/FaqAccordian/Accordian';
import Layout from '../components/Layout/Layout';
import LetsTalkComp from '../components/LetsTalkComp/LetsTalkComp';
import SectionBreakPoint from '../containers/CommonSections/SectionBreakPoint/SectionBreakPoint';
import SectionResource from '../containers/HomePage/SectionResource/SectionResource';
import Libraries from '../containers/RapidAppDev/SectionLibraries/Libraries';
import RadProcess from '../containers/RapidAppDev/SectionRadProcess/RadProcess';
import TextContent from '../containers/RapidAppDev/SectionTextContent/TextContent';
import TopTitle from '../containers/RapidAppDev/SectionTopTitle/TopTitle';
import TraditionalDevelopment from '../containers/RapidAppDev/SectionTraditionalDevelopment/TraditionalDevelopment';
import SectionWhyRad from '../containers/RapidAppDev/SectionWhyRad/SectionWhyRad';
import ServicePageBanner from '../containers/ServicePageBanners/ServicePageBanner';

const RapidAppDev = ({ uv, data }) => {
    return (
        <Layout>
            <ServicePageBanner content={data.apppage.sectionbanner} />
            <TopTitle
                title="What is RAD?"
                description="A technology that consistently delivers software ideas on-time and in budget">
                <TextContent>
                    Rapid application development (RAD) is a software development methodology used to expedite the application development process. RAD's approach towards software development emphasis more on the adaptive process than on planning.
                                    </TextContent>
            </TopTitle>
            <TopTitle title="RAD Process">
                <RadProcess />
            </TopTitle>
            <TopTitle title="Why RAD?"
                description="Stable, simple, minimal efforts, no or less coding and highly flexible">
                <TextContent>
                    In the RAD model, the functional modules are developed as prototypes and are integrated to form a complete product. There are quick implementation methods wherein features are exposed gradually and changes incorporated immediately. It uses readily available libraries and pre-built components to build applications thus saves time and provides benefits for companies of all sizes.
                                    </TextContent>
                <SectionWhyRad />
            </TopTitle>
            <TopTitle title="RAD <span> vs </span> Traditional Development"
                description="Though RAD has become popular development methodology still one cannot overlook the benefits of traditional development. Learn the pros of both the development methodologies.">
                <TraditionalDevelopment />
            </TopTitle>
            <TopTitle title="Intuz’s Libraries for RAD"
                description="Packages to match all your app requirements">
                <Libraries />
            </TopTitle>
            <TopTitle title="FAQs"
                description="For your quick reference, we have compiled the answers to some Frequently Asked Questions.">
                <Accordian />
            </TopTitle>
            <SectionBreakPoint content={data.apppage.sectionbreakpoints} />
            <SectionResource sectionresources={data.guides} />
            <LetsTalkComp />
        </Layout>

    )
}

export const getStaticProps: GetStaticProps = async (context) => {
    // console.log(context);
    let uv = await getUniversal('/rapid-application-development');
    let data = await getRapidAppData();

    return {
        props: {
            uv,
            data
        }
    }
}

export default RapidAppDev

