import NewBanner from "containers/ServicePageBanners/bannerPage";
import React from "react";
import CaseStudyCarousel from "../components/CaseStudyCarousel/CaseStudyCarousel";
import FaqComponent from "../components/FAQComponent/FaqComponent";
import FaqSchema from "../components/FAQComponent/FaqSchema";
import HeadAndFootNew from "../components/HeadAndFoot/HeadAndFootNew";
import IconGroup from "../components/IconGroup/IconGroup";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import ClientTestimonialCarousel from "../containers/CommonSections/ClientTestimonialCarousel/ClientTestimonialCarousel";
import SectionBreakPoint from "../containers/CommonSections/SectionBreakPoint/SectionBreakPoint";
import SectionIconDetail from "../containers/CommonSections/SectionIconDetail/SectionIconDetail";
import SectionDetailText from "../containers/IosDevelopment/SectionDetailText/SectionDetailText";
import { getAndroidDevelopmentPageData, getUniversal } from "../lib/api";

const AndroidDev = ({ data, uv }) => {
  return (
    <HeadAndFootNew data={uv}>
      <Layout>
        {/* <ServicePageBanner content={data.apppage.sectionbanner} /> */}
        <NewBanner title="Android App Development Services" backImage={"Android_83afebeb01.png"} buttonLabel='Our Work' link='/work' subTitle='Valuing What Gets Used, Not What Gets Built' />
        <SectionDetailText
          title="Android App Development Company"
          description="Strategize your business growth with top-notch mobile apps developed and deployed by the technically proficient Android app developers. Intuz aims to deliver top-quality apps that meet the specific needs of the clients."
          variation="topPadding"
        />
        <CaseStudyCarousel />
        <SectionDetailText
          title="Solution Expertise for Industry Verticals"
          description="Intuz has worked with the SMEs to large scale organizations from different industries such as education, health and fitness, transportation, social network, hospitality, business and productivity, e-commerce, lifestyle, technology, games, real estate, etc.<br /> <br />Intuz builds custom Android mobile applications using the proven processes and emerging tools and technologies. Having more than 8 years of industry experience, we have a great track record to serve clients from diversified domains and enable them with the best mobility solutions."
          variation="topPadding"
        >
          <IconGroup>{data.apppage.sectiontolicongroups}</IconGroup>
        </SectionDetailText>
        <ClientTestimonialCarousel content={data.testimonialcarouselcontents} />
        <SectionBreakPoint
          content={data.apppage.sectionbreakpoints}
          variation="addspace"
        />

        <SectionDetailText
          title="Best Android App Developers and Practices"
          description="We stay updated with latest technologies and trends to deliver the industry best mobile apps to our clientele. Gifted developers of Intuz offer tailored apps to meet the specific needs of versatile businesses."
          variation="topPadding"
          backColor="#f8f8f8"
        >
          <SectionIconDetail>
            {data.apppage.sectiontopicondetails}
          </SectionIconDetail>
        </SectionDetailText>
        <SectionDetailText
          title="Early Adopter of Advanced Technologies"
          description="We follow well-established practices to enable clients with next generation mobile applications using Android software development tools. We also offer exclusive integrations including payment gateways, push notifications, augmented reality, virtual reality, widgets, and more."
          variation="topPadding"
        >
          <IconGroup>{data.apppage.sectionbottomicongroups}</IconGroup>
        </SectionDetailText>
        <FaqComponent faqs={data.apppage.faqs[0].Faqs} />
        <LetsTalkComp />
        <FaqSchema faqs={data.apppage.faqs[0].Faqs} />
      </Layout>
    </HeadAndFootNew>
  );
};
export async function getStaticProps(context) {
  let uv = await getUniversal("/android-development");
  let data = await getAndroidDevelopmentPageData();

  return {
    props: {
      uv,
      data,
    },
  };
}

export default AndroidDev;
