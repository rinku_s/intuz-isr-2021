import React from 'react'
import HeadAndFootNew from '../components/HeadAndFoot/HeadAndFootNew';
import Layout from '../components/Layout/Layout';
import LetsTalkComp from '../components/LetsTalkComp/LetsTalkComp';
import CareerSliceZone from '../containers/CareersNewPage';
import { getNewCareerData, getUniversal } from '../lib/api';
import gsap from "gsap";
import { ScrollTrigger } from "gsap/dist/ScrollTrigger";
import { useEffect } from 'react';
gsap.registerPlugin(ScrollTrigger);
gsap.registerEffect({
    name:'fade',
    extendTimeline: true,
    effect:(target,config) => {
        return gsap.from(target, { y:config.y, autoAlpha:0, ease:"power1.out", stagger:config.stagger, duration:config.duration });
    },
    defaults: {duration: 0.5, y: 30, stagger:0},
})
const CareersNew = ({ uv, data }) => {

    useEffect(() => {
        
        return () => {
            ScrollTrigger.getAll().forEach((instance) => {
                instance.kill();
              });
              // This in case a scroll animation is active while the route is updated
              gsap.killTweensOf(window);
        }
    }, [])

    return (
        <HeadAndFootNew data={uv}>
            <Layout nunito={true}>
                <CareerSliceZone content={data.careerPage.content} />
                <LetsTalkComp />
            </Layout>
        </HeadAndFootNew>
    )
}

export async function getStaticProps(context) {
    let uv = await getUniversal("/careers");
    let data = await getNewCareerData();

    return {
        props: {
            uv,
            data,
        },
    };
}

export default CareersNew
