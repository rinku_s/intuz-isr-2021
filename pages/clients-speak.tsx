import HeadAndFootNew from "components/HeadAndFoot/HeadAndFootNew";
import ClientSpeakBanner from "containers/ClientSpeaksPage/Banner";
import TestimonialSection from "containers/ClientSpeaksPage/TestimonialSection/TestimonialSection";
import { getClientSpeakData, getUniversal } from "lib/api";
import React from "react";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import ClientSpeakSection from "../containers/ClientSpeaksPage/ClientSpeakSection/ClientSpeakSection";

const ClientSpeak = ({ uv, testimonials }) => {
  return (
    <HeadAndFootNew data={uv}>
      <Layout whiteBackground>
        <ClientSpeakBanner />
        <TestimonialSection {...testimonials} />
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};

export async function getStaticProps() {
  let uv = await getUniversal("/clients-speak");
  let testimonials = await getClientSpeakData(0);
  return {
    props: {
      uv,
      testimonials,
    },
  };
}

export default ClientSpeak;
