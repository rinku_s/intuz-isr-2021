import NewBanner from "containers/ServicePageBanners/bannerPage";
import { getIosDevelopment, getUniversal } from "lib/api";
import { GetStaticProps } from "next";
import React from "react";
import CaseStudyCarousel from "../components/CaseStudyCarousel/CaseStudyCarousel";
import FaqComponent from "../components/FAQComponent/FaqComponent";
import FaqSchema from "../components/FAQComponent/FaqSchema";
import IconGroup from "../components/IconGroup/IconGroup";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import ClientTestimonialCarousel from "../containers/CommonSections/ClientTestimonialCarousel/ClientTestimonialCarousel";
import SectionBreakPoint from "../containers/CommonSections/SectionBreakPoint/SectionBreakPoint";
import SectionIconDetail from "../containers/CommonSections/SectionIconDetail/SectionIconDetail";
import AppDevBlocks from "../containers/IosDevelopment/AppDevBlocks/index";
import IOSAppDevSection from "../containers/IosDevelopment/IOSAppDevSection/index";
import AllDevice from "../containers/IosDevelopment/IOSDevices/AllDevice";
import IOSResourceSection from "../containers/IosDevelopment/IOSResourceSection/IOSResourceSection";
import Slider from "../containers/IosDevelopment/IOSSlider/Slider";
import SectionDetailText from "../containers/IosDevelopment/SectionDetailText/SectionDetailText";
import WhyIOSApp from "../containers/IosDevelopment/WhyIOSApp/WhyIOSApp";

// /* <IconGroup>{data.apppage.sectionbottomicongroups}</IconGroup> */
const IosDev = ({ uv, data }) => {
  return (
    <Layout>
      {/* <ServicePageBanner content={data.apppage.sectionbanner} /> */}
      {/* <ApplicationPageBanner
        variation="iospage"
        content={data.apppage.sectionbanner}
      /> */}
      <NewBanner
        title="iOS App Development Services"
        backImage={"ios_Main_Banner_d99790d889.png"}
        buttonLabel="Our Work"
        link="/work"
        description="<p>Simple can be harder than complex

We aspire to create iOS Apps that look simple yet are very innovative.<p>"
      />

      <AppDevBlocks
        title="iOS App Development Company"
        description="Intuz belives in delivering best, nothing less than that! We are a premier iOS app development company having an intelliectual team of iPhone App Developers who build acant-grade mobile app.          "
      />
      <IOSAppDevSection
        title="iOS App Development Services"
        description="Leverage our high-end and intuitive iOS applications for your business,
           enthrall your audience, and beat the branded competition.
           "
      />
      <SectionIconDetail
        title="Best iOS App Developers & Practices"
        backColor="#f8f8f8"
        variation="addspace"
      >
        {data.apppage.sectiontopicondetails}
      </SectionIconDetail>
      <SectionBreakPoint
        content={data.apppage.sectionbreakpoints}
        variation="addspace"
      />
      <CaseStudyCarousel />
      <SectionDetailText
        title="A Great Blend of Mobility, Cloud, and loT"
        description="Using Apple's advanced SDKs, we build elegant apps with an extremely professional look and feel. A great mix of mobile app and IoT (Internet of Things) enables us to handover powerful apps that work alongside related software and deliver the results businesses are expecting.<br /><br />
                                Moreover, Intuz excels in offering advanced features integration including iBeacon, iWatch, Apple Pay, Chat (XMPP), Quickblox, iCloud Drive, and more."
        variation="topPadding"
      >
        <IconGroup>{data.apppage.sectiontolicongroups}</IconGroup>
      </SectionDetailText>
      <AllDevice />
      <SectionDetailText
        title="Our Expertise"
        description="Intuz has the capability to build scalable and high-performing iPhone and iPad mobile applications that not only appeal to users but also boost up business operations of the organizations. Iphone application developers of Intuz create innovative and creative iOS applications that offer formidable results.<br /><br /> We have proven expertise in providing industry best product apps, SaaS solutions, CRM apps, and much more. Having exclusive experience to work with startups to corporate, we have in-depth insights about the versatile needs of diversified industry domains."
        variation="SectionOnly"
      >
        <section>
          <IconGroup isbold={true}>
            {data.apppage.sectionbottomicongroups}
          </IconGroup>
        </section>
      </SectionDetailText>
      <ClientTestimonialCarousel content={data.testimonialcarouselcontents} />
      <SectionDetailText
        title="Excellence of our iOS Developer Team"
        description="iPhone developers of Intuz hold caliber to understand the client's custom needs and build a perfect mobile app.We also have an award-winning team of strategists, project managers, team leads, UI/UX designers, business analysts, etc. We interact with clients throughout the ipad/iphone app development process that starts from app ideation, planning, UI/UX design, development, testing, launch, and support."
        variation="topPadding"
      />
      <Slider />
      <WhyIOSApp />
      <FaqComponent faqs={data.apppage.faqs[0].Faqs} />
      <IOSResourceSection sectionresources={data.sectionresources} />
      <LetsTalkComp />
      <FaqSchema faqs={data.apppage.faqs[0].Faqs} />
    </Layout>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  // console.log(context);
  let uv = await getUniversal("/ios-development");
  let data = await getIosDevelopment();

  return {
    props: {
      uv,
      data,
    },
  };
};

export default IosDev;
