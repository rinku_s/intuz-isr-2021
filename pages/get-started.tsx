import React from "react";
// import HeadAndFoot from "../components/HeadAndFoot/HeadAndFoot";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
// import { withApollo } from "../config/apollo";
import SectionForm from "../containers/GetStartedPage/SectionForm";
import { getUniversal } from "../lib/api";
import HeadAndFootNew from "../components/HeadAndFoot/HeadAndFootNew";
import { GetStaticProps } from "next";

const Index = (props) => {
  return (
    <HeadAndFootNew data={props.uv}>
      <Layout whiteBackground>
        <SectionForm />
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};
export const getStaticProps: GetStaticProps = async (context) => {
  let uv = await getUniversal("/get-started");

  return {
    props: {
      uv,
    },
    revalidate: false,
  };
};

export default Index;
