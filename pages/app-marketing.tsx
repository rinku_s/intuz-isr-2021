// import gql from "graphql-tag";
import NewBanner from "containers/ServicePageBanners/bannerPage";
import React from "react";
import HeadAndFootNew from "../components/HeadAndFoot/HeadAndFootNew";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import Marketing from "../containers/App-Marketing/SectionAppMarketing/AppMarketing";
import PackageNavigation from "../containers/App-Marketing/SectionPackageNavigation/PackageNavigation";
import ReportNumber from "../containers/App-Marketing/SectionReportNUmbers/ReportNumber";
import SectionIconDetail from "../containers/CommonSections/SectionIconDetail/SectionIconDetail";
import SectionDetailText from "../containers/IosDevelopment/SectionDetailText/SectionDetailText";
import { getAppMarketingPageData, getUniversal } from "../lib/api";

const AppMarket = ({ data, uv }) => {
  return (
    <HeadAndFootNew data={uv}>
      <Layout>
        {/* <ServicePageBanner content={data.apppage.sectionbanner} /> */}
        {/* <ApplicationPageBanner
          content={data.apppage.sectionbanner}
          variation="appMarketing"
        /> */}
        <NewBanner title="Mobile App Marketing Services" backImage={"App_Marketing_136e9a8144.png"} buttonLabel='Let’s Connect' link='/get-started' subTitle='The key to your app’s ignition' />

        <ReportNumber />
        <SectionDetailText
          title="You do the math!"
          description="The app space is getting crowded every second, and your app/app idea is becoming just one among thousands. The expectations of an entrepreneur and the masses of <span style='color:#684574
                        '> unique apps </span> are getting extinct and we can find only rare fossils. In an app space, where your idea loses its uniqueness faster than time, it is indispensable to <span style='color:#684574
                        '> outbid the crowd</span>. Your app ideas are propelling and mind blowing, developed and executed right as well, but not marketed. It’s like building a beautiful house that no one knows the address of. So what do you do? Either make the house so tall that it's visible to everyone, or spread the word on its whereabouts.<br /><br /><br />Intuz integrated app marketing solutions are second to none with unmatchable focus and experience on maximizing the ROI. Our user acquisition and engagement services will guarantee your <span style='color:#684574'> app’s visibility </span>,
                        performance and downloads right from the launch.<br /><br /><br />
                        <span>Talk to the expert Intuz App marketing team for a free consultation.</span>"
        />
        <SectionIconDetail
          title="App Performance & Monetisation"
          topImage="App_Performance_Monetisation.svg"
        >
          {data.apppage.sectiontopicondetails}
        </SectionIconDetail>
        <Marketing
          title="User Acquisition, Advertisement & Visibility"
          topImage="User_Acquisition.svg"
        >
          {data.apppage.sectionmiddleicondetails}
        </Marketing>
        <PackageNavigation />
        <SectionIconDetail
          title="User Engagement & Retention"
          topImage="User_Engagement_Retention.svg"
        >
          {data.apppage.sectionbottomicondetails}
        </SectionIconDetail>
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};
export async function getStaticProps(context) {
  console.log(context);
  let uv = await getUniversal("/app-marketing");
  let data = await getAppMarketingPageData();

  return {
    props: {
      uv,
      data,
    },
  };
}

export default AppMarket;
