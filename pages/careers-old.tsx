import React from "react";
import HeadAndFootNew from "../components/HeadAndFoot/HeadAndFootNew";
import Layout from "../components/Layout/Layout";
import LetsTalkComp from "../components/LetsTalkComp/LetsTalkComp";
import SectionCareerProcess from "../containers/CareerPage/SectionCareerProcess/SectionCareerProcess";
import SectionCareerTeam from "../containers/CareerPage/SectionCareerTeam/SectionCareerTeam";
import ServiceBanner from "../containers/ServicePageBanners/ServicePageBanner";
import { getCareersData, getUniversal } from "../lib/api";

const Career = ({ uv, data }) => {
  let bannerContent = {
    title: "Careers",
    subTitle:
      "Transform your each step into a journey by working with great minds of industry",
    backImage: {
      name: "careerpage-banner.jpg",
    },
  };
  // console.log("Careers Data", data);
  return (
    <HeadAndFootNew data={uv}>
      <Layout>
        <ServiceBanner careers content={bannerContent} />
        <SectionCareerProcess />
        <SectionCareerTeam data={data.careers} />
        <LetsTalkComp />
      </Layout>
    </HeadAndFootNew>
  );
};
export async function getStaticProps(context) {
  console.log(context);
  let uv = await getUniversal("/careers");
  let data = await getCareersData();
  return {
    props: {
      uv,
      data,
    },
  };
}
export default Career;
