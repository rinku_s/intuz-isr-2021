import { getHireAppPageData, getUniversal } from 'lib/api';
import { GetStaticProps } from 'next';
import React, { useState } from 'react';
import CaseStudyCarousel from '../components/CaseStudyCarousel/CaseStudyCarousel';
import Layout from '../components/Layout/Layout';
import LetsTalkComp from '../components/LetsTalkComp/LetsTalkComp';
import AwardsAndRecognition from '../containers/CommonSections/AwardandRecognition/AwardsAndRecognition';
import ClientTestimonialCarousel from '../containers/CommonSections/ClientTestimonialCarousel/ClientTestimonialCarousel';
import TrustedSection from '../containers/CommonSections/TrustedSection/TrustedSection';
import FormSection from '../containers/HireDDPage/FormSection/FormSection';
import HiringProcessSection from '../containers/HireDDPage/HiringProcessSection/HiringProcess';
import MeetTheDeveloperSection from '../containers/HireDDPage/MeetTheDeveloperSection/MeetTheDeveloperSection';
import SkillSection from '../containers/HireDDPage/SkillSection/SkillSection';
import WeGetitSection from '../containers/HireDDPage/WeGetitSection/WeGetit';
import ServicePageBanner from '../containers/ServicePageBanners/ServicePageBanner';
import DeveloperContext from '../context/developerContext';



const HireDDPAge = ({ uv, data }) => {
    const [dev, setDev] = useState();
    return (


        <Layout>
            <DeveloperContext.Provider value={{ dev, setDev }}>
                <ServicePageBanner content={data.apppage.sectionbanner} variation='hireApp' />
                <MeetTheDeveloperSection />
                <TrustedSection heading f="s" light />
                <HiringProcessSection />
                <SkillSection />
                <CaseStudyCarousel />
                <WeGetitSection />
                <ClientTestimonialCarousel content={data.testimonialcarouselcontents} />
                <AwardsAndRecognition awardandrecognitions={data.awardandrecognitions} />
                <FormSection />
                <LetsTalkComp />
            </DeveloperContext.Provider>
        </Layout>

    )
}
export const getStaticProps: GetStaticProps = async (context) => {
    // console.log(context);
    let uv = await getUniversal('/hire-app-developers');
    let data = await getHireAppPageData();

    return {
        props: {
            uv,
            data
        }
    }
}

export default HireDDPAge
