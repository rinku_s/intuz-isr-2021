import { gql } from "graphql-request";

export const trustedSectionQuery = gql`

query{
    sectionclients(limit:8){
        id
        title
        image{
          name
        }
      }
}
`;

export const cloudFrontQuery = gql`
{
  cloudPage(id:"5ede18a187ae723e7a0701bc"){
    title
   description
   image{
    name
  }
   meta_tags
   footer_scripts
 }
    cloudGuide(id:"5ea17c836b554020a66780e4"){
      title
      subtitle
      title_first
      content
      logo{
        name
      }
    }
  }
`;

export const homepageQuery = gql`
query {
    sectionservices(limit: 4) {
      id
      image {
        name
      }
      title
      link
    }

    sectionclients(limit: 8) {
      id
      title
      image {
        name
      }
    }
    guides(sort: "updatedAt:desc", limit: 3, where: { ready: true }) {
      id
      title
      resourceBlockImage {
        name
      }
      image {
        name
      }
      link
    }
  }
`;

