import { gql } from "graphql-request";
import { callAPI } from "lib/api";

export async function getLatestGuides() {
    let query  = gql`
    {
        guides(sort: "updatedAt:desc", limit: 6, where: { ready: true }) {
            id
            resourceBlockImage {
              name
            }
            image {
              name
            }
            title
            link
            description
            updatedAt
          }
    }
    `

    const data = await callAPI(query);
    return data
}