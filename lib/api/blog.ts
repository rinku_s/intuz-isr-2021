import { gql } from "graphql-request";
import { callAPI } from "lib/api";
import { CategoryListModel } from "./CategoryListModel";

export async function getLatestBlogs() {
    let query  = gql`
    {
        blogs(sort: "createdAt:desc", limit: 4, where: { done: true }) {
          id
          title
          link
          description
          updatedAt
          resourceBlockImage {
            name
          }
          blog_category {
            id
            name
            link
          }
          blog_author {
            id
            name
            image {
              name
            }
            link
          }
        }
    }
    `

    const data = await callAPI(query);
    return data
}

export async function getBlogsForCategory(slug:string,limit:number) {
    let query  = gql`
    {
        blogs(sort: "createdAt:desc", limit: ${limit}, where: { done: true, blog_category:{
            link:"${slug}"
          }  }) {
          id
          title
          
          link
          description
          updatedAt
          resourceBlockImage {
            name
          }
          blog_category {
            id
            name
            link
          }
          blog_author {
            id
            name
            image {
              name
            }
            link
          }
        }
      }       
    `
    const data = await callAPI(query);
    return data
}


export async function getAllCategories() : Promise<CategoryListModel> {
    let query = gql`
    {
        blogCategories{
          name
          link
        }
      }
    `
    const data = await callAPI<CategoryListModel>(query)
    return data;
}


export async function getCategory(slug:string) {
    let query = gql `
    {
        blogCategories(where:{
          link:"${slug}"
        }) {
          name
          banner {
            name
          }
          subTitle
          link
        }
      }
      `
      const data = await callAPI(query)
      return data;
}



export async function getBlogAuthorData(link: string) {
    const allQuery = gql`
    query {
        blogAuthors(where: { link: "${link}" }) {
          id
          name
          authorInfo
          image {
            name
          }
          link
          designation
          linkedinUrl
          twitterUrl
          blog{
            id
          title
          link
          description
          updatedAt
          resourceBlockImage {
            name
          }
          blog_category {
            id
            name
            link
          }
          blog_author {
            id
            name
            image {
              name
            }
            link
          }
          }
        }
      }
      
      `;
  
    let data = await callAPI(allQuery);
    return data;
  }
  